package com.yalantis.flipviewpager.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ProgressBar;

import com.yalantis.flipviewpager.R;
import com.yalantis.flipviewpager.utils.FlipSettings;
import com.yalantis.flipviewpager.view.FlipViewPager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Yalantis
 */
public abstract class BaseFlipAdapter extends RecyclerView.Adapter {

    protected Map<Integer, FlipViewPager> mMapFlipViewPager = new HashMap<>();
    private List mArrFlipViewItems;
    private FlipSettings mFlipSettings;

    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    public BaseFlipAdapter(List mArrFlipViewItems, FlipSettings mFlipSettings) {
        this.mArrFlipViewItems = mArrFlipViewItems;
        this.mFlipSettings = mFlipSettings;
    }

    public interface CloseListener {
        void onClickClose();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.flipper, parent, false);
            CustomViewHolder viewHolder = new CustomViewHolder(itemView);
            viewHolder.mFlipViewPager = itemView.findViewById(R.id.flip_view);
            return viewHolder;
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof CustomViewHolder) {

            Object item1 = mArrFlipViewItems.get(position);

            // Listener to store flipped page
            ((CustomViewHolder) holder).mFlipViewPager.setOnChangePageListener(new FlipViewPager.OnChangePageListener() {
                @Override
                public void onFlipped(int page) {
                    mFlipSettings.savePageState(position, page);
                }
            });

            mMapFlipViewPager.put(position, ((CustomViewHolder) holder).mFlipViewPager);

            ((CustomViewHolder) holder).mFlipViewPager.setAdapter(new FlipViewAdapter(item1, position),
                    mFlipSettings.getDefaultPage(), position, mArrFlipViewItems.size());
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return mArrFlipViewItems == null ? 0 : mArrFlipViewItems.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrFlipViewItems.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    public abstract View getPage(int position, View convertView, ViewGroup parent, Object item1, CloseListener closeListener);

    public abstract int getPagesCount();

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    // Adapter merges 2 items together
    private class FlipViewAdapter extends BaseAdapter implements CloseListener {
        private Object item1;
        private int position;

        public FlipViewAdapter(Object item1, int position) {
            this.item1 = item1;
            this.position = position;
        }

        @Override
        public int getCount() {
            return getPagesCount();
        }

        @Override
        public Object getItem(int position) {
            return position; // Stub
        }

        @Override
        public long getItemId(int position) {
            return position; // Stub
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getPage(position, convertView, parent, item1, this);
        }

        @Override
        public void onClickClose() {
            mMapFlipViewPager.get(position).flipToPage(1);
        }
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder {
        FlipViewPager mFlipViewPager;

        public CustomViewHolder(View itemView) {
            super(itemView);
        }
    }
}
