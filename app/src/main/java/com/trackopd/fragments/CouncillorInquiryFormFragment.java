package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.R;
import com.trackopd.adapter.CouncillorInquiryFormAdapter;
import com.trackopd.adapter.CouncillorPackageAdapter;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.CouncillorInquiryFormsModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.utils.RecyclerProgressView;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CouncillorInquiryFormFragment extends Fragment {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private SwipeRefreshLayout mSwipeRefreshView;
    private RecyclerView mRecyclerView;
    private AppUtil mAppUtils;
    private RelativeLayout mRelativeNoData, mRelativeNoInternet;
    private Button mButtonRetry;
    private boolean isFirstTime = true;
    private ArrayList<CouncillorInquiryFormsModel> mArrCouncillorInquiryForms;
    private CouncillorInquiryFormAdapter mAdapterCouncillorInquiryForm;
    private String mUserType = "", mName = "", mMobileNo = "", mCouncillorDate = "1970-01-01";
    private int mCouncillorId = -1;

    // Load More Listener Variables
    private int currentPageIndex = 1, lastFetchRecord = 0, totalRecords = 0;
    private boolean isRefresh = false, isLoadMore = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_councillor_inquiry_form, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mAppUtils = new AppUtil(getActivity());
        mArrCouncillorInquiryForms = new ArrayList<>();
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        getBundle();
        getIds();
        setRegListeners();
        callShimmerView();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        final MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(true);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.action_search:
                callToSearchCouncillorInquiryFormFragment();
                return true;

            case R.id.action_add:
                openPopup();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                //mAddAppointment = bundle.getString(AppConstants.BUNDLE_ADD_APPOINTMENT);
                mName = bundle.getString(AppConstants.BUNDLE_PATIENT_NAME);
                mMobileNo = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mCouncillorDate = bundle.getString(AppConstants.BUNDLE_PATIENT_COUNCILLOR_DATE);
                mCouncillorId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_COUNCILLOR);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Recycler View
            mRecyclerView = mView.findViewById(R.id.recycler_view);

            // Swipe Refresh View
            mSwipeRefreshView = mView.findViewById(R.id.swipe_refresh_view);

            // Relative Layouts
            mRelativeNoData = mView.findViewById(R.id.relative_no_data_available);
            mRelativeNoInternet = mView.findViewById(R.id.relative_no_internet);

            // Buttons
            mButtonRetry = mView.findViewById(R.id.button_retry);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Click Listeners
            mButtonRetry.setOnClickListener(clickListener);

            // ToDo: Set On Refresh Listeners
            mSwipeRefreshView.setOnRefreshListener(refreshListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_retry:
                    callShimmerView();
                    break;
            }
        }
    };

    /**
     * Set On Refresh Listeners
     */
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            isFirstTime = true;

            if (!mAppUtils.getConnectionState()) {
                mRelativeNoInternet.setVisibility(View.VISIBLE);
                mSwipeRefreshView.setRefreshing(false);
                mArrCouncillorInquiryForms.clear();
                setViewVisibility();
            } else {
                mRelativeNoInternet.setVisibility(View.GONE);
                if (!isLoadMore) {
                    isRefresh = true;

                    mName = AppConstants.STR_EMPTY_STRING;
                    mMobileNo = AppConstants.STR_EMPTY_STRING;
                    mCouncillorDate = "1970-01-01";
                    mCouncillorId = -1;

                    if (mArrCouncillorInquiryForms != null)
                        mArrCouncillorInquiryForms.clear();
                    if (mAdapterCouncillorInquiryForm != null)
                        mAdapterCouncillorInquiryForm.notifyDataSetChanged();
                    currentPageIndex = 1;
                    callShimmerView();
                } else
                    mSwipeRefreshView.setRefreshing(false);
            }
        }
    };

    /**
     * This method is used to call the shimmer effect for recycler view before calling the API
     */
    private void callShimmerView() {
        if (!mAppUtils.getConnectionState()) {
            mRelativeNoInternet.setVisibility(View.VISIBLE);
            mArrCouncillorInquiryForms.clear();
            setViewVisibility();
        } else {
            mRelativeNoInternet.setVisibility(View.GONE);
            if (isFirstTime) {
                showProgressView();
                isFirstTime = false;
                callInquiryFormListAPI();
            } else {
                callInquiryFormListAPI();
            }
        }
    }

    /**
     * This method should call the Inquiry Form for councillor
     */
    private void callInquiryFormListAPI() {
        try {
            if (mName == null) {
                mName = AppConstants.STR_EMPTY_STRING;
            }

            if (mMobileNo == null) {
                mMobileNo = AppConstants.STR_EMPTY_STRING;
            }

            if (mCouncillorDate == null) {
                mCouncillorDate = "1970-01-01";
            }

            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setCouncillorInquiryFormListJson(currentPageIndex, mMobileNo, mName, mCouncillorId,
                            mCouncillorDate, mDatabaseName));

            Call<CouncillorInquiryFormsModel> call = RetrofitClient.createService(ApiInterface.class).getCouncillorInquiryFormsList(requestBody);
            call.enqueue(new Callback<CouncillorInquiryFormsModel>() {
                @Override
                public void onResponse(@NonNull Call<CouncillorInquiryFormsModel> call, @NonNull Response<CouncillorInquiryFormsModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        stopProgressView();
                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                ArrayList<CouncillorInquiryFormsModel> inquiryFormsModel = new ArrayList<>(Arrays
                                        .asList(new GsonBuilder().serializeNulls().create()
                                                .fromJson(new JsonParser().parse(jsonObject.getString(WebFields.DATA)).
                                                        getAsJsonArray(), CouncillorInquiryFormsModel[].class)));

                                if (mArrCouncillorInquiryForms != null && mArrCouncillorInquiryForms.size() > 0 &&
                                        mAdapterCouncillorInquiryForm != null) {
                                    mArrCouncillorInquiryForms.addAll(inquiryFormsModel);
                                    mAdapterCouncillorInquiryForm.notifyDataSetChanged();
                                    lastFetchRecord = mArrCouncillorInquiryForms.size();
                                } else {
                                    mArrCouncillorInquiryForms = inquiryFormsModel;
                                    setAdapterData();
                                    lastFetchRecord = mArrCouncillorInquiryForms.size();
                                    totalRecords = jsonObject.getInt(WebFields.ROW_COUNT);
                                    setLoadMoreClickListener();

                                    if (mRecyclerView.getVisibility() == View.GONE) {
                                        stopProgressView();
                                    }
                                }
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                            stopProgressView();
                            setViewVisibility();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CouncillorInquiryFormsModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());
                    stopProgressView();
                    setViewVisibility();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mGridLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(mGridLayoutManager);

            stopProgressView();
            mAdapterCouncillorInquiryForm = new CouncillorInquiryFormAdapter(mActivity, mRecyclerView, mArrCouncillorInquiryForms);
            mRecyclerView.setAdapter(mAdapterCouncillorInquiryForm);

            setViewVisibility();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the view visibility as per the array size
     */
    private void setViewVisibility() {
        if (mArrCouncillorInquiryForms.size() != 0) {
            mRelativeNoData.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mRelativeNoData.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    /**
     * Show progress view when page load for the first time
     */
    private void showProgressView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.VISIBLE);

                RecyclerProgressView.startProgress((LinearLayout) mView.findViewById(R.id.linear_add_recycler_progress_view),
                        R.layout.row_councillor_inquiry_forms_item, getActivity());
            }
        }, 100);
    }

    /**
     * Show progress view when page load for the first time
     */
    private void stopProgressView() {
        try {
            mSwipeRefreshView.setRefreshing(false);
            isRefresh = false;
            isLoadMore = false;
            RecyclerProgressView.stopProgress();
            mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            hideProgressBar();
        }
    }

    /**
     * Open popup for Appointment
     */
    private void openPopup() {
        try {
            new MaterialDialog.Builder(mActivity)
                    .title(R.string.app_name)
                    .items(R.array.patient_type)
                    .canceledOnTouchOutside(false)
                    .positiveText(R.string.action_ok)
                    .positiveColor(getResources().getColor(R.color.app_main_theme))
                    .negativeText(R.string.action_cancel)
                    .negativeColor(getResources().getColor(R.color.colorControlNormal))
                    .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                        @Override
                        public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                            if (which == 0) {
                                callToAddPatientFragment();
                            } else {
                                callToExistingAppointmentFragment();
                            }
                            return true;
                        }
                    })
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Receptionist Existing Appointment Fragment
     */
    private void callToExistingAppointmentFragment() {
        try {
            ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.header_search));
            Fragment fragment = new SearchReceptionistPatientFragment();
            Bundle args = new Bundle();
//            args.putString(AppConstants.BUNDLE_ADD_PATIENT, mAddAppointment);
            args.putString(AppConstants.BUNDLE_IS_FROM_SEARCH, AppConstants.BUNDLE_ADD_COUNCILLOR);
            fragment.setArguments(args);
            FragmentManager fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, getResources().getString(R.string.tag_search_receptionist_patient))
                    .addToBackStack(getResources().getString(R.string.back_stack_search_receptionist_patient))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Receptionist Add Patient Fragment
     */
    private void callToAddPatientFragment() {
        try {
            ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.nav_menu_patient));
            Fragment fragment = new AddPatientFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_LOGIN_TYPE, mUserType);
            fragment.setArguments(args);
            FragmentManager fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, getResources().getString(R.string.tag_add_new_patient))
                    .addToBackStack(getResources().getString(R.string.back_stack_add_new_patient))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Search Councillor Inquiry Form Fragment
     */
    private void callToSearchCouncillorInquiryFormFragment() {
        try {
            ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.header_search));
            Fragment fragment = new SearchCouncillorInquiryFormFragment();
            FragmentManager fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_search_councillor_inquiry_form))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_search_councillor_inquiry_form))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Hide the progress bar
     */
    private void hideProgressBar() {
        try {
            if (mArrCouncillorInquiryForms != null && mArrCouncillorInquiryForms.size() > 0 &&
                    mArrCouncillorInquiryForms.get(mArrCouncillorInquiryForms.size() - 1) == null) {

                mArrCouncillorInquiryForms.remove(mArrCouncillorInquiryForms.size() - 1);
                mAdapterCouncillorInquiryForm.notifyItemRemoved(mArrCouncillorInquiryForms.size());

                mAdapterCouncillorInquiryForm.notifyDataSetChanged();
                mAdapterCouncillorInquiryForm.setLoaded();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * On load more click listener
     */
    private void setLoadMoreClickListener() {
        mAdapterCouncillorInquiryForm.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (mArrCouncillorInquiryForms != null && mArrCouncillorInquiryForms.size() > 0 && mArrCouncillorInquiryForms.size() <
                        totalRecords) {
                    if (!mAppUtils.getConnectionState()) {
                        mRelativeNoInternet.setVisibility(View.VISIBLE);
                        mArrCouncillorInquiryForms.clear();
                        setViewVisibility();
                    } else {
                        mRelativeNoInternet.setVisibility(View.GONE);
                        if (!isRefresh) {
                            isLoadMore = true;
                            mArrCouncillorInquiryForms.add(null);
                            mAdapterCouncillorInquiryForm.notifyItemInserted(mArrCouncillorInquiryForms.size() - 1);

                            currentPageIndex = (mArrCouncillorInquiryForms.size() / Common.getPageSizeForPagination(mActivity)) + 1;
                            callInquiryFormListAPI();
                        }
                    }
                }
            }
        });
    }
}