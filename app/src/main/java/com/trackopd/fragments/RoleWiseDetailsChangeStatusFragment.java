package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.trackopd.R;
import com.trackopd.adapter.RoleWisePatientListAdapter;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.StatusTabListModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.utils.RecyclerProgressView;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoleWiseDetailsChangeStatusFragment extends Fragment {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private SwipeRefreshLayout mSwipeRefreshView;
    private RecyclerView mRecyclerView;
    private AppUtil mAppUtils;
    private RelativeLayout mRelativeNoData, mRelativeNoInternet;
    private Button mButtonRetry;
    private boolean isFirstTime = true, isStarted = false, isVisible = false;
    private String mStatusId, mUserType;
    private ArrayList<StatusTabListModel> mArrStatusTabList;
    private RoleWisePatientListAdapter mAdapterRoleWisePatient;

    // Load More Listener Variables
    private int currentPageIndex = 1, lastFetchRecord = 0, totalRecords = 0;
    private boolean isRefresh = false, isLoadMore = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_role_wise, container, false);
        mActivity = getActivity();
        mAppUtils = new AppUtil(getActivity());
        mArrStatusTabList = new ArrayList<>();

        getBundle();
        getIds();
        setRegListeners();
        return mView;
    }


    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mStatusId = bundle.getString(WebFields.GET_STATUS_TAB_LIST.REQUEST_STATUS_ID);
                mUserType = bundle.getString(AppConstants.BUNDLE_LOGIN_TYPE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Recycler View
            mRecyclerView = mView.findViewById(R.id.recycler_view);

            // Swipe Refresh View
            mSwipeRefreshView = mView.findViewById(R.id.swipe_refresh_view);

            // Relative Layouts
            mRelativeNoData = mView.findViewById(R.id.relative_no_data_available);
            mRelativeNoInternet = mView.findViewById(R.id.relative_no_internet);

            // Buttons
            mButtonRetry = mView.findViewById(R.id.button_retry);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Click Listeners
            mButtonRetry.setOnClickListener(clickListener);

            // ToDo: Set On Refresh Listeners
            mSwipeRefreshView.setOnRefreshListener(refreshListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_retry:
                    callShimmerView();
                    break;
            }
        }
    };

    /**
     * Set On Refresh Listeners
     */
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {

        @Override
        public void onRefresh() {
            isFirstTime = true;

            if (!mAppUtils.getConnectionState()) {
                mRelativeNoInternet.setVisibility(View.VISIBLE);
                mSwipeRefreshView.setRefreshing(false);
                mArrStatusTabList.clear();
                setViewVisibility();
            } else {
                mRelativeNoInternet.setVisibility(View.GONE);
                mRelativeNoData.setVisibility(View.GONE);
                if (!isLoadMore) {
                    isRefresh = true;

                    if (mArrStatusTabList != null)
                        mArrStatusTabList.clear();
                    if (mAdapterRoleWisePatient != null)
                        mAdapterRoleWisePatient.notifyDataSetChanged();
                    currentPageIndex = 1;
                    callShimmerView();
                } else
                    mSwipeRefreshView.setRefreshing(false);
            }
        }
    };

    /**
     * This method is used to call the shimmer effect for recycler view before calling the API
     */
    private void callShimmerView() {
        if (!mAppUtils.getConnectionState()) {
            mRelativeNoInternet.setVisibility(View.VISIBLE);
            setViewVisibility();
        } else {
            mRelativeNoInternet.setVisibility(View.GONE);
            if (isFirstTime) {
                showProgressView();
                isFirstTime = false;
            }
            callStatusTabListAPI();
        }
    }

    /**
     * This method should call the Change Status Tab Detail List API
     */
    private void callStatusTabListAPI() {
        try {
            String mName = AppConstants.STR_EMPTY_STRING;
            String mPatientCode = AppConstants.STR_EMPTY_STRING;
            String mMobileNo = AppConstants.STR_EMPTY_STRING;
            String mAppointmentNo = AppConstants.STR_EMPTY_STRING;
            String mAppointmentDate = "0000-00-00";

            String mUserId = "";
            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
            } else {
                mUserId = "-1";
            }

            String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.getStatusTabList(currentPageIndex, mName, mPatientCode, mMobileNo, mAppointmentNo, mAppointmentDate, mStatusId, mUserId,hospital_database));

            Call<StatusTabListModel> call = RetrofitClient.createService(ApiInterface.class).getStatusTabList(requestBody);
            call.enqueue(new Callback<StatusTabListModel>() {
                @Override
                public void onResponse(@NonNull Call<StatusTabListModel> call, @NonNull Response<StatusTabListModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        stopProgressView();
                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                ArrayList<StatusTabListModel> appointmentModel = new ArrayList<>(Arrays
                                        .asList(new GsonBuilder().serializeNulls().create()
                                                .fromJson(new JsonParser().parse(jsonObject.getString(WebFields.DATA)).
                                                        getAsJsonArray(), StatusTabListModel[].class)));

                                if (mArrStatusTabList != null && mArrStatusTabList.size() > 0 &&
                                        mAdapterRoleWisePatient != null) {
                                    mArrStatusTabList.addAll(appointmentModel);
                                    mAdapterRoleWisePatient.notifyDataSetChanged();
                                    lastFetchRecord = mArrStatusTabList.size();
                                } else {
                                    mArrStatusTabList = appointmentModel;
                                    setAdapterData();
                                    lastFetchRecord = mArrStatusTabList.size();
                                    totalRecords = jsonObject.getInt(WebFields.ROW_COUNT);
                                    setLoadMoreClickListener();

                                    if (mRecyclerView.getVisibility() == View.GONE) {
                                        stopProgressView();
                                    }
                                }
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                            stopProgressView();
                            setViewVisibility();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<StatusTabListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    stopProgressView();
                    setViewVisibility();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mGridLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(mGridLayoutManager);

            stopProgressView();
            mAdapterRoleWisePatient = new RoleWisePatientListAdapter(mActivity, mRecyclerView, mArrStatusTabList);
            mRecyclerView.setAdapter(mAdapterRoleWisePatient);

            setViewVisibility();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the view visibility as per the array size
     */
    private void setViewVisibility() {
        if (mArrStatusTabList.size() != 0) {
            mRelativeNoData.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mRelativeNoData.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    /**
     * Show progress view when page load for the first time
     */
    private void showProgressView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.VISIBLE);

                RecyclerProgressView.startProgress((LinearLayout) mView.findViewById(R.id.linear_add_recycler_progress_view),
                        R.layout.row_role_wise_patinet_list, getActivity());
            }
        }, 100);
    }

    /**
     * Show progress view when page load for the first time
     */
    private void stopProgressView() {
        try {
            mSwipeRefreshView.setRefreshing(false);
            isRefresh = false;
            isLoadMore = false;
            RecyclerProgressView.stopProgress();
            mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            hideProgressBar();
        }
    }

    /**
     * Hide the progress bar
     */
    private void hideProgressBar() {
        try {
            if (mArrStatusTabList != null && mArrStatusTabList.size() > 0 &&
                    mArrStatusTabList.get(mArrStatusTabList.size() - 1) == null) {

                mArrStatusTabList.remove(mArrStatusTabList.size() - 1);
                mAdapterRoleWisePatient.notifyItemRemoved(mArrStatusTabList.size());

                mAdapterRoleWisePatient.notifyDataSetChanged();
                mAdapterRoleWisePatient.setLoaded();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * On load more click listener
     */
    private void setLoadMoreClickListener() {
        mAdapterRoleWisePatient.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (mArrStatusTabList != null && mArrStatusTabList.size() > 0 && mArrStatusTabList.size() <
                        totalRecords) {
                    if (!mAppUtils.getConnectionState()) {
                        mRelativeNoInternet.setVisibility(View.VISIBLE);
                        mArrStatusTabList.clear();
                        setViewVisibility();
                    } else {
                        mRelativeNoInternet.setVisibility(View.GONE);
                        if (!isRefresh) {
                            isLoadMore = true;
                            mArrStatusTabList.add(null);
                            mAdapterRoleWisePatient.notifyItemInserted(mArrStatusTabList.size() - 1);

                            currentPageIndex = (mArrStatusTabList.size() / Common.getPageSizeForPagination(mActivity)) + 1;
                            callStatusTabListAPI();
                        }
                    }
                }
            }
        });
    }

    /**
     * Change Status in Role Wise Change Status Fragment on Start
     *
     * @param isVisibleToUser - This can check the fragment is visible when swiping and will return
     *                        the boolean value based on that
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        isVisible = isVisibleToUser;
        if (isStarted && isVisible) {
            mArrStatusTabList.clear();
            if (mAdapterRoleWisePatient != null) {
                mAdapterRoleWisePatient.notifyDataSetChanged();
            }
            currentPageIndex = 1;
            getBundle();
            callShimmerView();
            setAdapterData();
        }
    }

    /**
     * Change Status in Role Wise Change Status Fragment on Start
     */
    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
            mArrStatusTabList.clear();
            if (mAdapterRoleWisePatient != null) {
                mAdapterRoleWisePatient.notifyDataSetChanged();
            }
            currentPageIndex = 1;
            getBundle();
            callShimmerView();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isStarted = false;
        isVisible = false;
    }
}
