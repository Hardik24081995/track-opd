package com.trackopd.fragments;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.trackopd.R;
import com.trackopd.adapter.MenuPaymentAdapter;
import com.trackopd.model.PrintReceiptModel;
import com.trackopd.model.ReceptionistPaymentModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuPaymentListingFragment extends Fragment {

    Activity mActivity;
    private Menu menu;
    private AppUtil mAppUtils;
    RelativeLayout mRelativeNoDataFound;
    View mView;
    RecyclerView rv_payment;
    ImageView mImageBack;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    ArrayList<String> mArrayList = new ArrayList<>();
    private ArrayList<ReceptionistPaymentModel> mArrReceptionistPayment = new ArrayList<>();

    private boolean mHistoryAndComplaints = false, mVisionUCVA = false, mVisionBCVAUndilated = false, mVisionFinal = false,
            mVisionBCVADilated = false, mPrimaryExamination = false, mDiagnosis = false, mInvestigationSuggested = false,
            mTreatmentSuggested = false, mCounselingDetails = false, mSurgeryDetails = false, mPrescription = false, mPayment = true;


    public MenuPaymentListingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_menu_payment_listing, container, false);

        mActivity = getActivity();
        mAppUtils = new AppUtil(getActivity());

        getID();
        callPaymentListAPI();

        setHasOptionsMenu(true);

        mSwipeRefreshLayout.setOnRefreshListener(() -> mSwipeRefreshLayout.setRefreshing(false));

        return mView;
    }

    /**
     * This method should call the payment listing for receptionist
     */
    private void callPaymentListAPI() {
        try {

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            int currentPageIndex = 1;

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setReceptionistPaymentListJson(currentPageIndex, "", "", "", "", "0000-00-00", hospital_database));

            Call<ReceptionistPaymentModel> call = RetrofitClient.createService(ApiInterface.class).getReceptionistPaymentList(requestBody);
            call.enqueue(new Callback<ReceptionistPaymentModel>() {
                @Override
                public void onResponse(@NonNull Call<ReceptionistPaymentModel> call, @NonNull Response<ReceptionistPaymentModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {

                        if (response.body().getError() == AppConstants.API_SUCCESS_ERROR) {

                            mArrReceptionistPayment.addAll(response.body().getData());
                            setAdapter();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ReceptionistPaymentModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        final MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(false);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);
    }

    private void setAdapter() {
        MenuPaymentAdapter sitesAdapter = new MenuPaymentAdapter(mActivity, mArrReceptionistPayment) {
            @Override
            protected void getPaymentPrint(String AppointmentID) {
                super.getPaymentPrint(AppointmentID);
                getPrint(AppointmentID);
            }
        };
        rv_payment.setHasFixedSize(true);
        rv_payment.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_payment.setAdapter(sitesAdapter);
    }

    private void getPrint(String appointmentID) {
        try {


            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPrintReceiptJson(AddNewPreliminaryExaminationFragment.PreliminaryExminationId,
                            appointmentID, mHistoryAndComplaints, mVisionUCVA, mVisionBCVAUndilated,
                            mVisionFinal, mVisionBCVADilated, mPrimaryExamination, mDiagnosis, mInvestigationSuggested,
                            mTreatmentSuggested, mCounselingDetails, mSurgeryDetails, mPrescription,"",false, mPayment, hospital_database));

            Call<PrintReceiptModel> call = RetrofitClient.createService(ApiInterface.class).PrintReceipt(body);
            call.enqueue(new Callback<PrintReceiptModel>() {
                @Override
                public void onResponse(@NonNull Call<PrintReceiptModel> call, @NonNull Response<PrintReceiptModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body() != null) {
                                String url = response.body().getData();
                                callToReceiptPrint(url);
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PrintReceiptModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Call To Receipt Print
     *
     * @param url - String Url Print
     */
    private void callToReceiptPrint(String url) {
        /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);*/

        //   String pdfOpen = AppConstants.PDF_OPEN + url;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mActivity.startActivity(browserIntent);

        /*Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(pdfOpen));
        startActivity(i);*/
    }

    private void getID() {
        rv_payment = mView.findViewById(R.id.rv_payment);
        mSwipeRefreshLayout = mView.findViewById(R.id.swipeToRefresh);
        mRelativeNoDataFound = mView.findViewById(R.id.relative_no_data_available);
    }

}
