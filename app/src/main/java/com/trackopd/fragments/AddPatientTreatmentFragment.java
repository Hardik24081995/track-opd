package com.trackopd.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.KeyboardUtility;

public class AddPatientTreatmentFragment extends Fragment {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private EditText mEditName;
    private ImageView mImageAddMedicine;
    private LinearLayout mLinearAddMedicine;
    private TextView mTextDateTime;
    private Button mButtonSubmit;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_add_patient_treatment, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        getIds();
        setRegListeners();
        setData();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Set the prepare menu options
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);

        MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(false);
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Edit Texts
            mEditName = mView.findViewById(R.id.edit_add_patient_treatment_plan);

            // Text Views
            mTextDateTime = mView.findViewById(R.id.text_add_patient_treatment_date);

            // Linear Layouts
            mLinearAddMedicine = mView.findViewById(R.id.linear_add_patient_treatment_add_medicine);

            // Image Views
            mImageAddMedicine = mView.findViewById(R.id.image_add_patient_treatment_add_medicine);

            // Buttons
            mButtonSubmit = mView.findViewById(R.id.button_add_patient_treatment_submit);

            // Set Request Focus
            mEditName.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mTextDateTime.setOnClickListener(clickListener);
            mImageAddMedicine.setOnClickListener(clickListener);
            mButtonSubmit.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            // Set current date and time
            mTextDateTime.setText(Common.setCurrentDateTime(mActivity));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.text_add_patient_treatment_date:
                    Common.openDateTimePicker(mActivity, mTextDateTime);
                    break;

                case R.id.image_add_patient_treatment_add_medicine:
                    addMedicineDynamically();
                    break;

                case R.id.button_add_patient_treatment_submit:
                    doTreatment();
                    break;
            }
        }
    };

    /**
     * This method checks the validation first then call the API
     */
    private void doTreatment() {
        KeyboardUtility.HideKeyboard(mActivity, mEditName);
        if (checkValidation()) {
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {
        boolean status = true;

        String mName = mEditName.getText().toString().trim();

        if (TextUtils.isEmpty(mName)) {
            mEditName.requestFocus();
            mEditName.setError(getResources().getString(R.string.error_field_required));
            status = false;
        }
        return status;
    }

    private void addMedicineDynamically() {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addView = layoutInflater.inflate(R.layout.fragment_add_medicine, null);

            // Image Views
            ImageView mImageRemove = addView.findViewById(R.id.image_remove_medicine);

            // Edit Texts
            EditText mEditMedicineName = addView.findViewById(R.id.edit_add_medicine_name);
            EditText mEditDosage = addView.findViewById(R.id.edit_add_medicine_dosage);

            // Text Views
            TextView mTextDayWiseDosage = addView.findViewById(R.id.text_add_medicine_day_wise_dosage);

            // Click listener to remove view
            mImageRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((LinearLayout) addView.getParent()).removeView(addView);
                }
            });

            // Add dynamic view
            mLinearAddMedicine.addView(addView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
