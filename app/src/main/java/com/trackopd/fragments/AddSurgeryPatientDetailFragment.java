package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.model.ReceptionistAppointmentModel;
import com.trackopd.model.ReceptionistPatientModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddSurgeryPatientDetailFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private ImageView mImageUserProfilePic, mImageSpinnerPic;
    private TextView mTextName, mTextMobile;
    private ArrayList<ReceptionistAppointmentModel> mArrAppointment;
    private boolean IS_SURGERY = false;
    private AppUtil mAppUtils;
    private EditText mEditPatientCode, mEditAppointmentDate, mEditAppointmentTime, mEditNotes;
    private Spinner mSpinnerAppointment;
    private Button mButtonSubmit;
    private ArrayList<String> mArrAppointmentName;
    private String mPatientCode, mAppointmentNo, mAppointmentDate, mAppointmentId, mPatientId,
            mFirstName, mLastName, mMobileNo, mProfilePicture;
    private int currentPageIndex = 1, mSelectedApointmentIndex = 0;
    private LinearLayout mLinearAppointmentDateTime;
    private ReceptionistPatientModel mReceptionPatient;
    private RelativeLayout mRelativeProfile;
    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_add_surgery_patient_detail_submit:

                    if (checkAppointment()) {
                        if (IS_SURGERY) {
                            callToPatientToSurgeryList();
                        } else {
                            callToPreliminaryExaminationList();
                        }
                    }
                    break;
            }
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_add_surgery_patient_detail, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrAppointment = new ArrayList<>();
        mArrAppointmentName = new ArrayList<>();

        mAppUtils = new AppUtil(mActivity);

        getBundle();
        getIds();
        callToAppointmentAPI();
        setRegListeners();
        setData();
        setHasOptionsMenu(true);

        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * This Method receive Data from Add Payment Step One Fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {


                if (bundle.getString(AppConstants.BUNDLE_IS_FROM_SEARCH).equalsIgnoreCase(AppConstants.BUNDLE_ADD_SURGERY)) {
                    IS_SURGERY = true;
                    mReceptionPatient = (ReceptionistPatientModel) bundle.getSerializable(AppConstants.BUNDLE_ADD_PATIENT);

                    mFirstName = mReceptionPatient.getFirstName();
                    mLastName = mReceptionPatient.getLastName();
                    mMobileNo = mReceptionPatient.getMobileNo();
                    mPatientId = mReceptionPatient.getPatientID();
                    mPatientCode = mReceptionPatient.getPatientCode();
                    mProfilePicture = mReceptionPatient.getProfileImage();
                } else {
                    IS_SURGERY = false;

                    mReceptionPatient = (ReceptionistPatientModel) bundle.getSerializable(AppConstants.BUNDLE_ADD_PATIENT);

                    mFirstName = mReceptionPatient.getFirstName();
                    mLastName = mReceptionPatient.getLastName();
                    mMobileNo = mReceptionPatient.getMobileNo();
                    mPatientId = mReceptionPatient.getPatientID();
                    mPatientCode = mReceptionPatient.getPatientCode();
                    mProfilePicture = mReceptionPatient.getProfileImage();
                }
            }
            Common.insertLog("NEW PATIENT IDDDD::::> " + mPatientId);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {

            mRelativeProfile = mView.findViewById(R.id.relative_add_surgery_patient_detail_profile_pic);

            // Image View
            mImageUserProfilePic = mView.findViewById(R.id.image_add_surgery_patient_detail_profile_pic);
            mImageSpinnerPic = mView.findViewById(R.id.image_add_surgery_patient_detail_spinner_appointment);

            //Text View
            mTextName = mView.findViewById(R.id.text_input_add_surgery_patient_detail_name);
            mTextMobile = mView.findViewById(R.id.text_input_add_surgery_patient_detail_mobile_no);

            //Spinner
            mSpinnerAppointment = mView.findViewById(R.id.spinner_add_surgery_patient_detail_spinner_appointment);

            //Edit Text
            mEditPatientCode = mView.findViewById(R.id.edit_text_add_surgery_patient_detail_code);
            mEditAppointmentDate = mView.findViewById(R.id.edit_text_add_surgery_patient_detail_appointment_date);
            mEditAppointmentTime = mView.findViewById(R.id.edit_text_add_surgery_patient_detail_appointment_time);
            mEditNotes = mView.findViewById(R.id.edit_text_add_surgery_patient_detail_notes);

            //Button
            mButtonSubmit = mView.findViewById(R.id.button_add_surgery_patient_detail_submit);

            //Linear Layout
            mLinearAppointmentDateTime = mView.findViewById(R.id.linear_add_surgery_patient_detail_appointment_date_time);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            mTextName.setText(mFirstName + AppConstants.STR_EMPTY_SPACE + mLastName);
            mTextMobile.setText(mMobileNo);
            mEditPatientCode.setText(mPatientCode);
            mRelativeProfile.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.user_image_border));

            if (mProfilePicture != null && !mProfilePicture.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                //Set Thumbnail
                String thumbmail_url = WebFields.API_BASE_URL + WebFields.IMAGE_THUMBNAIL_URL + mProfilePicture;
                Glide.with(mActivity)
                        .load(thumbmail_url)
                        .apply(new RequestOptions().error(R.drawable.ic_documents_img).placeholder(R.drawable.ic_documents_img))
                        .into(mImageUserProfilePic);
            } else {
                mImageUserProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, mReceptionPatient.getFirstName(), mReceptionPatient.getLastName()));
            }
            //Set Color Image
            mImageSpinnerPic.setColorFilter(Common.setThemeColor(mActivity));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            //ToDo: Click Listener Event
            mButtonSubmit.setOnClickListener(clickListener);

            //ToDo: Spinner Select Listener
            mSpinnerAppointment.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position > 0) {
                        mLinearAppointmentDateTime.setVisibility(View.VISIBLE);
                        setDataAppointementDate();
                    } else {
                        mLinearAppointmentDateTime.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Data Appointment Select Date Time
     */
    private void setDataAppointementDate() {
        int indexAppointment = mSpinnerAppointment.getSelectedItemPosition();
        ReceptionistAppointmentModel Appointment = mArrAppointment.get(indexAppointment - 1);

        String date = Common.convertDateUsingDateFormat(mActivity, Appointment.getAppointmentDate(),
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_kk_mm_ss_hyphen),
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen));

        String time = Common.convertDateUsingDateFormat(mActivity, Appointment.getAppointmentDate(),
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_kk_mm_ss_hyphen),
                mActivity.getResources().getString(R.string.date_format_hh_mm_ss));

        mEditAppointmentDate.setText(date);
        mEditAppointmentTime.setText(time);
    }

    /**
     * This method should call the appointment listing for receptionist
     */
    private void callToAppointmentAPI() {
        try {

            String mName = mFirstName + AppConstants.STR_EMPTY_SPACE + mLastName;
            if (mPatientCode == null) {
                mPatientCode = AppConstants.STR_EMPTY_STRING;
            }

            if (mAppointmentNo == null) {
                mAppointmentNo = AppConstants.STR_EMPTY_STRING;
            }

            mAppointmentDate = mActivity.getResources().getString(R.string.date_format_first_time);

            int mDoctorId = -1;

            String mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setReceptionistAppointmentListJson(mActivity, currentPageIndex,
                            mUserType, mName, mPatientCode, mMobileNo, "", mAppointmentNo,
                            mAppointmentDate, mDoctorId, mActivity.getResources().getString
                                    (R.string.tab_all), mActivity.getResources().getString(R.string.tab_all),
                            String.valueOf(-1), hospital_database));

            Call<ReceptionistAppointmentModel> call = RetrofitClient.createService(ApiInterface.class).getReceptionistAppointmentList(requestBody);
            call.enqueue(new Callback<ReceptionistAppointmentModel>() {
                @Override
                public void onResponse(@NonNull Call<ReceptionistAppointmentModel> call, @NonNull Response<ReceptionistAppointmentModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                ArrayList<ReceptionistAppointmentModel> appointmentModel = new ArrayList<>(Arrays
                                        .asList(new GsonBuilder().serializeNulls().create()
                                                .fromJson(new JsonParser().parse(jsonObject.getString(WebFields.DATA)).
                                                        getAsJsonArray(), ReceptionistAppointmentModel[].class)));

                                if (mArrAppointment != null && mArrAppointment.size() > 0) {
                                    mArrAppointment.addAll(appointmentModel);
                                } else {
                                    mArrAppointment = appointmentModel;
                                }
                                setAppointmentSpinner();
                            } else {
                                setAppointmentSpinner();
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                            setAppointmentSpinner();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ReceptionistAppointmentModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setAppointmentSpinner();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * set Appointment Spinner
     */
    private void setAppointmentSpinner() {
        try {
            mArrAppointmentName.add(getString(R.string.spinner_select_appointment));

            if (mArrAppointment.size() > 0) {
                for (int i = 0; i < mArrAppointment.size(); i++) {
                    mArrAppointmentName.add(mArrAppointment.get(i).getTicketNumber());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerAppointment.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrAppointmentName) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedApointmentIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerAppointment.setAdapter(adapter);

            setSelectedAppointment();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Selected Appointment
     */
    private void setSelectedAppointment() {

        if (mArrAppointment.size() > 0) {
            for (int i = 0; i < mArrAppointment.size(); i++) {
                if (mArrAppointment.get(i).getAppointmentID().equalsIgnoreCase(mAppointmentNo)) {

                    mAppointmentId = mArrAppointment.get(i).getAppointmentID();
                    mSpinnerAppointment.setSelection(i + 1);
                    mLinearAppointmentDateTime.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    /**
     * Patient Surgery List
     */
    private void callToPatientToSurgeryList() {
        try {
            String mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);
            int indexAppointment = mSpinnerAppointment.getSelectedItemPosition();
            ReceptionistAppointmentModel Appointment = mArrAppointment.get(indexAppointment - 1);

            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, mReceptionPatient.getFirstName());
            args.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, mReceptionPatient.getLastName());
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mReceptionPatient.getMobileNo());
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, mReceptionPatient.getPatientCode());
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, Appointment.getTicketNumber());
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_ID, Appointment.getAppointmentID());
            args.putString(AppConstants.BUNDLE_PATIENT_PROFILE, Appointment.getProfileImage());

            Fragment fragment = new AddSurgeryFragment();
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
                fragmentManager =
                        ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
                fragmentManager =
                        ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
                fragmentManager =
                        ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
                fragmentManager =
                        ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_surgery))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_surgery))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Call To Preliminary Examination
     */
    private void callToPreliminaryExaminationList() {
        try {

            String mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);
            int indexAppointment = mSpinnerAppointment.getSelectedItemPosition();
            ReceptionistAppointmentModel Appointment = mArrAppointment.get(indexAppointment - 1);

            String CheckInDate = Appointment.getCheckINDate();
            String convertDate = Common.convertDateUsingDateFormat(mActivity, CheckInDate,
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss_hyphen),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen));

            /*String currentDate=Common.setCurrentDate(mActivity);
            String convertDate=Common.convertDateUsingDateFormat(mActivity,currentDate,
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen));*/

            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, mReceptionPatient.getFirstName());
            args.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, mReceptionPatient.getLastName());
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mReceptionPatient.getMobileNo());
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, mReceptionPatient.getPatientCode());
            args.putString(AppConstants.BUNDLE_PATIENT_ID, mReceptionPatient.getUserID());
            args.putString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE, convertDate);
            args.putString(AppConstants.BUNDLE_PATIENT_PROFILE, mReceptionPatient.getProfileImage());
            //args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO,Appointment.getTicketNumber());
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, Appointment.getAppointmentID());
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, Appointment.getDoctorID());


            Fragment fragment = new AddNewPreliminaryExaminationFragment();
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_preliminary_examination));
                fragmentManager =
                        ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_preliminary_examination));
                fragmentManager =
                        ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_preliminary_examination));
                fragmentManager =
                        ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_preliminary_examination));
                fragmentManager =
                        ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.nav_menu_preliminary_examination))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_surgery))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Check Appointment Selected
     *
     * @return false
     */
    private boolean checkAppointment() {
        boolean status = true;
        int indexAppointment = mSpinnerAppointment.getSelectedItemPosition();
        if (indexAppointment == 0) {
            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.error_select_appointment));
            status = false;
        }
        return status;
    }
}