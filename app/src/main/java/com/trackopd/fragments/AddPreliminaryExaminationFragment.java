package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.AddPreliminaryExaminationComplaintsAdapter;
import com.trackopd.adapter.AddPreliminaryExaminationFamilyOcularHistoryAdapter;
import com.trackopd.adapter.AddPreliminaryExaminationIPDAdapter;
import com.trackopd.adapter.AddPreliminaryExaminationSystemicHistoryAdapter;
import com.trackopd.adapter.PreliminaryExaminationComplaintsAdapter;
import com.trackopd.model.AddPreliminaryExaminationModel;
import com.trackopd.model.DiseaseModel;
import com.trackopd.model.DoctorModel;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.model.PreliminaryExaminationModel;
import com.trackopd.model.ReceptionistAppointmentModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.KeyboardUtility;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPreliminaryExaminationFragment extends Fragment implements AddPreliminaryExaminationComplaintsAdapter.SingleClickListener,
        AddPreliminaryExaminationFamilyOcularHistoryAdapter.SingleClickListener, AddPreliminaryExaminationSystemicHistoryAdapter.SingleClickListener,
        AddPreliminaryExaminationIPDAdapter.SingleClickListener {

    private View mView, mViewDisease, mViewAppointmentLocation, mViewLocation;
    private TextInputLayout mTextInputAppointmentLocation, mTextInputLocation;
    private Menu menu;
    private Activity mActivity;
    private TextView mTextPatientName, mTextPatientMobile, mTextViewHistory, mTextComplainsAdd,
            mTextFamilyOcularHistoryAdd, mTextSystemicHistoryAdd, mTextIPDAdd;
    private RadioGroup mRadioPreviousGlass;
    private RadioButton mRadioYes, mRadioNo;
    private EditText mEditAppointmentLocation, mEditLocation, mEditTreatmentDate, mEditLeftEyeDistSPH, mEditLeftEyeDistCYL, mEditLeftEyeDistAxis, mEditLeftEyeDistVA,
            mEditLeftEyeNearSPH, mEditLeftEyeNearCYL, mEditLeftEyeNearAxis, mEditLeftEyeNearVA, mEditLeftEyeNCT, mEditLeftEyeAT, mEditLeftEyePachymetry, mEditLeftEyeColorVision, mEditLeftEyeSyringing, mEditLeftEyeSchirmer,
            mEditLeftEyeOcularMotility, mEditLeftEyePupil, mEditLeftEyeAdnex, mEditLeftEyeAnteriorSegment, mEditLeftEyeLens, mEditLeftEyeFundus, mEditLeftEyeGonioscopy, mEditRightEyeDistSPH, mEditRightEyeDistCYL,
            mEditRightEyeDistAxis, mEditRightEyeDistVA, mEditRightEyeNearSPH, mEditRightEyeNearCYL, mEditRightEyeNearAxis, mEditRightEyeNearVA, mEditRightEyeNCT, mEditRightEyeAT, mEditRightEyePachymetry, mEditRightEyeColorVision, mEditRightEyeSyringing, mEditRightEyeSchirmer,
            mEditRightEyeOcularMotility, mEditRightEyePupil, mEditRightEyeAdnex, mEditRightEyeAnteriorSegment, mEditRightEyeLens, mEditRightEyeFundus, mEditRightEyeGonioscopy, mEditDiagnosis, mEditDilatedAR, mEditRemarks;
    private RecyclerView mRecyclerViewComplaints, mRecyclerViewFamilyOcularHistory, mRecyclerViewSystemicHistory, mRecyclerViewIPD;
    private ImageView mImageProfile, mImageAppointment, mImageDoctor, mImageOcularHistory, mImageAnatomicalLocation, mImageDisease, mImageEye, mImageEyeLens, mImageType;
    private Spinner mSpinnerAppointment, mSpinnerDoctor, mSpinnerOcularHistory, mSpinnerAnatomicalLocation, mSpinnerDisease, mSpinnerEye, mSpinnerEyeLens, mSpinnerType;
    private Button mButtonSubmit;
    private ArrayList<String> mArrAppointment;
    private ArrayList<ReceptionistAppointmentModel> mArrAppointmentList;
    private ArrayList<String> mArrDoctor;
    private ArrayList<DoctorModel> mArrDoctorList;
    private ArrayList<String> mArrOcularHistory;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.OcularHistory> mArrOcularHistoryList;
    private ArrayList<String> mArrAnatomicalLocation;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.AnatomicalLocation> mArrAnatomicalLocationList;
    private ArrayList<String> mArrDisease;
    private ArrayList<DiseaseModel> mArrDiseaseList;
    private ArrayList<String> mArrEye;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.Eye> mArrEyeList;
    private ArrayList<String> mArrEyeLens;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.EyeLence> mArrEyeLensList;
    private ArrayList<String> mArrType;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.Type> mArrTypeList;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.PrimaryComplain> mArrComplaints;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.PrimaryComplain> mArrTempComplaints;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.FamilyOcularHistory> mArrFamilyOcularHistory;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.SystemicHistory> mArrSystemicHistory;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.IPD> mArrIPD;
    private PreliminaryExaminationModel preliminaryExaminationModel;
    private LinearLayout mLinearDisease;
    private int mSelectedAppointmentIndex, mAppointmentId, mSelectedDoctorIndex, mDoctorId, mSelectedOcularHistoryIndex, mOcularHistoryId, mSelectedAnatomicalLocationIndex,
            mAnatomicalLocationId, mSelectedDiseaseIndex, mDiseaseId, mSelectedEyeIndex, mEyeId, mSelectedEyeLensIndex, mEyeLensId,
            mSelectedTypeIndex, mTypeId;
    private String mUserType, mPatientFirstName, mPatientLastName, mPatientMobile, mPatientId, mRemarks, mComplaints,
            mFamilyOcularHistory, mSystemicHistory, mIPD, mPreviousGlass, mDiagnosis, mDilatedAR, mAppointmentLocation, mLocation;
    private String mName = "", mPatientCode = "", mMobileNo = "", mMRDNo = "", mAppointmentNo = "", mAppointmentDate = "0000-00-00";

    //Adapter
    AddPreliminaryExaminationComplaintsAdapter mAdapterAddPreliminaryExaminationComplaints;
    AddPreliminaryExaminationIPDAdapter mAdapterAddPreliminaryExaminationIPD;
    AddPreliminaryExaminationFamilyOcularHistoryAdapter mAdapterAddPreliminaryExaminationFamilyOcularHistory;
    AddPreliminaryExaminationSystemicHistoryAdapter mAdapterAddPreliminaryExaminationSystemicHistory;

    //Temp Array List
    private ArrayList<PreliminaryExaminationDetailsModel.Data.IPD> mTempArrIPD;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.FamilyOcularHistory> mArrTempFamilyOcularHistory;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.SystemicHistory> mArrTempSystemicHistory;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_add_preliminary_examination, container, false);
        mActivity = getActivity();
        mArrAppointment = new ArrayList<>();
        mArrAppointmentList = new ArrayList<>();
        mArrDoctor = new ArrayList<>();
        mArrDoctorList = new ArrayList<>();
        mArrOcularHistory = new ArrayList<>();
        mArrOcularHistoryList = new ArrayList<>();
        mArrAnatomicalLocation = new ArrayList<>();
        mArrAnatomicalLocationList = new ArrayList<>();
        mArrDisease = new ArrayList<>();
        mArrDiseaseList = new ArrayList<>();
        mArrEye = new ArrayList<>();
        mArrEyeList = new ArrayList<>();
        mArrEyeLens = new ArrayList<>();
        mArrEyeLensList = new ArrayList<>();
        mArrType = new ArrayList<>();
        mArrTypeList = new ArrayList<>();
        mArrComplaints = new ArrayList<>();
        mArrTempComplaints=new ArrayList<>();
        mTempArrIPD=new ArrayList<>();
        mArrFamilyOcularHistory = new ArrayList<>();
        mArrTempFamilyOcularHistory=new ArrayList<>();
        mArrSystemicHistory = new ArrayList<>();
        mArrIPD = new ArrayList<>();
        mArrTempSystemicHistory=new ArrayList<>();


        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        getBundle();
        getIds();
        setRegListeners();
        setData();
        callAppointmentListAPI();
        callToDoctorAPI();
        callToPreliminaryExaminationAPI();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Get Bundle Key Value Previous screen
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mPatientFirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mPatientLastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mPatientMobile = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mAppointmentId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                preliminaryExaminationModel = (PreliminaryExaminationModel) bundle.getSerializable(AppConstants.BUNDLE_PATIENT_ARRAY);

                Common.insertLog("PATIENT NAME:::> " + mPatientFirstName + " " + mPatientLastName);
                Common.insertLog("PATIENT MOBILE:::> " + mPatientMobile);
                Common.insertLog("PATIENT PATIENT ID:::> " + mPatientId);
                Common.insertLog("PATIENT APPOINTMENT ID:::> " + mAppointmentId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);

        MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(false);
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Radio groups
            mRadioPreviousGlass = mView.findViewById(R.id.radio_group_add_preliminary_examination_previous_glass);

            // Radio Buttons
            mRadioYes = mView.findViewById(R.id.radio_button_add_preliminary_examination_yes);
            mRadioNo = mView.findViewById(R.id.radio_button_add_preliminary_examination_no);

            // Text Input Layouts
            mTextInputAppointmentLocation = mView.findViewById(R.id.text_input_add_preliminary_examination_appointment_location);
            mTextInputLocation = mView.findViewById(R.id.text_input_add_preliminary_examination_location);

            // Edit Texts
            mEditAppointmentLocation = mView.findViewById(R.id.edit_add_preliminary_examination_appointment_location);
            mEditLocation = mView.findViewById(R.id.edit_add_preliminary_examination_location);
            mEditTreatmentDate = mView.findViewById(R.id.edit_add_preliminary_examination_treatment_date);
            mEditLeftEyeDistSPH = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_dist_sph);
            mEditLeftEyeDistCYL = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_dist_cyl);
            mEditLeftEyeDistAxis = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_dist_axis);
            mEditLeftEyeDistVA = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_dist_va);
            mEditLeftEyeNearSPH = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_near_sph);
            mEditLeftEyeNearCYL = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_near_cyl);
            mEditLeftEyeNearAxis = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_near_axis);
            mEditLeftEyeNearVA = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_near_va);
            mEditLeftEyeNCT = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_nct);
            mEditLeftEyeAT = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_at);
            mEditLeftEyePachymetry = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_pachymetry);
            mEditLeftEyeColorVision = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_color_vision);
            mEditLeftEyeSyringing = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_syringing);
            mEditLeftEyeSchirmer = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_schirmer);
            mEditLeftEyeOcularMotility = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_ocular_motility);
            mEditLeftEyePupil = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_pupil);
            mEditLeftEyeAdnex = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_adnex);
            mEditLeftEyeAnteriorSegment = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_anterior_segment);
            mEditLeftEyeLens = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_lens);
            mEditLeftEyeFundus = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_fundus);
            mEditLeftEyeGonioscopy = mView.findViewById(R.id.edit_add_preliminary_examination_left_eye_gonioscopy);
            mEditRightEyeDistSPH = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_dist_sph);
            mEditRightEyeDistCYL = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_dist_cyl);
            mEditRightEyeDistAxis = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_dist_axis);
            mEditRightEyeDistVA = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_dist_va);
            mEditRightEyeNearSPH = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_near_sph);
            mEditRightEyeNearCYL = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_near_cyl);
            mEditRightEyeNearAxis = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_near_axis);
            mEditRightEyeNearVA = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_near_va);
            mEditRightEyeNCT = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_nct);
            mEditRightEyeAT = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_at);
            mEditRightEyePachymetry = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_pachymetry);
            mEditRightEyeColorVision = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_color_vision);
            mEditRightEyeSyringing = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_syringing);
            mEditRightEyeSchirmer = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_schirmer);
            mEditRightEyeOcularMotility = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_ocular_motility);
            mEditRightEyePupil = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_pupil);
            mEditRightEyeAdnex = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_adnex);
            mEditRightEyeAnteriorSegment = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_anterior_segment);
            mEditRightEyeLens = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_lens);
            mEditRightEyeFundus = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_fundus);
            mEditRightEyeGonioscopy = mView.findViewById(R.id.edit_add_preliminary_examination_right_eye_gonioscopy);
            mEditDiagnosis = mView.findViewById(R.id.edit_add_preliminary_examination_advice_and_treatment_diagnosis);
            mEditDilatedAR = mView.findViewById(R.id.edit_add_preliminary_examination_advice_and_treatment_dilated_ar);
            mEditRemarks = mView.findViewById(R.id.edit_add_preliminary_examination_advice_and_treatment_remarks);

            // Text Views
            mTextPatientName = mView.findViewById(R.id.text_add_preliminary_examination_patient_name);
            mTextPatientMobile = mView.findViewById(R.id.text_view_add_prelimimary_examination_patient_mobile);
            mTextViewHistory = mView.findViewById(R.id.text_add_preliminary_examination_view_history);
            mTextComplainsAdd = mView.findViewById(R.id.text_add_preliminary_examination_complaints_add);
            mTextFamilyOcularHistoryAdd = mView.findViewById(R.id.text_add_preliminary_examination_familiy_occular_history_add);
            mTextSystemicHistoryAdd = mView.findViewById(R.id.text_add_preliminary_examination_systemic_history_add);
            mTextIPDAdd = mView.findViewById(R.id.text_add_preliminary_examination_ipd_add);

            // Recycler Views
            mRecyclerViewComplaints = mView.findViewById(R.id.recycler_view_add_preliminary_examination_complaints);
            mRecyclerViewFamilyOcularHistory = mView.findViewById(R.id.recycler_view_add_preliminary_examination_family_ocular_history);
            mRecyclerViewSystemicHistory = mView.findViewById(R.id.recycler_view_add_preliminary_examination_systemic_history);
            mRecyclerViewIPD = mView.findViewById(R.id.recycler_view_add_preliminary_examination_ipd);

            // Image Views
            mImageProfile = mView.findViewById(R.id.image_add_preliminary_examination_patient_pro_pic);
            mImageAppointment = mView.findViewById(R.id.image_add_preliminary_examination_appointment);
            mImageDoctor = mView.findViewById(R.id.image_add_preliminary_examination_doctor);
            mImageAnatomicalLocation = mView.findViewById(R.id.image_add_preliminary_examination_advice_and_treatment_anatomical_location);
            mImageDisease = mView.findViewById(R.id.image_add_preliminary_examination_advice_and_treatment_disease);
            mImageEye = mView.findViewById(R.id.image_add_preliminary_examination_advice_and_treatment_eye);
            mImageEyeLens = mView.findViewById(R.id.image_add_preliminary_examination_advice_and_treatment_eye_lens);
            mImageType = mView.findViewById(R.id.image_add_preliminary_examination_advice_and_treatment_type);
            mImageOcularHistory = mView.findViewById(R.id.image_add_preliminary_examination_ocular_history);

            // Spinners
            mSpinnerAppointment = mView.findViewById(R.id.spinner_add_preliminary_examination_appointment);
            mSpinnerDoctor = mView.findViewById(R.id.spinner_add_preliminary_examination_doctor);
            mSpinnerOcularHistory = mView.findViewById(R.id.spinner_add_preliminary_examination_ocular_history);
            mSpinnerAnatomicalLocation = mView.findViewById(R.id.spinner_add_preliminary_examination_advice_and_treatment_anatomical_location);
            mSpinnerDisease = mView.findViewById(R.id.spinner_add_preliminary_examination_advice_and_treatment_disease);
            mSpinnerEye = mView.findViewById(R.id.spinner_add_preliminary_examination_advice_and_treatment_eye);
            mSpinnerEyeLens = mView.findViewById(R.id.spinner_add_preliminary_examination_advice_and_treatment_eye_lens);
            mSpinnerType = mView.findViewById(R.id.spinner_add_preliminary_examination_advice_and_treatment_type);

            // Linear Layouts
            mLinearDisease = mView.findViewById(R.id.linear_add_preliminary_examination_advice_and_treatment_disease);

            // Views
            mViewDisease = mView.findViewById(R.id.view_add_preliminary_examination_advice_and_treatment_disease);
            mViewAppointmentLocation = mView.findViewById(R.id.view_add_preliminary_examination_appointment_location);
            mViewLocation = mView.findViewById(R.id.view_add_preliminary_examination_location);

            // Buttons
            mButtonSubmit = mView.findViewById(R.id.button_add_preliminary_examination_advice_and_treatment_submit);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist)) || mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                // ToDo: Radio Button Click Listeners
                mRadioPreviousGlass.setOnCheckedChangeListener(checkedChangeListener);

                // ToDo: Click Listeners
                mButtonSubmit.setOnClickListener(clickListener);
                mEditTreatmentDate.setOnClickListener(clickListener);
                mTextViewHistory.setOnClickListener(clickListener);
                mTextComplainsAdd.setOnClickListener(clickListener);
                mTextFamilyOcularHistoryAdd.setOnClickListener(clickListener);
                mTextSystemicHistoryAdd.setOnClickListener(clickListener);
                mTextIPDAdd.setOnClickListener(clickListener);

                // ToDo: Spinners Item Select Listener
                mSpinnerAppointment.setOnItemSelectedListener(onItemSelectedListener);
                mSpinnerDoctor.setOnItemSelectedListener(onItemSelectedListener);
                mSpinnerOcularHistory.setOnItemSelectedListener(onItemSelectedListener);
                mSpinnerAnatomicalLocation.setOnItemSelectedListener(onItemSelectedListener);
                mSpinnerDisease.setOnItemSelectedListener(onItemSelectedListener);
                mSpinnerEye.setOnItemSelectedListener(onItemSelectedListener);
                mSpinnerEyeLens.setOnItemSelectedListener(onItemSelectedListener);
                mSpinnerType.setOnItemSelectedListener(onItemSelectedListener);
            } else {
                mRadioYes.setEnabled(false);
                mRadioNo.setEnabled(false);
                mSpinnerAppointment.setEnabled(false);
                mSpinnerDoctor.setEnabled(false);
                mSpinnerOcularHistory.setEnabled(false);
                mSpinnerAnatomicalLocation.setEnabled(false);
                mSpinnerDisease.setEnabled(false);
                mSpinnerEye.setEnabled(false);
                mSpinnerEyeLens.setEnabled(false);
                mSpinnerType.setEnabled(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            if (preliminaryExaminationModel != null) {
                mPatientFirstName = preliminaryExaminationModel.getPatientFirstName();
                mPatientLastName = preliminaryExaminationModel.getPatientLastName();
                mPatientId = preliminaryExaminationModel.getPatientID();
                mPatientMobile = preliminaryExaminationModel.getPatientMobileNo();
                mAppointmentId = Integer.parseInt(preliminaryExaminationModel.getAppointmentID());

                mImageProfile.setImageDrawable(Common.setLabeledImageView(mActivity, mPatientFirstName,
                        mPatientLastName));
                Common.insertLog("PATIENT DATA:::> " + mPatientId);
                mTextPatientName.setText(mPatientFirstName + AppConstants.STR_EMPTY_SPACE + mPatientLastName);
                mTextPatientMobile.setText(mPatientMobile);

                mEditTreatmentDate.setText(Common.convertDateUsingDateFormat(mActivity, preliminaryExaminationModel.getTreatmentDate(), mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen), mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy)));
                /*mEditDiagnosis.setText(preliminaryExaminationModel.getDiagnosis());
                mEditDilatedAR.setText(preliminaryExaminationModel.getDialatedAR());
                mEditRemarks.setText(preliminaryExaminationModel.getRemark());
                mEditLeftEyeDistSPH.setText(preliminaryExaminationModel.getLDSPH());
                mEditLeftEyeDistCYL.setText(preliminaryExaminationModel.getLDCyl());
                mEditLeftEyeDistAxis.setText(preliminaryExaminationModel.getLDAxis());
                mEditLeftEyeDistVA.setText(preliminaryExaminationModel.getLDVA());
                mEditLeftEyeNearSPH.setText(preliminaryExaminationModel.getLNSPH());
                mEditLeftEyeNearCYL.setText(preliminaryExaminationModel.getLNCyl());
                mEditLeftEyeNearAxis.setText(preliminaryExaminationModel.getLNAxis());
                mEditLeftEyeNearVA.setText(preliminaryExaminationModel.getLNVA());*/
                mEditLeftEyeNCT.setText(preliminaryExaminationModel.getLNCT());
                mEditLeftEyeAT.setText(preliminaryExaminationModel.getLAT());
                mEditLeftEyePachymetry.setText(preliminaryExaminationModel.getLPachymetry());
           /*     mEditLeftEyeColorVision.setText(preliminaryExaminationModel.getLColorVision());
                mEditLeftEyeSyringing.setText(preliminaryExaminationModel.getLSyringing());
                mEditLeftEyeSchirmer.setText(preliminaryExaminationModel.getLSchirmer());
                mEditLeftEyeOcularMotility.setText(preliminaryExaminationModel.getLOcularMotility());
                mEditLeftEyePupil.setText(preliminaryExaminationModel.getLPupil());
                mEditLeftEyeAdnex.setText(preliminaryExaminationModel.getLAdnex());
                mEditLeftEyeAnteriorSegment.setText(preliminaryExaminationModel.getLAnteriorSegment());
                mEditLeftEyeLens.setText(preliminaryExaminationModel.getLLens());
                mEditLeftEyeFundus.setText(preliminaryExaminationModel.getLFundus());
                mEditLeftEyeGonioscopy.setText(preliminaryExaminationModel.getLGonioscopy());
                mEditRightEyeDistSPH.setText(preliminaryExaminationModel.getRDSPH());
                mEditRightEyeDistCYL.setText(preliminaryExaminationModel.getRDCyl());
                mEditRightEyeDistAxis.setText(preliminaryExaminationModel.getRDAxis());
                mEditRightEyeDistVA.setText(preliminaryExaminationModel.getRDVA());
                mEditRightEyeNearSPH.setText(preliminaryExaminationModel.getRNSPH());
                mEditRightEyeNearCYL.setText(preliminaryExaminationModel.getRNCyl());
                mEditRightEyeNearAxis.setText(preliminaryExaminationModel.getRNAxis());
                mEditRightEyeNearVA.setText(preliminaryExaminationModel.getRNVA());
                mEditRightEyeNCT.setText(preliminaryExaminationModel.getRNCT());
                mEditRightEyeAT.setText(preliminaryExaminationModel.getRAT());
                mEditRightEyePachymetry.setText(preliminaryExaminationModel.getRPachymetry());
                mEditRightEyeColorVision.setText(preliminaryExaminationModel.getRColorVision());
                mEditRightEyeSyringing.setText(preliminaryExaminationModel.getRSyringing());
                mEditRightEyeSchirmer.setText(preliminaryExaminationModel.getRSchirmer());
                mEditRightEyeOcularMotility.setText(preliminaryExaminationModel.getROcularMotility());
                mEditRightEyePupil.setText(preliminaryExaminationModel.getRPupil());
                mEditRightEyeAdnex.setText(preliminaryExaminationModel.getRAdnex());
                mEditRightEyeAnteriorSegment.setText(preliminaryExaminationModel.getRAnteriorSegment());
                mEditRightEyeLens.setText(preliminaryExaminationModel.getRLens());
                mEditRightEyeFundus.setText(preliminaryExaminationModel.getRFundus());
                mEditRightEyeGonioscopy.setText(preliminaryExaminationModel.getRGonioscopy());

                if (preliminaryExaminationModel.getPreviousGlass().equalsIgnoreCase(mActivity.getResources().getString(R.string.text_yes))) {
                    mRadioYes.setChecked(true);
                } else {
                    mRadioNo.setChecked(true);
                }*/

                if (!mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist)) || mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {

                    // ToDo: Edit Text clickable event
                    mEditTreatmentDate.setFocusable(false);
                    mEditTreatmentDate.setClickable(false);
                    mEditDiagnosis.setFocusable(false);
                    mEditDilatedAR.setFocusable(false);
                    mEditRemarks.setFocusable(false);
                    mEditLeftEyeDistSPH.setFocusable(false);
                    mEditLeftEyeDistCYL.setFocusable(false);
                    mEditLeftEyeDistAxis.setFocusable(false);
                    mEditLeftEyeDistVA.setFocusable(false);
                    mEditLeftEyeNearSPH.setFocusable(false);
                    mEditLeftEyeNearCYL.setFocusable(false);
                    mEditLeftEyeNearAxis.setFocusable(false);
                    mEditLeftEyeNearVA.setFocusable(false);
                    mEditLeftEyeNCT.setFocusable(false);
                    mEditLeftEyeAT.setFocusable(false);
                    mEditLeftEyePachymetry.setFocusable(false);
                    mEditLeftEyeColorVision.setFocusable(false);
                    mEditLeftEyeSyringing.setFocusable(false);
                    mEditLeftEyeSchirmer.setFocusable(false);
                    mEditLeftEyeOcularMotility.setFocusable(false);
                    mEditLeftEyePupil.setFocusable(false);
                    mEditLeftEyeAdnex.setFocusable(false);
                    mEditLeftEyeAnteriorSegment.setFocusable(false);
                    mEditLeftEyeLens.setFocusable(false);
                    mEditLeftEyeFundus.setFocusable(false);
                    mEditLeftEyeGonioscopy.setFocusable(false);
                    mEditRightEyeDistSPH.setFocusable(false);
                    mEditRightEyeDistCYL.setFocusable(false);
                    mEditRightEyeDistAxis.setFocusable(false);
                    mEditRightEyeDistVA.setFocusable(false);
                    mEditRightEyeNearSPH.setFocusable(false);
                    mEditRightEyeNearCYL.setFocusable(false);
                    mEditRightEyeNearAxis.setFocusable(false);
                    mEditRightEyeNearVA.setFocusable(false);
                    mEditRightEyeNCT.setFocusable(false);
                    mEditRightEyeAT.setFocusable(false);
                    mEditRightEyePachymetry.setFocusable(false);
                    mEditRightEyeColorVision.setFocusable(false);
                    mEditRightEyeSyringing.setFocusable(false);
                    mEditRightEyeSchirmer.setFocusable(false);
                    mEditRightEyeOcularMotility.setFocusable(false);
                    mEditRightEyePupil.setFocusable(false);
                    mEditRightEyeAdnex.setFocusable(false);
                    mEditRightEyeAnteriorSegment.setFocusable(false);
                    mEditRightEyeLens.setFocusable(false);
                    mEditRightEyeFundus.setFocusable(false);
                    mEditRightEyeGonioscopy.setFocusable(false);

                    // ToDo: Radio Button clickable event
                    mRadioPreviousGlass.setEnabled(false);

                    // ToDo: Submit Button visibility
                    mButtonSubmit.setVisibility(View.GONE);
                }
            } else {
                mImageProfile.setImageDrawable(Common.setLabeledImageView(mActivity, mPatientFirstName,
                        mPatientLastName));
                mTextPatientName.setText(mPatientFirstName + AppConstants.STR_EMPTY_SPACE + mPatientLastName);
                mTextPatientMobile.setText(mPatientMobile);

                // Set current date and time
                mEditTreatmentDate.setText(Common.setCurrentDate(mActivity));
            }

            // Set Image Color as per the theme applied
            mImageAppointment.setColorFilter(Common.setThemeColor(mActivity));
            mImageDoctor.setColorFilter(Common.setThemeColor(mActivity));
            mImageOcularHistory.setColorFilter(Common.setThemeColor(mActivity));
            mImageAnatomicalLocation.setColorFilter(Common.setThemeColor(mActivity));
            mImageDisease.setColorFilter(Common.setThemeColor(mActivity));
            mImageEye.setColorFilter(Common.setThemeColor(mActivity));
            mImageEyeLens.setColorFilter(Common.setThemeColor(mActivity));
            mImageType.setColorFilter(Common.setThemeColor(mActivity));
            mImageAnatomicalLocation.setColorFilter(Common.setThemeColor(mActivity));

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerAppointment.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerDoctor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerOcularHistory.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerAnatomicalLocation.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerDisease.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerEye.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerEyeLens.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerType.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

            //set Text Theme Color
            mTextComplainsAdd.setTextColor(Common.setThemeColor(mActivity));

            if (preliminaryExaminationModel != null) {
                mComplaints = preliminaryExaminationModel.getPrimaryComplains();
                mFamilyOcularHistory = preliminaryExaminationModel.getFamilyOcularHistory();
                mIPD = preliminaryExaminationModel.getIPD();
                mSystemicHistory = preliminaryExaminationModel.getSystemicHistory();
            } else {
                mComplaints = AppConstants.STR_EMPTY_STRING;
                mFamilyOcularHistory = AppConstants.STR_EMPTY_STRING;
                mIPD = AppConstants.STR_EMPTY_STRING;
                mSystemicHistory = AppConstants.STR_EMPTY_STRING;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Checked Change listeners (Radio Button)
     */
    private RadioGroup.OnCheckedChangeListener checkedChangeListener = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            RadioButton mRadioButton = group.findViewById(checkedId);
            mRadioButton.clearFocus();

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist)) || mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                switch (mRadioButton.getId()) {
                    case R.id.radio_group_add_preliminary_examination_previous_glass:
                        int selId = group.getCheckedRadioButtonId();
                        if (selId == 1) {
                            mPreviousGlass = mActivity.getResources().getString(R.string.text_yes);
                        } else {
                            mPreviousGlass = mActivity.getResources().getString(R.string.text_no);
                        }
                        break;
                }
            } else {
                mRadioPreviousGlass.setEnabled(true);
            }
        }
    };

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.edit_add_preliminary_examination_treatment_date:
                    Common.openDatePicker(mActivity, mEditTreatmentDate);
                    break;

                case R.id.button_add_preliminary_examination_advice_and_treatment_submit:
                    redirectedToAddMedications();
//                    doAddPreliminaryExamination();
                    break;

                case R.id.text_add_preliminary_examination_view_history:
                    redirectedToOptometristPreliminaryExaminationViewHistoryFragment();
                    break;

                case R.id.text_add_preliminary_examination_complaints_add:
                    doOpenDialogBoxAddComplains();
                    break;

                case R.id.text_add_preliminary_examination_familiy_occular_history_add:
                    addFamilyOcularHistoryDialogBox("familiy_occular_history");
                    break;

                case R.id.text_add_preliminary_examination_systemic_history_add:
                    addFamilyOcularHistoryDialogBox("systemic_history");
                    break;

                case R.id.text_add_preliminary_examination_ipd_add:
                    addFamilyOcularHistoryDialogBox("ipd");
                    break;

            }
        }
    };


    /**
     * Open Dialog Box  Add Family ocular History
     */
    private void addFamilyOcularHistoryDialogBox(String type) {
        try {
            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.custom_dialog_preminary_examination_add_complains);

            RecyclerView recyclerView_family_ocular = dialog.findViewById(R.id.recycler_view);
            TextView mTextTitle = dialog.findViewById(R.id.text_row_add_preliminary_examination_add_complains_title);
            EditText editSearch=dialog.findViewById(R.id.edit_dialog_preliminary_examination_add_search);

            // Button
            Button mButtonOk = dialog.findViewById(R.id.button_add_preliminary_examination_add_complains_submit);

            recyclerView_family_ocular.setHasFixedSize(true);
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mActivity);
            recyclerView_family_ocular.setLayoutManager(mLinearLayoutManager);

            if (type.equalsIgnoreCase("familiy_occular_history")) {
                mTextTitle.setText(mActivity.getResources().getString(R.string.text_family_ocular_history));

                mAdapterAddPreliminaryExaminationFamilyOcularHistory = new AddPreliminaryExaminationFamilyOcularHistoryAdapter(mActivity, mUserType, mFamilyOcularHistory, mArrFamilyOcularHistory);
                recyclerView_family_ocular.setAdapter(mAdapterAddPreliminaryExaminationFamilyOcularHistory);
                mAdapterAddPreliminaryExaminationFamilyOcularHistory.setOnItemClickListener(this);


                mButtonOk.setText(mActivity.getResources().getString(R.string.action_add_family_ocular_history));

            } else if (type.equalsIgnoreCase("systemic_history")) {
                mTextTitle.setText(mActivity.getResources().getString(R.string.text_systemic_history));

                mAdapterAddPreliminaryExaminationSystemicHistory = new AddPreliminaryExaminationSystemicHistoryAdapter(mActivity, mUserType, mSystemicHistory, mArrSystemicHistory);
                recyclerView_family_ocular.setAdapter(mAdapterAddPreliminaryExaminationSystemicHistory);
                mAdapterAddPreliminaryExaminationSystemicHistory.setOnItemClickListener(this);

                mButtonOk.setText(mActivity.getResources().getString(R.string.action_add_systemic_history));
            } else if (type.equalsIgnoreCase("ipd")) {
                mTextTitle.setText(mActivity.getResources().getString(R.string.text_ipd));

                mAdapterAddPreliminaryExaminationIPD = new AddPreliminaryExaminationIPDAdapter(mActivity, mUserType, mIPD, mArrIPD);
                recyclerView_family_ocular.setAdapter(mAdapterAddPreliminaryExaminationIPD);
                mAdapterAddPreliminaryExaminationIPD.setOnItemClickListener(this);

                mButtonOk.setText(mActivity.getResources().getString(R.string.action_add_ipd));
            }

            recyclerView_family_ocular.setVisibility(View.VISIBLE);

            editSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (!s.toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                        setFilter(s.toString(),type);
                    }else {

                        if (type.equalsIgnoreCase("familiy_occular_history")){
                            mAdapterAddPreliminaryExaminationFamilyOcularHistory.update(mArrFamilyOcularHistory);
                        }
                        if (type.equals("ipd")){
                            mAdapterAddPreliminaryExaminationIPD.updateList(mArrIPD);
                        }
                        if(type.equalsIgnoreCase("systemic_history")){
                            mAdapterAddPreliminaryExaminationSystemicHistory.updateList(mArrSystemicHistory);
                        }

                    }
                }
            });



            // ToDo: Button Ok Click Listener
            mButtonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (type.equalsIgnoreCase("familiy_occular_history")) {
                        setFamilyOcularHistoryAdapterData();
                    } else if (type.equalsIgnoreCase("systemic_history")) {
                        setSystemicHistoryAdapterData();
                    } else if (type.equalsIgnoreCase("ipd")) {
                        setIPDAdapterData();
                    }
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This is Method Open Dialog Box from Add Complains on Click Add.
     */
    private void doOpenDialogBoxAddComplains() {
        try {
            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.custom_dialog_preminary_examination_add_complains);

            RecyclerView recyclerView_complains = dialog.findViewById(R.id.recycler_view);
            TextView mTextTitle = dialog.findViewById(R.id.text_row_add_preliminary_examination_add_complains_title);
            EditText editSearch=dialog.findViewById(R.id.edit_dialog_preliminary_examination_add_search);

            recyclerView_complains.setHasFixedSize(true);
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mActivity);
            recyclerView_complains.setLayoutManager(mLinearLayoutManager);

            mAdapterAddPreliminaryExaminationComplaints
                    = new AddPreliminaryExaminationComplaintsAdapter(mActivity, mUserType, mComplaints, mArrComplaints);
            recyclerView_complains.setAdapter(mAdapterAddPreliminaryExaminationComplaints);
            recyclerView_complains.setVisibility(View.VISIBLE);
            mAdapterAddPreliminaryExaminationComplaints.setOnItemClickListener(this);

            editSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (!s.toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                         setFilter(s.toString(),"Complaints");
                    }else {
                       mAdapterAddPreliminaryExaminationComplaints.updateList(mArrComplaints);
                    }
                }
            });


            // Button
            Button mButtonOk = dialog.findViewById(R.id.button_add_preliminary_examination_add_complains_submit);
            mButtonOk.setText(mActivity.getResources().getString(R.string.action_add_complaints));

            // ToDo: Button Ok Click Listener
            mButtonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                    setComplaintsAdapterData();
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Create Filter Array List
     * @param searchText
     * @param types
     */
    private void setFilter(String searchText, String types) {
       if (types.equalsIgnoreCase("Complaints")){

           mArrTempComplaints.clear();
           for (int i=0;i<mArrComplaints.size();i++){
              if (mArrComplaints.get(i).getPrimaryComplains().contains(searchText)){
                  mArrTempComplaints.add(mArrComplaints.get(i));
              }
           }
           mAdapterAddPreliminaryExaminationComplaints.updateList(mArrTempComplaints);
       }

       if (types.equalsIgnoreCase("ipd")){
           mTempArrIPD.clear();
           for (int i=0;i<mArrIPD.size();i++){
               if (mArrIPD.get(i).getIPD().contains(searchText)){
                   mTempArrIPD.add(mArrIPD.get(i));
               }
           }
           mAdapterAddPreliminaryExaminationIPD.updateList(mTempArrIPD);
       }

        if (types.equalsIgnoreCase("familiy_occular_history")){
            mArrTempFamilyOcularHistory.clear();
            for (int i=0;i<mArrFamilyOcularHistory.size();i++){
                if (mArrFamilyOcularHistory.get(i).getFamilyOcularHistory().contains(searchText)){
                    mArrTempFamilyOcularHistory.add(mArrFamilyOcularHistory.get(i));
                }
            }
            mAdapterAddPreliminaryExaminationFamilyOcularHistory.update(mArrTempFamilyOcularHistory);
        }

        if (types.equalsIgnoreCase("systemic_history")){
           mArrTempSystemicHistory.clear();
           for (int i=0;i<mArrSystemicHistory.size();i++){
               if (mArrSystemicHistory.get(i).getSystemicHistory().contains(searchText)){
                   mArrTempSystemicHistory.add(mArrSystemicHistory.get(i));
               }
           }
           mAdapterAddPreliminaryExaminationSystemicHistory.updateList(mArrTempSystemicHistory);
        }
    }

    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinner_add_preliminary_examination_appointment:
                    mSelectedAppointmentIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedAppointmentIndex != 0) {
                        mAppointmentId = (Integer.parseInt(mArrAppointmentList.get(mSelectedAppointmentIndex - 1).getAppointmentID()));
                        mAppointmentLocation = (mArrAppointmentList.get(mSelectedAppointmentIndex - 1).getAppointmentLocation());
                        mLocation = (mArrAppointmentList.get(mSelectedAppointmentIndex - 1).getLocation());

                        Common.insertLog("mAppointmentID::> " + mAppointmentId);
                        Common.insertLog("Appointment Location::> " + mAppointmentLocation);
                        Common.insertLog("Location::> " + mLocation);
                        setSpinnerDoctorSelected(mArrAppointmentList.get(mSelectedAppointmentIndex - 1).getDoctorID());

                        mTextInputAppointmentLocation.setVisibility(View.VISIBLE);
                        mViewAppointmentLocation.setVisibility(View.VISIBLE);
                        mEditAppointmentLocation.setText(mAppointmentLocation);

                        if (!mAppointmentLocation.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_direct))) {
                            mTextInputLocation.setVisibility(View.VISIBLE);
                            mViewLocation.setVisibility(View.VISIBLE);
                            mEditLocation.setText(mLocation);
                        } else {
                            mTextInputLocation.setVisibility(View.GONE);
                            mViewLocation.setVisibility(View.GONE);
                        }
                    }
                    break;

                case R.id.spinner_add_preliminary_examination_doctor:
                    mSelectedDoctorIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedDoctorIndex != 0) {
                        mDoctorId = (Integer.parseInt(mArrDoctorList.get(mSelectedDoctorIndex - 1).getDoctorID()));
                        String mDoctorName = (mArrDoctorList.get(mSelectedDoctorIndex - 1).getDoctorName());
                        Common.insertLog("mDoctorID::> " + mDoctorId);
                        Common.insertLog("mDoctorName::> " + mDoctorName);
                    }
                    break;

                case R.id.spinner_add_preliminary_examination_ocular_history:
                    mSelectedOcularHistoryIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedOcularHistoryIndex != 0) {
                        mOcularHistoryId = (Integer.parseInt(mArrOcularHistoryList.get(mSelectedOcularHistoryIndex - 1).getOcularHistoryID()));
                        String mOcularHistoryName = (mArrOcularHistoryList.get(mSelectedOcularHistoryIndex - 1).getOcularHistory());
                        Common.insertLog("mOcularHistoryID::> " + mOcularHistoryId);
                        Common.insertLog("mOcularHistoryName::> " + mOcularHistoryName);
                    }
                    break;

                case R.id.spinner_add_preliminary_examination_advice_and_treatment_anatomical_location:
                    mSelectedAnatomicalLocationIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedAnatomicalLocationIndex != 0) {
                        mAnatomicalLocationId = (Integer.parseInt(mArrAnatomicalLocationList.get(mSelectedAnatomicalLocationIndex - 1).getAnatomicalLocationID()));
                        String mAnatomicalLocationName = (mArrAnatomicalLocationList.get(mSelectedAnatomicalLocationIndex - 1).getAnatomicalLocation());
                        callToDiseaseAPI();
                    }
                    break;

                case R.id.spinner_add_preliminary_examination_advice_and_treatment_disease:
                    mSelectedDiseaseIndex = adapterView.getSelectedItemPosition();

                    mDiseaseId = (Integer.parseInt(mArrDiseaseList.get(mSelectedDiseaseIndex).getDiseaseID()));
                    String mDiseaseName = (mArrDiseaseList.get(mSelectedDiseaseIndex).getDisease());
                    Common.insertLog("mDiseaseID::> " + mDiseaseId);
                    Common.insertLog("mDiseaseName::> " + mDiseaseName);
                    break;

                case R.id.spinner_add_preliminary_examination_advice_and_treatment_eye:
                    mSelectedEyeIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedEyeIndex != 0) {
                        mEyeId = (Integer.parseInt(mArrEyeList.get(mSelectedEyeIndex - 1).getEyeID()));
                        String mEyeName = (mArrEyeList.get(mSelectedEyeIndex - 1).getEye());
                        Common.insertLog("mEyeID::> " + mEyeId);
                        Common.insertLog("mEyeName::> " + mEyeName);
                    }
                    break;

                case R.id.spinner_add_preliminary_examination_advice_and_treatment_eye_lens:
                    mSelectedEyeLensIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedEyeLensIndex != 0) {
                        mEyeLensId = (Integer.parseInt(mArrEyeLensList.get(mSelectedEyeLensIndex - 1).getEyeLenseID()));
                        String mEyeLensName = (mArrEyeLensList.get(mSelectedEyeLensIndex - 1).getEyeLense());
                        Common.insertLog("mEyeLensID::> " + mEyeLensId);
                        Common.insertLog("mEyeLensName::> " + mEyeLensName);
                    }
                    break;

                case R.id.spinner_add_preliminary_examination_advice_and_treatment_type:
                    mSelectedTypeIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedTypeIndex != 0) {
                        mTypeId = (Integer.parseInt(mArrTypeList.get(mSelectedTypeIndex - 1).getTypeID()));
                        String mTypeName = (mArrTypeList.get(mSelectedTypeIndex - 1).getType());
                        Common.insertLog("mTypeID::> " + mTypeId);
                        Common.insertLog("mTypeName::> " + mTypeName);
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    /**
     * This method redirected you to the Optometrist Preliminary Examination View History Fragment
     */
    private void redirectedToOptometristPreliminaryExaminationViewHistoryFragment() {
        try {

            Fragment fragment = new OptometristPreliminaryExaminationViewHistoryFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_ID, mPatientId);
            fragment.setArguments(args);
            FragmentManager fragmentManager = null;

            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_preliminary_examination));
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_preliminary_examination));
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_preliminary_examination));
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_preliminary_examination));
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_preliminary_examination_view_history))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_preliminary_examination_view_history))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * set it spinner select doctor
     *
     * @param doctorId - DoctorID;
     */
    private void setSpinnerDoctorSelected(String doctorId) {
        if (mArrDoctorList.size() > 0) {
            for (int d = 0; d < mArrDoctorList.size(); d++) {
                if (mArrDoctorList.get(d).getDoctorID().equalsIgnoreCase(doctorId)) {
                    mDoctorId = (Integer.parseInt(mArrDoctorList.get(d).getDoctorID()));
                    mSpinnerDoctor.setSelection(d + 1);
                }
            }
        }
    }

    /**
     * This method checks the validation first then call the API
     */
    private void doAddPreliminaryExamination() {
        KeyboardUtility.HideKeyboard(mActivity, mEditTreatmentDate);
        if (checkValidation()) {
            if (preliminaryExaminationModel != null) {
                String mPreliminaryExaminationId = preliminaryExaminationModel.getPreliminaryExaminationID();
               /* String mPreliminaryAdviceId = preliminaryExaminationModel.getPreliminaryAdviceID();
                String mPreliminaryLeftEyeId = preliminaryExaminationModel.getLPreliminaryEyeID();
                String mPreliminaryRightEyeId = preliminaryExaminationModel.getRPreliminaryEyeID();*/
                mPatientId = preliminaryExaminationModel.getPatientID();
                Common.insertLog("PATIENT API:::> " + mPatientId);
                /*callToAddPreliminaryExaminationAPI(AppConstants.STR_EDIT, mPreliminaryExaminationId, mPreliminaryAdviceId,
                        mPreliminaryLeftEyeId, mPreliminaryRightEyeId);*/
            } else {
                callToAddPreliminaryExaminationAPI(AppConstants.STR_ADD, AppConstants.STR_EMPTY_STRING, AppConstants.STR_EMPTY_STRING,
                        AppConstants.STR_EMPTY_STRING, AppConstants.STR_EMPTY_STRING);
            }
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {
        boolean status = true;



        if (mRadioYes.isChecked()) {
            mPreviousGlass = mActivity.getResources().getString(R.string.text_yes);
        } else {
            mPreviousGlass = mActivity.getResources().getString(R.string.text_no);
        }

        if (mSpinnerAppointment.getSelectedItemPosition() == 0) {
            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.val_select_appointment));
            status = false;
        } else if (mSpinnerDoctor.getSelectedItemPosition() == 0) {
            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.val_select_doctor));
            status = false;
        }

        return status;
    }

    /**
     * This method should call Add Preliminary Examination API
     */
    private void callToAddPreliminaryExaminationAPI(String mValue, String mPreliminaryExaminationId, String mPreliminaryAdviceId, String mPreliminaryLeftEyeId, String mPreliminaryRightEyeId) {

        try {
            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);

            String mConvertedDate = Common.convertDateToServer(mActivity, mEditTreatmentDate.getText().toString().trim());

            mDiagnosis = mEditDiagnosis.getText().toString().trim();
            mDilatedAR = mEditDilatedAR.getText().toString().trim();
            mRemarks = mEditRemarks.getText().toString().trim();

            String mLeftEyeDistSPH = mEditLeftEyeDistSPH.getText().toString().trim();
            String mLeftEyeDistCYL = mEditLeftEyeDistCYL.getText().toString().trim();
            String mLeftEyeDistAxis = mEditLeftEyeDistAxis.getText().toString().trim();
            String mLeftEyeDistVA = mEditLeftEyeDistVA.getText().toString().trim();
            String mLeftEyeNearSPH = mEditLeftEyeNearSPH.getText().toString().trim();
            String mLeftEyeNearCYL = mEditLeftEyeNearCYL.getText().toString().trim();
            String mLeftEyeNearAxis = mEditLeftEyeNearAxis.getText().toString().trim();
            String mLeftEyeNearVA = mEditLeftEyeNearVA.getText().toString().trim();
            String mLeftEyeNCT = mEditLeftEyeNCT.getText().toString().trim();
            String mLeftEyeAT = mEditLeftEyeAT.getText().toString().trim();
            String mLeftEyePachymetry = mEditLeftEyePachymetry.getText().toString().trim();
            String mLeftEyeColorVision = mEditLeftEyeColorVision.getText().toString().trim();
            String mLeftEyeSyringing = mEditLeftEyeSyringing.getText().toString().trim();
            String mLeftEyeSchirmer = mEditLeftEyeSchirmer.getText().toString().trim();
            String mLeftEyeOcularMotility = mEditLeftEyeOcularMotility.getText().toString().trim();
            String mLeftEyePupil = mEditLeftEyePupil.getText().toString().trim();
            String mLeftEyeAdnex = mEditLeftEyeAdnex.getText().toString().trim();
            String mLeftEyeAnteriorSegment = mEditLeftEyeAnteriorSegment.getText().toString().trim();
            String mLeftEyeLens = mEditLeftEyeLens.getText().toString().trim();
            String mLeftEyeFundus = mEditLeftEyeFundus.getText().toString().trim();
            String mLeftEyeGonioscopy = mEditLeftEyeGonioscopy.getText().toString().trim();
            String mRightEyeDistSPH = mEditRightEyeDistSPH.getText().toString().trim();
            String mRightEyeDistCYL = mEditRightEyeDistCYL.getText().toString().trim();
            String mRightEyeDistAxis = mEditRightEyeDistAxis.getText().toString().trim();
            String mRightEyeDistVA = mEditRightEyeDistVA.getText().toString().trim();
            String mRightEyeNearSPH = mEditRightEyeNearSPH.getText().toString().trim();
            String mRightEyeNearCYL = mEditRightEyeNearCYL.getText().toString().trim();
            String mRightEyeNearAxis = mEditRightEyeNearAxis.getText().toString().trim();
            String mRightEyeNearVA = mEditRightEyeNearVA.getText().toString().trim();
            String mRightEyeNCT = mEditRightEyeNCT.getText().toString().trim();
            String mRightEyeAT = mEditRightEyeAT.getText().toString().trim();
            String mRightEyePachymetry = mEditRightEyePachymetry.getText().toString().trim();
            String mRightEyeColorVision = mEditRightEyeColorVision.getText().toString().trim();
            String mRightEyeSyringing = mEditRightEyeSyringing.getText().toString().trim();
            String mRightEyeSchirmer = mEditRightEyeSchirmer.getText().toString().trim();
            String mRightEyeOcularMotility = mEditRightEyeOcularMotility.getText().toString().trim();
            String mRightEyePupil = mEditRightEyePupil.getText().toString().trim();
            String mRightEyeAdnex = mEditRightEyeAdnex.getText().toString().trim();
            String mRightEyeAnteriorSegment = mEditRightEyeAnteriorSegment.getText().toString().trim();
            String mRightEyeLens = mEditRightEyeLens.getText().toString().trim();
            String mRightEyeFundus = mEditRightEyeFundus.getText().toString().trim();
            String mRightEyeGonioscopy = mEditRightEyeGonioscopy.getText().toString().trim();

            Common.insertLog("PATIENT IN API:::> " + mPatientId);

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setOptometristAddPreliminaryExaminationJson(mValue, mPreliminaryExaminationId, mPreliminaryAdviceId,
                            mPreliminaryLeftEyeId, mPreliminaryRightEyeId, mAppointmentId, mPatientId, mUserId, mDoctorId, mOcularHistoryId, mComplaints,
                            mFamilyOcularHistory, mSystemicHistory, mIPD, mConvertedDate, mAnatomicalLocationId, mDiseaseId, mEyeId, mEyeLensId,
                            mRemarks, mTypeId, mPreviousGlass, mDiagnosis, mDilatedAR, mLeftEyeDistSPH, mLeftEyeDistCYL,
                            mLeftEyeDistAxis, mLeftEyeDistVA, mLeftEyeNearSPH, mLeftEyeNearCYL, mLeftEyeNearAxis, mLeftEyeNearVA,
                            mLeftEyeNCT, mLeftEyeAT, mLeftEyePachymetry, mLeftEyeColorVision, mLeftEyeSyringing, mLeftEyeSchirmer,
                            mLeftEyeOcularMotility, mLeftEyePupil, mLeftEyeAdnex, mLeftEyeAnteriorSegment, mLeftEyeLens, mLeftEyeFundus,
                            mLeftEyeGonioscopy, mRightEyeDistSPH, mRightEyeDistCYL, mRightEyeDistAxis, mRightEyeDistVA, mRightEyeNearSPH,
                            mRightEyeNearCYL, mRightEyeNearAxis, mRightEyeNearVA, mRightEyeNCT, mRightEyeAT, mRightEyePachymetry,
                            mRightEyeColorVision, mRightEyeSyringing, mRightEyeSchirmer, mRightEyeOcularMotility, mRightEyePupil, mRightEyeAdnex,
                            mRightEyeAnteriorSegment, mRightEyeLens, mRightEyeFundus, mRightEyeGonioscopy, hospital_database));

            Call<AddPreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).addPreliminaryExamination(requestBody);
            call.enqueue(new Callback<AddPreliminaryExaminationModel>() {
                @Override
                public void onResponse(@NonNull Call<AddPreliminaryExaminationModel> call, @NonNull Response<AddPreliminaryExaminationModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                Common.setCustomToast(mActivity, mMessage);
                                openPopupForAddMedication();
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddPreliminaryExaminationModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open popup for add medication
     */
    private void openPopupForAddMedication() {
        try {
            new MaterialDialog.Builder(mActivity)
                    .title(R.string.app_name)
                    .content(R.string.dialog_add_medication_message)
                    .canceledOnTouchOutside(false)
                    .positiveText(R.string.action_ok)
                    .positiveColor(getResources().getColor(R.color.app_main_theme))
                    .negativeText(R.string.action_cancel)
                    .negativeColor(getResources().getColor(R.color.colorControlNormal))

                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            redirectedToAddMedications();
                        }

                        public void onNegative(MaterialDialog dialog) {
                            dialog.dismiss();
                            //openPopupForAddPayment();
                            redirectedToOptometristPreliminaryExaminationFragment();
                        }
                    })
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open popup for add medication
     */
    private void openPopupForAddPayment() {
        try {
            new MaterialDialog.Builder(mActivity)
                    .title(R.string.app_name)
                    .content(R.string.dialog_add_payment_form_message)
                    .canceledOnTouchOutside(false)
                    .positiveText(R.string.action_ok)
                    .positiveColor(getResources().getColor(R.color.app_main_theme))
                    .negativeText(R.string.action_cancel)
                    .negativeColor(getResources().getColor(R.color.colorControlNormal))

                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            //dialog.dismiss();
                            redirectedToAddPayment();
                        }

                        public void onNegative(MaterialDialog dialog) {
                            dialog.dismiss();
                            redirectedToOptometristPreliminaryExaminationFragment();
                        }
                    })
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method redirected you to the Add Medications Fragment
     */
    private void redirectedToAddMedications() {
        try {
            String mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

            Fragment fragment = new AddMedicationFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, mPatientFirstName);
            args.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, mPatientLastName);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mPatientMobile);
            args.putString(AppConstants.BUNDLE_PATIENT_ID, mPatientId);
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, String.valueOf(mAppointmentId));
            fragment.setArguments(args);
            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.text_medication));
                fragmentManager =
                        ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.text_medication));
                fragmentManager =
                        ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.text_medication));
                fragmentManager =
                        ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.text_medication));
                fragmentManager =
                        ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_preliminary_examination))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_preliminary_examination))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method redirected you to the Add Payment Fragment
     */
    private void redirectedToAddPayment() {
        try {
            String mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

            Fragment fragment = new OptometristBillingFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, mPatientFirstName);
            args.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, mPatientLastName);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mPatientMobile);
            args.putString(AppConstants.BUNDLE_PATIENT_ID, mPatientId);
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, String.valueOf(mAppointmentId));
            fragment.setArguments(args);
            FragmentManager fragmentManager = null;

            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.text_payment));
                fragmentManager =
                        ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.text_payment));
                fragmentManager =
                        ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.text_payment));
                fragmentManager =
                        ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.text_payment));
                fragmentManager =
                        ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_billing_payment))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_billing_payment))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method redirected you to the Optometrist Preliminary Examination Fragment
     */
    private void redirectedToOptometristPreliminaryExaminationFragment() {
        try {
            Fragment fragment = new OptometristPreliminaryExaminationFragment();

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_preliminary_examination));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_preliminary_examination));
            }

            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_ADD_APPOINTMENT, mUserType);
            args.putString(AppConstants.BUNDLE_PATIENT_NAME, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE, getResources().getString(R.string.date_format_first_time));
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, -1);
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.nav_menu_preliminary_examination))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the appointment listing for receptionist
     */
    private void callAppointmentListAPI() {
        try {
            if (mName == null) {
                mName = AppConstants.STR_EMPTY_STRING;
            }

            if (mPatientCode == null) {
                mPatientCode = AppConstants.STR_EMPTY_STRING;
            }

            if (mMobileNo == null) {
                mMobileNo = AppConstants.STR_EMPTY_STRING;
            }

            if(mMRDNo == null) {
                mMRDNo = AppConstants.STR_EMPTY_STRING;
            }

            if (mAppointmentNo == null) {
                mAppointmentNo = AppConstants.STR_EMPTY_STRING;
            }

            if (mAppointmentDate == null) {
                mAppointmentDate = mActivity.getResources().getString(R.string.date_format_first_time);
            }

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            Common.insertLog("PATIENT APPO API BE4:::> " + mPatientId);
            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setReceptionistAppointmentListJson(mActivity, 1, mUserType, mName, mPatientCode,
                            mMobileNo, mMRDNo, mAppointmentNo, mAppointmentDate, -1, mActivity.getResources().getString(R.string.tab_all),
                            mActivity.getResources().getString(R.string.tab_all), mPatientId, hospital_database));
            Common.insertLog("PATIENT APPO API AFTER:::> " + mPatientId);

            Call<ReceptionistAppointmentModel> call = RetrofitClient.createService(ApiInterface.class).getReceptionistAppointmentList(requestBody);
            call.enqueue(new Callback<ReceptionistAppointmentModel>() {
                @Override
                public void onResponse(@NonNull Call<ReceptionistAppointmentModel> call, @NonNull Response<ReceptionistAppointmentModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null) {

                                // Complaints Array Binding
                                mArrAppointmentList.addAll(response.body().getData());
                                setAppointmentAdapterData();
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ReceptionistAppointmentModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setAppointmentAdapterData();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setAppointmentAdapterData() {
        try {
            mArrAppointment.add(0, mActivity.getString(R.string.spinner_select_appointment_no));
            if (mArrAppointmentList.size() > 0) {
                for (ReceptionistAppointmentModel receptionistAppointmentModel : mArrAppointmentList) {
                    mArrAppointment.add(receptionistAppointmentModel.getTicketNumber());
                }
            }
            mSpinnerAppointment.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrAppointment) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedAppointmentIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerAppointment.setAdapter(adapter);

            if (preliminaryExaminationModel != null) {
                for (int i = 0; i < mArrAppointmentList.size(); i++) {
                    if (mArrAppointmentList.get(i).getAppointmentID().equalsIgnoreCase(preliminaryExaminationModel.getAppointmentID())) {
                        String mAppointment = mArrAppointmentList.get(i).getTicketNumber();
                        mSpinnerAppointment.setSelection(adapter.getPosition(mAppointment));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Doctor API
     */
    private void callToDoctorAPI() {
        try {

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setDoctorListJson(hospital_database));

            Call<DoctorModel> call = RetrofitClient.createService(ApiInterface.class).getDoctorList(body);
            call.enqueue(new Callback<DoctorModel>() {
                @Override
                public void onResponse(@NonNull Call<DoctorModel> call, @NonNull Response<DoctorModel> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrDoctorList.addAll(response.body().getData());
                            }
                            setAdapterData();
                        } else {
                            //Common.setCustomToast(mActivity, mMessage);
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrDoctorList.addAll(response.body().getData());
                            }
                            setAdapterData();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DoctorModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setAdapterData();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mArrDoctor.add(0, mActivity.getString(R.string.spinner_select_doctor));
            if (mArrDoctorList.size() > 0) {
                for (DoctorModel doctorModel : mArrDoctorList) {
                    mArrDoctor.add(doctorModel.getDoctorName());
                }
            }
            mSpinnerDoctor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrDoctor) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedDoctorIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerDoctor.setAdapter(adapter);

            if (preliminaryExaminationModel != null) {
                for (int i = 0; i < mArrDoctorList.size(); i++) {
                    if (mArrDoctorList.get(i).getDoctorID().equalsIgnoreCase(preliminaryExaminationModel.getDoctorID())) {
                        String mDoctor = mArrDoctorList.get(i).getDoctorName();
                        mSpinnerDoctor.setSelection(adapter.getPosition(mDoctor));
                    }
                }
            }
//            mSpinnerDoctor.setSelection(adapter.getPosition(preliminaryExaminationModel.getDoctorFirstName() +AppConstants.STR_EMPTY_SPACE + preliminaryExaminationModel.getDoctorLastName()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Preliminary Examination API
     */
    private void callToPreliminaryExaminationAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPreliminaryExaminationDetailsJson(hospital_database));

            Call<PreliminaryExaminationDetailsModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationDetails(body);
            call.enqueue(new Callback<PreliminaryExaminationDetailsModel>() {
                @Override
                public void onResponse(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Response<PreliminaryExaminationDetailsModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null) {

                                // Complaints Array Binding
                                mArrComplaints.addAll(response.body().getData().getPrimaryComplains());
                                setComplaintsAdapterData();

                                // Family Ocular History Array Binding
                                mArrFamilyOcularHistory.addAll(response.body().getData().getFamilyOcularHistory());
                                setFamilyOcularHistoryAdapterData();

                                // Systemic History Array Binding
                                mArrSystemicHistory.addAll(response.body().getData().getSystemicHistory());
                                setSystemicHistoryAdapterData();

                                // IPD Array Binding
                                mArrIPD.addAll(response.body().getData().getIPD());
                                setIPDAdapterData();

                                // Ocular History Array Binding
                                mArrOcularHistoryList.addAll(response.body().getData().getOcularHistory());
                                setOcularHistoryAdapterData();

                                // Anatomical Location Array Binding
                                mArrAnatomicalLocationList.addAll(response.body().getData().getAnatomicalLocation());
                                setAnatomicalLocationAdapterData();

                                // Eye Array Binding
                                mArrEyeList.addAll(response.body().getData().getEye());
                                setEyeAdapterData();

                                // Eye Lens Array Binding
                                mArrEyeLensList.addAll(response.body().getData().getEyeLence());
                                setEyeLensAdapterData();

                                // Type Binding
                                mArrTypeList.addAll(response.body().getData().getType());
                                setTypeAdapterData();
                            }
                        } else {
//                            setOcularHistoryAdapterData();
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setOcularHistoryAdapterData() {
        try {
            mArrOcularHistory.add(0, mActivity.getResources().getString(R.string.spinner_select_ocular_history));
            if (mArrOcularHistoryList.size() > 0) {
                for (PreliminaryExaminationDetailsModel.Data.OcularHistory ocularHistoryModel : mArrOcularHistoryList) {
                    mArrOcularHistory.add(ocularHistoryModel.getOcularHistory());
                }
            }
            mSpinnerOcularHistory.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrOcularHistory) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedOcularHistoryIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerOcularHistory.setAdapter(adapter);

            if (preliminaryExaminationModel != null) {
                for (int i = 0; i < mArrOcularHistoryList.size(); i++) {
                    /*if (mArrOcularHistoryList.get(i).getOcularHistoryID().equalsIgnoreCase(preliminaryExaminationModel.getOcularHistoryID())) {
                        String mOcularHistory = mArrOcularHistoryList.get(i).getOcularHistory();
                        mSpinnerOcularHistory.setSelection(adapter.getPosition(mOcularHistory));
                    }*/
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setAnatomicalLocationAdapterData() {
        try {
            mArrAnatomicalLocation.add(0, mActivity.getResources().getString(R.string.spinner_select_anatomical_location));
            if (mArrAnatomicalLocationList.size() > 0) {
                for (PreliminaryExaminationDetailsModel.Data.AnatomicalLocation anatomicalLocationModel : mArrAnatomicalLocationList) {
                    mArrAnatomicalLocation.add(anatomicalLocationModel.getAnatomicalLocation());
                }
            }
            mSpinnerAnatomicalLocation.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrAnatomicalLocation) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedAnatomicalLocationIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerAnatomicalLocation.setAdapter(adapter);

            if (preliminaryExaminationModel != null) {
                for (int i = 0; i < mArrAnatomicalLocationList.size(); i++) {
                    /*if (mArrAnatomicalLocationList.get(i).getAnatomicalLocationID().equalsIgnoreCase(preliminaryExaminationModel.getAnatomicalLocationID())) {
                        String mAnatomicalLocation = mArrAnatomicalLocationList.get(i).getAnatomicalLocation();
                        mSpinnerAnatomicalLocation.setSelection(adapter.getPosition(mAnatomicalLocation));
                    }*/
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setEyeAdapterData() {
        try {
            mArrEye.add(0, mActivity.getResources().getString(R.string.spinner_select_eye));
            if (mArrEyeList.size() > 0) {
                for (PreliminaryExaminationDetailsModel.Data.Eye eyeModel : mArrEyeList) {
                    mArrEye.add(eyeModel.getEye());
                }
            }
            mSpinnerEye.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrEye) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedEyeIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerEye.setAdapter(adapter);

            if (preliminaryExaminationModel != null) {
                for (int i = 0; i < mArrEyeList.size(); i++) {
                   /* if (mArrEyeList.get(i).getEyeID().equalsIgnoreCase(preliminaryExaminationModel.getEyeID())) {
                        String mEye = mArrEyeList.get(i).getEye();
                        mSpinnerEye.setSelection(adapter.getPosition(mEye));
                    }*/
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setEyeLensAdapterData() {
        try {
            mArrEyeLens.add(0, mActivity.getResources().getString(R.string.spinner_select_eye_lens));
            if (mArrEyeLensList.size() > 0) {
                for (PreliminaryExaminationDetailsModel.Data.EyeLence eyeLensModel : mArrEyeLensList) {
                    mArrEyeLens.add(eyeLensModel.getEyeLense());
                }
            }
            mSpinnerEyeLens.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrEyeLens) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedEyeLensIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerEyeLens.setAdapter(adapter);

            if (preliminaryExaminationModel != null) {
                for (int i = 0; i < mArrEyeLensList.size(); i++) {
                    /*if (mArrEyeLensList.get(i).getEyeLenseID().equalsIgnoreCase(preliminaryExaminationModel.getEyeLenseID())) {
                        String mEyeLens = mArrEyeLensList.get(i).getEyeLense();
                        mSpinnerEyeLens.setSelection(adapter.getPosition(mEyeLens));
                    }*/
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setTypeAdapterData() {
        try {
            mArrType.add(0, mActivity.getResources().getString(R.string.spinner_select_type));
            if (mArrTypeList.size() > 0) {
                for (PreliminaryExaminationDetailsModel.Data.Type typeModel : mArrTypeList) {
                    mArrType.add(typeModel.getType());
                }
            }
            mSpinnerType.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrType) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedTypeIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerType.setAdapter(adapter);

            if (preliminaryExaminationModel != null) {
                for (int i = 0; i < mArrTypeList.size(); i++) {
                   /* if (mArrTypeList.get(i).getTypeID().equalsIgnoreCase(preliminaryExaminationModel.getTypeID())) {
                        String mType = mArrTypeList.get(i).getType();
                        mSpinnerType.setSelection(adapter.getPosition(mType));
                    }*/
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Disease API
     */
    private void callToDiseaseAPI() {
        try {

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setDiseaseListJson(mAnatomicalLocationId, hospital_database));

            Call<DiseaseModel> call = RetrofitClient.createService(ApiInterface.class).getDiseaseList(body);
            call.enqueue(new Callback<DiseaseModel>() {
                @Override
                public void onResponse(@NonNull Call<DiseaseModel> call, @NonNull Response<DiseaseModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        int mError = response.body().getError();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null && response.body().getData() != null) {
                                mLinearDisease.setVisibility(View.VISIBLE);
                                mViewDisease.setVisibility(View.VISIBLE);
                                mArrDiseaseList.addAll(response.body().getData());
                                setDiseaseAdapterData();
                            }
                        } else {
                            mLinearDisease.setVisibility(View.GONE);
                            mViewDisease.setVisibility(View.GONE);
                            mDiseaseId = -1;
//                            setDiseaseAdapterData();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DiseaseModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setDiseaseAdapterData();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setDiseaseAdapterData() {
        try {
            if (mArrDiseaseList.size() > 0) {
                for (DiseaseModel diseaseModel : mArrDiseaseList) {
                    mArrDisease.add(diseaseModel.getDisease());
                }
            }

            mSpinnerDisease.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrDisease) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedDiseaseIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerDisease.setAdapter(adapter);

            if (preliminaryExaminationModel != null) {
                for (int i = 0; i < mArrDiseaseList.size(); i++) {
                    /*if (mArrDiseaseList.get(i).getDiseaseID().equalsIgnoreCase(preliminaryExaminationModel.getDiseaseID())) {
                        String mDisease = mArrDiseaseList.get(i).getDisease();
                        mSpinnerDisease.setSelection(adapter.getPosition(mDisease));
                    }*/
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setComplaintsAdapterData() {
        try {
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerViewComplaints.setLayoutManager(mLinearLayoutManager);

            ArrayList<String> al = new ArrayList<String>();

            if (!mComplaints.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                String array[] = mComplaints.split(",");
                for (String data : array) {
                    al.add(data);
                }
            }
            PreliminaryExaminationComplaintsAdapter mAdapterPreliminaryExaminationComplaints =
                    new PreliminaryExaminationComplaintsAdapter(mActivity, mUserType, al) {
                        @Override
                        protected void RemoveItem(String title) {
                            super.RemoveItem(title);

                            ArrayList<String> item = new ArrayList<>();
                            String array[] = mComplaints.split(",");
                            for (String data : array) {
                                if (!data.equalsIgnoreCase(title)) {
                                    item.add(data);
                                }
                            }
                            mComplaints = item.toString().replaceAll("\\[", "").replaceAll("\\]", "").replace(", ", ",");
                            notifyDataSetChanged();
                            Common.insertLog("IPD:::> " + mComplaints);
                        }
                    };
            mRecyclerViewComplaints.setAdapter(mAdapterPreliminaryExaminationComplaints);
            if (al.size() > 0) {
                mRecyclerViewComplaints.setVisibility(View.VISIBLE);
            } else {
                mRecyclerViewComplaints.setVisibility(View.GONE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setFamilyOcularHistoryAdapterData() {
        try {
            mRecyclerViewFamilyOcularHistory.setHasFixedSize(true);
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerViewFamilyOcularHistory.setLayoutManager(mLinearLayoutManager);

            ArrayList<String> al = new ArrayList<String>();

            if (!mFamilyOcularHistory.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                String array[] = mFamilyOcularHistory.split(",");
                for (String data : array) {
                    al.add(data);
                }
            }
            PreliminaryExaminationComplaintsAdapter mAdapterPreliminaryExaminationComplaints =
                    new PreliminaryExaminationComplaintsAdapter(mActivity, mUserType, al) {
                        @Override
                        protected void RemoveItem(String title) {
                            super.RemoveItem(title);
                            ArrayList<String> item = new ArrayList<>();
                            String array[] = mFamilyOcularHistory.split(",");
                            for (String data : array) {
                                if (!data.equalsIgnoreCase(title)) {
                                    item.add(data);
                                }
                            }
                            mFamilyOcularHistory = item.toString().replaceAll("\\[", "").replaceAll("\\]", "").replace(", ", ",");
                            notifyDataSetChanged();
                            Common.insertLog("IPD:::> " + mFamilyOcularHistory);
                        }
                    };
            mRecyclerViewFamilyOcularHistory.setAdapter(mAdapterPreliminaryExaminationComplaints);
            if (al.size() > 0) {
                mRecyclerViewFamilyOcularHistory.setVisibility(View.VISIBLE);
            } else {
                mRecyclerViewFamilyOcularHistory.setVisibility(View.GONE);
            }

//            mRecyclerViewFamilyOcularHistory.setNestedScrollingEnabled(false);

//            if (preliminaryExaminationModel != null) {
//                mFamilyOcularHistory = preliminaryExaminationModel.getFamilyOcularHistory();
//            } else {
//                mFamilyOcularHistory = AppConstants.STR_EMPTY_STRING;
//            }
//            AddPreliminaryExaminationFamilyOcularHistoryAdapter mAdapterAddPreliminaryExaminationFamilyOcularHistory = new AddPreliminaryExaminationFamilyOcularHistoryAdapter(mActivity, mUserType, mFamilyOcularHistory, mArrFamilyOcularHistory);
//            mRecyclerViewFamilyOcularHistory.setAdapter(mAdapterAddPreliminaryExaminationFamilyOcularHistory);
//            mAdapterAddPreliminaryExaminationFamilyOcularHistory.setOnItemClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setSystemicHistoryAdapterData() {
        try {
            mRecyclerViewSystemicHistory.setHasFixedSize(true);
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerViewSystemicHistory.setLayoutManager(mLinearLayoutManager);

            ArrayList<String> al = new ArrayList<String>();

            if (!mSystemicHistory.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                String array[] = mSystemicHistory.split(",");
                for (String data : array) {
                    al.add(data);
                }
            }
            PreliminaryExaminationComplaintsAdapter mAdapterPreliminaryExaminationComplaints =
                    new PreliminaryExaminationComplaintsAdapter(mActivity, mUserType, al) {
                        @Override
                        protected void RemoveItem(String title) {
                            super.RemoveItem(title);

                            ArrayList<String> item = new ArrayList<>();
                            String array[] = mSystemicHistory.split(",");
                            for (String data : array) {
                                if (!data.equalsIgnoreCase(title)) {
                                    item.add(data);
                                }
                            }
                            mSystemicHistory = item.toString().replaceAll("\\[", "").replaceAll("\\]", "").replace(", ", ",");
                            notifyDataSetChanged();
                            Common.insertLog("System History :::> " + mSystemicHistory);
                        }
                    };
            mRecyclerViewSystemicHistory.setAdapter(mAdapterPreliminaryExaminationComplaints);
            if (al.size() > 0) {
                mRecyclerViewSystemicHistory.setVisibility(View.VISIBLE);
            } else {
                mRecyclerViewSystemicHistory.setVisibility(View.GONE);
            }

            /*if (preliminaryExaminationModel != null) {
                mSystemicHistory = preliminaryExaminationModel.getSystemicHistory();
            } else {
                mSystemicHistory = AppConstants.STR_EMPTY_STRING;
            }
            AddPreliminaryExaminationSystemicHistoryAdapter mAdapterAddPreliminaryExaminationSystemicHistory = new AddPreliminaryExaminationSystemicHistoryAdapter(mActivity, mUserType, mSystemicHistory, mArrSystemicHistory);
            mRecyclerViewSystemicHistory.setAdapter(mAdapterAddPreliminaryExaminationSystemicHistory);
            mAdapterAddPreliminaryExaminationSystemicHistory.setOnItemClickListener(this);*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setIPDAdapterData() {
        try {
            mRecyclerViewIPD.setHasFixedSize(true);
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerViewIPD.setLayoutManager(mLinearLayoutManager);
//            mRecyclerViewSystemicHistory.setNestedScrollingEnabled(false);


            ArrayList<String> al = new ArrayList<String>();

            if (!mIPD.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                String array[] = mIPD.split(",");
                for (String data : array) {
                    al.add(data);
                }
            }

            PreliminaryExaminationComplaintsAdapter mAdapterPreliminaryExaminationComplaints =
                    new PreliminaryExaminationComplaintsAdapter(mActivity, mUserType, al) {
                        @Override
                        protected void RemoveItem(String title) {
                            super.RemoveItem(title);

                            ArrayList<String> item = new ArrayList<>();
                            String array[] = mIPD.split(",");
                            for (String data : array) {
                                if (!data.equalsIgnoreCase(title)) {
                                    item.add(data);
                                }
                            }
                            mIPD = item.toString().replaceAll("\\[", "").replaceAll("\\]", "").replace(", ", ",");
                            notifyDataSetChanged();
                            Common.insertLog("System History :::> " + mIPD);
                        }
                    };
            mRecyclerViewIPD.setAdapter(mAdapterPreliminaryExaminationComplaints);
            if (al.size() > 0) {
                mRecyclerViewIPD.setVisibility(View.VISIBLE);
            } else {
                mRecyclerViewIPD.setVisibility(View.GONE);
            }

            /*if (preliminaryExaminationModel != null) {
                mIPD = preliminaryExaminationModel.getIPD();
            } else {
                mIPD = AppConstants.STR_EMPTY_STRING;
            }
            AddPreliminaryExaminationIPDAdapter mAdapterAddPreliminaryExaminationIPD = new AddPreliminaryExaminationIPDAdapter(mActivity, mUserType, mIPD, mArrIPD);
            mRecyclerViewIPD.setAdapter(mAdapterAddPreliminaryExaminationIPD);
            mAdapterAddPreliminaryExaminationIPD.setOnItemClickListener(this);*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onComplaintsCheckBoxClickListener(String mFinalComplaints) {
        mComplaints = mFinalComplaints;
    }

    @Override
    public void onFamilyOcularHistoryCheckBoxClickListener(String mFinalFamilyOcularHistory) {
        mFamilyOcularHistory = mFinalFamilyOcularHistory;
    }

    @Override
    public void onSystemicHistoryCheckBoxClickListener(String mFinalSystemicHistory) {
        mSystemicHistory = mFinalSystemicHistory;
    }

    @Override
    public void onIPDCheckBoxClickListener(String mFinalIPD) {
        mIPD = mFinalIPD;
    }
}
