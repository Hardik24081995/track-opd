package com.trackopd.fragments;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import java.util.ArrayList;

public class AddPaymentStepOneFragment extends Fragment {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private ImageView mImageSearchType;
    private Spinner mSpinnerField;
    private EditText mEditSearchText;
    private ArrayList<String> mArrField;
    private int mSelectedFieldIndex;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_add_payment_step_one, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrField = new ArrayList<>();

        getIds();
        setData();
        setRegListeners();
        setFieldSpinner();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Set the prepare menu options
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);

        MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(false);
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Image Views
            mImageSearchType = mView.findViewById(R.id.image_add_payment_step_one_search_type);

            // Spinners
            mSpinnerField = mView.findViewById(R.id.spinner_add_payment_step_one_search_type);

            // Edit Texts
            mEditSearchText = mView.findViewById(R.id.edit_add_payment_step_one_search);

            // Set Request Focus
            mEditSearchText.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Doctor - Spinner Item Select Listener
            mSpinnerField.setOnItemSelectedListener(onItemSelectedListener);

            // ToDo: Search - Edit Text Change Listener
            mEditSearchText.addTextChangedListener(new EditTextListener());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        // Set Image Color as per the theme applied
        mImageSearchType.setColorFilter(Common.setThemeColor(mActivity));

        // ToDo: Sets the spinner color as per the theme applied
        mSpinnerField.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
    }

    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinner_add_payment_step_one_search_type:
                    mSelectedFieldIndex = adapterView.getSelectedItemPosition();
                    if (mSelectedFieldIndex == 1) {
                        mEditSearchText.setInputType(InputType.TYPE_CLASS_TEXT);
                        AddPaymentFragment.mFieldName = mArrField.get(mSelectedFieldIndex);
                    } else if (mSelectedFieldIndex == 2) {
                        mEditSearchText.setInputType(InputType.TYPE_CLASS_TEXT);
                        AddPaymentFragment.mFieldName = mArrField.get(mSelectedFieldIndex);
                    } else if (mSelectedFieldIndex == 3) {
                        mEditSearchText.setInputType(InputType.TYPE_CLASS_NUMBER);
                        AddPaymentFragment.mFieldName = mArrField.get(mSelectedFieldIndex);
                    }else {
                        AddPaymentFragment.mFieldName = AppConstants.STR_EMPTY_STRING;
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    /**
     * Edit text on input text listener
     */
    private class EditTextListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            AddPaymentFragment.mFieldValue = s.toString();
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    }

    /**
     * Bind the data to the field spinner
     */
    private void setFieldSpinner() {
        try {
            mArrField.add(mActivity.getResources().getString(R.string.spinner_select_field));
            mArrField.add(mActivity.getResources().getString(R.string.edit_patient_name));
            mArrField.add(mActivity.getResources().getString(R.string.edit_patient_code));
            mArrField.add(mActivity.getResources().getString(R.string.edit_patient_mobile));

            //ToDo: Sets the spinner color as per the theme applied
            mSpinnerField.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrField) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedFieldIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerField.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
