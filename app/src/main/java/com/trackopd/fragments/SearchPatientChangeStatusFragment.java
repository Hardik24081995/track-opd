package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

public class SearchPatientChangeStatusFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private EditText mEditPatientName, mEditPatientCode, mEditPatientMobile;
    private Button mButtonSearch;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_search_patient_change_status, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        getIds();
        setRegListeners();
        setHasOptionsMenu(false);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Edit Texts
            mEditPatientName = mView.findViewById(R.id.edit_search_patient_change_status_name);
            mEditPatientCode = mView.findViewById(R.id.edit_search_patient_change_status_code);
            mEditPatientMobile = mView.findViewById(R.id.edit_search_patient_change_status_mobile);

            // Buttons
            mButtonSearch = mView.findViewById(R.id.button_search_patient_change_status_search);

            // Set Request Focus
            mEditPatientName.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Click Listeners
            mButtonSearch.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_search_patient_change_status_search:
                    if (checkValidation()) {
                        callToReceptionistPatientFragment();
                    }
                    break;
            }
        }
    };

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {
        boolean status = true;

        String name = mEditPatientName.getText().toString().trim();
        String code = mEditPatientCode.getText().toString().trim();
        String mobile = mEditPatientMobile.getText().toString().trim();

        if (name.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                && code.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                && mobile.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            Common.setCustomToast(mActivity, getString(R.string.error_search_change_status));
            status = false;
        }
        return status;
    }

    /**
     * Sets up the Receptionist Patient fragment
     */
    private void callToReceptionistPatientFragment() {
        try {
            String mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);
            String mName = mEditPatientName.getText().toString().trim();
            String mPatientCode = mEditPatientCode.getText().toString().trim();
            String mMobile = mEditPatientMobile.getText().toString().trim();

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(getResources().getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_change_status));
            } else if (mUserType.equalsIgnoreCase(getResources().getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_change_status));
            } else if (mUserType.equalsIgnoreCase(getResources().getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_change_status));
            } else if (mUserType.equalsIgnoreCase(getResources().getString(R.string.user_type_patient))) {
                fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
                ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_change_status));
            } else if (mUserType.equalsIgnoreCase(getResources().getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_change_status));
            }

            Fragment fragment = new ChangeStatusFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_NAME, mName);
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, mPatientCode);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mMobile);
            fragment.setArguments(args);

            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    mActivity.getResources().getString(R.string.nav_menu_change_status)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
