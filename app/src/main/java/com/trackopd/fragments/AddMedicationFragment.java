package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.SpecialInstructionsAdapter;
import com.trackopd.model.DosageModel;
import com.trackopd.model.MedicationModel;
import com.trackopd.model.PreliminaryExaminationModel;
import com.trackopd.model.SpecialInstructionsModel;
import com.trackopd.model.UOMModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMedicationFragment extends Fragment implements SpecialInstructionsAdapter.SingleClickListener {

    private View mView, mViewEndDate, mViewDosage, mViewHowManyTimesADay, mViewHowMany, mViewUOM, mViewDuration, mViewMedicationType;
    private Menu menu;
    private Activity mActivity;
    private TextView mTextPatientName, mTextMobileNumber;
    private ImageView mImageAdd, mImageProfile;
    private RadioGroup mRadioGroupMedicationType;
    private RadioButton mRadioNone, mRadioDays, mRadioWeek;
    private LinearLayout mLinearContainer, mLinearDosage, mLinearMedicationType, mLinearUOM;
    private TextInputLayout mTextInputEndDate, mTextInputHowManyTimesADay, mTextInputHowMany, mTextInputDuration;
    private Spinner mSpinnerMedication, mSpinnerDosage, mSpinnerUOM;
    private Button mButtonSubmit;
    private RecyclerView mRecyclerView;
    private ArrayList<String> mArrMedication;
    private ArrayList<MedicationModel> mArrMedicationList;
    private ArrayList<String> mArrDosage;
    private ArrayList<DosageModel> mArrDosageList;
    private ArrayList<String> mArrUOM;
    private ArrayList<UOMModel> mArrUOMList;
    private ArrayList<SpecialInstructionsModel> mArrSpecialInstructions;
    private int mSelectedMedicationIndex, mMedicationId, mSelectedDosageIndex, mDosageId, mSelectedUOMIndex, mUOMId, mType;
    private String mPatientFirstName, mPatientLastName, mPatientMobile, mAppointmentId, mUserType, mSpecialInstructions = "",
            mMedicationType, mSelMedicationType, EndDate;
    private PreliminaryExaminationModel preliminaryExaminationModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_add_medication, container, false);
        mActivity = getActivity();
        mArrMedication = new ArrayList<>();
        mArrMedicationList = new ArrayList<>();
        mArrDosage = new ArrayList<>();
        mArrDosageList = new ArrayList<>();
        mArrUOM = new ArrayList<>();
        mArrUOMList = new ArrayList<>();
        mArrSpecialInstructions = new ArrayList<>();
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        getBundle();
        getIds();
        setRegListeners();
        setData();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);

        MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(false);
    }

    /**
     * Get Bundle Key Value Previous screen
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mPatientFirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mPatientLastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mPatientMobile = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mAppointmentId = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                preliminaryExaminationModel = (PreliminaryExaminationModel) bundle.getSerializable(AppConstants.BUNDLE_PATIENT_ARRAY);

                Common.insertLog("Add Medication Appointment Id::>>> " + mAppointmentId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Text Views
            mTextPatientName = mView.findViewById(R.id.text_add_medication_patient_name);
            mTextMobileNumber = mView.findViewById(R.id.text_add_medication_patient_mobile);

            // Image Views
            mImageAdd = mView.findViewById(R.id.image_add_medication_view_add);
            mImageProfile = mView.findViewById(R.id.image_add_medication_profile_pic);

            // Buttons
            mButtonSubmit = mView.findViewById(R.id.button_add_medication_submit);

            // Linear Layouts
            mLinearContainer = mView.findViewById(R.id.linear_add_medication_container);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Click Listeners
            mButtonSubmit.setOnClickListener(clickListener);
            mImageAdd.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Value on View
     */
    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            mImageProfile.setImageDrawable(Common.setLabeledImageView(mActivity, mPatientFirstName,
                    mPatientLastName));
            mTextPatientName.setText(mPatientFirstName + AppConstants.STR_EMPTY_SPACE + mPatientLastName);
            mTextMobileNumber.setText(mPatientMobile);

            mMedicationType = mActivity.getResources().getString(R.string.text_none);

            if (preliminaryExaminationModel != null) {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.image_add_medication_view_add:
                    mArrMedication.clear();
                    mArrMedicationList.clear();
                    mArrDosage.clear();
                    mArrDosageList.clear();
                    mArrUOM.clear();
                    mArrUOMList.clear();
                    mArrSpecialInstructions.clear();
                    doAddMedication();
                    break;

                case R.id.button_add_medication_submit:
                    if (checkAddValidation()) {
                        callToAddMedication();
                    }
                    break;
            }
        }
    };


    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {
                case R.id.spinner_row_add_medication:
                    mSelectedMedicationIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedMedicationIndex != 0) {
                        mMedicationId = (Integer.parseInt(mArrMedicationList.get(mSelectedMedicationIndex - 1).getMedicationID()));
                        String mMedicationName = (mArrMedicationList.get(mSelectedMedicationIndex - 1).getMedicationName());
                        mSelMedicationType = (mArrMedicationList.get(mSelectedMedicationIndex - 1).getType());
                        Common.insertLog("mMedicationId::> " + mMedicationId);
                        Common.insertLog("mMedicationName::> " + mMedicationName);

                        if (!mSelMedicationType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_eye_drops))) {
                            mLinearDosage.setVisibility(View.VISIBLE);
                            mViewDosage.setVisibility(View.VISIBLE);
                            mTextInputEndDate.setVisibility(View.VISIBLE);
                            mViewEndDate.setVisibility(View.VISIBLE);
                            mTextInputHowMany.setVisibility(View.VISIBLE);
                            mViewHowMany.setVisibility(View.VISIBLE);
                            mLinearUOM.setVisibility(View.VISIBLE);
                            mViewUOM.setVisibility(View.VISIBLE);
                            mTextInputHowManyTimesADay.setVisibility(View.GONE);
                            mViewHowManyTimesADay.setVisibility(View.GONE);
                            mTextInputDuration.setVisibility(View.GONE);
                            mViewDuration.setVisibility(View.GONE);
                            mLinearMedicationType.setVisibility(View.GONE);
                            mViewMedicationType.setVisibility(View.GONE);
                        } else {
                            mLinearDosage.setVisibility(View.GONE);
                            mViewDosage.setVisibility(View.GONE);
                            mTextInputEndDate.setVisibility(View.GONE);
                            mViewEndDate.setVisibility(View.GONE);
                            mTextInputHowMany.setVisibility(View.GONE);
                            mViewHowMany.setVisibility(View.GONE);
                            mLinearUOM.setVisibility(View.GONE);
                            mViewUOM.setVisibility(View.GONE);
                            mTextInputHowManyTimesADay.setVisibility(View.VISIBLE);
                            mViewHowManyTimesADay.setVisibility(View.VISIBLE);
                            mTextInputDuration.setVisibility(View.VISIBLE);
                            mViewDuration.setVisibility(View.VISIBLE);
                            mLinearMedicationType.setVisibility(View.VISIBLE);
                            mViewMedicationType.setVisibility(View.VISIBLE);
                        }
                    }
                    break;

                /*case R.id.spinner_row_add_medication_dosage:
                    mSelectedDosageIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedDosageIndex != 0) {
                        mDosageId = (Integer.parseInt(mArrDosageList.get(mSelectedDosageIndex - 1).getDosageID()));
                        String mDosageName = (mArrDosageList.get(mSelectedDosageIndex - 1).getTitle());
                        Common.insertLog("mDosageId::> " + mDosageId);
                        Common.insertLog("mDosageName::> " + mDosageName);
                    }
                    break;*/

                case R.id.spinner_row_add_medication_uom:
                    mSelectedUOMIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedUOMIndex != 0) {
                        mUOMId = (Integer.parseInt(mArrUOMList.get(mSelectedUOMIndex - 1).getUOMID()));
                        String mUOMName = (mArrUOMList.get(mSelectedUOMIndex - 1).getUOM());
                        Common.insertLog("mUOMId::> " + mUOMId);
                        Common.insertLog("mUOMName::> " + mUOMName);
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    /**
     * Radio button on checked change listeners
     */
    private RadioGroup.OnCheckedChangeListener checkedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            RadioButton mRadioButton = group.findViewById(checkedId);
            mRadioButton.clearFocus();

            switch (group.getId()) {
                case R.id.radio_group_row_add_medication_type:
                    mType = group.getCheckedRadioButtonId();

                    if (mType == R.id.radio_row_add_medication_none) {
                        mMedicationType = mActivity.getResources().getString(R.string.text_none);
                    } else if (mType == R.id.radio_row_add_medication_days) {
                        mMedicationType = mActivity.getResources().getString(R.string.text_days);
                    } else if (mType == R.id.radio_row_add_medication_week) {
                        mMedicationType = mActivity.getResources().getString(R.string.text_week);
                    } else {
                        mMedicationType = mActivity.getResources().getString(R.string.text_month);
                    }
                    break;
            }
        }
    };

    /**
     * This method should add the medication dynamically
     */
    private void doAddMedication() {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addView = layoutInflater.inflate(R.layout.row_medication_item, null);

            // Linear Layouts
           // mLinearDosage = addView.findViewById(R.id.linear_row_add_medication_dosage);
            mLinearMedicationType = addView.findViewById(R.id.linear_row_add_medication_type);
            mLinearUOM = addView.findViewById(R.id.linear_row_add_medication_uom);

            // Spinners
            mSpinnerMedication = addView.findViewById(R.id.spinner_row_add_medication);
           // mSpinnerDosage = addView.findViewById(R.id.spinner_row_add_medication_dosage);
            mSpinnerUOM = addView.findViewById(R.id.spinner_row_add_medication_uom);

            // Text Input Layouts
            mTextInputEndDate = addView.findViewById(R.id.text_input_row_add_medication_end_date);
            mTextInputHowManyTimesADay = addView.findViewById(R.id.text_input_row_add_medication_how_many_times_a_day);
            mTextInputHowMany = addView.findViewById(R.id.text_input_row_add_medication_how_many);
            mTextInputDuration = addView.findViewById(R.id.text_input_row_add_medication_duration);

            // Edit Texts Views
            final EditText mEditStartDate = addView.findViewById(R.id.edit_row_add_medication_start_date);
            final EditText mEditEndDate = addView.findViewById(R.id.edit_row_add_medication_end_date);

            // Views
           // mViewDosage = addView.findViewById(R.id.view_row_add_medication_dosage);
            mViewEndDate = addView.findViewById(R.id.view_row_add_medication_end_date);
            mViewHowManyTimesADay = addView.findViewById(R.id.view_row_add_medication_how_many_times_a_day);
            mViewHowMany = addView.findViewById(R.id.view_row_add_medication_how_many);
            mViewUOM = addView.findViewById(R.id.view_row_add_medication_uom);
            mViewDuration = addView.findViewById(R.id.view_row_add_medication_duration);
            mViewMedicationType = addView.findViewById(R.id.view_row_add_medication_type);

            // Radio Group
            mRadioGroupMedicationType = addView.findViewById(R.id.radio_group_row_add_medication_type);

            // Radio Buttons
            mRadioNone = addView.findViewById(R.id.radio_row_add_medication_none);
            mRadioDays = addView.findViewById(R.id.radio_row_add_medication_days);
            mRadioWeek = addView.findViewById(R.id.radio_row_add_medication_week);

            // Image Views
            ImageView mImageRemove = addView.findViewById(R.id.image_row_add_medication_item_remove);

            // Recycler View
            mRecyclerView = addView.findViewById(R.id.recycler_view_row_add_medication_special_instructions);

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerMedication.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerDosage.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

            // ToDo: Doctor - Spinner Item Select Listener
            mSpinnerMedication.setOnItemSelectedListener(onItemSelectedListener);
            mSpinnerDosage.setOnItemSelectedListener(onItemSelectedListener);

            // ToDo: Radio Button Click Listeners
            mRadioGroupMedicationType.setOnCheckedChangeListener(checkedChangeListener);

            // Sets up the data
            mEditStartDate.setText(Common.setCurrentDate(mActivity));
            mEditEndDate.setText(Common.setCurrentDate(mActivity));

            // ToDo: Edit Text Start Date Click Listener
            mEditStartDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Common.openDatePicker(mActivity, mEditStartDate);
                }
            });

            // ToDo: Edit Text End Date Click Listener
            mEditEndDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Common.openDatePicker(mActivity, mEditEndDate);
                }
            });

            // ToDo: Item remove Text End Date Click Listener
            mImageRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLinearContainer.removeView(addView);
                }
            });

            callToMedicationAPI();
            callToDosageAPI();
            callToUOMAPI();
            callToSpecialInstructionsAPI();

            // Add dynamic view
            mLinearContainer.addView(addView, 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Medication API
     */
    private void callToMedicationAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setMedicationListJson(hospital_database));

            Call<MedicationModel> call = RetrofitClient.createService(ApiInterface.class).getMedicationList(body);
            call.enqueue(new Callback<MedicationModel>() {
                @Override
                public void onResponse(@NonNull Call<MedicationModel> call, @NonNull Response<MedicationModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrMedicationList.addAll(response.body().getData());
                            }
                            setsMedicationAdapter();
                        } else
                            setsMedicationAdapter();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MedicationModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setsMedicationAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsMedicationAdapter() {
        try {
            mArrMedication.add(0, mActivity.getResources().getString(R.string.spinner_select_medication));
            if (mArrMedicationList.size() > 0) {
                for (MedicationModel medicationModel : mArrMedicationList) {
                    String mMedication = medicationModel.getMedicationName() + " (" + medicationModel.getSuggestionDosage() + ")";
                    mArrMedication.add(mMedication);
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerMedication.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrMedication) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedMedicationIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerMedication.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Dosage API
     */
    private void callToDosageAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.setDosageListJson(hospital_database));

            Call<DosageModel> call = RetrofitClient.createService(ApiInterface.class).getDosage(body);
            call.enqueue(new Callback<DosageModel>() {
                @Override
                public void onResponse(@NonNull Call<DosageModel> call, @NonNull Response<DosageModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrDosageList.addAll(response.body().getData());
                            }
                            setsDosageAdapter();
                        } else {
                            setsDosageAdapter();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DosageModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setsDosageAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsDosageAdapter() {
        try {
            mArrDosage.add(0, mActivity.getResources().getString(R.string.spinner_select_dosage));


            // ToDo: Sets the spinner color as per the theme applied
            if (mArrDosageList.size() > 0) {
                for (DosageModel dosageModel : mArrDosageList) {
                    mArrDosage.add(dosageModel.getTitle());
                }
            }mSpinnerDosage.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrDosage) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedDosageIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerDosage.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the UOM API
     */
    private void callToUOMAPI() {
        try {
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setUOMListJson(mDatabaseName));

            Call<UOMModel> call = RetrofitClient.createService(ApiInterface.class).getUOMList(body);
            call.enqueue(new Callback<UOMModel>() {
                @Override
                public void onResponse(@NonNull Call<UOMModel> call, @NonNull Response<UOMModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrUOMList.addAll(response.body().getData());
                            }
                            setsUOMAdapter();
                        } else
                            setsUOMAdapter();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<UOMModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setsMedicationAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsUOMAdapter() {
        try {
            mArrUOM.add(0, mActivity.getResources().getString(R.string.spinner_select_uom));
            if (mArrUOMList.size() > 0) {
                for (UOMModel uomModel : mArrUOMList) {
                    String mUOM = uomModel.getUOM();
                    mArrUOM.add(mUOM);
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerUOM.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrUOM) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedUOMIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerUOM.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Special Instructions API
     */
    private void callToSpecialInstructionsAPI() {
        try {
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setSpecialInstructionsListJson(mDatabaseName));

            Call<SpecialInstructionsModel> call = RetrofitClient.createService(ApiInterface.class).getSpecialInstructionsList(body);
            call.enqueue(new Callback<SpecialInstructionsModel>() {
                @Override
                public void onResponse(@NonNull Call<SpecialInstructionsModel> call, @NonNull Response<SpecialInstructionsModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                mArrSpecialInstructions.addAll(response.body().getData());
                                setAdapterData();
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<SpecialInstructionsModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setNestedScrollingEnabled(false);
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(mLinearLayoutManager);

            SpecialInstructionsAdapter mAdapterSpecialInstructions = new SpecialInstructionsAdapter(mActivity, mUserType, mArrSpecialInstructions);
            mRecyclerView.setAdapter(mAdapterSpecialInstructions);
            mAdapterSpecialInstructions.setOnItemClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Used to remove all fragment in the fragment manager after logout
     */
    private void removeAllFragments() {
        try {
            List<Fragment> fragments = null;

            if (mUserType.equalsIgnoreCase(getString(R.string.user_type_receptionist))) {
                fragments = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_councillor))) {
                fragments = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
                fragments = ((OptometristHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                fragments = ((DoctorHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_patient))) {
                fragments = ((PatientHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            }

            if (fragments.size() > 0) {
                for (int i = 0; i < fragments.size(); i++) {
                    if (fragments.get(i) != null) {

                        if (mUserType.equalsIgnoreCase(getString(R.string.user_type_receptionist))) {
                            ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i));
                        } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_councillor))) {
                            ((CouncillorHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i));
                        } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
                            ((OptometristHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i));
                        } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                            ((DoctorHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i));
                        } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_patient))) {
                            ((PatientHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    public boolean checkAddValidation() {
        boolean status = true;

        if (mLinearContainer.getChildCount() == 0) {
            status = false;
            Common.setCustomToast(mActivity, getString(R.string.error_add_medication));
        } else {
            for (int i = 0; i < mLinearContainer.getChildCount(); i++) {
                View view = mLinearContainer.getChildAt(i);

                // Edit Texts
                EditText mEditStartDate = view.findViewById(R.id.edit_row_add_medication_start_date);
                EditText mEditHowMany = view.findViewById(R.id.edit_row_add_medication_how_many);
                EditText mEditHowManyTimesADay = view.findViewById(R.id.edit_row_add_medication_how_many_times_a_day);
                EditText mEditDuration = view.findViewById(R.id.edit_row_add_medication_duration);

                // Spinners
                Spinner mSpinnerMedication = view.findViewById(R.id.spinner_row_add_medication);
               // Spinner mSpinnerDosage = view.findViewById(R.id.spinner_row_add_medication_dosage);
                Spinner mSpinnerUOM = view.findViewById(R.id.spinner_row_add_medication_uom);

                int selectMedicine = mSpinnerMedication.getSelectedItemPosition();
                int selectDosage = mSpinnerDosage.getSelectedItemPosition();
                int selectedUOM = mSpinnerUOM.getSelectedItemPosition();

                String mHowMany = mEditHowMany.getText().toString().trim();
                String mHowManyTimesADay = mEditHowManyTimesADay.getText().toString().trim();
                String mDuration = mEditDuration.getText().toString().trim();

                mEditHowMany.setError(null);

                if (selectMedicine == 0) {
                    status = false;
                    Common.setCustomToast(mActivity, getString(R.string.error_select_medication));
                } else if (!mSelMedicationType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_eye_drops))) {
                    if (selectDosage == 0) {
                        status = false;
                        Common.setCustomToast(mActivity, getString(R.string.error_select_dosage));
                    } else if (selectedUOM == 0) {
                        status = false;
                        Common.setCustomToast(mActivity, getString(R.string.error_select_uom));
                    } else if (TextUtils.isEmpty(mHowMany)) {
                        mEditHowMany.requestFocus();
                        mEditHowMany.setError(getResources().getString(R.string.error_field_required));
                        status = false;
                    }
                } else {
                    if (TextUtils.isEmpty(mHowManyTimesADay)) {
                        mEditHowManyTimesADay.requestFocus();
                        mEditHowManyTimesADay.setError(getResources().getString(R.string.error_field_required));
                        status = false;
                    }
                    if (TextUtils.isEmpty(mDuration)) {
                        mEditDuration.requestFocus();
                        mEditDuration.setError(getResources().getString(R.string.error_field_required));
                        status = false;
                    }
                }

                String mStartDate = mEditStartDate.getText().toString();
                Date mEndDate = null;
                @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));;
                try {
                    mEndDate = format.parse(mStartDate);
                    System.out.println(mEndDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                if (!mSelMedicationType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_eye_drops))) {
                    mMedicationType = mActivity.getResources().getString(R.string.text_none);
                } else {
                    if (mRadioNone.isChecked()) {
                        mMedicationType = mActivity.getResources().getString(R.string.text_none);
                    } else if (mRadioDays.isChecked()) {
                        mMedicationType = mActivity.getResources().getString(R.string.text_days);
                        EndDate = String.valueOf(Common.addDay(mEndDate, Integer.parseInt(mDuration)));
                    } else if (mRadioWeek.isChecked()) {
                        mMedicationType = mActivity.getResources().getString(R.string.text_week);
                    } else {
                        mMedicationType = mActivity.getResources().getString(R.string.text_month);
                    }
                }
            }
        }
        return status;
    }

    /**
     * Call To API Add Medication On Patient
     */
    private void callToAddMedication() {
        try {
            JSONArray item = AddBindMedication();
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

//            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
//                    APICommonMethods.addMedication(item, hospital_database));
            /*Call<AddMedicationModel> call = RetrofitClient.createService(ApiInterface.class).addMedication(requestBody);
            call.enqueue(new Callback<AddMedicationModel>() {
                @Override
                public void onResponse(@NonNull Call<AddMedicationModel> call, Response<AddMedicationModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    String message = response.body().getMessage();
                    if (response.isSuccessful() && response.body().getError() == 200) {
                        Common.setCustomToast(mActivity, message);
                        redirectPreliminaryExaminationList();
                    } else {
                        Common.setCustomToast(mActivity, message);
                    }
                }

                @Override
                public void onFailure(Call<AddMedicationModel> call, Throwable t) {
                    Common.insertLog(t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());
                }
            });*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public JSONArray AddBindMedication() {
        JSONArray mArrMedication = new JSONArray();
        try {
            if (mLinearContainer.getChildCount() != 0) {
                String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);

                for (int i = 0; i < mLinearContainer.getChildCount(); i++) {
                    View view = mLinearContainer.getChildAt(i);
                    Spinner mSpinnerMedication = view.findViewById(R.id.spinner_row_add_medication);
                   // Spinner mSpinnerDosage = view.findViewById(R.id.spinner_row_add_medication_dosage);
                    Spinner mSpinnerUOM = view.findViewById(R.id.spinner_row_add_medication_uom);

                    // Edit Texts
                    EditText mEditStartDate = view.findViewById(R.id.edit_row_add_medication_start_date);
                    EditText mEditEndDate = view.findViewById(R.id.edit_row_add_medication_end_date);
                    EditText mEditNotes = view.findViewById(R.id.edit_row_add_medication_description);
                    EditText mEditHowManyTimesADay = view.findViewById(R.id.edit_row_add_medication_how_many_times_a_day);
                    EditText mEditHowMany = view.findViewById(R.id.edit_row_add_medication_how_many);
                    EditText mEditDuration = view.findViewById(R.id.edit_row_add_medication_duration);

                    int select_medication = mSpinnerMedication.getSelectedItemPosition();
                    String medication = mArrMedicationList.get(select_medication - 1).getMedicationID();
                    String mDosage = "0", mUOM = "0";

                    if (!mSelMedicationType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_eye_drops))) {
                        int select_dosage = mSpinnerDosage.getSelectedItemPosition();
                        int selectedUOM = mSpinnerUOM.getSelectedItemPosition();
                        mDosage = mArrDosageList.get(select_dosage - 1).getDosageID();
                        mUOM = mArrUOMList.get(selectedUOM - 1).getUOMID();
                    }

                    // SMILE
                    String startDate = Common.convertDateToServer(mActivity, mEditStartDate.getText().toString());
                    EndDate = Common.convertDateToServer(mActivity, mEditEndDate.getText().toString());
                    String Description = mEditNotes.getText().toString();
                    String mHowManyTimesADay = mEditHowManyTimesADay.getText().toString().trim();
                    String mHowMany = mEditHowMany.getText().toString().trim();
                    String mDuration = mEditDuration.getText().toString().trim();

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_APPOINTMENT_ID, mAppointmentId);
                    jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_USER_ID, mUserId);
                    jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_MEDICATION_ID, medication);
                    jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_DOSAGE_ID, mDosage);
                    jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_START_DATE, startDate);
                    jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_END_DATE, EndDate);
                    jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_DESCRIPTION, Description);
                    jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_TOTAL_TIMES, mHowManyTimesADay);
                    jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_TYPE, mMedicationType);
                    jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_TOTAL_DURATION, mDuration);
                    jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_TOTAL_QTY, mHowMany);
                    jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_UOM_ID, mUOM);
                    jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_SPECIAL_INSTRUCTION, mSpecialInstructions);
                    mArrMedication.put(jsonObject);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mArrMedication;
    }

    /**
     * Redirect To Preliminary Examination List
     */
    private void redirectPreliminaryExaminationList() {
        try {
            Fragment fragment = new OptometristPreliminaryExaminationFragment();
            Bundle args = new Bundle();

            FragmentManager fragmentManager = null;

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_preliminary_examination));
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_preliminary_examination));
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_preliminary_examination));
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_preliminary_examination));
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_patient))) {
                ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_preliminary_examination));
                fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            }
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    mActivity.getResources().getString(R.string.nav_menu_preliminary_examination)).commit();

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    @Override
    public void onSpecialInstructionsClickListener(String mFinalSpecialInstructions) {
        mSpecialInstructions = mFinalSpecialInstructions;
    }
}
