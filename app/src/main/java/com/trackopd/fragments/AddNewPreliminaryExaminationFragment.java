package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.ViewPagerAdapter;
import com.trackopd.model.AddAppointmentModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.PreliminaryExaminationModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import java.util.ArrayList;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class AddNewPreliminaryExaminationFragment extends Fragment {

    public static ViewPager mViewPager;
    //public  static JSONObject INSTANCE_DATA= new JSONObject();
    public static String INSTANCE_DATA = AppConstants.STR_EMPTY_STRING;
    public static String PRE_EXAMINATION = AppConstants.STR_EMPTY_STRING;
    public static String PRE_DIAGNOSIS = AppConstants.STR_EMPTY_STRING;
    public static String PreliminaryExminationId = "0";
    public static int PAGER_INDEX = 0;
    public Bundle mBundle;
    ViewPagerAdapter adapter = null;
    List<Fragment> fragmentList;
    private View mView;
    private Menu menu;
    private Activity mActivity;
    private TabLayout mTabLayout;
    private int mAppointmentId, CurrentPage = 1;
    private String mUserType, mPatientFirstName, mPatientLastName, mPatientMobile, mPatientId, mDoctorId,
            mProfileImage, mTreatmentDate, mCheckUpType;
    private PreliminaryExaminationModel preliminaryExaminationModel;
    private TextView mTextPatientName, mTextPatientMobile, mPatientCode;
    private ImageView mImageProfilePic, mImageHistory, mImagePreExamination, mImageCloseFile;
    private HistoryPreliminaryExaminationModel.Data data;
    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            List<Fragment> fragmentList = getFragmentManager().getFragments();

            if (mCheckUpType.equals(mActivity.getResources().getString(R.string.nav_menu_surgery))) {

                switch (PAGER_INDEX) {
                    case 0:
                        Fragment Surgery = adapter.getItem(0);
                        if (Surgery != null) {
                            ((CheckUpSurgeryFragment) Surgery).openDialogTreatmentSuggested();
                        }
                        break;
                    case 1:
                        Fragment Payment = adapter.getItem(1);
                        if (Payment != null) {
                            ((PaymentSurgeryFragment) Payment).openDialogTreatmentSuggested();
                        }
                        break;
                    case 2:
                        Fragment Print = adapter.getItem(2);
                        if (Print != null) {
                            ((SurgeryPrintFragment) Print).openDialogHistoryPrint();
                        }
                        break;
                }

            } else {

                switch (PAGER_INDEX) {
                    case 0:
                        Fragment historyComplains = adapter.getItem(0);
                        if (historyComplains != null) {
                            ((HistoryAndComplaintsFragment) historyComplains).openDialogHistoryandComplainList();
                        }
                        break;
                    case 1:
                        Fragment vision = adapter.getItem(1);
                        if (vision != null) {
                            ((VisionFragment) vision).openDialogHistoryandComplainList();
                        }

                        break;
                    case 2:
                        Fragment pre_examination = adapter.getItem(2);
                        if (pre_examination != null) {
                            ((PreExaminationFragment) pre_examination).openDialogPreExaminationHistory(mActivity);
                        }
                        break;
                    case 3:
                        Fragment diagnosis = adapter.getItem(3);
                        if (diagnosis != null) {
                            ((DiagnosisFragment) diagnosis).openDialogDiagnosis();
                        }
                        break;
                    case 4:
                        Fragment page = adapter.getItem(4);
                        if (page != null) {
                            ((DoodleFragment) page).openDialogDoodle();
                        }
                        break;
                    case 5:
                        Fragment doodle = adapter.getItem(5);
                        if (doodle != null) {
                            ((InvestigationFragment) doodle).openDialogDiagnosis();
                        }
                        break;
                    case 6:
                        Fragment treatment = adapter.getItem(6);
                        if (treatment != null) {
                            ((TreatmentSuggestedFragment) treatment).openDialogTreatmentSuggested();
                        }
                        break;

                    case 7:
                        Fragment surgerySuggested = adapter.getItem(7);
                        if (surgerySuggested != null) {
                            ((SurgerySuggestedFragment) surgerySuggested).openDialogTreatmentSuggested();
                        }
                        break;
                    case 8:
                        Fragment Couselling = adapter.getItem(8);
                        if (Couselling != null) {
                            ((CheckUpCounselingFragment) Couselling).openDialogTreatmentSuggested();
                        }
                        break;
                    case 9:
                        Fragment payment = adapter.getItem(9);
                        if (payment != null) {
                            ((CheckUpPaymentFragment) payment).openDialogTreatmentSuggested();
                        }
                        break;
                    case 10:
                        Fragment print = adapter.getItem(10);
                        if (print != null) {
                            ((PreliminaryPrintFragment) print).openDialogHistoryPrint();
                        }
                        break;
                }
            }
        }
    };

    public AddNewPreliminaryExaminationFragment() {

    }

    @SuppressLint("ValidFragment")
    public AddNewPreliminaryExaminationFragment(HistoryPreliminaryExaminationModel.Data data) {
        this.data = data;

    }

    // Set Current page
    public static void changePage(int pageIndex) {
        mViewPager.setCurrentItem(pageIndex);
    }

    public static String getPrelimary() {
        return PreliminaryExminationId;
    }

    public static int getCurrentPage() {
        Common.insertLog("Page Index" + PAGER_INDEX);
        return PAGER_INDEX;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_add_new_preliminary_examination, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        fragmentList = new ArrayList<>();

        getBundle();
        getIds();
        setRegListeners();
        setBundle();
        setData();
        setViewPager();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        final MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(false);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mPatientFirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mPatientLastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mPatientMobile = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mTreatmentDate = bundle.getString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE);
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mAppointmentId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mCheckUpType = bundle.getString(AppConstants.BUNDLE_CHECK_TYPE);
                PreliminaryExminationId = bundle.getString(AppConstants.BUNDLE_PRELIMINARY_EXAMINATION_ID);
                preliminaryExaminationModel = (PreliminaryExaminationModel) bundle.getSerializable(AppConstants.BUNDLE_PATIENT_ARRAY);
                mDoctorId = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);
                mProfileImage = bundle.getString(AppConstants.BUNDLE_ADD_PATIENT_PHOTO);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Tab Layout
            mTabLayout = mView.findViewById(R.id.tab_layout_preliminary_examination);

            //Text View
            mTextPatientName = mView.findViewById(R.id.text_view_add_new_prelimimary_examination_patient_name);
            mTextPatientMobile = mView.findViewById(R.id.text_view_add_new_prelimimary_examination_patient_mobile);
            //mPatientCode=mView.findViewById(R.id.text_add_new_prelimimary_examination_patient_code);

            //Image View
            mImageProfilePic = mView.findViewById(R.id.image_add_new_preliminary_examination_pic);
            mImageHistory = mView.findViewById(R.id.image_add_new_preliminary_examination_history);
            mImageCloseFile = mView.findViewById(R.id.image_close_patient_file);
            mImagePreExamination = mView.findViewById(R.id.image_add_new_preliminary_examination_pre_examination);

            // View Pager
            mViewPager = mView.findViewById(R.id.view_pager_preliminary_examination);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // mTabLayout.setOnTabSelectedListener(onTabSelectedListener(mViewPager));

            //Image ClickListener
            mImageHistory.setOnClickListener(clickListener);
            mImageCloseFile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callClick();
                }
            });
            mImagePreExamination.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment pre_examination = adapter.getItem(2);
                    if (pre_examination != null) {
                        ((PreExaminationFragment) pre_examination).openDialogPreExaminationChart(mActivity);
                    }
                }
            });

            //View Pager Change Scroll Listener
            mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {

                }

                @Override
                public void onPageSelected(int i) {
                    //viewPager.setCurrentItem(tab.getPosition());
                    PAGER_INDEX = i;
                    if (PAGER_INDEX == 2) {
                        if (mCheckUpType.equals(mActivity.getResources().getString(R.string.nav_menu_surgery))) {
                            mImagePreExamination.setVisibility(View.GONE);
                        } else {
                            mImagePreExamination.setVisibility(View.VISIBLE);
                        }
                    } else {
                        mImagePreExamination.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callClick() {

        if (mCheckUpType.equals(mActivity.getResources().getString(R.string.nav_menu_surgery))) {

            switch (PAGER_INDEX) {
                case 0:
                    Fragment Surgery = adapter.getItem(0);
                    if (Surgery != null) {
                        ((CheckUpSurgeryFragment) Surgery).openFileClosingAlert();
                    }
                    break;
                case 1:
                    Fragment Payment = adapter.getItem(1);
                    if (Payment != null) {
                        ((PaymentSurgeryFragment) Payment).openFileClosingAlert();
                    }
                    break;
                case 2:
                    Fragment Print = adapter.getItem(2);
                    if (Print != null) {
                        ((SurgeryPrintFragment) Print).openFileClosingAlert();
                    }
                    break;
            }

        } else {

            switch (PAGER_INDEX) {
                case 0:
                    Fragment historyComplains = adapter.getItem(0);
                    if (historyComplains != null) {
                        ((HistoryAndComplaintsFragment) historyComplains).openFileClosingAlert();
                    }
                    break;
                case 1:
                    Fragment vision = adapter.getItem(1);
                    if (vision != null) {
                        ((VisionFragment) vision).openFileClosingAlert();
                    }

                    break;
                case 2:
                    Fragment pre_examination = adapter.getItem(2);
                    if (pre_examination != null) {
                        ((PreExaminationFragment) pre_examination).openFileClosingAlert(mActivity);
                    }
                    break;
                case 3:
                    Fragment diagnosis = adapter.getItem(3);
                    if (diagnosis != null) {
                        ((DiagnosisFragment) diagnosis).openFileClosingAlert();
                    }
                    break;
                case 4:
                    Fragment page = adapter.getItem(4);
                    if (page != null) {
                        ((DoodleFragment) page).openFileClosingAlert();
                    }
                    break;
                case 5:
                    Fragment doodle = adapter.getItem(5);
                    if (doodle != null) {
                        ((InvestigationFragment) doodle).openFileClosingAlert();
                    }
                    break;
                case 6:
                    Fragment treatment = adapter.getItem(6);
                    if (treatment != null) {
                        ((TreatmentSuggestedFragment) treatment).openFileClosingAlert();
                    }
                    break;

                case 7:
                    Fragment surgerySuggested = adapter.getItem(7);
                    if (surgerySuggested != null) {
                        ((SurgerySuggestedFragment) surgerySuggested).openFileClosingAlert();
                    }
                    break;

                case 8:
                    Fragment Couselling = adapter.getItem(8);
                    if (Couselling != null) {
                        ((CheckUpCounselingFragment) Couselling).openFileClosingAlert();
                    }
                    break;
                case 9:
                    Fragment payment = adapter.getItem(9);
                    if (payment != null) {
                        ((CheckUpPaymentFragment) payment).openFileClosingAlert();
                    }
                    break;
                case 10:
                    Fragment print = adapter.getItem(10);
                    if (print != null) {
                        ((PreliminaryPrintFragment) print).openFileClosingAlert();
                    }
                    break;
            }
        }
    }

    /**
     * Set Data
     */
    private void setData() {
        try {
            mTextPatientName.setText(mPatientFirstName + AppConstants.STR_EMPTY_SPACE + mPatientLastName);
            mTextPatientMobile.setText(mPatientMobile);

            // Set Profile Pic
            if (mProfileImage != null &&
                    !mProfileImage.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                //Set Thumbnail
                String thumbmail_url = WebFields.API_BASE_URL + WebFields.IMAGE_THUMBNAIL_URL + mProfileImage;
                Glide.with(mActivity)
                        .load(thumbmail_url)
                        .apply(new RequestOptions().error(R.drawable.ic_documents_img).placeholder(R.drawable.ic_documents_img))
                        .into(mImageProfilePic);
            } else {
                mImageProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, mPatientFirstName, mPatientLastName));
            }

            String currentDate = Common.setCurrentDate(mActivity);
            String covertTreatmentDate = Common.convertDateUsingDateFormat(mActivity, mTreatmentDate,
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));

            if (currentDate.equalsIgnoreCase(covertTreatmentDate)) {
                mImageHistory.setVisibility(View.VISIBLE);
                mImageHistory.setEnabled(true);
            } else {
                mImageHistory.setVisibility(View.VISIBLE);
                mImageHistory.setEnabled(true);
                mImageHistory.setColorFilter(Common.setThemeColor(mActivity));
            }

            callToGetPreliminaryID();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the bundle data
     */
    private void setBundle() {
        try {
            mBundle = new Bundle();
            mBundle.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, mPatientFirstName);
            mBundle.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, mPatientLastName);
            mBundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mPatientMobile);
            mBundle.putString(AppConstants.BUNDLE_PATIENT_ID, mPatientId);
            mBundle.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, mAppointmentId);
            mBundle.putSerializable(AppConstants.BUNDLE_PATIENT_ARRAY, preliminaryExaminationModel);
            mBundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, mDoctorId);
            mBundle.putString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE, mTreatmentDate);
            mBundle.putString(AppConstants.BUNDLE_ADD_PATIENT_PHOTO, mProfileImage);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the view pager and binding the tab layouts
     */
    private void setViewPager() {
        if (mUserType.equalsIgnoreCase(getString(R.string.user_type_receptionist))) {
            adapter = new ViewPagerAdapter(((ReceptionistHomeActivity) mActivity).getSupportFragmentManager());
        } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_councillor))) {
            adapter = new ViewPagerAdapter(((CouncillorHomeActivity) mActivity).getSupportFragmentManager());
        } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
            adapter = new ViewPagerAdapter(((DoctorHomeActivity) mActivity).getSupportFragmentManager());

        } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
            adapter = new ViewPagerAdapter(((OptometristHomeActivity) mActivity).getSupportFragmentManager());
        }


        if (mCheckUpType.equals(mActivity.getResources().getString(R.string.nav_menu_surgery))) {
            adapter.addFragment(new CheckUpSurgeryFragment(), mActivity.getResources().getString(R.string.nav_menu_surgery), mBundle);
            adapter.addFragment(new PaymentSurgeryFragment(), mActivity.getResources().getString(R.string.nav_menu_payment), mBundle);
            adapter.addFragment(new SurgeryPrintFragment(), mActivity.getResources().getString(R.string.tab_print), mBundle);

        } else {
            adapter.addFragment(new HistoryAndComplaintsFragment(), mActivity.getResources().getString(R.string.tab_history_and_complaints), mBundle);
            adapter.addFragment(new VisionFragment(), mActivity.getResources().getString(R.string.tab_vision), mBundle);
            adapter.addFragment(new PreExaminationFragment(), mActivity.getResources().getString(R.string.tab_pre_examination), mBundle);
            adapter.addFragment(new DiagnosisFragment(), mActivity.getResources().getString(R.string.tab_diagnosis), mBundle);
            adapter.addFragment(new DoodleFragment(), mActivity.getResources().getString(R.string.tab_doodle), mBundle);
            adapter.addFragment(new InvestigationFragment(), mActivity.getResources().getString(R.string.tab_investigation), mBundle);
            adapter.addFragment(new TreatmentSuggestedFragment(), mActivity.getResources().getString(R.string.tab_prescription), mBundle);
            adapter.addFragment(new SurgerySuggestedFragment(), mActivity.getResources().getString(R.string.tab_surgery_suggested), mBundle);
            adapter.addFragment(new CheckUpCounselingFragment(), mActivity.getResources().getString(R.string.nav_menu_counselling), mBundle);
            adapter.addFragment(new CheckUpPaymentFragment(), mActivity.getResources().getString(R.string.nav_menu_payment), mBundle);
            adapter.addFragment(new PreliminaryPrintFragment(), mActivity.getResources().getString(R.string.tab_print), mBundle);
        }

        mViewPager.setAdapter(adapter);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setOffscreenPageLimit(1);

        if (mCheckUpType == null) {
            mViewPager.setCurrentItem(0);
        } else if (mCheckUpType.equalsIgnoreCase(mActivity.getResources().getString(R.string.tab_vision))) {
            mViewPager.setCurrentItem(1);
        } else if (mCheckUpType.equalsIgnoreCase(mActivity.getResources().getString(R.string.tab_diagnosis))) {
            mViewPager.setCurrentItem(3);
        } else if (mCheckUpType.equalsIgnoreCase(mActivity.getResources().getString(R.string.tab_investigation))) {
            mViewPager.setCurrentItem(5);
        } else if (mCheckUpType.equalsIgnoreCase(mActivity.getResources().getString(R.string.tab_doodle))) {
            mViewPager.setCurrentItem(4);
        } else if (mCheckUpType.equalsIgnoreCase(mActivity.getResources().getString(R.string.tab_prescription))) {
            mViewPager.setCurrentItem(6);
        } else if (mCheckUpType.equalsIgnoreCase(mActivity.getResources().getString(R.string.tab_surgery_suggested))) {
            mViewPager.setCurrentItem(7);
        } else if (mCheckUpType.equalsIgnoreCase(mActivity.getResources().getString(R.string.nav_menu_counselling))) {
            mViewPager.setCurrentItem(8);
        }

        mTabLayout.setSelectedTabIndicatorColor(Common.setThemeColor(mActivity));
        mTabLayout.setTabTextColors(mActivity.getResources().getColor(R.color.colorBlack), Common.setThemeColor(mActivity));
    }

    private TabLayout.OnTabSelectedListener onTabSelectedListener(final ViewPager viewPager) {

        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                PAGER_INDEX = tab.getPosition();

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        };
    }

    /**
     * get Preliminary Examination
     */
    private void callToGetPreliminaryID() {

        String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);
        String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);

        String convert_yyy_mm_dd = Common.convertDateUsingDateFormat(mActivity, mTreatmentDate,
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen),
                mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));

        RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                APICommonMethods.setAddPreliminaryBasicDetailsJson(convert_yyy_mm_dd, String.valueOf(mAppointmentId), mPatientId, mUserId, mDoctorId, hospital_database));

        Call<AddAppointmentModel> call = RetrofitClient.createService(ApiInterface.class).setAddPreliminaryBasicDetails(requestBody);
        call.enqueue(new Callback<AddAppointmentModel>() {
            @Override
            public void onResponse(Call<AddAppointmentModel> call, Response<AddAppointmentModel> response) {
                try {
                    if (response.isSuccessful() && response.body().getError() == 200) {
                        if (response.body().getData() != null
                                && response.body().getData().size() > 0) {
                            PreliminaryExminationId = response.body().getData().get(0).getID();
                        } else {
                            Common.insertLog(response.body().toString());
                        }
                    }
                } catch (Exception e) {
                    Common.insertLog(e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<AddAppointmentModel> call, Throwable t) {
                Common.insertLog("Failure:::> " + t.getMessage());
                Common.setCustomToast(mActivity, t.getMessage());
            }
        });
    }
}