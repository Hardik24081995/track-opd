package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.ReceptionistPatientAdapter;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.ReceptionistPatientModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.utils.RecyclerProgressView;
import com.trackopd.utils.SessionManager;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;
import com.yalantis.flipviewpager.utils.FlipSettings;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceptionistPatientFragment extends Fragment {

    private View mView;
    private Menu menu;
    private String mIsFromSearch;
    private Activity mActivity;
    private AppUtil mAppUtils;
    private SwipeRefreshLayout mSwipeRefreshView;
    private RelativeLayout mRelativeNoData, mRelativeNoInternet;
    private Button mButtonRetry;
    private RecyclerView mRecyclerView;
    private ReceptionistPatientAdapter mAdapterReceptionistPatients;
    private ArrayList<ReceptionistPatientModel> mArrReceptionistPatients;
    private boolean isFirstTime = true;
    private String mAddPatient;
    private String mName = "", mPatientCode = "", mMobileNo = "", mUserType;

    // Load More Listener Variables
    private int currentPageIndex = 1, lastFetchRecord = 0, totalRecords = 0;
    private boolean isRefresh = false, isLoadMore = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_receptionist_patient, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mAppUtils = new AppUtil(getActivity());
        mArrReceptionistPatients = new ArrayList<>();
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        // Keyboard  Hides
//        Common.hideSoftKeyboard(mActivity);
        getBundle();
        getIds();
        setRegListeners();
        callShimmerView();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Set the prepare menu options
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.invalidateOptionsMenu();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        final MenuItem menuSearch = menu.findItem(R.id.action_search);
        MenuItem menuAdd = menu.findItem(R.id.action_add);

        if(mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
            if(!mIsFromSearch.equalsIgnoreCase(AppConstants.BUNDLE_ADD_FOLLOW_UP) &&
                    !mIsFromSearch.equalsIgnoreCase(AppConstants.BUNDLE_ADD_APPOINTMENT)) {
                menuAdd.setVisible(true);
            }
        } else {
            menuAdd.setVisible(false);
        }
        menuSearch.setVisible(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_add:
                callToAddReceptionistPatientFragment();
                return true;

            case R.id.action_search:
                callToSearchPatientFragment();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mIsFromSearch = bundle.getString(AppConstants.BUNDLE_IS_FROM_SEARCH);
                mAddPatient = bundle.getString(AppConstants.BUNDLE_ADD_PATIENT);
                mName = bundle.getString(AppConstants.BUNDLE_PATIENT_NAME);
                mPatientCode = bundle.getString(AppConstants.BUNDLE_PATIENT_CODE);
                mMobileNo = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                Common.insertLog("mIsFromSearch::::> " +mIsFromSearch);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Recycler View
            mRecyclerView = mView.findViewById(R.id.recycler_view);

            // Swipe Refresh View
            mSwipeRefreshView = mView.findViewById(R.id.swipe_refresh_view);

            // Relative Layouts
            mRelativeNoData = mView.findViewById(R.id.relative_no_data_available);
            mRelativeNoInternet = mView.findViewById(R.id.relative_no_internet);

            // Buttons
            mButtonRetry = mView.findViewById(R.id.button_retry);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Click Listeners
            mButtonRetry.setOnClickListener(clickListener);

            // ToDo: Set On Refresh Listeners
            mSwipeRefreshView.setOnRefreshListener(refreshListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_retry:
                    callShimmerView();
                    break;
            }
        }
    };

    /**
     * Set On Refresh Listeners
     */
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {

        @Override
        public void onRefresh() {
            isFirstTime = true;

            if (!mAppUtils.getConnectionState()) {
                mRelativeNoInternet.setVisibility(View.VISIBLE);
                mSwipeRefreshView.setRefreshing(false);
                mArrReceptionistPatients.clear();
                setViewVisibility();
            } else {
                mRelativeNoInternet.setVisibility(View.GONE);
                if (!isLoadMore) {
                    isRefresh = true;

                    mName = AppConstants.STR_EMPTY_STRING;
                    mPatientCode = AppConstants.STR_EMPTY_STRING;
                    mMobileNo = AppConstants.STR_EMPTY_STRING;

                    if (mArrReceptionistPatients != null)
                        mArrReceptionistPatients.clear();
                    if (mAdapterReceptionistPatients != null)
                        mAdapterReceptionistPatients.notifyDataSetChanged();
                    currentPageIndex = 1;
                    callShimmerView();
                } else
                    mSwipeRefreshView.setRefreshing(false);
            }
        }
    };

    /**
     * This method is used to call the shimmer effect for recycler view before calling the API
     */
    private void callShimmerView() {
        if (!mAppUtils.getConnectionState()) {
            mRelativeNoInternet.setVisibility(View.VISIBLE);
            mArrReceptionistPatients.clear();
            setViewVisibility();
        } else {
            mRelativeNoInternet.setVisibility(View.GONE);
            if (isFirstTime) {
                showProgressView();
                isFirstTime = false;
            }
            callPatientListAPI();
        }
    }

    /**
     * Show progress view when page load for the first time
     */
    private void showProgressView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.VISIBLE);

                RecyclerProgressView.startProgress((LinearLayout) mView.findViewById(R.id.linear_add_recycler_progress_view),
                        R.layout.row_receptionist_patient_item, getActivity());
            }
        }, 100);
    }

    /**
     * This method should call the patient listing for receptionist
     */
    private void callPatientListAPI() {
        try {
            if (mName == null) {
                mName = AppConstants.STR_EMPTY_STRING;
            }

            if (mPatientCode == null) {
                mPatientCode = AppConstants.STR_EMPTY_STRING;
            }

            if (mMobileNo == null) {
                mMobileNo = AppConstants.STR_EMPTY_STRING;
            }
            String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setReceptionistPatientListJson(currentPageIndex, mName, mPatientCode, mMobileNo,hospital_database));

            Call<ReceptionistPatientModel> call = RetrofitClient.createService(ApiInterface.class).getReceptionistPatientList(requestBody);
            call.enqueue(new Callback<ReceptionistPatientModel>() {
                @Override
                public void onResponse(@NonNull Call<ReceptionistPatientModel> call, @NonNull Response<ReceptionistPatientModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        stopProgressView();
                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                ArrayList<ReceptionistPatientModel> patientModel = new ArrayList<>(Arrays
                                        .asList(new GsonBuilder().serializeNulls().create()
                                                .fromJson(new JsonParser().parse(jsonObject.getString(WebFields.DATA)).
                                                        getAsJsonArray(), ReceptionistPatientModel[].class)));


                                if (mArrReceptionistPatients != null && mArrReceptionistPatients.size() > 0 &&
                                        mAdapterReceptionistPatients != null) {
                                    mArrReceptionistPatients.addAll(patientModel);
                                    mAdapterReceptionistPatients.notifyDataSetChanged();
                                    lastFetchRecord = mArrReceptionistPatients.size();
                                } else {
                                    mArrReceptionistPatients = patientModel;
                                    setAdapterData();
                                    lastFetchRecord = mArrReceptionistPatients.size();
                                    totalRecords = jsonObject.getInt(WebFields.ROW_COUNT);
                                    setLoadMoreClickListener();

                                    if (mRecyclerView.getVisibility() == View.GONE) {
                                        stopProgressView();
                                    }
                                }
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                            stopProgressView();
                            setViewVisibility();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ReceptionistPatientModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    stopProgressView();
                    setViewVisibility();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mGridLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(mGridLayoutManager);

            stopProgressView();
            FlipSettings settings = new FlipSettings.Builder().defaultPage(1).build();

            mAdapterReceptionistPatients = new ReceptionistPatientAdapter(mActivity, mRecyclerView, mArrReceptionistPatients, mIsFromSearch, mAddPatient, settings);
            mRecyclerView.setAdapter(mAdapterReceptionistPatients);

            setViewVisibility();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the view visibility as per the array size
     */
    private void setViewVisibility() {
        if (mArrReceptionistPatients.size() != 0) {
            mRelativeNoData.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mRelativeNoData.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    /**
     * Show progress view when page load for the first time
     */
    private void stopProgressView() {
        try {
            mSwipeRefreshView.setRefreshing(false);
            isRefresh = false;
            isLoadMore = false;
            RecyclerProgressView.stopProgress();
            mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            hideProgressBar();
        }
    }

    /**
     * Sets up the Receptionist Add New Patient Fragment
     */
    private void callToAddReceptionistPatientFragment() {
        try {
            if (mUserType.equalsIgnoreCase(getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_patient));
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_patient));
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_patient));
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_patient));
            }

            Fragment fragment = new AddPatientFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_LOGIN_TYPE, mUserType);
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_new_patient))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_new_patient))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Receptionist Search Patient Fragment
     */
    private void callToSearchPatientFragment() {
        try {
            if (mUserType.equalsIgnoreCase(getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            }

            Fragment fragment = new SearchReceptionistPatientFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_IS_FROM_SEARCH, mIsFromSearch);
            args.putString(AppConstants.BUNDLE_ADD_PATIENT, mAddPatient);
            args.putString(AppConstants.BUNDLE_PATIENT_NAME, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, AppConstants.STR_EMPTY_STRING);
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_search_receptionist_patient))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_search_receptionist_patient))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Hide the progress bar
     */
    private void hideProgressBar() {
        try {
            if (mArrReceptionistPatients != null && mArrReceptionistPatients.size() > 0 &&
                    mArrReceptionistPatients.get(mArrReceptionistPatients.size() - 1) == null) {

                mArrReceptionistPatients.remove(mArrReceptionistPatients.size() - 1);
                mAdapterReceptionistPatients.notifyItemRemoved(mArrReceptionistPatients.size());

                mAdapterReceptionistPatients.notifyDataSetChanged();
                mAdapterReceptionistPatients.setLoaded();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RecyclerProgressView.stopProgress();
    }

    /**
     * On load more click listener
     */
    private void setLoadMoreClickListener() {
        mAdapterReceptionistPatients.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (mArrReceptionistPatients != null && mArrReceptionistPatients.size() > 0 && mArrReceptionistPatients.size() <
                        totalRecords) {
                    if (!mAppUtils.getConnectionState()) {
                        mRelativeNoInternet.setVisibility(View.VISIBLE);
                        mArrReceptionistPatients.clear();
                        setViewVisibility();
                    } else {
                        mRelativeNoInternet.setVisibility(View.GONE);
                        if (!isRefresh) {
                            isLoadMore = true;
                            mArrReceptionistPatients.add(null);
                            //mAdapterReceptionistPatients.notifyItemInserted(mArrReceptionistPatients.size() - 1);

                            currentPageIndex = (mArrReceptionistPatients.size() / Common.getPageSizeForPagination(mActivity)) + 1;
                            callPatientListAPI();
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isFirstTime = true;
        currentPageIndex = 1;
    }
}