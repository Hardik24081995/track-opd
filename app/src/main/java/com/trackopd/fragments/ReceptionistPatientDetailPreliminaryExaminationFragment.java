package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.adapter.OptometristPriliminaryExaminationAdapter;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.PreliminaryExaminationModel;
import com.trackopd.model.ReceptionistPatientModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.utils.RecyclerProgressView;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceptionistPatientDetailPreliminaryExaminationFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private SwipeRefreshLayout mSwipeRefreshView;
    private RecyclerView mRecyclerView;
    private AppUtil mAppUtils;
    private RelativeLayout mRelativeNoData, mRelativeNoInternet;
    private Button mButtonRetry;
    private String mUserType,mPatientProfile;
    private boolean isFirstTime = true;
    private ArrayList<PreliminaryExaminationModel> mArrPreliminaryExamination;
    private OptometristPriliminaryExaminationAdapter mAdapterOptometristPreliminaryExamination;
    private String mName = "", mMobileNo = "", mTreatmentDate = "0000-00-00";
    private int mDoctorId = -1, mAppointmentId = -1;

    // Load More Listener Variables
    private int currentPageIndex = 1, lastFetchRecord = 0, totalRecords = 0;
    private boolean isRefresh = false, isLoadMore = false, isStarted = false, isVisible = false;;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_optometrist_preliminary_examination, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mAppUtils = new AppUtil(getActivity());
        mArrPreliminaryExamination = new ArrayList<>();
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        getBundle();
        getIds();
        setRegListeners();
        callShimmerView();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        final MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(true);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                callToAddPreliminaryExaminationFragment();
                return true;

            case R.id.action_search:
                callToSearchOptometristPreliminaryExaminationFragment();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mName = bundle.getString(AppConstants.BUNDLE_PATIENT_NAME);
                mMobileNo = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mPatientProfile = bundle.getString(AppConstants.BUNDLE_PATIENT_PROFILE);

                mTreatmentDate = bundle.getString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE);

//                mDoctorId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Recycler View
            mRecyclerView = mView.findViewById(R.id.recycler_view);

            // Swipe Refresh View
            mSwipeRefreshView = mView.findViewById(R.id.swipe_refresh_view);

            // Relative Layouts
            mRelativeNoData = mView.findViewById(R.id.relative_no_data_available);
            mRelativeNoInternet = mView.findViewById(R.id.relative_no_internet);

            // Buttons
            mButtonRetry = mView.findViewById(R.id.button_retry);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Click Listeners
            mButtonRetry.setOnClickListener(clickListener);

            // ToDo: Set On Refresh Listeners
            mSwipeRefreshView.setOnRefreshListener(refreshListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_retry:
                    callShimmerView();
                    break;
            }
        }
    };

    /**
     * Set On Refresh Listeners
     */
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            isFirstTime = true;

            if (!mAppUtils.getConnectionState()) {
                mRelativeNoInternet.setVisibility(View.VISIBLE);
                mSwipeRefreshView.setRefreshing(false);
                mArrPreliminaryExamination.clear();
                setViewVisibility();
            } else {
                mRelativeNoInternet.setVisibility(View.GONE);
                if (!isLoadMore) {
                    isRefresh = true;

                    /*mName = AppConstants.STR_EMPTY_STRING;
                    mMobileNo = AppConstants.STR_EMPTY_STRING;
                    mTreatmentDate = mActivity.getResources().getString(R.string.date_format_first_time);
                    mDoctorId = -1;
                    mAppointmentId = -1;*/

                    getBundle();

                    if (mArrPreliminaryExamination != null)
                        mArrPreliminaryExamination.clear();
                    if (mAdapterOptometristPreliminaryExamination != null)
                        mAdapterOptometristPreliminaryExamination.notifyDataSetChanged();
                    currentPageIndex = 1;
                    callShimmerView();
                } else
                    mSwipeRefreshView.setRefreshing(false);
            }
        }
    };

    /**
     * This method is used to call the shimmer effect for recycler view before calling the API
     */
    private void callShimmerView() {
        if (!mAppUtils.getConnectionState()) {
            mRelativeNoInternet.setVisibility(View.VISIBLE);
            mArrPreliminaryExamination.clear();
            setViewVisibility();
        } else {
            mRelativeNoInternet.setVisibility(View.GONE);
            if (isFirstTime) {
                showProgressView();
                isFirstTime = false;
                callPreliminaryExaminationListAPI();
            } else {
                callPreliminaryExaminationListAPI();
            }
        }
    }

    /**
     * This method should call the preliminary examination listing for optometrist
     */
    private void callPreliminaryExaminationListAPI() {
        try {
            if (mName == null) {
                mName = AppConstants.STR_EMPTY_STRING;
            }

            if (mMobileNo == null) {
                mMobileNo = AppConstants.STR_EMPTY_STRING;
            }

            if (mTreatmentDate == null) {
                mTreatmentDate = mActivity.getResources().getString(R.string.date_format_first_time);
            }

            String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPreliminaryExaminationListJson(currentPageIndex, mName, mMobileNo, mTreatmentDate, mDoctorId, mAppointmentId, hospital_database));

            Call<PreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationList(requestBody);
            call.enqueue(new Callback<PreliminaryExaminationModel>() {
                @Override
                public void onResponse(@NonNull Call<PreliminaryExaminationModel> call, @NonNull Response<PreliminaryExaminationModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        stopProgressView();
                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                ArrayList<PreliminaryExaminationModel> preliminaryExaminationModel = new ArrayList<>(Arrays
                                        .asList(new GsonBuilder().serializeNulls().create()
                                                .fromJson(new JsonParser().parse(jsonObject.getString(WebFields.DATA)).
                                                        getAsJsonArray(), PreliminaryExaminationModel[].class)));

                                if (mArrPreliminaryExamination != null && mArrPreliminaryExamination.size() > 0 &&
                                        mAdapterOptometristPreliminaryExamination != null) {
                                    mArrPreliminaryExamination.addAll(preliminaryExaminationModel);
                                    mAdapterOptometristPreliminaryExamination.notifyDataSetChanged();
                                    lastFetchRecord = mArrPreliminaryExamination.size();
                                } else {
                                    mArrPreliminaryExamination = preliminaryExaminationModel;
                                    setAdapterData();
                                    lastFetchRecord = mArrPreliminaryExamination.size();
                                    totalRecords = jsonObject.getInt(WebFields.ROW_COUNT);
                                    setLoadMoreClickListener();

                                    if (mRecyclerView.getVisibility() == View.GONE) {
                                        stopProgressView();
                                    }
                                }
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                            stopProgressView();
                            setViewVisibility();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PreliminaryExaminationModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());
                    stopProgressView();
                    setViewVisibility();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mGridLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(mGridLayoutManager);

            stopProgressView();
            mAdapterOptometristPreliminaryExamination = new OptometristPriliminaryExaminationAdapter(mActivity, mUserType, mRecyclerView, mArrPreliminaryExamination);
            mRecyclerView.setAdapter(mAdapterOptometristPreliminaryExamination);

            setViewVisibility();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the view visibility as per the array size
     */
    private void setViewVisibility() {
        if (mArrPreliminaryExamination.size() != 0) {
            mRelativeNoData.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mRelativeNoData.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    /**
     * Show progress view when page load for the first time
     */
    private void showProgressView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.VISIBLE);

                RecyclerProgressView.startProgress((LinearLayout) mView.findViewById(R.id.linear_add_recycler_progress_view),
                        R.layout.row_optometrist_preliminary_examination_item, getActivity());
            }
        }, 100);
    }

    /**
     * Show progress view when page load for the first time
     */
    private void stopProgressView() {
        try {
            mSwipeRefreshView.setRefreshing(false);
            isRefresh = false;
            isLoadMore = false;
            RecyclerProgressView.stopProgress();
            mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            hideProgressBar();
        }
    }

    /**
     * Sets up the Add Preliminary Examination Fragment
     */
    private void callToAddPreliminaryExaminationFragment() {
        try {
            Fragment fragment = new AddSurgeryPatientDetailFragment();
            Bundle args = new Bundle();


            fragment.setArguments(args);
            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            }

            if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_search_receptionist_patient))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_search_receptionist_patient))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Search Optometrist Preliminary Examination Fragment
     */
    private void callToSearchOptometristPreliminaryExaminationFragment() {
        try {
            if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            }

            Fragment fragment = new SearchOptometristPreliminaryExaminationFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_ADD_PATIENT, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_IS_FROM_SEARCH, AppConstants.OPTOMETRIST_PRELIMINARY_EXAMINATION);
            fragment.setArguments(args);
            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_search_optometrist_preliminary_examination))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_optometrist_preliminary_examination))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Hide the progress bar
     */
    private void hideProgressBar() {
        try {
            if (mArrPreliminaryExamination != null && mArrPreliminaryExamination.size() > 0 &&
                    mArrPreliminaryExamination.get(mArrPreliminaryExamination.size() - 1) == null) {

                mArrPreliminaryExamination.remove(mArrPreliminaryExamination.size() - 1);
                mAdapterOptometristPreliminaryExamination.notifyItemRemoved(mArrPreliminaryExamination.size());

                mAdapterOptometristPreliminaryExamination.notifyDataSetChanged();
                mAdapterOptometristPreliminaryExamination.setLoaded();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * On load more click listener
     */
    private void setLoadMoreClickListener() {
        mAdapterOptometristPreliminaryExamination.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (mArrPreliminaryExamination != null && mArrPreliminaryExamination.size() > 0 && mArrPreliminaryExamination.size() <
                        totalRecords) {
                    if (!mAppUtils.getConnectionState()) {
                        mRelativeNoInternet.setVisibility(View.VISIBLE);
                        mArrPreliminaryExamination.clear();
                        setViewVisibility();
                    } else {
                        mRelativeNoInternet.setVisibility(View.GONE);
                        if (!isRefresh) {
                            isLoadMore = true;
                            mArrPreliminaryExamination.add(null);
                            mAdapterOptometristPreliminaryExamination.notifyItemInserted(mArrPreliminaryExamination.size() - 1);

                            currentPageIndex = (mArrPreliminaryExamination.size() / Common.getPageSizeForPagination(mActivity)) + 1;
                            callPreliminaryExaminationListAPI();
                        }
                    }
                }
            }
        });
    }

    /**
     * Receptionist Patient Detail Follow Up Fragment on visible
     *
     * @param isVisibleToUser - This can check the fragment is visible when swiping and will return
     *                        the boolean value based on that
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        isVisible = isVisibleToUser;
        if (isStarted && isVisible) {
            mArrPreliminaryExamination.clear();

            callShimmerView();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
            mArrPreliminaryExamination.clear();
            if (mAdapterOptometristPreliminaryExamination != null) {
                mAdapterOptometristPreliminaryExamination.notifyDataSetChanged();
            }
            getBundle();
            callShimmerView();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isVisible = false;
        isStarted = false;
    }
}