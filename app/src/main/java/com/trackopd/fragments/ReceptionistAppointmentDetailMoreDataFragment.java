package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.ViewPagerAdapter;
import com.trackopd.model.ReceptionistAppointmentModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

public class ReceptionistAppointmentDetailMoreDataFragment extends Fragment {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private TextView mTextPatientName, mTextPatientMobileNo, mTextAppointmentNo, mTextAppointmentDate, mTextDoctorName, mTextBookingType, mTextPaymentType, mTextReason;
    public ReceptionistAppointmentModel receptionistAppointmentModel;
    public String mFirstName="",mLastName="",mPatientCode="",mMobile="",mAppointmentNo="",
               mAppointmentType="",mAppointmentStatus="",mAppointmentDate="",mDoctorName="",
            PaymentStatus="";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_receptionist_appointment_detail_more_data, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        getBundle();
        getIds();
        setData();
        setHasOptionsMenu(false);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        final MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(false);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                //receptionistAppointmentModel = (ReceptionistAppointmentModel) bundle.getSerializable(AppConstants.BUNDLE_PATIENT_ARRAY);

                mFirstName=bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mLastName=bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mMobile=bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mAppointmentDate=bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DATE);
                mDoctorName=bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);
                mAppointmentType=bundle.getString(AppConstants.BUNDLE_APPOINTMENT_TYPE);
                PaymentStatus= bundle.getString(AppConstants.BUNDLE_PAYMENT_EDIT_MODE);
                mAppointmentNo=bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Text Views
            mTextPatientName = mView.findViewById(R.id.text_appointment_detail_more_data_patient_name);
            mTextPatientMobileNo = mView.findViewById(R.id.text_appointment_detail_more_data_patient_mobile_no);
            mTextAppointmentNo = mView.findViewById(R.id.text_appointment_detail_more_data_appointment_number);
            mTextAppointmentDate = mView.findViewById(R.id.text_appointment_detail_more_data_appointment_date);
            mTextDoctorName = mView.findViewById(R.id.text_appointment_detail_more_data_doctor_name);
            mTextBookingType = mView.findViewById(R.id.text_appointment_detail_more_data_booking_type);
            mTextPaymentType = mView.findViewById(R.id.text_appointment_detail_more_data_payment_type);
            mTextReason = mView.findViewById(R.id.text_appointment_detail_more_data_reason);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            String mPatientName = mFirstName + AppConstants.STR_EMPTY_SPACE + mLastName;

            mTextPatientName.setText(Common.isEmptyString(mActivity, mPatientName));
            mTextPatientMobileNo.setText(Common.isEmptyString(mActivity, mMobile));
            mTextAppointmentNo.setText(Common.isEmptyString(mActivity,mAppointmentNo ));
            mTextAppointmentDate.setText(Common.isEmptyString(mActivity, mAppointmentDate));
            mTextDoctorName.setText(Common.isEmptyString(mActivity, mDoctorName));
            mTextBookingType.setText(Common.isEmptyString(mActivity, mAppointmentType));
            mTextPaymentType.setText(Common.isEmptyString(mActivity,PaymentStatus));
            mTextReason.setText(Common.isEmptyString(mActivity, ""));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}