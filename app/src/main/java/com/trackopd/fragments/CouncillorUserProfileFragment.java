package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.trackopd.R;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

import de.hdodenhof.circleimageview.CircleImageView;

public class CouncillorUserProfileFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private ImageView mImageUserProfilePicWithText;
    private EditText mEditFirstName, mEditLastName, mEditMobile;
    private CircleImageView mImageUserProfilePicWithoutText;
    private RelativeLayout mRelativeImageWithText, mRelativeImageWithoutText;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_councillor_user_profile, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        getIds();
        setData();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layouts
            mRelativeImageWithText = mView.findViewById(R.id.relative_councillor_user_profile_pic_with_text);
            mRelativeImageWithoutText = mView.findViewById(R.id.relative_councillor_user_profile_pic_without_text);

            // Image Views
            mImageUserProfilePicWithText = mView.findViewById(R.id.image_councillor_user_profile_pic_with_text);
            mImageUserProfilePicWithoutText = mView.findViewById(R.id.image_councillor_user_profile_pic_without_text);

            // Edit Texts
            mEditFirstName = mView.findViewById(R.id.edit_councillor_user_profile_first_name);
            mEditLastName = mView.findViewById(R.id.edit_councillor_user_profile_last_name);
            mEditMobile = mView.findViewById(R.id.edit_councillor_user_profile_mobile_no);

            // Set Request Focus
            mEditFirstName.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            String mFirstName = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_FIRST_NAME);
            String mLastName = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_LAST_NAME);
            String mMobile = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_MOBILE_NO);
            String mUserName = mFirstName + AppConstants.STR_EMPTY_SPACE + mLastName;

            // ToDo: Round with Border
            if (mUserName.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                mRelativeImageWithText.setVisibility(View.GONE);
                mRelativeImageWithoutText.setVisibility(View.VISIBLE);

//            Glide.with(mActivity).load(SessionManager.getBaseUrlForVisitList(mActivity) + productListModel
//                    .getLogoId() + "_" + productListModel.getLogoName() + AppConstants
//                    .STR_FILE_SERVICE_TOKEN + SessionManager
//                    .getToken(mActivity)).into(((CustomViewHolder) holder).mImageRoundProduct);
            } else {
                mRelativeImageWithText.setVisibility(View.VISIBLE);
                mRelativeImageWithoutText.setVisibility(View.GONE);
                mImageUserProfilePicWithText.setImageDrawable(Common.setLabeledImageView(mActivity, mFirstName, mLastName));
            }

            mEditFirstName.setText(mFirstName);
            mEditLastName.setText(mLastName);
            mEditMobile.setText(mMobile);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
