package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.R;
import com.trackopd.model.AddPatientModel;
import com.trackopd.model.CouncillorModel;
import com.trackopd.model.DoctorModel;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.KeyboardUtility;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddInquiryFormFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private EditText mEditDate, mEditCouncillorFor, mEditRecommendedBy, mEditRemarks, mEditAdmissionDate;
    private Spinner mSpinnerCouncillor, mSpinnerDoctor, mSpinnerEye, mSpinnerOptionSuggested,
            mSpinnerPatietPreferredOption;
    private TextView mTextPatientName, mTextPatientMobile;
    private ImageView mImageUserProfilePic, mImageCouncillor, mImageDoctor, mImageEye, mImageOptionSuggested,
            mImagePatietPreferredOption;
    private Button mButtonSubmit;
    private String mFirstName, mLastName, mMobileNo, mPatientId, mRecommendedBy, mCouncillorFor,mEyeID="",
            mSurgeryTypeID="",mDiseaseExplained="",mSurgeryExplained="",mMediclaim="",mPatientProfile="",
            mDoctorId="" ;
    private AutoCompleteTextView mAutoCompletedSurgeryType;

    private int mSelectedCouncillorIndex, mCouncillorId,mSelectedDoctorIndex;
    private RadioGroup mRadioGroupDiseaseExplained, mRadioSurgeryExplained, mRadioGroupMedicaid;
    private ArrayList<String> mArrDoctor, mArrCouncillor, mArrSurgeryTypeName,mArrEyeName,mArrOptionName;
    private ArrayList<DoctorModel> mArrDoctorList;
    private ArrayList<CouncillorModel> mArrCouncillorList;
    public ArrayList<PreliminaryExaminationDetailsModel.Data.SurgeryType> mArrSurgeryType;
    public ArrayList<PreliminaryExaminationDetailsModel.Data.Eye> mArrEye;
    public ArrayList<PreliminaryExaminationDetailsModel.Data.OptionSuggested> mArrOptionList;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_add_inquiry_form, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrCouncillor = new ArrayList<>();
        mArrCouncillorList = new ArrayList<>();
        mArrDoctor = new ArrayList<>();
        mArrDoctorList = new ArrayList<>();
        mArrSurgeryTypeName = new ArrayList<>();
        mArrSurgeryType = new ArrayList<>();
        mArrEye=new ArrayList<>();
        mArrEyeName=new ArrayList<>();
        mArrOptionList=new ArrayList<>();
        mArrOptionName=new ArrayList<>();

        getBundle();
        getIds();
        setData();
        callToCouncillorAPI();
        callToDoctorAPI();
        callToPreliminaryExaminationAPI();
        setHasOptionsMenu(true);
        setRegListeners();
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
//                mAddFollowUp = bundle.getString(AppConstants.BUNDLE_ADD_PATIENT);
                mFirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mLastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mMobileNo = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mPatientProfile = bundle.getString(AppConstants.BUNDLE_PATIENT_PROFILE);
            }
            Common.insertLog("NEW PATIENT IDDDD::::> " + mPatientId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            //Edit Text
            mEditRemarks = mView.findViewById(R.id.edit_councillor_inquiry_form_remarks);
            mEditAdmissionDate = mView.findViewById(R.id.edit_councillor_inquiry_form_councillor_admission_date);

            //Auto Completed Text View
            mAutoCompletedSurgeryType = mView.findViewById(R.id.auto_complted_councillor_inquiry_form_surgery_type);

            // Text Views
            mTextPatientName = mView.findViewById(R.id.text_councillor_inquiry_form_patient_name);
            mTextPatientMobile = mView.findViewById(R.id.text_councillor_inquiry_form_patient_mobile_no);

            //Spinner
            mSpinnerEye = mView.findViewById(R.id.spinner_councillor_inquiry_form_surgery_eye);
            mSpinnerDoctor = mView.findViewById(R.id.spinner_councillor_inquiry_form_doctor);
            mSpinnerOptionSuggested = mView.findViewById(R.id.spinner_councillor_inquiry_form_option_suggested);
            mSpinnerPatietPreferredOption = mView.findViewById(R.id.spinner_councillor_inquiry_form_patient_preferred_option);

            // Image Views
            mImageUserProfilePic = mView.findViewById(R.id.image_councillor_inquiry_form_patient_profile_pic);
            mImageEye =mView.findViewById(R.id.image_councillor_inquiry_form_surgery_eye);
            mImageDoctor= mView.findViewById(R.id.image_councillor_inquiry_form_doctor);
            mImageOptionSuggested = mView.findViewById(R.id.image_councillor_inquiry_form_option_suggested);
            mImagePatietPreferredOption = mView.findViewById(R.id.image_councillor_inquiry_form_patient_preferred_option);
            //mImageCouncillor = mView.findViewById(R.id.image_councillor_inquiry_form_councillor);

            //Radio Group
            mRadioSurgeryExplained = mView.findViewById(R.id.radio_group_add_inquiry_form_surgery_explained);
            mRadioGroupMedicaid = mView.findViewById(R.id.radio_group_add_inquiry_form_medicaid);
            mRadioGroupDiseaseExplained = mView.findViewById(R.id.radio_group_add_inquiry_form_disease_explained);

            // Buttons
            mButtonSubmit = mView.findViewById(R.id.button_councillor_inquiry_form_submit);

            // Set Request Focus
            mEditCouncillorFor.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Click Listeners
            mButtonSubmit.setOnClickListener(clickListener);
            mEditAdmissionDate.setOnClickListener(clickListener);

            // ToDo: Councillor - Spinner Item Select Listener
            //mSpinnerCouncillor.setOnItemSelectedListener(onItemSelectedListener);
            mSpinnerEye.setOnItemSelectedListener(onItemSelectedListener);
            mSpinnerDoctor.setOnItemSelectedListener(onItemSelectedListener);
            mSpinnerPatietPreferredOption.setOnItemSelectedListener(onItemSelectedListener);
            mSpinnerOptionSuggested.setOnItemSelectedListener(onItemSelectedListener);

            mAutoCompletedSurgeryType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    mSurgeryTypeID= mArrSurgeryType.get(position).getSurgeryTypeID();
                }
            });

            mRadioGroupDiseaseExplained.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    int selAppointmentType = group.getCheckedRadioButtonId();
                    if (selAppointmentType == R.id.radio_button_add_inquiry_from_disease_explained_yes) {
                        mDiseaseExplained=mActivity.getResources().getString(R.string.text_yes);

                    } else {
                        mDiseaseExplained=mActivity.getResources().getString(R.string.text_no);
                    }
                }
            });

            mRadioSurgeryExplained.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    int selAppointmentType = group.getCheckedRadioButtonId();
                    if (selAppointmentType == R.id.radio_button_add_inquiry_form_surgery_explained_yes) {
                        mSurgeryExplained=mActivity.getResources().getString(R.string.text_yes);
                    } else {
                        mSurgeryExplained=mActivity.getResources().getString(R.string.text_no);
                    }
                }
            });

            mRadioGroupMedicaid.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    int selAppointmentType = group.getCheckedRadioButtonId();
                    if (selAppointmentType == R.id.radio_button_add_inquiry_form_medicaid_yes) {
                        mMediclaim=mActivity.getResources().getString(R.string.text_yes);
                    } else {
                        mMediclaim=mActivity.getResources().getString(R.string.text_no);
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            mTextPatientName.setText(mFirstName + AppConstants.STR_EMPTY_SPACE + mLastName);
            mTextPatientMobile.setText(mMobileNo);

            if (!mPatientProfile.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                //Set Thumbnail
                String thumbmail_url= WebFields.API_BASE_URL+WebFields.IMAGE_THUMBNAIL_URL+mPatientProfile;
                Glide.with(mActivity)
                        .load(thumbmail_url)
                        .apply(new RequestOptions().error(R.drawable.ic_documents_img).placeholder(R.drawable.ic_documents_img))
                        .into(mImageUserProfilePic);
            }else {
                mImageUserProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, mFirstName, mLastName));
            }

            // Set current date
            mEditAdmissionDate.setText(Common.setCurrentDateTime(mActivity));

            // Set Image Color as per the theme applied
           // mImageCouncillor.setColorFilter(Common.setThemeColor(mActivity));

            // Set Radio Button
            mDiseaseExplained=mActivity.getResources().getString(R.string.text_yes);
            mSurgeryExplained=mActivity.getResources().getString(R.string.text_yes);
            mMediclaim=mActivity.getResources().getString(R.string.text_yes);

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerEye.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerOptionSuggested.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerPatietPreferredOption.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerDoctor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerCouncillor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

            //ToDo: Set Theme color image
            mImagePatietPreferredOption.setColorFilter(Common.setThemeColor(mActivity));
            mImageEye.setColorFilter(Common.setThemeColor(mActivity));
            mImageOptionSuggested.setColorFilter(Common.setThemeColor(mActivity));
            mImageDoctor.setColorFilter(Common.setThemeColor(mActivity));
            mImageCouncillor.setColorFilter(Common.setThemeColor(mActivity));



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.edit_councillor_inquiry_form_councillor_admission_date:
                    Common.openDateTimePicker(mActivity, mEditDate);
                    break;

                case R.id.button_councillor_inquiry_form_submit:
                    doAddInquiryForm();
                    break;
            }
        }
    };

    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinner_councillor_inquiry_form_doctor:
                    mSelectedDoctorIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedDoctorIndex != 0) {
                        mDoctorId = mArrDoctorList.get(mSelectedDoctorIndex).getDoctorID();
                    }
                    break;
                case R.id.spinner_councillor_inquiry_form_surgery_eye:
                   int mSelectedEyeIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedEyeIndex != 0) {
                        mEyeID = mArrEye.get(mSelectedEyeIndex).getEyeID();
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    /**
     * Sets up the data to the councillor spinner
     */
    private void callToCouncillorAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setCouncillorListJson(hospital_database));

            Call<CouncillorModel> call = RetrofitClient.createService(ApiInterface.class).getCouncillorList(body);
            call.enqueue(new Callback<CouncillorModel>() {
                @Override
                public void onResponse(@NonNull Call<CouncillorModel> call, @NonNull Response<CouncillorModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrCouncillorList.addAll(response.body().getData());
                            }
                            setsCouncillorAdapter();
                        } else {
                            setsCouncillorAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CouncillorModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setsCouncillorAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsCouncillorAdapter() {
        try {
            mArrCouncillor.add(0, getString(R.string.spinner_select_councillor));
            if (mArrCouncillorList.size() > 0) {
                for (CouncillorModel councillorModel : mArrCouncillorList) {
                    mArrCouncillor.add(councillorModel.getCouncillorName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            //mSpinnerCouncillor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_dropdown_item, mArrCouncillor) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCouncillorIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerCouncillor.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method checks the validation first then call the API
     */
    private void doAddInquiryForm() {
        KeyboardUtility.HideKeyboard(mActivity, mEditCouncillorFor);
        if (checkValidation()) {
            callToAddPatientAPI();
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {
        boolean status = true;

        int index_option=mSpinnerOptionSuggested.getSelectedItemPosition();
        int index_patientPreferredOption=mSpinnerPatietPreferredOption.getSelectedItemPosition();
        int index_eye=mSpinnerEye.getSelectedItemPosition();
        int index_doctor=mSpinnerDoctor.getSelectedItemPosition();

        if (index_eye==0){
            Common.setCustomToast(mActivity,mActivity.getResources().getString(
                    R.string.error_select_eye));
            status=false;
            return status;
        }

        if (index_doctor==0){
            Common.setCustomToast(mActivity,mActivity.getResources().getString(
                    R.string.error_select_doctor));
            status=false;
            return status;
        }

        if (index_option==0){
            Common.setCustomToast(mActivity,mActivity.getResources().getString(
                    R.string.error_select_option_suggested));
            status=false;
            return status;
        }
        if (index_patientPreferredOption==0){
            Common.setCustomToast(mActivity,mActivity.getResources().getString(
                    R.string.error_select_preferred_option));
            status=false;
            return status;
        }

        if (mAutoCompletedSurgeryType.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
            mAutoCompletedSurgeryType.setError(mActivity.getResources().getString(R.string.error_field_required));
            status=false;
        }

        /*mRecommendedBy = mEditRecommendedBy.getText().toString().trim();
        mCouncillorFor = mEditCouncillorFor.getText().toString().trim();

        mEditRecommendedBy.setError(null);
        mEditCouncillorFor.setError(null);

        if (TextUtils.isEmpty(mRecommendedBy)) {
            mEditRecommendedBy.requestFocus();
            mEditRecommendedBy.setError(getResources().getString(R.string.error_field_required));
            status = false;
        }
        if (TextUtils.isEmpty(mCouncillorFor)) {
            mEditCouncillorFor.requestFocus();
            mEditCouncillorFor.setError(getResources().getString(R.string.error_field_required));
            status = false;
        }

        if (mSpinnerCouncillor.getSelectedItemPosition() == 0) {
            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.val_select_councillor));
            status = false;
        }*/
        return status;
    }

    /**
     * This method should call for Councillor Add Inquiry Form API
     */
    private void callToAddPatientAPI() {
        try {
            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            String mRemarks = mEditRemarks.getText().toString().trim();

            String today=Common.setCurrentDate(mActivity);
            String councillor_date=Common.convertDateUsingDateFormat(mActivity,
                   today,mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                    mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));


            String strAdmissionDate=mEditAdmissionDate.getText().toString();
            String admission_date=Common.convertDateUsingDateFormat(mActivity,strAdmissionDate,
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss),
                    mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));

            String admission_time=Common.convertDateUsingDateFormat(mActivity,strAdmissionDate,
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss),
                    mActivity.getResources().getString(R.string.date_format_hh_mm_ss));


            int index_option_suggested=mSpinnerOptionSuggested.getSelectedItemPosition();
            int index_option_patient_preferred=mSpinnerPatietPreferredOption.getSelectedItemPosition();


            String OpetionSuggestedID=mArrOptionList.get(index_option_suggested-1).getOpetionSuggestedID();
            String PatientPreferredOptionID=mArrOptionList.get(index_option_patient_preferred-1).getOpetionSuggestedID();


            RequestBody requestBody=RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAddCouncillorFormJson(mPatientId,mUserId,councillor_date,
                            mEyeID,mSurgeryTypeID, mDoctorId,mDiseaseExplained,mSurgeryExplained,OpetionSuggestedID,PatientPreferredOptionID,mMediclaim,admission_date,
                            admission_time,mRemarks,mUserId,mDatabaseName, AddNewPreliminaryExaminationFragment.PreliminaryExminationId));

            Call<AddPatientModel> call = RetrofitClient.createService(ApiInterface.class).addInquiryForm(requestBody);
            call.enqueue(new Callback<AddPatientModel>() {
                @Override
                public void onResponse(@NonNull Call<AddPatientModel> call, @NonNull Response<AddPatientModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                Common.setCustomToast(mActivity, mMessage);
                                redirectedToCouncillorInquiryFormFragment();
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddPatientModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method redirected you to the Councillor Inquiry Form Fragment
     */
    private void redirectedToCouncillorInquiryFormFragment() {
        try {
            Fragment fragment = new CouncillorInquiryFormFragment();
            Bundle args = new Bundle();
            ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.header_counselling));
            args.putString(AppConstants.BUNDLE_PATIENT_NAME, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_COUNCILLOR_DATE, "1970-01-01");
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_COUNCILLOR, -1);
            fragment.setArguments(args);
            FragmentManager fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.header_counselling))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Doctor API
     */
    private void callToDoctorAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setDoctorListJson(hospital_database));

            Call<DoctorModel> call = RetrofitClient.createService(ApiInterface.class).getDoctorList(body);
            call.enqueue(new Callback<DoctorModel>() {
                @Override
                public void onResponse(@NonNull Call<DoctorModel> call, @NonNull Response<DoctorModel> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrDoctorList.addAll(response.body().getData());
                            }
                            setsDoctorAdapter();
                        } else {
                            setsDoctorAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DoctorModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setsDoctorAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsDoctorAdapter() {
        try {
            mArrDoctor.add(0, mActivity.getResources().getString(R.string.spinner_select_doctor));
            if (mArrDoctorList.size() > 0) {
                for (DoctorModel doctorModel : mArrDoctorList) {
                    mArrDoctor.add(doctorModel.getDoctorName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerDoctor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrDoctor) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedDoctorIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerDoctor.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Preliminary Examination API
     */
    private void callToPreliminaryExaminationAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPreliminaryExaminationDetailsJson(hospital_database));

            Call<PreliminaryExaminationDetailsModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationDetails(body);
            call.enqueue(new Callback<PreliminaryExaminationDetailsModel>() {
                @Override
                public void onResponse(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Response<PreliminaryExaminationDetailsModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()
                                && response.body().getError() == 200) {
                            {
                                List<PreliminaryExaminationDetailsModel.Data.SurgeryType>
                                        mListSurgery = response.body().getData().getSurgeryType();

                                mArrSurgeryType.addAll(mListSurgery);
                                setSurgeryTypeAdapter();

                                List<PreliminaryExaminationDetailsModel.Data.Eye>
                                        mListEye = response.body().getData().getEye();

                                mArrEye.addAll(mListEye);
                                setEyeSpinnerAdapter();

                                // Option List
                                List<PreliminaryExaminationDetailsModel.Data.OptionSuggested>
                                        mListOption = response.body().getData().getOptionSuggested();

                                mArrOptionList.addAll(mListOption);
                                setOptionAdapter();

                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *  Set Adapter for Option suggested or Patient Option
     */
    private void setOptionAdapter() {
        try {
            mArrOptionName.add(0, mActivity.getResources().getString(R.string.spinner_select_option));
            if (mArrOptionList.size() > 0) {
                for (PreliminaryExaminationDetailsModel.Data.OptionSuggested optionModel : mArrOptionList) {
                    mArrOptionName.add(optionModel.getOpetionSuggested());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerOptionSuggested.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrOptionName) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedDoctorIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerOptionSuggested.setAdapter(adapter);

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerPatietPreferredOption.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter patient_preferred_adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrOptionName) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedDoctorIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            patient_preferred_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerPatietPreferredOption.setAdapter(patient_preferred_adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This is bind Eye Spinner Send
     */
    private void setEyeSpinnerAdapter() {
        try {
            mArrEyeName.add(0, mActivity.getResources().getString(R.string.spinner_select_eye));
            if (mArrEye.size() > 0) {
                for (PreliminaryExaminationDetailsModel.Data.Eye eyeModel : mArrEye) {
                    mArrEyeName.add(eyeModel.getEye());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerEye.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrEyeName) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedDoctorIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerEye.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setSurgeryTypeAdapter() {
        try{
            if (mArrSurgeryType!=null &&
                    mArrSurgeryType.size()>0){
                for (int i=0;i<mArrSurgeryType.size();i++){
                    mArrSurgeryTypeName.add(mArrSurgeryType.get(i).getSurgeryName());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>
                        (mActivity,android.R.layout.simple_selectable_list_item, mArrSurgeryTypeName);
                mAutoCompletedSurgeryType.setThreshold(2);
                mAutoCompletedSurgeryType.setAdapter(adapter);
            }
        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }
}
