package com.trackopd.fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.trackopd.R;
import com.trackopd.adapter.HistoryDiagnosisAdapter;
import com.trackopd.model.AddPreliminaryExaminationModel;
import com.trackopd.model.AnatomicalLocationModel;
import com.trackopd.model.CloseFileModel;
import com.trackopd.model.DiseaseModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.KeyboardUtility;
import com.trackopd.utils.SessionManager;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DiagnosisFragment extends Fragment {


    public static ArrayList<HistoryPreliminaryExaminationModel.Data> mArrHistory;
    public static int pageIndex;
    private static LinearLayout mLinearDiagnosis;
    private static RelativeLayout mRelativeNoDataFound;
    public ArrayList<PreliminaryExaminationDetailsModel.Data.Category> mArrCategoryList;
    public ArrayList<PreliminaryExaminationDetailsModel.Data.Eye> mArrEyeList;
    public ArrayList<PreliminaryExaminationDetailsModel.Data.EyeLence> mArrEyeId;
    public ArrayList<PreliminaryExaminationDetailsModel.Data.PatternType> mArrPatternType;
    public ArrayList<PreliminaryExaminationDetailsModel.Data.Stage> mArrStage;
    public ArrayList<PreliminaryExaminationDetailsModel.Data.SideOfBrain> mArrSideBrain;
    public ArrayList<PreliminaryExaminationDetailsModel.Data.CataractType> mArrCataractType;
    public PreliminaryExaminationDetailsModel mPreliminaryExaminationData;
    public ArrayList<AnatomicalLocationModel> mArrAnatomicalLocation, mArrTempAnatomical;
    public ArrayList<DiseaseModel> mArrDiseaseList, mArrTempDieseaseList;
    public ArrayList<String> mArrCategoryName, mArrEyeName, mArrEyeIdName, mArrPatternTypeName,
            mArrStageName, mArrSideBrainName, mArrCataractTypeName, mArrAnatomicalName, mArrDisease;
    boolean mUserVisibleHint = true, isSelect = false;
    private View mView;
    private Activity mActivity;
    private ImageView mImageAddDiagnosis;
    private Button mButtonSubmit;
    private String hospital_database;
    private boolean isVisible = false, isStarted = false;
    private int mAppointmentId, mCurrentPage = 1, mHistoryDoctorID = -1;
    private String mPatientId, mDoctorId, mTreatmentDate;
    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_diagnosis_add_container:
                    addDiagnosisContainer();
                    break;
                case R.id.button_add_diagnosis_submit:
                    SessionManager manager = new SessionManager(mActivity);
                    String closeFile = manager.getPreferences("ClosingFlags", "");

                    if (closeFile.equalsIgnoreCase("Yes")) {
                        mButtonSubmit.setClickable(false);
                        Toast.makeText(mActivity, "Patient File Closed", Toast.LENGTH_SHORT).show();
                    } else {
                        if (checkValidation()) {
                            callToAddDiagnosis();
                        }
                    }
                    break;
            }
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_diagnosis, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;

        mUserVisibleHint = false;

        mActivity = getActivity();

        mArrCategoryList = new ArrayList<>();
        mArrEyeList = new ArrayList<>();
        mArrEyeId = new ArrayList<>();
        mArrPatternType = new ArrayList<>();
        mArrStage = new ArrayList<>();
        mArrSideBrain = new ArrayList<>();
        mArrCataractType = new ArrayList<>();
        mArrAnatomicalLocation = new ArrayList<>();
        mArrDiseaseList = new ArrayList<>();
        mArrHistory = new ArrayList<>();
        mArrTempAnatomical = new ArrayList<>();
        mArrDiseaseList = new ArrayList<>();

        mArrCategoryName = new ArrayList<>();
        mArrEyeName = new ArrayList<>();
        mArrEyeIdName = new ArrayList<>();
        mArrPatternTypeName = new ArrayList<>();
        mArrStageName = new ArrayList<>();
        mArrSideBrainName = new ArrayList<>();
        mArrCataractTypeName = new ArrayList<>();
        mArrAnatomicalName = new ArrayList<>();
        mArrDisease = new ArrayList<>();


        mPreliminaryExaminationData = new PreliminaryExaminationDetailsModel();

        hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);
        getBundle();
        getIds();
        setRegListeners();
        setData();
        callToPreliminaryExaminationAPI();
        callToDiseaseAPI();
        callToAnatomicalLocation();

        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mAppointmentId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mDoctorId = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);
                mTreatmentDate = bundle.getString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Declare IDs
     */
    private void getIds() {
        try {
            mImageAddDiagnosis = mView.findViewById(R.id.image_diagnosis_add_container);
            mLinearDiagnosis = mView.findViewById(R.id.linear_diagnosis_container);

            //Relative Layout
            mRelativeNoDataFound = mView.findViewById(R.id.relative_diagnosis_no_data_found);

            //Button
            mButtonSubmit = mView.findViewById(R.id.button_add_diagnosis_submit);

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Set Data Value
     */
    private void setData() {

        String currentDate = Common.setCurrentDate(mActivity);
        String covertTreatmentDate = Common.convertDateUsingDateFormat(mActivity, mTreatmentDate,
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen),
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));

        if (!currentDate.equals(covertTreatmentDate)) {
            mRelativeNoDataFound.setVisibility(View.VISIBLE);
        } else {
            mRelativeNoDataFound.setVisibility(View.GONE);
        }

//        if (mLinearDiagnosis.getChildCount() > 0) {
//            mLinearDiagnosis.removeAllViews();
//        }
    }

    /**
     * Set Register Listener
     */
    private void setRegListeners() {
        mImageAddDiagnosis.setOnClickListener(clickListener);
        mButtonSubmit.setOnClickListener(clickListener);

    }

    private boolean checkValidation() {
        boolean status = true;

        if (mLinearDiagnosis.getChildCount() > 0) {

            for (int i = 0; i < mLinearDiagnosis.getChildCount(); i++) {
                View view = mLinearDiagnosis.getChildAt(i);

                Spinner spinner_category = view.findViewById(R.id.spinner_row_add_diagnosis_category);
                Spinner spinner_anatomical = view.findViewById(R.id.spinner_row_add_diagnosis_anatomical_location);
                Spinner spinner_eye = view.findViewById(R.id.spinner_row_add_diagnosis_eye);
                Spinner spinner_disease = view.findViewById(R.id.spinner_row_add_diagnosis_disease);

                int index_category = spinner_category.getSelectedItemPosition();
                int index_anatomical = spinner_anatomical.getSelectedItemPosition();
                int index_eye = spinner_eye.getSelectedItemPosition();
                int index_disease = spinner_disease.getSelectedItemPosition();


                if (index_category == 0) {
                    Common.setCustomToast(mActivity, getString(R.string.error_select_category));
                    Toast.makeText(mActivity, "", Toast.LENGTH_SHORT).show();
                    status = false;
                } else if (index_anatomical == 0) {
                    Common.setCustomToast(mActivity, getString(R.string.error_anatomical_location));
                    status = false;
                } else if (index_eye == 0) {
                    Common.setCustomToast(mActivity, getString(R.string.error_select_eye));
                    status = false;
                } else if (index_disease == 0) {
                    Common.setCustomToast(mActivity, getString(R.string.error_select_disease));
                    status = false;
                }
            }
        } else {
            Common.setCustomToast(mActivity, mActivity.getString(R.string.edit_add_diagnosis));
            status = false;
        }

        return status;
    }

    /**
     *
     */
    private void callToAddDiagnosis() {
        try {

            String preliminaryExaminationId = AddNewPreliminaryExaminationFragment.getPrelimary();

            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
            JSONArray jsonArray = getArrayDiagnosis();
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.getAddPreliminaryDiagnosis(preliminaryExaminationId, mUserId, jsonArray, hospital_database));

            Common.insertLog("Dignosis.." + body.toString());

            Call<AddPreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).addPreExamination(body);
            call.enqueue(new Callback<AddPreliminaryExaminationModel>() {
                @Override
                public void onResponse(@NonNull Call<AddPreliminaryExaminationModel> call, @NonNull Response<AddPreliminaryExaminationModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            Common.setCustomToast(mActivity, response.body().getMessage());
                            if (mArrHistory.size() > 0) {
                                mArrHistory.clear();
                            }
                            callToPreliminarHistoryDiagnosis();

                            AddNewPreliminaryExaminationFragment.changePage(4);

                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddPreliminaryExaminationModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @return - JSON Array
     */
    private JSONArray getArrayDiagnosis() {
        JSONArray jsonArray = new JSONArray();
        try {
            if (mLinearDiagnosis.getChildCount() > 0) {
                for (int a = 0; a < mLinearDiagnosis.getChildCount(); a++) {
                    View child_view = mLinearDiagnosis.getChildAt(a);

                    Spinner spinner_category = child_view.findViewById(R.id.spinner_row_add_diagnosis_category);
                    Spinner spinner_eye = child_view.findViewById(R.id.spinner_row_add_diagnosis_eye);
                    Spinner spinner_disease = child_view.findViewById(R.id.spinner_row_add_diagnosis_disease);
                    Spinner spinner_eye_id = child_view.findViewById(R.id.spinner_row_add_diagnosis_eye_id);
                    Spinner spinner_cataract = child_view.findViewById(R.id.spinner_row_add_diagnosis_cataract_type);
                    Spinner spinner_stage = child_view.findViewById(R.id.spinner_row_add_diagnosis_stage);
                    Spinner spinner_side_of_brain = child_view.findViewById(R.id.spinner_row_add_diagnosis_side_of_brain);
                    Spinner spinner_pattern = child_view.findViewById(R.id.spinner_row_add_diagnosis_pattern_type);
                    Spinner spinner_anatomical = child_view.findViewById(R.id.spinner_row_add_diagnosis_anatomical_location);

                    String CategoryID = "0", eye = "0", eye_id = "0", cataract = "0", side_of_brain = "0",
                            stage = "0", pattern = "0", anatomicalId = "0", diesesId = "0";

                    int index_category = spinner_category.getSelectedItemPosition();
                    if (index_category > 0)
                        CategoryID = mArrCategoryList.get(index_category - 1).getCategoryID();

                    int index_eye = spinner_eye.getSelectedItemPosition();
                    if (index_eye > 0)
                        eye = mArrEyeList.get(index_eye - 1).getEyeID();

                    int index_eyeid = spinner_eye_id.getSelectedItemPosition();
                    if (index_eyeid > 0)
                        eye_id = mArrEyeId.get(index_eyeid - 1).getEyeLenseID();

                    int index_cataract = spinner_cataract.getSelectedItemPosition();
                    if (index_cataract > 0)
                        cataract = mArrCataractType.get(index_cataract - 1).getCataracttypeID();

                    int index_sideofbrain = spinner_side_of_brain.getSelectedItemPosition();
                    if (index_sideofbrain > 0)
                        side_of_brain = mArrSideBrain.get(index_sideofbrain - 1).getSideofbrainID();

                    int index_stage = spinner_stage.getSelectedItemPosition();
                    if (index_stage > 0)
                        stage = mArrStage.get(index_stage - 1).getStageID();

                    int index_pattern = spinner_pattern.getSelectedItemPosition();
                    if (index_pattern > 0)
                        pattern = mArrPatternType.get(index_pattern - 1).getPatterntypeID();

                    int index_anatomical = spinner_anatomical.getSelectedItemPosition();
                    if (index_anatomical > 0)
                        anatomicalId = mArrAnatomicalLocation.get(index_anatomical - 1).getAnatomicalLocationID();

                    int index_disease = spinner_disease.getSelectedItemPosition();
                    if (index_disease > 0)
                        diesesId = mArrDiseaseList.get(index_disease - 1).getDiseaseID();

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("CategoryID", CategoryID);
                    jsonObject.put("AnatomicalLocationID", anatomicalId);
                    jsonObject.put("EyeID", eye);
                    jsonObject.put("DiseaseID", diesesId);
                    jsonObject.put("EyeLenseID", eye_id);
                    jsonObject.put("CataracttypeID", cataract);
                    jsonObject.put("StageID", stage);
                    jsonObject.put("SideofbrainID", side_of_brain);
                    jsonObject.put("PatterntypeID", pattern);

                    jsonArray.put(jsonObject);
                }
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
        return jsonArray;
    }

    /**
     * Add Linear Layout DiagnosisModel
     */
    private void addDiagnosisContainer() {

        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(mActivity.getBaseContext()).
                inflate(R.layout.row_add_diagnosis_item, null, false);

        Spinner spinner_category = view.findViewById(R.id.spinner_row_add_diagnosis_category);
        Spinner spinner_anatomical = view.findViewById(R.id.spinner_row_add_diagnosis_anatomical_location);
        Spinner spinner_eye = view.findViewById(R.id.spinner_row_add_diagnosis_eye);
        Spinner spinner_disease = view.findViewById(R.id.spinner_row_add_diagnosis_disease);
        Spinner spinner_eye_id = view.findViewById(R.id.spinner_row_add_diagnosis_eye_id);
        Spinner spinner_cataract = view.findViewById(R.id.spinner_row_add_diagnosis_cataract_type);
        Spinner spinner_stage = view.findViewById(R.id.spinner_row_add_diagnosis_stage);
        Spinner spinner_side_of_brain = view.findViewById(R.id.spinner_row_add_diagnosis_side_of_brain);
        Spinner spinner_pattern = view.findViewById(R.id.spinner_row_add_diagnosis_pattern_type);

        TextView textSelectedAnatomical = view.findViewById(R.id.text_row_add_diagnosis_selected_anatomical);
        TextView textSelectedDisease = view.findViewById(R.id.text_row_add_diagnosis_selected_disease);

        //set Text
        textSelectedAnatomical.setText("0");
        textSelectedDisease.setText("0");

        isSelect = true;

        //setAdapter
        bindCategoryAdaper(mActivity, spinner_category);
        bindEyeAdapter(mActivity, spinner_eye);
        bindEyeIDAdapter(mActivity, spinner_eye_id);
        bindPatternAdapter(mActivity, spinner_pattern);
        bindStageAdapter(mActivity, spinner_stage);
        bindSideOfBrianAdapter(mActivity, spinner_side_of_brain);
        bindCataractAdapter(mActivity, spinner_cataract);
        setAnatomicalLocationAdapter(spinner_anatomical);
        setDiseaseAdapterData(spinner_disease);


        // ToDo: Sets the spinner color as per the theme applied
        spinner_category.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        spinner_anatomical.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        spinner_eye.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        spinner_disease.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        spinner_eye_id.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        spinner_cataract.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        spinner_stage.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        spinner_side_of_brain.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        spinner_pattern.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);


        ImageView mImageRemove = view.findViewById(R.id.image_row_add_diagnosis_remove);
        mImageRemove.setOnClickListener(v -> mLinearDiagnosis.removeView(view));

        spinner_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtility.HideKeyboard(mActivity, spinner_category);
                if (position > 0) {
                    SelectAnatomical(mArrCategoryList.get(position - 1), spinner_anatomical);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_anatomical.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtility.HideKeyboard(mActivity, spinner_category);
                if (position > 0) {
                    SelectCategory(mArrAnatomicalLocation.get(position - 1), spinner_category);
                    SelectDieseasData(mArrAnatomicalLocation.get(position - 1), spinner_disease);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_disease.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtility.HideKeyboard(mActivity, spinner_disease);
                if (position > 0) {
                    SelectDiseaseAnatomical(mArrDiseaseList.get(position - 1), spinner_anatomical, spinner_disease);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_eye.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtility.HideKeyboard(mActivity, spinner_eye);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_eye_id.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtility.HideKeyboard(mActivity, spinner_eye_id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_cataract.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtility.HideKeyboard(mActivity, spinner_category);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_stage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtility.HideKeyboard(mActivity, spinner_category);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_side_of_brain.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtility.HideKeyboard(mActivity, spinner_category);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_pattern.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtility.HideKeyboard(mActivity, spinner_category);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mLinearDiagnosis.addView(view);
    }


    /**
     * Set Selected  Deiseas  Anatomical Location
     *
     * @param anatomicalLocationModel - Anatomical
     * @param spinner_anatomical      - Spinner Anatomical
     * @param spinner_disease         - Anatomical
     */
    private void SelectDiseaseAnatomical(DiseaseModel anatomicalLocationModel, Spinner spinner_anatomical,
                                         Spinner spinner_disease) {

        int index_antomical = spinner_anatomical.getSelectedItemPosition();
        if (index_antomical == 0) {
            for (int a = 0; a < mArrAnatomicalLocation.size(); a++) {
                if (mArrAnatomicalLocation.get(a).getAnatomicalLocationID().
                        equals(anatomicalLocationModel.getAnatomicalLocationID())) {
                    int index = a + 1;
                    spinner_anatomical.setSelection(index, true);
                }
            }
        } else {
            String id = mArrAnatomicalLocation.get(index_antomical - 1).getAnatomicalLocationID();
            if (!id.equalsIgnoreCase(anatomicalLocationModel.getAnatomicalLocationID())) {
                for (int a = 0; a < mArrAnatomicalLocation.size(); a++) {
                    if (mArrAnatomicalLocation.get(a).getAnatomicalLocationID().
                            equals(anatomicalLocationModel.getAnatomicalLocationID())) {
                        int index = a + 1;
                        spinner_anatomical.setSelection(index, true);
                    }
                }
            }
        }
    }

    /**
     * Set Select Disease to set Category or Anatomical
     */
    private void SelectDieseas(DiseaseModel diseaseModel, Spinner spinner_anatomical, Spinner spinner_category) {

        if (mArrAnatomicalLocation.size() > 0) {
            for (int a = 0; a < mArrAnatomicalLocation.size(); a++) {
                if (mArrAnatomicalLocation.get(a).getAnatomicalLocationID().equals(diseaseModel.getAnatomicalLocationID())) {
                    //int index=a+1;
                    //spinner_anatomical.setSelection(index,false);
                    SelectCategory(mArrAnatomicalLocation.get(a), spinner_category);
                }
            }
        }
    }

    /**
     * Set Select Category to set Anatomical and Diesease
     *
     * @param category
     * @param spinner_anatomical
     */
    private void SelectAnatomical(PreliminaryExaminationDetailsModel.Data.Category category,
                                  Spinner spinner_anatomical) {


        int seleted_anatomical = spinner_anatomical.getSelectedItemPosition();

        if (seleted_anatomical == 0) {
            if (mArrAnatomicalLocation.size() > 0) {
                for (int a = 0; a < mArrAnatomicalLocation.size(); a++) {
                    if (mArrAnatomicalLocation.get(a).getCategoryID().equals
                            (category.getCategoryID())) {
                        int index = a + 1;
                        spinner_anatomical.setSelection(index, true);
                    }
                }
            }
        } else {
            String category_id = mArrAnatomicalLocation.get(seleted_anatomical - 1).getCategoryID();

            if (!category_id.equalsIgnoreCase(category.getCategoryID())) {
                if (mArrAnatomicalLocation.size() > 0) {
                    for (int a = 0; a < mArrAnatomicalLocation.size(); a++) {
                        if (mArrAnatomicalLocation.get(a).getCategoryID().equals
                                (category.getCategoryID())) {
                            int index = a + 1;
                            spinner_anatomical.setSelection(index, true);
                        }
                    }
                }
            }
        }


    }

    /**
     * Set Selecetd  Anatomical Location
     *
     * @param anatomicalLocationModel
     * @param spinner_disease
     */
    private void SelectDieseasData(AnatomicalLocationModel anatomicalLocationModel,
                                   Spinner spinner_disease) {

        int index_dieseadata = spinner_disease.getSelectedItemPosition();
        if (index_dieseadata == 0) {
            for (int a = 0; a < mArrDiseaseList.size(); a++) {
                if (mArrDiseaseList.get(a).getAnatomicalLocationID()
                        .equalsIgnoreCase(anatomicalLocationModel.getAnatomicalLocationID())) {
                    int selected = a + 1;
                    spinner_disease.setSelection(selected, true);
                }
            }
        } else {
            String id = mArrDiseaseList.get(index_dieseadata - 1).getAnatomicalLocationID();
            if (!id.equalsIgnoreCase(anatomicalLocationModel.getAnatomicalLocationID())) {
                for (int a = 0; a < mArrDiseaseList.size(); a++) {
                    if (mArrDiseaseList.get(a).getAnatomicalLocationID()
                            .equalsIgnoreCase(anatomicalLocationModel.getAnatomicalLocationID())) {
                        int selected = a + 1;
                        spinner_disease.setSelection(selected, true);
                    }
                }
            }
        }
    }


    /**
     * Set Selecetd  Anatomical Location
     *
     * @param anatomicalLocationModel
     * @param spinner_category
     */
    private void SelectCategory(AnatomicalLocationModel anatomicalLocationModel,
                                Spinner spinner_category) {


        int index = spinner_category.getSelectedItemPosition();
        if (index == 0) {
            for (int a = 0; a < mArrCategoryList.size(); a++) {
                if (mArrCategoryList.get(a).getCategoryID().equals(anatomicalLocationModel.getCategoryID())) {
                    int _position = a + 1;
                    spinner_category.setSelection(_position, true);
                }
            }
        } else {
            String categoryid = mArrCategoryList.get(index - 1).getCategoryID();
            if (!categoryid.equalsIgnoreCase(anatomicalLocationModel.getCategoryID())) {
                for (int a = 0; a < mArrCategoryList.size(); a++) {
                    if (mArrCategoryList.get(a).getCategoryID().equals(anatomicalLocationModel.getCategoryID())) {
                        int position = a + 1;
                        spinner_category.setSelection(position, true);
                    }
                }
            }
        }
    }


    /**
     * Call Anatomical API set Spinner Data;
     */
    private void callToAnatomicalLocation() {
        try {

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.getAnatomicalLocation("-1", hospital_database));

            Call<AnatomicalLocationModel> call = RetrofitClient.createService(ApiInterface.class).getAnatomicalLocation(body);
            call.enqueue(new Callback<AnatomicalLocationModel>() {
                @Override
                public void onResponse(Call<AnatomicalLocationModel> call, Response<AnatomicalLocationModel> response) {
                    Common.insertLog("response Aatomical Location.." + response.body().toString());
                    if (response.isSuccessful() &&
                            response.body().getError() == 200) {
                        if (response.body().getData() != null
                                && response.body().getData().size() > 0) {
                            mArrAnatomicalLocation.addAll(response.body().getData());
                            callToPreliminarHistoryDiagnosis();
                        } else {
                            callToPreliminarHistoryDiagnosis();
                        }
                    }
                }

                @Override
                public void onFailure(Call<AnatomicalLocationModel> call, Throwable t) {
                    Common.insertLog("Failer Aatomical Location.." + t.getMessage());
                    callToPreliminarHistoryDiagnosis();
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * This method should call the Disease API
     */
    private void callToDiseaseAPI() {
        try {

            int anatomicalId = Integer.parseInt("-1");

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setDiseaseListJson(anatomicalId, hospital_database));

            Call<DiseaseModel> call = RetrofitClient.createService(ApiInterface.class).getDiseaseList(body);
            call.enqueue(new Callback<DiseaseModel>() {
                @Override
                public void onResponse(@NonNull Call<DiseaseModel> call, @NonNull Response<DiseaseModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        int mError = response.body().getError();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null && response.body().getData() != null) {
                                if (mArrDiseaseList.size() > 0) {
                                    mArrDiseaseList.clear();
                                }
                                mArrDiseaseList.addAll(response.body().getData());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DiseaseModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * This method should call the Preliminary Examination API
     */
    private void callToPreliminaryExaminationAPI() {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPreliminaryExaminationDetailsJson(hospital_database));

            Call<PreliminaryExaminationDetailsModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationDetails(body);
            call.enqueue(new Callback<PreliminaryExaminationDetailsModel>() {
                @Override
                public void onResponse(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Response<PreliminaryExaminationDetailsModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null) {

                                mPreliminaryExaminationData = response.body();

                                PreliminaryExaminationDetailsModel.Data data = response.body().getData();

                                // Category
                                mArrCategoryList.addAll(data.getCategory());


                                //Eye
                                if (data.getEye() != null
                                        && data.getEye().size() > 0) {

                                    mArrEyeList.addAll(response.body().getData().getEye());
                                }
                                //Eye Leance Id
                                if (data.getEyeLence() != null
                                        && data.getEyeLence().size() > 0) {

                                    mArrEyeId.addAll(data.getEyeLence());
                                }

                                //Pattern Type
                                if (data.getPatternType() != null
                                        && data.getPatternType().size() > 0) {
                                    mArrPatternType.addAll(data.getPatternType());
                                }

                                //Stage
                                if (data.getStage() != null
                                        && data.getStage().size() > 0) {
                                    mArrStage.addAll(data.getStage());
                                }
                                //Side Brain
                                if (data.getSideOfBrain() != null
                                        && data.getSideOfBrain().size() > 0) {
                                    mArrSideBrain.addAll(data.getSideOfBrain());
                                }

                                //CataractType
                                if (data.getCataractType() != null
                                        && data.getCataractType().size() > 0) {
                                    mArrCataractType.addAll(data.getCataractType());
                                }

                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Bind  Category Adapter
     *
     * @param mActivity        - Activity
     * @param spinner_category - Spinner
     */
    private void bindCategoryAdaper(Activity mActivity, Spinner spinner_category) {
        try {

            if (mArrCategoryName.size() > 0) {
                mArrCategoryName.clear();
            }

            if (mArrCategoryList != null && mArrCategoryList.size() > 0) {

                mArrCategoryName.add(mActivity.getResources().getString(R.string.spinner_select_category));

                for (int i = 0; i < mArrCategoryList.size(); i++) {
                    mArrCategoryName.add(mArrCategoryList.get(i).getCategory());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<>
                        (mActivity, android.R.layout.simple_selectable_list_item, mArrCategoryName);

                spinner_category.setAdapter(adapter);
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Bind  Category Adapter
     *
     * @param mActivity
     * @param spinner_eye - Spinner
     */
    private void bindEyeAdapter(Activity mActivity, Spinner spinner_eye) {
        try {
            if (mArrEyeName.size() > 0) {
                mArrEyeName.clear();
            }

            if (mArrEyeList != null && mArrEyeList.size() > 0) {
                mArrEyeName.add(mActivity.getResources().getString(R.string.spinner_select_eye));
                for (int i = 0; i < mArrEyeList.size(); i++) {
                    mArrEyeName.add(mArrEyeList.get(i).getEye());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>
                        (mActivity, android.R.layout.simple_selectable_list_item, mArrEyeName);

                spinner_eye.setAdapter(adapter);
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Bind  EyeLense Adapter
     *
     * @param mActivity
     * @param spinner_eyeID - Spinner
     */
    private void bindEyeIDAdapter(Activity mActivity, Spinner spinner_eyeID) {
        try {

            if (mArrEyeIdName.size() > 0) {
                mArrEyeIdName.clear();
            }

            if (mArrEyeId != null && mArrEyeId.size() > 0) {
                mArrEyeIdName.add(mActivity.getResources().getString(R.string.spinner_select_eye_Lid));
                for (int i = 0; i < mArrEyeId.size(); i++) {
                    mArrEyeIdName.add(mArrEyeId.get(i).getEyeLense());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>
                        (mActivity, android.R.layout.simple_selectable_list_item, mArrEyeIdName);

                spinner_eyeID.setAdapter(adapter);
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Bind Pattern Adapter
     *
     * @param mActivity
     * @param spinner_pattenr - Spinner
     */
    private void bindPatternAdapter(Activity mActivity, Spinner spinner_pattenr) {
        try {

            if (mArrPatternTypeName.size() > 0) {
                mArrPatternTypeName.clear();
            }

            if (mArrPatternType != null && mArrPatternType.size() > 0) {
                mArrPatternTypeName.add(mActivity.getResources().getString(R.string.spinner_select_pattern));
                for (int i = 0; i < mArrPatternType.size(); i++) {
                    mArrPatternTypeName.add(mArrPatternType.get(i).getPatterntype());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>
                        (mActivity, android.R.layout.simple_selectable_list_item, mArrPatternTypeName);

                spinner_pattenr.setAdapter(adapter);
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Bind  Stage Adapter
     *
     * @param mActivity
     * @param spinner_stage - Spinner
     */
    private void bindStageAdapter(Activity mActivity, Spinner spinner_stage) {
        try {

            if (mArrStageName.size() > 0) {
                mArrStageName.clear();
            }

            if (mArrStage != null && mArrStage.size() > 0) {
                mArrStageName.add(mActivity.getResources().getString(R.string.spinner_select_stage));
                for (int i = 0; i < mArrStage.size(); i++) {
                    mArrStageName.add(mArrStage.get(i).getStage());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>
                        (mActivity, android.R.layout.simple_selectable_list_item, mArrStageName);

                spinner_stage.setAdapter(adapter);
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Bind  Category Adapter
     *
     * @param mActivity
     * @param spinner_sidebrain - Spinner
     */
    private void bindSideOfBrianAdapter(Activity mActivity, Spinner spinner_sidebrain) {
        try {
            if (mArrSideBrainName.size() > 0) {
                mArrSideBrainName.clear();
            }

            if (mArrSideBrain != null && mArrSideBrain.size() > 0) {
                mArrSideBrainName.add(mActivity.getResources().getString(R.string.select_side_of_brain));
                for (int i = 0; i < mArrSideBrain.size(); i++) {
                    mArrSideBrainName.add(mArrSideBrain.get(i).getSideofbrain());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>
                        (mActivity, android.R.layout.simple_selectable_list_item, mArrSideBrainName);
                spinner_sidebrain.setAdapter(adapter);
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Bind  Category Adapter
     *
     * @param mActivity
     * @param spinner_cataract - Spinner
     */
    private void bindCataractAdapter(Activity mActivity, Spinner spinner_cataract) {
        try {
            if (mArrCataractTypeName.size() > 0) {
                mArrCataractTypeName.clear();
            }

            if (mArrCataractType != null && mArrCataractType.size() > 0) {
                mArrCataractTypeName.add(mActivity.getResources().getString(R.string.spinner_select_cataract));
                for (int i = 0; i < mArrCataractType.size(); i++) {
                    mArrCataractTypeName.add(mArrCataractType.get(i).getCataracttype());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>
                        (mActivity, android.R.layout.simple_selectable_list_item, mArrCataractTypeName);

                spinner_cataract.setAdapter(adapter);
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }


    /**
     * set Bind Spinner Anatomical Location
     *
     * @param spinner_anatomical - Anatomical Location
     */
    private void setAnatomicalLocationAdapter(Spinner spinner_anatomical) {
        try {

            if (mArrAnatomicalName.size() > 0) {
                mArrAnatomicalName.clear();
            }

            if (mArrAnatomicalLocation != null && mArrAnatomicalLocation.size() > 0) {

                mArrAnatomicalName.add(mActivity.getResources().getString(R.string.spinner_select_anatomical_location));
                for (int i = 0; i < mArrAnatomicalLocation.size(); i++) {
                    mArrAnatomicalName.add(mArrAnatomicalLocation.get(i).getAnatomicalLocation());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>
                        (mActivity, android.R.layout.simple_selectable_list_item, mArrAnatomicalName);

                spinner_anatomical.setAdapter(adapter);
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * set Bind Spinner Disease Location
     *
     * @param spinner_disease - Anatomical Location
     */
    private void setDiseaseAdapterData(Spinner spinner_disease) {
        try {
            mArrDisease.clear();

            if (mArrDiseaseList != null && mArrDiseaseList.size() > 0) {

                mArrDisease.add(mActivity.getResources().getString(R.string.spinner_select_disease));
                for (int i = 0; i < mArrDiseaseList.size(); i++) {
                    mArrDisease.add(mArrDiseaseList.get(i).getDisease());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>
                        (mActivity, android.R.layout.simple_selectable_list_item, mArrDisease);

                spinner_disease.setAdapter(adapter);
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    /**
     * OpenDialog for closing patient file
     */
    public void openFileClosingAlert() {


        AlertDialog.Builder builder = new AlertDialog.Builder(
                mActivity);
        builder.setTitle("Close File");
        builder.setMessage("Are you sure you want to close file?");
        builder.setNegativeButton("NO",
                (dialog, which) -> dialog.dismiss());
        builder.setPositiveButton("YES",
                (dialog, which) -> closeFile());
        builder.show();

    }

    private void closeFile() {
        try {
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.closeFileJson(mAppointmentId, mDatabaseName));

            Call<CloseFileModel> call = RetrofitClient.createService(ApiInterface.class).closeFileAPI(body);
            call.enqueue(new Callback<CloseFileModel>() {
                @Override
                public void onResponse(@NonNull Call<CloseFileModel> call, @NonNull Response<CloseFileModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        int mError = response.body().getError();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                Common.insertLog("IF");
                                Common.setCustomToast(mActivity,mMessage);
                            } else {
                                Common.insertLog("ELSE");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CloseFileModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Open History Dialog
     */
    public void openDialogDiagnosis() {

        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_history_preliminary);

        TextView mTextTitle = dialog.findViewById(R.id.text_view_custom_dialog_history_preliminary);
        RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
        ImageView mImageClose = dialog.findViewById(R.id.image_custom_dialog_history_preliminary_close);

        RelativeLayout noInternetConnection = dialog.findViewById(R.id.relative_no_internet);
        RelativeLayout noDataFound = dialog.findViewById(R.id.relative_no_data_available);

        mTextTitle.setText(mActivity.getResources().getString(R.string.tab_diagnosis));

        mImageClose.setColorFilter(Common.setThemeColor(mActivity));

        mImageClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        LinearLayoutManager manager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(manager);

        HistoryDiagnosisAdapter adapter = new HistoryDiagnosisAdapter(mActivity, recyclerView, mArrHistory,
                this) {
            @Override
            protected void onSelectedItem(Activity mActivity, HistoryPreliminaryExaminationModel.Data checkInData) {
                super.onSelectedItem(mActivity, checkInData);
                dialog.dismiss();
                mRelativeNoDataFound.setVisibility(View.GONE);
                if (checkInData.getDiagnosis().size() > 0) {
                    for (int a = 0; a < checkInData.getDiagnosis().size(); a++) {
                        addEditInvestigation(mActivity, checkInData.getDiagnosis().get(a));
                    }
                }
            }
        };
        recyclerView.setAdapter(adapter);

        if (mArrHistory.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            noDataFound.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.GONE);
            noDataFound.setVisibility(View.VISIBLE);
        }

        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    }


    public void setArrayListNotNull() {

        if (mArrCategoryList == null) {
            mArrCategoryList = new ArrayList<>();
        }
        if (mArrEyeList == null) {
            mArrEyeList = new ArrayList<>();
        }

        if (mArrEyeId == null) {
            mArrEyeId = new ArrayList<>();
        }

        if (mArrPatternType == null) {
            mArrPatternType = new ArrayList<>();
        }

        if (mArrStage == null) {
            mArrStage = new ArrayList<>();
        }
        if (mArrSideBrain == null) {
            mArrSideBrain = new ArrayList<>();
        }

        if (mArrCataractType == null) {
            mArrCataractType = new ArrayList<>();
        }
    }

    private void callToPreliminarHistoryDiagnosis() {
        try {

            String defualt_date = "1000-01-01";
            String Mobile = AppConstants.STR_EMPTY_STRING;
            String Type = "Diagnosis";

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setHistoryPreliminaryExaminationJson(defualt_date, mCurrentPage, mPatientId, Mobile, mHistoryDoctorID, Type, hospital_database));

            Call<HistoryPreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationHistory(body);
            call.enqueue(new Callback<HistoryPreliminaryExaminationModel>() {
                @Override
                public void onResponse(@NonNull Call<HistoryPreliminaryExaminationModel> call, Response<HistoryPreliminaryExaminationModel> response) {
                    Common.insertLog("Response History::::> " + new Gson().toJson(response.body()));
                    try {
                        if (response.body() != null &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body().getData().size() > 0) {

                                mArrHistory.addAll(response.body().getData());
//                                setTodayData();
                            }
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<HistoryPreliminaryExaminationModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Today Already Added Data
     */
    private void setTodayData() {

        String currentDate = Common.setCurrentDate(mActivity);
        String covertTreatmentDate = Common.convertDateUsingDateFormat(mActivity, currentDate,
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen));

        if (mArrHistory.size() > 0) {

            if (mLinearDiagnosis.getChildCount() > 0) {
                mLinearDiagnosis.removeAllViews();
            }

            for (int i = 0; i < mArrHistory.size(); i++) {
                if (mArrHistory.get(i).getTreatmentDate().equals(covertTreatmentDate)) {

                    if (mArrHistory.get(i).getAppointmentID().equals(String.valueOf(mAppointmentId))) {

                        if (mArrHistory.get(i).getDiagnosis().size() > 0) {
                            for (int h = 0; h < mArrHistory.get(i).getDiagnosis().size(); h++) {
                                addEditInvestigation(mActivity, mArrHistory.get(i).getDiagnosis().get(h));
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Set Update Doiagnosis History
     *
     * @param mActivity - Activity
     * @param diagnosi  - Diagnosis
     */
    private void addEditInvestigation(Activity mActivity, HistoryPreliminaryExaminationModel.Diagnosi diagnosi) {

        @SuppressLint("InflateParams")
        View view = LayoutInflater.from(mActivity).
                inflate(R.layout.row_add_diagnosis_item, null, false);

        Spinner spinner_category = view.findViewById(R.id.spinner_row_add_diagnosis_category);
        Spinner spinner_anatomical = view.findViewById(R.id.spinner_row_add_diagnosis_anatomical_location);
        Spinner spinner_eye = view.findViewById(R.id.spinner_row_add_diagnosis_eye);
        Spinner spinner_disease = view.findViewById(R.id.spinner_row_add_diagnosis_disease);
        Spinner spinner_eye_id = view.findViewById(R.id.spinner_row_add_diagnosis_eye_id);
        Spinner spinner_cataract = view.findViewById(R.id.spinner_row_add_diagnosis_cataract_type);
        Spinner spinner_stage = view.findViewById(R.id.spinner_row_add_diagnosis_stage);
        Spinner spinner_side_of_brain = view.findViewById(R.id.spinner_row_add_diagnosis_side_of_brain);
        Spinner spinner_pattern = view.findViewById(R.id.spinner_row_add_diagnosis_pattern_type);

        TextView textSelectedAnatomical = view.findViewById(R.id.text_row_add_diagnosis_selected_anatomical);
        TextView textSelectedDisease = view.findViewById(R.id.text_row_add_diagnosis_selected_disease);

        ImageView mImageRemove = view.findViewById(R.id.image_row_add_diagnosis_remove);

        //set Text
        textSelectedAnatomical.setText("0");
        textSelectedDisease.setText("0");

        // ToDo: Sets the spinner color as per the theme applied
        spinner_category.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        spinner_anatomical.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        spinner_eye.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        spinner_disease.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        spinner_eye_id.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        spinner_cataract.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        spinner_stage.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        spinner_side_of_brain.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        spinner_pattern.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

        setArrayListNotNull();

        bindCategoryAdaper(mActivity, spinner_category);
        bindEyeAdapter(mActivity, spinner_eye);
        bindEyeIDAdapter(mActivity, spinner_eye_id);
        bindPatternAdapter(mActivity, spinner_pattern);
        bindStageAdapter(mActivity, spinner_stage);
        bindSideOfBrianAdapter(mActivity, spinner_side_of_brain);
        bindCataractAdapter(mActivity, spinner_cataract);
        setAnatomicalLocationAdapter(spinner_anatomical);
        setDiseaseAdapterData(spinner_disease);


        if (mArrCategoryList.size() > 0) {
            for (int a = 0; a < mArrCategoryList.size(); a++) {
                if (mArrCategoryList.get(a).getCategoryID().
                        equals(diagnosi.getCategoryID())) {
                    int select_pos = a + 1;
                    spinner_category.setSelection(select_pos, true);
                }
            }
        }
        if (mArrAnatomicalLocation.size() > 0) {
            for (int a = 0; a < mArrAnatomicalLocation.size(); a++) {
                if (mArrAnatomicalLocation.get(a).getAnatomicalLocationID().
                        equals(diagnosi.getAnatomicalLocationID())) {
                    int select_pos = a + 1;
                    spinner_anatomical.setSelection(select_pos, true);
                }
            }
        }

        if (mArrEyeList.size() > 0) {
            for (int a = 0; a < mArrEyeList.size(); a++) {
                if (mArrEyeList.get(a).getEyeID().
                        equals(diagnosi.getEyeID())) {
                    int select_pos = a + 1;
                    spinner_eye.setSelection(select_pos, true);
                }
            }
        }

        if (mArrDiseaseList.size() > 0) {
            for (int a = 0; a < mArrDiseaseList.size(); a++) {
                if (mArrDiseaseList.get(a).getDiseaseID().
                        equals(diagnosi.getDiseaseID())) {
                    int select_pos = a + 1;
                    spinner_disease.setSelection(select_pos, true);
                }
            }
        }

        if (mArrEyeId.size() > 0) {
            for (int a = 0; a < mArrEyeId.size(); a++) {
                if (mArrEyeId.get(a).getEyeLenseID().
                        equals(diagnosi.getEyeLenseID())) {
                    int select_pos = a + 1;
                    spinner_eye_id.setSelection(select_pos, true);
                }
            }
        }

        if (mArrCataractType.size() > 0) {
            for (int a = 0; a < mArrCataractType.size(); a++) {
                if (mArrCataractType.get(a).getCataracttypeID().
                        equals(diagnosi.getCataracttypeID())) {
                    int select_pos = a + 1;
                    spinner_cataract.setSelection(select_pos, true);
                }
            }
        }

        if (mArrStage.size() > 0) {
            for (int a = 0; a < mArrStage.size(); a++) {
                if (mArrStage.get(a).getStageID().
                        equals(diagnosi.getStageID())) {
                    int select_pos = a + 1;
                    spinner_stage.setSelection(select_pos, true);
                }
            }
        }
        if (mArrSideBrain.size() > 0) {
            for (int a = 0; a < mArrSideBrain.size(); a++) {
                if (mArrSideBrain.get(a).getSideofbrainID().
                        equals(diagnosi.getSideofbrainID())) {
                    int select_pos = a + 1;
                    spinner_side_of_brain.setSelection(select_pos, true);
                }
            }
        }
        if (mArrPatternType.size() > 0) {
            for (int a = 0; a < mArrPatternType.size(); a++) {
                if (mArrPatternType.get(a).getPatterntypeID().
                        equals(diagnosi.getPatterntypeID())) {
                    int select_pos = a + 1;
                    spinner_pattern.setSelection(select_pos, true);
                }
            }
        }


        //ToDo:Set on Spinner set On Item Select Listener
        spinner_category.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtility.HideKeyboard(mActivity, spinner_category);
                if (position > 0) {
                    SelectAnatomical(mArrCategoryList.get(position - 1), spinner_anatomical);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_anatomical.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtility.HideKeyboard(mActivity, spinner_category);
                if (position > 0) {
                    if (position > 0) {
                        SelectCategory(mArrAnatomicalLocation.get(position - 1), spinner_category);
                        SelectDieseasData(mArrAnatomicalLocation.get(position - 1), spinner_disease);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_disease.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtility.HideKeyboard(mActivity, spinner_category);
                if (position > 0) {
                    SelectDiseaseAnatomical(mArrDiseaseList.get(position - 1), spinner_anatomical, spinner_disease);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_eye.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtility.HideKeyboard(mActivity, spinner_category);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_eye_id.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtility.HideKeyboard(mActivity, spinner_category);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_cataract.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtility.HideKeyboard(mActivity, spinner_category);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_stage.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtility.HideKeyboard(mActivity, spinner_category);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_side_of_brain.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtility.HideKeyboard(mActivity, spinner_category);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner_pattern.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                KeyboardUtility.HideKeyboard(mActivity, spinner_category);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //TODO: Set On Click Listener
        mImageRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLinearDiagnosis.removeView(view);
            }
        });

        mLinearDiagnosis.addView(view);
    }
}
