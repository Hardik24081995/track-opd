package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.adapter.PatientTreatmentPlanAdapter;
import com.trackopd.model.PatientPrescriptionModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import java.util.ArrayList;

public class PatientPrescriptionDetailsFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private RelativeLayout mRelativeImageBorder, mRelativeNoData;
    private ImageView mImageUserProfilePic;
    private TextView mTextName, mTextMobileNo, mTextCode, mTextAppointmentNo, mTextAppointmentDate;
    private PatientPrescriptionModel patientPrescriptionModel;
    private RecyclerView mRecyclerView;
    private ArrayList<PatientPrescriptionModel.Medication> mArrPatientTreatmentModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_medication_details, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrPatientTreatmentModel = new ArrayList<>();

        getBundle();
        getIds();
        setData();
        setMedicationList();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                patientPrescriptionModel = (PatientPrescriptionModel) bundle.getSerializable(AppConstants.BUNDLE_PATIENT_ARRAY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layouts
            mRelativeImageBorder = mView.findViewById(R.id.relative_patient_prescription_profile_pic);
            mRelativeNoData = mView.findViewById(R.id.relative_no_data_available);

            // Image Views
            mImageUserProfilePic = mView.findViewById(R.id.image_patient_prescription_profile_pic);

            // Text Views
            mTextName = mView.findViewById(R.id.text_patient_prescription_name);
            mTextMobileNo = mView.findViewById(R.id.text_patient_prescription_mobile_no);
            mTextCode = mView.findViewById(R.id.text_patient_prescription_patient_code);
            mTextAppointmentNo = mView.findViewById(R.id.text_patient_prescription_patient_appointment_no);
            mTextAppointmentDate = mView.findViewById(R.id.text_patient_prescription_appointment_date);

            // Recycler View
            mRecyclerView = mView.findViewById(R.id.recycler_view);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            String mPatientFirstName = patientPrescriptionModel.getPatientFirstName();
            String mPatientLastName = patientPrescriptionModel.getPatientLastName();
            String mPatientMobileNo = patientPrescriptionModel.getPatientMobileNo();
            String mAppointmentNo = patientPrescriptionModel.getTicketNumber();
            String mAppointmentDate = patientPrescriptionModel.getAppointmentDate();

            mRelativeImageBorder.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.user_image_border));
            mImageUserProfilePic.setImageDrawable(Common.setLabeledDefaultImageView(mActivity, mPatientFirstName, mPatientLastName));
            mTextName.setText(mPatientFirstName + AppConstants.STR_EMPTY_SPACE + mPatientLastName);
            mTextMobileNo.setText(mPatientMobileNo);
            mTextAppointmentNo.setText(mActivity.getResources().getString(R.string.edit_patient_appointment_number) + AppConstants.STR_COLON + mAppointmentNo);
            mTextAppointmentDate.setText(mActivity.getResources().getString(R.string.edit_patient_appointment_date) + AppConstants.STR_COLON + mAppointmentDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Binds the medication data
     */
    private void setMedicationList() {
        try {
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mGridLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(mGridLayoutManager);

            ArrayList<PatientPrescriptionModel.Medication> patientMedication = (ArrayList<PatientPrescriptionModel.Medication>) patientPrescriptionModel.getMedication();
            mArrPatientTreatmentModel.addAll(patientMedication);

            PatientTreatmentPlanAdapter mAdapterPatientTreatment = new PatientTreatmentPlanAdapter(mActivity, mArrPatientTreatmentModel);
            mRecyclerView.setAdapter(mAdapterPatientTreatment);

            setViewVisibility();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the view visibility as per the array size
     */
    private void setViewVisibility() {
        if (mArrPatientTreatmentModel.size() != 0) {
            mRelativeNoData.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mRelativeNoData.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }
}
