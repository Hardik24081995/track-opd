package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.afollestad.materialdialogs.MaterialDialog;
import com.github.badoualy.datepicker.DatePickerTimeline;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.ReceptionistAppointmentAdapter;
import com.trackopd.adapter.ReceptionistAppointmentTimeLineAdapter;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.AppointmentReasonsModel;
import com.trackopd.model.ReceptionistAppointmentModel;
import com.trackopd.model.ReceptionistTimeSlotAppointmentModel;
import com.trackopd.model.TimeSlotModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.utils.RecyclerProgressView;
import com.trackopd.utils.TimeSlotCalculation;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReceptionistAppointmentFragment extends Fragment {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    public Bundle mBundle;
    private String mName = "", mPatientCode = "", mMobileNo = "", mMRDNo = "", mAppointmentNo = "", mAppointmentDate = "0000-00-00",
            mUserType, mAddAppointment, mAppointmentStatus, mStatus;
    private int mDoctorId = -1;
    private Boolean isStarted = false, isVisible = false, isFirstTime = true;
    private DatePickerTimeline mDatePickerTimeLine;
    private Date mSelectedDate, mCurrentDate;
    private AppUtil mAppUtils;
    private ArrayList<ReceptionistAppointmentModel> mArrReceptionistAppointment;
    private ArrayList<AppointmentReasonsModel> mArrAppointmentReasons;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshView;
    private RelativeLayout mRelativeNoData, mRelativeNoInternet;
    private Button mButtonRetry;
    // Load More Listener Variables
    private int currentPageIndex = 1, lastFetchRecord = 0, totalRecords = 0;
    private boolean isRefresh = false, isLoadMore = false;
    private ReceptionistAppointmentAdapter mAdapterReceptionistAppointment;
    private RadioGroup mRadioGroupType;
    private RadioButton mRadioPending, mRadioOngoing, mRadioCompleted, mRadioCancelled, mRadioAll;
    private ReceptionistAppointmentTimeLineAdapter mAppointmentTimeLineAdapter;

    private ArrayList<ReceptionistTimeSlotAppointmentModel.Data> mArrTimeSlotAppointment;

    

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_receptionist_appointment, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();


        mAppUtils = new AppUtil(getActivity());
        mArrReceptionistAppointment = new ArrayList<>();
        mArrAppointmentReasons = new ArrayList<>();
        mArrTimeSlotAppointment=new ArrayList<>();
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        getBundle();
        getIds();
        setRegListeners();
        setData();
        getCurrentDate();
        callToGetReasonAPI();

        setHasOptionsMenu(true);
        return mView;
    }



    /**
     * Set the prepare menu options
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_appointment, menu);

        final MenuItem menuSearch = menu.findItem(R.id.action_appointment_search);
        menuSearch.setVisible(true);

        MenuItem menuAdd = menu.findItem(R.id.action_appointment_add);

        if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
            menuAdd.setVisible(true);
        } else {
            menuAdd.setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_appointment_add:
                openPopup();
                return true;

            case R.id.action_appointment_search:
                callToSearchAppointmentFragment();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mName = bundle.getString(AppConstants.BUNDLE_PATIENT_NAME);
                mPatientCode = bundle.getString(AppConstants.BUNDLE_PATIENT_CODE);
                mMobileNo = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mMRDNo = bundle.getString(AppConstants.BUNDLE_MRD_NO);
                mAppointmentNo = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mAppointmentDate = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DATE);
                mDoctorId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);
                mStatus = bundle.getString(AppConstants.BUNDLE_APPOINTMENT_STATUS);
                Common.insertLog("Status:::> " + mAppointmentDate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Date Picker Time Line
            mDatePickerTimeLine = mView.findViewById(R.id.date_picker_time_line_book_appointment);

            // Tab Layout
            mTabLayout = mView.findViewById(R.id.tab_layout_receptionist_appointment);

            // View Pager
            mViewPager = mView.findViewById(R.id.view_pager_receptionist_appointment);

            // Radio Group
            mRadioGroupType = mView.findViewById(R.id.radio_group_receptionist_appointment_type);

            // Radio Buttons
            mRadioPending = mView.findViewById(R.id.radio_button_receptionist_appointment_pending);
            mRadioOngoing = mView.findViewById(R.id.radio_button_receptionist_appointment_ongoing);
            mRadioCompleted = mView.findViewById(R.id.radio_button_receptionist_appointment_completed);
            mRadioCancelled = mView.findViewById(R.id.radio_button_receptionist_appointment_cancelled);
            mRadioAll = mView.findViewById(R.id.radio_button_receptionist_appointment_all);

            // Recycler View
            mRecyclerView = mView.findViewById(R.id.recycler_view);

            // Swipe Refresh View
            mSwipeRefreshView = mView.findViewById(R.id.swipe_refresh_view);

            // Relative Layouts
            mRelativeNoData = mView.findViewById(R.id.relative_no_data_available);
            mRelativeNoInternet = mView.findViewById(R.id.relative_no_internet);

            // Buttons
            mButtonRetry = mView.findViewById(R.id.button_retry);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Date Selected Listener
            mDatePickerTimeLine.setOnDateSelectedListener(dateSelectedListener);
            mButtonRetry.setOnClickListener(clickListener);

            // ToDo: Set On Refresh Listeners
            mSwipeRefreshView.setOnRefreshListener(refreshListener);

            mRadioGroupType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @SuppressLint("ResourceType")
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    RadioButton rb = group.findViewById(checkedId);
                    if (null != rb && checkedId > -1) {

                        if (rb.getText().toString().equalsIgnoreCase(mActivity.getResources().getString(R.string.text_cancelled))) {
                            mAppointmentStatus = mActivity.getResources().getString(R.string.text_reject);
                        } else if (rb.getText().toString().equalsIgnoreCase(mActivity.getResources().getString(R.string.text_on_going))) {
                            mAppointmentStatus = mActivity.getResources().getString(R.string.text_ongoing);
                        } else {
                            mAppointmentStatus = rb.getText().toString();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            if (mStatus == null) {
                mAppointmentStatus = mActivity.getResources().getString(R.string.status_pending);
            } else {
                if (mStatus.equalsIgnoreCase(mActivity.getResources().getString(R.string.status_pending))) {
                    mRadioPending.setChecked(true);
                    mAppointmentStatus = mActivity.getResources().getString(R.string.status_pending);
                } else if (mStatus.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_ongoing))) {
                    mRadioOngoing.setChecked(true);
                    mAppointmentStatus = mActivity.getResources().getString(R.string.text_ongoing);
                } else if (mStatus.equalsIgnoreCase(mActivity.getResources().getString(R.string.status_completed))) {
                    mRadioCompleted.setChecked(true);
                    mAppointmentStatus = mActivity.getResources().getString(R.string.status_completed);
                } else if (mStatus.equalsIgnoreCase(mActivity.getResources().getString(R.string.status_completed))) {
                    mRadioCompleted.setChecked(true);
                    mAppointmentStatus = mActivity.getResources().getString(R.string.status_completed);
                } else {
                    mRadioCancelled.setChecked(true);
                    mAppointmentStatus = mActivity.getResources().getString(R.string.tab_all);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_retry:
                    callShimmerView();
                    break;
            }
        }
    };

    /**
     * Set On Refresh Listeners
     */
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            mSwipeRefreshView.setRefreshing(false);
            isFirstTime = true;

            if (!mAppUtils.getConnectionState()) {
                mRelativeNoInternet.setVisibility(View.VISIBLE);
                mSwipeRefreshView.setRefreshing(false);
                //mArrReceptionistAppointment.clear();
                setViewVisibility();
            } else {
                mRelativeNoInternet.setVisibility(View.GONE);
                if (!isLoadMore) {
                    isRefresh = true;

                    mName = AppConstants.STR_EMPTY_STRING;
                    mPatientCode = AppConstants.STR_EMPTY_STRING;
                    mMobileNo = AppConstants.STR_EMPTY_STRING;
                    mMRDNo = AppConstants.STR_EMPTY_STRING;
                    mAppointmentNo = AppConstants.STR_EMPTY_STRING;
                    //mAppointmentDate = mActivity.getResources().getString(R.string.date_format_first_time);
                    mDoctorId = -1;

                    /*if (mArrReceptionistAppointment != null)
                        mArrReceptionistAppointment.clear();*/

                    currentPageIndex = 1;
                    callShimmerView();

                } else
                    mSwipeRefreshView.setRefreshing(false);
            }
        }
    };

    /**
     * Set Current date in date picker and get Current date
     */
    private void getCurrentDate() {
        Calendar CurrentCalender = Calendar.getInstance();
        if (mAppointmentDate.equalsIgnoreCase(mActivity.getResources().getString(R.string.date_format_first_time))) {
            mDatePickerTimeLine.setFirstVisibleDate(CurrentCalender.get(Calendar.YEAR), 0, 1);
            mDatePickerTimeLine.setSelectedDate(CurrentCalender.get(Calendar.YEAR),
                    CurrentCalender.get(Calendar.MONTH),
                    CurrentCalender.get(Calendar.DAY_OF_MONTH));

            try {
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat dateFormatter = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
                String strCurrentDate = dateFormatter.format(CurrentCalender.getTime());
                mSelectedDate = dateFormatter.parse(strCurrentDate);
                mCurrentDate = dateFormatter.parse(strCurrentDate);
                @SuppressLint("SimpleDateFormat")
                DateFormat dateFormat = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
                mAppointmentDate = dateFormat.format(mSelectedDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Common.insertLog("Else 1");
            String[] mValue = mAppointmentDate.split("-");
            int mYear = Integer.parseInt(mValue[0]);
            int mMonth = Integer.parseInt(mValue[1]);
            int mDay = Integer.parseInt(mValue[2]);
            mDatePickerTimeLine.setFirstVisibleDate(CurrentCalender.get(Calendar.YEAR), 0, 1);
            mDatePickerTimeLine.setSelectedDate(mYear, mMonth-1, mDay);

            try {
                String mDate = mYear + "-" + mMonth + "-" + mDay;
                @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
                mSelectedDate = df.parse(mDate);
                @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
                mAppointmentDate = dateFormat.format(mSelectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
//            callAppointmentListAPI();
            callTimeLineAppointmentList();
        }
    }


    /**
     * Date Selected Click listener
     */
    private DatePickerTimeline.OnDateSelectedListener dateSelectedListener = new DatePickerTimeline.OnDateSelectedListener() {
        @Override
        public void onDateSelected(int year, int month, int day, int index) {
            String mDate = year + "-" + (month + 1) + "-" + day;
            try {
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat df = new SimpleDateFormat(
                        mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));

                mSelectedDate = df.parse(mDate);
                DateFormat dateFormat = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
                mAppointmentDate = dateFormat.format(mSelectedDate);
                //callTimeLineAppointmentList();

                isFirstTime = true;
                mArrTimeSlotAppointment.clear();

                callShimmerView();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    /**
     * Receptionist Upcoming Appointment Fragment on visible
     *
     * @param isVisibleToUser - This can check the fragment is visible when swiping and will return
     *                        the boolean value based on that
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        isVisible = isVisibleToUser;
        if (isStarted && isVisible) {
            isFirstTime = true;

            callShimmerView();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
    }

    /**
     * Call To Get Appointment Reason List API
     */
    private void callToGetReasonAPI() {
        try {

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAppointmentReason(hospital_database));
            Call<AppointmentReasonsModel> call = RetrofitClient.createService(ApiInterface.class).getReason(requestBody);
            call.enqueue(new Callback<AppointmentReasonsModel>() {
                @Override
                public void onResponse(@NonNull Call<AppointmentReasonsModel> call, @NonNull Response<AppointmentReasonsModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        if (response.isSuccessful() && response.body().getError() == 200) {
                            mArrAppointmentReasons.addAll(response.body().getData());
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AppointmentReasonsModel> call, @NonNull Throwable t) {
                    Common.insertLog(t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Get Time Slot on Selected Date
     */
    private void callTimeLineAppointmentList() {
        try {
                if (mName == null) {
                    mName = AppConstants.STR_EMPTY_STRING;
                }

                if (mPatientCode == null) {
                    mPatientCode = AppConstants.STR_EMPTY_STRING;
                }

                if (mMRDNo == null) {
                    mMRDNo = AppConstants.STR_EMPTY_STRING;
                }

                if (mMobileNo == null) {
                    mMobileNo = AppConstants.STR_EMPTY_STRING;
                }

                if (mAppointmentNo == null) {
                    mAppointmentNo = AppConstants.STR_EMPTY_STRING;
                }

                if (mAppointmentDate == null) {
                    mAppointmentDate = mActivity.getResources().getString(R.string.date_format_first_time);
                }

                if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                    mDoctorId = Integer.parseInt(GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID));
                }

                String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

                RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                        APICommonMethods.setReceptionistTimeAppointmentListJson(mActivity, currentPageIndex, mUserType, mName, mPatientCode, mMobileNo, mMRDNo, mAppointmentNo, mAppointmentDate, mDoctorId, mActivity.getResources().getString(R.string.tab_all), mAppointmentStatus, String.valueOf(-1), hospital_database));

                Call<ReceptionistTimeSlotAppointmentModel> call = RetrofitClient.createService(ApiInterface.class).getReceptionistTimeAppointmentList(requestBody);

                call.enqueue(new Callback<ReceptionistTimeSlotAppointmentModel>() {
                    @Override
                    public void onResponse(Call<ReceptionistTimeSlotAppointmentModel> call, Response<ReceptionistTimeSlotAppointmentModel> response) {
                        Common.insertLog(response.body().toString());
                        try {

                            Common.insertLog(response.body().toString());
                            if(response.isSuccessful() &&
                                    response.body().getError()==200){

                                String mJson = (new Gson().toJson(response.body()));
                                JSONObject jsonObject = new JSONObject(mJson);
                                String mMessage = jsonObject.getString("Message");
                                Common.insertLog("mMessage:::> " + mMessage);
                                stopProgressView();

                                ArrayList<ReceptionistTimeSlotAppointmentModel.Data> appointmentModel= (ArrayList<ReceptionistTimeSlotAppointmentModel.Data>) response.body().getData();

                                if (mArrTimeSlotAppointment != null && mArrTimeSlotAppointment.size() > 0 &&
                                        mAppointmentTimeLineAdapter != null) {
                                    mArrTimeSlotAppointment.addAll(appointmentModel);
                                    mAppointmentTimeLineAdapter.notifyDataSetChanged();
                                    lastFetchRecord = mArrTimeSlotAppointment.size();
                                } else {
                                    mArrTimeSlotAppointment = appointmentModel;
                                    setAdapterData();
                                    lastFetchRecord = mArrTimeSlotAppointment.size();
                                    totalRecords = mArrTimeSlotAppointment.size();
                                }
                            }else{
                                Common.setCustomToast(mActivity, response.body().getMessage());
                                setViewVisibility();
                                stopProgressView();
                            }

                        }catch (Exception e){
                            Common.insertLog(e.getMessage());
                            setViewVisibility();
                            stopProgressView();
                        }
                    }

                    @Override
                    public void onFailure(Call<ReceptionistTimeSlotAppointmentModel> call, Throwable t) {
                        Common.insertLog("Failure:::> " + t.getMessage());
                        setViewVisibility();
                        stopProgressView();
                    }
                });


        }catch (Exception e){
            Common.insertLog(e.getMessage());
            stopProgressView();
        }
    }


    /**
     * This method is used to call the shimmer effect for recycler view before calling the API
     */
    private void callShimmerView() {
        if (!mAppUtils.getConnectionState()) {
            mRelativeNoInternet.setVisibility(View.VISIBLE);
            mArrTimeSlotAppointment.clear();
            setViewVisibility();
        } else {
            mRelativeNoInternet.setVisibility(View.GONE);
            if (isFirstTime) {
                showProgressView();
                isFirstTime = false;
            }
            mArrTimeSlotAppointment.clear();
            if (mAppointmentTimeLineAdapter!=null){
                mAppointmentTimeLineAdapter.notifyDataSetChanged();
            }
            callTimeLineAppointmentList();
        }
    }

    /**
     * Sets the view visibility as per the array size
     */
    private void setViewVisibility() {
        if (mArrTimeSlotAppointment.size() != 0)
        {
            mRelativeNoData.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mRelativeNoData.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    /**
     * Show progress view when page load for the first time
     */
    private void showProgressView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.VISIBLE);
                RecyclerProgressView.startProgress((LinearLayout) mView.findViewById(R.id.linear_add_recycler_progress_view),
                        R.layout.row_receptionist_appointment_time_line_item, getActivity());
            }
        }, 100);
    }
    /**
     * Show progress view when page load for the first time
     */
    private void stopProgressView() {
        try {
            mSwipeRefreshView.setRefreshing(false);
            isRefresh = false;
            isLoadMore = false;
            RecyclerProgressView.stopProgress();
            mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            hideProgressBar();
        }
    }

    /**
     * Hide the progress bar
     */
    private void hideProgressBar() {
        try {
            if (mArrReceptionistAppointment != null && mArrReceptionistAppointment.size() > 0 &&
                    mArrReceptionistAppointment.get(mArrReceptionistAppointment.size() - 1) == null) {

                mArrReceptionistAppointment.remove(mArrReceptionistAppointment.size() - 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mGridLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(mGridLayoutManager);

            mAppointmentTimeLineAdapter=new ReceptionistAppointmentTimeLineAdapter
                    (mActivity,mRecyclerView,mArrTimeSlotAppointment,mArrAppointmentReasons)
            {
                @Override
                protected void Update(String date) {
                    super.Update(date);
                    Common.insertLog(date);


                    // date format is  Date Format "dd-MM-yyyy"
                    String[] mValue = date.split("-");
                    int mYear = Integer.parseInt(mValue[0]);
                    int mMonth = Integer.parseInt(mValue[1]);
                    int mDay = Integer.parseInt(mValue[2]);
                    mDatePickerTimeLine.setSelectedDate(mYear, mMonth-1, mDay);

                    try {
                        String mDate = mYear + "-" + mMonth + "-" + mDay;
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
                        mSelectedDate = df.parse(mDate);
                        @SuppressLint("SimpleDateFormat")
                        DateFormat dateFormat = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
                        mAppointmentDate = dateFormat.format(mSelectedDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    isFirstTime = true;
                    mArrTimeSlotAppointment.clear();

                    if (mAppointmentTimeLineAdapter!=null){
                        mAppointmentTimeLineAdapter.notifyDataSetChanged();
                    }

                    callShimmerView();
                }
            };
            mRecyclerView.setAdapter(mAppointmentTimeLineAdapter);

            setViewVisibility();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * On load more click listener
     */
    private void setLoadMoreClickListener() {

        mAppointmentTimeLineAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
               if (mArrTimeSlotAppointment!=null && mArrTimeSlotAppointment.size()>0
                       && mArrTimeSlotAppointment.size()<totalRecords)
               {
                   if (!mAppUtils.getConnectionState()) {
                       mRelativeNoInternet.setVisibility(View.VISIBLE);
                       mArrTimeSlotAppointment.clear();
                       setViewVisibility();
                   } else {
                       mRelativeNoInternet.setVisibility(View.GONE);
                       mArrTimeSlotAppointment.clear();

                       if (mAppointmentTimeLineAdapter!=null){
                           mAppointmentTimeLineAdapter.notifyDataSetChanged();
                       }

                       currentPageIndex = (mArrTimeSlotAppointment.size() / Common.getPageSizeForPagination(mActivity)) + 1;
                       callTimeLineAppointmentList();
                   }
               }
            }
        });
    }


    /**
     * Open popup for Appointment
     */
    private void openPopup() {
        try {
            new MaterialDialog.Builder(mActivity)
                    .title(R.string.app_name)
                    .items(R.array.patient_type)
                    .canceledOnTouchOutside(false)
                    .positiveText(R.string.action_ok)
                    .positiveColor(mActivity.getResources().getColor(R.color.app_main_theme))
                    .negativeText(R.string.action_cancel)
                    .negativeColor(mActivity.getResources().getColor(R.color.colorControlNormal))
                    .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                        @Override
                        public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                            if (which == 0) {
                                callToAddPatientFragment();
                            } else {
                                callToExistingAppointmentFragment();
                            }
                            return true;
                        }
                    })
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Receptionist Add Patient Fragment
     */
    private void callToAddPatientFragment() {
        try {
            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_patient));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_patient));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_patient))) {
                ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_patient));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_patient));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_patient));
            }

            Fragment fragment = new AddPatientFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_LOGIN_TYPE, mUserType);
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_patient))) {
                fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_new_patient))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_new_patient))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Search Receptionist Appointment Fragment
     */
    private void callToSearchAppointmentFragment() {
        try {
            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_patient))) {
                ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            }

            Fragment fragment = new SearchReceptionistAppointmentFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_ADD_APPOINTMENT, mAddAppointment);
            args.putString(AppConstants.BUNDLE_APPOINTMENT_STATUS, mAppointmentStatus);
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_patient))) {
                fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_search_receptionist_appointment))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_search_receptionist_appointment))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Receptionist Existing Appointment Fragment
     */
    private void callToExistingAppointmentFragment() {
        try {

            Fragment fragment = new SearchReceptionistPatientFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_ADD_PATIENT, mAddAppointment);
            args.putString(AppConstants.BUNDLE_IS_FROM_SEARCH, AppConstants.BUNDLE_ADD_APPOINTMENT);
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_patient))) {
                fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
                ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();

                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_search_receptionist_patient))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_search_receptionist_patient))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}