package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.adapter.PatientTreatmentPlanAdapter;
import com.trackopd.model.PatientTreatmentPlanModel;
import com.trackopd.utils.AppConstants;

import java.util.ArrayList;

public class PatientTreatmentPlanFragment extends Fragment {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private SwipeRefreshLayout mSwipeRefreshView;
    private RecyclerView mRecyclerView;
    private ArrayList<PatientTreatmentPlanModel> mArrPatientTreatment;
    private ArrayList<PatientTreatmentPlanModel.PatientTreatmentPlanMedicine> mArrPatientTreatmentMedicine;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_patient_treatment_plan, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrPatientTreatment = new ArrayList<>();
        mArrPatientTreatmentMedicine = new ArrayList<>();
        getIds();
        setRegListeners();
        setAdapterData();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Set the prepare menu options
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);

        MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
//                callToAddPatientTreatmentFragment();
                return true;

            case R.id.action_search:
                callToSearchPatientTreatmentPlanFragment();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Recycler View
            mRecyclerView = mView.findViewById(R.id.recycler_view);

            // Swipe Refresh View
            mSwipeRefreshView = mView.findViewById(R.id.swipe_refresh_view);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mSwipeRefreshView.setOnRefreshListener(refreshListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mGridLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerView.setLayoutManager(mGridLayoutManager);

        for (int i = 1; i <= 10; i++) {
            if (mArrPatientTreatmentMedicine.size() > 0)
                mArrPatientTreatmentMedicine.clear();

            for (int j = 0; j < 2; j++) {
                PatientTreatmentPlanModel.PatientTreatmentPlanMedicine patientTreatmentPlanMedicine
                         = new PatientTreatmentPlanModel.PatientTreatmentPlanMedicine();

                int pos = j + 1;
                patientTreatmentPlanMedicine.setMedicineName("Dengue Tablet " + pos);
                patientTreatmentPlanMedicine.setMorning_dosage("250 mg");
                patientTreatmentPlanMedicine.setEvening_dosage("500 mg");
                patientTreatmentPlanMedicine.setAfternoon_dosage("250 mg");
                mArrPatientTreatmentMedicine.add(patientTreatmentPlanMedicine);
            }

            PatientTreatmentPlanModel patientTreatmentPlanModel = new PatientTreatmentPlanModel(AppConstants.STR_EMPTY_STRING, "Dengue fever", "1/12/2018", mArrPatientTreatmentMedicine);
            mArrPatientTreatment.add(patientTreatmentPlanModel);
        }

//        PatientTreatmentPlanAdapter mAdapterPatientTreatment = new PatientTreatmentPlanAdapter(mActivity, mRecyclerView, mArrPatientTreatment);
//        mRecyclerView.setAdapter(mAdapterPatientTreatment);

        if (mArrPatientTreatment.size() != 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    /**
     * Sets up the Add Patient Treatment Fragment
     */
    private void callToAddPatientTreatmentFragment() {
        try {
            ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.nav_menu_treatment_plan));
            Fragment fragment = new AddPatientTreatmentFragment();
            FragmentManager fragmentManager =
                    ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_patient_treatment))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_patient_treatment))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Add Patient Treatment Plan Fragment
     */
    private void callToSearchPatientTreatmentPlanFragment() {
        try {
            ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.header_search));
            Fragment fragment = new SearchPatientTreatmentPlanFragment();
            FragmentManager fragmentManager =
                    ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_search_patient_treatment))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_search_patient_treatment))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners (Swipe Refresh)
     */
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            mSwipeRefreshView.setRefreshing(false);
        }
    };
}