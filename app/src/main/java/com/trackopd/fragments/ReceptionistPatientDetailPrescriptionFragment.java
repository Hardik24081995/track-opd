package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.adapter.PatientPrescriptionAdapter;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.PatientPrescriptionModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.utils.RecyclerProgressView;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Arrays;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.trackopd.webServices.WebFields.MESSAGE;
import static com.trackopd.webServices.WebFields.ROW_COUNT;

public class ReceptionistPatientDetailPrescriptionFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private SwipeRefreshLayout mSwipeRefreshView;
    private RecyclerView mRecyclerView;
    private AppUtil mAppUtils;
    private RelativeLayout mRelativeNoData, mRelativeNoInternet;
    private Button mButtonRetry;
    private boolean isFirstTime = true;
    private ArrayList<HistoryPreliminaryExaminationModel.Data> mArrPatientPrescription;
    private PatientPrescriptionAdapter mAdapterPatientPrescription;
    private String mTitle, mPatientId;

    // Load More Listener Variables
    private int currentPageIndex = 1, lastFetchRecord = 0, totalRecords = 0;
    private boolean isRefresh = false, isLoadMore = false, isStarted = false, isVisible = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_receptionist_patient_detail_prescription, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mAppUtils = new AppUtil(getActivity());
        mArrPatientPrescription = new ArrayList<>();

        getBundle();
        getIds();
        setRegListeners();
        callShimmerView();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_grid:
                callToAddPrescriptionsFragment();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Set up Get Value On Bundle
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Recycler View
            mRecyclerView = mView.findViewById(R.id.recycler_view);

            // Swipe Refresh View
            mSwipeRefreshView = mView.findViewById(R.id.swipe_refresh_view);

            // Relative Layouts
            mRelativeNoData = mView.findViewById(R.id.relative_no_data_available);
            mRelativeNoInternet = mView.findViewById(R.id.relative_no_internet);

            // Buttons
            mButtonRetry = mView.findViewById(R.id.button_retry);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Click Listeners
            mButtonRetry.setOnClickListener(clickListener);

            // ToDo: Set On Refresh Listeners
            mSwipeRefreshView.setOnRefreshListener(refreshListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_retry:
                    callShimmerView();
                    break;
            }
        }
    };

    /**
     * Set On Refresh Listeners
     */
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            isFirstTime = true;

            if (!mAppUtils.getConnectionState()) {
                mRelativeNoInternet.setVisibility(View.VISIBLE);
                mSwipeRefreshView.setRefreshing(false);
                mArrPatientPrescription.clear();
                setViewVisibility();
            } else {
                mRelativeNoInternet.setVisibility(View.GONE);
                if (!isLoadMore) {
                    isRefresh = true;

                    mTitle = AppConstants.STR_EMPTY_STRING;

                    if (mArrPatientPrescription != null)
                        mArrPatientPrescription.clear();
                    if (mAdapterPatientPrescription != null)
                        mAdapterPatientPrescription.notifyDataSetChanged();
                    currentPageIndex = 1;
                    callShimmerView();
                } else
                    mSwipeRefreshView.setRefreshing(false);
            }
        }
    };

    /**
     * This method is used to call the shimmer effect for recycler view before calling the API
     */
    private void callShimmerView() {
        if (!mAppUtils.getConnectionState()) {
            mRelativeNoInternet.setVisibility(View.VISIBLE);
            mArrPatientPrescription.clear();
            setViewVisibility();
        } else {
            mRelativeNoInternet.setVisibility(View.GONE);
            if (isFirstTime) {
                showProgressView();
                isFirstTime = false;
            }
            callPrescriptionListAPI();
        }
    }

    /**
     * This method should call the payment listing for receptionist
     */
    private void callPrescriptionListAPI() {
        try {

            String defualt_date="1000-01-01";
            String Mobile=AppConstants.STR_EMPTY_STRING;
            String Type="TreatmentSuggested";

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setHistoryPreliminaryExaminationJson(defualt_date,currentPageIndex,mPatientId,Mobile,-1,Type,hospital_database));

            Call<HistoryPreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationHistory(body);
            call.enqueue(new Callback<HistoryPreliminaryExaminationModel>() {
                @Override
                public void onResponse(Call<HistoryPreliminaryExaminationModel> call, Response<HistoryPreliminaryExaminationModel> response) {
                    Common.insertLog("Response History::::> " + new Gson().toJson(response.body()));
                    try {
                        if (response.body()!=null &&
                                response.body().getError()==AppConstants.API_SUCCESS_ERROR)
                        {
                            if(response.body().getData().size()>0) {

                                mArrPatientPrescription.addAll(response.body().getData());
                                setAdapterData();
                                lastFetchRecord = mArrPatientPrescription.size();
                                totalRecords = mArrPatientPrescription.size();
                                setLoadMoreClickListener();

                                if (mRecyclerView.getVisibility() == View.GONE) {
                                    stopProgressView();
                                }
                            }
                        }else {
                            Common.setCustomToast(mActivity, response.body().getMessage());
                            stopProgressView();
                            setViewVisibility();
                        }
                    }catch (Exception e){
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<HistoryPreliminaryExaminationModel> call, Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());
                    stopProgressView();
                    setViewVisibility();
                }
            });

            /*Call<PatientPrescriptionModel> call = RetrofitClient.createService(ApiInterface.class).getPatientPrescriptionsList(requestBody);
            call.enqueue(new Callback<PatientPrescriptionModel>() {
                @Override
                public void onResponse(@NonNull Call<PatientPrescriptionModel> call, @NonNull Response<PatientPrescriptionModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        stopProgressView();
                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                ArrayList<PatientPrescriptionModel> prescriptionModel = new ArrayList<>(Arrays
                                        .asList(new GsonBuilder().serializeNulls().create()
                                                .fromJson(new JsonParser().parse(jsonObject.getString(WebFields.DATA)).
                                                        getAsJsonArray(), PatientPrescriptionModel[].class)));

                                if (mArrPatientPrescription != null && mArrPatientPrescription.size() > 0 &&
                                        mAdapterPatientPrescription != null) {
                                    mArrPatientPrescription.addAll(prescriptionModel);
                                    mAdapterPatientPrescription.notifyDataSetChanged();
                                    lastFetchRecord = mArrPatientPrescription.size();
                                } else {
                                    mArrPatientPrescription = prescriptionModel;
                                    setAdapterData();
                                    lastFetchRecord = mArrPatientPrescription.size();
                                    totalRecords = jsonObject.getInt(ROW_COUNT);
                                    setLoadMoreClickListener();

                                    if (mRecyclerView.getVisibility() == View.GONE) {
                                        stopProgressView();
                                    }
                                }
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                            stopProgressView();
                            setViewVisibility();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PatientPrescriptionModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());
                    stopProgressView();
                    setViewVisibility();
                }
            });*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mGridLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(mGridLayoutManager);

            stopProgressView();
            mAdapterPatientPrescription = new PatientPrescriptionAdapter(mActivity, mRecyclerView, mArrPatientPrescription);
            mRecyclerView.setAdapter(mAdapterPatientPrescription);

            setViewVisibility();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the view visibility as per the array size
     */
    private void setViewVisibility() {
        if (mArrPatientPrescription.size() != 0) {
            mRelativeNoData.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mRelativeNoData.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    /**
     * Show progress view when page load for the first time
     */
    private void showProgressView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.VISIBLE);

                RecyclerProgressView.startProgress((LinearLayout) mView.findViewById(R.id.linear_add_recycler_progress_view),
                        R.layout.row_receptionist_payment_item, getActivity());
            }
        }, 100);
    }

    /**
     * Show progress view when page load for the first time
     */
    private void stopProgressView() {
        try {
            mSwipeRefreshView.setRefreshing(false);
            isRefresh = false;
            isLoadMore = false;
            RecyclerProgressView.stopProgress();
            mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            hideProgressBar();
        }
    }

    /**
     * Sets up the Add Prescriptions Fragment
     */
    private void callToAddPrescriptionsFragment() {
        try {
            android.support.v4.app.Fragment fragment = new AddMedicationFragment();
            FragmentManager fragmentManager =
                    ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_prescriptions))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_prescriptions))
                    .commit();
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Hide the progress bar
     */
    private void hideProgressBar() {
        try {
            if (mArrPatientPrescription != null && mArrPatientPrescription.size() > 0 &&
                    mArrPatientPrescription.get(mArrPatientPrescription.size() - 1) == null) {

                mArrPatientPrescription.remove(mArrPatientPrescription.size() - 1);
                mAdapterPatientPrescription.notifyItemRemoved(mArrPatientPrescription.size());

                mAdapterPatientPrescription.notifyDataSetChanged();
                mAdapterPatientPrescription.setLoaded();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * On load more click listener
     */
    private void setLoadMoreClickListener() {
        mAdapterPatientPrescription.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (mArrPatientPrescription != null && mArrPatientPrescription.size() > 0 && mArrPatientPrescription.size() <
                        totalRecords) {
                    if (!mAppUtils.getConnectionState()) {
                        mRelativeNoInternet.setVisibility(View.VISIBLE);
                        mArrPatientPrescription.clear();
                        setViewVisibility();
                    } else {
                        mRelativeNoInternet.setVisibility(View.GONE);
                        if (!isRefresh) {
                            isLoadMore = true;
                            mArrPatientPrescription.add(null);
                            mAdapterPatientPrescription.notifyItemInserted(mArrPatientPrescription.size() - 1);

                            currentPageIndex = (mArrPatientPrescription.size() / Common.getPageSizeForPagination(mActivity)) + 1;
                            callPrescriptionListAPI();
                        }
                    }
                }
            }
        });
    }

    /**
     * Receptionist Patient Detail Appointment Fragment on visible
     *
     * @param isVisibleToUser - This can check the fragment is visible when swiping and will return
     *                        the boolean value based on that
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        isVisible = isVisibleToUser;
        if (isStarted && isVisible) {
            mArrPatientPrescription.clear();

            callShimmerView();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
            mArrPatientPrescription.clear();
            if (mAdapterPatientPrescription != null) {
                mAdapterPatientPrescription.notifyDataSetChanged();
            }
            getBundle();
            callShimmerView();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isVisible = false;
        isStarted = false;
    }
}