package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.badoualy.stepperindicator.StepperIndicator;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.AddPaymentPatientSearchAdapter;
import com.trackopd.adapter.ViewPagerAdapter;
import com.trackopd.model.BillingListItemModel;
import com.trackopd.model.ReceptionistPatientModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.DotsIndicator;

import java.util.ArrayList;

public class AddPaymentFragment extends Fragment implements AddPaymentPatientSearchAdapter.onSelectedList {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private LinearLayout mLinearPrevious, mLinearNext;
    private ViewPager mViewPager;
    private int pageIndex = 0;
    private DotsIndicator mDotsIndicator;
    public static String mFieldName = "";
    public static String mFieldValue = "",mPatientID="",mPaymentDate="",mAppointmentNo="",mNotes=""
            ,mProfilePic=AppConstants.STR_EMPTY_STRING;

    public static ReceptionistPatientModel receptionistPatientModel;

    private StepperIndicator mStepIndicator;
    public static ArrayList<BillingListItemModel> mArrayBillingItem;
    private boolean EditPayment=false;
    private String mFirstName,mLastName,mPatientMobile;
    private Bundle bundle;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_add_payment, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        mFieldName = AppConstants.STR_EMPTY_STRING;
        mFieldValue = AppConstants.STR_EMPTY_STRING;

        mArrayBillingItem=new ArrayList<>();
        getBundle();
        getIds();
        setRegListeners();
        setViewPager();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * get Bundle Value
     **/
    private void getBundle() {
      try {
          bundle=this.getArguments();
          if (bundle!=null){
              mFirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
              mLastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
              mPatientMobile = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
              mPatientID = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
              mAppointmentNo = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
              mProfilePic=bundle.getString(AppConstants.BUNDLE_PATIENT_PROFILE);
          }

      }catch (Exception e){
          Common.insertLog(e.getMessage());
      }
    }

    /**
     * Set the prepare menu options
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);

        MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(false);
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // View Pager
            mViewPager = mView.findViewById(R.id.view_pager_add_payment);

            // Linear Layouts
            mLinearPrevious = mView.findViewById(R.id.linear_add_payment_prev);
            mLinearNext = mView.findViewById(R.id.linear_add_payment_next);

            //Step Indicator
            mStepIndicator=mView.findViewById(R.id.step_indicator_add_payment);
            // Image Views
            ImageView mImagePrev = mView.findViewById(R.id.image_add_payment_prev);
            ImageView mImageNext = mView.findViewById(R.id.image_add_payment_next);

            // Sets the image color
            mImagePrev.setColorFilter(Common.setThemeColor(mActivity));
            mImageNext.setColorFilter(Common.setThemeColor(mActivity));

            mLinearNext.setVisibility(View.VISIBLE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mLinearPrevious.setOnClickListener(clickListener);
            mLinearNext.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the view pager and binding the tab layouts
     */
    private void setViewPager() {
        try {
            ViewPagerAdapter adapter = new ViewPagerAdapter(((ReceptionistHomeActivity) mActivity).getSupportFragmentManager());

            if(bundle!=null){
                adapter.addFragment(new AddPaymentStepTwoFragment(), "Appointment",bundle);
                adapter.addFragment(new AddServiceAndPackage(),"Services");
                adapter.addFragment(new BillingSummaryFragment(),"Summary");
            }else {
                adapter.addFragment(new AddPaymentStepOneFragment(),"Search");
                adapter.addFragment(new ReceptionistPaymentPatientSearchList(), "Patient");
                adapter.addFragment(new AddPaymentStepTwoFragment(), "Appointment");
                adapter.addFragment(new AddServiceAndPackage(),"Services");
                adapter.addFragment(new BillingSummaryFragment(),"Summary");
            }
            mViewPager.setAdapter(adapter);
            mStepIndicator.setViewPager(mViewPager, mViewPager.getAdapter().getCount());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.linear_add_payment_next:
                    onPrevNext();
                    break;

                case R.id.linear_add_payment_prev:
                    onPreviousIndex();
                    break;
            }
        }
    };

    /**
     * set Previous Step counted
     */
    private void onPreviousIndex() {

        if(bundle!=null){
           switch (pageIndex){
               case 0:
                   pageIndex = pageIndex - 1;
                   mViewPager.setCurrentItem(pageIndex);
                   mViewPager.setCurrentItem(pageIndex);

                   break;
               case 1:
                   pageIndex = pageIndex - 1;
                   mViewPager.setCurrentItem(pageIndex);
                   mViewPager.setCurrentItem(pageIndex);
                   break;
               case 2:
                   pageIndex = pageIndex - 1;
                   mViewPager.setCurrentItem(pageIndex);
                   mViewPager.setCurrentItem(pageIndex);
                   mLinearNext.setVisibility(View.VISIBLE);
                   break;
           }
        }else {
            switch (pageIndex){
                case 0:
                    pageIndex = pageIndex - 1;
                    mLinearPrevious.setVisibility(View.GONE);
                    mLinearNext.setVisibility(View.VISIBLE);
                    mViewPager.setCurrentItem(0);
                    break;
                case 1:
                    pageIndex = pageIndex - 1;
                    mViewPager.setCurrentItem(pageIndex);

                    break;
                case 2:
                    pageIndex = pageIndex - 1;
                    mViewPager.setCurrentItem(pageIndex);
                    mViewPager.setCurrentItem(pageIndex);

                    break;
                case 3:
                    pageIndex = pageIndex - 1;
                    mViewPager.setCurrentItem(pageIndex);
                    mViewPager.setCurrentItem(pageIndex);
                    break;
                case 4:
                    pageIndex = pageIndex - 1;
                    mViewPager.setCurrentItem(pageIndex);
                    mViewPager.setCurrentItem(pageIndex);
                    mLinearNext.setVisibility(View.VISIBLE);
                    break;
            }
        }


    }

    /**
     * on Next Step clicked
     */
    private void onPrevNext() {

      if (bundle!=null){
          switch (pageIndex){
              case 0:
                  mLinearPrevious.setVisibility(View.GONE);
                  mLinearNext.setVisibility(View.VISIBLE);
                  AddPaymentStepTwoFragment.setDetailI();
                  pageIndex = pageIndex + 1;
                  mViewPager.setCurrentItem(pageIndex);

                  break;
              case 1:
                  mLinearPrevious.setVisibility(View.VISIBLE);
                  if (checkServiceItem()){

                      pageIndex = pageIndex + 1;
                      mViewPager.setCurrentItem(pageIndex);
                      mLinearNext.setVisibility(View.GONE);
                  }
                  break;
              case 2:
                  mLinearPrevious.setVisibility(View.VISIBLE);
                  pageIndex = pageIndex + 1;
                  mViewPager.setCurrentItem(pageIndex);
                  break;
          }
      }else{
          switch (pageIndex){
            case 0:
                mLinearNext.setVisibility(View.VISIBLE);
                if (checkValidationForFirstFragment()) {
                   pageIndex = pageIndex + 1;
                   mViewPager.setCurrentItem(pageIndex);
                   mLinearPrevious.setVisibility(View.VISIBLE);
                }
                break;
            case 1:
                 if (checkValidationForSecondFragment()) {
                    pageIndex = pageIndex + 1;
                    mViewPager.setCurrentItem(pageIndex);
                    mLinearNext.setVisibility(View.VISIBLE);
                 }
               break;
            case 2:

                 AddPaymentStepTwoFragment.setDetailI();
                 pageIndex = pageIndex + 1;
                 mViewPager.setCurrentItem(pageIndex);
                break;
            case 3:
                if (checkServiceItem()){
                    mLinearPrevious.setVisibility(View.VISIBLE);
                    pageIndex = pageIndex + 1;
                    mViewPager.setCurrentItem(pageIndex);
                    mLinearNext.setVisibility(View.GONE);
                }
                break;
            case 4:
                mLinearPrevious.setVisibility(View.VISIBLE);
                pageIndex = pageIndex + 1;
                mViewPager.setCurrentItem(pageIndex);
                break;
        }
      }
    }

    /**
     *
     * @return
     */
    private boolean checkServiceItem() {
        boolean status=true;
        if (mArrayBillingItem.size()==0){
            Common.setCustomToast(mActivity,mActivity.getResources().
                    getString(R.string.error_please_select_service));
            status=false;
        }
        return status;
    }

    /**
     * Check the validation for the first fragment*/

    private boolean checkValidationForFirstFragment() {
        boolean status = true;

        if (mFieldName.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.val_select_field));
            status = false;
        }

        if (mFieldValue.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            Common.setCustomToast(mActivity, getResources().getString(R.string.val_enter_search_text));
            status = false;
        }
        return status;
    }

    /**
     * Check if the patient is selected or not for the second fragment*/

    private boolean checkValidationForSecondFragment() {
        boolean status = true;

        if (ReceptionistPaymentPatientSearchList.mArrReceptionistPatients.size() > 0) {
            if (receptionistPatientModel == null) {
                status = false;
                Common.setCustomToast(mActivity, getString(R.string.error_select_patient));
            }
        } else {
            status = false;
            Common.setCustomToast(mActivity, getString(R.string.error_patient_not_found));
        }
        return status;
    }

    /**
     * on Item Selected
     *
     * @param receptionistPatientsModel - Receptionist Patients Model*/

    @Override
    public void onSelected(ReceptionistPatientModel receptionistPatientsModel) {
        if (receptionistPatientsModel != null) {
            receptionistPatientModel = receptionistPatientsModel;
            mLinearNext.setClickable(true);
           // mTextNext.setTextColor(Common.setThemeColor(mActivity));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        receptionistPatientModel = null;
        mFieldName = AppConstants.STR_EMPTY_STRING;
        mFieldValue = AppConstants.STR_EMPTY_STRING;
    }

    public void setSecoundPage() {
      mViewPager.setCurrentItem(1);
    }
}
