package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.model.AddFollowUpModel;
import com.trackopd.model.CouncillorModel;
import com.trackopd.model.DoctorModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.GenerateTimeSlot;
import com.trackopd.utils.KeyboardUtility;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddFollowUpFragment extends Fragment {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private Button mButtonSubmit;
    private EditText mEditNotes;
    private Spinner mSpinnerDoctor, mSpinnerCouncillor;
    private ImageView mImageProfile, mImageDoctor, mImageCouncillor;
    private TextView mTextPatientName, mTextPatientMobile;
    private EditText mEditDate, mEditTime;
    private String mAddPatient, mFirstName, mLastName, mMobileNo, mPatientId, mUserType,mProfileImage=AppConstants.STR_EMPTY_STRING;
    private ArrayList<String> mArrDoctor;
    private ArrayList<DoctorModel> mArrDoctorList;
    private ArrayList<String> mArrCouncillor;
    private ArrayList<CouncillorModel> mArrCouncillorList;
    private int mSelectedDoctorIndex, mSelectedCouncillorIndex, mDoctorId, mCouncillorId;
    private ViewGroup mGroupRevealAnimation;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_add_follow_up, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrDoctor = new ArrayList<>();
        mArrDoctorList = new ArrayList<>();
        mArrCouncillor = new ArrayList<>();
        mArrCouncillorList = new ArrayList<>();
        mGroupRevealAnimation = container;
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);
        getBundle();
        getIds();
        setRegListeners();
        setData();
        callToDoctorAPI();
        callToCouncillorAPI();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);

        MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mAddPatient = bundle.getString(AppConstants.BUNDLE_ADD_PATIENT);
                mFirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mLastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mMobileNo = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mProfileImage=bundle.getString(AppConstants.BUNDLE_PATIENT_PROFILE);
            }
            Common.insertLog("Add Follow Up::::> " + mAddPatient);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Image Views
            mImageProfile = mView.findViewById(R.id.image_add_follow_up_profile_pic);
            mImageDoctor = mView.findViewById(R.id.image_add_follow_up_doctor);
            mImageCouncillor = mView.findViewById(R.id.image_add_follow_up_councillor);

            // Text Views
            mTextPatientName = mView.findViewById(R.id.text_add_follow_up_patient_name);
            mTextPatientMobile = mView.findViewById(R.id.text_add_follow_up_patient_mobile_no);

            // Edit Texts
            mEditDate = mView.findViewById(R.id.edit_add_follow_up_date);
            mEditNotes = mView.findViewById(R.id.edit_add_follow_up_note);
            mEditTime=mView.findViewById(R.id.edit_add_follow_up_time);

            // Spinners
            mSpinnerCouncillor = mView.findViewById(R.id.spinner_add_follow_up_councillor);
            mSpinnerDoctor = mView.findViewById(R.id.spinner_add_follow_up_doctor);

            // Buttons
            mButtonSubmit = mView.findViewById(R.id.button_add_follow_up_submit);

            // Set Request Focus
            //mEditNotes.requestFocus();
            mEditDate.setKeyListener(null);
            mEditTime.setKeyListener(null);
            mEditNotes.setKeyListener(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            //ToDo: Click Listeners
            mEditDate.setOnClickListener(clickListener);
            mButtonSubmit.setOnClickListener(clickListener);
            mEditTime.setOnClickListener(clickListener);

            // ToDo: Doctor - Spinner Item Select Listener
            mSpinnerDoctor.setOnItemSelectedListener(onItemSelectedListener);
            mSpinnerCouncillor.setOnItemSelectedListener(onItemSelectedListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            mTextPatientName.setText(mFirstName + AppConstants.STR_EMPTY_SPACE + mLastName);
            mTextPatientMobile.setText(mMobileNo);

            // Set image to the selected view
            if (mProfileImage!=null && !mProfileImage.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                //Set Thumbnail
                String thumbmail_url= WebFields.API_BASE_URL+WebFields.IMAGE_THUMBNAIL_URL+mProfileImage;
                Glide.with(mActivity)
                        .load(thumbmail_url)
                        .apply(new RequestOptions().error(R.drawable.ic_documents_img).placeholder(R.drawable.ic_documents_img))
                        .into(mImageProfile);
            }else {
                mImageProfile.setImageDrawable(Common.setLabeledImageView(mActivity, mFirstName, mLastName));
            }
            
            // Set current date and time
            mEditDate.setText(Common.setCurrentDate(mActivity));

            // Set Image Color as per the theme applied
            mImageDoctor.setColorFilter(Common.setThemeColor(mActivity));
            mImageCouncillor.setColorFilter(Common.setThemeColor(mActivity));

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerCouncillor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerDoctor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.edit_add_follow_up_date:
                    Common.openDatePicker(mActivity, mEditDate);
                    break;

                case R.id.button_add_follow_up_submit:
                    doAddFollowUp();
                    break;

                case R.id.edit_add_follow_up_time:
                   // GenerateTimeSlot.openTimeSlotDialog(mActivity, mEditTime);
                    break;
            }
        }
    };

    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {
                case R.id.spinner_add_follow_up_doctor:
                    mSelectedDoctorIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedDoctorIndex != 0) {
                        mDoctorId = (Integer.parseInt(mArrDoctorList.get(mSelectedDoctorIndex - 1).getDoctorID()));
                        String mDoctorName = (mArrDoctorList.get(mSelectedDoctorIndex - 1).getDoctorName());
                        Common.insertLog("mDoctorID::> " + mDoctorId);
                        Common.insertLog("mDoctorName::> " + mDoctorName);
                    }
                    break;

                case R.id.spinner_add_follow_up_councillor:
                    mSelectedCouncillorIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedCouncillorIndex != 0) {
                        mCouncillorId = (Integer.parseInt(mArrCouncillorList.get(mSelectedCouncillorIndex - 1).getCouncillorID()));
                        String mCouncillorName = (mArrCouncillorList.get(mSelectedCouncillorIndex - 1).getCouncillorName());
                        Common.insertLog("mCouncillorId::> " + mCouncillorId);
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    /**
     * This method should call the Doctor API
     */
    private void callToDoctorAPI() {
        try {
            String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setDoctorListJson(hospital_database));

            Call<DoctorModel> call = RetrofitClient.createService(ApiInterface.class).getDoctorList(body);
            call.enqueue(new Callback<DoctorModel>() {
                @Override
                public void onResponse(@NonNull Call<DoctorModel> call, @NonNull Response<DoctorModel> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrDoctorList.addAll(response.body().getData());
                            }
                            setsDoctorAdapter();
                        } else {
                            setsDoctorAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DoctorModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setsDoctorAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsDoctorAdapter() {
        try {
            mArrDoctor.add(0, mActivity.getResources().getString(R.string.spinner_select_doctor));
            if (mArrDoctorList.size() > 0) {
                for (DoctorModel doctorModel : mArrDoctorList) {
                    mArrDoctor.add(doctorModel.getDoctorName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerDoctor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrDoctor) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedDoctorIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerDoctor.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data to the councillor spinner
     */
    private void callToCouncillorAPI() {
        try {

            String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setCouncillorListJson(hospital_database));

            Call<CouncillorModel> call = RetrofitClient.createService(ApiInterface.class).getCouncillorList(body);
            call.enqueue(new Callback<CouncillorModel>() {
                @Override
                public void onResponse(@NonNull Call<CouncillorModel> call, @NonNull Response<CouncillorModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrCouncillorList.addAll(response.body().getData());
                            }
                            setsCouncillorAdapter();
                        } else {
                            setsCouncillorAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CouncillorModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setsCouncillorAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsCouncillorAdapter() {
        try {
            mArrCouncillor.add(0, getString(R.string.spinner_select_councillor));
            if (mArrCouncillorList.size() > 0) {
                for (CouncillorModel councillorModel : mArrCouncillorList) {
                    mArrCouncillor.add(councillorModel.getCouncillorName());
                }
            }
            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerCouncillor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_dropdown_item, mArrCouncillor) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCouncillorIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerCouncillor.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method checks the validation first then call the API
     */
    private void doAddFollowUp() {
        KeyboardUtility.HideKeyboard(mActivity, mEditNotes);
        if (checkValidation()) {
            callToAddFollowUpAPI();
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {
        boolean status = true;

        String mDateTime = mEditDate.getText().toString();

        if (mDateTime.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            mEditDate.requestFocus();
            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.val_select_dat_time));
            status = false;
        }

        if (mSpinnerDoctor.getSelectedItemPosition() == 0) {
            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.val_select_doctor));
            status = false;
        }

        if (mSpinnerCouncillor.getSelectedItemPosition() == 0) {
            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.val_select_councillor));
            status = false;
        }
        return status;
    }

    /**
     * This Method Call To Add Follow Up API
     */
    private void callToAddFollowUpAPI() {
        try {
            String mConvertedDate = Common.convertDateToServer(mActivity, mEditDate.getText().toString().trim());
            String mConvertedTime = Common.convertTime(mActivity, mEditTime.getText().toString().trim());
            String mDateTime = mConvertedDate + AppConstants.STR_EMPTY_SPACE +mConvertedTime;

            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
            String mNotes = mEditNotes.getText().toString().trim();

            String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setReceptionistAddFollowUpJson(mPatientId, mDoctorId, mCouncillorId, mDateTime, mUserId, mNotes,hospital_database));

            Call<AddFollowUpModel> call = RetrofitClient.createService(ApiInterface.class).addReceptionistFollowUp(requestBody);
            call.enqueue(new Callback<AddFollowUpModel>() {
                @Override
                public void onResponse(@NonNull Call<AddFollowUpModel> call, @NonNull Response<AddFollowUpModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            Common.setCustomToast(mActivity, mMessage);
                            if (response.body() != null) {
                                String mFollowUpNo = response.body().getData().get(0).getID();
                                Common.insertLog("mFollowUpNo:::> " + mFollowUpNo);
                                openFollowUpDialog(mFollowUpNo);
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddFollowUpModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open up the Follow Up dialog
     */
    @SuppressLint("SetTextI18n")
    public void openFollowUpDialog(String mFollowUpNo) {
        try {
            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.custom_dialog_follow_up);

            // Text Views
            TextView mTextFollowUpNo = dialog.findViewById(R.id.text_dialog_receptionist_follow_up_number);
            TextView mTextName = dialog.findViewById(R.id.text_dialog_receptionist_follow_up_name);
            TextView mTextMobile = dialog.findViewById(R.id.text_dialog_receptionist_follow_up_mobile_no);
            TextView mTextDate = dialog.findViewById(R.id.text_dialog_receptionist_follow_up_date);
            TextView mTextTime = dialog.findViewById(R.id.text_dialog_receptionist_follow_up_time);
            TextView mTextDoctorName = dialog.findViewById(R.id.text_dialog_receptionist_follow_up_doctor_name);
            TextView mTextCouncillorName = dialog.findViewById(R.id.text_dialog_receptionist_follow_up_councillor_name);
            TextView mTextDescription = dialog.findViewById(R.id.text_dialog_receptionist_follow_up_description);

            // Button
            Button mButtonOk = dialog.findViewById(R.id.button_dialog_receptionist_follow_up_ok);

            // Set values to the text views
            mTextFollowUpNo.setText(Common.isEmptyString(mActivity, mFollowUpNo));
            mTextName.setText(Common.isEmptyString(mActivity, mFirstName) + AppConstants.STR_EMPTY_SPACE + Common.isEmptyString(mActivity, mLastName));
            mTextMobile.setText(Common.isEmptyString(mActivity, mMobileNo));
            mTextDate.setText(Common.isEmptyString(mActivity, mEditDate.getText().toString().trim()));
            mTextTime.setText(Common.isEmptyString(mActivity, mEditTime.getText().toString()));
            mTextDoctorName.setText(Common.isEmptyString(mActivity, mSpinnerDoctor.getSelectedItem().toString()));
            mTextCouncillorName.setText(Common.isEmptyString(mActivity, mSpinnerCouncillor.getSelectedItem().toString()));
            mTextDescription.setText(Common.isEmptyString(mActivity, mEditNotes.getText().toString().trim()));

            // ToDo: Button Ok Click Listener
            mButtonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    //Toast.makeText(mActivity, "Your follow up has been added successfully.", Toast.LENGTH_SHORT).show();
                    /*removeAllFragments();
                    redirectedToReceptionistFollowUpFragment();*/
                    mActivity.onBackPressed();
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Used to remove all fragment in the fragment manager after logout
     */
    private void removeAllFragments() {
        try {
            List<Fragment> fragments = null;

            if (mAddPatient.equalsIgnoreCase("1")) {
                fragments = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mAddPatient.equalsIgnoreCase("2")) {
                fragments = ((OptometristHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mAddPatient.equalsIgnoreCase("3")) {
                fragments = ((DoctorHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mAddPatient.equalsIgnoreCase("4")) {
                fragments = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            }

            if (fragments.size() > 0) {
                for (int i = 0; i < fragments.size(); i++) {
                    if (fragments.get(i) != null) {

                        if (mAddPatient.equalsIgnoreCase("1")) {
                            ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        } else if (mAddPatient.equalsIgnoreCase("2")) {
                            ((OptometristHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        } else if (mAddPatient.equalsIgnoreCase("3")) {
                            ((DoctorHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        } else if (mAddPatient.equalsIgnoreCase("4")) {
                            ((CouncillorHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method redirected you to the Receptionist Follow Up Fragment
     */
    private void redirectedToReceptionistFollowUpFragment() {
        try {
            Fragment fragment = new ReceptionistFollowUpFragment();
            Bundle args = new Bundle();

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_follow_up));
//                args.putString(AppConstants.BUNDLE_ADD_FOLLOW_UP, "1");
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_follow_up));
//                args.putString(AppConstants.BUNDLE_ADD_FOLLOW_UP, "2");
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_follow_up));
//                args.putString(AppConstants.BUNDLE_ADD_FOLLOW_UP, "4");
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_follow_up));
//                args.putString(AppConstants.BUNDLE_ADD_FOLLOW_UP, "3");
            }

            args.putString(AppConstants.BUNDLE_PATIENT_NAME, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_FOLLOW_UP_DATE, "1970-01-01");
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, -1);
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_COUNCILLOR, -1);
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.nav_menu_follow_up))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mView.removeCallbacks(Common.setAnimation(mActivity, mView, mGroupRevealAnimation));
    }
}