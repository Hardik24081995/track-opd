package com.trackopd.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.trackopd.R;
import com.trackopd.adapter.HistorySurgeryAdapter;
import com.trackopd.model.AddMedicationModel;
import com.trackopd.model.AddMediclaimModel;
import com.trackopd.model.CloseFileModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.SessionManager;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.StringTokenizer;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckUpSurgeryFragment extends Fragment implements HistorySurgeryAdapter.onHistoryDiagnosisListener {

//    CloseFiles

    private View mView;
    private Activity mActivity;
    private ImageView mImageContainer;
    private LinearLayout mLinearContainer;
    private RelativeLayout mRelativeDataNotFound;

    private ArrayList<PreliminaryExaminationDetailsModel.Data.Eye> mArrEye;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.AnaesthesiaType> mArrAnaesthesiaType;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.SurgeryType> mArrSurgeryType;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.ImplantBrand> mArrImplantBrand;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.ImplantName> mArrImplantName;
    private ArrayList<HistoryPreliminaryExaminationModel.Data> mArrHistory;

    private ArrayList<String> mArrEyeName, mArrAnaesthesiaTypeName, mArrSurgeryTypeName,
            mArrImplantBrandName, mArrImplantNameString;

    private int mSelectedAnaesthesiaEyeIndex, mSelectedAnaesthesiaTypeIndex, mSelectedSurgeryIndex, mSelectedSurgeryIndexTwo, mSelectedSurgeryEyeIndex;

    private Button mButtonSubmit, mButtonMediclaim, mButtonAnaesthesia, mButtonNotes, mButtonImplant;

    private EditText mEditImplantExpireDate, mEditSuregeryNoteStartTime, mEditSuregeryNoteEndTime,
            mEditCompany, mEditTpa, mEditExpireDate, mEditPolicyNo, mEditSettlementAmount, mEditPulse,
            mEditBP, mEditSPO2, mEditRBS, mEditAnaesthesiaMedicine,
            mEditAConstant, mEditImplantPower, mEditImplant, mEditPlacement, mEditImplantSlNo, mEditNote, mEditAcd, mEditAL, mEditIncisionSize, mEditIncisionType, mEditViscoelastics,
            mEditSurgeryRemarks, mEditSystolic, mEditDiastolic;

    private AutoCompleteTextView mEditImplantBrand, mEditImplantName;

    private ExpandableLayout mExpandableMediclaim, mExpandableAnaesthesia, mExpandableSurgeryNotes,
            mExpandableImplant;

    private String strSettlement, mPatientId, mDoctorId, mTreatmentDate, mUserId, hospital_database,
            mPreliminaryExaminationID;
    private int mAppointmentId, mCurrentPage = 1, mHistoryDoctorID = -1;

    private Spinner mSpinnerAnaesthesiaEye, mSpinnerAnaesthesiaType, mSpinnerSurgeryNoteEye,
            mSpinnerSurgeryNoteSurgery, mSpinnerSurgeryNoteSurgeryTwo;
    private RadioGroup mRadioSettlement;
    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_check_up_surgery_submit:
                    callAddSurgery();
                    break;
            }
        }
    };
    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {
                case R.id.spinner_row_check_up_surgery_anaesthesia_eye:
                    mSelectedAnaesthesiaEyeIndex = adapterView.getSelectedItemPosition();
                    break;
                case R.id.spinner_row_check_up_surgery_anaesthesia_type:
                    mSelectedAnaesthesiaTypeIndex = adapterView.getSelectedItemPosition();
                    break;
                case R.id.spinner_row_check_up_surgery_notes_sugery:
                    mSelectedSurgeryIndex = adapterView.getSelectedItemPosition();
                    break;
                case R.id.spinner_row_check_up_surgery_notes_sugery_two:
                    mSelectedSurgeryIndexTwo = adapterView.getSelectedItemPosition();
                    break;
                case R.id.spinner_row_check_up_surgery_notes_eye:
                    mSelectedSurgeryEyeIndex = adapterView.getSelectedItemPosition();
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_check_up_surgery, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        mArrEye = new ArrayList<>();
        mArrAnaesthesiaType = new ArrayList<>();
        mArrSurgeryType = new ArrayList<>();
        mArrHistory = new ArrayList<>();

        mArrEyeName = new ArrayList<>();
        mArrAnaesthesiaTypeName = new ArrayList<>();
        mArrSurgeryTypeName = new ArrayList<>();
        mArrImplantBrand = new ArrayList<>();
        mArrImplantBrandName = new ArrayList<>();
        mArrImplantName = new ArrayList<>();
        mArrImplantNameString = new ArrayList<>();

        strSettlement = mActivity.getString(R.string.text_yes);

        getBundle();
        getIds();
        addInvestigationContainer();
        setRegListeners();
        callToPreliminaryExaminationAPI();

        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
                hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mAppointmentId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mDoctorId = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);
                mTreatmentDate = bundle.getString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE);
            }

            mPreliminaryExaminationID = AddNewPreliminaryExaminationFragment.getPrelimary();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * get Declare Ids
     */
    private void getIds() {
        try {
            // Image Views
            mImageContainer = mView.findViewById(R.id.image_check_up_surgery_add);
            // Linear Layouts
            mLinearContainer = mView.findViewById(R.id.linear_check_up_surgery_container);
            //Button
            mButtonSubmit = mView.findViewById(R.id.button_check_up_surgery_submit);

            mRelativeDataNotFound = mView.findViewById(R.id.relative_surgery_no_data_found);
            //Set Visiblity
            mImageContainer.setVisibility(View.GONE);

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Register Listener
     */
    private void setRegListeners() {
        try {
            mImageContainer.setOnClickListener(clickListener);

            mButtonSubmit.setOnClickListener(clickListener);

            //Disable Emoji keyeword
            mEditCompany.setFilters(Common.setFilter());
            mEditCompany.setFilters(new InputFilter[]{new InputFilter.LengthFilter(20)});
            mEditTpa.setFilters(Common.getFilter());
            mEditTpa.setFilters(new InputFilter[]{new InputFilter.LengthFilter(15)});
            mEditPolicyNo.setFilters(Common.getFilter());
            mEditPolicyNo.setFilters(new InputFilter[]{new InputFilter.LengthFilter(12)});
            mEditAnaesthesiaMedicine.setFilters(Common.getFilter());
            mEditAnaesthesiaMedicine.setFilters(new InputFilter[]{new InputFilter.LengthFilter(15)});
            mEditIncisionType.setFilters(Common.getFilter());
            mEditViscoelastics.setFilters(Common.getFilter());
            mEditSurgeryRemarks.setFilters(Common.getFilter());
            mEditImplant.setFilters(Common.getFilter());
            mEditPlacement.setFilters(Common.getFilter());
            mEditImplantSlNo.setFilters(Common.getFilter());
            mEditNote.setFilters(Common.getFilter());

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Data on Fragment Check Up Surgery
     */
    private void setData() {

        mEditSuregeryNoteStartTime.setText(Common.setCheckCurrentDateTime(mActivity));
        mEditSuregeryNoteEndTime.setText(Common.setCheckCurrentDateTime(mActivity));
    }

    /**
     * Set Data on Fragment Check Up Surgery
     */
    private void setFirstTimeData() {
        String currentDate = Common.setCurrentDate(mActivity);
        String covertTreatmentDate = Common.convertDateUsingDateFormat(mActivity, mTreatmentDate,
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen),
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));

        if (currentDate.equalsIgnoreCase(covertTreatmentDate)) {
            mRelativeDataNotFound.setVisibility(View.GONE);
            //.setDataCompolains();
        } else {
            mRelativeDataNotFound.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Add Investigation Container
     */
    private void addInvestigationContainer() {

        View view = LayoutInflater.from(mActivity.getBaseContext()).
                inflate(R.layout.row_check_up_surgery_item, null, false);

        //Linear Layout
        LinearLayout mLinearMediclaim = view.findViewById(R.id.linear_row_check_up_mediclaim_expandable_view);
        LinearLayout mLinearAnaesthesia = view.findViewById(R.id.linear_row_check_up_anaesthesia_expandable_view);
        LinearLayout mLinearSurgeryNotes = view.findViewById(R.id.linear_row_check_up_surgery_notes_expandable_view);
        LinearLayout mLinearImplant = view.findViewById(R.id.linear_row_check_up_implant_expandable_view);
        LinearLayout mLinearSettlement = view.findViewById(R.id.linear_row_check_up_surgery_mediclaim_settlement_amount);

        //Expandble Layout
        mExpandableMediclaim = view.findViewById(R.id.expandable_layout_row_check_up_mediclaim);
        mExpandableAnaesthesia = view.findViewById(R.id.expandable_layout_row_check_up_anaesthesia);
        mExpandableSurgeryNotes = view.findViewById(R.id.expandable_layout_row_check_up_surgery_notes);
        mExpandableImplant = view.findViewById(R.id.expandable_layout_row_check_up_implant);

        // Image View
        ImageView imageArrowMediclaim = view.findViewById(R.id.image_row_check_up_mediclaim_expandable_view);
        ImageView imageArrowAnaesthesia = view.findViewById(R.id.image_row_check_up_anaesthesia_expandable_view);
        ImageView imageArrowSurgeryNote = view.findViewById(R.id.image_row_check_up_surgery_notes_expandable_view);
        ImageView imageArrowImplant = view.findViewById(R.id.image_row_check_up_implant_expandable_view);
        ImageView imageRemove = view.findViewById(R.id.image_row_check_up_mediclaim_remove);

        imageRemove.setVisibility(View.GONE);

        //Radio Group
        mRadioSettlement = view.findViewById(R.id.radio_group_row_check_up_surgery_mediclaim_settlement_received);
        RadioButton mRadioMediclamYes = view.findViewById(R.id.radio_button_row_check_up_surgery_mediclaim_yes);

        //Buttons
        mButtonMediclaim = view.findViewById(R.id.button_check_up_surgery_submit_medicalim);
        mButtonAnaesthesia = view.findViewById(R.id.button_check_up_surgery_submit_anesthesia);
        mButtonNotes = view.findViewById(R.id.button_check_up_surgery_submit_notes);
        mButtonImplant = view.findViewById(R.id.button_check_up_surgery_submit_implant);

        //Spinner
        mSpinnerAnaesthesiaEye = view.findViewById(R.id.spinner_row_check_up_surgery_anaesthesia_eye);
        mSpinnerAnaesthesiaType = view.findViewById(R.id.spinner_row_check_up_surgery_anaesthesia_type);
        mSpinnerSurgeryNoteEye = view.findViewById(R.id.spinner_row_check_up_surgery_notes_eye);
        mSpinnerSurgeryNoteSurgery = view.findViewById(R.id.spinner_row_check_up_surgery_notes_sugery);
        mSpinnerSurgeryNoteSurgeryTwo = view.findViewById(R.id.spinner_row_check_up_surgery_notes_sugery_two);


        // Edit Text
        mEditImplantExpireDate = view.findViewById(R.id.edit_row_check_up_surgery_implant_expiry_date);
        mEditSuregeryNoteStartTime = view.findViewById(R.id.edit_row_check_up_surgery_notes_start_time);
        mEditSuregeryNoteEndTime = view.findViewById(R.id.edit_row_check_up_surgery_notes_end_time);
        mEditCompany = view.findViewById(R.id.edit_text_row_checkup_surgery_item_company);
        mEditTpa = view.findViewById(R.id.edit_text_row_checkup_surgery_item_tpa);
        mEditExpireDate = view.findViewById(R.id.edit_row_check_up_surgery_mediclaim_expires_on);
        mEditPolicyNo = view.findViewById(R.id.edit_row_check_up_surgery_mediclaim_policy_no);
        mEditSettlementAmount = view.findViewById(R.id.edit_row_check_up_surgery_mediclaim_settlement_amount);

        mEditPulse = view.findViewById(R.id.edit_row_check_up_surgery_anaesthesia_pulse);
        //mEditBP=view.findViewById(R.id.edit_row_check_up_surgery_anaesthesia_bp);
        mEditSystolic = view.findViewById(R.id.edit_row_check_up_surgery_anaesthesia_bp_systolic);
        mEditDiastolic = view.findViewById(R.id.edit_row_check_up_surgery_anaesthesia_diastolic);

        mEditSPO2 = view.findViewById(R.id.edit_row_check_up_surgery_anaesthesia_spo_two);
        mEditRBS = view.findViewById(R.id.edit_row_check_up_surgery_anaesthesia_rbs);
        mEditAnaesthesiaMedicine = view.findViewById(R.id.edit_row_check_up_surgery_anaesthesia_medicine);

        mEditImplantBrand = view.findViewById(R.id.edit_row_check_up_surgery_implant_brand);
        mEditImplantName = view.findViewById(R.id.edit_row_check_up_surgery_implant_name);
        mEditAConstant = view.findViewById(R.id.edit_row_check_up_surgery_implant_a_constant);
        mEditImplantPower = view.findViewById(R.id.edit_row_check_up_surgery_implant_power);
        mEditImplant = view.findViewById(R.id.edit_row_check_up_surgery_implant);
        mEditPlacement = view.findViewById(R.id.edit_row_check_up_surgery_implant_placement);
        mEditImplantSlNo = view.findViewById(R.id.edit_row_check_up_surgery_implant_sl_no);
        mEditNote = view.findViewById(R.id.edit_row_check_up_surgery_implant_notes);
        // mEditImplantExpireDate=view.findViewById(R.id.edit_row_check_up_surgery_implant_expiry_date);

        //mEditSurgeryName=view.findViewById(R.id.edit_row_check_up_surgery_notes_surgery_name);
        mEditAcd = view.findViewById(R.id.edit_row_check_up_surgery_notes_acd);
        mEditAL = view.findViewById(R.id.edit_row_check_up_surgery_notes_al);
        mEditIncisionSize = view.findViewById(R.id.edit_row_check_up_surgery_notes_incision_size);
        mEditIncisionType = view.findViewById(R.id.edit_row_check_up_surgery_notes_incision_type);
        mEditViscoelastics = view.findViewById(R.id.edit_row_check_up_surgery_notes_viscoelastics);
        mEditSurgeryRemarks = view.findViewById(R.id.edit_row_check_up_surgery_notes_remarks);


        setData();

        //ToDo:Radio Button Check Change Listener
        mRadioSettlement.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selId = group.getCheckedRadioButtonId();
                // find the radiobutton by returned id
                RadioButton radioButton = view.findViewById(selId);
                if (radioButton.getId() == mRadioMediclamYes.getId()) {
                    mLinearSettlement.setVisibility(View.VISIBLE);
                    strSettlement = mActivity.getString(R.string.text_yes);
                } else {
                    mLinearSettlement.setVisibility(View.GONE);
                    strSettlement = mActivity.getString(R.string.text_no);
                }
            }
        });


        // TODO: Set On Click Listener
        mLinearMediclaim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mExpandableMediclaim.isExpanded()) {
                    mExpandableMediclaim.setExpanded(false);
                    imageArrowMediclaim.setImageResource(R.drawable.ic_down_arrow);
                } else {
                    mExpandableMediclaim.setExpanded(true);
                    imageArrowMediclaim.setImageResource(R.drawable.ic_up_arrow);
                }

            }
        });
        mLinearAnaesthesia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mExpandableAnaesthesia.isExpanded()) {
                    mExpandableAnaesthesia.setExpanded(false);
                    imageArrowAnaesthesia.setImageResource(R.drawable.ic_down_arrow);
                } else {
                    mExpandableAnaesthesia.setExpanded(true);
                    imageArrowAnaesthesia.setImageResource(R.drawable.ic_up_arrow);
                }

            }
        });
        mLinearSurgeryNotes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mExpandableSurgeryNotes.isExpanded()) {
                    mExpandableSurgeryNotes.setExpanded(false);
                    imageArrowSurgeryNote.setImageResource(R.drawable.ic_down_arrow);
                } else {
                    mExpandableSurgeryNotes.setExpanded(true);
                    imageArrowSurgeryNote.setImageResource(R.drawable.ic_up_arrow);
                }

            }
        });
        mLinearImplant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mExpandableImplant.isExpanded()) {
                    mExpandableImplant.setExpanded(false);
                    imageArrowImplant.setImageResource(R.drawable.ic_down_arrow);
                } else {
                    mExpandableImplant.setExpanded(true);
                    imageArrowImplant.setImageResource(R.drawable.ic_up_arrow);
                }

            }
        });

        mEditImplantExpireDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.openDatePicker(mActivity, mEditImplantExpireDate);
            }
        });

        mEditExpireDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.openDatePicker(mActivity, mEditExpireDate);
            }
        });

        mEditSuregeryNoteStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.openDateTimePicker(mActivity, mEditSuregeryNoteStartTime);
            }
        });
        mEditSuregeryNoteEndTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.openDateTimePicker(mActivity, mEditSuregeryNoteEndTime);
            }
        });

        imageRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLinearContainer.removeView(view);
            }
        });

        mButtonMediclaim.setOnClickListener(v -> {

            SessionManager manager = new SessionManager(mActivity);
            String closeFile = manager.getPreferences("CloseFiles", "");

            if (closeFile.equalsIgnoreCase("Yes")) {
                mButtonSubmit.setClickable(false);
                Toast.makeText(mActivity, "Patient File Closed", Toast.LENGTH_SHORT).show();
            } else {
                if (checkValidationMediclaim()) {
                    callToAddMediclaimAPI();
                } else {
                    Common.setCustomToast(mActivity, mActivity.getString(R.string.erro_enter_surgery_data));
                }
            }


        });

        mButtonAnaesthesia.setOnClickListener(v -> {

            SessionManager manager = new SessionManager(mActivity);
            String closeFile = manager.getPreferences("CloseFiles", "");

            if (closeFile.equalsIgnoreCase("Yes")) {
                mButtonSubmit.setClickable(false);
                Toast.makeText(mActivity, "Patient File Closed", Toast.LENGTH_SHORT).show();
            } else {
                if (checkAnaesthesia()) {
                    callToAnaesthesia();
                } else {
                    Common.setCustomToast(mActivity, mActivity.getString(R.string.erro_enter_surgery_data));
                }

            }


        });

        mButtonNotes.setOnClickListener(v -> {

            SessionManager manager = new SessionManager(mActivity);
            String closeFile = manager.getPreferences("CloseFiles", "");

            if (closeFile.equalsIgnoreCase("Yes")) {
                mButtonSubmit.setClickable(false);
                Toast.makeText(mActivity, "Patient File Closed", Toast.LENGTH_SHORT).show();
            } else {
                if (checkSurgery()) {
                    callToSurgery();
                } else {
                    Common.setCustomToast(mActivity, mActivity.getString(R.string.erro_enter_surgery_data));
                }

            }


        });

        mButtonImplant.setOnClickListener(v -> {


            SessionManager manager = new SessionManager(mActivity);
            String closeFile = manager.getPreferences("CloseFiles", "");

            if (closeFile.equalsIgnoreCase("Yes")) {
                mButtonSubmit.setClickable(false);
                Toast.makeText(mActivity, "Patient File Closed", Toast.LENGTH_SHORT).show();
            } else {
                callToImplantAPI();
              /*  if (checkValidationImplant()) {

                } else {
                    Common.setCustomToast(mActivity, mActivity.getString(R.string.erro_enter_surgery_data));
                }*/

            }


        });

        mLinearContainer.addView(view);
    }

    /**
     * This method should call the Preliminary Examination API
     */
    private void callToPreliminaryExaminationAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPreliminaryExaminationDetailsJson(hospital_database));

            Call<PreliminaryExaminationDetailsModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationDetails(body);
            call.enqueue(new Callback<PreliminaryExaminationDetailsModel>() {
                @Override
                public void onResponse(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Response<PreliminaryExaminationDetailsModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null &&
                                    response.body().getError() == AppConstants.API_SUCCESS_ERROR) {

                                PreliminaryExaminationDetailsModel.Data data =
                                        response.body().getData();

                                mArrEye.addAll(data.getEye());
                                mArrAnaesthesiaType.addAll(data.getAnaesthesiaType());

                                mArrSurgeryType.addAll(data.getSurgeryType());

                                mArrImplantBrand.addAll(data.getImplantBrand());
                                mArrImplantName.addAll(data.getImplantName());

                                bindEye();
                                bindAnaesthesiaType();
                                bindSurgeryType();
                                bindImplantBrandName();
                                bindImplantName();

                                callToHistoryPreliminaryExamination();
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    callToHistoryPreliminaryExamination();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Implant Brand Name
     */
    private void bindImplantBrandName() {
        if (mArrImplantBrandName.size() > 0) {
            mArrImplantBrandName.clear();
        }

        if (mArrImplantBrand.size() > 0) {
            for (int i = 0; i < mArrImplantBrand.size(); i++) {
                mArrImplantBrandName.add(mArrImplantBrand.get(i).getImplantBrand());
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (mActivity, android.R.layout.select_dialog_item, mArrImplantBrandName);
        mEditImplantBrand.setThreshold(1); //will start working from first character
        mEditImplantBrand.setAdapter(adapter);
        mEditImplantBrand.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mEditImplantBrand.showDropDown();
                return false;
            }
        });
    }

    /**
     * Set Implant Brand Name
     */
    private void bindImplantName() {
        if (mArrImplantNameString.size() > 0) {
            mArrImplantNameString.clear();
        }

        if (mArrImplantName.size() > 0) {
            for (int i = 0; i < mArrImplantName.size(); i++) {
                mArrImplantNameString.add(mArrImplantName.get(i).getImplantName());
            }
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (mActivity, android.R.layout.select_dialog_item, mArrImplantNameString);
        mEditImplantName.setThreshold(1); //will start working from first character
        mEditImplantName.setAdapter(adapter);
        mEditImplantName.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mEditImplantName.showDropDown();
                return false;
            }
        });
    }


    /**
     * Bind Anaesthesia Type Array to String Array
     */
    private void bindAnaesthesiaType() {
        if (mArrAnaesthesiaTypeName.size() > 0) {
            mArrAnaesthesiaTypeName.clear();
        }

        if (mArrAnaesthesiaType.size() > 0) {
            mArrAnaesthesiaTypeName.add(mActivity.getResources().
                    getString(R.string.spinner_select_anaesthesia));
            for (int a = 0; a < mArrAnaesthesiaType.size(); a++) {
                mArrAnaesthesiaTypeName.add(mArrAnaesthesiaType.get(a).getAnaesthesiaType());
            }
        }

        //Set Anaesthesia Type Index
        mSpinnerAnaesthesiaType.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        ArrayAdapter adapter_anesthesia_type = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_dropdown_item, mArrAnaesthesiaTypeName) {
            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                // Cast the drop down items (popup items) as text view
                TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                // Set the text color of drop down items
                tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                // If this item is selected item
                if (position == mSelectedAnaesthesiaTypeIndex) {
                    // Set spinner selected popup item's text color
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorPrimaryBlue));
                }
                // Return the modified view
                return tv;
            }
        };
        adapter_anesthesia_type.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerAnaesthesiaType.setAdapter(adapter_anesthesia_type);
        mSpinnerAnaesthesiaType.setSelection(0, true);
    }

    /**
     * Bind Surgery Type Array to String Array
     */
    private void bindSurgeryType() {
        if (mArrSurgeryTypeName.size() > 0) {
            mArrSurgeryTypeName.clear();
        }

        if (mArrSurgeryType.size() > 0) {
            mArrSurgeryTypeName.add(mActivity.getResources().
                    getString(R.string.spinner_select_surgery));
            for (int a = 0; a < mArrSurgeryType.size(); a++) {
                mArrSurgeryTypeName.add(mArrSurgeryType.get(a).getSurgeryName());
            }
        }

        mSpinnerSurgeryNoteSurgery.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_dropdown_item, mArrSurgeryTypeName) {
            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                // Cast the drop down items (popup items) as text view
                TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                // Set the text color of drop down items
                tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                // If this item is selected item
                if (position == mSelectedSurgeryIndex) {
                    // Set spinner selected popup item's text color
                    tv.setTextColor(Common.setThemeColor(mActivity));
                }
                // Return the modified view
                return tv;
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerSurgeryNoteSurgery.setAdapter(adapter);
        mSpinnerSurgeryNoteSurgery.setSelection(0, true);

        mSpinnerSurgeryNoteSurgeryTwo.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        ArrayAdapter adapter2 = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_dropdown_item, mArrSurgeryTypeName) {
            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                // Cast the drop down items (popup items) as text view
                TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                // Set the text color of drop down items
                tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                // If this item is selected item
                if (position == mSelectedSurgeryIndexTwo) {
                    // Set spinner selected popup item's text color
                    tv.setTextColor(Common.setThemeColor(mActivity));
                }
                // Return the modified view
                return tv;
            }
        };
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerSurgeryNoteSurgeryTwo.setAdapter(adapter2);
        mSpinnerSurgeryNoteSurgeryTwo.setSelection(0, true);
    }


    /**
     * Bind Anaesthesia Type Array to String Array
     */
    private void bindEye() {
        if (mArrEyeName.size() > 0) {
            mArrEyeName.clear();
        }
        if (mArrEye.size() > 0) {
            mArrEyeName.add(mActivity.getResources().getString(R.string.spinner_select_eye));
            for (int a = 0; a < mArrEye.size(); a++) {
                mArrEyeName.add(mArrEye.get(a).getEye());
            }
        }

        //Set Eye Adapter
        mSpinnerAnaesthesiaEye.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        ArrayAdapter adapter_anesthesia_eye = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_dropdown_item, mArrEyeName) {
            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                // Cast the drop down items (popup items) as text view
                TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                // Set the text color of drop down items
                tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                // If this item is selected item
                if (position == mSelectedAnaesthesiaEyeIndex) {
                    // Set spinner selected popup item's text color
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorPrimaryBlue));
                }
                // Return the modified view
                return tv;
            }
        };
        adapter_anesthesia_eye.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerAnaesthesiaEye.setAdapter(adapter_anesthesia_eye);
        mSpinnerAnaesthesiaEye.setSelection(0, true);


        //Set Surgery Eye
        mSpinnerSurgeryNoteEye.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        ArrayAdapter adapter_surgerynote_eye = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_dropdown_item, mArrEyeName) {
            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                // Cast the drop down items (popup items) as text view
                TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                // Set the text color of drop down items
                tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                // If this item is selected item
                if (position == mSelectedSurgeryEyeIndex) {
                    // Set spinner selected popup item's text color
                    tv.setTextColor(Common.setThemeColor(mActivity));
                }
                // Return the modified view
                return tv;
            }
        };
        adapter_surgerynote_eye.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerSurgeryNoteEye.setAdapter(adapter_surgerynote_eye);
        mSpinnerSurgeryNoteEye.setSelection(0, true);

    }

    /**
     * Set Add Surgery
     */
    private void callAddSurgery() {
        if (mLinearContainer.getChildCount() > 0) {

//            if (checkValidationMediclaim()) {
//                callToAddMediclaimAPI();
//            } else if (checkAnaesthesia()) {
//                callToAnaesthesia();
//            } else if (checkSurgery()) {
//                callToSurgery();
//            } else if (checkValidationImplant()) {
//                callToImplantAPI();
//            } else {
//                Common.setCustomToast(mActivity, mActivity.getString(R.string.erro_enter_surgery_data));
//            }
        }
    }

    /**
     * Check Surgery Data validation
     *
     * @return
     */
    private boolean checkSurgery() {
        boolean status = true;
        String startTime = mEditSuregeryNoteStartTime.getText().toString();
        String endTime = mEditSuregeryNoteEndTime.getText().toString();
        String acd = mEditAcd.getText().toString();
        String al = mEditAL.getText().toString();
        String incisionType = mEditIncisionType.getText().toString();
        String incisionSize = mEditIncisionSize.getText().toString();
        String viscoelastics = mEditViscoelastics.getText().toString();

        int indexEye = mSpinnerSurgeryNoteEye.getSelectedItemPosition();
        int indexType = mSpinnerSurgeryNoteSurgery.getSelectedItemPosition();

        if (startTime.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                && endTime.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                && acd.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                && al.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                && incisionType.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                && incisionSize.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                && viscoelastics.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            status = false;

        } else {
            if (startTime.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
                mEditSuregeryNoteStartTime.setError(mActivity.getString(R.string.error_field_required));
            }
            if (endTime.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
                mEditSuregeryNoteEndTime.setError(mActivity.getString(R.string.error_field_required));
            }
        /*    if (acd.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
                mEditAcd.setError(mActivity.getString(R.string.error_field_required));
            }
            if (al.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
                mEditAL.setError(mActivity.getString(R.string.error_field_required));
            }
            if (incisionSize.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
                mEditIncisionSize.setError(mActivity.getString(R.string.error_field_required));
            }
            if (incisionType.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
                mEditIncisionType.setError(mActivity.getString(R.string.error_field_required));
            }
            if (viscoelastics.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
                mEditViscoelastics.setError(mActivity.getString(R.string.error_field_required));
            }*/
/*
            if (indexEye == 0) {
                status = false;
                Common.setCustomToast(mActivity, mActivity.getString(R.string.error_select_surgery_eye));
            }*/
            if (indexType == 0) {
                status = false;
                Common.setCustomToast(mActivity, mActivity.getString(R.string.error_select_surgery_type));
            }
        }

        return status;
    }

    private boolean checkAnaesthesia() {

        boolean status = true;
        String Pulse = mEditPulse.getText().toString();
        String Systolic = mEditSystolic.getText().toString();
        String Diastolic = mEditDiastolic.getText().toString();

        String SPO2 = mEditSPO2.getText().toString();
        String RBS = mEditRBS.getText().toString();

        int indexEye = mSpinnerAnaesthesiaEye.getSelectedItemPosition();
        int indexType = mSpinnerAnaesthesiaType.getSelectedItemPosition();

        if (Pulse.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                && Systolic.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                && Diastolic.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                && SPO2.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                && RBS.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            status = false;

        } else {
            if (Pulse.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
                mEditPulse.setError(mActivity.getString(R.string.error_field_required));
            }
            if (Systolic.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
                mEditSystolic.setError(mActivity.getString(R.string.error_field_required));
            }
            if (Diastolic.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
                mEditDiastolic.setError(mActivity.getString(R.string.error_field_required));
            }
           /* if (SPO2.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
                mEditSPO2.setError(mActivity.getString(R.string.error_field_required));
            }
            if (RBS.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
                mEditRBS.setError(mActivity.getString(R.string.error_field_required));
            }*/

          /*  if (indexEye == 0) {
                status = false;
                Common.setCustomToast(mActivity, mActivity.getString(R.string.error_select_eye));
            }
            if (indexType == 0) {
                status = false;
                Common.setCustomToast(mActivity, mActivity.getString(R.string.error_select_anaesthesia));
            }*/
        }
        return status;
    }

    /**
     * Check Validation Mediclaim
     *
     * @return
     */
    private boolean checkValidationMediclaim() {
        boolean status = true;
        String Company = mEditCompany.getText().toString();
        String tpa = mEditTpa.getText().toString();
        String Policy = mEditPolicyNo.getText().toString();
        String date = mEditExpireDate.getText().toString();
        if (Company.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                && tpa.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                && Policy.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                && date.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            status = false;

        } else {
            if (Company.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
                mEditCompany.setError(mActivity.getString(R.string.error_field_required));
            }
            /*if (tpa.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
                mEditTpa.setError(mActivity.getString(R.string.error_field_required));
            }
            if (Policy.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
                mEditPolicyNo.setError(mActivity.getString(R.string.error_field_required));
            }
            if (date.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
                mEditExpireDate.setError(mActivity.getString(R.string.error_field_required));
            }*/
        }
        return status;
    }

    /**
     * Check Validation Mediclaim
     *
     * @return - Boolean
     */
    private boolean checkValidation() {
        boolean status = true;

        mEditTpa.setError(null);
        mEditPolicyNo.setError(null);
        mEditExpireDate.setError(null);
        mEditSettlementAmount.setError(null);

        if (mEditCompany.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            status = false;
        }

        if (status) {
            if (mEditTpa.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
                mEditTpa.setError(mActivity.getString(R.string.error_field_required));
            }
            if (mEditPolicyNo.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
                mEditPolicyNo.setError(mActivity.getString(R.string.error_field_required));
            }
            if (mEditExpireDate.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
                mEditExpireDate.setError(mActivity.getString(R.string.error_field_required));
            }

            if (strSettlement.equalsIgnoreCase(mActivity.getString(R.string.text_yes))) {
                if (mEditSettlementAmount.getText().toString()
                        .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                    status = false;
                    mEditSettlementAmount.setError(mActivity.getString(R.string.error_field_required));
                }
            }
        }

        return status;
    }

    /**
     * Call Add Mediclaim API
     */
    private void callToAddMediclaimAPI() {
        try {

            String Company = mEditCompany.getText().toString();
            String tpa = mEditTpa.getText().toString();
            String Policy = mEditPolicyNo.getText().toString();
            String date = mEditExpireDate.getText().toString();
            String convertDate = Common.convertDateUsingDateFormat(mActivity, date,
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                    mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));

            String settlement_amount = mEditSettlementAmount.getText().toString();

            if (settlement_amount.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                settlement_amount = "0";
            }


            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAddMediclaimJson(mUserId, hospital_database, String.valueOf(mAppointmentId),
                            Company, tpa, Policy, convertDate, strSettlement, settlement_amount, mPreliminaryExaminationID));

            Call<AddMedicationModel> call = RetrofitClient.createService(ApiInterface.class).addMedication(requestBody);
            call.enqueue(new Callback<AddMedicationModel>() {
                @Override
                public void onResponse(@NonNull Call<AddMedicationModel> call, Response<AddMedicationModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    String message = response.body().getMessage();
                    if (response.isSuccessful() && response.body().getError() == 200) {
                        Common.setCustomToast(mActivity, message);
                        mArrHistory.clear();
                        callToHistoryPreliminaryExamination();
                        AddNewPreliminaryExaminationFragment.changePage(1);
                    } else {
                        Common.setCustomToast(mActivity, message);
                    }
                }

                @Override
                public void onFailure(Call<AddMedicationModel> call, Throwable t) {
                    Common.insertLog(t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());
//                    callToAnaesthesia();
                }
            });

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Check Anaesthesia Validation
     *
     * @return - boolean Value
     */
    private boolean checkValidationAnaesthesia() {
        boolean status = true;
        try {
            if (mEditPulse.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
            if (mEditSystolic.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
            if (mEditSPO2.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
            if (mEditRBS.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
            if (mEditAnaesthesiaMedicine.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }

            int selected = mSpinnerAnaesthesiaEye.getSelectedItemPosition();
            if (selected == 0) {
                status = false;
            }
            int selected_anesthesia = mSpinnerAnaesthesiaType.getSelectedItemPosition();
            if (selected_anesthesia == 0) {
                status = false;
            }

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
        return status;
    }

    private void callToAnaesthesia() {
        try {

            String pulse = mEditPulse.getText().toString();
            //String bp=mEditBP.getText().toString();

            String Systolic = mEditSystolic.getText().toString();
            String Diastolic = mEditDiastolic.getText().toString();

            String spo2 = mEditSPO2.getText().toString();
            String RBS = mEditRBS.getText().toString();
            String anaesthesia_medicine = mEditAnaesthesiaMedicine.getText().toString();
            int selected = mSpinnerAnaesthesiaEye.getSelectedItemPosition();

            String eye_id = "", eye_anaesthesiaType = AppConstants.STR_EMPTY_STRING;
            if (selected != 0) {
                eye_id = mArrEye.get(selected - 1).getEyeID();
            } else {
                eye_id = "0";
            }

            int selected_anesthesia = mSpinnerAnaesthesiaType.getSelectedItemPosition();

            if (selected_anesthesia != 0) {
                eye_anaesthesiaType = mArrAnaesthesiaType.get(selected_anesthesia - 1).getAnaesthesiaTypeID();
            } else {
                eye_anaesthesiaType = "0";
            }

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAddAnaesthesiaSurgeryJson(mUserId, hospital_database, String.valueOf(mAppointmentId), pulse, "", spo2,
                            RBS, eye_id, eye_anaesthesiaType, anaesthesia_medicine, mPreliminaryExaminationID, Systolic, Diastolic));
            Common.insertLog("Request " + requestBody);

            Call<AddMediclaimModel> call = RetrofitClient.createService(ApiInterface.class).AddAnaesthesiaSurgery(requestBody);

            call.enqueue(new Callback<AddMediclaimModel>() {
                @Override
                public void onResponse(Call<AddMediclaimModel> call, Response<AddMediclaimModel> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = jsonObject.getInt(WebFields.ERROR);
                        Common.insertLog("mMessage:::> " + mMessage);


                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                Common.setCustomToast(mActivity, mMessage);
                                //AddSurgeryFragment.setPage(2);
//                                callToSurgery();
                                mArrHistory.clear();
                                callToHistoryPreliminaryExamination();
                                AddNewPreliminaryExaminationFragment.changePage(1);
                            }

                        } else {
//                            callToSurgery();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<AddMediclaimModel> call, Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
//                    callToSurgery();
                }
            });

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }


    /**
     * Check Implant Validation
     *
     * @return - boolean Value
     */
    private boolean checkValidationImplant() {
        boolean status = true;
        try {
            if (mEditImplantBrand.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
            if (mEditImplantName.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
            if (mEditAConstant.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
            if (mEditImplantPower.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
            if (mEditPlacement.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
            if (mEditImplantExpireDate.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
            if (mEditImplantSlNo.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
            if (mEditNote.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
        return status;
    }

    /**
     * Call To Implant API
     */
    private void callToImplantAPI() {

      //  if (checkValidationImplant()) {

            try {
                String ImplantBrand = mEditImplantBrand.getText().toString();
                String ImplantName = mEditImplantName.getText().toString();
                String a_constant = mEditImplantBrand.getText().toString();
                String implant_power = mEditImplantPower.getText().toString();
                String implant_placement = mEditPlacement.getText().toString();

                String date = mEditImplantExpireDate.getText().toString();
                // String implant_expirty=date;
                String implant_expirty = Common.convertDateUsingDateFormat(mActivity, date,
                        mActivity.getString(R.string.date_format_dd_mm_yyyy),
                        mActivity.getString(R.string.date_format_yyyy_mm_dd));

                String implant_SlNo = mEditImplantSlNo.getText().toString();
                String implant_notes = mEditNote.getText().toString();

                RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                        APICommonMethods.setAddImplantSurgeryJson(mUserId, hospital_database, String.valueOf(mAppointmentId),
                                ImplantBrand, ImplantName, a_constant, implant_power,
                                implant_placement, implant_expirty, implant_SlNo, implant_notes, mPreliminaryExaminationID));
                Common.insertLog("Request " + requestBody);

                Call<AddMediclaimModel> call = RetrofitClient.createService(ApiInterface.class).AddImplantSurgery(requestBody);

                call.enqueue(new Callback<AddMediclaimModel>() {
                    @Override
                    public void onResponse(Call<AddMediclaimModel> call, Response<AddMediclaimModel> response) {
                        Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                        try {
                            String mJson = (new Gson().toJson(response.body()));
                            JSONObject jsonObject = new JSONObject(mJson);
                            String mMessage = jsonObject.getString(WebFields.MESSAGE);
                            int mError = jsonObject.getInt(WebFields.ERROR);
                            Common.insertLog("mMessage:::> " + mMessage);

                            if (response.isSuccessful() && mError == 200) {
                                if (response.body() != null) {
                                    mArrHistory.clear();
                                    callToHistoryPreliminaryExamination();
                                    AddNewPreliminaryExaminationFragment.changePage(1);

                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddMediclaimModel> call, Throwable t) {
                        Common.insertLog("Failure:::> " + t.getMessage());
                    }
                });
            } catch (Exception e) {
                Common.insertLog(e.getMessage());
            }

       // }
    }

    /**
     * Check Validation Surgery Notes
     *
     * @return
     */
    private boolean checkValidationSurgeryNotes() {
        boolean status = true;
        try {
            if (mEditSuregeryNoteStartTime.getText().toString()
                    .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
            if (mEditSuregeryNoteEndTime.getText().toString()
                    .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
            if (mSpinnerSurgeryNoteSurgery.getSelectedItemPosition() == 0) {
                status = false;
            }
            if (mEditAcd.getText().toString()
                    .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
            if (mEditAL.getText().toString()
                    .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
            if (mEditIncisionSize.getText().toString()
                    .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
            if (mEditSuregeryNoteEndTime.getText().toString()
                    .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
            if (mEditIncisionType.getText().toString()
                    .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
            if (mEditViscoelastics.getText().toString()
                    .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
            if (mEditSurgeryRemarks.getText().toString()
                    .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                status = false;
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
        return status;
    }

    /**
     * Call To Surgery
     */
    private void callToSurgery() {
        try {
            String startTime = mEditSuregeryNoteStartTime.getText().toString();
            String endTime = mEditSuregeryNoteEndTime.getText().toString();

            String surgeryTypeID = AppConstants.STR_EMPTY_STRING,
                    EyeID = AppConstants.STR_EMPTY_STRING,
                    surgeryTypeIDTwo = AppConstants.STR_EMPTY_STRING;

            int index_TypeID = mSpinnerSurgeryNoteSurgery.getSelectedItemPosition() - 1;
            int index_SugeryEye = mSpinnerSurgeryNoteEye.getSelectedItemPosition() - 1;
            int index_SurgeryTwo = mSpinnerSurgeryNoteSurgeryTwo.getSelectedItemPosition() - 1;

            if (index_TypeID != 0) {
                surgeryTypeID = mArrSurgeryType.get(index_TypeID).getSurgeryTypeID();
            }

            if (index_SurgeryTwo != 0 && index_SurgeryTwo != -1) {
                surgeryTypeIDTwo = mArrSurgeryType.get(index_SurgeryTwo).getSurgeryName();
            }

            if (index_SugeryEye != 0) {
                EyeID = mArrEye.get(index_SugeryEye).getEyeID();
            }

            String current_date = Common.setCurrentDate(mActivity);
            String surgery_date = Common.convertDateUsingDateFormat(mActivity, current_date,
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                    mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));

            StringTokenizer tokens = new StringTokenizer(startTime, " ");
            String convert_starDate = tokens.nextToken();
            String convert_statTime = tokens.nextToken();

            StringTokenizer tokensEndDate = new StringTokenizer(endTime, " ");
            String convert_endDate = tokensEndDate.nextToken();
            String convert_endTime = tokensEndDate.nextToken();

            convert_starDate = Common.convertDateUsingDateFormat(mActivity, convert_starDate,
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                    mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));

            convert_endDate = Common.convertDateUsingDateFormat(mActivity, convert_endDate,
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                    mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));

            String ACD = mEditAcd.getText().toString();
            String AL = mEditAL.getText().toString();
            String IncisionType = mEditIncisionType.getText().toString();
            String IncisionSize = mEditIncisionSize.getText().toString();
            String SurgeryNote = AppConstants.STR_EMPTY_STRING;
            String Viscoelastics = mEditViscoelastics.getText().toString();
            String Remarks = mEditSurgeryRemarks.getText().toString();


            //get Preliminary Examination ID
            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAddPreliminarySurgeryNoteJson(mUserId, hospital_database,
                            String.valueOf(mAppointmentId), surgery_date, convert_statTime,
                            convert_endTime, surgeryTypeID, ACD, AL, IncisionSize, IncisionType,
                            SurgeryNote, Viscoelastics, Remarks, EyeID, mPreliminaryExaminationID,
                            surgeryTypeIDTwo, convert_starDate, convert_endDate));

            Common.insertLog("Request " + requestBody);

            Call<AddMediclaimModel> call = RetrofitClient.createService(ApiInterface.class).AddImplantSurgery(requestBody);

            call.enqueue(new Callback<AddMediclaimModel>() {
                @Override
                public void onResponse(Call<AddMediclaimModel> call, Response<AddMediclaimModel> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = jsonObject.getInt(WebFields.ERROR);
                        Common.insertLog("mMessage:::> " + mMessage);


                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                mArrHistory.clear();
                                callToHistoryPreliminaryExamination();
                                AddNewPreliminaryExaminationFragment.changePage(1);
//                                callToImplantAPI();
                            }
                        } else {
//                            callToImplantAPI();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<AddMediclaimModel> call, Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
//            callToImplantAPI();
        }
    }

    /**
     * Open Dialog Treatment Suggested
     */
    public void openDialogTreatmentSuggested() {
        try {
            Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.custom_dialog_history_preliminary);

            TextView mTextTitle = dialog.findViewById(R.id.text_view_custom_dialog_history_preliminary);
            RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
            ImageView mImageClose = dialog.findViewById(R.id.image_custom_dialog_history_preliminary_close);

            RelativeLayout noInternetConnection = dialog.findViewById(R.id.relative_no_internet);
            RelativeLayout noDataFound = dialog.findViewById(R.id.relative_no_data_available);

            mTextTitle.setText(mActivity.getResources().getString(R.string.nav_menu_surgery));

            mImageClose.setColorFilter(Common.setThemeColor(mActivity));

            mImageClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            LinearLayoutManager manager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(manager);


            HistorySurgeryAdapter adapter = new HistorySurgeryAdapter(mActivity, recyclerView, mArrHistory, this) {
                @Override
                protected void onSelectedItem(Activity activity, HistoryPreliminaryExaminationModel.Data data) {
                    super.onSelectedItem(activity, data);
                    dialog.dismiss();
                }
            };

            recyclerView.setAdapter(adapter);

            if (mArrHistory.size() > 0) {
                noDataFound.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            } else {
                noDataFound.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * OpenDialog for closing patient file
     */
    public void openFileClosingAlert() {


        AlertDialog.Builder builder = new AlertDialog.Builder(
                mActivity);
        builder.setTitle("Close File");
        builder.setMessage("Are you sure you want to close file?");
        builder.setNegativeButton("NO",
                (dialog, which) -> {
                    dialog.dismiss();
                });
        builder.setPositiveButton("YES",
                (dialog, which) -> closeFile());
        builder.show();

    }

    private void closeFile() {
        try {
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.closeFileJson(mAppointmentId, mDatabaseName));

            Call<CloseFileModel> call = RetrofitClient.createService(ApiInterface.class).closeFileAPI(body);
            call.enqueue(new Callback<CloseFileModel>() {
                @Override
                public void onResponse(@NonNull Call<CloseFileModel> call, @NonNull Response<CloseFileModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        int mError = response.body().getError();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                Common.insertLog("IF");
                                Common.setCustomToast(mActivity,mMessage);
                            } else {
                                Common.insertLog("ELSE");

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CloseFileModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    /**
     * Set Call API History Preliminary Examination
     */
    private void callToHistoryPreliminaryExamination() {
        try {
            String defualt_date = "1000-01-01";
            String Mobile = AppConstants.STR_EMPTY_STRING;
            String Type = "Surgery";
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setHistoryPreliminaryExaminationJson(defualt_date, mCurrentPage, mPatientId, Mobile, mHistoryDoctorID, Type, hospital_database));

            Call<HistoryPreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationHistory(body);
            call.enqueue(new Callback<HistoryPreliminaryExaminationModel>() {
                @Override
                public void onResponse(Call<HistoryPreliminaryExaminationModel> call, Response<HistoryPreliminaryExaminationModel> response) {
                    Common.insertLog("Response History::::> " + new Gson().toJson(response.body()));
                    try {
                        if (response.body() != null &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body().getData().size() > 0) {

                                mArrHistory.addAll(response.body().getData());
                                checkTodayPreliminaryExamination();

                            }
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<HistoryPreliminaryExaminationModel> call, Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    @Override
    public void onSelectHistorySurgery(Activity mActivity, HistoryPreliminaryExaminationModel.Data checkInData) {
        mRelativeDataNotFound.setVisibility(View.GONE);
        EditData(checkInData.getSurgery().get(0));
    }

    /**
     * set Edit History Preliminary Examination Data
     *
     * @param checkInData - Treatment Date
     */
    public void EditData(HistoryPreliminaryExaminationModel.Surgery checkInData) {

        try {
            String strCompany = checkInData.getCompany().trim();
            mEditCompany.setText(checkInData.getCompany());
            mEditTpa.setText(checkInData.getTPA());
            mEditPolicyNo.setText(checkInData.getPolicyNo());


            String convertDate = Common.convertDateUsingDateFormat(mActivity,
                    checkInData.getExpiryDate(),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));

            mEditExpireDate.setText(convertDate);

            if (checkInData.getSettlementReceived()
                    .equals(this.mActivity.getString(R.string.text_yes))) {
                mRadioSettlement.check(R.id.radio_button_row_check_up_surgery_mediclaim_yes);
            } else {
                mRadioSettlement.check(R.id.radio_button_row_check_up_surgery_mediclaim_no);
            }

            mEditSettlementAmount.setText(checkInData.getSettlementAmount());

            //Anaesthesia
            mEditPulse.setText(checkInData.getPulse());
            //mEditBP.setText(checkInData.getSystolicBP());
            mEditSystolic.setText(checkInData.getSystolicBP());
            mEditDiastolic.setText(checkInData.getDiastolicBP());

            mEditSPO2.setText(checkInData.getSPO2());
            mEditRBS.setText(checkInData.getRBS());
            mEditAnaesthesiaMedicine.setText(checkInData.getAnaesthesiaMedicine());

            if (!checkInData.getAnaesthesiaEyeID().
                    equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                if (mArrEye.size() > 0) {
                    for (int a = 0; a < mArrEye.size(); a++) {
                        if (checkInData.getAnaesthesiaEyeID().
                                equalsIgnoreCase(mArrEye.get(a).getEyeID())) {
                            int index = a + 1;
                            mSpinnerAnaesthesiaEye.setSelection(index);
                        }
                    }
                }
            }


            if (!checkInData.getAnaesthesiaTypeID().
                    equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                if (mArrAnaesthesiaType.size() > 0) {
                    for (int a = 0; a < mArrAnaesthesiaType.size(); a++) {
                        if (checkInData.getAnaesthesiaTypeID().equals(mArrAnaesthesiaType.get(a).getAnaesthesiaTypeID())) {
                            int index = a + 1;
                            mSpinnerAnaesthesiaType.setSelection(index, true);
                        }
                    }
                }
            }

            //Surgery Note

            if (!checkInData.getEndDate().equalsIgnoreCase("")) {
            }

            String convertEndDate = Common.convertDateUsingDateFormat(mActivity,
                    checkInData.getEndDate(),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));


            String convertEndDateTime = Common.convertDateUsingDateFormat(mActivity,
                    checkInData.getEndTime(),
                    mActivity.getResources().getString(R.string.date_format_hh_mm_ss),
                    mActivity.getResources().getString(R.string.date_format_hh_mm_ss));


            String convertStartDate = Common.convertDateUsingDateFormat(mActivity,
                    checkInData.getSurgeryDate(),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));

            String convertStartTime = Common.convertDateUsingDateFormat(mActivity,
                    checkInData.getStartTime(),
                    mActivity.getResources().getString(R.string.date_format_hh_mm_ss),
                    mActivity.getResources().getString(R.string.date_format_hh_mm_ss));


            mEditSuregeryNoteStartTime.setText(convertStartDate + AppConstants.STR_EMPTY_SPACE + convertStartTime);
            mEditSuregeryNoteEndTime.setText(convertEndDate + AppConstants.STR_EMPTY_SPACE + convertEndDateTime);
            mEditAcd.setText(checkInData.getACD());
            mEditAL.setText(checkInData.getAL());
            mEditIncisionSize.setText(checkInData.getIncisionSize());
            mEditIncisionType.setText(checkInData.getIncisionType());
            mEditViscoelastics.setText(checkInData.getViscoelastics());
            mEditSurgeryRemarks.setText(checkInData.getRemarks());

            if (mArrSurgeryType.size() > 0) {
                for (int a = 0; a < mArrSurgeryType.size(); a++) {
                    if (mArrSurgeryType.get(a).getSurgeryTypeID().equals
                            (checkInData.getSurgeryTypeID())) {
                        int index = a + 1;
                        mSpinnerSurgeryNoteSurgery.setSelection(index, true);
                    }
                }
            }

            if (mArrSurgeryType.size() > 0) {
                for (int a = 0; a < mArrSurgeryType.size(); a++) {
                    if (mArrSurgeryType.get(a).getSurgeryName().equals
                            (checkInData.getSurgeryType2())) {
                        int index = a + 1;
                        mSpinnerSurgeryNoteSurgeryTwo.setSelection(index, true);
                    }
                }
            }

            if (!checkInData.getSurgeryEyeID().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                if (mArrEye.size() > 0) {
                    for (int a = 0; a < mArrEye.size(); a++) {
                        if (mArrEye.get(a).getEyeID().equals
                                (checkInData.getSurgeryEyeID())) {
                            int index = a + 1;
                            mSpinnerSurgeryNoteEye.setSelection(index, true);
                        }
                    }
                }
            }

            mEditImplantBrand.setText(checkInData.getImplantBrand());
            mEditImplantName.setText(checkInData.getImplantName());
            mEditAConstant.setText(checkInData.getAConstant());
            mEditImplantPower.setText(checkInData.getImplantPower());
            mEditPlacement.setText(checkInData.getImplantPlacement());

            String convertImplant = Common.convertDateUsingDateFormat(mActivity,
                    checkInData.getImplantExpiry(),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));


            mEditImplantExpireDate.setText(convertImplant);
            mEditImplantSlNo.setText(checkInData.getImplantSlNo());
            mEditNote.setText(checkInData.getNotes());

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Check Today Preliminary Examination
     */
    private void checkTodayPreliminaryExamination() {
        String currentDate = Common.setCurrentDate(mActivity);


        String covertCurrentDate = Common.convertDateUsingDateFormat(mActivity, currentDate,
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen));

        String covertTreatmentDate = Common.convertDateUsingDateFormat(mActivity, mTreatmentDate,
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss_hyphen),
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen));


        if (!covertCurrentDate.equals(covertTreatmentDate)) {
            mRelativeDataNotFound.setVisibility(View.VISIBLE);
        } else {
            mRelativeDataNotFound.setVisibility(View.GONE);

            if (mArrHistory.size() > 0) {
                for (int a = 0; a < mArrHistory.size(); a++) {

                    if (mArrHistory.get(a).getTreatmentDate().equals(covertTreatmentDate)) {
                        EditData(mArrHistory.get(a).getSurgery().get(0));
                    }
                }
            }
        }
    }
}
