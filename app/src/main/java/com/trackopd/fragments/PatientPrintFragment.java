package com.trackopd.fragments;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.HistoryPrintAdapter;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.PreliminaryExaminationModel;
import com.trackopd.model.PrintReceiptModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PatientPrintFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private RecyclerView recyclerView;
    private RelativeLayout mRelativeNoData;
    private int mAppointmentId,mCurrentPage=1;
    private ArrayList<PreliminaryExaminationModel> mArrPreliminaryExamination;
    private String mPatientFirstName, mPatientLastName, mPatientMobile, mPatientId,
            mDoctorId,mTitle;

    private static ArrayList<HistoryPreliminaryExaminationModel.Data> mArrHistory;
    private String mUserType;

    public PatientPrintFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_patient_vision, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrPreliminaryExamination = new ArrayList<>();
        mArrHistory=new ArrayList<>();

        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        getBundle();
        getIds();
        callToHistoryInvestigationAPI();
        setHasOptionsMenu(true);
        return mView;
    }


    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = getArguments();
            if (bundle != null) {
                mPatientFirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mPatientLastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mPatientMobile = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_USER_ID);
                mAppointmentId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mTitle=bundle.getString(AppConstants.STR_TITLE);
                mDoctorId=bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIds() {
        try {
            // Recycler View
            recyclerView = mView.findViewById(R.id.recycler_view);

            // Relative Layouts
            mRelativeNoData = mView.findViewById(R.id.relative_no_data_available);

            LinearLayoutManager manager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(manager);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * get History Investigation
     */
    private void callToHistoryInvestigationAPI()
    {
        try {
            String defualt_date="1000-01-01";
            String Mobile=AppConstants.STR_EMPTY_STRING;
            String Type="all";

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setHistoryPreliminaryExaminationJson(defualt_date,mCurrentPage,mPatientId,Mobile,-1,Type,hospital_database));

            Call<HistoryPreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationHistory(body);
            call.enqueue(new Callback<HistoryPreliminaryExaminationModel>() {
                @Override
                public void onResponse(Call<HistoryPreliminaryExaminationModel> call, Response<HistoryPreliminaryExaminationModel> response) {
                    Common.insertLog("Response History::::> "
                            + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getError() == 200) {
                                ArrayList<HistoryPreliminaryExaminationModel.Data> appointmentModel = new ArrayList<>(Arrays
                                        .asList(new GsonBuilder().serializeNulls().create()
                                                .fromJson(new JsonParser().parse(jsonObject.getString(WebFields.DATA)).
                                                        getAsJsonArray(), HistoryPreliminaryExaminationModel.Data[].class)));


                                mArrHistory.addAll(appointmentModel);
                                setAdapterDoodle();
                            }else {
                                setVisibility();
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                            setVisibility();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        setVisibility();
                    }
                }

                @Override
                public void onFailure(Call<HistoryPreliminaryExaminationModel> call, Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setVisibility();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    public void setAdapterDoodle(){

        HistoryPrintAdapter adapter=new HistoryPrintAdapter(mActivity,recyclerView,
                mArrHistory){
            @Override
            protected void onSelectedItem(Activity activity,
                                          HistoryPreliminaryExaminationModel.Data data, boolean mHistoryAndComplains, boolean visionUCVA, boolean visionFinal, boolean visionBCVAUndilated, boolean visionBCVADilated, boolean mPrimaryExamination, boolean diagnosis, boolean counselingDetails, boolean investigationSuggested, boolean treatmentSuggested, boolean surgeryDetails, boolean prescription,String lang ,boolean discharge,boolean payment) {
                super.onSelectedItem(activity, data, mHistoryAndComplains, visionUCVA, visionFinal, visionBCVAUndilated, visionBCVADilated, mPrimaryExamination, diagnosis, counselingDetails, investigationSuggested, treatmentSuggested, surgeryDetails, prescription,lang,discharge, payment);
                //callToPrintFinalVision(data.getAppointmentID());
                //callToAddPreliminaryExaminationFragment(data);
                callToPrintFinalVision(data.getAppointmentID(),mHistoryAndComplains, visionUCVA, visionFinal, visionBCVAUndilated, visionBCVADilated, mPrimaryExamination, diagnosis, counselingDetails, investigationSuggested, treatmentSuggested, surgeryDetails, prescription,lang,discharge, payment);
            }
        };
        recyclerView.setAdapter(adapter);
        setVisibility();
    }



    public void setVisibility(){
        if (mArrHistory.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            mRelativeNoData.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.GONE);
            mRelativeNoData.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Sets up the Optometrist Add New Preliminary Examination Fragment
     *
     * @param data Receptionist Patients Model
     */
    private void callToAddPreliminaryExaminationFragment(HistoryPreliminaryExaminationModel.Data data) {

        try {
            Bundle mBundle = new Bundle();

            mBundle.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, data.getPatientFirstName());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, data.getPatientLastName());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE,data.getPatientMobileNo());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_PROFILE, "");
            mBundle.putString(AppConstants.BUNDLE_PATIENT_ID,data.getPatientID());
            mBundle.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, Integer.parseInt(data.getAppointmentID()));
            mBundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR,data.getDoctorID());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE,data.getTreatmentDate());
            mBundle.putString(AppConstants.BUNDLE_PRELIMINARY_EXAMINATION_ID,data.getPreliminaryExaminationID());
            mBundle.putString(AppConstants.BUNDLE_CHECK_TYPE,mActivity.getString(R.string.tab_doodle));

            Fragment fragment = new AddNewPreliminaryExaminationFragment(data);
            fragment.setArguments(mBundle);


            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager =
                        ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager =
                        ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager =
                        ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager =
                        ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_preliminary_examination))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_preliminary_examination))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Call Print Final Vision
     */
    private void callToPrintFinalVision(String appointmentID, boolean mHistoryAndComplains, boolean visionUCVA, boolean visionFinal, boolean visionBCVAUndilated, boolean visionBCVADilated, boolean mPrimaryExamination, boolean diagnosis, boolean counselingDetails, boolean investigationSuggested, boolean treatmentSuggested, boolean surgeryDetails, boolean prescription,String lang,boolean discharge, boolean payment) {
        try{
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);


            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPrintReceiptJson(AddNewPreliminaryExaminationFragment.PreliminaryExminationId,
                            String.valueOf(appointmentID),mHistoryAndComplains,visionUCVA, visionBCVAUndilated,
                            visionFinal, visionBCVADilated, mPrimaryExamination , diagnosis,investigationSuggested,
                            treatmentSuggested, counselingDetails,surgeryDetails, prescription, lang,discharge,payment,hospital_database));


            Call<PrintReceiptModel> call = RetrofitClient.createService(ApiInterface.class).PrintReceipt(body);
            call.enqueue(new Callback<PrintReceiptModel>() {
                @Override
                public void onResponse(@NonNull Call<PrintReceiptModel> call, @NonNull Response<PrintReceiptModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() &&
                                response.body().getError()==AppConstants.API_SUCCESS_ERROR) {
                            if (response.body() != null) {
                                String url=response.body().getData();
                                callToReceiptPrint(url);
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PrintReceiptModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Call To Receipt Print
     * @param url - String Url Print
     */
    private void callToReceiptPrint(String url) {
        /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);*/

        //   String pdfOpen=AppConstants.PDF_OPEN+url;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mActivity.startActivity(browserIntent);
    }
}
