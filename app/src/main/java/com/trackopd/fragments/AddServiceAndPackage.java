package com.trackopd.fragments;

import android.app.Activity;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.gson.Gson;
import com.trackopd.R;
import com.trackopd.adapter.PackageExpandableListAdapter;
import com.trackopd.adapter.ServicesExpandableListAdapter;
import com.trackopd.model.BillingListItemModel;
import com.trackopd.model.ServicesPackagesModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.utils.StringUtils;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddServiceAndPackage extends Fragment {

    private View view;
    private Activity mActivity;
    private ExpandableListView mExpandableService,mExpandablePackage;
    private RadioGroup mRadioGroup;
    private RadioButton mRadioButtonServices, mRadioButtonPackage,mRadioButtonOther;
    private AppUtil mAppUtils;
    private static ArrayList<ServicesPackagesModel.Service> mArrServices;
    private static ArrayList<ServicesPackagesModel.Package> mArrPackage;
    private ServicesExpandableListAdapter mServicesExpandableListAdapter;
    private PackageExpandableListAdapter mPackageExpandableListAdapter;
    private LinearLayout mLinearOther,mLinearOtherContainer;
    private ImageView mImageOtherAdd;
    private Button mButtonOtherSubmit;

    private boolean isVisible=false,isStarted=false;

    public AddServiceAndPackage() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_services_and_package, container, false);
        mActivity=getActivity();
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mAppUtils=new AppUtil(mActivity);
        mArrServices=new ArrayList<>();
        mArrPackage=new ArrayList<>();

        getIds();
        setRegister();
        callServiceAndPackageAPI();
        setOtherItems();
        return view;
    }


    /**
     *  get Declare Id on
     */
    private void getIds() {
        try{
            mExpandableService=view.findViewById(R.id.expandable_list_services_and_package_view_services);
            mExpandablePackage=view.findViewById(R.id.expandable_list_services_and_package_view_package);

            mRadioButtonServices = view.findViewById(R.id.radio_button_fragment_Services_service);
            mRadioButtonPackage = view.findViewById(R.id.radio_button_fragment_Services_package);
            mRadioButtonOther = view.findViewById(R.id.radio_button_fragment_Services_other);

            mRadioGroup = view.findViewById(R.id.radio_group_fragment_services);

            mLinearOther=view.findViewById(R.id.linear_add_services_and_package_other);
            mLinearOtherContainer=view.findViewById(R.id.linear_add_services_and_package_other_container);

            mImageOtherAdd=view.findViewById(R.id.image_linear_add_services_and_package_other_add);

            mButtonOtherSubmit=view.findViewById(R.id.button_add_service_and_package_other_submit);
            mRadioButtonServices.setChecked(true);



        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }
    private void setRegister() {
        try {
            mRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    if (mRadioGroup.getCheckedRadioButtonId() == R.id.radio_button_fragment_Services_service) {

                        mExpandableService.setVisibility(View.VISIBLE);
                        mExpandablePackage.setVisibility(View.GONE);
                        mLinearOther.setVisibility(View.GONE);

                        //selected Service Tag
                        mRadioButtonServices.setBackgroundResource(R.drawable.tab_left_selected);
                        mRadioButtonServices.setTextColor(mActivity.getResources().getColor(R.color.colorWhite));

                        //unselected Package
                        mRadioButtonPackage.setBackgroundResource(R.drawable.tab_defult_selected);
                        mRadioButtonPackage.setTextColor(Common.setThemeColor(mActivity));
                        mRadioButtonOther.setBackgroundResource(R.drawable.tab_right_defult_selected);
                        mRadioButtonOther.setTextColor(Common.setThemeColor(mActivity));


                    } else if (mRadioGroup.getCheckedRadioButtonId()
                            == R.id.radio_button_fragment_Services_package) {

                        mExpandableService.setVisibility(View.GONE);
                        mExpandablePackage.setVisibility(View.VISIBLE);
                        mLinearOther.setVisibility(View.GONE);

                        //un selected Services
                        mRadioButtonServices.setBackgroundResource(R.drawable.tab_left_defult_selected);
                        mRadioButtonServices.setTextColor(Common.setThemeColor(mActivity));
                        mRadioButtonOther.setBackgroundResource(R.drawable.tab_right_defult_selected);
                        mRadioButtonOther.setTextColor(Common.setThemeColor(mActivity));

                        //selected Package
                        mRadioButtonPackage.setBackgroundResource(R.drawable.tab_selected);
                        mRadioButtonPackage.setTextColor(mActivity.getResources().getColor(R.color.colorWhite));
                    }else if (mRadioGroup.getCheckedRadioButtonId()
                            == R.id.radio_button_fragment_Services_other){

                        mExpandableService.setVisibility(View.GONE);
                        mExpandablePackage.setVisibility(View.GONE);
                        mLinearOther.setVisibility(View.VISIBLE);

                        //un selected Services
                        mRadioButtonServices.setBackgroundResource(R.drawable.tab_left_defult_selected);
                        mRadioButtonServices.setTextColor(Common.setThemeColor(mActivity));
                        mRadioButtonPackage.setBackgroundResource(R.drawable.tab_defult_selected);
                        mRadioButtonPackage.setTextColor(Common.setThemeColor(mActivity));

                        //selected Package
                        mRadioButtonOther.setBackgroundResource(R.drawable.tab_right_selected);
                        mRadioButtonOther.setTextColor(mActivity.getResources().getColor(R.color.colorWhite));

                    }
                }
            });


            //TODO Set On Click Listener
            mImageOtherAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mButtonOtherSubmit.setVisibility(View.VISIBLE);
                    addOtherItem();
                }
            });

            mButtonOtherSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (checkOtherService()){
                        mButtonOtherSubmit.setVisibility(View.GONE);
                        setOtherDataOnSummaryList();
                    }
                }
            });

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     *
     * Return Status Check Other Apk
     * @return -return Status
     */
    private boolean checkOtherService() {

        boolean status=true;
        try{

            if (mLinearOtherContainer.getChildCount()>0){
               for (int i=0;i<mLinearOtherContainer.getChildCount();i++){

                   View childView=mLinearOtherContainer.getChildAt(i);
                   EditText  editName=childView.findViewById(R.id.edit_row_add_service_and_package_other_item_name);
                   EditText  editAmount=childView.findViewById(R.id.edit_row_add_service_and_package_other_item_name);

                   if (editName.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                       editName.setError(mActivity.getResources().getString(R.string.error_field_required));
                       status=false;
                   }
                   if (editAmount.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                       editAmount.setError(mActivity.getResources().getString(R.string.error_field_required));
                       status=false;
                   }
               }
            }else {
                Common.setCustomToast(mActivity,
                        mActivity.getResources().getString(R.string.error_please_click_add_other_item));
                status=false;
            }
        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
        return status;
    }

    /**
     * Add Other Data set on ArrayList
     */
    private void setOtherDataOnSummaryList() {
       try{

           if (mLinearOtherContainer.getChildCount()>0){

               for (int i=0;i<mLinearOtherContainer.getChildCount();i++){

                   View childView=mLinearOtherContainer.getChildAt(i);
                   EditText  editName=childView.findViewById(R.id.edit_row_add_service_and_package_other_item_name);
                   EditText  editAmount=childView.findViewById(R.id.edit_row_add_service_and_package_other_item_amount);

                   BillingListItemModel billingListItemModel
                           =new BillingListItemModel(editName.getText().toString(),"","",editAmount.getText().toString(),"Other");
                   AddPaymentFragment.mArrayBillingItem.add(billingListItemModel);
               }

           }
       }catch (Exception e){
           Common.insertLog(e.getMessage());
       }
    }

    /**
     * This is Add Item Layout inflate on Other can be show
     */
    private void addOtherItem() {
        try {
             View view_container=LayoutInflater.from(mActivity)
              .inflate(R.layout.row_add_service_and_package_other_item,null,false);

             EditText  editName=view_container.findViewById(R.id.edit_row_add_service_and_package_other_item_name);
             EditText  editAmount=view_container.findViewById(R.id.edit_row_add_service_and_package_other_item_name);
             ImageView mImageReove=view_container.findViewById(R.id.image_row_add_service_and_package_other_item_delete);

             mImageReove.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     mLinearOtherContainer.removeView(view_container);
                 }
             });

             if (mLinearOtherContainer.getChildCount()==0){
                 mImageReove.setVisibility(View.GONE);
                 mButtonOtherSubmit.setVisibility(View.VISIBLE);
             }

             mLinearOtherContainer.addView(view_container);
        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Call API on Get Services And Packages
     */
    private void callServiceAndPackageAPI() {
        if (!mAppUtils.getConnectionState()){

        }else {
            callToServiceAPI();
        }
    }

    /**
     * Method should to call Service and Package API
     */
    private void callToServiceAPI() {
        try{



            String hospital_database= GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPaymentServicesPackageListJson(hospital_database));

            Call<ServicesPackagesModel> call = RetrofitClient.createService(ApiInterface.class).getPaymentServicesPackageList(body);
            call.enqueue(new Callback<ServicesPackagesModel>() {
                @Override
                public void onResponse(@NonNull Call<ServicesPackagesModel> call, @NonNull Response<ServicesPackagesModel> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        
                        if (response.isSuccessful()&&
                                response.body().getError().equals(AppConstants.API_SUCCESS_ERROR)){

                            ArrayList<ServicesPackagesModel.Service> services =
                                    (ArrayList<ServicesPackagesModel.Service>) response.body().getService();
                            ArrayList<ServicesPackagesModel.Package> packages =
                                    (ArrayList<ServicesPackagesModel.Package>) response.body().getPackage();

                            if (AddPaymentFragment.mArrayBillingItem != null
                                  && AddPaymentFragment.mArrayBillingItem.size()>0)
                            {
                                bindServices(services);
                                bindPackage(packages);
                            } else {
                                mArrServices.addAll(services);
                                mArrPackage.addAll(packages);
                            }

                            if (mArrServices!=null&&  mArrServices.size()>0) {
                                setServicesAdapter();
                            }

                            if (mArrPackage!=null&&mArrPackage.size()>0) {
                                setPackageAdapter();
                            }
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ServicesPackagesModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });


        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }


    private void bindPackage(ArrayList<ServicesPackagesModel.Package> packages)
    {
        try {
            if (AddPaymentFragment.mArrayBillingItem!=null
                    && AddPaymentFragment.mArrayBillingItem.size()>0) {

                for (int i = 0; i < packages.size(); i++) {
                    String strPackageId = packages.get(i).getPackageID();
                    for (int k = 0; k < AddPaymentFragment.mArrayBillingItem.size(); k++) {

                        if (AddPaymentFragment.mArrayBillingItem.
                                get(k).getType().equalsIgnoreCase("Package")){

                            String Parent_id=AddPaymentFragment.mArrayBillingItem.get(k).getParentId();

                            if (Parent_id.equalsIgnoreCase(strPackageId)){
                                packages.get(i).setSelected(true);
                            }
                        }
                    }
                    mArrPackage.add(packages.get(i));
                }
            } else {
                mArrPackage.addAll(packages);
            }

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     *
     * @param services - Services Array
     */
    private void bindServices(ArrayList<ServicesPackagesModel.Service> services) {
        try {
            if (services != null && services.size() > 0) {

                for (int i = 0; i < services.size(); i++) {

                    for (int j = 0; j < services.get(i).getItem().size(); j++) {

                        String strVendorService = services.get(i).getItem().get(j).getBillingID();

                        if (AddPaymentFragment.mArrayBillingItem!=null
                                && AddPaymentFragment.mArrayBillingItem.size()>0)
                        {
                            for (int k = 0; k < AddPaymentFragment.mArrayBillingItem.size(); k++)
                            {
                                if (AddPaymentFragment.mArrayBillingItem.get(k).getType().
                                        equalsIgnoreCase("Service")){

                                    String billing_id = AddPaymentFragment.mArrayBillingItem.get(k).getId();
                                    if (billing_id.equalsIgnoreCase(strVendorService)){
                                        services.get(i).getItem().get(j).setSelected(true);
                                    }
                                }
                            }
                        }
                    }
                    mArrServices.add(services.get(i));
                }
            } else {
                mArrServices.addAll(services);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * set Service Expandable List View
     */
    private void setServicesAdapter() {
        try{
            if (mArrServices!=null && mArrServices.size() > 0) {

                mServicesExpandableListAdapter = new ServicesExpandableListAdapter(
                        getContext(), mArrServices, mExpandableService) {
                    @Override
                    protected void addItem(ServicesPackagesModel.Service service, int position) {
                        super.addItem(service, position);

                        BillingListItemModel billingListItemModel
                                =new BillingListItemModel(service.getItem().get(position).getServicesName(),
                                service.getItem().get(position).getBillingID(),
                                service.getItem().get(position).getParentID(),
                                service.getItem().get(position).getRate(),"Service");

                        AddPaymentFragment.mArrayBillingItem.add(billingListItemModel);
                    }

                    @Override
                    protected void removeItem(ServicesPackagesModel.Service packages, int position) {
                        super.removeItem(packages, position);
                        removeBillingItem(packages.getBillingID());
                    }
                };

                mExpandableService.setAdapter(mServicesExpandableListAdapter);
                //mExpandableService.setVisibility(View.VISIBLE);
                mExpandableService.expandGroup(0);
            }
        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }
    /**
     * set Service Expandable List View
     */
    private void setPackageAdapter() {
        try{
            try{
                if (mArrServices!=null && mArrServices.size() > 0) {

                    mPackageExpandableListAdapter = new PackageExpandableListAdapter(
                            getContext(), mArrPackage, mExpandablePackage) {

                        @Override
                        protected void addItem(ServicesPackagesModel.Package packages, int position) {
                            super.addItem(packages, position);
                            BillingListItemModel billingListItemModel
                                    =new BillingListItemModel(packages.getTitle(),
                                    packages.getPackageID(),packages.getPackageID(),
                                    packages.getDiscountAmount(),"Package");

                            AddPaymentFragment.mArrayBillingItem.add(billingListItemModel);
                        }

                        @Override
                        protected void removeItem(ServicesPackagesModel.Package packages, int position) {
                            super.removeItem(packages, position);
                            removeBillingItem(packages.getPackageID());

                        }
                    };

                    mExpandablePackage.setAdapter(mPackageExpandableListAdapter);

                    //mExpandablePackage.setVisibility(View.VISIBLE);
                    mExpandablePackage.expandGroup(0);
                }
            }catch (Exception e){
                Common.insertLog(e.getMessage());
            }
        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Package Id
     * @param packageID - String Package ID
     */
    private void removeBillingItem(String packageID) {
        if (AddPaymentFragment.mArrayBillingItem.size()>0){
           for (int i=0;i<AddPaymentFragment.mArrayBillingItem.size();i++){
               if (AddPaymentFragment.mArrayBillingItem.get(i).getId().
                       equalsIgnoreCase(packageID)){
                   AddPaymentFragment.mArrayBillingItem.remove(i);
               }
           }
        }else {
            //AddPaymentFragment.setVisibilityNext(false);
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isStarted && isVisible) {
//            mArrServices.clear();
//            mArrPackage.clear();
//            mLinearOtherContainer.removeAllViews();
//           // callServiceAndPackageAPI();
//            setOtherItems();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
//            mArrServices.clear();
//            mArrPackage.clear();
//            mLinearOtherContainer.removeAllViews();
//            callServiceAndPackageAPI();
//            setOtherItems();
        }
    }

    /**
     * Other Item cam already Exit
     */
    private void setOtherItems() {
      try{
          if (AddPaymentFragment.mArrayBillingItem!=null
           && AddPaymentFragment.mArrayBillingItem.size()>0){

              for (int o=0;o<AddPaymentFragment.mArrayBillingItem.size();o++){

                if (AddPaymentFragment.mArrayBillingItem.get(o).getType().
                        equalsIgnoreCase("Other")){

                    OtherItemAddWitValue(AddPaymentFragment.mArrayBillingItem.get(o));
                }
             }

             //No Item add Other than show On
             if (mLinearOtherContainer.getChildCount()==0){
                addOtherItem();
             }

          }else {
              //No Item add Other than show On
              addOtherItem();
          }
      }catch (Exception e){
          Common.insertLog(e.getMessage());
      }
    }

    /**
     * Set Other Data add On Layout
     * @param billingListItemModel -Billing Model Class
     */
    private void OtherItemAddWitValue(BillingListItemModel billingListItemModel) {
        try {
            View view_container=LayoutInflater.from(mActivity)
                    .inflate(R.layout.row_add_service_and_package_other_item,null,false);

            EditText  editName=view_container.findViewById(R.id.edit_row_add_service_and_package_other_item_name);
            EditText  editAmount=view_container.findViewById(R.id.edit_row_add_service_and_package_other_item_amount);
            ImageView mImageReove=view_container.findViewById(R.id.image_row_add_service_and_package_other_item_delete);

            editName.setText(billingListItemModel.getName());
            editAmount.setText(billingListItemModel.getRate());

            mImageReove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLinearOtherContainer.removeView(view_container);
                }
            });

            if (mLinearOtherContainer.getChildCount()==0){
                mImageReove.setVisibility(View.GONE);
            }

            mLinearOtherContainer.addView(view_container);
        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }
}
