package com.trackopd.fragments;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.model.DoctorModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchPatientAppointmentFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private EditText mEditAppointmentNumber, mEditDate;
    private Spinner mSpinnerDoctor;
    private Button mButtonSearch;
    private ImageView mImageDoctor;
    private ArrayList<String> mArrDoctor;
    private ArrayList<DoctorModel> mArrDoctorList;
    private int mSelectedDoctorIndex, mDoctorId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_search_patient_appointment, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrDoctor = new ArrayList<>();
        mArrDoctorList = new ArrayList<>();

        getIds();
        setData();
        setRegListeners();
        callToDoctorAPI();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Set the prepare menu options
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Edit Texts
            mEditAppointmentNumber = mView.findViewById(R.id.edit_search_patient_appointment_no);
            mEditDate = mView.findViewById(R.id.edit_search_patient_appointment_date);

            // Spinners
            mSpinnerDoctor = mView.findViewById(R.id.spinner_search_patient_appointment_doctor);

            // Buttons
            mButtonSearch = mView.findViewById(R.id.button_search_patient_appointment_search);

            // Image Views
            mImageDoctor = mView.findViewById(R.id.image_search_patient_appointment_doctor);

            // Set Request Focus
            mEditAppointmentNumber.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            // Set Image Color as per the theme applied
            mImageDoctor.setColorFilter(Common.setThemeColor(mActivity));

            //ToDo: Sets the spinner color as per the theme applied
            mSpinnerDoctor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Click Listeners
            mButtonSearch.setOnClickListener(clickListener);
            mEditDate.setOnClickListener(clickListener);

            // ToDo: Doctor - Spinner Item Select Listener
            mSpinnerDoctor.setOnItemSelectedListener(onItemSelectedListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.button_search_patient_appointment_search:
                    callToPatientAppointmentFragment();
                    break;

                case R.id.edit_search_patient_appointment_date:
                    Common.openDatePickerForAllDates(mActivity, mEditDate);
                    break;
            }
        }
    };

    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {
                case R.id.spinner_search_patient_appointment_doctor:
                    mSelectedDoctorIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedDoctorIndex != 0) {
                        mDoctorId = (Integer.parseInt(mArrDoctorList.get(mSelectedDoctorIndex - 1).getDoctorID()));
                        String mDoctorName = (mArrDoctorList.get(mSelectedDoctorIndex - 1).getDoctorName());
                        Common.insertLog("mDoctorID::> " + mDoctorId);
                        Common.insertLog("mDoctorName::> " + mDoctorName);
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    /**
     * This method should call the Doctor API
     */
    private void callToDoctorAPI() {
        try {

            String hospital_database= GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setDoctorListJson(hospital_database));

            Call<DoctorModel> call = RetrofitClient.createService(ApiInterface.class).getDoctorList(body);
            call.enqueue(new Callback<DoctorModel>() {
                @Override
                public void onResponse(@NonNull Call<DoctorModel> call, @NonNull Response<DoctorModel> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrDoctorList.addAll(response.body().getData());
                            }
                            setAdapterData();
                        } else {
                            //Common.setCustomToast(mActivity, mMessage);
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrDoctorList.addAll(response.body().getData());
                            }
                            setAdapterData();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DoctorModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setAdapterData();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mArrDoctor.add(0, getString(R.string.spinner_select_doctor));
            if (mArrDoctorList.size() > 0) {
                for (DoctorModel doctorModel : mArrDoctorList) {
                    mArrDoctor.add(doctorModel.getDoctorName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerDoctor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrDoctor) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedDoctorIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerDoctor.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Patient Appointment Fragment
     */
    private void callToPatientAppointmentFragment() {
        try {
            String mAppointmentDate;

            String mAppointmentNo = mEditAppointmentNumber.getText().toString().trim();

            if (mEditDate.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                mAppointmentDate = mActivity.getResources().getString(R.string.date_format_first_time);
            } else {
                mAppointmentDate = Common.convertDateWithoutTime(mActivity, mEditDate.getText().toString().trim(), mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
            }

            if (mSpinnerDoctor.getSelectedItemPosition() == 0) {
                mDoctorId = -1;
            }

            ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_appointment));
            Fragment fragment = new PatientAppointmentFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, mAppointmentNo);
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DATE, mAppointmentDate);
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, mDoctorId);
            fragment.setArguments(args);
            FragmentManager fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    mActivity.getResources().getString(R.string.nav_menu_appointment)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}