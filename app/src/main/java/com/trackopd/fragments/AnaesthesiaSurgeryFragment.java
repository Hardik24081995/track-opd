package com.trackopd.fragments;


import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.model.AddMediclaimModel;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.model.SurgeryListModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class AnaesthesiaSurgeryFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private EditText mEditAnaesthesiaMedicine,mEditPulse,mEditBP,mEditSPO2,mEditRBS;
    private Spinner mSpinnerAnaesthesiaType,mSpinnerEye;
    private ImageView mImageAnaesthesiaType,mImageEye;
    private Button mButtonSubmit;
    private ArrayList<String> mArrEye,mArrAnaesthesiaType;
    private int mSelectedEyeIndex=0;
    public ArrayList<PreliminaryExaminationDetailsModel.Data.Eye> mArrEyeList;
    public ArrayList<PreliminaryExaminationDetailsModel.Data.AnaesthesiaType> mArrAnaesthesiaTypeList;
    public AppUtil mAppUtils;
    private SurgeryListModel mSurgeryDetail;
    private String AppointmentID,mUserType;

    public AnaesthesiaSurgeryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      // Inflate the layout for this fragment
      mView= inflater.inflate(R.layout.fragment_anaesthesia_surgery, container, false);

      mActivity=getActivity();
      AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
      mArrEye=new ArrayList<>();
      mArrEyeList=new ArrayList<>();
      mArrAnaesthesiaType=new ArrayList<>();
      mArrAnaesthesiaTypeList=new ArrayList<>();
      mAppUtils=new AppUtil(mActivity);
      mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

      getBundle();
      getIds();
      setData();
      setRegListeners();
      //setEyeAdapter();
      callToPreliminaryExaminationAPI();

      return mView;
    }

    /**
     * This Method get Bundle Value can be pass
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle.getSerializable(AppConstants.BUNDLE_ADD_SURGERY)!=null) {
                mSurgeryDetail = (SurgeryListModel) bundle.getSerializable(AppConstants.BUNDLE_ADD_SURGERY);
                AppointmentID = mSurgeryDetail.getAppointmentID();
            }else {
                AppointmentID=bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_ID);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
       try{
           mEditPulse=mView.findViewById(R.id.edit_text_anaesthesia_surgery_pulse);
           mEditAnaesthesiaMedicine=mView.findViewById(R.id.edit_text_anaesthesia_surgery_anaesthesia_medicine);
           mEditBP=mView.findViewById(R.id.edit_text_anaesthesia_surgery_bp);
           mEditSPO2=mView.findViewById(R.id.edit_text_anaesthesia_surgery_spo_two);
           mEditRBS=mView.findViewById(R.id.edit_text_anaesthesia_surgery_rbs);

           mSpinnerAnaesthesiaType=mView.findViewById(R.id.spinner_anaesthesia_surgery_anaesthesia_type);
           mSpinnerEye=mView.findViewById(R.id.spinner_anaesthesia_surgery_eye);

           mImageAnaesthesiaType=mView.findViewById(R.id.image_anaesthesia_surgery_anaesthesia_type);
           mImageEye=mView.findViewById(R.id.image_anaesthesia_surgery_eye);

           mButtonSubmit=mView.findViewById(R.id.button_anaesthesia_surgery_submit);
       }catch (Exception e){
           Common.insertLog(e.getMessage());
       }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            // Set Image Color as per the theme applied
            mImageAnaesthesiaType.setColorFilter(Common.setThemeColor(mActivity));
            mImageEye.setColorFilter(Common.setThemeColor(mActivity));

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerAnaesthesiaType.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerEye.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);


            if (mSurgeryDetail!=null){
               mEditPulse.setText(mSurgeryDetail.getPulse());
               mEditBP.setText(mSurgeryDetail.getBP());
               mEditSPO2.setText(mSurgeryDetail.getSPO2());
               mEditRBS.setText(mSurgeryDetail.getRBS());
               mEditAnaesthesiaMedicine.setText(mSurgeryDetail.getAnaesthesiaMedicine());

               String AnaesthesiaTypeID=mSurgeryDetail.getAnaesthesiaTypeID();
               for (int a=0;a<mArrAnaesthesiaTypeList.size();a++){
                  if (mArrAnaesthesiaTypeList.get(a).getAnaesthesiaTypeID().
                          equalsIgnoreCase(AnaesthesiaTypeID)){
                      int pos=a+1;
                      mSpinnerAnaesthesiaType.setSelection(pos);

                  }
               }
               String eyeID=mSurgeryDetail.getEyeID();
                for (int e=0;e<mArrEyeList.size();e++){
                    if (mArrEyeList.get(e).getEyeID().
                            equalsIgnoreCase(eyeID)){
                        int pos=e+1;
                        mSpinnerEye.setSelection(pos);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * this is Method Bind  Eye Adapter
     */
    private void setEyeAdapter() {
      try{
          mArrEye.add(0,mActivity.getResources().getString(R.string.spinner_select_eye));
          if (mArrEyeList.size() > 0) {
              for (PreliminaryExaminationDetailsModel.Data.Eye eyeModel : mArrEyeList) {
                  mArrEye.add(eyeModel.getEye());
              }
          }

          ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrEye) {
              @Override
              public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                  // Cast the drop down items (popup items) as text view
                  TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                  // Set the text color of drop down items
                  tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                  // If this item is selected item
                  if (position == mSelectedEyeIndex) {
                      // Set spinner selected popup item's text color
                      tv.setTextColor(Common.setThemeColor(mActivity));
                  }
                  // Return the modified view
                  return tv;
              }
          };
          adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
          mSpinnerEye.setAdapter(adapter);
      }catch (Exception e){
          Common.insertLog(e.getMessage());
      }
    }

    /**
     * This method should call the Preliminary Examination API
     */
    private void callToPreliminaryExaminationAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPreliminaryExaminationDetailsJson(hospital_database));

            Call<PreliminaryExaminationDetailsModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationDetails(body);
            call.enqueue(new Callback<PreliminaryExaminationDetailsModel>() {
                @Override
                public void onResponse(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Response<PreliminaryExaminationDetailsModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null) {

                                // Eye Array Binding
                                mArrEyeList.addAll(response.body().getData().getEye());
                                setEyeAdapter();

                                // Anaesthesia Type Array Type
                                mArrAnaesthesiaTypeList.addAll(response.body().getData().getAnaesthesiaType());
                                setAnaesthesiaTypeAdapter();
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * this is Method Bind  Eye Adapter
     */
    private void setAnaesthesiaTypeAdapter() {
        try{
            mArrAnaesthesiaType.add(0,mActivity.getResources().getString(R.string.spinner_select_anaesthesia));
            if (mArrAnaesthesiaTypeList.size() > 0) {
                for (PreliminaryExaminationDetailsModel.Data.AnaesthesiaType anaesthesiaTypeModel : mArrAnaesthesiaTypeList) {
                    mArrAnaesthesiaType.add(anaesthesiaTypeModel.getAnaesthesiaType());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrAnaesthesiaType) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedEyeIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerAnaesthesiaType.setAdapter(adapter);
        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Edit Click Listeners
            mButtonSubmit.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_anaesthesia_surgery_submit:
                    if (checkAnaesthesiaValidation()){
                        callToAnaesthesiaSurgery();
                    }
                    break;
            }
        }
    };

    /**
     *
     */
    private void callToAnaesthesiaSurgery() {
        if (!mAppUtils.getConnectionState()){
           Common.setCustomToast(mActivity,mActivity.getResources().getString(R.string.no_internet_connection));
        }else {
           try{
               String pulse=mEditPulse.getText().toString();
               String bp=mEditBP.getText().toString();
               String spo2=mEditSPO2.getText().toString();
               String RBS=mEditRBS.getText().toString();
               String Anaesthesia_Medicine=mEditAnaesthesiaMedicine.getText().toString();

               String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
               String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

               int selected_eye=mSpinnerEye.getSelectedItemPosition();
               String eye_id=mArrEyeList.get(selected_eye-1).getEyeID();

               int selected_anaesthesia=mSpinnerAnaesthesiaType.getSelectedItemPosition();
               String anaesthesia_id=mArrAnaesthesiaTypeList.get(selected_anaesthesia-1).getAnaesthesiaTypeID();

               RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                       APICommonMethods.setAddAnaesthesiaSurgeryJson(mUserId, mDatabaseName,AppointmentID,pulse,bp,spo2,
                               RBS,eye_id,anaesthesia_id,Anaesthesia_Medicine, ""));
               Common.insertLog("Request "+requestBody);

               Call<AddMediclaimModel> call = RetrofitClient.createService(ApiInterface.class).AddAnaesthesiaSurgery(requestBody);

               call.enqueue(new Callback<AddMediclaimModel>() {
                   @Override
                   public void onResponse(Call<AddMediclaimModel> call, Response<AddMediclaimModel> response) {
                       Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                       try {
                           String mJson = (new Gson().toJson(response.body()));
                           JSONObject jsonObject = new JSONObject(mJson);
                           String mMessage = jsonObject.getString(WebFields.MESSAGE);
                           int mError = jsonObject.getInt(WebFields.ERROR);
                           Common.insertLog("mMessage:::> " + mMessage);

                           if (response.isSuccessful() && mError == 200) {
                               if (response.body() != null) {
                                   /*removeAllFragments();
                                   callToMediclaimList();*/

                                   Common.setCustomToast(mActivity,mMessage);
                                   AddSurgeryFragment.setPage(2);
                               }

                           }
                       } catch (JSONException e) {
                           e.printStackTrace();
                       }
                   }

                   @Override
                   public void onFailure(Call<AddMediclaimModel> call, Throwable t) {
                       Common.insertLog("Failure:::> " + t.getMessage());
                   }
               });
           }catch (Exception e){
               Common.insertLog(e.getMessage());
           }
        }
    }

    /**
     * Check Anaesthesia Surgery Detail
     * @return
     */
    private boolean checkAnaesthesiaValidation() {
        boolean status=true;
        return status;
    }

    /**
     * Used to remove all fragment in the fragment manager after logout
     */
    private void removeAllFragments() {
        try {
            List<Fragment> fragments = null;

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragments = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist)))
            {
                fragments = ((OptometristHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragments = ((DoctorHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragments = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            }


            if (fragments.size() > 0) {
                for (int i = 0; i < fragments.size(); i++) {
                    if (fragments.get(i) != null) {

                        if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                            ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                            ((OptometristHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                            ((DoctorHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                            ((CouncillorHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callToMediclaimList() {
        try {
            Fragment fragment = new SurgeryListFragment();
            Bundle args = new Bundle();

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
            }

            args.putString(AppConstants.BUNDLE_PATIENT_NAME, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_FOLLOW_UP_DATE, "1970-01-01");
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, -1);
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_COUNCILLOR, -1);
            //fragment.setArguments(args);

            FragmentManager fragmentManager = null;

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.nav_menu_surgery))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
