package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

public class SearchReceptionistExistingAppointmentFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private EditText mEditMRDNumber, mEditAppointmentNumber, mEditDate;
    private Button mButtonSearch;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_search_receptionist_existing_appointment, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        getIds();
        setRegListeners();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Set the prepare menu options
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Edit Texts
            mEditMRDNumber = mView.findViewById(R.id.edit_search_receptionist_existing_appointment_mrd_number);
            mEditAppointmentNumber = mView.findViewById(R.id.edit_search_receptionist_existing_appointment_number);
            mEditDate = mView.findViewById(R.id.edit_search_receptionist_existing_appointment_date);

            // Buttons
            mButtonSearch = mView.findViewById(R.id.button_search_receptionist_existing_appointment_search);

            // Set Request Focus
            mEditMRDNumber.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Click Listeners
            mButtonSearch.setOnClickListener(clickListener);
            mEditDate.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.button_search_receptionist_existing_appointment_search:
                    callToReceptionistAppointmentFragment();
                    break;

                case R.id.edit_search_receptionist_existing_appointment_date:
                    Common.openDatePickerForAllDates(mActivity, mEditDate);
                    break;
            }
        }
    };

    /**
     * Sets up the Receptionist Appointment Fragment
     */
    private void callToReceptionistAppointmentFragment() {
        try {
            String mAppointmentDate;

            String mAppointmentNo = mEditAppointmentNumber.getText().toString().trim();
            String mMRDNo = mEditMRDNumber.getText().toString().trim();

            if (mEditDate.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                mAppointmentDate = mActivity.getResources().getString(R.string.date_format_first_time);
            } else {
                mAppointmentDate = Common.convertDateWithoutTime(mActivity, mEditDate.getText().toString().trim(), mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
            }

            ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_appointment));
            Fragment fragment = new ReceptionistAppointmentFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_ADD_APPOINTMENT, "1");
            args.putString(AppConstants.BUNDLE_PATIENT_NAME, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_MRD_NO, mMRDNo);
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, mAppointmentNo);
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DATE, mAppointmentDate);
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, -1);
            fragment.setArguments(args);
            FragmentManager fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    mActivity.getResources().getString(R.string.nav_menu_appointment)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
