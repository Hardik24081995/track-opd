package com.trackopd.fragments;


import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.BillingSummaryItemAdapter;
import com.trackopd.model.AddBillingPaymentModel;
import com.trackopd.model.BillingListItemModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class BillingSummaryFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLinearManager;
    public static Button mButtonSubmit;
    private EditText mEditReceivedAmount,mEditDiscount,mEditSubTotal,mEditTotal,mEditOutStanding,
                     mEditChequeNo,mEditIFSC,mEditUTR,mEditBankName,mEditBranchName,mEditAccNo,
                     mEditContactNo;
    private BillingSummaryItemAdapter mBillingSummaryItemAdapter;
    private AppUtil mAppUtils;
    private boolean isStarted=false,isVisible=false;
    private double subTotal=0,Total=0,discount=0,received_Amount=0,outstanding=0;
    private RadioGroup mRadioGroupPaymentType;
    private RadioButton mRadioButtonCash,mRadioButtonCheque,mRadioButtonOnline;
    private LinearLayout mLinearPayment;
    private TextInputLayout mTextInputCheque,mTextInputIFSC,mTextInputUTR;
    private String strPaymetType;
    private View viewCheque,viewIFSC,viewUTR;

    public BillingSummaryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_billing_summary, container, false);
        mView=inflater.inflate(R.layout.fragment_billing_summary, container, false);

        mActivity=getActivity();
        mAppUtils=new AppUtil(mActivity);
        getIds();
        setAdapterBilling();
        setRegisetr();
        return mView;
    }

    /**
     *  get Declare Id on
     */
    private void getIds() {
        try{
            //Edit Text
            //mEditDiscount=mView.findViewById(R.id.edit_billing_summary_discount);
            mEditReceivedAmount=mView.findViewById(R.id.edit_billing_summary_receive_amount);
            mEditSubTotal=mView.findViewById(R.id.edit_billing_summary_sub_total);
            mEditTotal=mView.findViewById(R.id.edit_billing_summary_total_price);
            mEditOutStanding=mView.findViewById(R.id.edit_billing_summary_outstading_amount);
            mEditChequeNo=mView.findViewById(R.id.edit_billing_summary_outstading_cheque_no);
            mEditIFSC=mView.findViewById(R.id.edit_billing_summary_ifsc_code);
            mEditUTR=mView.findViewById(R.id.edit_billing_summary_outstading_utr_no);
            mEditBankName=mView.findViewById(R.id.edit_billing_summary_outstading_bank_name);
            mEditBranchName=mView.findViewById(R.id.edit_billing_summary_branch);
            mEditAccNo=mView.findViewById(R.id.edit_billing_summary_account_no);
            mEditContactNo=mView.findViewById(R.id.edit_billing_summary_mobile);

            //Linear Layout
            mLinearPayment=mView.findViewById(R.id.linear_billing_summary_payment);

            //Button
            mButtonSubmit=mView.findViewById(R.id.button_billing_summary_pay_now);

            // Recycler View
            mRecyclerView=mView.findViewById(R.id.recycler_view_billing_summary_list);

            mLinearManager=new LinearLayoutManager(mActivity,LinearLayoutManager.VERTICAL,false);
            mRecyclerView.setLayoutManager(mLinearManager);

            //Radio Group
            mRadioGroupPaymentType=mView.findViewById(R.id.radio_group_billing_summary_payment_type);

            //Radio Button
            mRadioButtonCash=mView.findViewById(R.id.radio_button_billing_summary_cash);
            mRadioButtonCheque=mView.findViewById(R.id.radio_button_billing_summary_cheque);
            mRadioButtonOnline=mView.findViewById(R.id.radio_button_billing_summary_online_payment);

            //Text Input Layout
            mTextInputCheque=mView.findViewById(R.id.text_input_billing_summary_cheque_no);
            mTextInputIFSC=mView.findViewById(R.id.text_input_billing_summary_ifsc_code);
            mTextInputUTR=mView.findViewById(R.id.text_input_billing_summary_utr_no);

            //View
            viewCheque=mView.findViewById(R.id.view_billing_summary_cheque);
            viewIFSC=mView.findViewById(R.id.view_billing_summary_ifsc);
            viewUTR=mView.findViewById(R.id.view_billing_summary_utr_no);

            mRadioButtonCash.setChecked(true);
            mLinearPayment.setVisibility(View.GONE);

        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Billing Item Adapter
     */
    private void setAdapterBilling() {
        try {
            if (AddPaymentFragment.mArrayBillingItem.size()>0){
                mBillingSummaryItemAdapter=new BillingSummaryItemAdapter(mActivity,
                                 mRecyclerView,AddPaymentFragment.mArrayBillingItem){
                    @Override
                    protected void removeBillingItem(BillingListItemModel data, int i) {
                        super.removeBillingItem(data, i);

                        AddPaymentFragment.mArrayBillingItem.remove(i);
                        mBillingSummaryItemAdapter.notifyDataSetChanged();
                        Calculate();
                    }

                    @Override
                    protected void UpdateRate(BillingListItemModel data, int i) {
                        super.UpdateRate(data, i);
                       /* AddPaymentFragment.mArrayBillingItem.remove(i);
                        mBillingSummaryItemAdapter.notifyDataSetChanged();*/

                        Calculate();
                    }
                };
                mRecyclerView.setAdapter(mBillingSummaryItemAdapter);
                Calculate();
            }
        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Calculate Some Array List
     */
    private void Calculate(){
       try{

           //set Default
           setDefault();

           if (AddPaymentFragment.mArrayBillingItem.size()>0){
               for (BillingListItemModel model:AddPaymentFragment.mArrayBillingItem)
               {
                   double rate = Double.parseDouble(model.getRate());
                   subTotal=subTotal+rate;
               }
           }

           //set Sub Total Value
           mEditSubTotal.setText(String.valueOf(subTotal));

           //set Total Value
           mEditTotal.setText(String.valueOf(subTotal));

           //received Amount
           if (!mEditReceivedAmount.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){

               received_Amount= Double.parseDouble(mEditReceivedAmount.getText().toString());
               if (received_Amount>subTotal){
                   mEditReceivedAmount.setError(mActivity.getResources().
                           getString(R.string.error_outstanding_payment));
               }else {
                   outstanding=subTotal-received_Amount;
                   mEditReceivedAmount.setError(null);
               }
           }
           //Out standing Payment
           mEditOutStanding.setText(String.valueOf(outstanding));
       }catch (Exception e){
           Common.insertLog(e.getMessage());
       }
    }

    /**
     *
     */
    private void setDefault() {
        subTotal=0;
        Total=0;
        discount=0;
        received_Amount=0;
    }

    /**
     * Register on Click Listener
     */
    private void setRegisetr() {

        mEditReceivedAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {

                    Calculate();
                }
            }
        });

        mButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (checkValidationPayNow()){

                    if (!mAppUtils.getConnectionState()){
                       Common.setCustomToast(mActivity,mActivity.getResources().
                               getString(R.string.no_internet_connection));
                    }else {
                        callToPayNow();
                    }
                }
            }
        });

        mRadioGroupPaymentType.setOnCheckedChangeListener(checkedChangeListener);
    }


    /**
     * Radio button on checked change listeners
     */
    private RadioGroup.OnCheckedChangeListener checkedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            RadioButton mRadioButton = group.findViewById(checkedId);
            mRadioButton.clearFocus();
            switch (mRadioButton.getId()) {
                case R.id.radio_button_billing_summary_cash:
                      strPaymetType=mActivity.getResources().getString(R.string.text_cash);
                      mLinearPayment.setVisibility(View.GONE);
                    break;
                case R.id.radio_button_billing_summary_cheque:
                      strPaymetType=mActivity.getResources().getString(R.string.text_cheque);
                      setPaymentMode();
                    break;
                case R.id.radio_button_billing_summary_online_payment:
                      strPaymetType=mActivity.getResources().getString(R.string.text_online_payment);
                      setPaymentMode();
                    break;
            }
        }
    };

    /**
     * Set Visible paymen Layout on Cheque or Online Payment
     */
    private void setPaymentMode() {
       if (strPaymetType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_cheque)))
       {
           //show
           mLinearPayment.setVisibility(View.VISIBLE);
           mTextInputCheque.setVisibility(View.VISIBLE);
           viewCheque.setVisibility(View.VISIBLE);

           //Hide
           mTextInputUTR.setVisibility(View.GONE);
           mTextInputIFSC.setVisibility(View.GONE);
           viewIFSC.setVisibility(View.GONE);
           viewUTR.setVisibility(View.GONE);
       }else if (strPaymetType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_online_payment)))
       {
           //Hide
           mLinearPayment.setVisibility(View.VISIBLE);
           mTextInputCheque.setVisibility(View.GONE);
           viewCheque.setVisibility(View.GONE);

           //Show
           mTextInputUTR.setVisibility(View.VISIBLE);
           mTextInputIFSC.setVisibility(View.VISIBLE);
           viewIFSC.setVisibility(View.VISIBLE);
           viewUTR.setVisibility(View.VISIBLE);
       }
    }

    /***
     * this method call Payment API
     */
    private void callToPayNow() {
        try{

            String appointmentID=AddPaymentFragment.mAppointmentNo;
            String patient_id=AddPaymentFragment.mPatientID;
            String paymentDate=AddPaymentFragment.mPaymentDate;
            String notes=AddPaymentFragment.mNotes;
            String mStatus = AppConstants.STR_STATUS_SUCCESS;
            String mUserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);


            String subTotal=mEditSubTotal.getText().toString();
            String total=mEditTotal.getText().toString();
            String received=mEditReceivedAmount.getText().toString();
            String outstanding=mEditOutStanding.getText().toString();
            String ChequeNo=AppConstants.STR_EMPTY_STRING;
            String bankName=AppConstants.STR_EMPTY_STRING;
            String branch=AppConstants.STR_EMPTY_STRING;
            String account_no=AppConstants.STR_EMPTY_STRING;
            String ifsc_code=AppConstants.STR_EMPTY_STRING;
            String utr_no=AppConstants.STR_EMPTY_STRING;

            if (strPaymetType.equalsIgnoreCase(mActivity.getResources().
                    getString(R.string.text_cheque))){
                 ChequeNo=mEditChequeNo.getText().toString();
                 bankName=mEditBankName.getText().toString();
                 branch=mEditBranchName.getText().toString();
                 account_no=mEditAccNo.getText().toString();
            }
            if (strPaymetType.equalsIgnoreCase(mActivity.getResources().
                    getString(R.string.text_online_payment))){
                ifsc_code=mEditIFSC.getText().toString();
                utr_no=mEditUTR.getText().toString();
                bankName=mEditBankName.getText().toString();
                branch=mEditBranchName.getText().toString();
                account_no=mEditAccNo.getText().toString();
            }

            JSONArray arrayItems=getArrPayItem();

            String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);


            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAddPaymentJson(patient_id,paymentDate,appointmentID,mStatus,notes,mUserID,
                    subTotal,total,received,outstanding,arrayItems,strPaymetType,ChequeNo,ifsc_code,
                    utr_no,bankName,branch,account_no,hospital_database));

            Common.insertLog("Request Payment...."+requestBody.toString());
            Call<AddBillingPaymentModel> call = RetrofitClient.createService(ApiInterface.class).addBillingsPayment(requestBody);
            call.enqueue(new Callback<AddBillingPaymentModel>() {
                @Override
                public void onResponse(@NonNull Call<AddBillingPaymentModel> call, @NonNull Response<AddBillingPaymentModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            Common.setCustomToast(mActivity, mMessage);
                            removeAllFragments();
                            redirectToReceptionistPaymentFragment();
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddBillingPaymentModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Create Json Array Item List
     * @return
     */
    private JSONArray getArrPayItem() {
       JSONArray jsonArray=new JSONArray();
       try{
         if (AddPaymentFragment.mArrayBillingItem.size()>0){
            for (int j=0;j<AddPaymentFragment.mArrayBillingItem.size();j++){

                JSONObject jsonObject=new JSONObject();
                jsonObject.put(WebFields.ADD_PAYMENT_LIST.REQUEST_ITEM_Type,
                        AddPaymentFragment.mArrayBillingItem.get(j).getType());

                jsonObject.put(WebFields.ADD_PAYMENT_LIST.REQUEST_ITEM_ID,
                        AddPaymentFragment.mArrayBillingItem.get(j).getId());

                jsonObject.put(WebFields.ADD_PAYMENT_LIST.REQUEST_ITEM_PARENT_ID,
                        AddPaymentFragment.mArrayBillingItem.get(j).getParentId());

                jsonObject.put(WebFields.ADD_PAYMENT_LIST.REQUEST_ITEM_TITLE,
                        AddPaymentFragment.mArrayBillingItem.get(j).getName());

                jsonObject.put(WebFields.ADD_PAYMENT_LIST.REQUEST_ITEM_AMOUNT,
                        AddPaymentFragment.mArrayBillingItem.get(j).getRate());

                jsonObject.put(WebFields.ADD_PAYMENT_LIST.REQUEST_ITEM_ORIGINAL_AMOUNT,
                        AddPaymentFragment.mArrayBillingItem.get(j).getOriginal_Price());

                jsonArray.put(jsonObject);
            }
         }
       }catch (Exception e){
           Common.insertLog(e.getMessage());
       }
       return jsonArray;
    }

    /**
     * Check Validation on Payment
     * @return status
     */
    private boolean checkValidationPayNow() {
        boolean status=true;
         try{
            if (mEditReceivedAmount.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING))
            {
                status=false;
                mEditReceivedAmount.setError(mActivity.getResources().getString(R.string.error_field_required));
            }

            if (strPaymetType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_cheque))){
                if (mEditChequeNo.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING))
                {
                    status=false;
                    mEditChequeNo.setError(mActivity.getResources().getString(R.string.error_field_required));
                }

                if(status){
                    if (mEditChequeNo.getText().length()<6){
                        status=false;
                        mEditChequeNo.setError(mActivity.getResources().getString(R.string.error_wrong_enter_cheque_number));
                    }
                }
            }

             if (strPaymetType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_online_payment))){

                 if (mEditIFSC.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING))
                 {
                     status=false;
                     mEditIFSC.setError(mActivity.getResources().getString(R.string.error_field_required));
                 }
                 if (mEditUTR.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING))
                 {
                     status=false;
                     mEditUTR.setError(mActivity.getResources().getString(R.string.error_field_required));
                 }

                 if(status){
                     if (mEditIFSC.getText().length()<11){
                         status=false;
                         mEditIFSC.setError(mActivity.getResources().getString(R.string.error_wrong_ifsc_number));
                     }

                     if (mEditUTR.getText().length()<12){
                         status=false;
                         mEditUTR.setError(mActivity.getResources().getString(R.string.error_wrong_utr_number));
                     }
                 }
             }


             if (strPaymetType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_cheque))
                 || strPaymetType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_online_payment)))
             {
                 if (mEditBankName.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING))
                 {
                     status=false;
                     mEditBankName.setError(mActivity.getResources().getString(R.string.error_field_required));
                 }
                 if (mEditBranchName.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING))
                 {
                     status=false;
                     mEditBranchName.setError(mActivity.getResources().getString(R.string.error_field_required));
                 }
                 if (mEditAccNo.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING))
                 {
                     status=false;
                     mEditAccNo.setError(mActivity.getResources().getString(R.string.error_field_required));
                 }

                 if (status){
                     if (mEditAccNo.getText().length()<11){
                         status=false;
                         mEditAccNo.setError(mActivity.getResources().getString(R.string.error_wrong_account_no));
                     }
                 }

             }
         }catch (Exception e){
             Common.insertLog(e.getMessage());
         }
        return status;
    }


    /**
     * Used to remove all fragment in the fragment manager after logout
     */
    private void removeAllFragments() {
        try {
            List<Fragment> fragments = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            if (fragments.size() > 0) {
                for (int i = 0; i < fragments.size(); i++) {
                    if (fragments.get(i) != null) {
                        ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                .commit();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method redirected you to the Receptionist Payment Fragment
     */
    private void redirectToReceptionistPaymentFragment() {
        try {
            ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.nav_menu_payment));
            Fragment fragment = new ReceptionistPaymentFragment();
            FragmentManager fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.nav_menu_payment))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    // User Visible on Screen
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isStarted && isVisible) {
           setAdapterBilling();
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
            setAdapterBilling();
        }
    }
}
