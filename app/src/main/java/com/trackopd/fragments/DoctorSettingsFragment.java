package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.trackopd.DoctorHomeActivity;
import com.trackopd.R;
import com.trackopd.utils.AppConstants;

public class DoctorSettingsFragment extends Fragment {

    private View mView, mViewChangePassword;
    private Activity mActivity;
    private RelativeLayout mRelativeAboutUS, mRelativeTermsAndConditions,
            mRelativePrivacyPolicy, mRelativeChangePassword, mRelativeChangeTheme;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_settings, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        setHasOptionsMenu(true);

        getIds();
        setRegListeners();
        setData();
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layouts
            mRelativeAboutUS = mView.findViewById(R.id.relative_settings_about_us);
            mRelativeTermsAndConditions = mView.findViewById(R.id.relative_settings_terms_and_conditions);
            mRelativePrivacyPolicy = mView.findViewById(R.id.relative_settings_privacy_policy);
            mRelativeChangePassword = mView.findViewById(R.id.relative_settings_change_password);
            mRelativeChangeTheme = mView.findViewById(R.id.relative_settings_change_theme);

            // Views
            mViewChangePassword = mView.findViewById(R.id.view_settings_change_password);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mRelativeAboutUS.setOnClickListener(clickListener);
            mRelativeTermsAndConditions.setOnClickListener(clickListener);
            mRelativePrivacyPolicy.setOnClickListener(clickListener);
            mRelativeChangePassword.setOnClickListener(clickListener);
            mRelativeChangeTheme.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            mRelativeChangePassword.setVisibility(View.VISIBLE);
            mViewChangePassword.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.relative_settings_about_us:
                    callAboutUsFragment();
                    break;

                case R.id.relative_settings_terms_and_conditions:
                    callTermsAndConditionsFragment();
                    break;

                case R.id.relative_settings_privacy_policy:
                    callPrivacyPolicyFragment();
                    break;

                case R.id.relative_settings_change_password:
                    callChangePasswordFragment();
                    break;

                case R.id.relative_settings_change_theme:
                    callChangeThemeFragment();
                    break;
            }
        }
    };

    /**
     * This method redirects you to the about us fragment
     */
    private void callAboutUsFragment() {
        ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R
                .string.header_about_us));
        ((DoctorHomeActivity) mActivity).setVisibilityHeaderIcon(View.GONE);
        Fragment fragment = new AboutUsFragment();
        FragmentManager fragmentManager =
                ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                fragment, mActivity.getResources()
                        .getString(R.string.tag_settings_about_us))
                .addToBackStack(mActivity.getResources()
                        .getString(R.string.back_stack_settings_about_us))
                .commit();
    }

    /**
     * This method redirects you to the terms and conditions fragment
     */
    private void callTermsAndConditionsFragment() {
        ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R
                .string.header_terms_and_conditions));
        ((DoctorHomeActivity) mActivity).setVisibilityHeaderIcon(View.GONE);
        Fragment fragment = new TermsAndConditionsFragment();
        FragmentManager fragmentManager =
                ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                fragment, mActivity.getResources()
                        .getString(R.string.tag_settings_terms_and_conditions))
                .addToBackStack(mActivity.getResources()
                        .getString(R.string.back_stack_settings_terms_and_conditions))
                .commit();
    }

    /**
     * This method redirects you to the privacy policy fragment
     */
    private void callPrivacyPolicyFragment() {
        ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R
                .string.header_privacy_policy));
        ((DoctorHomeActivity) mActivity).setVisibilityHeaderIcon(View.GONE);
        Fragment fragment = new PrivacyPolicyFragment();
        FragmentManager fragmentManager =
                ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                fragment, mActivity.getResources()
                        .getString(R.string.tag_settings_privacy_policy))
                .addToBackStack(mActivity.getResources()
                        .getString(R.string.back_stack_settings_privacy_policy))
                .commit();
    }

    /**
     * This method redirects you to the change password fragment
     */
    private void callChangePasswordFragment() {
        ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R
                .string.header_change_password));
        ((DoctorHomeActivity) mActivity).setVisibilityHeaderIcon(View.GONE);
        Fragment fragment = new ChangePasswordFragment();
        FragmentManager fragmentManager =
                ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                fragment, mActivity.getResources()
                        .getString(R.string.tag_settings_change_password))
                .addToBackStack(mActivity.getResources()
                        .getString(R.string.back_stack_settings_change_password))
                .commit();
    }

    /**
     * This method redirects you to the change theme fragment
     */
    private void callChangeThemeFragment() {
        ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R
                .string.header_change_theme));
        Bundle args = new Bundle();
        ((DoctorHomeActivity) mActivity).setVisibilityHeaderIcon(View.GONE);
        Fragment fragment = new ChangeThemeFragment();
        args.putString(AppConstants.BUNDLE_LOGIN_TYPE, "3");
        fragment.setArguments(args);
        FragmentManager fragmentManager =
                ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                fragment, mActivity.getResources()
                        .getString(R.string.tag_settings_change_theme))
                .addToBackStack(mActivity.getResources()
                        .getString(R.string.back_stack_settings_change_theme))
                .commit();
    }
}