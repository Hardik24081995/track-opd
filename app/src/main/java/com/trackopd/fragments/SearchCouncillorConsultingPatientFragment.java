package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.trackopd.CouncillorHomeActivity;
import com.trackopd.R;
import com.trackopd.utils.AppConstants;

public class SearchCouncillorConsultingPatientFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private EditText mEditPatientName, mEditPatientCode, mEditMobileNo;
    private Button mButtonSearch;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_search_councillor_consulting_patient, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        getIds();
        setRegListeners();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Set the prepare menu options
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Edit Texts
            mEditPatientName = mView.findViewById(R.id.edit_search_councillor_consulting_patient_name);
            mEditPatientCode = mView.findViewById(R.id.edit_search_councillor_consulting_patient_code);
            mEditMobileNo = mView.findViewById(R.id.edit_search_councillor_consulting_patient_mobile_no);

            // Buttons
            mButtonSearch = mView.findViewById(R.id.button_search_councillor_consulting_patient_search);

            // Set Request Focus
            mEditPatientName.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mButtonSearch.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_search_councillor_consulting_patient_search:
                    callToCouncillorConsultingPatientFragment();
                    break;
            }
        }
    };

    /**
     * Sets up the Councillor Consulting Patient Fragment
     */
    private void callToCouncillorConsultingPatientFragment() {
        try {
            String mName = mEditPatientName.getText().toString().trim();
            String mPatientCode = mEditPatientCode.getText().toString().trim();
            String mMobileNo = mEditMobileNo.getText().toString().trim();

            ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_consulting_patient));
            Fragment fragment = new CouncillorConsultingPatientFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_NAME, mName);
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, mPatientCode);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mMobileNo);
            fragment.setArguments(args);
            FragmentManager fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    mActivity.getResources().getString(R.string.nav_menu_consulting_patient)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}