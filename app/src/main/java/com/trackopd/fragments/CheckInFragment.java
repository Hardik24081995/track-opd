package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.github.badoualy.datepicker.DatePickerTimeline;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.CheckInAdapter;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.CheckInListModel;
import com.trackopd.model.CheckInListModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.utils.RecyclerProgressView;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckInFragment extends Fragment {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private SwipeRefreshLayout mSwipeRefreshView;
    private RecyclerView mRecyclerView;
    private AppUtil mAppUtils;
    private RelativeLayout mRelativeNoData, mRelativeNoInternet;
    private Button mButtonRetry;
    private boolean isFirstTime = true;
    private ArrayList<CheckInListModel.Data> mArrCheckIn;
    private CheckInAdapter mAdapterCheckIn;
    private String mTitle,mCheckInDate= "0000-00-00",Name,Phone,mUserType;
    private DatePickerTimeline mDatePickerTimeLine;
    private Date mSelectedDate,mCurrentDate;


    // Load More Listener Variables
    private int currentPageIndex = 1, lastFetchRecord = 0, totalRecords = 0;
    private boolean isRefresh = false, isLoadMore = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_check_in, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mAppUtils = new AppUtil(getActivity());
        mArrCheckIn = new ArrayList<>();
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        getBundle();
        getIds();
        setRegListeners();
        getCurrentDate();
       // callShimmerView();
        setHasOptionsMenu(true);
        return mView;
    }

    private void getBundle() {
        Bundle bundle=this.getArguments();
        if (bundle!=null){
            Name = bundle.getString(AppConstants.BUNDLE_PATIENT_NAME);
            //mPatientCode = bundle.getString(AppConstants.BUNDLE_PATIENT_CODE);
            Phone = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
               /* mMRDNo = bundle.getString(AppConstants.BUNDLE_MRD_NO);
                mAppointmentID = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mSurgerytDate = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DATE);*/
            //mDoctorId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);
            // mStatus = bundle.getString(AppConstants.BUNDLE_APPOINTMENT_STATUS);
            //Common.insertLog("Status:::> " + mSurgerytDate);
        }
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        final MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(true);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Date Picker Time Line
            mDatePickerTimeLine = mView.findViewById(R.id.date_picker_check_in_time_line);

            // 
            // Recycler View
            mRecyclerView = mView.findViewById(R.id.recycler_view);

            // Swipe Refresh View
            mSwipeRefreshView = mView.findViewById(R.id.swipe_refresh_view);

            // Relative Layouts
            mRelativeNoData = mView.findViewById(R.id.relative_no_data_available);
            mRelativeNoInternet = mView.findViewById(R.id.relative_no_internet);

            // Buttons
            mButtonRetry = mView.findViewById(R.id.button_retry);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Click Listeners
            mButtonRetry.setOnClickListener(clickListener);

            // ToDo: Set On Refresh Listeners
            mSwipeRefreshView.setOnRefreshListener(refreshListener);

            // ToDo: Date Selected Listener
            mDatePickerTimeLine.setOnDateSelectedListener(dateSelectedListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_search:
                callToSearchAppointmentFragment();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Sets up the Search Receptionist Appointment Fragment
     */
    private void callToSearchAppointmentFragment() {
        try {
            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_patient))) {
                ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            }

            Fragment fragment = new SearchReceptionistCheckUpFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_IS_FROM_SEARCH,mActivity.getString(R.string.nav_menu_check_up));
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_patient))) {
                fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_search_receptionist_checkup))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_search_receptionist_checkup))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    /**
     * Date Selected Click listener
     */
    private DatePickerTimeline.OnDateSelectedListener dateSelectedListener = new DatePickerTimeline.OnDateSelectedListener() {
        @Override
        public void onDateSelected(int year, int month, int day, int index) {
            String mDate = year + "-" + (month + 1) + "-" + day;
            try {
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat df = new SimpleDateFormat(
                        mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));

                mSelectedDate = df.parse(mDate);
                DateFormat dateFormat = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
                mCheckInDate = dateFormat.format(mSelectedDate);
                //callCheckInListAPI();
                if (mArrCheckIn != null)
                    mArrCheckIn.clear();
                if (mAdapterCheckIn != null)
                    mAdapterCheckIn.notifyDataSetChanged();
                currentPageIndex = 1;
                callShimmerView();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_retry:
                    callShimmerView();
                    break;
            }
        }
    };

    /**
     * Set On Refresh Listeners
     */
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            isFirstTime = true;

            if (!mAppUtils.getConnectionState()) {
                mRelativeNoInternet.setVisibility(View.VISIBLE);
                mSwipeRefreshView.setRefreshing(false);
                mArrCheckIn.clear();
                setViewVisibility();
            } else {
                mRelativeNoInternet.setVisibility(View.GONE);
                if (!isLoadMore) {
                    isRefresh = true;

                    mTitle = AppConstants.STR_EMPTY_STRING;

                    if (mArrCheckIn != null)
                        mArrCheckIn.clear();
                    if (mAdapterCheckIn != null)
                        mAdapterCheckIn.notifyDataSetChanged();
                    currentPageIndex = 1;
                    Name=AppConstants.STR_EMPTY_STRING;
                    Phone=AppConstants.STR_EMPTY_STRING;

                    callShimmerView();
                } else
                    mSwipeRefreshView.setRefreshing(false);
            }
        }
    };

    /**
     * Set Current date in date picker and get Current date
     */
    private void getCurrentDate() {
        Calendar CurrentCalender = Calendar.getInstance();
        if (mCheckInDate.equalsIgnoreCase(mActivity.getResources().getString(R.string.date_format_first_time))) {

            mDatePickerTimeLine.setFirstVisibleDate(CurrentCalender.get(Calendar.YEAR), 0, 1);
            mDatePickerTimeLine.setSelectedDate(CurrentCalender.get(Calendar.YEAR),
                    CurrentCalender.get(Calendar.MONTH),
                    CurrentCalender.get(Calendar.DAY_OF_MONTH));

            try {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormatter = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
                String strCurrentDate = dateFormatter.format(CurrentCalender.getTime());
                mSelectedDate = dateFormatter.parse(strCurrentDate);
                mCurrentDate = dateFormatter.parse(strCurrentDate);
                @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
                mCheckInDate = dateFormat.format(mSelectedDate);
            } catch (Exception e) {
                e.printStackTrace();
            }

            /*Common.insertLog("Else 1");
            String[] mValue = mCheckInDate.split("-");
            int mYear = Integer.parseInt(mValue[0]);
            int mMonth = Integer.parseInt(mValue[1]);
            int mDay = Integer.parseInt(mValue[2]);
            mDatePickerTimeLine.setFirstVisibleDate(CurrentCalender.get(Calendar.YEAR), 0, 1);
            mDatePickerTimeLine.setSelectedDate(mYear, mMonth-1, mDay);

            try {
                String mDate = mYear + "-" + mMonth + "-" + mDay;
                @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
                mSelectedDate = df.parse(mDate);
                @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
                mCheckInDate = dateFormat.format(mSelectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }*/

        }
    }

    /**
     * This method is used to call the shimmer effect for recycler view before calling the API
     */
    private void callShimmerView() {
        if (!mAppUtils.getConnectionState()) {
            mRelativeNoInternet.setVisibility(View.VISIBLE);
            mArrCheckIn.clear();
            setViewVisibility();
        } else {
            mRelativeNoInternet.setVisibility(View.GONE);
            if (isFirstTime) {
                showProgressView();
                isFirstTime = false;

            }

            mTitle = AppConstants.STR_EMPTY_STRING;
            if (mArrCheckIn != null)
                mArrCheckIn.clear();
            if (mAdapterCheckIn != null)
                mAdapterCheckIn.notifyDataSetChanged();
            currentPageIndex = 1;

            callCheckInListAPI();
        }
    }

    /**
     * This method should call the payment listing for receptionist
     */
    private void callCheckInListAPI() {
        try {

            if (Name==null){
                Name=AppConstants.STR_EMPTY_STRING;
            }
            if (Phone==null){
                Phone=AppConstants.STR_EMPTY_STRING;
            }

            String hospital_database= GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            String type="Check-up";

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setCheckInListJson(currentPageIndex,Name,Phone,mCheckInDate,hospital_database,type));

            Call<CheckInListModel> call = RetrofitClient.createService(ApiInterface.class).getCheckInList(requestBody);
            call.enqueue(new Callback<CheckInListModel>() {
                @Override
                public void onResponse(@NonNull Call<CheckInListModel> call, @NonNull Response<CheckInListModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        stopProgressView();
                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                ArrayList<CheckInListModel.Data> paymentModel = new ArrayList<>(Arrays
                                        .asList(new GsonBuilder().serializeNulls().create()
                                                .fromJson(new JsonParser().parse(jsonObject.getString(WebFields.DATA)).
                                                        getAsJsonArray(), CheckInListModel.Data[].class)));

                                if (mArrCheckIn != null && mArrCheckIn.size() > 0 &&
                                        mAdapterCheckIn != null) {
                                    mArrCheckIn.addAll(paymentModel);
                                    mAdapterCheckIn.notifyDataSetChanged();
                                    lastFetchRecord = mArrCheckIn.size();
                                } else {
                                    mArrCheckIn = paymentModel;
                                    setAdapterData();
                                    lastFetchRecord = mArrCheckIn.size();
                                    totalRecords = jsonObject.getInt(WebFields.ROW_COUNT);
                                    //setLoadMoreClickListener();

                                    if (mRecyclerView.getVisibility() == View.GONE) {
                                        stopProgressView();
                                    }
                                }
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                            stopProgressView();
                            setViewVisibility();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CheckInListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());
                    stopProgressView();
                    setViewVisibility();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mGridLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(mGridLayoutManager);

            stopProgressView();
            mAdapterCheckIn = new CheckInAdapter(mActivity, mRecyclerView, mArrCheckIn);
            mRecyclerView.setAdapter(mAdapterCheckIn);

            setViewVisibility();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the view visibility as per the array size
     */
    private void setViewVisibility() {
        if (mArrCheckIn.size() != 0) {
            mRelativeNoData.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mRelativeNoData.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    /**
     * Show progress view when page load for the first time
     */
    private void showProgressView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.VISIBLE);

                RecyclerProgressView.startProgress((LinearLayout) mView.findViewById(R.id.linear_add_recycler_progress_view),
                        R.layout.row_councillor_package_item, getActivity());
            }
        }, 100);
    }

    /**
     * Show progress view when page load for the first time
     */
    private void stopProgressView() {
        try {
            mSwipeRefreshView.setRefreshing(false);
            isRefresh = false;
            isLoadMore = false;
            RecyclerProgressView.stopProgress();
            mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            hideProgressBar();
        }
    }

    /**
     * Hide the progress bar
     */
    private void hideProgressBar() {
        try {
            if (mArrCheckIn != null && mArrCheckIn.size() > 0 &&
                    mArrCheckIn.get(mArrCheckIn.size() - 1) == null) {

                mArrCheckIn.remove(mArrCheckIn.size() - 1);
                mAdapterCheckIn.notifyItemRemoved(mArrCheckIn.size());

                mAdapterCheckIn.notifyDataSetChanged();
                mAdapterCheckIn.setLoaded();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
