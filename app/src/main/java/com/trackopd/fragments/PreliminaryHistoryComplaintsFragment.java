package com.trackopd.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PreliminaryHistoryComplaintsFragment extends Fragment{

    private View mView;
    private Activity mActivity;
    private ImageView mImagePrimaryComplainsAdd,mImageSystemicHistoryAdd,mImageOculartHistoryAdd,
                      mImageFamilyHistoryAdd;
    private String mUserType,mOcularHistory;
    
    private LinearLayout mLinearPrimaryComplainsContainer,mLinearSystemicHistoryContainer,
            mLinearOculartHistoryContainer, mLinearFamilyHistoryContainer;
    private ExpandableLayout mExpandablePrimaryComplains,mExpandableSystemicHistory,
            mExpandableOcularHistory,mExpandableFamilyHistory;
    private LinearLayout mLinearContainer;

    //ArrayList
    private ArrayList<String> mArrComplains;

    public PreliminaryHistoryComplaintsFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView=inflater.inflate(R.layout.fragment_preliminry_history_complaints, container, false);

        mActivity=getActivity();
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        mArrComplains=new ArrayList<>();
        getIds();
        setRegListeners();
        return mView;
    }

    /**
     * Id declarations
     */
    private void getIds() {
        //Image View
        mImagePrimaryComplainsAdd=mView.findViewById(R.id.image_prelimanry_history_complaints_primary_complaints);
        mImageSystemicHistoryAdd=mView.findViewById(R.id.image_layout_prelimanry_history_complaints_systemic_history);
        mImageOculartHistoryAdd=mView.findViewById(R.id.image_prelimanry_history_complaints_ocular_history);
        mImageFamilyHistoryAdd=mView.findViewById(R.id.image_prelimanry_history_complaints_family_history);

        //Linear Layout
        mLinearPrimaryComplainsContainer=mView.findViewById(R.id.linear_layout_prelimanry_history_complaints_primary_complaints_container);
        mLinearSystemicHistoryContainer=mView.findViewById(R.id.linear_layout_prelimanry_history_complaints_family_history_container);
        mLinearOculartHistoryContainer=mView.findViewById(R.id.linear_layout_prelimanry_history_ocular_history_container);
        mLinearFamilyHistoryContainer=mView.findViewById(R.id.linear_prelimanry_history_complaints_family_history_container);

        //Expandable Layout
        mExpandablePrimaryComplains=mView.findViewById(R.id.expandable_layout_prelimanry_history_complaints_primary_complaints);
        mExpandableSystemicHistory=mView.findViewById(R.id.expandable_layout_prelimanry_history_complaints_systemic_history);
        mExpandableOcularHistory=mView.findViewById(R.id.expandable_layout_prelimanry_history_complaints_ocular_history);
        mExpandableFamilyHistory=mView.findViewById(R.id.expandable_layout_prelimanry_history_complaints_family_history);
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
      try{
          mImagePrimaryComplainsAdd.setOnClickListener(clickListener);
          mImageSystemicHistoryAdd.setOnClickListener(clickListener);
          mImageOculartHistoryAdd.setOnClickListener(clickListener);
          mImageFamilyHistoryAdd.setOnClickListener(clickListener);
      }catch (Exception e){
          Common.insertLog(e.getMessage());
      }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
           switch (v.getId()){
               case R.id.image_prelimanry_history_complaints_primary_complaints:
                     openDialogPreliminaryComplains("Primary Complains");
                   break;
               case R.id.image_layout_prelimanry_history_complaints_systemic_history:
                     openDialogPreliminaryComplains("Systemic History");
                   break;
               case R.id.image_prelimanry_history_complaints_ocular_history:
                     openDialogPreliminaryComplains("Ocular History");
                   break;
               case R.id.image_prelimanry_history_complaints_family_history:
                     openDialogPreliminaryComplains("Family History");
                   break;
           }
        }
    };

    /**
     * Open Preliminary Complaints
     * @param type Type
     */
    private void openDialogPreliminaryComplains(String type) {
       
       try {
            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialoag_prelimanry_examimation_add_history);

            TextView mTextTitle=dialog.findViewById(R.id.text_dialog_preliminry_examination_add_history_title);
            mLinearContainer=dialog.findViewById(R.id.linear_layout_dialog_preliminry_examination_add_history);
            Button mButtonSubmit=dialog.findViewById(R.id.button_dialog_add_preliminary_examination_submit);
            ImageView mImageAdd=dialog.findViewById(R.id.image_dialog_preliminry_examination_add_history_plus);

            //set Text
            mTextTitle.setText(type);

            mImageAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addLayout(dialog,AppConstants.STR_EMPTY_STRING);
                }
            });

            mButtonSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callBindArrayData();
                    callToAddLayoutComplains(type);
                    dialog.dismiss();
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Add Binary Data
     */
    private void callBindArrayData() {
        if (mLinearContainer!=null && mLinearContainer.getChildCount()>0){
           for (int a=0; a<mLinearContainer.getChildCount();a++){
               View view=mLinearContainer.getChildAt(a);
               AutoCompleteTextView TextView=view.findViewById(R.id.auto_complete_editText_row_add_preliminary_complains);
               String data=TextView.getText().toString();
               mArrComplains.add(data);
           }
        }
    }


    /**
     *  Add Conatiner Layout
     * @param dialog -View
     * @param strEmptyString -String
     */
    private void addLayout(Dialog dialog, String strEmptyString)
    {
        View childView=LayoutInflater.from(mActivity)
                .inflate(R.layout.row_add_preliminary_complains_item,null,false);

        AutoCompleteTextView autoCompleteTextView=childView.findViewById(R.id.auto_complete_editText_row_add_preliminary_complains);

        ImageView imageRemove=childView.findViewById(R.id.image_row_add_preliminary_complains_remove);
        //Set On Click Listener
        imageRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLinearContainer.removeView(childView);
            }
        });

        mLinearContainer.addView(childView);
    }

    /**
     *
     * @param type - Type Complaints To Add
     */
    private void callToAddLayoutComplains(String type) {
        for(String s:mArrComplains){
           View childView=LayoutInflater.from(mActivity).inflate(R.layout.row_add_preliminary_examination_compliance_item,null,false);
           TextView textView=childView.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
           ImageView mImageRemove=childView.findViewById(R.id.image_row_add_preliminary_examination_compliance_delete);
           textView.setText(s);

           mLinearPrimaryComplainsContainer.addView(childView);
           mExpandablePrimaryComplains.expand();
        }
    }
}
