package com.trackopd.fragments;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.model.CouncillorModel;
import com.trackopd.model.DoctorModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchReceptionistFollowUpFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private EditText mEditPatientName, mEditPatientCode, mEditMobileNo, mEditDate;
    private Spinner mSpinnerDoctor, mSpinnerCouncillor;
    private Button mButtonSearch;
    private ImageView mImageDoctor, mImageCouncillor;
    private ArrayList<String> mArrDoctor;
    private ArrayList<DoctorModel> mArrDoctorList;
    private ArrayList<String> mArrCouncillor;
    private ArrayList<CouncillorModel> mArrCouncillorList;
    private int mSelectedDoctorIndex, mSelectedCouncillorIndex, mDoctorId, mCouncillorId;
    private String mAddFollowUp, mUserType;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_search_receptionist_follow_up, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrDoctor = new ArrayList<>();
        mArrDoctorList = new ArrayList<>();
        mArrCouncillor = new ArrayList<>();
        mArrCouncillorList = new ArrayList<>();

        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        getBundle();
        getIds();
        setRegListeners();
        setData();
        callToDoctorAPI();
        callToCouncillorAPI();
        setHasOptionsMenu(true);

        mEditPatientCode.setFilters(Common.getFilter());
        mEditPatientName.setFilters(Common.getFilter());

        return mView;
    }

    /**
     * Set the prepare menu options
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mAddFollowUp = bundle.getString(AppConstants.BUNDLE_ADD_FOLLOW_UP);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Edit Texts
            mEditPatientName = mView.findViewById(R.id.edit_search_receptionist_follow_up_patient_name);
            mEditPatientCode = mView.findViewById(R.id.edit_search_receptionist_follow_up_patient_code);
            mEditMobileNo = mView.findViewById(R.id.edit_search_receptionist_follow_up_patient_mobile);
            mEditDate = mView.findViewById(R.id.edit_search_receptionist_follow_up_follow_date);

            // Spinners
            mSpinnerDoctor = mView.findViewById(R.id.spinner_search_receptionist_follow_up_doctor);
            mSpinnerCouncillor = mView.findViewById(R.id.spinner_search_receptionist_follow_up_councillor);

            // Buttons
            mButtonSearch = mView.findViewById(R.id.button_search_receptionist_follow_up_search);

            // Image Views
            mImageDoctor = mView.findViewById(R.id.image_search_receptionist_follow_up_doctor);
            mImageCouncillor = mView.findViewById(R.id.image_search_receptionist_follow_up_councillor);

            // Set Request Focus
            mEditPatientName.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            //ToDo: Click Listeners
            mButtonSearch.setOnClickListener(clickListener);
            mEditDate.setOnClickListener(clickListener);

            // ToDo: Doctor - Spinner Item Select Listener
            mSpinnerDoctor.setOnItemSelectedListener(onItemSelectedListener);
            mSpinnerCouncillor.setOnItemSelectedListener(onItemSelectedListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            // Set Image Color as per the theme applied
            mImageDoctor.setColorFilter(Common.setThemeColor(mActivity));
            mImageCouncillor.setColorFilter(Common.setThemeColor(mActivity));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.button_search_receptionist_follow_up_search:
                    callToReceptionistFollowUpFragment();
                    break;

                case R.id.edit_search_receptionist_follow_up_follow_date:
                    Common.openDatePickerForAllDates(mActivity, mEditDate);
                    break;
            }
        }
    };

    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {
                case R.id.spinner_search_receptionist_follow_up_doctor:
                    mSelectedDoctorIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedDoctorIndex != 0) {
                        mDoctorId = (Integer.parseInt(mArrDoctorList.get(mSelectedDoctorIndex - 1).getDoctorID()));
                        String mDoctorName = (mArrDoctorList.get(mSelectedDoctorIndex - 1).getDoctorName());
                        Common.insertLog("mDoctorID::> " + mDoctorId);
                        Common.insertLog("mDoctorName::> " + mDoctorName);
                    }
                    break;

                case R.id.spinner_search_receptionist_follow_up_councillor:
                    mSelectedCouncillorIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedCouncillorIndex != 0) {
                        mCouncillorId = (Integer.parseInt(mArrCouncillorList.get(mSelectedCouncillorIndex - 1).getCouncillorID()));
                        String mCouncillorName = (mArrCouncillorList.get(mSelectedCouncillorIndex - 1).getCouncillorName());
                        Common.insertLog("mCouncillorId::> " + mCouncillorId);
                        Common.insertLog("mCouncillorName::> " + mCouncillorName);
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    /**
     * This method should call the Doctor API
     */
    private void callToDoctorAPI() {

        String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

        RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                APICommonMethods.setDoctorListJson(hospital_database));

        Call<DoctorModel> call = RetrofitClient.createService(ApiInterface.class).getDoctorList(body);
        call.enqueue(new Callback<DoctorModel>() {
            @Override
            public void onResponse(@NonNull Call<DoctorModel> call, @NonNull Response<DoctorModel> response) {
                Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                try {
                    String mJson = (new Gson().toJson(response.body()));
                    JSONObject jsonObject = new JSONObject(mJson);
                    String mMessage = jsonObject.getString(WebFields.MESSAGE);
                    Common.insertLog("mMessage:::> " + mMessage);

                    if (response.isSuccessful()) {
                        assert response.body() != null;
                        if (response.body().getData() != null) {
                            mArrDoctorList.addAll(response.body().getData());
                        }
                        setsDoctorAdapter();
                    } else {
                        setsDoctorAdapter();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<DoctorModel> call, @NonNull Throwable t) {
                Common.insertLog("Failure:::> " + t.getMessage());
                setsDoctorAdapter();
            }
        });
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsDoctorAdapter() {

        mArrDoctor.add(0, mActivity.getResources().getString(R.string.spinner_select_doctor));
        if (mArrDoctorList.size() > 0) {
            for (DoctorModel doctorModel : mArrDoctorList) {
                mArrDoctor.add(doctorModel.getDoctorName());
            }
        }
        mSpinnerDoctor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

        ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrDoctor) {
            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                // Cast the drop down items (popup items) as text view
                TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                // Set the text color of drop down items
                tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                // If this item is selected item
                if (position == mSelectedDoctorIndex) {
                    // Set spinner selected popup item's text color
                    tv.setTextColor(Common.setThemeColor(mActivity));
                }
                // Return the modified view
                return tv;
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerDoctor.setAdapter(adapter);
    }

    /**
     * Sets up the data to the councillor spinner
     */
    private void callToCouncillorAPI() {
        try {

            String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setCouncillorListJson(hospital_database));

            Call<CouncillorModel> call = RetrofitClient.createService(ApiInterface.class).getCouncillorList(body);
            call.enqueue(new Callback<CouncillorModel>() {
                @Override
                public void onResponse(@NonNull Call<CouncillorModel> call, @NonNull Response<CouncillorModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrCouncillorList.addAll(response.body().getData());
                            }
                            setsCouncillorAdapter();
                        } else {
                            setsCouncillorAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CouncillorModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setsCouncillorAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsCouncillorAdapter() {
        try {
            mArrCouncillor.add(0, getString(R.string.spinner_select_councillor));
            if (mArrCouncillorList.size() > 0) {
                for (CouncillorModel councillorModel : mArrCouncillorList) {
                    mArrCouncillor.add(councillorModel.getCouncillorName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerCouncillor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_dropdown_item, mArrCouncillor) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCouncillorIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerCouncillor.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Receptionist Follow Up Fragment
     */
    private void callToReceptionistFollowUpFragment() {
        try {
            String mFollowUpDate;

            String mName = mEditPatientName.getText().toString().trim();
            String mPatientCode = mEditPatientCode.getText().toString().trim();
            String mMobileNo = mEditMobileNo.getText().toString().trim();

            if (mEditDate.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                mFollowUpDate = mActivity.getResources().getString(R.string.date_format_first_time);
            } else {
                mFollowUpDate = Common.convertDateWithoutTime(mActivity, mEditDate.getText().toString().trim(), mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
            }

            if (mSpinnerDoctor.getSelectedItemPosition() == 0) {
                mDoctorId = -1;
            }

            if (mSpinnerCouncillor.getSelectedItemPosition() == 0) {
                mCouncillorId = -1;
            }

            if (mUserType.equalsIgnoreCase(getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_follow_up));
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_follow_up));
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_follow_up));
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_follow_up));
            }

            Fragment fragment = new ReceptionistFollowUpFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_ADD_FOLLOW_UP, mAddFollowUp);
            args.putString(AppConstants.BUNDLE_PATIENT_NAME, mName);
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, mPatientCode);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mMobileNo);
            args.putString(AppConstants.BUNDLE_PATIENT_FOLLOW_UP_DATE, mFollowUpDate);
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, mDoctorId);
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_COUNCILLOR, mCouncillorId);
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    mActivity.getResources().getString(R.string.nav_menu_follow_up)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
