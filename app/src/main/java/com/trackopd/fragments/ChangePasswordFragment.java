package com.trackopd.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.trackopd.LoginActivity;
import com.trackopd.R;
import com.trackopd.model.ChangePasswordModel;
import com.trackopd.utils.AnimUtils;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.ApplicationConfiguration;
import com.trackopd.utils.Common;
import com.trackopd.utils.KeyboardUtility;
import com.trackopd.utils.SessionManager;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private Button mButtonSubmit;
    private EditText mEditOldPassword, mEditNewPassword, mEditConfirmPassword;
    private String mOldPass, mNewPass, mConfirmPass;
    private SessionManager mSessionManager;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_change_password, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mSessionManager = new SessionManager(getActivity());

        getIds();
        setRegListeners();
        return mView;
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Edit Texts
            mEditOldPassword = mView.findViewById(R.id.edit_change_password_old_pass);
            mEditNewPassword = mView.findViewById(R.id.edit_change_password_new_pass);
            mEditConfirmPassword = mView.findViewById(R.id.edit_change_password_confirm_pass);

            // Buttons
            mButtonSubmit = mView.findViewById(R.id.button_change_password_submit);

            // Set Request Focus
            mEditOldPassword.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mButtonSubmit.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_change_password_submit:
                    doChangePassword();
                    break;
            }
        }
    };

    /**
     * This method checks the function first then call the API
     */
    private void doChangePassword() {
        KeyboardUtility.HideKeyboard(mActivity, mEditOldPassword);
        if (checkValidation()) {
            callChangePasswordAPI();
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return
     */
    private boolean checkValidation() {
        boolean status = true;

        mEditOldPassword.setError(null);
        mEditNewPassword.setError(null);
        mEditConfirmPassword.setError(null);

        mOldPass = mEditOldPassword.getText().toString().trim();
        mNewPass = mEditNewPassword.getText().toString().trim();
        mConfirmPass = mEditConfirmPassword.getText().toString().trim();

        if (TextUtils.isEmpty(mOldPass)) {
            mEditOldPassword.requestFocus();
            mEditOldPassword.setError(mActivity.getResources().getString(R.string.error_field_required));
            status = false;
        }
        if (TextUtils.isEmpty(mNewPass)) {
            mEditNewPassword.requestFocus();
            mEditNewPassword.setError(mActivity.getResources().getString(R.string.error_field_required));
            status = false;
        }
        if (TextUtils.isEmpty(mConfirmPass)) {
            mEditConfirmPassword.requestFocus();
            mEditConfirmPassword.setError(mActivity.getResources().getString(R.string.error_field_required));
            status = false;
        }

        if (status) {
            if (mOldPass.length() != ApplicationConfiguration.MAX_PASSWORD_LENGTH) {
                mEditOldPassword.requestFocus();
                mEditOldPassword.setError(mActivity.getResources().getString(R.string.error_invalid_password));
                status = false;
            }

            if (mNewPass.length() != ApplicationConfiguration.MAX_PASSWORD_LENGTH) {
                mEditNewPassword.requestFocus();
                mEditNewPassword.setError(mActivity.getResources().getString(R.string.error_invalid_password));
                status = false;
            }
            if (status) {
                if (!mNewPass.equals(mConfirmPass)) {
                    mEditConfirmPassword.requestFocus();
                    mEditConfirmPassword.setError(mActivity.getResources().getString(R.string.error_incorrect_password));
                    status = false;
                }
            }
        }

        if (status) {
            if (!mOldPass.equals(mSessionManager.getPreferences(mSessionManager.APP_PASS_CODE, ""))) {
                mEditOldPassword.setError(mActivity.getResources().getString(R.string.error_incorrect_password));
                status = false;
            }
        }
        return status;
    }

    /**
     * This method should call the Change Password API
     */
    private void callChangePasswordAPI() {
        try {
            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setChangePasswordJson(mUserId, mOldPass, mNewPass));

            Call<ChangePasswordModel> call = RetrofitClient.createService(ApiInterface.class).changePasswordAPI(requestBody);
            call.enqueue(new Callback<ChangePasswordModel>() {
                @Override
                public void onResponse(@NonNull Call<ChangePasswordModel> call, @NonNull Response<ChangePasswordModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            redirectedToLogin();
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ChangePasswordModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Redirected to the Login Activity
     */
    private void redirectedToLogin() {
        try {
            SessionManager mSharedPref = new SessionManager(mActivity);
            mSharedPref.clearAllPreferences();
            Intent intent = new Intent(mActivity, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            AnimUtils.activityExitAnim(mActivity);
            mActivity.finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}