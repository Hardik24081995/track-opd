package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.CheckInAdapter;
import com.trackopd.adapter.PackageExpandableListAdapter;
import com.trackopd.adapter.ServicesExpandableListAdapter;
import com.trackopd.model.AddPaymentModel;
import com.trackopd.model.BillingListItemModel;
import com.trackopd.model.ReceptionistAppointmentModel;
import com.trackopd.model.ServicesPackagesModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.utils.KeyboardUtility;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPaymentStepTwoFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private ImageView mImageUserProfilePic,mImageSpinnerPic;
    private TextView mTextName, mTextMobile;
    private EditText mEditAmount, mEditNotes,mEditDateTime,mEditAppointmentDateTime;
    private String mFirstName, mLastName, mPatientMobile, mPatientId, mAppointmentNo,
            mAppointmentDate, mPatientCode, mAppointmentId,mPaymentDate,
            mPaymentHistoryId,mPaymentAmount=AppConstants.STR_EMPTY_STRING,mProfileImage;

    private static Button mButtonSubmit;
    private Boolean isStarted = false, isVisible = false;
    private RadioGroup mRadioGroupAppointment;
    private Spinner mSpinnerAppointment;
    private LinearLayout mLinearAppointment,mLinearAppointmentDate,mLinearServiceOther;
    int currentPageIndex = 1, mSelectedApointmentIndex = 0, AppointmentType;
    private ArrayList<String> mArrAppointmentName;
    private ArrayList<ReceptionistAppointmentModel> mArrAppointment;
    private boolean edit_payment=false;
    private ExpandableListView mExpandableService,mExpandablePackage;
    private RadioButton mRBWithAppointment,mRBWithoutAppointment;
    private AppUtil mAppUtils;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_add_payment_step_two, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrAppointment = new ArrayList<>();
        mArrAppointmentName = new ArrayList<>();

        mAppUtils=new AppUtil(mActivity);

        getBundle();
        getIds();
        callToAppointmentAPI();
        setRegListeners();
        setData();
        setHasOptionsMenu(true);

        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * This Method receive Data from Add Payment Step One Fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                //AddPaymentFragment.receptionistPatientModel = null;
                mFirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mLastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mPatientMobile = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mAppointmentNo = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mProfileImage=bundle.getString(AppConstants.BUNDLE_PATIENT_PROFILE);

                if (bundle.getString(AppConstants.BUNDLE_PAYMENT_EDIT_MODE)!=null){

                    edit_payment=true;
                    mPaymentDate=bundle.getString(AppConstants.BUNDLE_PATIENT_PAYMENT_DATE);
                    mPaymentAmount=bundle.getString(AppConstants.BUNDLE_PATIENT_PAYMENT_AMOUNT);
                    mPaymentHistoryId=bundle.getString(AppConstants.BUNDLE_PATIENT_PAYMENT_HISTORY_ID);
                    mProfileImage=bundle.getString(AppConstants.BUNDLE_PATIENT_PROFILE);
                }
            }

            if (AddPaymentFragment.receptionistPatientModel != null) {
                mFirstName = AddPaymentFragment.receptionistPatientModel.getFirstName();
                mLastName = AddPaymentFragment.receptionistPatientModel.getLastName();
                mPatientMobile = AddPaymentFragment.receptionistPatientModel.getMobileNo();
                mPatientId = AddPaymentFragment.receptionistPatientModel.getUserID();
                mProfileImage=AddPaymentFragment.receptionistPatientModel.getProfileImage();
            }



        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {

            // Radio Group
            mRadioGroupAppointment = mView.findViewById(R.id.radio_group_add_payment_step_two_appointment);
            //mRadioGroupService = mView.findViewById(R.id.radio_group_add_payment_step_two);

            // Radio Button
            mRBWithAppointment = mView.findViewById(R.id.radio_button_add_payment_step_two_with_appointment);
            mRBWithoutAppointment = mView.findViewById(R.id.radio_button_add_payment_step_two_without_appointment);
            //mRadioButtonServices = mView.findViewById(R.id.radio_button_add_payment_step_two_service);
            //mRadioButtonPackage = mView.findViewById(R.id.radio_button_add_payment_step_two_package);

            // Image Views
            mImageUserProfilePic = mView.findViewById(R.id.image_add_payment_step_two_profile_pic);
            mImageSpinnerPic= mView.findViewById(R.id.image_add_payment_step_two_spinner_appointment);

            // Text Views
            mTextName = mView.findViewById(R.id.text_input_add_payment_step_two_name);
            mTextMobile = mView.findViewById(R.id.text_input_add_payment_step_two_mobile_no);

            // Edit Texts
            mEditDateTime = mView.findViewById(R.id.edit_add_payment_step_two_date);
            //mEditAmount = mView.findViewById(R.id.edit_add_payment_step_two_amount);
            mEditNotes = mView.findViewById(R.id.edit_add_payment_step_two_notes);

            // Spinner
            mSpinnerAppointment = mView.findViewById(R.id.spinner_add_payment_step_two_spinner_appointment);

            //Linear Layout
            mLinearAppointment = mView.findViewById(R.id.linear_add_payment_step_two_spinner_appointment);
           // mLinearServiceOther= mView.findViewById(R.id.linear_add_payment_step_two_other_services);

            // Buttons
            mButtonSubmit = mView.findViewById(R.id.button_add_payment_step_two_submit);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    @SuppressLint("SetTextI18n")
    private void setData() {
        try {

            mTextName.setText(mFirstName + AppConstants.STR_EMPTY_SPACE + mLastName);
            mTextMobile.setText(mPatientMobile);

            //Set Payment Amount Value
            if (mPaymentDate!=null && !mPaymentDate.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){

                mEditDateTime.setText(mPaymentDate);
            }else {
                mEditDateTime.setText(Common.setCurrentDateTime(mActivity));
            }

            // Set image to the selected view
            if (mProfileImage!=null && !mProfileImage.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                //Set Thumbnail
                String thumbmail_url= WebFields.API_BASE_URL+WebFields.IMAGE_THUMBNAIL_URL+mProfileImage;
                Glide.with(mActivity)
                        .load(thumbmail_url)
                        .apply(new RequestOptions().error(R.drawable.ic_documents_img).placeholder(R.drawable.ic_documents_img))
                        .into(mImageUserProfilePic);
            }else {
                mImageUserProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, mFirstName,
                      mLastName));
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerAppointment.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mImageSpinnerPic.setColorFilter(Common.setThemeColor(mActivity));

            //if Edit Payment Change Button Text
            if (edit_payment){
                mButtonSubmit.setVisibility(View.VISIBLE);
                mButtonSubmit.setText(mActivity.getResources().getString(R.string.action_update_payment));
                if (!mAppointmentNo.equalsIgnoreCase("0")){
                    mRadioGroupAppointment.check(R.id.radio_button_add_payment_step_two_with_appointment);
                    mSpinnerAppointment.setVisibility(View.VISIBLE);
                    AppointmentType = 1;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Selected Appointment
     */
    private void setSelectedAppointment() {

        if (mArrAppointment.size()>0){
            for (int i=0;i<mArrAppointment.size();i++){
               if (mArrAppointment.get(i).getAppointmentID().equalsIgnoreCase(mAppointmentNo)){

                   mAppointmentId = mArrAppointment.get(i).getAppointmentID();
                   mSpinnerAppointment.setSelection(i+1);

               }
            }
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mEditDateTime.setOnClickListener(clickListener);
            mButtonSubmit.setOnClickListener(clickListener);

            // ToDo: Radio Button Click Listeners
            mRadioGroupAppointment.setOnCheckedChangeListener(checkedChangeListener);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.edit_add_payment_step_two_date:
                    Common.openAllDateTimePicker(mActivity, mEditDateTime);
                    break;

                case R.id.button_add_payment_step_two_submit:
                    doAddPayment();
                    break;
            }
        }
    };


    /**
     * Radio button on checked change listeners
     */
    private RadioGroup.OnCheckedChangeListener checkedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            RadioButton mRadioButton = group.findViewById(checkedId);
            mRadioButton.clearFocus();

            switch (group.getId()) {
                case R.id.radio_group_add_payment_step_two_appointment:

                    int selAppointmentType = group.getCheckedRadioButtonId();
                    if (selAppointmentType == R.id.radio_button_add_payment_step_two_with_appointment) {
                        mLinearAppointment.setVisibility(View.VISIBLE);
                        AppointmentType = 1;

                    } else {
                        mLinearAppointment.setVisibility(View.GONE);
                        AppointmentType = 0;
                    }
                    break;

            }
        }
    };

    /**
     * This method checks the validation first then call the API
     */
    private void doAddPayment() {
        KeyboardUtility.HideKeyboard(mActivity, mEditNotes);
        if (checkValidation()) {
            if (edit_payment){
                //callToUpdatePaymentAPI();
                //setDetailI();
            }else {
                callToAddPaymentAPI();
            }
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {
        boolean status = true;

        int selectAppointment = mSpinnerAppointment.getSelectedItemPosition();
        if (AppointmentType == 1) {
            if (selectAppointment == 0) {
                status = false;
                Common.setCustomToast(mActivity, getString(R.string.error_select_appointment));
            }
        }

        return status;
    }

    /**
     * This method should call the Add Payment API for Receptionist
     */
    private void callToAddPaymentAPI() {

        AddPaymentFragment.mPatientID=mPatientId;
        AddPaymentFragment.mPaymentDate=Common.convertDateTimeToServer(mActivity, mEditDateTime.getText().toString());;
        AddPaymentFragment.mNotes=mEditNotes.getText().toString();
        if (AppointmentType == 1) {
            int selected_index = mSpinnerAppointment.getSelectedItemPosition();
            AddPaymentFragment.mAppointmentNo = mArrAppointment.get(selected_index - 1).getAppointmentID();
        } else {
            AddPaymentFragment.mAppointmentNo = "0";
        }



        /*try {

            String mAmount = "0";//mEditAmount.getText().toString();
            String mDateTime = Common.convertDateTimeToServer(mActivity, mEditDateTime.getText().toString());
            String mStatus = AppConstants.STR_STATUS_SUCCESS;

            if (AppointmentType == 1) {
                int selected_index = mSpinnerAppointment.getSelectedItemPosition();
                mAppointmentId = mArrAppointment.get(selected_index - 1).getAppointmentID();
            } else {
                mAppointmentId = "0";
            }

            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);

            String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setReceptionistAddPaymentJson(mPatientId, mAmount, mDateTime, mStatus, mAppointmentId, mUserId,hospital_database));

            Call<AddPaymentModel> call = RetrofitClient.createService(ApiInterface.class).addPayment(requestBody);
            call.enqueue(new Callback<AddPaymentModel>() {
                @Override
                public void onResponse(@NonNull Call<AddPaymentModel> call, @NonNull Response<AddPaymentModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            Common.setCustomToast(mActivity, mMessage);
                            removeAllFragments();
                            redirectToReceptionistPaymentFragment();
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddPaymentModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }


    /**
     * This method should call the Add Payment API for Receptionist
     */
    private void callToUpdatePaymentAPI() {

        AddPaymentFragment.mPatientID=mPatientId;
        AddPaymentFragment.mPaymentDate=Common.convertDateTimeToServer(mActivity, mEditDateTime.getText().toString());;
        AddPaymentFragment.mNotes=mEditNotes.getText().toString();
        if (AppointmentType == 1) {
            int selected_index = mSpinnerAppointment.getSelectedItemPosition();
            AddPaymentFragment.mAppointmentNo = mArrAppointment.get(selected_index - 1).getAppointmentID();
        } else {
            AddPaymentFragment.mAppointmentNo = "0";
        }

      /*try {

          String mAmount = mEditAmount.getText().toString();
          String mPaymentDateTime = Common.convertDateTimeToServer(mActivity, mEditDateTime.getText().toString());
          String mStatus = AppConstants.STR_STATUS_SUCCESS;
          String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);

          if (AppointmentType == 1) {
              int selected_index = mSpinnerAppointment.getSelectedItemPosition();
              mAppointmentId = mArrAppointment.get(selected_index - 1).getAppointmentID();
          } else {
              mAppointmentId = "0";
          }

          String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

          RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                  APICommonMethods.setReceptionistEditPaymentJson(mAppointmentId,mStatus,
                          mPaymentDateTime,mAmount,mUserId, mPatientId,mPaymentHistoryId,hospital_database));

          Common.insertLog("Edit Payment Request...."+requestBody.toString());

          Call<AddPaymentModel> call = RetrofitClient.createService(ApiInterface.class).addPayment(requestBody);
          call.enqueue(new Callback<AddPaymentModel>() {
              @Override
              public void onResponse(@NonNull Call<AddPaymentModel> call, @NonNull Response<AddPaymentModel> response) {

                  Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                  try {
                      String mJson = (new Gson().toJson(response.body()));
                      JSONObject jsonObject = new JSONObject(mJson);
                      String mMessage = jsonObject.getString(WebFields.MESSAGE);
                      Common.insertLog("mMessage:::> " + mMessage);

                      if (response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                          Common.setCustomToast(mActivity, mMessage);;
                          redirectToReceptionistPaymentFragment();
                      } else {
                          Common.setCustomToast(mActivity, mMessage);
                      }
                  } catch (JSONException e) {
                      e.printStackTrace();
                  }
              }

              @Override
              public void onFailure(@NonNull Call<AddPaymentModel> call, @NonNull Throwable t) {
                  Common.insertLog("Failure:::> " + t.getMessage());
              }
          });

      }catch (Exception e){
          Common.insertLog(e.getMessage());
      }*/
    }

    /**
     * Used to remove all fragment in the fragment manager after logout
     */
    private void removeAllFragments() {
        try {
            List<Fragment> fragments = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            if (fragments.size() > 0) {
                for (int i = 0; i < fragments.size(); i++) {
                    if (fragments.get(i) != null) {
                        ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                .commit();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method redirected you to the Receptionist Payment Fragment
     */
    private void redirectToReceptionistPaymentFragment() {
        try {
            ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.nav_menu_payment));
            Fragment fragment = new ReceptionistPaymentFragment();
            FragmentManager fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.nav_menu_payment))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the appointment listing for receptionist
     */
    private void callToAppointmentAPI() {
        try {

            String mFirstName = AppConstants.STR_EMPTY_STRING;
            if (mPatientCode == null) {
                mPatientCode = AppConstants.STR_EMPTY_STRING;
            }

            if (mAppointmentNo == null) {
                mAppointmentNo = AppConstants.STR_EMPTY_STRING;
            }

            mAppointmentDate = mActivity.getResources().getString(R.string.date_format_first_time);

            int mDoctorId = -1;

            String mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

            String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setReceptionistAppointmentListJson(mActivity, currentPageIndex,
                            mUserType, mFirstName, mPatientCode, mPatientMobile, "", mAppointmentNo,
                            mAppointmentDate, mDoctorId, mActivity.getResources().getString
                            (R.string.tab_all), mActivity.getResources().getString(R.string.tab_all),
                            String.valueOf(-1), hospital_database));

            Call<ReceptionistAppointmentModel> call = RetrofitClient.createService(ApiInterface.class).getReceptionistAppointmentList(requestBody);
            call.enqueue(new Callback<ReceptionistAppointmentModel>() {
                @Override
                public void onResponse(@NonNull Call<ReceptionistAppointmentModel> call, @NonNull Response<ReceptionistAppointmentModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                ArrayList<ReceptionistAppointmentModel> appointmentModel = new ArrayList<>(Arrays
                                        .asList(new GsonBuilder().serializeNulls().create()
                                                .fromJson(new JsonParser().parse(jsonObject.getString(WebFields.DATA)).
                                                        getAsJsonArray(), ReceptionistAppointmentModel[].class)));

                                if (mArrAppointment != null && mArrAppointment.size() > 0) {
                                    mArrAppointment.addAll(appointmentModel);
                                } else {
                                    mArrAppointment = appointmentModel;
                                }
                                setAppointmentSpinner();
                            } else {
                                setAppointmentSpinner();
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                            setAppointmentSpinner();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ReceptionistAppointmentModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setAppointmentSpinner();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * set Appointment Spinner
     */
    private void setAppointmentSpinner() {
        try {
            mArrAppointmentName.add(getString(R.string.spinner_select_appointment));

            if (mArrAppointment.size() > 0) {
                for (int i = 0; i < mArrAppointment.size(); i++) {
                    mArrAppointmentName.add(mArrAppointment.get(i).getTicketNumber());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerAppointment.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrAppointmentName) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedApointmentIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerAppointment.setAdapter(adapter);

            setSelectedAppointment();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Receptionist Past Follow Up Fragment on visible
     *
     * @param isVisibleToUser - This can check the fragment is visible when swiping and will return
     *                        the boolean value based on that
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        isVisible = isVisibleToUser;
        if (isStarted && isVisible) {

            if (mArrAppointmentName.size() > 0) {
                mArrAppointmentName.clear();
                mArrAppointmentName.clear();
            }
            getBundle();
            setData();
            callToAppointmentAPI();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isVisible = false;
        isStarted = false;
    }

    public static void setDetailI() {

        mButtonSubmit.callOnClick();
    }
}