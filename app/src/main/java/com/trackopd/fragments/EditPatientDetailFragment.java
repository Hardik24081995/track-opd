package com.trackopd.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.trackopd.R;
import com.trackopd.model.AddPatientModel;
import com.trackopd.model.AddUploadPicModel;
import com.trackopd.model.AreaModel;
import com.trackopd.model.CityModel;
import com.trackopd.model.CountryModel;
import com.trackopd.model.ReceptionistPatientModel;
import com.trackopd.model.StateModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppPermissions;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.CameraGalleryPermission;
import com.trackopd.utils.Common;
import com.trackopd.utils.Compressor;
import com.trackopd.utils.ImageUtil;
import com.trackopd.utils.KeyboardUtility;
import com.trackopd.utils.StringUtils;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.trackopd.webServices.APICommonMethods.setEditPatientDetailJson;

public class EditPatientDetailFragment extends Fragment {

    private View mView, mViewState;
    private Menu menu;
    private Activity mActivity;

    private EditText mEditFirstName, mEditMiddleName, mEditLastName, mEditMobile, mEditEmail, mEditOccupation, mEditAddress, mEditCity, mEditArea, mEditPinCode, mEditBirthDate;
    private ImageView mImagePatientPicture, mImageState, mImageCity, mImageArea, mImageCountry, mImageRemoveCity, mImageRemoveArea, mImageAddCity, mImageAddArea;
    private Spinner mSpinnerState, mSpinnerCity, mSpinnerArea, mSpinnerCountry, mSpinnerNameTitle;
    private RelativeLayout mRelativeAddNewCity, mRelativeAddNewArea;
    private LinearLayout mLinearUploadPic, mLinearState, mLinearCity, mLinearArea, mLinearCitySpinner, mLinearAreaSpinner;
    private RadioGroup mRadioGender;
    private Button mButtonSubmit;
    private RadioButton mRadioMale, mRadioFemale, mRadioOther;

    private ArrayList<String> mArrCountry, mArrState, mArrCity, mArrArea, mArrPrefix;
    private ArrayList<CountryModel> mArrCountryList;
    private ArrayList<StateModel> mArrStateList;
    private ArrayList<CityModel> mArrCityList;
    private ArrayList<AreaModel> mArrAreaList;
    ReceptionistPatientModel data;
    private int mSelectedCountryIndex, mSelectedStateIndex, mSelectedCityIndex, mSelectedAreaIndex, mCountryId, mStateId, mCityId, mAreaId;
    private String mPatientCode, mName, mMobileNo, mGender, mImagePath, mPatientUserId, mFilePath = "",
            mUserType = "", chosenTask;

    // Load More Listener Variables
    private int currentPageIndex = 1, lastFetchRecord = 0, totalRecords = 0, REQUEST_CAMERA = 101, REQUEST_GALLERY = 102;
    private boolean isRefresh = false, isLoadMore = false;

    private File destination, mPath;
    private Bitmap mBitmap;
    private Uri mImageUri;
    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinner_edit_patient_country:
                    mSelectedCountryIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedCountryIndex != 0) {
                        mLinearState.setVisibility(View.VISIBLE);
                        mViewState.setVisibility(View.VISIBLE);
                        mCountryId = (Integer.parseInt(mArrCountryList.get(mSelectedCountryIndex - 1).getCountryID()));
                        String mCountryName = (mArrCountryList.get(mSelectedCountryIndex - 1).getCountryName());
                        Common.insertLog("mCountryId::> " + mCountryId);
                        Common.insertLog("mCountryName::> " + mCountryName);
                        callToStateAPI(mCountryId);
                    } else {
                        mLinearState.setVisibility(View.GONE);
                        mViewState.setVisibility(View.GONE);
                        //mLinearArea.setVisibility(View.GONE);
                        mLinearCity.setVisibility(View.GONE);
                    }
                    break;

                case R.id.spinner_edit_patient_state:
                    mSelectedStateIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedStateIndex != 0) {
                        mLinearCity.setVisibility(View.VISIBLE);
                        mStateId = (Integer.parseInt(mArrStateList.get(mSelectedStateIndex - 1).getStateID()));
                        String mStateName = (mArrStateList.get(mSelectedStateIndex - 1).getStateName());
                        Common.insertLog("mStateId::> " + mStateId);
                        Common.insertLog("mStateName::> " + mStateName);
                        callToCityAPI(mStateId);
                    } else {
                        mLinearCity.setVisibility(View.GONE);
                        // mLinearArea.setVisibility(View.GONE);
                    }
                    break;

                case R.id.spinner_edit_patient_city:
                    mSelectedCityIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedCityIndex != 0) {
                        mLinearArea.setVisibility(View.VISIBLE);
                        mCityId = (Integer.parseInt(mArrCityList.get(mSelectedCityIndex - 1).getCityID()));
                        String mCityName = (mArrCityList.get(mSelectedCityIndex - 1).getCityName());
                        Common.insertLog("mCityId::> " + mCityId);
                        Common.insertLog("mCityName::> " + mCityName);
                        callToAreaAPI(mCityId);
                    } else {
                        // mLinearArea.setVisibility(View.GONE);
                    }
                    break;

                case R.id.spinner_edit_patient_area:
                    mSelectedAreaIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedAreaIndex != 0) {
                        mAreaId = (Integer.parseInt(mArrAreaList.get(mSelectedAreaIndex - 1).getAreaID()));
                        String mAreaName = (mArrAreaList.get(mSelectedAreaIndex - 1).getAreaName());
                        Common.insertLog("mAreaId::> " + mAreaId);
                        Common.insertLog("mAreaName::> " + mAreaName);
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };
    /**
     * Checked Change listeners (Radio Button)
     */
    private RadioGroup.OnCheckedChangeListener checkedChangeListener = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            RadioButton mRadioButton = group.findViewById(checkedId);
            mRadioButton.clearFocus();

            switch (mRadioButton.getId()) {
                case R.id.radio_group_edit_patient_gender:
                    int selId = group.getCheckedRadioButtonId();
                    if (selId == 1) {
                        mGender = mActivity.getResources().getString(R.string.text_gender_male);
                    } else if (selId == 2) {
                        mGender = mActivity.getResources().getString(R.string.text_gender_female);
                    } else {
                        mGender = mActivity.getResources().getString(R.string.text_gender_others);
                    }
                    break;
            }
        }
    };
    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.linear_edit_patient_upload_picture:
                    openGalleryPopup();
                    break;

                case R.id.image_edit_patient_add_city:
                    addNewCity();
                    break;

                case R.id.image_edit_patient_city_remove:
                    removeNewCity();
                    break;

                case R.id.image_edit_patient_add_new_area:
                    addNewArea();
                    break;

                case R.id.image_edit_patient_area_remove:
                    removeNewArea();
                    break;

                case R.id.button_edit_patient_submit:
                    doEditUpdatePatient();
                    break;

                case R.id.edit_edit_patient_bod:
                    Common.openPastDatePicker(mActivity, mEditBirthDate);
                    break;
                case R.id.image_edit_patient_user_profile_pic_without_text:
                    openGalleryPopup();
                    break;
            }
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_edit_patient_detail, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;

        mActivity = getActivity();
        mArrCountry = new ArrayList<>();
        mArrState = new ArrayList<>();
        mArrCity = new ArrayList<>();
        mArrArea = new ArrayList<>();
        mArrCountryList = new ArrayList<>();
        mArrStateList = new ArrayList<>();
        mArrCityList = new ArrayList<>();
        mArrAreaList = new ArrayList<>();
        mArrPrefix = new ArrayList<>();

        getBundle();
        getIds();
        setData();
        callToCountryAPI();


        setRegListeners();
        setHasOptionsMenu(true);

        mEditFirstName.setFilters(Common.getFilter());
        mEditMiddleName.setFilters(Common.getFilter());
        mEditLastName.setFilters(Common.getFilter());
        mEditMobile.setFilters(Common.getFilter());
        mEditEmail.setFilters(Common.getFilter());
        mEditOccupation.setFilters(Common.getFilter());
        mEditAddress.setFilters(Common.getFilter());
        mEditCity.setFilters(Common.getFilter());
        mEditArea.setFilters(Common.getFilter());
        mEditPinCode.setFilters(Common.getFilter());
        mEditBirthDate.setFilters(Common.getFilter());

        return mView;
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mPatientUserId = bundle.getString(AppConstants.BUNDLE_PATIENT_USER_ID);
                mPatientCode = bundle.getString(AppConstants.BUNDLE_PATIENT_CODE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        final MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(false);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Edit Texts
            mEditFirstName = mView.findViewById(R.id.edit_edit_patient_first_name);
            mEditLastName = mView.findViewById(R.id.edit_edit_patient_last_name);
            mEditMobile = mView.findViewById(R.id.edit_edit_patient_mobile_no);
            mEditEmail = mView.findViewById(R.id.edit_edit_patient_email);
            mEditOccupation = mView.findViewById(R.id.edit_edit_patient_occupation);
            mEditAddress = mView.findViewById(R.id.edit_edit_patient_address);
            mEditCity = mView.findViewById(R.id.edit_edit_patient_add_new_city);
            mEditArea = mView.findViewById(R.id.edit_edit_patient_add_new_area);
            mEditPinCode = mView.findViewById(R.id.edit_edit_patient_pincode);
            mEditBirthDate = mView.findViewById(R.id.edit_edit_patient_bod);
            mEditMiddleName = mView.findViewById(R.id.edit_edit_patient_middle_name);
            // Image Views
            mImageState = mView.findViewById(R.id.image_edit_patient_state);
            mImageCity = mView.findViewById(R.id.image_edit_patient_city);
            mImageArea = mView.findViewById(R.id.image_edit_patient_area);
            mImageCountry = mView.findViewById(R.id.image_edit_patient_country);
            mImageAddCity = mView.findViewById(R.id.image_edit_patient_add_city);
            mImageAddArea = mView.findViewById(R.id.image_edit_patient_add_new_area);
            mImageRemoveCity = mView.findViewById(R.id.image_edit_patient_city_remove);
            mImageRemoveArea = mView.findViewById(R.id.image_edit_patient_area_remove);
            mImagePatientPicture = mView.findViewById(R.id.image_edit_patient_user_profile_pic_without_text);
            // Edit Texts
            mSpinnerCountry = mView.findViewById(R.id.spinner_edit_patient_country);
            mSpinnerState = mView.findViewById(R.id.spinner_edit_patient_state);
            mSpinnerCity = mView.findViewById(R.id.spinner_edit_patient_city);
            mSpinnerArea = mView.findViewById(R.id.spinner_edit_patient_area);
            mSpinnerNameTitle = mView.findViewById(R.id.spinner_edit_patient_name_title);
            // Radio groups
            mRadioGender = mView.findViewById(R.id.radio_group_edit_patient_gender);
            // Radio Buttons
            mRadioMale = mView.findViewById(R.id.radio_button_edit_patient_gender_male);
            mRadioFemale = mView.findViewById(R.id.radio_button_edit_patient_gender_female);
            mRadioOther = mView.findViewById(R.id.radio_button_edit_patient_gender_others);
            // Buttons
            mButtonSubmit = mView.findViewById(R.id.button_edit_patient_submit);
            // Relative Layouts
            mRelativeAddNewCity = mView.findViewById(R.id.relative_edit_patient_add_city);
            mRelativeAddNewArea = mView.findViewById(R.id.relative_edit_patient_add_area);
            // Linear Layouts
            mLinearUploadPic = mView.findViewById(R.id.linear_edit_patient_upload_picture);
            mLinearCity = mView.findViewById(R.id.linear_edit_patient_city);
            mLinearArea = mView.findViewById(R.id.linear_edit_patient_area);
            mLinearState = mView.findViewById(R.id.linear_edit_patient_state);
            mLinearCitySpinner = mView.findViewById(R.id.linear_edit_patient_city_spinner);
            mLinearAreaSpinner = mView.findViewById(R.id.linear_edit_patient_area_spinner);
            // Views
            mViewState = mView.findViewById(R.id.view_edit_patient_state);
            // Set Request Focus
            mEditFirstName.requestFocus();
            mEditBirthDate.setKeyListener(null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            // Set Image Color as per the theme applied
            mImageState.setColorFilter(Common.setThemeColor(mActivity));
            mImageCity.setColorFilter(Common.setThemeColor(mActivity));
            mImageArea.setColorFilter(Common.setThemeColor(mActivity));
            mImageCountry.setColorFilter(Common.setThemeColor(mActivity));

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerState.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerCity.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerArea.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerNameTitle.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

            // Set Data
            mEditBirthDate.setText(Common.setCurrentDate(mActivity));

            //Set Spinner Name Title
            mArrPrefix.addAll(Arrays.asList(mActivity.getResources().getStringArray(R.array.name_title)));

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerNameTitle.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrPrefix);
            mSpinnerNameTitle.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Change Click Listener
     */
    private void setRegListeners() {
        // ToDo: Doctor - Spinner Item Select Listener
        mSpinnerCountry.setOnItemSelectedListener(onItemSelectedListener);
        mSpinnerState.setOnItemSelectedListener(onItemSelectedListener);
        mSpinnerCity.setOnItemSelectedListener(onItemSelectedListener);
        mSpinnerArea.setOnItemSelectedListener(onItemSelectedListener);

        // ToDo: Radio Button Click Listeners
        mRadioGender.setOnCheckedChangeListener(checkedChangeListener);

        // ToDo: Click Listeners
        mLinearUploadPic.setOnClickListener(clickListener);
        mImageAddCity.setOnClickListener(clickListener);
        mImageRemoveCity.setOnClickListener(clickListener);
        mImageAddArea.setOnClickListener(clickListener);
        mImageRemoveArea.setOnClickListener(clickListener);
        mButtonSubmit.setOnClickListener(clickListener);
        mEditBirthDate.setOnClickListener(clickListener);
        mImagePatientPicture.setOnClickListener(clickListener);
    }

    /**
     * This method should call the patient listing for receptionist
     */
    private void callPatientListAPI() {

        try {
            if (mName == null) {
                mName = AppConstants.STR_EMPTY_STRING;
            }

            if (mPatientCode == null) {
                mPatientCode = AppConstants.STR_EMPTY_STRING;
            }

            if (mMobileNo == null) {
                mMobileNo = AppConstants.STR_EMPTY_STRING;
            }
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setReceptionistPatientListJson(currentPageIndex, mName, mPatientCode, mMobileNo, hospital_database));

            Call<ReceptionistPatientModel> call = RetrofitClient.createService(ApiInterface.class).getReceptionistPatientList(requestBody);
            call.enqueue(new Callback<ReceptionistPatientModel>() {
                @Override
                public void onResponse(@NonNull Call<ReceptionistPatientModel> call, @NonNull Response<ReceptionistPatientModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        if (response.isSuccessful() &&
                                response.body().getError() == 200) {

                            if (response.body().getData() != null &&
                                    response.body().getData().size() > 0) {
                                data = response.body().getData().get(0);

                                setPatientDetail(response.body().getData().get(0));
                            } else {
                                Common.setCustomToast(mActivity, response.body().getMessage());
                            }
                        } else {
                            Common.setCustomToast(mActivity, response.body().getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ReceptionistPatientModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Patient Data
     *
     * @param data - Response Data
     */
    private void setPatientDetail(ReceptionistPatientModel data) {
        try {


            mEditFirstName.setText(data.getFirstName());
            mEditLastName.setText(data.getLastName());
            mEditMiddleName.setText(data.getMiddleName());
            mEditMobile.setText(data.getMobileNo());
            mEditEmail.setText(data.getEmailID());
            mEditAddress.setText(data.getAddress());

            //Set Spinner
            for (int p = 0; p < mArrPrefix.size(); p++) {
                if (mArrPrefix.get(p).equalsIgnoreCase(data.getPrefix())) {
                    mSpinnerNameTitle.setSelection(p);
                }
            }

            //Set Spinner Country
            if (!data.getCountryName().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                for (int c = 0; c < mArrCountry.size(); c++) {
                    if (mArrCountry.get(c).equalsIgnoreCase(data.getCountryName())) {
                        mSpinnerCountry.setSelection(c);
                       // callToStateAPI(Integer.parseInt(data.getCountryID()));
                    }
                }
            } else {
                mSpinnerCountry.setSelection(0);
            }




            // set Image View
            if (data.getProfileImage() != null &&
                    !data.getProfileImage().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                //Set Thumbnail
                String thumbmail_url = WebFields.API_BASE_URL + WebFields.IMAGE_THUMBNAIL_URL + data.getProfileImage();
                Glide.with(mActivity)
                        .load(thumbmail_url)
                        .apply(new RequestOptions().error(R.drawable.ic_documents_img).placeholder(R.drawable.ic_documents_img))
                        .into(mImagePatientPicture);
            } else {
                mImagePatientPicture.setImageDrawable(Common.setLabeledImageView(mActivity, data.getFirstName(), data.getLastName()));
            }


            //Set Radio Button Index
            if (data.getGender().equalsIgnoreCase(mActivity.getResources().getString(R.string.text_gender_male))) {
                mRadioGender.check(mRadioMale.getId());
                mGender = data.getGender();
            } else if (data.getGender().equalsIgnoreCase(mActivity.getResources().getString(R.string.text_gender_female))) {
                mRadioGender.check(mRadioFemale.getId());
                mGender = data.getGender();
            } else {
                mRadioGender.check(mRadioOther.getId());
                mGender = data.getGender();
            }

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * This method should call the Country API
     */
    private void callToCountryAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setCountryListJson(hospital_database));

            Call<CountryModel> call = RetrofitClient.createService(ApiInterface.class).getCountryList(body);
            call.enqueue(new Callback<CountryModel>() {
                @Override
                public void onResponse(@NonNull Call<CountryModel> call, @NonNull Response<CountryModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                mArrCountryList.addAll(response.body().getData());
                                setCountryAdapter();
                                callPatientListAPI();
                            }
                        } else {
                            setCountryAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CountryModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setCountryAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    public void setCountryAdapter() {
        try {
            mArrCountry.add(0, getString(R.string.spinner_select_country));
            if (mArrCountryList.size() > 0) {
                for (CountryModel country : mArrCountryList) {
                    mArrCountry.add(country.getCountryName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerCountry.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, mArrCountry) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCountryIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerCountry.setAdapter(adapter);

            // Default Selected India Country
          /*  for (int c = 0; c < mArrCountry.size(); c++) {
                if (mArrCountry.get(c).equalsIgnoreCase("India")) {
                    mSpinnerCountry.setSelection(c);
                }
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the State API
     */
    private void callToStateAPI(int mCountryId) {
        try {
            if (mArrState.size() > 0)
                mArrState.clear();
            if (mArrStateList.size() > 0)
                mArrStateList.clear();

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setStateListJson(String.valueOf(mCountryId), hospital_database));

            Call<StateModel> call = RetrofitClient.createService(ApiInterface.class).getStateList(body);
            call.enqueue(new Callback<StateModel>() {
                @Override
                public void onResponse(@NonNull Call<StateModel> call, @NonNull Response<StateModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                mArrStateList.addAll(response.body().getData());
                                setStateAdapter();

                            }
                        } else {
                            setStateAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<StateModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setStateAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    public void setStateAdapter() {
        try {
            mSpinnerState.setAdapter(null);
            mArrState.add(getString(R.string.spinner_select_state));

            if (mArrStateList.size() > 0) {
                for (StateModel state : mArrStateList) {
                    mArrState.add(state.getStateName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerState.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrState) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedStateIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerState.setAdapter(adapter);

            // Set State
            if (!data.getStateName().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                for (int s = 0; s < mArrState.size(); s++) {
                    if (mArrState.get(s).equalsIgnoreCase(data.getStateName())) {
                        mSpinnerState.setSelection(s);
                    }
                }
            } else {
                mSpinnerState.setSelection(0);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the City API
     */
    private void callToCityAPI(int mStateId) {
        try {
            if (mArrCity.size() > 0)
                mArrCity.clear();
            if (mArrCityList.size() > 0)
                mArrCityList.clear();

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setCityListJson(String.valueOf(mStateId), hospital_database));

            Call<CityModel> call = RetrofitClient.createService(ApiInterface.class).getCityList(body);
            call.enqueue(new Callback<CityModel>() {
                @Override
                public void onResponse(@NonNull Call<CityModel> call, @NonNull Response<CityModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                mArrCityList.addAll(response.body().getData());
                                setCityAdapter();
                            }
                        } else {
                            setCityAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CityModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setCityAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    public void setCityAdapter() {
        try {
            mArrCity.add(0, getString(R.string.spinner_select_city));

            if (mArrCityList.size() > 0) {
                for (CityModel cityModel : mArrCityList) {
                    mArrCity.add(cityModel.getCityName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerCity.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrCity) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCityIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerCity.setAdapter(adapter);

            //Set City
            if (!data.getCityName().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                for (int c = 0; c < mArrCity.size(); c++) {
                    if (mArrCity.get(c).equalsIgnoreCase(data.getCityName())) {
                        mSpinnerCity.setSelection(c);
                    }
                }
            } else {
                mSpinnerCity.setSelection(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Area API
     */
    private void callToAreaAPI(int mCityId) {
        try {
            if (mArrAreaList.size() > 0)
                mArrAreaList.clear();
            if (mArrArea.size() > 0)
                mArrArea.clear();

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAreaListJson(String.valueOf(mCityId), hospital_database));

            Call<AreaModel> call = RetrofitClient.createService(ApiInterface.class).getAreaList(body);
            call.enqueue(new Callback<AreaModel>() {
                @Override
                public void onResponse(@NonNull Call<AreaModel> call, @NonNull Response<AreaModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                mArrAreaList.addAll(response.body().getData());
                                setAreaAdapter();
                            }
                        } else {
                            setAreaAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AreaModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setAreaAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    public void setAreaAdapter() {
        try {
            mArrArea.add(0, getString(R.string.spinner_select_area));

            if (mArrAreaList.size() > 0) {
                for (AreaModel areaModel : mArrAreaList) {
                    mArrArea.add(areaModel.getAreaName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerArea.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrArea) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedAreaIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerArea.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Receptionist Patient Detail Process Fragment on visible
     *
     * @param isVisibleToUser - This can check the fragment is visible when swiping and will return
     *                        the boolean value based on that
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     * on Edit Text Update
     */
    private void doEditUpdatePatient() {
        KeyboardUtility.HideKeyboard(mActivity, mEditFirstName);
        try {
            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);

            String mFirstName = mEditFirstName.getText().toString().trim();
            String mLastName = mEditLastName.getText().toString().trim();
            String mEmail = mEditEmail.getText().toString().trim();
            String mMobile = mEditMobile.getText().toString();
            String mMiddleName = mEditMiddleName.getText().toString();
            String mOccupation = mEditOccupation.getText().toString();
            String mAddress = mEditAddress.getText().toString().trim();
            String mPinCode = mEditPinCode.getText().toString().trim();

            String mCityName = AppConstants.STR_EMPTY_STRING;
            if (!mEditCity.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                mCityName = mEditCity.getText().toString();
                mCityId = -1;
            }

            String mAreaName = AppConstants.STR_EMPTY_STRING;
            if (!mEditArea.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                mAreaName = mEditCity.getText().toString();
                mAreaId = -1;
            }

            // Selected Name Prefix
            int selected_prefix = mSpinnerNameTitle.getSelectedItemPosition();
            String NamePrefix = mArrPrefix.get(selected_prefix);

            String age = Common.getAge(mEditBirthDate.getText().toString());
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    setEditPatientDetailJson(mFirstName, mLastName, mEmail, mMobile, mCountryId, mStateId,
                            mCityId, mAreaId, mGender, age, mCityName, mAreaName, mUserId, mAddress, mPinCode, mOccupation, NamePrefix, mMiddleName, mPatientUserId, hospital_database));
            Call<AddPatientModel> call = RetrofitClient.createService(ApiInterface.class).addReceptionistPatient(requestBody);
            call.enqueue(new Callback<AddPatientModel>() {
                @Override
                public void onResponse(@NonNull Call<AddPatientModel> call, @NonNull Response<AddPatientModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {

                            if (response.body() != null) {
                                String mPatientId = response.body().getData().get(0).getID();
                                if (StringUtils.isEmpty(mImagePath)) {
                                    Common.setCustomToast(mActivity, response.body().getMessage());
                                } else {
                                    doSubmitProfileDetail(mPatientId);
                                }
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddPatientModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Add new city (Visible the edit text field and disable the spinner layout)
     */
    private void addNewCity() {
        mLinearCitySpinner.setVisibility(View.GONE);
        mRelativeAddNewCity.setVisibility(View.VISIBLE);
        mImageAddCity.setVisibility(View.GONE);
    }

    /**
     * Remove new city (Visible the spinner layout and disable the edit text field)
     */
    private void removeNewCity() {
        mLinearCitySpinner.setVisibility(View.VISIBLE);
        mRelativeAddNewCity.setVisibility(View.GONE);
        mImageAddCity.setVisibility(View.VISIBLE);
        mEditCity.setText(AppConstants.STR_EMPTY_STRING);
    }

    /**
     * Add new area (Visible the edit text field and disable the spinner layout)
     */
    private void addNewArea() {
        mLinearAreaSpinner.setVisibility(View.GONE);
        mRelativeAddNewArea.setVisibility(View.VISIBLE);
        mImageAddArea.setVisibility(View.GONE);
    }

    /**
     * Remove new area (Visible the spinner layout and disable the edit text field)
     */
    private void removeNewArea() {
        mLinearAreaSpinner.setVisibility(View.VISIBLE);
        mRelativeAddNewArea.setVisibility(View.GONE);
        mImageAddArea.setVisibility(View.VISIBLE);
        mEditArea.setText(AppConstants.STR_EMPTY_STRING);
    }

    /**
     * Open popup for camera and gallery
     */
    private void openGalleryPopup() {
        final CharSequence[] menus = {mActivity.getResources().getString(R.string.action_capture_image), mActivity.getResources().getString(R.string.action_select_from_gallery),
                mActivity.getResources().getString(R.string.action_cancel)};

        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        builder.setTitle(mActivity.getResources().getString(R.string.header_choose_image));
        builder.setItems(menus, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int pos) {

                boolean result = CameraGalleryPermission.checkPermission(mActivity,
                        AppPermissions.ReadWriteExternalStorageRequiredPermission());

                if (pos == 0) {
                    chosenTask = mActivity.getResources().getString(R.string.action_capture_image);
                    if (result) {
                        openCameraIntent();
                    }
                } else if (pos == 1) {
                    chosenTask = mActivity.getResources().getString(R.string.action_select_from_gallery);
                    if (result) {
                        openGalleryIntent();
                    }
                } else if (pos == 2) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**
     * Open the camera intent
     */
    private void openCameraIntent() {
        destination = AppUtil.currentTimeStampFile();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
        } else {
            File file = new File(Uri.fromFile(destination).getPath());
            Uri photoUri = FileProvider.getUriForFile(mActivity, mActivity.getPackageName() + ".provider", file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        }
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (intent.resolveActivity(mActivity.getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    /**
     * Open the gallery intent
     */
    private void openGalleryIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, REQUEST_GALLERY);
    }

    /**
     * Request camera permission dynamically
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case CameraGalleryPermission.INTERNAL_EXTERNAL_PERMISSION:
                Map<String, Integer> perms = new HashMap<>();
                perms.put(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(android.Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                if (perms.get(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    if (chosenTask.equals(mActivity.getResources().getString(R.string.action_capture_image))) {
                        openCameraIntent();
                    } else if (chosenTask.equals(mActivity.getResources().getString(R.string.action_select_from_gallery))) {
                        openGalleryIntent();
                    }
                } else {
                    Toast.makeText(mActivity, mActivity.getResources().getString(R.string.error_permission_denied), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                CropImage.activity(Uri.fromFile(destination)).setMinCropResultSize(1800, 1800)
                        .setMaxCropResultSize(1800, 1800).start(mActivity, this);
            } else if (requestCode == REQUEST_GALLERY) {
                mImageUri = data.getData();
                mFilePath = Common.getRealPathFromURI(mImageUri, mActivity);
                CropImage.activity(mImageUri).setMinCropResultSize(1800, 1800)
                        .setMaxCropResultSize(1800, 1800).start(mActivity, this);

                if (mFilePath == null)
                    mFilePath = mImageUri.getPath(); // from File Manager

                if (mFilePath != null) {
                    File mFile = new File(mFilePath);
                    mBitmap = Common.decodeFile(mFile, AppConstants.PIC_WIDTH,
                            AppConstants.PIC_HEIGHT);
                    mPath = mFile;
                }
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                mImageUri = result.getUri();
                onSelectFromGalleryResult(mImageUri);
            }
        }
    }

    /**
     * Sets the cropped image from the gallery or camera intent
     */
    private void onSelectFromGalleryResult(Uri dataUri) {
        Bitmap bm = null;
        String imageName = null;
        if (dataUri != null) {
            try {
                //  bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), dataUri);
                File file = new File(dataUri.getPath());
                imageName = file.getName();
                mImagePath = dataUri.getPath();
                Compressor compressor = new Compressor(mActivity);
                if (ImageUtil.convertURItoFile(mActivity, dataUri) != null)
                    bm = compressor.compressToBitmap(ImageUtil.convertURItoFile(mActivity, dataUri));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        if (bm != null) {
            bm.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        }

        mImagePatientPicture.setImageBitmap(bm);
        mImagePatientPicture.setVisibility(View.VISIBLE);
    }

    /**
     * this method Call Image Upload API Call
     *
     * @param strCustomerId
     */
    private void doSubmitProfileDetail(final String strCustomerId) {
        try {

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            //Common.showLoadingDialog(mActivity, getString(R.string.dialog_file_Upload));
            //Multipart
            File file = new File(mImagePath);
            final RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);

            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part body =
                    MultipartBody.Part.createFormData(WebFields.ADD_UPLOAD_PICS.REQUEST_IMAGE_DATA, file.getName(), requestFile);

            // add another part within the multipart request
            RequestBody method =
                    RequestBody.create(MediaType.parse("text/plain"), WebFields.ADD_UPLOAD_PICS.MODE);
            RequestBody AccessType =
                    RequestBody.create(MediaType.parse("text/plain"), "Patient");
            RequestBody UserID =
                    RequestBody.create(MediaType.parse("text/plain"), strCustomerId);
            RequestBody mDatabaseName =
                    RequestBody.create(MediaType.parse("text/plain"), hospital_database);

            Call<AddUploadPicModel> callRepos = new RetrofitClient().createService(ApiInterface.class).addUploadPics(method,
                    AccessType, UserID, mDatabaseName, body);

            callRepos.enqueue(new Callback<AddUploadPicModel>() {
                @Override
                public void onResponse(@NonNull Call<AddUploadPicModel> call, @NonNull Response<AddUploadPicModel> response) {

                    Common.insertLog("Add UploadPics" + call.toString());
                    Common.hideDialog();
                    try {
                        int error = response.body().getError();
                        String message = response.body().getMessage();

                        if (error == AppConstants.API_SUCCESS_ERROR) {
                            Common.setCustomToast(mActivity, message);
                        } else {
                            Common.setCustomToast(mActivity, message);
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddUploadPicModel> call, @NonNull Throwable t) {
                    Common.insertLog("Add UploadPics error " + t.getMessage());
                    Common.hideDialog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}