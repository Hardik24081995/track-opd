package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.badoualy.datepicker.DatePickerTimeline;
import com.trackopd.R;
import com.trackopd.model.DoctorHomeListModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DoctorHomeFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private TextView mTextTodayAppointment, mTextPendingAppointment, mTextConfirmedAppointment,
            mTextCompletedAppointment;
    private DatePickerTimeline mDatePickerTimeLine;
    private Date mSelectedDate, mCurrentDate;
    private String mAppointmentDate;
    private AppUtil mAppUtils;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_doctor_home, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mAppUtils = new AppUtil(mActivity);

        getIds();
        getCurrentDate();
        setRegListeners();
        callDoctorHomeListAPI();
        setHasOptionsMenu(true);
        return mView;
    }


    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * get Declare Id on Widgets
     */
    private void getIds() {
        try {
            // Date Picker Time Line
            mDatePickerTimeLine = mView.findViewById(R.id.date_picker_time_line_doctor_home);

            //Text Views
            mTextTodayAppointment = mView.findViewById(R.id.text_doctor_home_today_appointment);
            mTextPendingAppointment = mView.findViewById(R.id.text_doctor_home_pending_appointment);
            mTextConfirmedAppointment = mView.findViewById(R.id.text_doctor_home_confirmed_appointment);
            mTextCompletedAppointment = mView.findViewById(R.id.text_doctor_home_completed_appointment);
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Date Selected Listener
            mDatePickerTimeLine.setOnDateSelectedListener(dateSelectedListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Date Selected Click listener
     */
    private DatePickerTimeline.OnDateSelectedListener dateSelectedListener = new DatePickerTimeline.OnDateSelectedListener() {
        @Override
        public void onDateSelected(int year, int month, int day, int index) {
            Common.insertLog("Date Selected::> " + year + "/" + month + "/" + day);
            String mDate = year + "-" + (month + 1) + "-" + day;
            try {
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat df = new SimpleDateFormat(
                        mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));

                mSelectedDate = df.parse(mDate);

                DateFormat dateFormat = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
                mAppointmentDate = dateFormat.format(mSelectedDate);

                callDoctorHomeListAPI();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * Call To Doctor Home Count API
     */
    private void callDoctorHomeListAPI() {
        if (!mAppUtils.getConnectionState()) {
            setData();
        } else {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setDoctorHomeListJson(mAppointmentDate, hospital_database));

            Call<DoctorHomeListModel> call = RetrofitClient.createService(ApiInterface.class)
                    .getDoctorHomeListApi(requestBody);

            call.enqueue(new Callback<DoctorHomeListModel>() {
                @Override
                public void onResponse(Call<DoctorHomeListModel> call, Response<DoctorHomeListModel> response) {
                    Common.insertLog(response.body().toString());
                    if (response.isSuccessful() &&
                            response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                        setResposneData(response.body().getData());
                    } else {
                        Common.setCustomToast(mActivity, response.body().getMessage());
                        setData();
                    }
                }

                @Override
                public void onFailure(Call<DoctorHomeListModel> call, Throwable t) {
                    Common.insertLog(t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());
                    setData();
                }
            });
        }
    }

    /**
     * Set Response Data
     *
     * @param data Data from Doctor Home List
     */
    private void setResposneData(List<DoctorHomeListModel.Data> data) {
        try {
            if (data != null && data.size() > 0) {

                String total = data.get(0).getTotalAppointment();
                String pending = data.get(0).getPending();
                String confirmed = data.get(0).getConfirmed();
                String completed = data.get(0).getCompleted();

                //Text Views
                mTextTodayAppointment.setText("(" + total + ")");
                mTextPendingAppointment.setText("(" + pending + ")");
                mTextConfirmedAppointment.setText("(" + confirmed + ")");
                mTextCompletedAppointment.setText("(" + completed + ")");

            } else {
                setData();
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
            setData();
        }
    }

    private void setData() {
        //Text Views
        mTextTodayAppointment.setText("(0)");
        mTextPendingAppointment.setText("(0)");
        mTextConfirmedAppointment.setText("(0)");
        mTextCompletedAppointment.setText("(0)");
    }

    /***
     * Set Current date in date picker and get Current date
     */
    private void getCurrentDate() {
        Calendar CurrentCalender = Calendar.getInstance();
        mDatePickerTimeLine.setFirstVisibleDate(CurrentCalender.get(Calendar.YEAR),
                0,
                1);

        mDatePickerTimeLine.setSelectedDate(CurrentCalender.get(Calendar.YEAR),
                CurrentCalender.get(Calendar.MONTH),
                CurrentCalender.get(Calendar.DAY_OF_MONTH));

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        String strCurrentDate = dateFormatter.format(CurrentCalender.getTime());
        try {
            mSelectedDate = dateFormatter.parse(strCurrentDate);
            mCurrentDate = dateFormatter.parse(strCurrentDate);

            DateFormat dateFormat = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
            mAppointmentDate = dateFormat.format(mSelectedDate);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}