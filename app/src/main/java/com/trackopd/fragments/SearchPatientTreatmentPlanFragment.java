package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

public class SearchPatientTreatmentPlanFragment extends Fragment {

    private Activity mActivity;
    private View mView;
    private Button mButtonSearch;
    private EditText mEditDate;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_search_patient_treatment_plan, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        getIds();
        setRegListeners();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Set the prepare menu options
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Text Views
            mEditDate = mView.findViewById(R.id.edit_search_patient_treatment_plan_date);

            // Buttons
            mButtonSearch = mView.findViewById(R.id.button_search_patient_treatment_plan_search);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mEditDate.setOnClickListener(clickListener);
            mButtonSearch.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.button_search_patient_treatment_plan_search:
                    callToPatientTreatmentPlanFragment();
                    break;

                case R.id.edit_search_patient_treatment_plan_date:
                    Common.openDatePickerForAllDates(mActivity, mEditDate);
                    break;
            }
        }
    };

    /**
     * Sets up the Patient Treatment Plan Fragment
     */
    private void callToPatientTreatmentPlanFragment() {
        try {
            String mTreatmentPlanDate;

            if (mEditDate.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                mTreatmentPlanDate = mActivity.getResources().getString(R.string.date_format_first_time);
            } else {
                mTreatmentPlanDate = Common.convertDateWithoutTime(mActivity, mEditDate.getText().toString().trim(), mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
            }

            Fragment fragment = new PatientTreatmentPlanFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_FOLLOW_UP_DATE, mTreatmentPlanDate);
            fragment.setArguments(args);
            FragmentManager fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    mActivity.getResources().getString(R.string.nav_menu_treatment_plan)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
