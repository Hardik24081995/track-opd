package com.trackopd.fragments;


import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.model.AddMediclaimModel;
import com.trackopd.model.SurgeryListModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ImplantSurgeryFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private EditText mEditAConstant,mEditImplantPower,mEditImplantPlacement,mEditImplantExpiry,
            mEditImplantSLNo,mEditImplantNotes;
    private Button mButtonSubmit;
    private SurgeryListModel mSurgeryDetail;
    private String AppointmentID,mUserType;
    private AppUtil mAppUtils;
    private AutoCompleteTextView mEditImplantBrand,mEditImplantName;


    public ImplantSurgeryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView= inflater.inflate(R.layout.fragment_implant_surgery, container, false);
        mActivity=getActivity();
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mAppUtils=new AppUtil(mActivity);

        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        getBundle();
        getIds();
        setData();
        setRegListeners();
        return mView;
    }

    /**
     * This Method get Bundle Value can be pass
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle.getSerializable(AppConstants.BUNDLE_ADD_SURGERY)!=null) {
                mSurgeryDetail = (SurgeryListModel) bundle.getSerializable(AppConstants.BUNDLE_ADD_SURGERY);
                AppointmentID = mSurgeryDetail.getAppointmentID();
            }else {
                AppointmentID=bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_ID);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try{
            //Edit Text
            mEditAConstant=mView.findViewById(R.id.edit_text_implant_surgery_a_constant);
            mEditImplantPower=mView.findViewById(R.id.edit_text_implant_surgery_implant_power);
            mEditImplantPlacement=mView.findViewById(R.id.edit_text_implant_surgery_implant_placement);
            mEditImplantExpiry=mView.findViewById(R.id.edit_text_implant_surgery_implant_expirty_date);
            mEditImplantSLNo=mView.findViewById(R.id.edit_text_implant_surgery_implant_sl_no);
            mEditImplantNotes=mView.findViewById(R.id.edit_text_implant_surgery_notes);

            mEditImplantBrand=mView.findViewById(R.id.edit_text_implant_surgery_implant_brand);
            mEditImplantName=mView.findViewById(R.id.edit_text_implant_surgery_implant_name);

            mButtonSubmit=mView.findViewById(R.id.button_implant_surgery_submit);
        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            if (mSurgeryDetail!=null) {
                 mEditImplantBrand.setText(mSurgeryDetail.getImplantBrand());
                 mEditImplantName.setText(mSurgeryDetail.getImplantName());
                 mEditAConstant.setText(mSurgeryDetail.getAConstant());
                 mEditImplantPower.setText(mSurgeryDetail.getImplantPower());
                 mEditImplantPlacement.setText(mSurgeryDetail.getImplantPlacement());
                 mEditImplantExpiry.setText(mSurgeryDetail.getImplantExpiry());
                 mEditImplantSLNo.setText(mSurgeryDetail.getImplantSlNo());
                 mEditImplantNotes.setText(mSurgeryDetail.getNotes());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Edit Click Listeners
            mButtonSubmit.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_implant_surgery_submit:
                    if (checkImplantValidation()){
                        callToImplantSurgery();
                    }
                    break;
            }
        }
    };

    private void callToImplantSurgery() {
       if (!mAppUtils.getConnectionState()){
           Common.setCustomToast(mActivity,mActivity.getResources().getString(R.string.no_internet_connection));
       }else {

           String a_constant=mEditAConstant.getText().toString();
           String implant_power=mEditImplantPower.getText().toString();
           String implant_placement=mEditImplantPlacement.getText().toString();
           String implant_expirty=mEditImplantExpiry.getText().toString();
           String implant_SlNo=mEditImplantSLNo.getText().toString();
           String implant_notes=mEditImplantNotes.getText().toString();

           String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
           String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

           String ImplantBrand=mEditImplantBrand.getText().toString();
           String ImplantName=mEditImplantBrand.getText().toString();

           RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                   APICommonMethods.setAddImplantSurgeryJson(mUserId, mDatabaseName,AppointmentID,ImplantBrand,ImplantName,a_constant,implant_power,
                           implant_placement,implant_expirty,implant_SlNo,implant_notes));
           Common.insertLog("Request "+requestBody);

           Call<AddMediclaimModel> call = RetrofitClient.createService(ApiInterface.class).AddImplantSurgery(requestBody);

           call.enqueue(new Callback<AddMediclaimModel>() {
               @Override
               public void onResponse(Call<AddMediclaimModel> call, Response<AddMediclaimModel> response) {
                   Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                   try {
                       String mJson = (new Gson().toJson(response.body()));
                       JSONObject jsonObject = new JSONObject(mJson);
                       String mMessage = jsonObject.getString(WebFields.MESSAGE);
                       int mError = jsonObject.getInt(WebFields.ERROR);
                       Common.insertLog("mMessage:::> " + mMessage);

                       if (response.isSuccessful() && mError == 200) {
                           if (response.body() != null) {
                              /* removeAllFragments();
                               callToImplantList();*/
                               Common.setCustomToast(mActivity,mMessage);
                               AddSurgeryFragment.setPage(3);
                           }
                       }
                   } catch (JSONException e) {
                       e.printStackTrace();
                   }
               }

               @Override
               public void onFailure(Call<AddMediclaimModel> call, Throwable t) {
                   Common.insertLog("Failure:::> " + t.getMessage());
               }
           });
       }
    }

    /**
     * Check Implant Validation
     * @return  boolean value true/false
     */
    private boolean checkImplantValidation() {
        boolean status=true;
        return status;
    }
    /**
     * Used to remove all fragment in the fragment manager after logout
     */
    private void removeAllFragments() {
        try {
            List<Fragment> fragments = null;

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragments = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist)))
            {
                fragments = ((OptometristHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragments = ((DoctorHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragments = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            }


            if (fragments.size() > 0) {
                for (int i = 0; i < fragments.size(); i++) {
                    if (fragments.get(i) != null) {

                        if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                            ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                            ((OptometristHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                            ((DoctorHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                            ((CouncillorHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callToImplantList() {
        try {
            Fragment fragment = new SurgeryListFragment();
            Bundle args = new Bundle();

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
            }

            args.putString(AppConstants.BUNDLE_PATIENT_NAME, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_FOLLOW_UP_DATE, "1970-01-01");
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, -1);
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_COUNCILLOR, -1);
            //fragment.setArguments(args);

            FragmentManager fragmentManager = null;

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.nav_menu_surgery))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
