package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.adapter.MedicationAdapter;
import com.trackopd.adapter.PatientTreatmentPlanAdapter;
import com.trackopd.model.PatientPrescriptionModel;
import com.trackopd.model.PreliminaryExaminationModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import java.util.ArrayList;

public class MedicationDetailsFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private RelativeLayout mRelativeImageBorder, mRelativeNoData;
    private ImageView mImageUserProfilePic;
    private TextView mTextName, mTextMobileNo, mTextAppointmentNo, mTextAppointmentDate;
    private PreliminaryExaminationModel preliminaryExaminationModel;
    private RecyclerView mRecyclerView;
    private ArrayList<PreliminaryExaminationModel.Medication> mArrPreliminaryExaminationModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_patient_prescription_details, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrPreliminaryExaminationModel = new ArrayList<>();

        getBundle();
        getIds();
        setData();
        setMedicationList();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                preliminaryExaminationModel = (PreliminaryExaminationModel) bundle.getSerializable(AppConstants.BUNDLE_PATIENT_ARRAY);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layouts
            mRelativeImageBorder = mView.findViewById(R.id.relative_patient_prescription_profile_pic);
            mRelativeNoData = mView.findViewById(R.id.relative_no_data_available);

            // Image Views
            mImageUserProfilePic = mView.findViewById(R.id.image_patient_prescription_profile_pic);

            // Text Views
            mTextName = mView.findViewById(R.id.text_patient_prescription_name);
            mTextMobileNo = mView.findViewById(R.id.text_patient_prescription_mobile_no);
            mTextAppointmentNo = mView.findViewById(R.id.text_patient_prescription_patient_appointment_no);
            mTextAppointmentDate = mView.findViewById(R.id.text_patient_prescription_appointment_date);

            // Recycler View
            mRecyclerView = mView.findViewById(R.id.recycler_view);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            String mPatientFirstName = preliminaryExaminationModel.getPatientFirstName();
            String mPatientLastName = preliminaryExaminationModel.getPatientLastName();
            String mPatientMobileNo = preliminaryExaminationModel.getPatientMobileNo();
            String mAppointmentNo = preliminaryExaminationModel.getTicketNumber();
            String mAppointmentDate = preliminaryExaminationModel.getTreatmentDate();

            mRelativeImageBorder.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.user_image_border));
            mImageUserProfilePic.setImageDrawable(Common.setLabeledDefaultImageView(mActivity, mPatientFirstName, mPatientLastName));
            mTextName.setText(mPatientFirstName + AppConstants.STR_EMPTY_SPACE + mPatientLastName);
            mTextMobileNo.setText(mPatientMobileNo);
            mTextAppointmentNo.setText(mActivity.getResources().getString(R.string.edit_patient_appointment_number) + AppConstants.STR_COLON + mAppointmentNo);
            mTextAppointmentDate.setText(mActivity.getResources().getString(R.string.edit_patient_treatment_date) + AppConstants.STR_COLON + mAppointmentDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Binds the medication data
     */
    private void setMedicationList() {
        try {
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mGridLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(mGridLayoutManager);

            ArrayList<PreliminaryExaminationModel.Medication> medication = (ArrayList<PreliminaryExaminationModel.Medication>) preliminaryExaminationModel.getMedication();
            mArrPreliminaryExaminationModel.addAll(medication);

            MedicationAdapter mAdapterMedication = new MedicationAdapter(mActivity, mArrPreliminaryExaminationModel);
            mRecyclerView.setAdapter(mAdapterMedication);

            setViewVisibility();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the view visibility as per the array size
     */
    private void setViewVisibility() {
        if (mArrPreliminaryExaminationModel.size() != 0) {
            mRelativeNoData.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mRelativeNoData.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }
}
