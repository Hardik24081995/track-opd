package com.trackopd.fragments;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.trackopd.R;
import com.trackopd.utils.AppConstants;
import com.trackopd.webServices.WebFields;

public class PrivacyPolicyFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private WebView mWebView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_privacy_policy, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        getIds();
        setData();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Web View
            mWebView = mView.findViewById(R.id.web_view_privacy_policy);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data to the web view
     */
    private void setData() {
        try {
//            mWebView.loadDataWithBaseURL(null, AppConstants.STR_ABOUT_US, AppConstants.STR_MIME_TYPE, AppConstants.STR_UTF, null);
            mWebView.loadUrl(WebFields.API_PRIVACY_POLICY);
            mWebView.setOverScrollMode(View.OVER_SCROLL_NEVER);
            mWebView.setHorizontalScrollBarEnabled(false);
            mWebView.setVerticalScrollBarEnabled(false);
            mWebView.setFocusable(false);
            mWebView.setFocusableInTouchMode(false);
            mWebView.setBackgroundColor(Color.TRANSPARENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}