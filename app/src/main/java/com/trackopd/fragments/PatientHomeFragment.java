package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.model.DoctorModel;
import com.trackopd.model.GetPatientHomeModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientHomeFragment extends Fragment {

    private View mView;
    private Activity mActivity;

    private TextView mTextAppointmentCount,mTextNextAppointmentNo,mTextNextAppointmentDateTime,
            mTextNextDoctor,mTextNextBookingType,mTextNextPaymentType,mTextNextPaymentAmount,
            mTextLastAppointmentNo,mTextLastAppointmentDateTime,
            mTextLastDoctor,mTextLastBookingType,mTextLastPaymentType,mTextLastPaymentAmount;

    private CardView mCardNextAppointment,mCardLastAppointment;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_patient_home, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        getIds();
        callToPatientHome();
        setHasOptionsMenu(true);
        return mView;
    }


    /**
     * Get Declare Ids
     */
    private void getIds() {
        try{
           mTextAppointmentCount= mView.findViewById(R.id.text_patient_appointment_count);
           mTextNextAppointmentNo= mView.findViewById(R.id.text_patient_home_next_appointment_number);
           mTextNextAppointmentDateTime= mView.findViewById(R.id.text_patient_home_next_appointment_date);
           mTextNextDoctor= mView.findViewById(R.id.text_patient_home_next_doctor_name);
           mTextNextBookingType= mView.findViewById(R.id.text_patient_home_next_booking_type);
           mTextNextPaymentType= mView.findViewById(R.id.text_patient_home_next_payment_type);
           mTextNextPaymentAmount= mView.findViewById(R.id.text_patient_home_next_payment_type_payment_amount);

           mTextLastAppointmentNo= mView.findViewById(R.id.text_patient_home_last_appointment_number);
           mTextLastAppointmentDateTime= mView.findViewById(R.id.text_patient_home_last_appointment_date);
           mTextLastDoctor= mView.findViewById(R.id.text_patient_home_last_doctor_name);
           mTextLastBookingType= mView.findViewById(R.id.text_patient_home_last_booking_type);
           mTextLastPaymentType= mView.findViewById(R.id.text_patient_home_last_payment_type);
           mTextLastPaymentAmount= mView.findViewById(R.id.text_patient_home_last_payment_amount);

           mCardNextAppointment=mView.findViewById(R.id.card_patient_home_next_appointment);
           mCardLastAppointment=mView.findViewById(R.id.card_patient_home_last_appointment);

        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Call API Patient Home Screen Count
     */
    private void callToPatientHome() {
      try{

          String mPatientId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);

          String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

          RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                  APICommonMethods.setPatientHomeJson(mPatientId,hospital_database));

          Call<GetPatientHomeModel> call = RetrofitClient.createService(ApiInterface.class).getPatientHomeList(body);
          call.enqueue(new Callback<GetPatientHomeModel>() {
              @Override
              public void onResponse(Call<GetPatientHomeModel> call, Response<GetPatientHomeModel> response) {
                 Common.insertLog("Resposne..."+ response.toString());
                 if (response.isSuccessful()
                         &&response.body().getError()==AppConstants.API_SUCCESS_ERROR){

                     setResposneData(response.body());

                 }else {
                     Common.setCustomToast(mActivity,response.body().getMessage());
                 }
              }
              @Override
              public void onFailure(Call<GetPatientHomeModel> call, Throwable t) {
                 Common.insertLog(t.getMessage());
                 Common.setCustomToast(mActivity,t.getMessage());
              }
          });

      }catch (Exception e){
         Common.insertLog(e.getMessage());
      }
    }

    /**
     * Set Response Data on Text View
     * @param body - Body Response API
     */
    private void setResposneData(GetPatientHomeModel body) {
        try{

            ArrayList<GetPatientHomeModel.TotalAppointment> mArrAppointment=
                    (ArrayList<GetPatientHomeModel.TotalAppointment>) body.getTotalAppointment();

            if (mArrAppointment!=null && mArrAppointment.size()>0){
                if (mArrAppointment.get(0).getAppointment()!=null){
                    String count=mArrAppointment.get(0).getAppointment();

                    mTextAppointmentCount.setText("("+count+")");
                }else {
                    mTextAppointmentCount.setText("("+0+")");
                }
            }else {
                mTextAppointmentCount.setText("("+0+")");
            }

            //Set Next Appointment
            ArrayList<GetPatientHomeModel.NextVisit> mArrNextVisit=
                    (ArrayList<GetPatientHomeModel.NextVisit>) body.getNextVisit();

            if (mArrNextVisit!=null && mArrNextVisit.size()>0){
                if (mArrNextVisit.get(0).getAppoinmentDateTime()!=null){
                    String dateTime=mArrNextVisit.get(0).getAppoinmentDateTime();
                    String ticketNo=mArrNextVisit.get(0).getTicketNumber();
                    String doctorName=mArrNextVisit.get(0).getDoctorName();
                    String type=mArrNextVisit.get(0).getAppointmentType();
                    String payment=mArrNextVisit.get(0).getPaymentStatus();

                    mTextNextAppointmentDateTime.setText(Common.isEmptyString(mActivity,dateTime));
                    mTextNextAppointmentNo.setText(Common.isEmptyString(mActivity,ticketNo));
                    mTextNextDoctor.setText(Common.isEmptyString(mActivity,doctorName));
                    mTextNextBookingType.setText(Common.isEmptyString(mActivity,type));
                    mTextNextPaymentType.setText(Common.isEmptyString(mActivity,payment));

                }else {
                    mCardNextAppointment.setVisibility(View.GONE);
                }
            }else {
                mCardNextAppointment.setVisibility(View.GONE);
            }


            //Set Last Appointment
            ArrayList<GetPatientHomeModel.LastVisit> mArrLastVisit=
                    (ArrayList<GetPatientHomeModel.LastVisit>) body.getLastVisit();

            if (mArrLastVisit!=null && mArrLastVisit.size()>0){
                if (mArrLastVisit.get(0).getAppoinmentDateTime()!=null){

                    String dateTime=mArrLastVisit.get(0).getAppoinmentDateTime();
                    String ticketNo=mArrLastVisit.get(0).getTicketNumber();
                    String doctorName=mArrLastVisit.get(0).getDoctorName();
                    String type=mArrLastVisit.get(0).getAppointmentType();
                    String payment=mArrLastVisit.get(0).getPaymentStatus();

                    mTextLastAppointmentDateTime.setText(Common.isEmptyString(mActivity,dateTime));
                    mTextLastAppointmentNo.setText(Common.isEmptyString(mActivity,ticketNo));
                    mTextLastDoctor.setText(Common.isEmptyString(mActivity,doctorName));
                    mTextLastBookingType.setText(Common.isEmptyString(mActivity,type));
                    mTextLastPaymentType.setText(Common.isEmptyString(mActivity,payment));
                }else {
                    mCardLastAppointment.setVisibility(View.GONE);
                }
            }else {
                mCardLastAppointment.setVisibility(View.GONE);
            }
        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }
}