package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.ViewPagerAdapter;
import com.trackopd.model.ReceptionistPatientModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

public class ReceptionistPatientDetailFragment extends Fragment {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    public String mPatientFirstName, mPatientLastName, mPatientMobile, mPatientCode, mPatientId,
            mUserType,mPatientProfile,mPatientUserID;
    public Bundle mBundle;
    public ReceptionistPatientModel mPatientModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_receptionist_patient_detail, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        getBundle();
        getIds();
        setRegListeners();
        setBundle();
        setViewPager();
        setHasOptionsMenu(false);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        final MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(false);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
               /* mPatientFirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mPatientLastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mPatientMobile = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mPatientCode = bundle.getString(AppConstants.BUNDLE_PATIENT_CODE);
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mPatientProfile = bundle.getString(AppConstants.BUNDLE_PATIENT_PROFILE);*/
                mPatientModel= (ReceptionistPatientModel) bundle.getSerializable(AppConstants.BUNDLE_ADD_PATIENT);
            }
            Common.insertLog("Bundle Detail Get::::> " + mPatientFirstName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Tab Layout
            mTabLayout = mView.findViewById(R.id.tab_receptionist_patient_detail);

            // View Pager
            mViewPager = mView.findViewById(R.id.view_pager_receptionist_patient_detail);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mTabLayout.setOnTabSelectedListener(onTabSelectedListener(mViewPager));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set up the fragment pass data
     */
    private void setBundle() {
        try {
            mBundle = new Bundle();
            mBundle.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, mPatientModel.getFirstName());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, mPatientModel.getLastName());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_CODE, mPatientModel.getPatientCode());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mPatientModel.getMobileNo());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_ID, mPatientModel.getPatientID());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_USER_ID, mPatientModel.getUserID());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_PROFILE,mPatientModel.getProfileImage());
            mBundle.putString(AppConstants.BUNDLE_MRD_NO,mPatientModel.getPatientCode());

            mBundle.putBoolean(AppConstants.BUNDLE_ADD_PAYMENT, true);
            mBundle.putBoolean(AppConstants.BUNDLE_ADD_PATIENT, false);
            mBundle.putSerializable(AppConstants.BUNDLE_PATIENT_ARRAY, mPatientModel);


            mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the view pager and binding the tab layouts
     */
    private void setViewPager() {
        try {
            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();

            }



            String patientName=mPatientModel.getFirstName()+AppConstants.STR_EMPTY_SPACE+
                    mPatientModel.getLastName();

            ViewPagerAdapter adapter = new ViewPagerAdapter(fragmentManager);
            adapter.addFragment(new EditPatientDetailFragment(), getString(R.string.text_details),mBundle);
            adapter.addFragment(new ReceptionistPatientDetailProcessFragment(), getString(R.string.nav_menu_process), mBundle);
            adapter.addFragment(new ReceptionistPatientDetailAppointmentFragment(), getString(R.string.nav_menu_appointment), mBundle);
            adapter.addFragment(new ReceptionistPatientDetailHistoryAndComplaintsFragment(), getString(R.string.tab_history_and_complaints), mBundle);
            adapter.addFragment(new PatientVisionFragment(),
                    getString(R.string.tab_vision), mBundle);
            adapter.addFragment(new PatientPreExaminationFragment(),"Pre-Examination" , mBundle);
            adapter.addFragment(new PatientDiagnosisFragment(),
                    getString(R.string.tab_diagnosis), mBundle);
            adapter.addFragment(new PatientDoodleFragment(),
                    getString(R.string.tab_doodle), mBundle);
            adapter.addFragment(new PatientInvestigationFragment(),
                    getString(R.string.tab_investigation), mBundle);
            adapter.addFragment(new PatientTreatmentFragment(),
                    getString(R.string.tab_prescription), mBundle);
            adapter.addFragment(new PatientSurgerySuggestedFragment(), getString(R.string.tab_surgery_suggested), mBundle );
            adapter.addFragment(new PatientCouncellingFragment(),
                    getString(R.string.nav_menu_counselling), mBundle);
            adapter.addFragment(new ReceptionistPatientDetailPrelimimaryExaminationFragment(),
                    getString(R.string.nav_menu_surgery), mBundle);
//            adapter.addFragment(new PatientDetailPaymentFragment(),
//                    getString(R.string.nav_menu_payment), mBundle);
            adapter.addFragment(new PatientPrintFragment(),
                    getString(R.string.tab_print), mBundle);
            adapter.addFragment(new PatientPaymentHistoryFragment(), "Payment History" , mBundle);

            /**adapter.addFragment(new ReceptionistPatientDetailPrescriptionFragment(), getString(R.string.nav_menu_prescription), mBundle);
            adapter.addFragment(new ReceptionistPatientDetailDocumentFragment(), getString(R.string.nav_menu_document), mBundle);
            adapter.addFragment(new ReceptionistPatientDetailFollowUpFragment(), getString(R.string.nav_menu_follow_up), mBundle);*/

            mViewPager.setAdapter(adapter);
            mTabLayout.setupWithViewPager(mViewPager);
            mViewPager.setOffscreenPageLimit(1);
            mViewPager.setCurrentItem(0);

            mTabLayout.setSelectedTabIndicatorColor(Common.setThemeColor(mActivity));
            mTabLayout.setTabTextColors(mActivity.getResources().getColor(R.color.colorBlack),
                    Common.setThemeColor(mActivity));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private TabLayout.OnTabSelectedListener onTabSelectedListener(final ViewPager viewPager) {

        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }
}