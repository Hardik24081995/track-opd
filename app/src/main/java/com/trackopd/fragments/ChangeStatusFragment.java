package com.trackopd.fragments;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.model.AddChangeStatusModel;
import com.trackopd.model.ChangeStatusModel;
import com.trackopd.model.ReceptionistAppointmentModel;
import com.trackopd.model.UserByUserTypeModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangeStatusFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private Spinner mSpinnerAppointment, mSpinnerNextStatus, mSpinnerAssigned;
    private ImageView mImageNextStatus, mImageAppointment, mImageAssigned;
    private EditText mEditAssignName, mEditCurrentStatus, mEditPatient, mEditAppointmentNo;
    private Button mButtonSubmit;
    private int mSelectedAppointmentIndex, mSelectedStatusIndex, mSelectedAssignedIndex, currentPageIndex = 1;
    private String mPatientName, mPatientCode, mPatientMobile, mAppointmentNo, mAppointmentDate, mFirstName, mLastName, mCurrentStatus,
            mAppointmentID, mAssigned, mPatientID, mUserType;
    private LinearLayout mLinearAppointment, mLinearCurrentStatus, mLinearAssignName, mLinearAssigned, mLinearNextStatus;
    private ArrayList<ReceptionistAppointmentModel> mArrAppointmentList;
    private ArrayList<ChangeStatusModel> mArrNextStatusList;
    private ArrayList<UserByUserTypeModel> mArrUserList;
    private ArrayList<String> mArrAppointment;
    private ArrayList<String> mArrNextStatus;
    private ArrayList<String> mArrUser;
    private ArrayAdapter mAdapterAppointment, mAdapterChangeStatus;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_change_status, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrAppointmentList = new ArrayList<>();
        mArrAppointment = new ArrayList<>();
        mArrNextStatusList = new ArrayList<>();
        mArrNextStatus = new ArrayList<>();
        mArrUserList = new ArrayList<>();
        mArrUser = new ArrayList<>();
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        getBundle();
        getIds();
        setRegListeners();
        setData();
        setHasOptionsMenu(true);
        callToNextStatusAPI();
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mFirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mLastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mPatientCode = bundle.getString(AppConstants.BUNDLE_PATIENT_CODE);
                mPatientMobile = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mCurrentStatus = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_STATUS);
                mAppointmentID = bundle.getString(WebFields.CHANGE_STATUS.REQUEST_APPOINTMENT_ID);
                mPatientID = bundle.getString(WebFields.CHANGE_STATUS.REQUEST_PATIENT_ID);
                mAssigned = bundle.getString(WebFields.CHANGE_STATUS.REQUEST_ACTION_USER_ID);
                mAppointmentNo = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mPatientName = mFirstName + AppConstants.STR_EMPTY_SPACE + mLastName;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Spinners
            mSpinnerNextStatus = mView.findViewById(R.id.spinner_change_status_next_status);
            mSpinnerAppointment = mView.findViewById(R.id.spinner_change_status_appointment);
            mSpinnerAssigned = mView.findViewById(R.id.spinner_change_status_assign);

            // Image Views
            mImageNextStatus = mView.findViewById(R.id.image_change_status_next_status);
            mImageAppointment = mView.findViewById(R.id.image_change_status_appointment);
            mImageAssigned = mView.findViewById(R.id.image_change_status_assign);

            // Edit Texts
            mEditAssignName = mView.findViewById(R.id.edit_change_status_assign_name);
            mEditCurrentStatus = mView.findViewById(R.id.edit_change_status_current_state);
            mEditPatient = mView.findViewById(R.id.edit_change_status_patient);
            mEditAppointmentNo = mView.findViewById(R.id.edit_change_status_appointment);
            // Buttons
            mButtonSubmit = mView.findViewById(R.id.button_change_status_submit);

            // Linear Layouts
            mLinearAppointment = mView.findViewById(R.id.linear_layout_change_status_appointment);
            mLinearAssignName = mView.findViewById(R.id.linear_layout_change_status_assign_name);
            mLinearCurrentStatus = mView.findViewById(R.id.linear_layout_change_status_current_state);
            mLinearNextStatus = mView.findViewById(R.id.linear_layout_change_status_next_status);
            mLinearAssigned = mView.findViewById(R.id.linear_layout_change_status_assign);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Click Listeners
            mButtonSubmit.setOnClickListener(clickListener);
            // ToDo:  Spinner Item Select Listener
            mSpinnerNextStatus.setOnItemSelectedListener(onItemSelectedListener);
            mSpinnerAppointment.setOnItemSelectedListener(onItemSelectedListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_change_status_submit:
                    if (checkValidation()) {
                        doChangeStatus();
                    }
                    break;
                case R.id.edit_change_status_patient:
                    callToSearchPatientChangeStatusFragment();
                    break;
            }
        }
    };

    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {

            switch (adapterView.getId()) {
                case R.id.spinner_change_status_appointment:
                    mSelectedAppointmentIndex = adapterView.getSelectedItemPosition();
                    onSelectedAppointmentSpinner();
                    break;

                case R.id.spinner_change_status_assign:
                    mSelectedAssignedIndex = adapterView.getSelectedItemPosition();
                    break;

                case R.id.spinner_change_status_next_status:
                    mSelectedStatusIndex = adapterView.getSelectedItemPosition();
                    onSelectedNextStatusSpinner();
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            // Set Image Color as per the theme applied
            mImageNextStatus.setColorFilter(Common.setThemeColor(mActivity));
            mImageAppointment.setColorFilter(Common.setThemeColor(mActivity));
            mImageAssigned.setColorFilter(Common.setThemeColor(mActivity));

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerAssigned.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerAppointment.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerNextStatus.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

            mEditCurrentStatus.setKeyListener(null);
            mEditAssignName.setKeyListener(null);
            mEditPatient.setKeyListener(null);
            mEditAppointmentNo.setKeyListener(null);


            if (!mPatientName.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                mEditPatient.setText(mPatientName);
            }
            if (!mCurrentStatus.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                mEditCurrentStatus.setText(mCurrentStatus);
            }
            if (!mAppointmentNo.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                mEditAppointmentNo.setText(mAppointmentNo);
            }
            if (!mAssigned.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                mEditAssignName.setText(mAssigned);
            }

            mButtonSubmit.setVisibility(View.VISIBLE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsAppointmentSpinner() {
        try {
            mArrAppointment.add(mActivity.getResources().getString(R.string.spinner_select_appointment));

            if (mArrAppointmentList.size() > 0) {
                for (int i = 0; i < mArrAppointmentList.size(); i++) {
                    mArrAppointment.add(mArrAppointmentList.get(i).getTicketNumber());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerAppointment.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrAppointment) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedAppointmentIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerAppointment.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the value for current Status and will assign name when selecting appointment spinner
     */
    private void onSelectedAppointmentSpinner() {
        if (mSelectedAppointmentIndex != 0) {
            mLinearAssignName.setVisibility(View.VISIBLE);
            mLinearCurrentStatus.setVisibility(View.VISIBLE);
            mEditAssignName.setText(mArrAppointmentList.get(mSelectedAppointmentIndex - 1).getAsigneeName());
            mEditCurrentStatus.setText(mArrAppointmentList.get(mSelectedAppointmentIndex - 1).getCurrentStatus());

            if (mArrNextStatus.size() > 0) {
                mArrNextStatus.clear();
                mArrNextStatusList.clear();
            }

            if (mAdapterChangeStatus != null) {
                mAdapterChangeStatus.notifyDataSetChanged();
            }
            //callToNextStatusAPI();
        } else {
            mLinearAssignName.setVisibility(View.GONE);
            mLinearCurrentStatus.setVisibility(View.GONE);
            mLinearNextStatus.setVisibility(View.GONE);
            mLinearAssigned.setVisibility(View.GONE);

            mEditAssignName.setText(AppConstants.STR_EMPTY_STRING);
            mEditCurrentStatus.setText(AppConstants.STR_EMPTY_STRING);

            if (mArrNextStatus.size() > 0) {
                mArrNextStatus.clear();
                mArrNextStatusList.clear();
            }

            if (mAdapterChangeStatus != null) {
                mAdapterChangeStatus.notifyDataSetChanged();
            }
        }
    }

    /**
     * This method should call the Next Status API for Change Status
     */
    private void callToNextStatusAPI() {
        try {
            //String appointmentId = mArrAppointmentList.get(mSelectedAppointmentIndex - 1).getAppointmentID();

            String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setStatusListJson(mAppointmentID,hospital_database));

            Call<ChangeStatusModel> call = RetrofitClient.createService(ApiInterface.class).getStatusComboBox(requestBody);
            call.enqueue(new Callback<ChangeStatusModel>() {
                @Override
                public void onResponse(@NonNull Call<ChangeStatusModel> call, @NonNull Response<ChangeStatusModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                ArrayList<ChangeStatusModel> changeStatuses = new ArrayList<>(Arrays
                                        .asList(new GsonBuilder().serializeNulls().create()
                                                .fromJson(new JsonParser().parse(jsonObject.getString(WebFields.DATA)).
                                                        getAsJsonArray(), ChangeStatusModel[].class)));

                                if (mArrNextStatusList != null && mArrNextStatusList.size() > 0) {
                                    mArrNextStatusList.addAll(changeStatuses);
                                } else {
                                    mArrNextStatusList = changeStatuses;
                                }
                                setsNextStatusAdapter();
                            } else {
                                setsNextStatusAdapter();
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                            setsNextStatusAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ChangeStatusModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setsNextStatusAdapter();
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsNextStatusAdapter() {
        try {
            mLinearNextStatus.setVisibility(View.VISIBLE);
            mArrNextStatus.add(mActivity.getResources().getString(R.string.spinner_select_next_status));

            if (mArrNextStatusList.size() > 0) {
                for (int i = 0; i < mArrNextStatusList.size(); i++) {
                    mArrNextStatus.add(mArrNextStatusList.get(i).getTitle());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerNextStatus.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mAdapterChangeStatus = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrNextStatus) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedStatusIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            mAdapterChangeStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerNextStatus.setAdapter(mAdapterChangeStatus);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the value for next status and will assign value when selecting next status spinner
     */
    private void onSelectedNextStatusSpinner() {
        try {
            if (mSelectedStatusIndex != 0) {
                String mUserType = AppConstants.STR_EMPTY_STRING;
                String mSelectedValue = mArrNextStatusList.get(mSelectedStatusIndex - 1).getTitle();

                if (mSelectedValue.contains(mActivity.getResources().getString(R.string.user_type_doctor))) {
                    mUserType = mActivity.getResources().getString(R.string.user_type_doctor);
                }

                if (mSelectedValue.contains(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                    mUserType = mActivity.getResources().getString(R.string.user_type_optometrist);
                }

                if (mSelectedValue.contains(mActivity.getResources().getString(R.string.user_type_councillor))) {
                    mUserType = mActivity.getResources().getString(R.string.user_type_councillor);
                }

                if (mSelectedValue.contains(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                    mUserType = mActivity.getResources().getString(R.string.user_type_receptionist);
                }

                if (mArrUser.size() > 0) {
                    mArrUser.clear();
                    mArrUserList.clear();
                }
                callToUserTypeToUserAPI(mUserType);
            } else {
                if (mArrUser.size() > 0) {
                    mArrUser.clear();
                    mArrUserList.clear();
                }
                mLinearAssigned.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the UserType To User API for Change Status
     *
     * @param mUserType - User Type
     */
    private void callToUserTypeToUserAPI(String mUserType) {
        try {

            String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setUserByUserTypeJson(mUserType,hospital_database));

            Call<UserByUserTypeModel> call = RetrofitClient.createService(ApiInterface.class).getUserByUserType(requestBody);
            call.enqueue(new Callback<UserByUserTypeModel>() {
                @Override
                public void onResponse(@NonNull Call<UserByUserTypeModel> call, @NonNull Response<UserByUserTypeModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                ArrayList<UserByUserTypeModel> changeStatuses = new ArrayList<>(Arrays
                                        .asList(new GsonBuilder().serializeNulls().create()
                                                .fromJson(new JsonParser().parse(jsonObject.getString(WebFields.DATA)).
                                                        getAsJsonArray(), UserByUserTypeModel[].class)));

                                if (mArrUserList != null && mArrUserList.size() > 0) {
                                    mArrUserList.addAll(changeStatuses);
                                } else {
                                    mArrUserList = changeStatuses;
                                }
                                setsUserTypeToUserAdapter(mUserType);
                            } else {
                                setsUserTypeToUserAdapter(mUserType);
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                            setsUserTypeToUserAdapter(mUserType);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<UserByUserTypeModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setsUserTypeToUserAdapter(mUserType);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsUserTypeToUserAdapter(String userType) {
        try {
            if (userType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                mArrUser.add(mActivity.getResources().getString(R.string.spinner_select_doctor));
            }

            if (userType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                mArrUser.add(mActivity.getResources().getString(R.string.spinner_select_optometrist));
            }

            if (userType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                mArrUser.add(mActivity.getResources().getString(R.string.spinner_select_councillor));
            }

            if (userType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                mArrUser.add(mActivity.getResources().getString(R.string.spinner_select_receptionist));
            }

            if (mArrUserList.size() > 0) {
                for (int i = 0; i < mArrUserList.size(); i++) {
                    mArrUser.add(mArrUserList.get(i).getName());
                }
            }

            mLinearAssigned.setVisibility(View.VISIBLE);

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerAssigned.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrUser) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedAssignedIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerAssigned.setAdapter(adapter);

            //This is Selected Position
            if (mArrUserList.size() > 0) {
                for (int i = 0; i < mArrUserList.size(); i++) {
                    if (mEditAssignName.getText().toString().
                            equalsIgnoreCase(mArrUserList.get(i).getName())){
                       mSpinnerAssigned.setSelection(i+1);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {
        boolean result = true;


        if (mSelectedStatusIndex == 0) {
            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.error_select_next_status));
            result = false;

        }
        int selected = mSpinnerAssigned.getSelectedItemPosition();
        if (selected == 0) {
            String selectValue = mArrNextStatusList.get(mSelectedStatusIndex - 1).getTitle();

            if (selectValue.contains(mActivity.getResources().getString(R.string.user_type_doctor))) {
                Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.error_select_doctor));
            }

            if (selectValue.contains(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.error_select_optometrist));
            }

            if (selectValue.contains(mActivity.getResources().getString(R.string.user_type_councillor))) {
                Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.error_select_councillor));
            }

            if (selectValue.contains(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.error_select_receptionist));
            }
            result = false;
        }
        return result;
    }

    /**
     * This method should call the change status API
     */
    private void doChangeStatus() {
        try {
            int pos = mSpinnerAssigned.getSelectedItemPosition();
            String mNextStatusId = mArrNextStatusList.get(mSelectedStatusIndex - 1).getStatusID();
            String mActionUserId = mArrUserList.get(pos - 1).getUserID();
            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
            String mTimeDifference = "20";

            String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setChangeStatusJson(mAppointmentID, mNextStatusId, mActionUserId, mTimeDifference, mPatientID, mUserId,hospital_database));

            Call<AddChangeStatusModel> call = RetrofitClient.createService(ApiInterface.class).doChangeStatus(requestBody);
            call.enqueue(new Callback<AddChangeStatusModel>() {
                @Override
                public void onResponse(@NonNull Call<AddChangeStatusModel> call, @NonNull Response<AddChangeStatusModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        if (response.isSuccessful() && response.body().getError() == 200) {
                            Common.setCustomToast(mActivity, response.body().getMessage());
                            String mId = response.body().getData().get(0).getID();
                            removeAllFragments();
                            redirectedToRoleWiseChangeStatusFragment();
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddChangeStatusModel> call, @NonNull Throwable t) {
                    Common.insertLog(t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * This method redirected you to the Role Wise Change Status Fragment
     */
    private void redirectedToRoleWiseChangeStatusFragment() {
        try {

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_change_status));
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_change_status));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_change_status));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_change_status));
            }

            String mNextStatusId = mArrNextStatusList.get(mSelectedStatusIndex - 1).getTitle();
            Bundle bundle=new Bundle();
            bundle.putString(AppConstants.BUNDLE_CHANGE_STATUS,mNextStatusId);

            Fragment fragment = new RoleWiseChangeStatusFragment();
            fragment.setArguments(bundle);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_patient_appointment))
                    .addToBackStack(null)
                    .commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Used to remove all fragment in the fragment manager after logout
     */
    private void removeAllFragments() {
        try {
            List<Fragment> fragments = null;

            if (mUserType.equalsIgnoreCase(getString(R.string.user_type_receptionist))) {
                fragments = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_councillor))) {
                fragments = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
                fragments = ((OptometristHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                fragments = ((DoctorHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_patient))) {
                fragments = ((PatientHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            }

            if (fragments.size() > 0) {
                for (int i = 0; i < fragments.size(); i++) {
                    if (fragments.get(i) != null) {

                        if (mUserType.equalsIgnoreCase(getString(R.string.user_type_receptionist))) {
                            ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i));
                        } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_councillor))) {
                            ((CouncillorHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i));
                        } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
                            ((OptometristHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i));
                        } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                            ((DoctorHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i));
                        } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_patient))) {
                            ((PatientHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i));
                        }
                    }
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Search Patient Change Status Fragment
     */
    private void callToSearchPatientChangeStatusFragment() {
        try {
            String mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);
            Fragment fragment = new SearchPatientChangeStatusFragment();
            FragmentManager fragmentManager = null;

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_patient));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_patient));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_patient));
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_search_change_status_patient))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_search_change_status_patient))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}