package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.ViewPagerAdapter;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.model.ReceptionistAppointmentModel;
import com.trackopd.model.ReceptionistPatientModel;
import com.trackopd.model.SurgeryListModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class AddSurgeryFragment extends Fragment {


    public static ViewPager mViewPager;
    public static ArrayList<PreliminaryExaminationDetailsModel.Data.Eye> mArrEyeList;
    public static ArrayList<PreliminaryExaminationDetailsModel.Data.SurgeryType> mArrSurgeryType;
    public Bundle mBundle;
    private View mView;
    private Activity mActivity;
    private TabLayout mTabLayout;
    private ViewPagerAdapter mAdapter;
    private ArrayList<String> mArrTabsName;
    private TextView mTextName, mTextMobile, mTextAppointmentNo;
    private ImageView mImageProfilePic, mImageAppointment;
    private ReceptionistPatientModel mReceptionPatient;
    private RelativeLayout mRelativeImageBorder;
    private ReceptionistAppointmentModel Appointment;
    private String FirstName = "", LastName = "", Mobile = "", PatientCode = "", AppointmentNo = "", AppointmentID = "",
            mProfilePic = "", mTreatmentDate = "";

    private SurgeryListModel mSurgeryDetail;

    /**
     * Set Current page
     *
     * @param page - Page Index
     */
    public static void setPage(int page) {
        mViewPager.setCurrentItem(page);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //return inflater.inflate(R.layout.fragment_add_surgery, container, false);
        mView = inflater.inflate(R.layout.fragment_add_surgery, container, false);

        mActivity = getActivity();
        mArrTabsName = new ArrayList<>();
        mArrEyeList = new ArrayList<>();
        mArrSurgeryType = new ArrayList<>();

        getBundle();
        getIds();
        setData();
        setRegListeners();
        setSurgeryFragment();
        return mView;
    }

    /**
     * This Method receive Data from Add Payment Step One Fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                if (bundle.getSerializable(AppConstants.BUNDLE_ADD_SURGERY) != null) {

                    mSurgeryDetail = (SurgeryListModel) bundle.getSerializable(AppConstants.BUNDLE_ADD_SURGERY);

                    FirstName = mSurgeryDetail.getPatientFirstName();
                    LastName = mSurgeryDetail.getPatientLastName();
                    Mobile = mSurgeryDetail.getPatientMobileNo();
                    PatientCode = mSurgeryDetail.getPatientCode();
                    AppointmentNo = mSurgeryDetail.getTicketNumber();
                    AppointmentID = mSurgeryDetail.getAppointmentID();
                    mProfilePic = mSurgeryDetail.getProfileImage();
                    mTreatmentDate = mSurgeryDetail.getSurgeryDate();


                } else {
                    FirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                    LastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                    Mobile = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                    PatientCode = bundle.getString(AppConstants.BUNDLE_PATIENT_CODE);
                    AppointmentNo = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                    AppointmentID = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_ID);
                    mProfilePic = bundle.getString(AppConstants.BUNDLE_PATIENT_PROFILE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {

            mTextAppointmentNo = mView.findViewById(R.id.text_view_add_surgery_appointment_no);
            // Tab Layout
            mTabLayout = mView.findViewById(R.id.tab_layout_add_surgery);

            // View Pager
            mViewPager = mView.findViewById(R.id.view_pager_add_surgery);

            //Text View
            mTextName = mView.findViewById(R.id.text_view_add_surgery_patient_name);
            mTextMobile = mView.findViewById(R.id.text_view_add_surgery_patient_mobile_no);

            //Image View
            mImageProfilePic = mView.findViewById(R.id.image_add_surgery_patient_profile_pic);
            mImageAppointment = mView.findViewById(R.id.image_add_surgery_appointment_no);
            //Relatieve Layout
            mRelativeImageBorder = mView.findViewById(R.id.relative_add_surgery_patient_picture);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    @SuppressLint("SetTextI18n")
    private void setData() {
        try {

            mTextName.setText(FirstName + AppConstants.STR_EMPTY_SPACE + LastName);
            mTextMobile.setText(Mobile);
            mRelativeImageBorder.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.user_image_border));
            mImageProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, FirstName, LastName));
            mTextAppointmentNo.setText(AppointmentNo);

            // Set image to the selected view
            if (mProfilePic != null && !mProfilePic.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                //Set Thumbnail
                String thumbmail_url = WebFields.API_BASE_URL + WebFields.IMAGE_THUMBNAIL_URL + mProfilePic;
                Glide.with(mActivity)
                        .load(thumbmail_url)
                        .apply(new RequestOptions().error(R.drawable.ic_documents_img).placeholder(R.drawable.ic_documents_img))
                        .into(mImageProfilePic);
            } else {
                mImageProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, FirstName, LastName));
            }

            //set Theme Color
            mImageAppointment.setColorFilter(Common.setThemeColor(mActivity));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the bundle data
     */
    private void setBundle() {
        try {
            mBundle = new Bundle();
            mBundle.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, FirstName);
            mBundle.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, LastName);
            mBundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE, Mobile);
            mBundle.putString(AppConstants.BUNDLE_PATIENT_ID, PatientCode);
            mBundle.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, Integer.parseInt(AppointmentID));
            mBundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, "-1");
            mBundle.putString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE, mTreatmentDate);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Surgery Fragment
     */
    private void setSurgeryFragment() {
        setBundle();
        Fragment fragment = new CheckUpSurgeryFragment();
        //fragment.setArguments();
        FragmentManager fragmentManager = getChildFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.frame_add_surgery_container,
                fragment, mActivity.getResources()
                        .getString(R.string.tag_add_surgery))
                .addToBackStack(mActivity.getResources()
                        .getString(R.string.back_stack_add_surgery))
                .commit();
    }

    /**
     * Sets up the view pager and binding the tab layouts
     */
    private void setViewPager() {
        try {

            Bundle args = new Bundle();
            //set Put Bundle Data
            args.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, FirstName);
            args.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, LastName);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, Mobile);
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, PatientCode);
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, AppointmentNo);
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_ID, AppointmentID);


            if (mSurgeryDetail != null) {
                args.putSerializable(AppConstants.BUNDLE_ADD_SURGERY, mSurgeryDetail);
            }

            mArrTabsName.addAll(Arrays.asList(getResources().getStringArray(R.array.array_surgery)));

            String mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }


            ViewPagerAdapter adapter = new ViewPagerAdapter(fragmentManager);

            adapter.addFragment(new SurgeryNotesSurgeryFragment(), mArrTabsName.get(0), args);
            adapter.addFragment(new AnaesthesiaSurgeryFragment(), mArrTabsName.get(1), args);
            adapter.addFragment(new ImplantSurgeryFragment(), mArrTabsName.get(2), args);
            adapter.addFragment(new AddMediclaimFragment(), mArrTabsName.get(3), args);


            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                mAdapter = new ViewPagerAdapter(((DoctorHomeActivity) mActivity).getSupportFragmentManager());
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                mAdapter = new ViewPagerAdapter(((CouncillorHomeActivity) mActivity).getSupportFragmentManager());
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                mAdapter = new ViewPagerAdapter(((ReceptionistHomeActivity) mActivity).getSupportFragmentManager());
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                mAdapter = new ViewPagerAdapter(((OptometristHomeActivity) mActivity).getSupportFragmentManager());
            }

            mViewPager.setAdapter(adapter);
            mTabLayout.setupWithViewPager(mViewPager);
            mViewPager.setOffscreenPageLimit(1);
            mViewPager.setCurrentItem(0);

            mTabLayout.setSelectedTabIndicatorColor(Common.setThemeColor(mActivity));
            mTabLayout.setTabTextColors(mActivity.getResources().getColor(R.color.colorBlack), Common.setThemeColor(mActivity));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            //  mTabLayout.setOnTabSelectedListener(onTabSelectedListener(mViewPager));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Tab Select Listener
     *
     * @param mViewPager - View Pager
     * @return - Return Tabs Selected
     */
    private TabLayout.BaseOnTabSelectedListener onTabSelectedListener(ViewPager mViewPager) {
        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }


    /**
     * get Preliminary Examination
     */
   /* private void callToGetPreliminaryID() {

        String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);
        String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);

        String convert_yyy_mm_dd=Common.convertDateUsingDateFormat(mActivity,mTreatmentDate,
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen),
                mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));

        RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                APICommonMethods.setAddPreliminaryBasicDetailsJson(convert_yyy_mm_dd,
                        AppointmentID, ,mUserId,mDoctorId,hospital_database));

        Call<AddAppointmentModel> call = RetrofitClient.createService(ApiInterface.class).setAddPreliminaryBasicDetails(requestBody);
        call.enqueue(new Callback<AddAppointmentModel>() {
            @Override
            public void onResponse(Call<AddAppointmentModel> call, Response<AddAppointmentModel> response) {
                try{
                    if (response.isSuccessful() && response.body().getError()==200)
                    {
                        if (response.body().getData()!=null
                                && response.body().getData().size()>0){
                            PreliminaryExminationId=response.body().getData().get(0).getID();
                        }else {
                            Common.insertLog(response.body().toString());
                        }
                    }
                }catch (Exception e){
                    Common.insertLog(e.getMessage());
                }
            }
            @Override
            public void onFailure(Call<AddAppointmentModel> call, Throwable t) {
                Common.insertLog("Failure:::> " + t.getMessage());
                Common.setCustomToast(mActivity, t.getMessage());
            }
        });
    }*/


}
