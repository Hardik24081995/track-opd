package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.gson.Gson;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.model.AddMediclaimModel;
import com.trackopd.model.ReceptionistAppointmentModel;
import com.trackopd.model.ReceptionistPatientModel;
import com.trackopd.model.SurgeryListModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddMediclaimFragment extends Fragment {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private EditText mEditCompany,mEditTPA,mEditPolicyNo,mEditExpiryDate,mEditSettlementAmount;
    private RadioGroup mRadioGroupSettlementReceived;
    private Button mButtonSubmit;
    private boolean isMedicallaim=false;
    private ReceptionistPatientModel mReceptionPatient;
    private ReceptionistAppointmentModel Appointment;
    private LinearLayout mLinearMediclaimAmount;
    private String strCompany,strTPA,strPolicyNo,strExpireDate,strSettlement,strSettlementAmount,
            mUserType,AppointmentID;
    private AppUtil mAppUtils;
    private SurgeryListModel mSurgeryDetail;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_add_mediclaim, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mAppUtils=new AppUtil(mActivity);
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        getBundle();
        getIds();
        setRegListeners();
        setData();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);

        MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(false);
    }

    /**
     * This Method get Bundle Value can be pass
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle.getSerializable(AppConstants.BUNDLE_ADD_SURGERY)!=null) {
                mSurgeryDetail = (SurgeryListModel) bundle.getSerializable(AppConstants.BUNDLE_ADD_SURGERY);
                AppointmentID = mSurgeryDetail.getAppointmentID();
            }else {
                AppointmentID=bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_ID);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            //Edit Text
            mEditCompany=mView.findViewById(R.id.edit_text_add_mediclaim_company);
            mEditTPA=mView.findViewById(R.id.edit_text_add_mediclaim_tpa);
            mEditPolicyNo=mView.findViewById(R.id.edit_text_add_mediclaim_policy_no);
            mEditExpiryDate=mView.findViewById(R.id.edit_text_add_mediclaim_expirty_date);
            mEditSettlementAmount=mView.findViewById(R.id.edit_text_add_mediclaim_expirty_date);

            // Linear Layout
            mLinearMediclaimAmount=mView.findViewById(R.id.linear_add_mediclaim_settlement_amount);

            //Radio Group
            mRadioGroupSettlementReceived=mView.findViewById(R.id.radio_group_add_mediclaim_settlement_received);

            //Button
            mButtonSubmit=mView.findViewById(R.id.button_add_surgery_medication_submit);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Edit Click Listeners
            mEditExpiryDate.setOnClickListener(clickListener);
            mButtonSubmit.setOnClickListener(clickListener);

            // ToDo: Radio Button Click Listeners
            mRadioGroupSettlementReceived.setOnCheckedChangeListener(checkedChangeListener);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            mEditExpiryDate.setText(Common.setCurrentDate(mActivity));

            if (mSurgeryDetail!=null){
                mEditCompany.setText(mSurgeryDetail.getCompany());
                mEditTPA.setText(mSurgeryDetail.getTPA());
                mEditPolicyNo.setText(mSurgeryDetail.getPolicyNo());

                //Set Expire Date
                if (mSurgeryDetail.getExpiryDate()!=null && mSurgeryDetail.getExpiryDate().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                    String date=Common.convertDateUsingDateFormat(mActivity,mSurgeryDetail.getExpiryDate(),
                            mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen),
                            mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));
                    mEditExpiryDate.setText(date);
                }else {
                    mEditExpiryDate.setText(Common.setCurrentDate(mActivity));
                }

                // set Settlement Received
                if (mSurgeryDetail.getSettlementReceived().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                    isMedicallaim=false;
                    mRadioGroupSettlementReceived.check(R.id.radio_button_add_mediclaim_settlement_received_no);
                }else {
                    isMedicallaim=true;
                    mRadioGroupSettlementReceived.check(R.id.radio_button_add_mediclaim_settlement_received_yes);
                }

                //set Settlement Amount
                mEditSettlementAmount.setText(mSurgeryDetail.getSettlementAmount());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Checked Change listeners (Radio Button)
     */
    private RadioGroup.OnCheckedChangeListener checkedChangeListener = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            RadioButton mRadioButton = group.findViewById(checkedId);
            mRadioButton.clearFocus();

            switch (mRadioButton.getId()) {
                case R.id.radio_group_add_mediclaim_settlement_received:
                    int selId = group.getCheckedRadioButtonId();
                    if (selId == 1) {
                        isMedicallaim = true;
                        mLinearMediclaimAmount.setVisibility(View.GONE);
                    } else if (selId == 2) {
                        isMedicallaim = false;
                        mLinearMediclaimAmount.setVisibility(View.VISIBLE);
                    }
                    break;
            }
        }
    };

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.edit_text_add_mediclaim_expirty_date:
                    Common.openDatePicker(mActivity, mEditExpiryDate);
                    break;
                case R.id.button_add_surgery_medication_submit:
                       if (checkMediclaimValidation()){
                           callToAddMediclaim();
                       }
                    break;
            }
        }
    };

    /**
     * Call To Add Mediclaim on Surgery
     */
    private void callToAddMediclaim() {
       try{
         if (!mAppUtils.getConnectionState()){
            Common.setCustomToast(mActivity,mActivity.getResources().getString(R.string.no_internet_connection));
         }else {

             String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
             String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);


             String strSettlementReceived=AppConstants.STR_EMPTY_STRING;
             if (isMedicallaim){
                 strSettlementReceived="Yes";
             }else {
                 strSettlementReceived="No";
             }
             RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                     APICommonMethods.setAddMediclaimJson(mUserId, mDatabaseName,AppointmentID,strCompany,strTPA,strPolicyNo,
                             strExpireDate,strSettlementReceived,strSettlementAmount, ""));

             Call<AddMediclaimModel> call = RetrofitClient.createService(ApiInterface.class).AddMediclaim(requestBody);
             call.enqueue(new Callback<AddMediclaimModel>() {
                 @Override
                 public void onResponse(@NonNull Call<AddMediclaimModel> call, @NonNull Response<AddMediclaimModel> response) {

                     Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                     try {
                         String mJson = (new Gson().toJson(response.body()));
                         JSONObject jsonObject = new JSONObject(mJson);
                         String mMessage = jsonObject.getString(WebFields.MESSAGE);
                         int mError = jsonObject.getInt(WebFields.ERROR);
                         Common.insertLog("mMessage:::> " + mMessage);

                         if (response.isSuccessful() && mError == 200) {
                             if (response.body() != null) {
                                 removeAllFragments();
                                 callToMediclaimList();
                                 //AddSurgeryFragment.setPage(1);

                             }
                         }
                     } catch (JSONException e) {
                         e.printStackTrace();
                     }
                 }

                 @Override
                 public void onFailure(@NonNull Call<AddMediclaimModel> call, @NonNull Throwable t) {
                     Common.insertLog("Failure:::> " + t.getMessage());
                 }
             });


         }
       }catch (Exception e){
          Common.insertLog(e.getMessage());
       }
    }

    private void callToMediclaimList() {
        try {
            Fragment fragment = new SurgeryListFragment();
            Bundle args = new Bundle();

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
            }

            args.putString(AppConstants.BUNDLE_PATIENT_NAME, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_FOLLOW_UP_DATE, "1970-01-01");
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, -1);
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_COUNCILLOR, -1);
            //fragment.setArguments(args);

            FragmentManager fragmentManager = null;

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.nav_menu_surgery))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Check Validation for Mediclaim
     * @return - return True /false
     */
    private boolean checkMediclaimValidation() {
       boolean status=true;

       strCompany=mEditCompany.getText().toString().trim();
       strTPA=mEditTPA.getText().toString().trim();
       strPolicyNo=mEditPolicyNo.getText().toString().trim();
       String strDate=mEditExpiryDate.getText().toString().trim();
       strExpireDate=Common.convertDateUsingDateFormat(mActivity,strDate,
                             mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                             mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));

       mEditCompany.setError(null);
       mEditTPA.setError(null);
       mEditPolicyNo.setError(null);
       mEditExpiryDate.setError(null);

       if (strCompany==null || strCompany.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
          status=false;
          mEditCompany.setError(mActivity.getResources().getString(R.string.error_field_required));
       }

       if (strTPA==null || strTPA.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
           status=false;
           mEditTPA.setError(mActivity.getResources().getString(R.string.error_field_required));
       }

       if (strPolicyNo==null || strPolicyNo.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
           status=false;
           mEditPolicyNo.setError(mActivity.getResources().getString(R.string.error_field_required));
       }

       if (strExpireDate==null || strExpireDate.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
           status=false;
           mEditExpiryDate.setError(mActivity.getResources().getString(R.string.error_field_required));
       }

       if (isMedicallaim){
          strSettlementAmount=mEditSettlementAmount.getText().toString().trim();
       }else {
           strSettlementAmount="0";
       }
       return status;
    }

    /**
     * Used to remove all fragment in the fragment manager after logout
     */
    private void removeAllFragments() {
        try {
            List<Fragment> fragments = null;

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragments = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist)))
            {
                fragments = ((OptometristHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragments = ((DoctorHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragments = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            }


            if (fragments.size() > 0) {
                for (int i = 0; i < fragments.size(); i++) {
                    if (fragments.get(i) != null) {

                        if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                            ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                            ((OptometristHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                            ((DoctorHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                            ((CouncillorHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}