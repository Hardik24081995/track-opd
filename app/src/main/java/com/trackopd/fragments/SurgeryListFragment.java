package com.trackopd.fragments;



import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.github.badoualy.datepicker.DatePickerTimeline;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.SurgeryListAdapter;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.CheckInListModel;
import com.trackopd.model.SurgeryListModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.utils.RecyclerProgressView;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SurgeryListFragment extends Fragment {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    public Bundle mBundle;
    private String mName = "", mPatientCode = "", mMobileNo = "", mMRDNo = "", mAppointmentID = "",
            mSurgerytDate = "0000-00-00",
            mUserType, mAddAppointment, mAppointmentStatus, mStatus,Name,Phone;
    private int mDoctorId = -1;
    private Boolean isStarted = false, isVisible = false, isFirstTime = true;
    private DatePickerTimeline mDatePickerTimeLine;
    private Date mSelectedDate, mCurrentDate;
    private AppUtil mAppUtils;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshView;
    private RelativeLayout mRelativeNoData, mRelativeNoInternet;
    private Button mButtonRetry;
    // Load More Listener Variables
    private int currentPageIndex = 1, lastFetchRecord = 0, totalRecords = 0;
    private boolean isRefresh = false, isLoadMore = false;
    private RadioGroup mRadioGroupType;
    private RadioButton mRadioPending, mRadioOngoing, mRadioCompleted, mRadioCancelled, mRadioAll;
    private SurgeryListAdapter mSurgeryListAdapter;
    
    /// Surgery Array List
    private ArrayList<SurgeryListModel> mArrSurgery;

    private ArrayList<CheckInListModel.Data> mArrCheckIn;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_receptionist_appointment, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        mAppUtils = new AppUtil(getActivity());
        
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);
        mArrSurgery=new ArrayList<>();
        mArrCheckIn=new ArrayList<>();
        
        getBundle();
        getIds();
        setRegListeners();
        setData();
        getCurrentDate();
        //callShimmerView();

        setHasOptionsMenu(true);
        return mView;
    }


    /**
     * Set the prepare menu options
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_appointment, menu);

        final MenuItem menuSearch = menu.findItem(R.id.action_appointment_search);
        menuSearch.setVisible(true);

        MenuItem menuAdd = menu.findItem(R.id.action_appointment_add);

        menuAdd.setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_appointment_add:
                callToAddSurgeryFragment();
                return true;

            case R.id.action_appointment_search:
                callToSearchAppointmentFragment();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                Name = bundle.getString(AppConstants.BUNDLE_PATIENT_NAME);
                mPatientCode = bundle.getString(AppConstants.BUNDLE_PATIENT_CODE);
                Phone = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
               /* mMRDNo = bundle.getString(AppConstants.BUNDLE_MRD_NO);
                mAppointmentID = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mSurgerytDate = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DATE);*/
                mDoctorId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);
               // mStatus = bundle.getString(AppConstants.BUNDLE_APPOINTMENT_STATUS);
                Common.insertLog("Status:::> " + mSurgerytDate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Date Picker Time Line
            mDatePickerTimeLine = mView.findViewById(R.id.date_picker_time_line_book_appointment);

            // Tab Layout
            mTabLayout = mView.findViewById(R.id.tab_layout_receptionist_appointment);

            // View Pager
            mViewPager = mView.findViewById(R.id.view_pager_receptionist_appointment);

            // Radio Group
            mRadioGroupType = mView.findViewById(R.id.radio_group_receptionist_appointment_type);

            // Radio Buttons
            mRadioPending = mView.findViewById(R.id.radio_button_receptionist_appointment_pending);
            mRadioOngoing = mView.findViewById(R.id.radio_button_receptionist_appointment_ongoing);
            mRadioCompleted = mView.findViewById(R.id.radio_button_receptionist_appointment_completed);
            mRadioCancelled = mView.findViewById(R.id.radio_button_receptionist_appointment_cancelled);
            mRadioAll = mView.findViewById(R.id.radio_button_receptionist_appointment_all);

            // Recycler View
            mRecyclerView = mView.findViewById(R.id.recycler_view);

            // Swipe Refresh View
            mSwipeRefreshView = mView.findViewById(R.id.swipe_refresh_view);

            // Relative Layouts
            mRelativeNoData = mView.findViewById(R.id.relative_no_data_available);
            mRelativeNoInternet = mView.findViewById(R.id.relative_no_internet);

            // Buttons
            mButtonRetry = mView.findViewById(R.id.button_retry);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {

            // ToDo: Date Selected Listener
            mDatePickerTimeLine.setOnDateSelectedListener(dateSelectedListener);
            mButtonRetry.setOnClickListener(clickListener);

            // ToDo: Set On Refresh Listeners
            mSwipeRefreshView.setOnRefreshListener(refreshListener);

            mRadioGroupType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @SuppressLint("ResourceType")
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    RadioButton rb = group.findViewById(checkedId);
                    if (null != rb && checkedId > -1) {

                        if (rb.getText().toString().equalsIgnoreCase(mActivity.getResources().getString(R.string.text_cancelled))) {
                            mAppointmentStatus = mActivity.getResources().getString(R.string.text_reject);
                        } else if (rb.getText().toString().equalsIgnoreCase(mActivity.getResources().getString(R.string.text_on_going))) {
                            mAppointmentStatus = mActivity.getResources().getString(R.string.text_ongoing);
                        } else {
                            mAppointmentStatus = rb.getText().toString();
                        }

                        if (mArrCheckIn.size() > 0) {
                            mArrCheckIn.clear();
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            if (mStatus == null) {
                mAppointmentStatus = mActivity.getResources().getString(R.string.status_pending);
            } else {
                if (mStatus.equalsIgnoreCase(mActivity.getResources().getString(R.string.status_pending))) {
                    mRadioPending.setChecked(true);
                    mAppointmentStatus = mActivity.getResources().getString(R.string.status_pending);
                } else if (mStatus.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_ongoing))) {
                    mRadioOngoing.setChecked(true);
                    mAppointmentStatus = mActivity.getResources().getString(R.string.text_ongoing);
                } else if (mStatus.equalsIgnoreCase(mActivity.getResources().getString(R.string.status_completed))) {
                    mRadioCompleted.setChecked(true);
                    mAppointmentStatus = mActivity.getResources().getString(R.string.status_completed);
                } else if (mStatus.equalsIgnoreCase(mActivity.getResources().getString(R.string.status_completed))) {
                    mRadioCompleted.setChecked(true);
                    mAppointmentStatus = mActivity.getResources().getString(R.string.status_completed);
                } else {
                    mRadioCancelled.setChecked(true);
                    mAppointmentStatus = mActivity.getResources().getString(R.string.tab_all);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_retry:
                    callShimmerView();
                    break;
            }
        }
    };

    /**
     * Set On Refresh Listeners
     */
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            mSwipeRefreshView.setRefreshing(false);
            isFirstTime = true;

            if (!mAppUtils.getConnectionState()) {
                mRelativeNoInternet.setVisibility(View.VISIBLE);
                mSwipeRefreshView.setRefreshing(false);
                mArrSurgery.clear();
                setViewVisibility();
            } else {
                mRelativeNoInternet.setVisibility(View.GONE);
                if (!isLoadMore) {
                    isRefresh = true;

                    Name = AppConstants.STR_EMPTY_STRING;
                    mPatientCode = AppConstants.STR_EMPTY_STRING;
                    Phone = AppConstants.STR_EMPTY_STRING;
                    mMRDNo = AppConstants.STR_EMPTY_STRING;
                    mAppointmentID = AppConstants.STR_EMPTY_STRING;
                    mDoctorId = -1;

                     if (mArrCheckIn!=null)
                          mArrCheckIn.clear();

                    currentPageIndex = 1;
                    callShimmerView();

                } else
                    mSwipeRefreshView.setRefreshing(false);
            }
        }
    };

    /**
     * Set Current date in date picker and get Current date
     */
    private void getCurrentDate() {
        Calendar CurrentCalender = Calendar.getInstance();
        if (mSurgerytDate.equalsIgnoreCase(mActivity.getResources().getString(R.string.date_format_first_time))) {
            mDatePickerTimeLine.setFirstVisibleDate(CurrentCalender.get(Calendar.YEAR), 0, 1);
            mDatePickerTimeLine.setSelectedDate(CurrentCalender.get(Calendar.YEAR),
                    CurrentCalender.get(Calendar.MONTH),
                    CurrentCalender.get(Calendar.DAY_OF_MONTH));

            try {
                @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormatter = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
                String strCurrentDate = dateFormatter.format(CurrentCalender.getTime());
                mSelectedDate = dateFormatter.parse(strCurrentDate);
                mCurrentDate = dateFormatter.parse(strCurrentDate);
                @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
                mSurgerytDate = dateFormat.format(mSelectedDate);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            Common.insertLog("Else 1");
            String[] mValue = mSurgerytDate.split("-");
            int mYear = Integer.parseInt(mValue[0]);
            int mMonth = Integer.parseInt(mValue[1]);
            int mDay = Integer.parseInt(mValue[2]);
            mDatePickerTimeLine.setFirstVisibleDate(CurrentCalender.get(Calendar.YEAR), 0, 1);
            mDatePickerTimeLine.setSelectedDate(mYear, mMonth-1, mDay);

            try {
                String mDate = mYear + "-" + mMonth + "-" + mDay;
                @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
                mSelectedDate = df.parse(mDate);
                @SuppressLint("SimpleDateFormat") DateFormat dateFormat = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
                mSurgerytDate = dateFormat.format(mSelectedDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Date Selected Click listener
     */
    private DatePickerTimeline.OnDateSelectedListener dateSelectedListener = new DatePickerTimeline.OnDateSelectedListener() {
        @Override
        public void onDateSelected(int year, int month, int day, int index) {
            String mDate = year + "-" + (month + 1) + "-" + day;
            try {
                @SuppressLint("SimpleDateFormat")
                SimpleDateFormat df = new SimpleDateFormat(
                        mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));

                mSelectedDate = df.parse(mDate);
                DateFormat dateFormat = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
                mSurgerytDate = dateFormat.format(mSelectedDate);

                if (mArrCheckIn.size()>0){
                    mArrCheckIn.clear();
                }

                if (mSurgeryListAdapter!=null){
                    mSurgeryListAdapter.notifyDataSetChanged();
                }
                isFirstTime = true;
                callShimmerView();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    /**
     * Receptionist Upcoming Appointment Fragment on visible
     *
     * @param isVisibleToUser - This can check the fragment is visible when swiping and will return
     *                        the boolean value based on that
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        isVisible = isVisibleToUser;
        if (isStarted && isVisible) {
            isFirstTime = true;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
    }


    /**
     * On load more click listener
     */
    private void setLoadMoreClickListener() {

        mSurgeryListAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
               if (mArrCheckIn!=null && mArrCheckIn.size()>0
                       && mArrCheckIn.size()<totalRecords)
               {
                   if (!mAppUtils.getConnectionState()) {
                       mRelativeNoInternet.setVisibility(View.VISIBLE);
                       mArrCheckIn.clear();
                       setViewVisibility();
                   } else {
                       mRelativeNoInternet.setVisibility(View.GONE);
                       mArrCheckIn.clear();

                       if (mSurgeryListAdapter!=null){
                           mSurgeryListAdapter.notifyDataSetChanged();
                       }

                       currentPageIndex = (mArrCheckIn.size() / Common.getPageSizeForPagination(mActivity)) + 1;
                       callToSurgeryListAPI();
                   }
               }
            }
        });
     }

    /**
     * Sets up the Search Receptionist Appointment Fragment
     */
    private void callToSearchAppointmentFragment() {
        try {

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_patient))) {
                ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            }

            Fragment fragment = new SearchReceptionistCheckUpFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_IS_FROM_SEARCH,mActivity.getString(R.string.nav_menu_surgery));
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_patient))) {
                fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_search_receptionist_surgery))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_search_receptionist_surgery))
                    .commit();

            /*if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_patient))) {
                ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            }

            Fragment fragment = new SearchReceptionistAppointmentFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_ADD_APPOINTMENT, mAddAppointment);
            args.putString(AppConstants.BUNDLE_APPOINTMENT_STATUS, mAppointmentStatus);

            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_patient))) {
                fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_search_surgury))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_search_surgury))
                    .commit();*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Receptionist Existing Appointment Fragment
     */
    private void callToAddSurgeryFragment() {
        try {
            
            Fragment fragment = new SearchReceptionistPatientFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_ADD_PATIENT, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_IS_FROM_SEARCH, AppConstants.BUNDLE_ADD_SURGERY);
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_patient))) {
                fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
                ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();

                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.header_search));
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_search_receptionist_patient))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_search_receptionist_patient))
                    .commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is used to call the shimmer effect for recycler view before calling the API
     */
    private void callShimmerView() {
        if (!mAppUtils.getConnectionState()) {
            mRelativeNoInternet.setVisibility(View.VISIBLE);
            //mArrSurgery.clear();
            mArrCheckIn.clear();
            setViewVisibility();
        } else {
            mRelativeNoInternet.setVisibility(View.GONE);
            if (isFirstTime) {
                showProgressView();
                isFirstTime = false;
            }
            callToSurgeryListAPI();
        }
    }

    /**
     * Surgery List API
     */
    private void callToSurgeryListAPI() {
        try {

            if (mAppointmentID.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                mAppointmentID="-1";
            }
            if (Name==null){
                Name=AppConstants.STR_EMPTY_STRING;
            }
            if (Phone==null){
                Phone=AppConstants.STR_EMPTY_STRING;
            }

            String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            String type="Surgery";
            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setCheckInListJson(currentPageIndex,Name,Phone,mSurgerytDate,hospital_database, type));

            Call<CheckInListModel> call = RetrofitClient.createService(ApiInterface.class).getCheckInList(requestBody);
            call.enqueue(new Callback<CheckInListModel>() {
                @Override
                public void onResponse(@NonNull Call<CheckInListModel> call, @NonNull Response<CheckInListModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        stopProgressView();
                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                ArrayList<CheckInListModel.Data> paymentModel = new ArrayList<>(Arrays
                                        .asList(new GsonBuilder().serializeNulls().create()
                                                .fromJson(new JsonParser().parse(jsonObject.getString(WebFields.DATA)).
                                                        getAsJsonArray(), CheckInListModel.Data[].class)));

                                if (mArrCheckIn != null && mArrCheckIn.size() > 0 &&
                                        mSurgeryListAdapter != null) {
                                    mArrCheckIn.addAll(paymentModel);
                                    mSurgeryListAdapter.notifyDataSetChanged();
                                    lastFetchRecord = mArrCheckIn.size();
                                } else {
                                    mArrCheckIn = paymentModel;
                                    setAdapterData();
                                    lastFetchRecord = mArrCheckIn.size();
                                    totalRecords = jsonObject.getInt(WebFields.ROW_COUNT);

                                    if (mRecyclerView.getVisibility() == View.GONE) {
                                        stopProgressView();
                                    }
                                }
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                            stopProgressView();
                            setViewVisibility();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CheckInListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());
                    stopProgressView();
                    setViewVisibility();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

          /*  RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setSurgeryListJson(mAppointmentID,mSurgerytDate,hospital_database,currentPageIndex,mName, mPatientCode, mMobileNo));

            Call<SurgeryListModel> call = RetrofitClient.createService(ApiInterface.class).getSurgeryList(requestBody);
            call.enqueue(new Callback<SurgeryListModel>() {
                @Override
                public void onResponse(@NonNull Call<SurgeryListModel> call, @NonNull Response<SurgeryListModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        stopProgressView();
                        if (response.isSuccessful() &&
                               response.body().getError()==AppConstants.API_SUCCESS_ERROR){

                            String mJson = (new Gson().toJson(response.body()));
                            JSONObject jsonObject = new JSONObject(mJson);
                            String mMessage = jsonObject.getString(WebFields.MESSAGE);
                            Common.insertLog("mMessage:::> " + mMessage);

                            ArrayList<SurgeryListModel> surgeryModel = new ArrayList<>(Arrays
                                    .asList(new GsonBuilder().serializeNulls().create()
                                            .fromJson(new JsonParser().parse(jsonObject.getString(WebFields.DATA)).
                                                    getAsJsonArray(), SurgeryListModel[].class)));

                            if (mArrSurgery != null && mArrSurgery.size() > 0 &&
                                    mSurgeryListAdapter != null) {
                                mArrSurgery.addAll(surgeryModel);
                                mSurgeryListAdapter.notifyDataSetChanged();
                                lastFetchRecord = mArrSurgery.size();
                            } else {
                                mArrSurgery = surgeryModel;
                                setAdapterData();
                                lastFetchRecord = mArrSurgery.size();
                                totalRecords = Integer.parseInt(response.body().getRowCount());
                                setLoadMoreClickListener();
                                if (mRecyclerView.getVisibility() == View.GONE) {
                                    stopProgressView();
                                }
                            }
                        }else {
                           // Common.setCustomToast(mActivity, response.body().getMessage());
                            stopProgressView();
                            setViewVisibility(); 
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<SurgeryListModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    stopProgressView();
                    setViewVisibility();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {

            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mGridLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(mGridLayoutManager);

            mSurgeryListAdapter= new SurgeryListAdapter(mActivity,mRecyclerView,mArrCheckIn);
            mRecyclerView.setAdapter(mSurgeryListAdapter);

            setViewVisibility();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the view visibility as per the array size
     */
    private void setViewVisibility() {
        if (mArrCheckIn.size() != 0)
        {
            mRelativeNoData.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mRelativeNoData.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }
    /**
     * Show progress view when page load for the first time
     */
    private void showProgressView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.VISIBLE);
                RecyclerProgressView.startProgress((LinearLayout) mView.findViewById(R.id.linear_add_recycler_progress_view),
                        R.layout.row_surgery_item, getActivity());
            }
        }, 100);
    }
    /**
     * Show progress view when page load for the first time
     */
    private void stopProgressView() {
        try {
            mSwipeRefreshView.setRefreshing(false);
            isRefresh = false;
            isLoadMore = false;
            RecyclerProgressView.stopProgress();
            mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            hideProgressBar();
        }
    }

    /**
     * Hide the progress bar
     */
    private void hideProgressBar() {
        try {
            if (mArrCheckIn != null && mArrCheckIn.size() > 0 &&
                    mArrCheckIn.get(mArrCheckIn.size() - 1) == null) {

                mArrCheckIn.remove(mArrCheckIn.size() - 1);
                mSurgeryListAdapter.notifyItemRemoved(mArrCheckIn.size());

                mSurgeryListAdapter.notifyDataSetChanged();
                mSurgeryListAdapter.setLoaded();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}