package com.trackopd.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.gson.Gson;
import com.trackopd.R;
import com.trackopd.adapter.HistoryPreExaminationAdapter;
import com.trackopd.model.AddPreliminaryExaminationModel;
import com.trackopd.model.CloseFileModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.KeyboardUtility;
import com.trackopd.utils.SessionManager;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PreExaminationFragment extends Fragment {

    public static ArrayList<HistoryPreliminaryExaminationModel.Data> mArrHistory;
    final String[] quarters = new String[]{"Q1", "Q2", "Q3", "Q4"};
    public IAxisValueFormatter formatter = new IAxisValueFormatter() {

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            return quarters[(int) value];
        }
    };
    private View mView;
    private Menu menu;
    private Activity mActivity;
    private EditText mEditRightEyeNCT, mEditRightEyeAT, mEditRightEyePachymetry, mEditRightEyeSchirmer,
            mEditLeftEyeNCT, mEditLeftEyeAT, mEditLeftEyePachymetry, mEditLeftEyeSchirmer;
    private Button mButtonSubmit;
    private boolean isVisible = false, isStarted = false;
    private int mCurrentPage = 1, mAppointmentId, mHistoryDoctorID = -1;
    private String mPatientId, mDoctorId, mTreatmentDate, hospital_database;
    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.button_add_pre_examination_submit:
                    SessionManager manager = new SessionManager(mActivity);
                    String closeFile = manager.getPreferences("ClosingFlags", "");

                    if (closeFile.equalsIgnoreCase("Yes")) {
                        mButtonSubmit.setClickable(false);
                        Toast.makeText(mActivity, "Patient File Closed", Toast.LENGTH_SHORT).show();
                    } else {
                        doAddPreExamination();
                    }

                    break;
            }
        }
    };

    /**
     * @return - Line Data
     */
    public static void getData(LineChart lineChart) {
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        ArrayList<Entry> LeftAct = new ArrayList<>();
        ArrayList<Entry> RightAct = new ArrayList<>();

        final ArrayList<String> xAxisLabel = new ArrayList<>();


        for (int l = 0; l < mArrHistory.size(); l++) {


            float pos = l;

            for (int i = 0; i < mArrHistory.get(l).getPreExamination().size(); i++) {
                float Left = 0;
                if (!mArrHistory.get(l).getPreExamination().get(i).getLNCT()
                        .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {

                    Left = Float.parseFloat(mArrHistory.get(l).getPreExamination().get(i).getLNCT());
                }
                LeftAct.add(new Entry(pos, Left));

                float right = 0;
                if (!mArrHistory.get(l).getPreExamination().get(i).
                        getRNCT().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {

                    right = Float.parseFloat(mArrHistory.get(l).getPreExamination().get(i).getRNCT());
                }
                RightAct.add(new Entry(pos, right));

                xAxisLabel.add(mArrHistory.get(l).getTreatmentDate());

                // Date.add(new Entry(pos,mArrHistory.get(l).getTreatmentDate()));
            }
        }


        LineDataSet d = new LineDataSet(LeftAct, "Left NCT");
        d.setLineWidth(3.0f);
        d.setCircleRadius(5f);

        d.setColor(Color.RED);//
        d.setCircleColor(Color.RED);
        dataSets.add(d);
        d.setAxisDependency(YAxis.AxisDependency.LEFT);

        LineDataSet right = new LineDataSet(RightAct, "Right NCT");
        right.setLineWidth(3.0f);
        right.setCircleRadius(5f);

        right.setColor(Color.GREEN);
        right.setCircleColor(Color.GREEN);
        dataSets.add(right);
        right.setAxisDependency(YAxis.AxisDependency.LEFT);


        lineChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(xAxisLabel));

        LineData data = new LineData(dataSets);
        lineChart.setData(data);

        lineChart.invalidate();
    }

    private static ArrayList<String> labelArrayList() {
        ArrayList<String> arrayList = new ArrayList<>();

        for (int a = 0; a < mArrHistory.size();
             a++) {

            arrayList.add(mArrHistory.get(a).getTreatmentDate());
        }
        return arrayList;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_pre_examination, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        mArrHistory = new ArrayList<>();
        hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

        getBundle();
        getIds();
        setRegListeners();
        callToPrelminarHistoryPreExamination();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mAppointmentId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mDoctorId = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);
                mTreatmentDate = bundle.getString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);

        MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(false);
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Edit Text Views
            mEditRightEyeNCT = mView.findViewById(R.id.edit_add_pre_examination_right_eye_nct);
            mEditRightEyeAT = mView.findViewById(R.id.edit_add_pre_examination_right_eye_at);
            mEditRightEyePachymetry = mView.findViewById(R.id.edit_add_pre_examination_right_eye_pachymetry);
            mEditRightEyeSchirmer = mView.findViewById(R.id.edit_add_pre_examination_right_eye_schirmer);
            mEditLeftEyeNCT = mView.findViewById(R.id.edit_add_pre_examination_left_eye_nct);
            mEditLeftEyeAT = mView.findViewById(R.id.edit_add_pre_examination_left_eye_at);
            mEditLeftEyePachymetry = mView.findViewById(R.id.edit_add_pre_examination_left_eye_pachymetry);
            mEditLeftEyeSchirmer = mView.findViewById(R.id.edit_add_pre_examination_left_eye_schirmer);

            // Buttons
            mButtonSubmit = mView.findViewById(R.id.button_add_pre_examination_submit);

            // Set Request Focus
            mEditRightEyeNCT.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Item Click Listener
            mButtonSubmit.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callToPrelminarHistoryPreExamination() {
        try {
            String defualt_date = "1000-01-01";
            String Mobile = AppConstants.STR_EMPTY_STRING;
            String Type = "PreExamination";

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setHistoryPreliminaryExaminationJson(defualt_date, mCurrentPage, mPatientId, Mobile, mHistoryDoctorID, Type, hospital_database));

            Call<HistoryPreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationHistory(body);
            call.enqueue(new Callback<HistoryPreliminaryExaminationModel>() {
                @Override
                public void onResponse(Call<HistoryPreliminaryExaminationModel> call, Response<HistoryPreliminaryExaminationModel> response) {
                    Common.insertLog("Response History::::> " + new Gson().toJson(response.body()));
                    try {
                        if (response.body() != null &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body().getData().size() > 0) {
                                mArrHistory.addAll(response.body().getData());
                                setTodayData();
                            }
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<HistoryPreliminaryExaminationModel> call, Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Today Preliminary Examination id
     */
    private void setTodayData() {

        String current_date = Common.setCurrentDate(mActivity);
        String convert_current_date = Common.setConvertDateFormat(mActivity,
                current_date, mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen));


        if (mArrHistory.size() > 0) {
            for (int a = 0; a < mArrHistory.size(); a++) {
                if (mArrHistory.get(a).getTreatmentDate().equalsIgnoreCase(convert_current_date)) {
                    if (mArrHistory.get(a).getAppointmentID().equalsIgnoreCase(String.valueOf(mAppointmentId))) {
                        if (mArrHistory.get(a).getPreExamination().size() > 0) {
                            for (int i = 0; i < mArrHistory.get(a).getPreExamination().size(); i++) {
                                addEditPreExamination(mActivity, mArrHistory.get(a).getPreExamination().get(i));
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Set Edot
     *
     * @param mActivity
     * @param preExamination
     */
    private void addEditPreExamination(Activity mActivity, HistoryPreliminaryExaminationModel.PreExamination preExamination) {
        try {
            mEditRightEyeNCT.setText(preExamination.getRNCT());
            mEditLeftEyeNCT.setText(preExamination.getLNCT());
            mEditRightEyeAT.setText(preExamination.getRAT());
            mEditLeftEyeAT.setText(preExamination.getLAT());
            mEditRightEyePachymetry.setText(preExamination.getRPachymetry());
            mEditLeftEyePachymetry.setText(preExamination.getLPachymetry());
            mEditRightEyeSchirmer.setText(preExamination.getRSchirmer());
            mEditLeftEyeSchirmer.setText(preExamination.getLSchirmer());

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * This method checks the validation first then call the API
     */
    private void doAddPreExamination() {
        KeyboardUtility.HideKeyboard(mActivity, mEditRightEyeNCT);
        if (checkValidation()) {
            callToAddPreExaminationAPI();
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {
        boolean status = true;

        String mRightEyeNCT = mEditRightEyeNCT.getText().toString().trim();

        mEditRightEyeNCT.setError(null);

        if (TextUtils.isEmpty(mRightEyeNCT)) {
            mEditRightEyeNCT.requestFocus();
            mEditRightEyeNCT.setError(mActivity.getResources().getString(R.string.error_field_required));
            status = false;
        }

        return status;
    }

    /**
     * This method should call Add Pre Examination API
     */
    private void callToAddPreExaminationAPI() {
        try {
            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);
            String mRightEyeNCT = mEditRightEyeNCT.getText().toString().trim();
            String mRightEyeAT = mEditRightEyeAT.getText().toString().trim();
            String mRightEyePachymetry = mEditRightEyePachymetry.getText().toString().trim();
            String mRightEyeSchirmer = mEditRightEyeSchirmer.getText().toString().trim();
            String mLeftEyeNCT = mEditLeftEyeNCT.getText().toString().trim();
            String mLeftEyeAT = mEditLeftEyeAT.getText().toString().trim();
            String mLeftEyePachymetry = mEditLeftEyePachymetry.getText().toString().trim();
            String mLeftEyeSchirmer = mEditLeftEyeSchirmer.getText().toString().trim();

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAddPreExaminationJson(AddNewPreliminaryExaminationFragment.PreliminaryExminationId, mUserId, mDatabaseName,
                            mRightEyeNCT, mRightEyeAT, mRightEyePachymetry, mRightEyeSchirmer, mLeftEyeNCT, mLeftEyeAT,
                            mLeftEyePachymetry, mLeftEyeSchirmer));

            Call<AddPreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).addPreExamination(requestBody);
            call.enqueue(new Callback<AddPreliminaryExaminationModel>() {
                @Override
                public void onResponse(@NonNull Call<AddPreliminaryExaminationModel> call, @NonNull Response<AddPreliminaryExaminationModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                Common.setCustomToast(mActivity, response.message());

                                if (mArrHistory.size() > 0) {
                                    mArrHistory.clear();
                                }
                                callToPrelminarHistoryPreExamination();

                                AddNewPreliminaryExaminationFragment.changePage(3);
                            } else {
                                Common.setCustomToast(mActivity, response.message());
                            }

                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddPreliminaryExaminationModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



  /*  private static void setData(LineChart lineChart) {
        ArrayList<Entry> LeftAct = new ArrayList<>();
        ArrayList<Entry> RightAct = new ArrayList<>();

        ArrayList<String> xAxis = labelArrayList();


        for (int l=0;l<AddNewPreliminaryExaminationFragment.mArrPreliminaryExamination.size();
                l++){

            float a=71;
            float pos=l;
            if (!AddNewPreliminaryExaminationFragment.mArrPreliminaryExamination.get(l).
                    getLNCT().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){

                a= Float.parseFloat(AddNewPreliminaryExaminationFragment.mArrPreliminaryExamination.get(l).
                        getLNCT());
            }

            LeftAct.add(new Entry(pos,a));
        }

        for (int r=0;r<AddNewPreliminaryExaminationFragment.mArrPreliminaryExamination.size();
             r++){

            float a=70;
            float pos=r;
            if (!AddNewPreliminaryExaminationFragment.mArrPreliminaryExamination.get(r).
                    getRNCT().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){

                a= Float.parseFloat(AddNewPreliminaryExaminationFragment.mArrPreliminaryExamination.get(r).
                        getLNCT());
            }

            RightAct.add(new Entry(pos,a));
        }

        String[] mStringArray = (String[]) xAxis.toArray();

        ArrayList<LineDataSet> lines = new ArrayList<LineDataSet> ();

        // create a dataset and give it a type
        LineDataSet set1 = new LineDataSet(LeftAct, "Left NCT");
        set1.setAxisDependency(YAxis.AxisDependency.LEFT);
        set1.setColor(ColorTemplate.getHoloBlue());
        set1.setValueTextColor(ColorTemplate.getHoloBlue());
        set1.setLineWidth(1.5f);
        set1.setDrawCircles(true);
        set1.setDrawValues(true);
        set1.setFillAlpha(65);
        set1.setFillColor(ColorTemplate.getHoloBlue());
        set1.setHighLightColor(Color.rgb(244, 117, 117));
        set1.setDrawCircleHole(true);

        // create a dataset and give it a type
        LineDataSet set2 = new LineDataSet(RightAct, "Right NCT");
        set2.setAxisDependency(YAxis.AxisDependency.LEFT);
        set2.setColor(Color.GREEN);
        set2.setValueTextColor(Color.GREEN);
        set2.setLineWidth(1.5f);
        set2.setDrawCircles(true);
        set2.setDrawValues(true);
        set2.setFillAlpha(65);
        set2.setFillColor(Color.GREEN);
        set2.setHighLightColor(Color.rgb(244, 117, 117));
        set2.setDrawCircleHole(true);


        // create a data object with the data sets
        LineData data = new LineData(set1);
        data.setValueTextColor(Color.WHITE);
        data.setValueTextSize(9f);

        // create a data object with the data sets
        LineData rdata = new LineData(set2);
        rdata.setValueTextColor(Color.WHITE);
        rdata.setValueTextSize(9f);

        // set data

        lineChart.setData(rdata);
        lineChart.setData(data);
    }*/

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        // setTodayData();
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        //setTodayData();
    }

    /**
     * Open Dialog Pre Examination History graph
     *
     * @param mActivity
     */
    public void openDialogPreExaminationChart(Activity mActivity) {
        final Dialog dialog = new Dialog(mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_history_pre_examination_graph);

        TextView mTextTitle = dialog.
                findViewById(R.id.text_view_custom_dialog_history_pre_examination_title);
        ImageView mImageClose = dialog.
                findViewById(R.id.image_custom_dialog_history_pre_examination_graph_close);

        LineChart lineChart = dialog.
                findViewById(R.id.line_chart_custom_dialog_history_pre_examination_graph);

        mTextTitle.setText(mActivity.getResources().getString(R.string.tab_pre_examination));

        mImageClose.setColorFilter(Common.setThemeColor(mActivity));

        mImageClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        //****Line Chart Code START **********//
        lineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

            }

            @Override
            public void onNothingSelected() {

            }
        });

        lineChart.setDrawGridBackground(true);
        lineChart.getDescription().setEnabled(true);
        lineChart.setDrawBorders(false);

        lineChart.getAxisLeft().setEnabled(true);
        lineChart.getAxisRight().setDrawAxisLine(true);
        lineChart.getAxisRight().setDrawGridLines(true);
        lineChart.getXAxis().setDrawAxisLine(true);
        lineChart.getXAxis().setDrawGridLines(true);
        lineChart.getXAxis().setGranularity(1f); // minimum axis-step (interval) is 1
        // enable touch gestures
        lineChart.setTouchEnabled(true);
        // enable scaling and dragging
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);
        // if disabled, scaling can be done on x- and y-axis separately
        lineChart.setPinchZoom(true);


        Legend l = lineChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);

        getData(lineChart);

        //****Line Chart Code END **********//
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    /**
     *OpenDialog for closing patient file
     */
    public void openFileClosingAlert(Activity mActivity) {


        AlertDialog.Builder builder = new AlertDialog.Builder(
                mActivity);
        builder.setTitle("Close File");
        builder.setMessage("Are you sure you want to close file?");
        builder.setNegativeButton("NO",
                (dialog, which) -> dialog.dismiss());
        builder.setPositiveButton("YES",
                (dialog, which) -> closeFile());
        builder.show();

    }

    private void closeFile() {
        try {
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.closeFileJson(mAppointmentId, mDatabaseName));

            Call<CloseFileModel> call = RetrofitClient.createService(ApiInterface.class).closeFileAPI(body);
            call.enqueue(new Callback<CloseFileModel>() {
                @Override
                public void onResponse(@NonNull Call<CloseFileModel> call, @NonNull Response<CloseFileModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        int mError = response.body().getError();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                Common.insertLog("IF");
                                Common.setCustomToast(mActivity,mMessage);
                            } else {
                                Common.insertLog("ELSE");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CloseFileModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openDialogPreExaminationHistory(Activity mActivity) {
        try {

            if (mActivity == null) {
                mActivity = getActivity();
            }

            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.custom_dialog_history_preliminary);

            TextView mTextTitle = dialog.findViewById(R.id.text_view_custom_dialog_history_preliminary);
            ImageView mImageClose = dialog.findViewById(R.id.image_custom_dialog_history_preliminary_close);

            RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);

            RelativeLayout noDataFound = dialog.findViewById(R.id.relative_no_data_available);

            mTextTitle.setText(mActivity.getResources().getString(R.string.tab_pre_examination));

            mImageClose.setColorFilter(Common.setThemeColor(mActivity));

            mImageClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            LinearLayoutManager manager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(manager);

            HistoryPreExaminationAdapter adapter = new HistoryPreExaminationAdapter(mActivity, recyclerView,
                    mArrHistory,false) {
                /*@Override
                protected void onSelectIten(HistoryPreliminaryExaminationModel.Data checkInData) {
                    super.onSelectIten(checkInData);
                    dialog.dismiss();
                    setHistorySetData(mActivity,checkInData.getHistoryAndComplains().get(0));
                }*/
            };
            recyclerView.setAdapter(adapter);

            if (mArrHistory.size() > 0) {
                recyclerView.setVisibility(View.VISIBLE);
                noDataFound.setVisibility(View.GONE);
            } else {
                recyclerView.setVisibility(View.GONE);
                noDataFound.setVisibility(View.VISIBLE);
            }

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
