package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

public class SearchReceptionistPaymentFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private EditText mEditPatientName, mEditPatientCode, mEditPatientMobile, mEditAppointmentNumber, mEditDate;
    private Button mButtonSearch;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_search_receptionist_payment, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        getIds();
        setRegListeners();
        setHasOptionsMenu(true);

        mEditPatientCode.setFilters(Common.getFilter());
        mEditPatientName.setFilters(Common.getFilter());

        return mView;
    }

    /**
     * Set the prepare menu options
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Edit Texts
            mEditPatientName = mView.findViewById(R.id.edit_search_receptionist_payment_patient_name);
            mEditPatientCode = mView.findViewById(R.id.edit_search_receptionist_payment_patient_code);
            mEditPatientMobile = mView.findViewById(R.id.edit_search_receptionist_payment_patient_mobile);
            mEditAppointmentNumber = mView.findViewById(R.id.edit_search_receptionist_payment_patient_appointment_number);

            // Text Views
            mEditDate = mView.findViewById(R.id.edit_search_receptionist_payment_date);

            // Buttons
            mButtonSearch = mView.findViewById(R.id.button_search_receptionist_payment_search);

            // Set Request Focus
            mEditPatientName.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mButtonSearch.setOnClickListener(clickListener);
            mEditDate.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_search_receptionist_payment_search:
                    callToReceptionistPaymentFragment();
                    break;

                case R.id.edit_search_receptionist_payment_date:
                    Common.openDatePickerForAllDates(mActivity, mEditDate);
                    break;
            }
        }
    };

    /**
     * Sets up the Receptionist Payment fragment
     */
    private void callToReceptionistPaymentFragment() {
        try {
            String mAppointmentDate;

            String mName = mEditPatientName.getText().toString().trim();
            String mPatientCode = mEditPatientCode.getText().toString().trim();
            String mMobile = mEditPatientMobile.getText().toString().trim();
            String mAppointmentNo = mEditAppointmentNumber.getText().toString().trim();

            if (mEditDate.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                mAppointmentDate = mActivity.getResources().getString(R.string.date_format_first_time);
            } else {
                mAppointmentDate = Common.convertDateWithoutTime(mActivity, mEditDate.getText().toString().trim(), mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
            }

            if (!mEditPatientName.getText().toString().trim().equals("")) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_payment));
                Fragment fragment = new ReceptionistPaymentFragment();
                Bundle args = new Bundle();
                args.putString(AppConstants.BUNDLE_PATIENT_NAME, mName);
                args.putString(AppConstants.BUNDLE_PATIENT_CODE, mPatientCode);
                args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mMobile);
                args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, mAppointmentNo);
                args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DATE, mAppointmentDate);
                fragment.setArguments(args);
                FragmentManager fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
                fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                        mActivity.getResources().getString(R.string.nav_menu_payment)).commit();

            } else {
                mEditPatientName.setError("Please Enter Patient Name");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
