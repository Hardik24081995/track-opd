package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.trackopd.MyApplication;
import com.trackopd.R;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.SessionManager;
import com.trackopd.utils.SharedPrefs;

public class ChangeThemeFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private Button mButtonChangeTheme;
    private ImageView mImageRedFullView, mImageGreenFullView, mImageDarkBlueFullView, mImageLightBlueFullView,
            mImageRedSelView, mImageGreenSelView, mImageDarkBlueSelView, mImageLightBlueSelView;
    private String mIsLoginType;
    private SessionManager mSessionManager;
    private SharedPrefs sharedPrefs;
    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            setDefaultImage();
            switch (v.getId()) {
                case R.id.image_change_theme_red_full_view:
                    mImageRedSelView.setBackgroundResource(R.drawable.ic_check_red);
                    sharedPrefs.setPreferences(sharedPrefs.CurrentTheme, MyApplication.THEME_RED);
                    break;

                case R.id.image_change_theme_green_full_view:
                    mImageGreenSelView.setBackgroundResource(R.drawable.ic_check_green);
                    sharedPrefs.setPreferences(sharedPrefs.CurrentTheme, MyApplication.THEME_GREEN);
                    break;

                case R.id.image_change_theme_dark_blue_full_view:
                    mImageDarkBlueSelView.setBackgroundResource(R.drawable.ic_check_theme);
                    sharedPrefs.setPreferences(sharedPrefs.CurrentTheme, MyApplication.THEME_DARK_BLUE);
                    break;

                case R.id.image_change_theme_light_blue_full_view:
                    mImageLightBlueSelView.setBackgroundResource(R.drawable.ic_check_blue);
                    sharedPrefs.setPreferences(sharedPrefs.CurrentTheme, MyApplication.THEME_LIGHT_BLUE);
                    break;

                case R.id.button_change_theme_submit:
                    MyApplication.changeToTheme(getActivity(), mIsLoginType, sharedPrefs.getPreferences(sharedPrefs.CurrentTheme, MyApplication.THEME_RED));
                    break;
            }
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_change_theme, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        MyApplication.onActivityCreateSetTheme(mActivity);
        setHasOptionsMenu(true);

        sharedPrefs = new SharedPrefs(mActivity);

        getBundle();
        getIds();
        setRegListeners();
        setData();
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mIsLoginType = bundle.getString(AppConstants.BUNDLE_LOGIN_TYPE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Image Views
            mImageRedFullView = mView.findViewById(R.id.image_change_theme_red_full_view);
            mImageGreenFullView = mView.findViewById(R.id.image_change_theme_green_full_view);
            mImageDarkBlueFullView = mView.findViewById(R.id.image_change_theme_dark_blue_full_view);
            mImageLightBlueFullView = mView.findViewById(R.id.image_change_theme_light_blue_full_view);
            mImageRedSelView = mView.findViewById(R.id.image_change_theme_red_sel_view);
            mImageGreenSelView = mView.findViewById(R.id.image_change_theme_green_sel_view);
            mImageDarkBlueSelView = mView.findViewById(R.id.image_change_theme_dark_blue_sel_view);
            mImageLightBlueSelView = mView.findViewById(R.id.image_change_theme_light_blue_sel_view);

            // Buttons
            mButtonChangeTheme = mView.findViewById(R.id.button_change_theme_submit);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mImageRedFullView.setOnClickListener(clickListener);
            mImageGreenFullView.setOnClickListener(clickListener);
            mImageDarkBlueFullView.setOnClickListener(clickListener);
            mImageLightBlueFullView.setOnClickListener(clickListener);
            mButtonChangeTheme.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the theme background
     */
    private void setData() {
        try {
            sharedPrefs = new SharedPrefs(getActivity());
            mSessionManager = new SessionManager(getActivity());

            if (sharedPrefs.getPreferences(sharedPrefs.CurrentTheme, MyApplication.THEME_LIGHT_BLUE) == MyApplication.THEME_RED) {
                mImageRedSelView.setBackgroundResource(R.drawable.ic_check_red);
            } else if (sharedPrefs.getPreferences(sharedPrefs.CurrentTheme, MyApplication.THEME_LIGHT_BLUE) == MyApplication.THEME_GREEN) {
                mImageGreenSelView.setBackgroundResource(R.drawable.ic_check_green);
            } else if (sharedPrefs.getPreferences(sharedPrefs.CurrentTheme, MyApplication.THEME_LIGHT_BLUE) == MyApplication.THEME_DARK_BLUE) {
                mImageDarkBlueSelView.setBackgroundResource(R.drawable.ic_check_theme);
            } else {
                mImageLightBlueSelView.setBackgroundResource(R.drawable.ic_check_blue);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is used to set the default checked theme background
     */
    private void setDefaultImage() {
        try {
            mImageRedSelView.setBackground(null);
            mImageGreenSelView.setBackground(null);
            mImageDarkBlueSelView.setBackground(null);
            mImageLightBlueSelView.setBackground(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}