package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.InputFilter;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.model.AddAppointmentModel;
import com.trackopd.model.AppointmentValidityModel;
import com.trackopd.model.DoctorModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.GenerateTimeSlot;
import com.trackopd.utils.KeyboardUtility;
import com.trackopd.utils.SessionManager;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddAppointmentFragment extends Fragment {

    private View mView, mViewAmount, mViewLocations;
    private Menu menu;
    private Activity mActivity;
    private Button mButtonSubmit;
    private EditText mEditNotes, mEditAmount, mEditLocations, mEditFollowDate, mEditFollowTime, mEditDate, mEditTime;
    private RadioButton mRadioBooking, mRadioPayNow, mRadioReferral, mRadioCamp, mRadioFolloup;
    private RadioGroup mRadioGroupAppointmentType, mRadioGroupPaymentType, mRadioGroupLocations, mRadioGroupTreatment;
    private String mAppointmentType, mPaymentType, mSelectedLocations, mSelectTreatment, mPatientImage;
    private Spinner mSpinnerDoctor;
    private ImageView mImageUserProfilePic;
    private TextView mTextPatientName, mTextPatientMobile;
    private TextInputLayout mInputLayoutAmount, mInputLayoutLocations;
    private String mFirstName, mLastName, mMobileNo, mPatientId, mProfileImage, mAmount, mLocations, mUserType, mAppointmentConfig, mProfileImageLocal, mPatientUserId;
    private ArrayList<String> mArrDoctor;
    private ArrayList<DoctorModel> mArrDoctorList;
    private int mSelectedDoctorIndex, mDoctorId, selPaymentType, mSelLocations, mSelTreatment;
    private LinearLayout mLinearFollowUp;

    private GenerateTimeSlot generateTimeSlot;


    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.edit_add_appointment_date:
                    Common.openDatePicker(mActivity, mEditDate);
                    break;
                case R.id.edit_add_appointment_follow_date:
                    Common.openDatePicker(mActivity, mEditFollowDate);
                    break;

                case R.id.edit_add_appointment_time:
//                    GenerateTimeSlot.openTimeSlotDialog(mActivity, mEditTime);

                    String doctor_id = "-1";
                    int selected_doctor_index = mSpinnerDoctor.getSelectedItemPosition();
                    if (selected_doctor_index != 0) {
                        doctor_id = String.valueOf(mDoctorId);
                    }
                    generateTimeSlot.openTimeSlotDialog(mActivity, mEditTime, mEditDate, doctor_id);
                    break;

                case R.id.button_add_appointment_submit:
                    doAddAppointment();
                    break;

                case R.id.edit_add_appointment_follow_time:

                    String id = "-1";
                    int selected_doctor = mSpinnerDoctor.getSelectedItemPosition();
                    if (selected_doctor != 0) {
                        id = String.valueOf(mDoctorId);
                    }
                    generateTimeSlot.openTimeSlotDialog(mActivity, mEditFollowTime, mEditFollowDate, id);
                    break;
            }
        }
    };
    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {
                case R.id.spinner_add_appointment_doctor:
                    mSelectedDoctorIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedDoctorIndex != 0) {
                        mDoctorId = (Integer.parseInt(mArrDoctorList.get(mSelectedDoctorIndex - 1).getDoctorID()));
                        String mDoctorName = (mArrDoctorList.get(mSelectedDoctorIndex - 1).getDoctorName());
                        Common.insertLog("mDoctorID::> " + mDoctorId);
                        Common.insertLog("mDoctorName::> " + mDoctorName);
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };
    /**
     * Radio button on checked change listeners
     */
    private RadioGroup.OnCheckedChangeListener checkedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            RadioButton mRadioButton = group.findViewById(checkedId);
            mRadioButton.clearFocus();

            switch (group.getId()) {
                case R.id.radio_group_add_appointment_type:
                    int selAppointmentType = group.getCheckedRadioButtonId();
                    if (selAppointmentType == 1) {
                        mAppointmentType = mActivity.getResources().getString(R.string.text_booking);
                    } else {
                        mAppointmentType = mActivity.getResources().getString(R.string.text_walk_in);
                    }
                    break;

                case R.id.radio_group_add_appointment_payment_type:
                    selPaymentType = group.getCheckedRadioButtonId();

                    if (selPaymentType == R.id.radio_button_add_appointment_payment_type_payment_later) {
                        mPaymentType = mActivity.getResources().getString(R.string.text_later);
                        mInputLayoutAmount.setVisibility(View.GONE);
                        mViewAmount.setVisibility(View.GONE);
                    } else {
                        mPaymentType = mActivity.getResources().getString(R.string.text_now);
                        mInputLayoutAmount.setVisibility(View.VISIBLE);
                        mViewAmount.setVisibility(View.VISIBLE);
                    }
                    break;

                case R.id.radio_group_add_appointment_locations:
                    mSelLocations = group.getCheckedRadioButtonId();

                    if (mSelLocations == R.id.radio_button_add_appointment_locations_referral) {
                        mSelectedLocations = mActivity.getResources().getString(R.string.text_referral);
                        mInputLayoutLocations.setVisibility(View.VISIBLE);
                        mViewLocations.setVisibility(View.VISIBLE);
                        mEditLocations.setText("");
                        mLinearFollowUp.setVisibility(View.GONE);
                    } else if (mSelLocations == R.id.radio_button_add_appointment_locations_camp) {
                        mSelectedLocations = mActivity.getResources().getString(R.string.text_camp);
                        mInputLayoutLocations.setVisibility(View.VISIBLE);
                        mViewLocations.setVisibility(View.VISIBLE);
                        mEditLocations.setText("");
                        mLinearFollowUp.setVisibility(View.GONE);
                    } else if (mSelLocations == R.id.radio_button_add_appointment_locations_default) {
                        mSelectedLocations = mActivity.getResources().getString(R.string.text_direct);
                        mInputLayoutLocations.setVisibility(View.GONE);
                        mViewLocations.setVisibility(View.GONE);
                        mEditLocations.setText("");
                        mLinearFollowUp.setVisibility(View.GONE);
                    } else if (mSelLocations == R.id.radio_button_add_appointment_locations_followup) {
                        mSelectedLocations = mActivity.getResources().getString(R.string.text_follow_ups);
                        mInputLayoutLocations.setVisibility(View.GONE);
                        mViewLocations.setVisibility(View.GONE);
                        mEditLocations.setText("");
                        mLinearFollowUp.setVisibility(View.VISIBLE);
                    }

                    break;
                case R.id.radio_group_add_appointment_treatment_type:
                    mSelTreatment = group.getCheckedRadioButtonId();
                    if (mSelTreatment == R.id.radio_button_add_appointment_treatment_type_checkup) {
                        mSelectTreatment = "Check-up";
                    } else if (mSelTreatment == R.id.radio_button_add_appointment_treatment_type_surgery) {
                        mSelectTreatment = "Surgery";
                    }
                    break;
            }
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_add_appointment, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrDoctor = new ArrayList<>();
        mArrDoctorList = new ArrayList<>();
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);
        mAppointmentConfig = GetJsonData.getConfigurationData(mActivity, WebFields.CONFIGURATION.RESPONSE_APPOINTMENT_CONFIGURATION);
        Common.insertLog("Appointment Config:::> " + mAppointmentConfig);

        generateTimeSlot = new GenerateTimeSlot();

        getBundle();
        getIds();
        setRegListeners();
        setData();
        callToDoctorAPI();
        setHasOptionsMenu(true);

        mEditNotes.setFilters(Common.getFilter());
        mEditAmount.setFilters(Common.getFilter());
        mEditLocations.setFilters(Common.getFilter());
        mEditFollowDate.setFilters(Common.getFilter());
        mEditFollowTime.setFilters(Common.getFilter());
        mEditDate.setFilters(Common.getFilter());
        mEditTime.setFilters(Common.getFilter());

        InputFilter[] fa = new InputFilter[1];
        fa[0] = new InputFilter.LengthFilter(25);
        InputFilter[] famount
                = new InputFilter[1];
        famount[0] = new InputFilter.LengthFilter(8);
        mEditLocations.setFilters(fa);
        mEditAmount.setFilters(famount);

        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R
                .menu.menu_receptionist_patients, menu);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);

        MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {

                mFirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mLastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mMobileNo = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mPatientUserId = bundle.getString(AppConstants.BUNDLE_ADD_PATIENT_USERID);
                mProfileImage = bundle.getString(AppConstants.BUNDLE_PATIENT_PROFILE);
                mProfileImageLocal = bundle.getString(AppConstants.BUNDLE_ADD_PATIENT_PHOTO_LOCAL);

            }
            Common.insertLog("Bundle Add Appointment Get::::> " + mFirstName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Text Views
            mTextPatientName = mView.findViewById(R.id.text_add_appointment_patient_name);
            mTextPatientMobile = mView.findViewById(R.id.text_add_appointment_patient_mobile);

            // Input Layouts
            mInputLayoutAmount = mView.findViewById(R.id.text_input_add_appointment_amount);
            mInputLayoutLocations = mView.findViewById(R.id.text_input_add_appointment_locations);

            // Edit Texts
            mEditDate = mView.findViewById(R.id.edit_add_appointment_date);
            mEditTime = mView.findViewById(R.id.edit_add_appointment_time);
            mEditAmount = mView.findViewById(R.id.edit_add_appointment_amount);
            mEditNotes = mView.findViewById(R.id.edit_add_appointment_notes);
            mEditLocations = mView.findViewById(R.id.edit_add_appointment_locations);
            mEditFollowDate = mView.findViewById(R.id.edit_add_appointment_follow_date);
            mEditFollowTime = mView.findViewById(R.id.edit_add_appointment_follow_time);

            // Image Views
            mImageUserProfilePic = mView.findViewById(R.id.image_add_appointment_profile_pic);

            // Radio Buttons
            mRadioBooking = mView.findViewById(R.id.radio_button_add_appointment_booking);
            mRadioPayNow = mView.findViewById(R.id.radio_button_add_appointment_payment_type_payment_now);
            mRadioReferral = mView.findViewById(R.id.radio_button_add_appointment_locations_referral);
            mRadioCamp = mView.findViewById(R.id.radio_button_add_appointment_locations_camp);
            mRadioFolloup = mView.findViewById(R.id.radio_button_add_appointment_locations_followup);

            // Views
            mViewAmount = mView.findViewById(R.id.view_add_appointment_amount);
            mViewLocations = mView.findViewById(R.id.view_add_appointment_view_locations);

            // Spinners
            mSpinnerDoctor = mView.findViewById(R.id.spinner_add_appointment_doctor);

            // Radio Groups
            mRadioGroupAppointmentType = mView.findViewById(R.id.radio_group_add_appointment_type);
            mRadioGroupPaymentType = mView.findViewById(R.id.radio_group_add_appointment_payment_type);
            mRadioGroupLocations = mView.findViewById(R.id.radio_group_add_appointment_locations);
            mRadioGroupTreatment = mView.findViewById(R.id.radio_group_add_appointment_treatment_type);

            // Buttons
            mButtonSubmit = mView.findViewById(R.id.button_add_appointment_submit);

            //Linear Layout
            mLinearFollowUp = mView.findViewById(R.id.linear_add_appointment_follow_date);

            // Set Request Focus
            mEditAmount.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Click Listeners
            mEditDate.setOnClickListener(clickListener);
            mEditFollowDate.setOnClickListener(clickListener);
            mEditTime.setOnClickListener(clickListener);
            mButtonSubmit.setOnClickListener(clickListener);
            mEditFollowTime.setOnClickListener(clickListener);

            // ToDo: Doctor - Spinner Item Select Listener
            mSpinnerDoctor.setOnItemSelectedListener(onItemSelectedListener);

            // ToDo: Radio Button Click Listeners
            mRadioGroupAppointmentType.setOnCheckedChangeListener(checkedChangeListener);
            mRadioGroupPaymentType.setOnCheckedChangeListener(checkedChangeListener);
            mRadioGroupLocations.setOnCheckedChangeListener(checkedChangeListener);
            mRadioGroupTreatment.setOnCheckedChangeListener(checkedChangeListener);


            //Disable Emoji Keyboard input
            mEditLocations.setFilters(Common.getFilter());
            mEditNotes.setFilters(Common.getFilter());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            mTextPatientName.setText(mFirstName + AppConstants.STR_EMPTY_SPACE + mLastName);
            mTextPatientMobile.setText(mMobileNo);

            // Set Image
            if (mProfileImage != null && !mProfileImage.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                //Set Thumbnail
                String thumbmail_url = WebFields.API_BASE_URL + WebFields.IMAGE_THUMBNAIL_URL + mProfileImage;
                Glide.with(mActivity)
                        .load(thumbmail_url)
                        .apply(new RequestOptions().error(R.drawable.ic_documents_img).placeholder(R.drawable.ic_documents_img))
                        .into(mImageUserProfilePic);
            } else {
                if (mProfileImageLocal != null && !mProfileImageLocal.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {

                    try {
                        File file = new File(mProfileImageLocal);

                        if (file.exists()) {
                            Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
                            mImageUserProfilePic.setImageBitmap(myBitmap);
                        }
                    } catch (Exception e) {
                        mImageUserProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, mFirstName,
                                mLastName));
                    }
                } else {
                    mImageUserProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, mFirstName,
                            mLastName));
                }
            }

            // Set current date
            mEditDate.setText(Common.setCurrentDate(mActivity));
            mEditFollowDate.setText(Common.setCurrentDate(mActivity));

            mAppointmentType = mActivity.getResources().getString(R.string.text_booking);
            mPaymentType = mActivity.getResources().getString(R.string.text_later);
            mSelectTreatment = "Check-up";


            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerDoctor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Doctor API
     */
    private void callToDoctorAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setDoctorListJson(hospital_database));

            Call<DoctorModel> call = RetrofitClient.createService(ApiInterface.class).getDoctorList(body);
            call.enqueue(new Callback<DoctorModel>() {
                @Override
                public void onResponse(@NonNull Call<DoctorModel> call, @NonNull Response<DoctorModel> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrDoctorList.addAll(response.body().getData());
                            }
                            setsDoctorAdapter();
                        } else {
                            setsDoctorAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DoctorModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setsDoctorAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsDoctorAdapter() {
        try {
            mArrDoctor.add(0, mActivity.getResources().getString(R.string.spinner_select_doctor));
            if (mArrDoctorList.size() > 0) {
                for (DoctorModel doctorModel : mArrDoctorList) {
                    mArrDoctor.add(doctorModel.getDoctorName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerDoctor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrDoctor) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedDoctorIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerDoctor.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method checks the validation first then call the API
     */
    private void doAddAppointment() {
        KeyboardUtility.HideKeyboard(mActivity, mEditNotes);
        if (checkValidation()) {
            checkAppointmentValidityAPI();
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {
        boolean status = true;

        mAmount = mEditAmount.getText().toString().trim();
        mLocations = mEditLocations.getText().toString().trim();

        mEditAmount.setError(null);

        if (mEditDate.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.val_select_date));
            return false;
        } else if (mEditTime.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.val_select_time));
            return false;
        }

        if (mSpinnerDoctor.getSelectedItemPosition() == 0) {
            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.val_select_doctor));
            status = false;
        }

        if (mRadioBooking.isChecked()) {
            mAppointmentType = mActivity.getResources().getString(R.string.text_booking);
        } else {
            mAppointmentType = mActivity.getResources().getString(R.string.text_walk_in);
        }

        if (mAmount.equalsIgnoreCase("") && mAmount.equalsIgnoreCase("0") && mAmount.equalsIgnoreCase("00") && mAmount.equalsIgnoreCase("000") && mAmount.equalsIgnoreCase("0000") && mAmount.equalsIgnoreCase("00000") && mAmount.equalsIgnoreCase("000000") && mAmount.equalsIgnoreCase("0000000") && mAmount.equalsIgnoreCase("00000000")) {
            mEditAmount.setError("Please enter valid amount");
            status = false;
        }


        // Reference
        if (mRadioReferral.isChecked()) {
            mSelectedLocations = mActivity.getResources().getString(R.string.text_referral);
        } else if (mRadioCamp.isChecked()) {
            mSelectedLocations = mActivity.getResources().getString(R.string.text_camp);
        } else if (mRadioFolloup.isChecked()) {
            mSelectedLocations = mActivity.getResources().getString(R.string.text_follow_ups);
        } else {
            mSelectedLocations = mActivity.getResources().getString(R.string.text_direct);
        }

        if (mRadioPayNow.isChecked()) {
            mPaymentType = mActivity.getResources().getString(R.string.text_now);

            Common.insertLog("mAmount:::> " + mAmount);
            if (TextUtils.isEmpty(mAmount)) {
                mEditAmount.setError(getResources().getString(R.string.error_enter_amount));
                status = false;
            }
        } else {
            mPaymentType = mActivity.getResources().getString(R.string.text_later);
        }

        return status;
    }

    /**
     * This method should check the doctor availability and based on that it wil call the API
     */
    private void checkAppointmentValidityAPI() {
        try {
            String mConvertedDate = Common.convertDateToServer(mActivity, mEditDate.getText().toString());
            String mConvertedTime = Common.convertTime(mActivity, mEditTime.getText().toString().trim());
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.checkAppointmentValidityJson(mConvertedDate, mConvertedTime, mDoctorId, mDatabaseName));

            Call<AppointmentValidityModel> call = RetrofitClient.createService(ApiInterface.class).checkAppointmentValidity(requestBody);
            call.enqueue(new Callback<AppointmentValidityModel>() {
                @Override
                public void onResponse(@NonNull Call<AppointmentValidityModel> call, @NonNull Response<AppointmentValidityModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = jsonObject.getInt(WebFields.ERROR);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                callToAddAppointmentAPI();
                            }
                        } else {
                            if (mAppointmentConfig.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_confirmation))) {
                                checkAppointmentValidityPopup();
                            } else {
                                openPopup();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AppointmentValidityModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open appointment validity popup
     */
    private void checkAppointmentValidityPopup() {
        try {
            new MaterialDialog.Builder(mActivity)
                    .title(R.string.app_name)
                    .content(R.string.dialog_confirmation_message)
                    .canceledOnTouchOutside(false)
                    .positiveText(R.string.action_ok)
                    .positiveColor(getResources().getColor(R.color.app_main_theme))
                    .negativeText(R.string.action_cancel)
                    .negativeColor(getResources().getColor(R.color.colorControlNormal))

                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            dialog.dismiss();
                            callToAddAppointmentAPI();
                        }

                        public void onNegative(MaterialDialog dialog) {
                            dialog.dismiss();
                            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.error_select_other_time_slot));
                        }
                    })
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open popup for make it mandatory appointment creation
     */
    private void openPopup() {
        try {
            new MaterialDialog.Builder(mActivity)
                    .title(R.string.app_name)
                    .content(R.string.dialog_make_it_mandatory_message)
                    .canceledOnTouchOutside(false)
                    .positiveText(R.string.action_ok)
                    .positiveColor(getResources().getColor(R.color.app_main_theme))

                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            dialog.dismiss();
                            redirectedToReceptionistAppointmentFragment();
                        }
                    })
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call Add Appointment API
     */
    private void callToAddAppointmentAPI() {
        try {
            String mConvertedDate = Common.convertDateToServer(mActivity, mEditDate.getText().toString());
            String mConvertedTime = Common.convertTime(mActivity, mEditTime.getText().toString().trim());

            SessionManager manager = new SessionManager(getActivity());
            String PatientUserID = manager.getPreferences(manager.PATIENT_USERID, "");

            String mComment = mEditNotes.getText().toString();
            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);

            String PID = "";
            if (manager.getPreferences(manager.PID, "").equalsIgnoreCase("patient")) {
                PID = PatientUserID;
            } else if (manager.getPreferences(manager.PID, "").equalsIgnoreCase("patientAppointment")) {
                PID = PatientUserID;
            } else {
                PID = mPatientId;
            }

            if (mEditAmount.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                mAmount = "0";
            } else {
                mAmount = mEditAmount.getText().toString();
            }

            String followDate = AppConstants.STR_EMPTY_STRING;
            if (mSelectedLocations.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_follow_ups))) {
                followDate = Common.convertDateToServer(mActivity, mEditFollowDate.getText().toString());
                String mConvertedFollowTime = Common.convertTime(mActivity, mEditFollowTime.getText().toString().trim());
                followDate = followDate + AppConstants.STR_EMPTY_STRING + mConvertedFollowTime;
            }

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setReceptionistAddAppointmentJson(PID, mDoctorId, mConvertedDate,
                            mConvertedTime, mAmount, mAppointmentType, mPaymentType, mComment, mUserId, mSelectedLocations, mLocations, followDate, mSelectTreatment, hospital_database));
            Call<AddAppointmentModel> call = RetrofitClient.createService(ApiInterface.class).addReceptionistAppointment(requestBody);
            call.enqueue(new Callback<AddAppointmentModel>() {
                @Override
                public void onResponse(@NonNull Call<AddAppointmentModel> call, @NonNull Response<AddAppointmentModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = jsonObject.getInt(WebFields.ERROR);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            Common.setCustomToast(mActivity, mMessage);
                            if (response.body() != null) {
                                String mAppointmentNo = response.body().getData().get(0).getTicketNumber();
                                openAppointmentDialog(mAppointmentNo);
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddAppointmentModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open up the Appointment dialog
     */
    @SuppressLint("SetTextI18n")
    public void openAppointmentDialog(String mAppointmentNo) {
        try {
            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.custom_dialog_appointment);

            // Text Views
            TextView mTextAppointmentNo = dialog.findViewById(R.id.text_dialog_receptionist_appointment_number);
            TextView mTextName = dialog.findViewById(R.id.text_dialog_receptionist_appointment_name);
            TextView mTextMobile = dialog.findViewById(R.id.text_dialog_receptionist_appointment_mobile_no);
            TextView mTextDate = dialog.findViewById(R.id.text_dialog_receptionist_appointment_date);
            TextView mTextTime = dialog.findViewById(R.id.text_dialog_receptionist_appointment_time);
            TextView mTextDoctorName = dialog.findViewById(R.id.text_dialog_receptionist_appointment_doctor_name);
            TextView mTextAppointmentType = dialog.findViewById(R.id.text_dialog_receptionist_appointment_type);
            TextView mTextPaymentType = dialog.findViewById(R.id.text_dialog_receptionist_appointment_payment_type);
            TextView mTextAmount = dialog.findViewById(R.id.text_dialog_receptionist_appointment_payment_amount);
            TextView mTextSelLocations = dialog.findViewById(R.id.text_dialog_receptionist_appointment_sel_locations);
            TextView mTextLocations = dialog.findViewById(R.id.text_dialog_receptionist_appointment_locations);
            TextView mTextNote = dialog.findViewById(R.id.text_dialog_receptionist_appointment_note);

            // Button
            Button mButtonOk = dialog.findViewById(R.id.button_dialog_receptionist_appointment_ok);

            // Set values to the text views
            mTextAppointmentNo.setText(Common.isEmptyString(mActivity, mAppointmentNo));
            mTextName.setText(Common.isEmptyString(mActivity, mFirstName) + AppConstants.STR_EMPTY_SPACE + Common.isEmptyString(mActivity, mLastName));
            mTextMobile.setText(Common.isEmptyString(mActivity, mMobileNo));
            mTextDate.setText(Common.isEmptyString(mActivity, mEditDate.getText().toString()));
            mTextTime.setText(Common.isEmptyString(mActivity, mEditTime.getText().toString()));
            mTextDoctorName.setText(Common.isEmptyString(mActivity, mSpinnerDoctor.getSelectedItem().toString()));
            mTextAppointmentType.setText(Common.isEmptyString(mActivity, mAppointmentType));
            mTextPaymentType.setText(Common.isEmptyString(mActivity, mPaymentType));
            mTextAmount.setText(Common.isEmptyString(mActivity, mAmount));
            mTextSelLocations.setText(Common.isEmptyString(mActivity, mSelectedLocations));
            mTextLocations.setText(Common.isEmptyString(mActivity, mLocations));
            mTextNote.setText(Common.isEmptyString(mActivity, mEditNotes.getText().toString()));

            // ToDo: Button Ok Click Listener
            mButtonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    //Toast.makeText(mActivity, "Your appointment has been added successfully.", Toast.LENGTH_SHORT).show();
                    removeAllFragments();
                    redirectedToReceptionistAppointmentFragment();
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Used to remove all fragment in the fragment manager after logout
     */
    private void removeAllFragments() {
        try {
            List<Fragment> fragments = null;

            if (mUserType.equalsIgnoreCase(getString(R.string.user_type_receptionist))) {
                fragments = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_councillor))) {
                fragments = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
                fragments = ((OptometristHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                fragments = ((DoctorHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_patient))) {
                fragments = ((PatientHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            }

            if (fragments.size() > 0) {
                for (int i = 0; i < fragments.size(); i++) {
                    if (fragments.get(i) != null) {

                        if (mUserType.equalsIgnoreCase(getString(R.string.user_type_receptionist))) {
                            ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i));
                        } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_councillor))) {
                            ((CouncillorHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i));
                        } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
                            ((OptometristHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i));
                        } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                            ((DoctorHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i));
                        } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_patient))) {
                            ((PatientHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method redirected you to the Receptionist Appointment Fragment
     */
    private void redirectedToReceptionistAppointmentFragment() {
        try {
            Fragment fragment = new ReceptionistAppointmentFragment();
            Bundle args = new Bundle();

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_patient))) {
                ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            }

            args.putString(AppConstants.BUNDLE_ADD_APPOINTMENT, "1");
            args.putString(AppConstants.BUNDLE_PATIENT_NAME, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_MRD_NO, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DATE, getResources().getString(R.string.date_format_first_time));
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, -1);
            fragment.setArguments(args);
            FragmentManager fragmentManager = null;

            if (mUserType.equalsIgnoreCase(getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_patient))) {
                fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.nav_menu_appointment))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
