package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.adapter.PatientAppointmentAdapter;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.PatientAppointmentModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.utils.RecyclerProgressView;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientAppointmentFragment extends Fragment {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private AppUtil mAppUtils;
    private SwipeRefreshLayout mSwipeRefreshView;
    private RelativeLayout mRelativeNoData, mRelativeNoInternet;
    private Button mButtonRetry;
    private RecyclerView mRecyclerView;
    private PatientAppointmentAdapter mAdapterPatientAppointment;
    private ArrayList<PatientAppointmentModel> mArrPatientAppointment;
    private boolean isFirstTime = true;
    private String mAppointmentNo = "", mAppointmentDate = "0000-00-00";
    private int mDoctorId = -1;

    // Load More Listener Variables
    private int currentPageIndex = 1, lastFetchRecord = 0, totalRecords = 0;
    private boolean isRefresh = false, isLoadMore = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_patient_appointment, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mAppUtils = new AppUtil(getActivity());
        mArrPatientAppointment = new ArrayList<>();

        getBundle();
        getIds();
        setRegListeners();
        callShimmerView();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Set the prepare menu options
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);

        MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                return true;

            case R.id.action_search:
                callToSearchPatientAppointmentFragment();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mAppointmentNo = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mAppointmentDate = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DATE);
                mDoctorId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Recycler View
            mRecyclerView = mView.findViewById(R.id.recycler_view);

            // Swipe Refresh View
            mSwipeRefreshView = mView.findViewById(R.id.swipe_refresh_view);

            // Relative Layouts
            mRelativeNoData = mView.findViewById(R.id.relative_no_data_available);
            mRelativeNoInternet = mView.findViewById(R.id.relative_no_internet);

            // Buttons
            mButtonRetry = mView.findViewById(R.id.button_retry);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Click Listeners
            mButtonRetry.setOnClickListener(clickListener);

            // ToDo: Set On Refresh Listeners
            mSwipeRefreshView.setOnRefreshListener(refreshListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_retry:
                    callShimmerView();
                    break;
            }
        }
    };

    /**
     * Set On Refresh Listeners
     */
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {

        @Override
        public void onRefresh() {
            isFirstTime = true;

            if (!mAppUtils.getConnectionState()) {
                mRelativeNoInternet.setVisibility(View.VISIBLE);
                mSwipeRefreshView.setRefreshing(false);
                mArrPatientAppointment.clear();
                setViewVisibility();
            } else {
                mRelativeNoInternet.setVisibility(View.GONE);
                if (!isLoadMore) {
                    isRefresh = true;

                    mAppointmentNo = AppConstants.STR_EMPTY_STRING;
                    mAppointmentDate = mActivity.getResources().getString(R.string.date_format_first_time);
                    mDoctorId = -1;

                    if (mArrPatientAppointment != null)
                        mArrPatientAppointment.clear();
                    if (mAdapterPatientAppointment != null)
                        mAdapterPatientAppointment.notifyDataSetChanged();
                    currentPageIndex = 1;
                    callShimmerView();
                } else {
                    mSwipeRefreshView.setRefreshing(false);
                }
            }
        }
    };

    /**
     * This method is used to call the shimmer effect for recycler view before calling the API
     */
    private void callShimmerView() {
        if (!mAppUtils.getConnectionState()) {
            mRelativeNoInternet.setVisibility(View.VISIBLE);
            mArrPatientAppointment.clear();
            setViewVisibility();
        } else {
            mRelativeNoInternet.setVisibility(View.GONE);
            if (isFirstTime) {
                showProgressView();
                isFirstTime = false;
                callAppointmentListAPI();
            } else {
                callAppointmentListAPI();
            }
        }
    }

    /**
     * Show progress view when page load for the first time
     */
    private void showProgressView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.VISIBLE);

                RecyclerProgressView.startProgress((LinearLayout) mView.findViewById(R.id.linear_add_recycler_progress_view),
                        R.layout.row_patient_appointment_item, getActivity());
            }
        }, 100);
    }

    /**
     * This method should call the appointment listing for patient
     */
    private void callAppointmentListAPI() {
        try {
            String mPatientId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);

            if (mAppointmentNo == null) {
                mAppointmentNo = AppConstants.STR_EMPTY_STRING;
            }

            if (mAppointmentDate == null) {
                mAppointmentDate = mActivity.getResources().getString(R.string.date_format_first_time);
            }
            String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPatientAppointmentListJson(currentPageIndex, mAppointmentNo, mAppointmentDate, mDoctorId, mPatientId,
                            mActivity.getResources().getString(R.string.tab_today),hospital_database));

            Call<PatientAppointmentModel> call = RetrofitClient.createService(ApiInterface.class).getPatientAppointmentList(requestBody);
            call.enqueue(new Callback<PatientAppointmentModel>() {
                @Override
                public void onResponse(@NonNull Call<PatientAppointmentModel> call, @NonNull Response<PatientAppointmentModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        stopProgressView();
                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                ArrayList<PatientAppointmentModel> appointmentModel = new ArrayList<>(Arrays
                                        .asList(new GsonBuilder().serializeNulls().create()
                                                .fromJson(new JsonParser().parse(jsonObject.getString(WebFields.DATA)).
                                                        getAsJsonArray(), PatientAppointmentModel[].class)));

                                if (mArrPatientAppointment != null && mArrPatientAppointment.size() > 0 &&
                                        mAdapterPatientAppointment != null) {
                                    mArrPatientAppointment.addAll(appointmentModel);
                                    mAdapterPatientAppointment.notifyDataSetChanged();
                                    lastFetchRecord = mArrPatientAppointment.size();
                                } else {
                                    mArrPatientAppointment = appointmentModel;
                                    setAdapterData();
                                    lastFetchRecord = mArrPatientAppointment.size();
                                    totalRecords = jsonObject.getInt(WebFields.ROW_COUNT);
                                    setLoadMoreClickListener();

                                    if (mRecyclerView.getVisibility() == View.GONE) {
                                        stopProgressView();
                                    }
                                }
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                            stopProgressView();
                            setViewVisibility();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PatientAppointmentModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    stopProgressView();
                    setViewVisibility();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mGridLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(mGridLayoutManager);

            stopProgressView();
            mAdapterPatientAppointment = new PatientAppointmentAdapter(mActivity, mRecyclerView, mArrPatientAppointment);
            mRecyclerView.setAdapter(mAdapterPatientAppointment);

            setViewVisibility();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the view visibility as per the array size
     */
    private void setViewVisibility() {
        if (mArrPatientAppointment.size() != 0) {
            mRelativeNoData.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mRelativeNoData.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    /**
     * Show progress view when page load for the first time
     */
    private void stopProgressView() {
        try {
            mSwipeRefreshView.setRefreshing(false);
            isRefresh = false;
            isLoadMore = false;
            RecyclerProgressView.stopProgress();
            mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            hideProgressBar();
        }
    }

    /**
     * Sets up the Patient Appointment Search Fragment
     */
    private void callToSearchPatientAppointmentFragment() {
        try {
            ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.header_search));
            Fragment fragment = new SearchPatientAppointmentFragment();
            FragmentManager fragmentManager =
                    ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_search_patient_appointment))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_search_patient_appointment))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Hide the progress bar
     */
    private void hideProgressBar() {
        try {
            if (mArrPatientAppointment != null && mArrPatientAppointment.size() > 0 &&
                    mArrPatientAppointment.get(mArrPatientAppointment.size() - 1) == null) {

                mArrPatientAppointment.remove(mArrPatientAppointment.size() - 1);
                mAdapterPatientAppointment.notifyItemRemoved(mArrPatientAppointment.size());

                mAdapterPatientAppointment.notifyDataSetChanged();
                mAdapterPatientAppointment.setLoaded();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RecyclerProgressView.stopProgress();
    }

    /**
     * On load more click listener
     */
    private void setLoadMoreClickListener() {
        mAdapterPatientAppointment.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (mArrPatientAppointment != null && mArrPatientAppointment.size() > 0 && mArrPatientAppointment.size() <
                        totalRecords) {
                    if (!mAppUtils.getConnectionState()) {
                        mRelativeNoInternet.setVisibility(View.VISIBLE);
                        mArrPatientAppointment.clear();
                        setViewVisibility();
                    } else {
                        mRelativeNoInternet.setVisibility(View.GONE);
                        if (!isRefresh) {
                            isLoadMore = true;
                            mArrPatientAppointment.add(null);
                            mAdapterPatientAppointment.notifyItemInserted(mArrPatientAppointment.size() - 1);

                            currentPageIndex = (mArrPatientAppointment.size() / Common.getPageSizeForPagination(mActivity)) + 1;
                            callAppointmentListAPI();
                        }
                    }
                }
            }
        });
    }
}