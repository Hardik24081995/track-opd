package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.trackopd.R;
import com.trackopd.adapter.HistoryTreatmentSuggestedAdapter;
import com.trackopd.adapter.SpecialInstructionsAdapter;
import com.trackopd.model.AddMedicationModel;
import com.trackopd.model.CloseFileModel;
import com.trackopd.model.DosageModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.MedicationModel;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.model.PrintReceiptModel;
import com.trackopd.model.SpecialInstructionsModel;
import com.trackopd.model.TempleteModel;
import com.trackopd.model.UOMModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.SessionManager;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TreatmentSuggestedFragment extends Fragment implements
        SpecialInstructionsAdapter.SingleClickListener, HistoryTreatmentSuggestedAdapter.onHistoryDiagnosisListener {

    CardView cardTemplete, cardManually;
    ArrayList<TempleteModel.Datum> templeteName = new ArrayList<>();
    ArrayList<String> nameArr = new ArrayList<>();
    private View mView, mViewEndDate, mViewDosage, mViewHowManyTimesADay, mViewHowMany, mViewUOM, mViewDuration, mViewMedicationType;
    private FragmentActivity mActivity;
    private ImageView mImageAddPrescriptions, mImageAddSurgery;
    private LinearLayout mLinearPrescriptions,
            mLinearSurgery, mLinearDosage, mLinearMedicationType, mLinearUOM, mLinearPrescriptionPrint;
    private Button mButtonSubmit;
    private RelativeLayout mRelativeNoDataFound;
    private RecyclerView mRecyclerView;
    private TextInputLayout mTextInputEndDate, mTextInputHowManyTimesADay, mTextInputHowMany, mTextInputDuration;
    private Spinner mSpinnerMedication, mSpinnerDosage, mSpinnerUOM, mSpinnerCategory, mSpinnerSurgeryName,
            mSpinnerSurgeryPadEntry, mSpinnerSurgeryWithinTime, mSpinnerSurgeryEye, mSpinnerSurgeryTwo;
    private EditText mEditTextSurgeryRemarks, mEditSurgeryManyDays;
    private RadioGroup mRadioGroupMedicationType;
    private RadioButton mRadioNone, mRadioDays, mRadioWeek;
    private Spinner spinnerTempetes;
    private RadioGroup radioGroupTemplete;
    private RadioButton rb_templete, rb_manual;
    private Spinner mSpinnerTemplete;
    private ArrayList<DosageModel> mArrDosageList;
    private ArrayList<MedicationModel> mArrMedicationList;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.Category> mArrCategory;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.Eye> mArrEye;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.SurgeryType> mArrSurgeryType;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.OptionSuggested> mArrOptionSuggested;
    private ArrayList<SpecialInstructionsModel> mArrSpecialInstructions;
    private ArrayList<UOMModel> mArrUOMList;
    private ArrayList<HistoryPreliminaryExaminationModel.Data> mArrHistory;

    private ArrayList<String> mArrDuration, mArrSurgeryCategory, mArrEyeName,
            mArrSurgery, mArrOption, mArrSurgeryTime, mArrUOM, mArrDosage, mArrMedication;

    private int mSelectedDosageIndex = 0, mSelectedCityIndex = 0, mType, mMedicationId, mDosageId,
            mSelectedUOMIndex, mUOMId, mTemplalteID, mSelectedMedicationIndex, mAppointmentId, mCurrentPage = 1,
            mHistoryDoctorID = -1, mSelectedCategory = 0, mSelectedEye = 0, mSelectedWithin = 0, mSelectedTempleteIndex = 0;

    private String mUserType, mSpecialInstructions, mMedicationType, mSelMedicationType, EndDate,
            mPatientId, mDoctorId, mTreatmentDate, Times;
    /**
     * Radio button on checked change listeners
     */
    private RadioGroup.OnCheckedChangeListener checkedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            RadioButton mRadioButton = group.findViewById(checkedId);
            mRadioButton.clearFocus();

            switch (group.getId()) {
                case R.id.radio_group_row_add_medication_type:

                    mType = group.getCheckedRadioButtonId();

                    if (mType == R.id.radio_row_add_medication_none) {
                        mMedicationType = mActivity.getResources().getString(R.string.text_none);
                    } else if (mType == R.id.radio_row_add_medication_days) {
                        mMedicationType = mActivity.getResources().getString(R.string.text_days);
                    } else if (mType == R.id.radio_row_add_medication_week) {
                        mMedicationType = mActivity.getResources().getString(R.string.text_week);
                    } else {
                        mMedicationType = mActivity.getResources().getString(R.string.text_month);
                    }
                    break;
            }
        }
    };
    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {
                case R.id.spinner_row_add_medication:
                    mSelectedMedicationIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedMedicationIndex != 0) {
                        mMedicationId = (Integer.parseInt(mArrMedicationList.get(mSelectedMedicationIndex - 1).getMedicationID()));
                        String mMedicationName = (mArrMedicationList.get(mSelectedMedicationIndex - 1).getMedicationName());
                        mSelMedicationType = (mArrMedicationList.get(mSelectedMedicationIndex - 1).getType());
                        Common.insertLog("mMedicationId::> " + mMedicationId);
                        Common.insertLog("mMedicationName::> " + mMedicationName);

                        if (!mSelMedicationType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_eye_drops))) {
                            mLinearDosage.setVisibility(View.GONE);
                            mViewDosage.setVisibility(View.GONE);
                            mTextInputEndDate.setVisibility(View.GONE);
                            mViewEndDate.setVisibility(View.GONE);
                            mTextInputHowMany.setVisibility(View.GONE);
                            mViewHowMany.setVisibility(View.GONE);
                            mLinearUOM.setVisibility(View.GONE);
                            mViewUOM.setVisibility(View.GONE);
                            mTextInputHowManyTimesADay.setVisibility(View.GONE);
                            mViewHowManyTimesADay.setVisibility(View.GONE);
                            mTextInputDuration.setVisibility(View.GONE);
                            mViewDuration.setVisibility(View.GONE);
                            mLinearMedicationType.setVisibility(View.GONE);
                            mViewMedicationType.setVisibility(View.GONE);
                        } else {
                            mLinearDosage.setVisibility(View.GONE);
                            mViewDosage.setVisibility(View.GONE);
                            mTextInputEndDate.setVisibility(View.GONE);
                            mViewEndDate.setVisibility(View.GONE);
                            mTextInputHowMany.setVisibility(View.GONE);
                            mViewHowMany.setVisibility(View.GONE);
                            mLinearUOM.setVisibility(View.GONE);
                            mViewUOM.setVisibility(View.GONE);
                            mTextInputHowManyTimesADay.setVisibility(View.GONE);
                            mViewHowManyTimesADay.setVisibility(View.GONE);
                            mTextInputDuration.setVisibility(View.GONE);
                            mViewDuration.setVisibility(View.GONE);
                            mLinearMedicationType.setVisibility(View.GONE);
                            mViewMedicationType.setVisibility(View.GONE);
                        }
                    }
                    break;

                case R.id.spinner_row_add_medication_dosage:
                    mSelectedDosageIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedDosageIndex != 0) {
                        mDosageId = (Integer.parseInt(mArrDosageList.get(mSelectedDosageIndex - 1).getDosageID()));
                        String mDosageName = (mArrDosageList.get(mSelectedDosageIndex - 1).getTitle());
                        Common.insertLog("mDosageId::> " + mDosageId);
                        Common.insertLog("mDosageName::> " + mDosageName);
                    }
                    break;

                case R.id.spinner_row_add_medication_uom:
                    mSelectedUOMIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedUOMIndex != 0) {
                        mUOMId = (Integer.parseInt(mArrUOMList.get(mSelectedUOMIndex - 1).getUOMID()));
                        String mUOMName = (mArrUOMList.get(mSelectedUOMIndex - 1).getUOM());
                        Common.insertLog("mUOMId::> " + mUOMId);
                        Common.insertLog("mUOMName::> " + mUOMName);
                    }
                    break;

                case R.id.spinnerTempletes:
                    mSelectedTempleteIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedTempleteIndex != 0) {
                        mTemplalteID = (Integer.parseInt(templeteName.get(mSelectedTempleteIndex - 1).getTemplateID()));
                        String mTempleteName = (templeteName.get(mSelectedTempleteIndex - 1).getTemplateName());
                        Common.insertLog("mUOMId::> " + mTemplalteID);
                        Common.insertLog("mUOMName::> " + mTempleteName);

                        setDataTemplate(String.valueOf(mTemplalteID));
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };
    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_treatment_suggested_prescription:
                    //callAddPrescription();
                   /* mArrMedication.clear();
                    mArrMedicationList.clear();
                    mArrDosage.clear();
                    mArrDosageList.clear();
                    mArrUOM.clear();
                    mArrUOMList.clear();
                    mArrSpecialInstructions.clear();*/
                    doAddMedication();
                    break;
                case R.id.image_treatment_suggested_surgery_suggested:

                    if (mLinearSurgery.getVisibility() == View.GONE) {
                        mLinearSurgery.setVisibility(View.GONE);
                        mImageAddSurgery.setImageResource(R.drawable.ic_down_arrow);
                    } else {
                        mImageAddSurgery.setImageResource(R.drawable.ic_up_arrow);
                        mLinearSurgery.setVisibility(View.GONE);
                    }

                    break;
                case R.id.button_treatment_suggested_submit:
                    if (checkAddValidation()) {
                        callToAddMedication();
                    }

                    if (checkVadlidationSurgery()) {
                        callToAddSurgery();
                    }

                    break;

                case R.id.linear_treatment_suggested_prescription_print:
                    callAlert();
                    break;
            }
        }
    };


    private void callAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                mActivity);
        builder.setTitle("Language Options");
        builder.setMessage("Choose your desired language");
        builder.setNegativeButton("Hindi",
                (dialog, which) -> {
                    String lang = "Hindi";
                    callToPrintPdf(lang);
                });
        builder.setPositiveButton("Gujarati",
                (dialog, which) -> {
                    String lang = "Gujarati";
                    callToPrintPdf(lang);
                });
        builder.show();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_treatment_suggested, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        setHasOptionsMenu(true);

        mArrDosageList = new ArrayList<>();
        mArrMedicationList = new ArrayList<>();
        mArrCategory = new ArrayList<>();
        mArrEye = new ArrayList<>();
        mArrSurgeryType = new ArrayList<>();
        mArrOptionSuggested = new ArrayList<>();
        mArrSpecialInstructions = new ArrayList<>();
        mArrHistory = new ArrayList<>();

        mArrUOMList = new ArrayList<>();
        mArrDosage = new ArrayList<>();
        mArrUOM = new ArrayList<>();
        mArrMedication = new ArrayList<>();
        mArrDuration = new ArrayList<>();
        mArrSurgeryCategory = new ArrayList<>();
        mArrEyeName = new ArrayList<>();
        mArrSurgery = new ArrayList<>();
        mArrOption = new ArrayList<>();
        mArrSurgeryTime = new ArrayList<>();

        String[] strArrDuration = mActivity.getResources().getStringArray(R.array.array_duration);
        mArrDuration = new ArrayList<>(Arrays.asList(strArrDuration)); //new ArrayList is only needed if you absolutely need an ArrayList

        String[] strArrSurgeryTime = mActivity.getResources().getStringArray(R.array.array_surgery_time);
        mArrSurgeryTime = new ArrayList<>(Arrays.asList(strArrSurgeryTime)); //new ArrayList is only needed if you absolutely need an ArrayList

        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        getBundle();
        getIds();
        setData();
        setRegListeners();
        callToMedicationAPI();
        callToDosageAPI();
        callToUOMAPI();
        callToSpecialInstructionsAPI();

        callToPreliminaryExaminationAPI();
        getTempleteData();

        mEditTextSurgeryRemarks.setFilters(Common.getFilter());

        return mView;
    }

    private void getTempleteData() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.getTempleteJson(hospital_database));

            Call<TempleteModel> call = RetrofitClient.createService(ApiInterface.class).getTempleteAPI(body);
            call.enqueue(new Callback<TempleteModel>() {
                @Override
                public void onResponse(@NonNull Call<TempleteModel> call, @NonNull Response<TempleteModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null &&
                                    response.body().getError() == AppConstants.API_SUCCESS_ERROR) {

                                templeteName.addAll(response.body().getData());
                                setTempleteSpinnerAdapter();
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<TempleteModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    public void setTempleteSpinnerAdapter() {
        try {
            nameArr.add(0, "Select Template");
            if (templeteName.size() > 0) {
                for (TempleteModel.Datum Services : templeteName) {
                    nameArr.add(Services.getTemplateName());
                }
            }

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, nameArr) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedTempleteIndex) {
                        // Set spinner selected popup item's text color

                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerTemplete.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mAppointmentId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mDoctorId = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);
                mTreatmentDate = bundle.getString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* Declared Id on Widest
     */
    private void getIds() {
        // Image View
        mImageAddPrescriptions = mView.findViewById(R.id.image_treatment_suggested_prescription);
        mImageAddSurgery = mView.findViewById(R.id.image_treatment_suggested_surgery_suggested);

        //Linear Layout
        mLinearSurgery = mView.findViewById(R.id.linear_treatment_suggested_surgery_suggested_container);
        mLinearPrescriptions = mView.findViewById(R.id.linear_treatment_suggested_prescription_container);
        mLinearPrescriptionPrint = mView.findViewById(R.id.linear_treatment_suggested_prescription_print);

        //Button
        mButtonSubmit = mView.findViewById(R.id.button_treatment_suggested_submit);

        //Relative Layout
        mRelativeNoDataFound = mView.findViewById(R.id.relative_treatment_suggested_no_data_found);

        mSpinnerCategory = mView.findViewById(R.id.spinner_treatment_suggested_surgery_suggested_category);
        mSpinnerSurgeryName = mView.findViewById(R.id.spinner_treatment_suggested_surgery_suggested_surgery);
        mSpinnerSurgeryTwo = mView.findViewById(R.id.spinner_treatment_suggested_surgery_suggested_surgery_two);
        mSpinnerSurgeryPadEntry = mView.findViewById(R.id.spinner_treatment_suggested_surgery_suggested_pad_entry);
        mSpinnerSurgeryWithinTime = mView.findViewById(R.id.spinner_treatment_suggested_surgery_suggested_within_time);
        mSpinnerSurgeryEye = mView.findViewById(R.id.spinner_treatment_suggested_surgery_eye);

        mEditTextSurgeryRemarks = mView.findViewById(R.id.spinner_treatment_suggested_surgery_suggested_remarks);
        mEditSurgeryManyDays = mView.findViewById(R.id.edit_treatment_suggested_many_times);


        //Set Defulat Value Medication Type
        mMedicationType = mActivity.getResources().getString(R.string.text_none);

        mLinearSurgery.setVisibility(View.GONE);
        mImageAddSurgery.setImageResource(R.drawable.ic_down_arrow);

        radioGroupTemplete = mView.findViewById(R.id.radioGroupTemplete);
        rb_templete = mView.findViewById(R.id.rb_templete);
        rb_manual = mView.findViewById(R.id.rb_manual);

        mSpinnerTemplete = mView.findViewById(R.id.spinnerTempletes);
        mSpinnerTemplete.setOnItemSelectedListener(onItemSelectedListener);

        cardManually = mView.findViewById(R.id.cardManually);

        // callAddSurgerySuggested();
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {

            radioGroupTemplete.setOnCheckedChangeListener((group, checkedId) -> {
                RadioButton radioButton = mView.findViewById(checkedId);

                if (radioButton.getId() == R.id.rb_templete) {
                    mSpinnerTemplete.setVisibility(View.VISIBLE);
                } else if (radioButton.getId() == R.id.rb_manual) {
                    mSpinnerTemplete.setVisibility(View.GONE);
                }
            });

            mImageAddSurgery.setOnClickListener(clickListener);
            mImageAddPrescriptions.setOnClickListener(clickListener);

            SessionManager manager = new SessionManager(mActivity);
            String closeFile = manager.getPreferences("ClosingFlags", "");

            if (closeFile.equalsIgnoreCase("Yes")) {
                mButtonSubmit.setClickable(false);
                Toast.makeText(mActivity, "Patient File Closed", Toast.LENGTH_SHORT).show();
            } else {
                mButtonSubmit.setOnClickListener(clickListener);
            }

            mLinearPrescriptionPrint.setOnClickListener(clickListener);

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Data Value
     */
    private void setData() {
        String currentDate = Common.setCurrentDate(mActivity);
        String covertTreatmentDate = Common.convertDateUsingDateFormat(mActivity, mTreatmentDate,
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen),
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));

        if (currentDate.equalsIgnoreCase(covertTreatmentDate)) {
            mRelativeNoDataFound.setVisibility(View.GONE);
            //.setDataCompolains();
        } else {
            mRelativeNoDataFound.setVisibility(View.VISIBLE);
        }

        ArrayAdapter adapter_surgery_within = new ArrayAdapter<String>(mActivity,
                android.R.layout.simple_spinner_dropdown_item, mArrDuration) {
            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                // Cast the drop down items (popup items) as text view
                TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                // Set the text color of drop down items
                tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                // If this item is selected item
                if (position == mSelectedWithin) {
                    // Set spinner selected popup item's text color
                    tv.setTextColor(Common.setThemeColor(mActivity));
                }
                // Return the modified view
                return tv;
            }
        };
        adapter_surgery_within.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerSurgeryWithinTime.setAdapter(adapter_surgery_within);
        mSpinnerSurgeryWithinTime.setSelection(0, true);

    }

    /**
     * this PDF wise  Add Prescription
     * Add Prescription on Layout
     */
    private void callAddPrescription() {
        View view = LayoutInflater.from(mActivity).
                inflate(R.layout.row_treatment_suggested_prescription, null, false);

        //Image View
        ImageView mImageRemove = view.findViewById(R.id.image_treatment_suggested_prescription_remove);
        ImageView mImagePrint = view.findViewById(R.id.image_treatment_suggested_prescription_print);
        ImageView mImageAddDosage = view.findViewById(R.id.image_treatment_suggested_prescription_dosage_duration);

        LinearLayout mLinearDosage = view.findViewById(R.id.linear_treatment_suggested_prescription_dosage_container);
        //Spinner
        Spinner mSpinnerMedicine = view.findViewById(R.id.spinner_treatment_suggested_prescription_medicine);
        Spinner mSpinnerEye = view.findViewById(R.id.spinner_treatment_suggested_prescription_eye);
        EditText mEditStartDate = view.findViewById(R.id.edit_text_input_row_treatment_suggested_prescription_start_date);
        EditText mEditEndDate = view.findViewById(R.id.edit_text_input_row_treatment_suggested_prescription_end_date);


        //Recyler View
        RecyclerView mRecyclerView = view.findViewById(R.id.recycler_view_row_add_medication_special_instructions);

        if (mArrMedicationList.size() > 0) {
            for (MedicationModel medicationModel : mArrMedicationList) {
                String mMedication = medicationModel.getMedicationName() + " (" + medicationModel.getSuggestionDosage() + ")";
                mArrMedication.add(mMedication);
            }
        }

        // ToDo: Sets the spinner color as per the theme applied
        mSpinnerMedicine.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        mSpinnerEye.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

        ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrMedication) {
            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                // Cast the drop down items (popup items) as text view
                TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                // Set the text color of drop down items
                tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                // If this item is selected item
                if (position == mSelectedCityIndex) {
                    // Set spinner selected popup item's text color
                    tv.setTextColor(Common.setThemeColor(mActivity));
                }
                // Return the modified view
                return tv;
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerMedicine.setAdapter(adapter);

        ArrayAdapter adapter_eye = new ArrayAdapter<String>(mActivity,
                android.R.layout.simple_spinner_item, mArrEyeName) {
            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                // Cast the drop down items (popup items) as text view
                TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                // Set the text color of drop down items
                tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                // If this item is selected item
                if (position == mSelectedCityIndex) {
                    // Set spinner selected popup item's text color
                    tv.setTextColor(Common.setThemeColor(mActivity));
                }
                // Return the modified view
                return tv;
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerEye.setAdapter(adapter_eye);

        //Set Special Instruction

        setAdapterData(mRecyclerView);

        //TODO: set on Click Listener
        mImageRemove.setOnClickListener(v -> mLinearPrescriptions.removeView(view));

        mImageAddDosage.setOnClickListener(v -> openDosageDurationDialog(mLinearDosage));

        mEditStartDate.setOnClickListener(v -> Common.openDateTimePicker(mActivity, mEditStartDate));
        mEditEndDate.setOnClickListener(v -> Common.openDateTimePicker(mActivity, mEditEndDate));
        mLinearPrescriptions.addView(view);
    }

    /**
     * Open Medication Dialog box
     */
    private void openMedicationDialog() {
        try {
            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.custom_dialog_treatment_suggested_dosage_duration);

            //LinearLayout
            LinearLayout mLayoutContainer = dialog.
                    findViewById(R.id.linear_custom_dialog_treatment_suggested_dosage_duration_container);

            //Image View
            ImageView mImageAddDosageDuration = dialog.
                    findViewById(R.id.image_custom_dialog_treatment_suggested_dosage_duration_plus);

            //Button
            Button btnSubmitDosageDuration = dialog.
                    findViewById(R.id.button_custom_dialog_treatment_suggested_dosage_duration_submit);

            // ToDo: Button Ok Click Listener
            btnSubmitDosageDuration.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();

                }
            });

            mImageAddDosageDuration.setOnClickListener(v -> dialogToAddDosageandDuration(mLayoutContainer));


            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Add Surgery Suggested on Layout
     */
    private void callAddSurgerySuggested() {

        View view = LayoutInflater.from(mActivity.getBaseContext()).
                inflate(R.layout.row_treatment_suggested_surgery_suggested, null, false);


        ImageView mImageRemoveSurgery = view.findViewById(R.id.image_treatment_suggested_surgery_suggested_remove);

        //set Visibility
        mImageRemoveSurgery.setVisibility(View.GONE);

        // ToDo: Sets the spinner color as per the theme applied
        mSpinnerCategory.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        mSpinnerSurgeryName.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        mSpinnerSurgeryTwo.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        mSpinnerSurgeryPadEntry.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
        mSpinnerSurgeryWithinTime.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

        //TODO: Set On Click Listener
        mImageRemoveSurgery.setOnClickListener(v -> mLinearSurgery.removeView(view));
        mLinearSurgery.addView(view);
    }

    /**
     * Open up the Appointment dialog
     *
     * @param mLinearDosage
     */
    @SuppressLint("SetTextI18n")
    public void openDosageDurationDialog(LinearLayout mLinearDosage) {
        try {
            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.custom_dialog_treatment_suggested_dosage_duration);

            //LinearLayout
            LinearLayout mLayoutContainer = dialog.
                    findViewById(R.id.linear_custom_dialog_treatment_suggested_dosage_duration_container);

            //Image View
            ImageView mImageAddDosageDuration = dialog.
                    findViewById(R.id.image_custom_dialog_treatment_suggested_dosage_duration_plus);
            //Button
            Button btnSubmitDosageDuration = dialog.
                    findViewById(R.id.button_custom_dialog_treatment_suggested_dosage_duration_submit);

            // ToDo: Button Ok Click Listener
            btnSubmitDosageDuration.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if (mLayoutContainer.getChildCount() > 0) {
                        for (int a = 0; a < mLayoutContainer.getChildCount(); a++) {
                            View childView = mLayoutContainer.getChildAt(a);
                            Spinner mSpinnerDosage = childView.findViewById(R.id.spinner_row_dosage_duration_item_dosage);
                            Spinner mSpinnerDuration = childView.findViewById(R.id.spinner_row_dosage_duration_item_duration);

                            int selected_dosage = mSpinnerDosage.getSelectedItemPosition();
                            int selected_duration = mSpinnerDuration.getSelectedItemPosition();

                            addLayoutInflatePrescription(mLinearDosage, mArrDosageList.get(selected_dosage),
                                    mArrDuration.get(selected_duration));
                        }
                    }
                }
            });

            mImageAddDosageDuration.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialogToAddDosageandDuration(mLayoutContainer);
                }
            });
            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Layout inflater Dosage Duration on Prescription
     *
     * @param mLinearDosage -Linear Layout for Dosage
     * @param dosageModel   - Dosage Detail
     * @param duration      - Duration
     */
    private void addLayoutInflatePrescription(LinearLayout mLinearDosage, DosageModel dosageModel, String duration) {
        try {

            View view = LayoutInflater.from(mActivity).
                    inflate(R.layout.row_dosage_duration_prescription_item, null, false);
            TextView mTextDosage = view.findViewById(R.id.text_dosage_duration_prescription_item_dosage);
            TextView mTextDuration = view.findViewById(R.id.text_dosage_duration_prescription_item_duration);
            ImageView mImageRemove = view.findViewById(R.id.image_dosage_duration_prescription_item_remove);

            mTextDosage.setText(dosageModel.getTitle());
            mTextDuration.setText(duration);

            mImageRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLinearDosage.removeView(view);
                }
            });

            mLinearDosage.addView(view);
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * @param mLayoutContainer - Linear Layout Inflater
     */
    private void dialogToAddDosageandDuration(LinearLayout mLayoutContainer) {
        try {
            @SuppressLint("InflateParams")
            View view = LayoutInflater.from(mActivity).
                    inflate(R.layout.row_dosage_duration_item, null, false);

            Spinner mSpinnerDosage = view.findViewById(R.id.spinner_row_dosage_duration_item_dosage);
            Spinner mSpinnerDuration = view.findViewById(R.id.spinner_row_dosage_duration_item_duration);


            mSpinnerDosage.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrDosage) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedDosageIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };


            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerDosage.setAdapter(adapter);

            mSpinnerDosage.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapterDuration = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrDuration) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedDosageIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };

            adapterDuration.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerDuration.setAdapter(adapterDuration);

            mLayoutContainer.addView(view);

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * This method should call the Dosage API
     */
    private void callToDosageAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.setDosageListJson(hospital_database));

            Call<DosageModel> call = RetrofitClient.createService(ApiInterface.class).getDosage(body);
            call.enqueue(new Callback<DosageModel>() {
                @Override
                public void onResponse(@NonNull Call<DosageModel> call, @NonNull Response<DosageModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getData() != null) {

                                try {
                                    mArrDosageList.addAll(response.body().getData());
                                    if (mArrDosageList.size() > 0) {
                                    }
                                } catch (Exception e) {
                                    Common.insertLog(e.getMessage());
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DosageModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Medication API
     */
    private void callToMedicationAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setMedicationListJson(hospital_database));

            Call<MedicationModel> call = RetrofitClient.createService(ApiInterface.class).getMedicationList(body);
            call.enqueue(new Callback<MedicationModel>() {
                @Override
                public void onResponse(@NonNull Call<MedicationModel> call, @NonNull Response<MedicationModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrMedicationList.addAll(response.body().getData());

                                mArrMedication.add(0, mActivity.getResources().getString(R.string.spinner_select_medication));
                                if (mArrMedicationList.size() > 0) {
                                    for (MedicationModel medicationModel : mArrMedicationList) {
                                        String mMedication = medicationModel.getMedicationName() + " (" + medicationModel.getSuggestionDosage() + ")";
                                        mArrMedication.add(mMedication);
                                    }
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<MedicationModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Preliminary Examination API
     */
    private void callToPreliminaryExaminationAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPreliminaryExaminationDetailsJson(hospital_database));

            Call<PreliminaryExaminationDetailsModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationDetails(body);
            call.enqueue(new Callback<PreliminaryExaminationDetailsModel>() {
                @Override
                public void onResponse(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Response<PreliminaryExaminationDetailsModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null &&
                                    response.body().getError() == AppConstants.API_SUCCESS_ERROR) {

                                PreliminaryExaminationDetailsModel.Data data =
                                        response.body().getData();

                                mArrCategory.addAll(data.getCategory());
                                setCategoryArrayList();

                                mArrSurgeryType.addAll(data.getSurgeryType());
                                setSurgeryArrayList();

                                mArrEye.addAll(data.getEye());
                                setEyeArrayList();

                                mArrOptionSuggested.addAll(data.getOptionSuggested());
                                setOptionSuggestedArrayList();

                                callToHistoryPreliminaryExamination();
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Category
     */
    private void setCategoryArrayList() {
        try {
            if (mArrCategory != null && mArrCategory.size() > 0) {
                mArrSurgeryCategory.add(mActivity.getString(R.string.spinner_select_category));
                for (int i = 0; i < mArrCategory.size(); i++) {
                    mArrSurgeryCategory.add(mArrCategory.get(i).getCategory());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerCategory.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity,
                    android.R.layout.simple_spinner_dropdown_item, mArrSurgeryCategory) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCategory) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerCategory.setAdapter(adapter);
            mSpinnerCategory.setSelection(0, true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Category
     */
    private void setSurgeryArrayList() {
        try {
            if (mArrSurgeryType != null && mArrSurgeryType.size() > 0) {
                mArrSurgery.add(mActivity.getString(R.string.spinner_select_surgery));
                for (int i = 0; i < mArrSurgeryType.size(); i++) {
                    mArrSurgery.add(mArrSurgeryType.get(i).getSurgeryName());
                }
            }


            mSpinnerSurgeryName.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerSurgeryTwo.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);


            ArrayAdapter adapter_surgery_two = new ArrayAdapter<String>(mActivity,
                    android.R.layout.simple_spinner_dropdown_item, mArrSurgery) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCategory) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter_surgery_two.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerSurgeryTwo.setAdapter(adapter_surgery_two);
            mSpinnerSurgeryTwo.setSelection(0, true);


            ArrayAdapter adapter_surgery_name = new ArrayAdapter<String>(mActivity,
                    android.R.layout.simple_spinner_dropdown_item, mArrSurgery) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCategory) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter_surgery_name.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerSurgeryName.setAdapter(adapter_surgery_name);
            mSpinnerSurgeryName.setSelection(0, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Array List Eye
     */
    private void setEyeArrayList() {
        try {
            if (mArrEye != null && mArrEye.size() > 0) {
                mArrEyeName.add(mActivity.getString(R.string.spinner_select_eye));
                for (int i = 0; i < mArrEye.size(); i++) {
                    mArrEyeName.add(mArrEye.get(i).getEye());
                }
            }

            mSpinnerSurgeryEye.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);


            ArrayAdapter adapter_surgery_eye = new ArrayAdapter<String>(mActivity,
                    android.R.layout.simple_spinner_dropdown_item, mArrEyeName) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedEye) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter_surgery_eye.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerSurgeryEye.setAdapter(adapter_surgery_eye);
            mSpinnerSurgeryEye.setSelection(0, true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Array List Eye
     */
    private void setOptionSuggestedArrayList() {
        try {
            if (mArrOptionSuggested != null && mArrOptionSuggested.size() > 0) {

                mArrOption.add(mActivity.getString(R.string.spinner_select_option));
                for (int i = 0; i < mArrOptionSuggested.size(); i++) {
                    mArrOption.add(mArrOptionSuggested.get(i).getOpetionSuggested());
                }
            }

            ArrayAdapter<String> adapter_option = new ArrayAdapter<>
                    (mActivity, android.R.layout.simple_selectable_list_item, mArrOption);
            mSpinnerSurgeryPadEntry.setAdapter(adapter_option);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Special Instructions API
     */
    private void callToSpecialInstructionsAPI() {
        try {
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setSpecialInstructionsListJson(mDatabaseName));

            Call<SpecialInstructionsModel> call = RetrofitClient.createService(ApiInterface.class).getSpecialInstructionsList(body);
            call.enqueue(new Callback<SpecialInstructionsModel>() {
                @Override
                public void onResponse(@NonNull Call<SpecialInstructionsModel> call, @NonNull Response<SpecialInstructionsModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                mArrSpecialInstructions.addAll(response.body().getData());
                            }

                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<SpecialInstructionsModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setAdapterData(RecyclerView recyclerView) {
        try {
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.setHasFixedSize(true);
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mActivity);
            recyclerView.setLayoutManager(mLinearLayoutManager);

            SpecialInstructionsAdapter mAdapterSpecialInstructions = new SpecialInstructionsAdapter(mActivity, mUserType, mArrSpecialInstructions);
            recyclerView.setAdapter(mAdapterSpecialInstructions);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the UOM API
     */
    private void callToUOMAPI() {
        try {
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setUOMListJson(mDatabaseName));

            Call<UOMModel> call = RetrofitClient.createService(ApiInterface.class).getUOMList(body);
            call.enqueue(new Callback<UOMModel>() {
                @Override
                public void onResponse(@NonNull Call<UOMModel> call, @NonNull Response<UOMModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrUOMList.addAll(response.body().getData());

                                mArrUOM.add(0, mActivity.getResources().getString(R.string.spinner_select_dosage_form));
                                if (mArrUOMList.size() > 0) {
                                    for (UOMModel uomModel : mArrUOMList) {
                                        String mUOM = uomModel.getUOM();
                                        mArrUOM.add(mUOM);
                                    }
                                }
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<UOMModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    //setsMedicationAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should add the medication dynamically
     */
    private void doAddMedication() {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addView = layoutInflater.inflate(R.layout.row_medication_item, null);

            // Linear Layouts
            mLinearDosage = addView.findViewById(R.id.linear_row_add_medication_dosage);
            mLinearMedicationType = addView.findViewById(R.id.linear_row_add_medication_type);
            mLinearUOM = addView.findViewById(R.id.linear_row_add_medication_uom);
            LinearLayout mLinearDosages = addView.findViewById(R.id.linear_medication_item_add_dosage_continer);

            // Spinners
            mSpinnerMedication = addView.findViewById(R.id.spinner_row_add_medication);
            mSpinnerDosage = addView.findViewById(R.id.spinner_row_add_medication_dosage);
            mSpinnerUOM = addView.findViewById(R.id.spinner_row_add_medication_uom);

            // Text Input Layouts
            mTextInputEndDate = addView.findViewById(R.id.text_input_row_add_medication_end_date);
            mTextInputHowManyTimesADay = addView.findViewById(R.id.text_input_row_add_medication_how_many_times_a_day);
            mTextInputHowMany = addView.findViewById(R.id.text_input_row_add_medication_how_many);
            mTextInputDuration = addView.findViewById(R.id.text_input_row_add_medication_duration);

            // Edit Texts Views
            final EditText mEditStartDate = addView.findViewById(R.id.edit_row_add_medication_start_date);
            final EditText mEditEndDate = addView.findViewById(R.id.edit_row_add_medication_end_date);

            // Views
            mViewDosage = addView.findViewById(R.id.view_row_add_medication_dosage);
            mViewEndDate = addView.findViewById(R.id.view_row_add_medication_end_date);
            mViewHowManyTimesADay = addView.findViewById(R.id.view_row_add_medication_how_many_times_a_day);
            mViewHowMany = addView.findViewById(R.id.view_row_add_medication_how_many);
            mViewUOM = addView.findViewById(R.id.view_row_add_medication_uom);
            mViewDuration = addView.findViewById(R.id.view_row_add_medication_duration);
            mViewMedicationType = addView.findViewById(R.id.view_row_add_medication_type);

            // Radio Group
            mRadioGroupMedicationType = addView.findViewById(R.id.radio_group_row_add_medication_type);

            // Radio Buttons
            mRadioNone = addView.findViewById(R.id.radio_row_add_medication_none);
            mRadioDays = addView.findViewById(R.id.radio_row_add_medication_days);
            mRadioWeek = addView.findViewById(R.id.radio_row_add_medication_week);

            // Image Views
            ImageView mImageRemove = addView.findViewById(R.id.image_row_add_medication_item_remove);
            ImageView mImageDosage = addView.findViewById(R.id.image_medication_item_add_dosage);


            // Recycler View
            mRecyclerView = addView.findViewById(R.id.recycler_view_row_add_medication_special_instructions);

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerMedication.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerDosage.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

            // ToDo: Doctor - Spinner Item Select Listener
            mSpinnerMedication.setOnItemSelectedListener(onItemSelectedListener);
            mSpinnerDosage.setOnItemSelectedListener(onItemSelectedListener);

            // ToDo: Radio Button Click Listeners
            mRadioGroupMedicationType.setOnCheckedChangeListener(checkedChangeListener);

            // Sets up the data
            mEditStartDate.setText(Common.setCurrentDate(mActivity));
            mEditEndDate.setText(Common.setCurrentDate(mActivity));


            setsMedicationAdapter(mSpinnerMedication);
            setsUOMAdapter(mSpinnerUOM);
            setsDosageAdapter();
            setAdapterData();


            // ToDo: Edit Text Start Date Click Listener
            mEditStartDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Common.openDatePicker(mActivity, mEditStartDate);
                }
            });

            // ToDo: Edit Text End Date Click Listener
            mEditEndDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Common.openDatePicker(mActivity, mEditEndDate);
                }
            });

            // ToDo: Item remove Text End Date Click Listener
            mImageRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLinearPrescriptions.removeView(addView);
                    showPrintPrescription();
                }
            });

            // ToDo: Item remove Text End Date Click Listener
            mImageDosage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // mLinearPrescriptions.removeView(addView);
                    AddDosage(mLinearDosages);
                }
            });
            mLinearPrescriptions.addView(addView, 0);
            showPrintPrescription();//show Print Option

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Add Dosage on Prescription
     *
     * @param mLinearDosage
     */
    private void AddDosage(LinearLayout mLinearDosage) {
        LayoutInflater layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view_dosage = layoutInflater.inflate(R.layout.row_treatment_suggested_prescription_dosage_item, null, false);

        EditText mTimes = view_dosage.findViewById(R.id.edit_row_prescription_dosage_how_times);
        Spinner mSpinnerTime = view_dosage.findViewById(R.id.spinner_row_treatment_suggested_prescription_dosage_iten_time);
        EditText editTextTimes = view_dosage.findViewById(R.id.edit_prescription_number);

        ImageView mImageRemove = view_dosage.findViewById(R.id.image_prescription_dosage_time_remove);

        ArrayAdapter adapter_surgery_within = new ArrayAdapter<String>
                (mActivity,
                        android.R.layout.simple_spinner_dropdown_item, mArrDuration) {
            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                // Cast the drop down items (popup items) as text view
                TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                // Set the text color of drop down items
                tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                // If this item is selected item
                if (position == mSelectedWithin) {
                    // Set spinner selected popup item's text color
                    tv.setTextColor(Common.setThemeColor(mActivity));
                }
                // Return the modified view
                return tv;
            }
        };
        adapter_surgery_within.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerTime.setAdapter(adapter_surgery_within);


        mImageRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLinearDosage.removeView(view_dosage);
            }
        });

        mLinearDosage.addView(view_dosage);
    }

    /**
     * Add Dosage on Prescription
     *
     * @param mLinearDosage
     * @param medication
     */
    private void AddEditDosage(LinearLayout mLinearDosage, HistoryPreliminaryExaminationModel.Medication medication) {
        LayoutInflater layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view_dosage = layoutInflater.inflate(R.layout.row_treatment_suggested_prescription_dosage_item, null, false);

        EditText mTimes = view_dosage.findViewById(R.id.edit_row_prescription_dosage_how_times);
        Spinner mSpinnerTime = view_dosage.findViewById(R.id.spinner_row_treatment_suggested_prescription_dosage_iten_time);

        ImageView mImageRemove = view_dosage.findViewById(R.id.image_prescription_dosage_time_remove);

        ArrayAdapter adapter_surgery_within = new ArrayAdapter<String>
                (mActivity,
                        android.R.layout.simple_spinner_dropdown_item, mArrDuration) {
            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                // Cast the drop down items (popup items) as text view
                TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                // Set the text color of drop down items
                tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                // If this item is selected item
                if (position == mSelectedWithin) {
                    // Set spinner selected popup item's text color
                    tv.setTextColor(Common.setThemeColor(mActivity));
                }
                // Return the modified view
                return tv;
            }
        };
        adapter_surgery_within.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerTime.setAdapter(adapter_surgery_within);

        for (int s = 0; s < mArrDuration.size(); s++) {
            if (mArrDuration.get(s).equals(medication)) {
                mSpinnerTime.setSelection(s);
            }
        }

        mTimes.setText(medication.getTotalTimes());


        mImageRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLinearDosage.removeView(view_dosage);
                showPrintPrescription();
            }
        });

        mLinearDosage.addView(view_dosage);
    }

    private void showPrintPrescription() {
        if (mLinearPrescriptions.getChildCount() == 0) {
            mLinearPrescriptionPrint.setVisibility(View.GONE);
        } else {
            mLinearPrescriptionPrint.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsMedicationAdapter(Spinner spinner) {
        try {

            // ToDo: Sets the spinner color as per the theme applied
            spinner.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrMedication) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedMedicationIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsDosageAdapter() {
        try {

            mSpinnerDosage.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrDosage) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedDosageIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerDosage.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsUOMAdapter(Spinner spinner) {
        try {
            // ToDo: Sets the spinner color as per the theme applied
            spinner.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrUOM) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedUOMIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setNestedScrollingEnabled(false);
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(mLinearLayoutManager);

            SpecialInstructionsAdapter mAdapterSpecialInstructions = new SpecialInstructionsAdapter(mActivity, mUserType, mArrSpecialInstructions);
            mRecyclerView.setAdapter(mAdapterSpecialInstructions);
            mAdapterSpecialInstructions.setOnItemClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSpecialInstructionsClickListener(String mFinalSpecialInstructions) {
        mSpecialInstructions = mFinalSpecialInstructions;
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    public boolean checkAddValidation() {
        boolean status = true;

//        if (Times.equalsIgnoreCase("")) {
//            status = false;
//            Toast.makeText(mActivity, "Enter Duration Time", Toast.LENGTH_SHORT).show();
//        }

        if (mLinearPrescriptions.getChildCount() == 0) {
            status = false;
            // Common.setCustomToast(mActivity, getString(R.string.error_add_medication));
        } else {
            for (int i = 0; i < mLinearPrescriptions.getChildCount(); i++) {
                View view = mLinearPrescriptions.getChildAt(i);

                LinearLayout mLinearDosages = view.findViewById(R.id.linear_medication_item_add_dosage_continer);

                if (mLinearDosages.getChildCount() == 0) {
                    status = false;
                    Common.setCustomToast(mActivity, mActivity.getString(R.string.error_dosage_time));
                }

                Spinner mSpinnerMedication = view.findViewById(R.id.spinner_row_add_medication);

                if (mSpinnerMedication.getSelectedItemPosition() == 0) {
                    status = false;
                    Common.setCustomToast(mActivity, getString(R.string.error_select_medication));
                    mSpinnerMedication.requestFocus();
                }

                if (!status) {
                    return status;
                }

               /* // Edit Texts
                EditText mEditStartDate = view.findViewById(R.id.edit_row_add_medication_start_date);
                EditText mEditHowMany = view.findViewById(R.id.edit_row_add_medication_how_many);
                EditText mEditHowManyTimesADay = view.findViewById(R.id.edit_row_add_medication_how_many_times_a_day);
                EditText mEditDuration = view.findViewById(R.id.edit_row_add_medication_duration);

                // Spinners
                Spinner mSpinnerMedication = view.findViewById(R.id.spinner_row_add_medication);
                Spinner mSpinnerDosage = view.findViewById(R.id.spinner_row_add_medication_dosage);
                Spinner mSpinnerUOM = view.findViewById(R.id.spinner_row_add_medication_uom);

                int selectMedicine = mSpinnerMedication.getSelectedItemPosition();
                int selectDosage = mSpinnerDosage.getSelectedItemPosition();
                int selectedUOM = mSpinnerUOM.getSelectedItemPosition();

                String mHowMany = mEditHowMany.getText().toString().trim();
                String mHowManyTimesADay = mEditHowManyTimesADay.getText().toString().trim();
                String mDuration = mEditDuration.getText().toString().trim();

                mEditHowMany.setError(null);

                if (selectMedicine ==0) {
                    status = false;
                    Common.setCustomToast(mActivity, getString(R.string.error_select_medication));
                }else {

                    if (!mSelMedicationType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_eye_drops))) {
                        if (selectDosage == 0) {
                            status = false;
                            Common.setCustomToast(mActivity, getString(R.string.error_select_dosage));
                        } else if (selectedUOM == 0) {
                            status = false;
                            Common.setCustomToast(mActivity, getString(R.string.error_select_uom));
                        } else if (TextUtils.isEmpty(mHowMany)) {
                            mEditHowMany.requestFocus();
                            mEditHowMany.setError(getResources().getString(R.string.error_field_required));
                            status = false;
                        }
                    } else {
                        if (TextUtils.isEmpty(mHowManyTimesADay)) {
                            mEditHowManyTimesADay.requestFocus();
                            mEditHowManyTimesADay.setError(getResources().getString(R.string.error_field_required));
                            status = false;
                        }
                        if (TextUtils.isEmpty(mDuration)) {
                            mEditDuration.requestFocus();
                            mEditDuration.setError(getResources().getString(R.string.error_field_required));
                            status = false;
                        }
                    }

                    String mStartDate = mEditStartDate.getText().toString();
                    Date mEndDate = null;
                    @SuppressLint("SimpleDateFormat")
                    SimpleDateFormat format = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));;
                    try {
                        mEndDate = format.parse(mStartDate);
                        System.out.println(mEndDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    if (!mSelMedicationType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_eye_drops))) {
                        mMedicationType = mActivity.getResources().getString(R.string.text_none);
                    } else {
                        if (mRadioNone.isChecked()) {
                            mMedicationType = mActivity.getResources().getString(R.string.text_none);
                        } else if (mRadioDays.isChecked()) {
                            mMedicationType = mActivity.getResources().getString(R.string.text_days);
                            EndDate = String.valueOf(Common.addDay(mEndDate, Integer.parseInt(mDuration)));
                        } else if (mRadioWeek.isChecked()) {
                            mMedicationType = mActivity.getResources().getString(R.string.text_week);
                        } else {
                            mMedicationType = mActivity.getResources().getString(R.string.text_month);
                        }
                    }*/
            }
        }
        return status;
    }

    /**
     * Call To API Add Medication On Patient
     */
    private void callToAddMedication() {
        try {
            JSONArray item = AddBindMedication();
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            String preliminary_examination = AddNewPreliminaryExaminationFragment.getPrelimary();

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.addMedication(item, hospital_database, preliminary_examination));

            Call<AddMedicationModel> call = RetrofitClient.createService(ApiInterface.class).addMedication(requestBody);
            call.enqueue(new Callback<AddMedicationModel>() {
                @Override
                public void onResponse(@NonNull Call<AddMedicationModel> call, Response<AddMedicationModel> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    if (response.isSuccessful() && response.body().getError() == 200) {
                        Common.setCustomToast(mActivity, response.body().getMessage());
                        if (!checkVadlidationSurgery()) {
                            if (mArrHistory.size() > 0) {
                                mArrHistory.clear();
                            }
                            callToHistoryPreliminaryExamination();
                            callToNextPage();
                        }
                    } else {
                        Common.setCustomToast(mActivity, response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<AddMedicationModel> call, Throwable t) {
                    Common.insertLog(t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public JSONArray AddBindMedication() {
        JSONArray mArrMedication = new JSONArray();
        try {
            if (mLinearPrescriptions.getChildCount() != 0) {

                String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);

                for (int i = 0; i < mLinearPrescriptions.getChildCount(); i++) {
                    View view = mLinearPrescriptions.getChildAt(i);
                    Spinner mSpinnerMedication = view.findViewById(R.id.spinner_row_add_medication);
                    Spinner mSpinnerDosage = view.findViewById(R.id.spinner_row_add_medication_dosage);
                    Spinner mSpinnerUOM = view.findViewById(R.id.spinner_row_add_medication_uom);

                    EditText editDuration = view.findViewById(R.id.edit_row_add_medication_description);

                    Times = editDuration.getText().toString();

                    int select_medication = mSpinnerMedication.getSelectedItemPosition();
                    String medication = mArrMedicationList.get(select_medication - 1)
                            .getMedicationID();
                    LinearLayout mLinearDosages = view.findViewById(R.id.linear_medication_item_add_dosage_continer);

                    String startDate = Common.setCurrentDate(mActivity);
                    String convertCurrentDate = Common.convertDateUsingDateFormat(mActivity,
                            startDate,
                            mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                            mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));


                    EditText mEditEndDate = view.findViewById(R.id.edit_row_add_medication_end_date);

                    String EndDate = Common.setCurrentDate(mActivity);
                    String convertEndDate = Common.convertDateUsingDateFormat(mActivity,
                            EndDate,
                            mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                            mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));


                    if (mLinearDosages.getChildCount() > 0) {
                        for (int d = 0; d < mLinearDosages.getChildCount(); d++) {
                            View view_dosage = mLinearDosages.getChildAt(d);
                            EditText mEditTime = view_dosage.findViewById(R.id.edit_row_prescription_dosage_how_times);
                            Spinner mSpinnerDay = view_dosage.findViewById(R.id.spinner_row_treatment_suggested_prescription_dosage_iten_time);
                            EditText mEditText = view_dosage.findViewById(R.id.edit_prescription_number);

                            String mHowManyTimesADay = mEditTime.getText().toString();
                            String HowManyTimesDuration = mEditText.getText().toString();

                            int index_duration = mSpinnerDay.getSelectedItemPosition();
                            String mDuration = mArrDuration.get(index_duration);


                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_APPOINTMENT_ID, mAppointmentId);
                            jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_USER_ID, mUserId);
                            jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_MEDICATION_ID, medication);
                            jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_DOSAGE_ID, "0");
                            jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_START_DATE, convertCurrentDate);
                            jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_END_DATE, convertEndDate);
                            jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_DESCRIPTION, Times);
                            jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_TOTAL_TIMES, mHowManyTimesADay);
                            jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_TYPE, mDuration);
                            jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_TOTAL_DURATION, HowManyTimesDuration);
                            jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_TOTAL_QTY, "0");
                            jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_UOM_ID, "0");
                            jsonObject.put(WebFields.ADD_MEDICATION.REQUEST_SPECIAL_INSTRUCTION, "");
                            mArrMedication.put(jsonObject);

                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mArrMedication;
    }

    /**
     * Check Surgery Validation
     */
    private void callToAddSurgery() {
        try {

            int indexCategor = mSpinnerCategory.getSelectedItemPosition();
            int indexSurgery = mSpinnerSurgeryName.getSelectedItemPosition();
            int indexSurgeryPadEntry = mSpinnerSurgeryPadEntry.getSelectedItemPosition();
            int indexSurgeryWithin = mSpinnerSurgeryWithinTime.getSelectedItemPosition();
            int indexSurgeryEye = mSpinnerSurgeryEye.getSelectedItemPosition();
            int indexSurgeryTwo = mSpinnerSurgeryTwo.getSelectedItemPosition();

            // String data=mArrCategory.get(indexCategor-1).getCategoryID();

            String CategoryID = mArrCategory.get(indexCategor - 1).getCategoryID();
            String SurgeryTypeID = mArrSurgeryType.get(indexSurgery - 1).getSurgeryTypeID();
            String EyeID = mArrEye.get(indexSurgeryEye - 1).getEyeID();
            String Duration = mArrDuration.get(indexSurgeryWithin);
            String Remarks = mEditTextSurgeryRemarks.getText().toString();
            String SurgeryTwo = mArrSurgeryType.get(indexSurgeryTwo - 1).getSurgeryName();

            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);


            String prelimianaryID = AddNewPreliminaryExaminationFragment.getPrelimary();


            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.addSurgerySuggested(mAppointmentId,
                            Integer.parseInt(CategoryID), SurgeryTypeID,
                            EyeID, Duration, Remarks, mUserId, hospital_database, prelimianaryID, SurgeryTwo));


            Call<AddMedicationModel> call = RetrofitClient.createService(ApiInterface.class).addMedication(requestBody);
            call.enqueue(new Callback<AddMedicationModel>() {
                @Override
                public void onResponse(@NonNull Call<AddMedicationModel> call, Response<AddMedicationModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    if (response.isSuccessful() && response.body().getError() == 200) {
                        Common.setCustomToast(mActivity, response.body().getMessage());

                        if (mArrHistory.size() > 0) {
                            mArrHistory.clear();
                        }
                        callToHistoryPreliminaryExamination();
                        callToNextPage();
                    } else {
                        Common.setCustomToast(mActivity, response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<AddMedicationModel> call, Throwable t) {
                    Common.insertLog(t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set  Bind Surgery JSon
     *
     * @return - Return Json Array
     */
    private JSONArray AddBindSurgery() {
        JSONArray mArrSurgery = new JSONArray();
        try {

            for (int a = 0; a < mLinearSurgery.getChildCount(); a++) {


                View view = mLinearSurgery.getChildAt(a);

                Spinner mSpinnerCategory = view.findViewById(R.id.spinner_treatment_suggested_surgery_suggested_category);
                Spinner mSpinnerSurgeryName = view.findViewById(R.id.spinner_treatment_suggested_surgery_suggested_surgery);
                Spinner mSpinnerSurgeryPadEntry = view.findViewById(R.id.spinner_treatment_suggested_surgery_suggested_pad_entry);
                Spinner mSpinnerSurgeryWithinTime = view.findViewById(R.id.spinner_treatment_suggested_surgery_suggested_within_time);
                Spinner mSpinnerSurgeryEye = view.findViewById(R.id.spinner_treatment_suggested_surgery_eye);
                Spinner mSpinnerSurgeryTwo = view.findViewById(R.id.spinner_treatment_suggested_surgery_suggested_surgery_two);

                EditText mEditTextSurgeryRemarks = view.findViewById(R.id.spinner_treatment_suggested_surgery_suggested_remarks);

                int indexCategor = mSpinnerCategory.getSelectedItemPosition();
                int indexSurgery = mSpinnerSurgeryName.getSelectedItemPosition();
                int indexSurgeryPadEntry = mSpinnerSurgeryPadEntry.getSelectedItemPosition();
                int indexSurgeryWithin = mSpinnerSurgeryWithinTime.getSelectedItemPosition();
                int indexSurgeryEye = mSpinnerSurgeryEye.getSelectedItemPosition();
                int indexSurgeryTwo = mSpinnerSurgeryTwo.getSelectedItemPosition();

                // String data=mArrCategory.get(indexCategor-1).getCategoryID();


                JSONObject jsonObject = new JSONObject();
                jsonObject.put("CategoryID", mArrCategory.get(indexCategor - 1).getCategoryID());
                jsonObject.put("SurgeryTypeID", mArrSurgeryType.get(indexSurgery - 1).getSurgeryTypeID());
                jsonObject.put("SurgeryTwo", mArrSurgeryType.get(indexSurgeryTwo - 1).getSurgeryName());
                jsonObject.put("EyeID", mArrEye.get(indexSurgeryEye - 1).getEyeID());
                jsonObject.put("Duration", mArrSurgeryTime.get(indexSurgeryWithin - 1));
                jsonObject.put("Remarks", mEditTextSurgeryRemarks.getText().toString());

                mArrSurgery.put(jsonObject);
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
        return mArrSurgery;
    }

    /**
     * Set Call API History Preliminary Examination
     */
    private void callToHistoryPreliminaryExamination() {
        try {

            String defualt_date = "1000-01-01";
            String Mobile = AppConstants.STR_EMPTY_STRING;
            String Type = "TreatmentSuggested";
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setHistoryPreliminaryExaminationJson(defualt_date, mCurrentPage, mPatientId, Mobile, mHistoryDoctorID, Type, hospital_database));

            Call<HistoryPreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationHistory(body);
            call.enqueue(new Callback<HistoryPreliminaryExaminationModel>() {
                @Override
                public void onResponse(Call<HistoryPreliminaryExaminationModel> call, Response<HistoryPreliminaryExaminationModel> response) {
                    Common.insertLog("Response History::::> " + new Gson().toJson(response.body()));
                    try {
                        if (response.body() != null &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body().getData().size() > 0) {

                                mArrHistory.addAll(response.body().getData());
//                                setTodayData(response.body().getData());
                            }
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<HistoryPreliminaryExaminationModel> call, Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    private void setDataTemplate(String ID) {
        if (mLinearPrescriptions.getChildCount() > 0) {
            mLinearPrescriptions.removeAllViews();
        }

        if (templeteName.size() > 0) {
            for (int i = 0; i < templeteName.size(); i++) {

                if (templeteName.get(i).getTemplateID().equalsIgnoreCase(ID)) {
                    int id = Integer.parseInt(ID);
                    addTemplate(templeteName.get(i).getItem());
                }
            }
        }

//        mLinearPrescriptions.addView(addView);

    }

    private void addTemplate(List<TempleteModel.Datum.Item> item) {
        if (item.size() > 0) {
            for (int n = 0 ; n < item.size() ; n++) {

                String Medication = item.get(n).getMedicationName();
                String Type = item.get(n).getType();
                String TotalTime = item.get(n).getTotalTimes();
                String Duration = item.get(n).getTotalDuration();

                try {
                    LayoutInflater layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View addView = layoutInflater.inflate(R.layout.row_medication_item, null);

                    // Linear Layouts
                    mLinearDosage = addView.findViewById(R.id.linear_row_add_medication_dosage);
                    mLinearMedicationType = addView.findViewById(R.id.linear_row_add_medication_type);
                    mLinearUOM = addView.findViewById(R.id.linear_row_add_medication_uom);
                    LinearLayout mLinearDosages = addView.findViewById(R.id.linear_medication_item_add_dosage_continer);

                    // Spinners
                    mSpinnerMedication = addView.findViewById(R.id.spinner_row_add_medication);
                    mSpinnerDosage = addView.findViewById(R.id.spinner_row_add_medication_dosage);
                    mSpinnerUOM = addView.findViewById(R.id.spinner_row_add_medication_uom);

                    // Text Input Layouts
                    mTextInputEndDate = addView.findViewById(R.id.text_input_row_add_medication_end_date);
                    mTextInputHowManyTimesADay = addView.findViewById(R.id.text_input_row_add_medication_how_many_times_a_day);
                    mTextInputHowMany = addView.findViewById(R.id.text_input_row_add_medication_how_many);
                    mTextInputDuration = addView.findViewById(R.id.text_input_row_add_medication_duration);

                    // Edit Texts Views
                    final EditText mEditStartDate = addView.findViewById(R.id.edit_row_add_medication_start_date);
                    final EditText mEditEndDate = addView.findViewById(R.id.edit_row_add_medication_end_date);

                    // Views
                    mViewDosage = addView.findViewById(R.id.view_row_add_medication_dosage);
                    mViewEndDate = addView.findViewById(R.id.view_row_add_medication_end_date);
                    mViewHowManyTimesADay = addView.findViewById(R.id.view_row_add_medication_how_many_times_a_day);
                    mViewHowMany = addView.findViewById(R.id.view_row_add_medication_how_many);
                    mViewUOM = addView.findViewById(R.id.view_row_add_medication_uom);
                    mViewDuration = addView.findViewById(R.id.view_row_add_medication_duration);
                    mViewMedicationType = addView.findViewById(R.id.view_row_add_medication_type);

                    // Radio Group
                    mRadioGroupMedicationType = addView.findViewById(R.id.radio_group_row_add_medication_type);

                    // Radio Buttons
                    mRadioNone = addView.findViewById(R.id.radio_row_add_medication_none);
                    mRadioDays = addView.findViewById(R.id.radio_row_add_medication_days);
                    mRadioWeek = addView.findViewById(R.id.radio_row_add_medication_week);

                    // Image Views
                    ImageView mImageRemove = addView.findViewById(R.id.image_row_add_medication_item_remove);
                    ImageView mImageDosage = addView.findViewById(R.id.image_medication_item_add_dosage);


                    // Recycler View
                    mRecyclerView = addView.findViewById(R.id.recycler_view_row_add_medication_special_instructions);

                    // ToDo: Sets the spinner color as per the theme applied
                    mSpinnerMedication.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
                    mSpinnerDosage.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

                    // ToDo: Doctor - Spinner Item Select Listener
                    mSpinnerMedication.setOnItemSelectedListener(onItemSelectedListener);
                    mSpinnerDosage.setOnItemSelectedListener(onItemSelectedListener);

                    // ToDo: Radio Button Click Listeners
                    mRadioGroupMedicationType.setOnCheckedChangeListener(checkedChangeListener);

                    setsMedicationAdapter(mSpinnerMedication);
                    setsUOMAdapter(mSpinnerUOM);
                    setsDosageAdapter();
                    setAdapterData();
                    int index = 0;


                    for (int i = 0; i < item.size(); i++) {
                        if (item.get(i).getMedicationName().
                                equalsIgnoreCase(Medication))
                        mSpinnerMedication.setSelection(i + 1);
                    }

                    addDosage(mLinearDosages, TotalTime, Type, Duration, item);


                    // Sets up the data
                    mEditStartDate.setText(Common.setCurrentDate(mActivity));

                    mEditEndDate.setText(Common.setCurrentDate(mActivity));

                    // ToDo: Edit Text Start Date Click Listener
                    mEditStartDate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Common.openDatePicker(mActivity, mEditStartDate);
                        }
                    });

                    // ToDo: Edit Text End Date Click Listener
                    mEditEndDate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Common.openDatePicker(mActivity, mEditEndDate);
                        }
                    });

                    // ToDo: Item remove Text End Date Click Listener
                    mImageRemove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mLinearPrescriptions.removeView(addView);
                            showPrintPrescription();
                        }
                    });

                    // ToDo: Item remove Text End Date Click Listener
                    mImageDosage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // mLinearPrescriptions.removeView(addView);
                            AddDosage(mLinearDosages);
                        }
                    });

                    mLinearPrescriptions.addView(addView);
                    showPrintPrescription();//show Print Option

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void addDosage(LinearLayout mLinearDosages, String TotalTime, String Type, String Duration, List<TempleteModel.Datum.Item> item) {

        LayoutInflater layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view_dosage = layoutInflater.inflate(R.layout.row_treatment_suggested_prescription_dosage_item, null, false);

        EditText mTimes = view_dosage.findViewById(R.id.edit_row_prescription_dosage_how_times);
        Spinner mSpinnerTime = view_dosage.findViewById(R.id.spinner_row_treatment_suggested_prescription_dosage_iten_time);

        ImageView mImageRemove = view_dosage.findViewById(R.id.image_prescription_dosage_time_remove);
        EditText mEditDuration = view_dosage.findViewById(R.id.edit_prescription_number);

        ArrayAdapter adapter_surgery_within = new ArrayAdapter<String>
                (mActivity,
                        android.R.layout.simple_spinner_dropdown_item, mArrDuration) {
            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                // Cast the drop down items (popup items) as text view
                TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                // Set the text color of drop down items
                tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                // If this item is selected item
                if (position == mSelectedWithin) {
                    // Set spinner selected popup item's text color
                    tv.setTextColor(Common.setThemeColor(mActivity));
                }
                // Return the modified view
                return tv;
            }
        };
        adapter_surgery_within.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerTime.setAdapter(adapter_surgery_within);


        for (int s = 0; s < item.size(); s++) {
            if (item.get(s).getType().equals(Type)) {
                mSpinnerTime.setSelection(s - 1);
            }
            if (item.get(s).getTotalTimes().equals(TotalTime)) {
                mTimes.setText(item.get(s).getTotalTimes());
            }
            if (item.get(s).getTotalDuration().equals(Duration)) {
                mEditDuration.setText(item.get(s).getTotalDuration());
            }
        }



//
//        ArrayList<TempleteModel.Datum.Item> itemArrayList = new ArrayList<>();
//
//
//        for (int s = 0; s < itemArrayList.size(); s++) {
//            if (mArrDuration.get(s).equals(item.getTotalDuration())) {
//                mSpinnerTime.setSelection(s);
//            }
//        }
//
//        mTimes.setText(item.getTotalTimes());
//

        mImageRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLinearDosages.removeView(view_dosage);
                showPrintPrescription();
            }
        });

        mLinearDosages.addView(view_dosage);

    }

    /**
     * Set Today Data
     *
     * @param data -Data
     */
    private void setTodayData(List<HistoryPreliminaryExaminationModel.Data> data) {
        try {
            String current_date = Common.setCurrentDate(mActivity);
            String convert_current_date = Common.setConvertDateFormat(mActivity,
                    current_date, mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen));

            if (mLinearPrescriptions.getChildCount() > 0) {
                mLinearPrescriptions.removeAllViews();
            }

            if (data.size() > 0) {
                for (int a = 0; a < data.size(); a++) {
                    if (data.get(a).getTreatmentDate().equalsIgnoreCase(convert_current_date)) {
                        editTreatmentSuggested(data.get(a));
                        editMedications(data.get(a));
                    }
                }
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * OpenDialog for closing patient file
     */
    public void openFileClosingAlert() {


        AlertDialog.Builder builder = new AlertDialog.Builder(
                mActivity);
        builder.setTitle("Close File");
        builder.setMessage("Are you sure you want to close file?");
        builder.setNegativeButton("NO",
                (dialog, which) -> dialog.dismiss());
        builder.setPositiveButton("YES",
                (dialog, which) -> closeFile());
        builder.show();

    }

    private void closeFile() {
        try {
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.closeFileJson(mAppointmentId, mDatabaseName));

            Call<CloseFileModel> call = RetrofitClient.createService(ApiInterface.class).closeFileAPI(body);
            call.enqueue(new Callback<CloseFileModel>() {
                @Override
                public void onResponse(@NonNull Call<CloseFileModel> call, @NonNull Response<CloseFileModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        int mError = response.body().getError();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                Common.insertLog("IF");
                                Common.setCustomToast(mActivity, mMessage);
                            } else {
                                Common.insertLog("ELSE");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CloseFileModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open Dialog Treatment Suggested
     */
    public void openDialogTreatmentSuggested() {
        try {
            Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.custom_dialog_history_preliminary);

            TextView mTextTitle = dialog.findViewById(R.id.text_view_custom_dialog_history_preliminary);
            RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
            ImageView mImageClose = dialog.findViewById(R.id.image_custom_dialog_history_preliminary_close);

            RelativeLayout noInternetConnection = dialog.findViewById(R.id.relative_no_internet);
            RelativeLayout noDataFound = dialog.findViewById(R.id.relative_no_data_available);

            mTextTitle.setText(mActivity.getResources().getString(R.string.tab_prescription));

            mImageClose.setColorFilter(Common.setThemeColor(mActivity));

            mImageClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            LinearLayoutManager manager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(manager);
            HistoryTreatmentSuggestedAdapter adapter = new
                    HistoryTreatmentSuggestedAdapter(mActivity, recyclerView, mArrHistory,
                            this) {
                        @Override
                        protected void onSelectedTreatmentSuggested(Activity mActivity, HistoryPreliminaryExaminationModel.Data checkInData) {
                            super.onSelectedTreatmentSuggested(mActivity, checkInData);
                            dialog.dismiss();
                            mRelativeNoDataFound.setVisibility(View.GONE);
                            if (mLinearPrescriptions.getChildCount() > 0) {
                                mLinearPrescriptions.removeAllViews();
                            }

                            editTreatmentSuggested(checkInData);
                            editMedications(checkInData);
                        }
                    };

            recyclerView.setAdapter(adapter);

            if (mArrHistory.size() > 0) {
                noDataFound.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            } else {
                noDataFound.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Check Validation Surgery Suggested
     *
     * @return
     */
    private boolean checkVadlidationSurgery() {
        boolean status = true;

        int indexCategor = mSpinnerCategory.getSelectedItemPosition();
        int indexSurgery = mSpinnerSurgeryName.getSelectedItemPosition();
        int indexSurgery2 = mSpinnerSurgeryTwo.getSelectedItemPosition();
        int indexEye = mSpinnerSurgeryEye.getSelectedItemPosition();

        int indexSurgeryWithin = mSpinnerSurgeryWithinTime.getSelectedItemPosition();


        if (indexCategor == 0) {
            status = false;
        }

        if (indexSurgery == 0) {
            status = false;
        }
        if (indexEye == 0) {
            status = false;
        }

        if (mEditSurgeryManyDays.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            status = false;
        }
             /*if (indexSurgeryWithin==0){
                 status=false;
                 Common.setCustomToast(mActivity,mActivity.getResources().getString(R.string.error_select_surgery_suggestion_time));
             }*/


       /* int indexSurgeryPadEntry = mSpinnerSurgeryPadEntry.getSelectedItemPosition();
        int indexSurgeryWithin = mSpinnerSurgeryWithinTime.getSelectedItemPosition();
        int indexSurgeryEye = mSpinnerSurgeryEye.getSelectedItemPosition();

        if (indexCategor == 0) {
            status = false;
            Common.setCustomToast(mActivity, mActivity.getString(R.string.error_select_category));
        } else if (indexSurgery == 0) {
            status = false;
            Common.setCustomToast(mActivity, mActivity.getString(R.string.error_select_surgery));
        } else if (indexSurgeryWithin == 0) {
            status = false;
            Common.setCustomToast(mActivity, mActivity.getString(R.string.error_select_duration));
        } else if (indexSurgeryEye == 0) {
            status = false;
            Common.setCustomToast(mActivity, mActivity.getString(R.string.error_select_eye));
        }*/
        return status;
    }

    /**
     * Set Call To Next Screen
     */
    private void callToNextPage() {
        AddNewPreliminaryExaminationFragment.changePage(7);
    }


    @Override
    public void onSelectHistioyDiagnosis(Activity mActivity, HistoryPreliminaryExaminationModel.Data checkInData) {
        mRelativeNoDataFound.setVisibility(View.GONE);
        editTreatmentSuggested(checkInData);
        editMedications(checkInData);
    }


    public void editTreatmentSuggested(HistoryPreliminaryExaminationModel.Data checkInData) {
        if (checkInData.getSurgerySuggested().size() > 0) {

            String category = checkInData.getSurgerySuggested().get(0).getCategoryID();
            if (!category.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                if (mArrCategory.size() > 0) {
                    for (int a = 0; a < mArrCategory.size(); a++) {
                        if (mArrCategory.get(a).getCategoryID()
                                .equalsIgnoreCase(category)) {
                            int index = a + 1;
                            mSpinnerCategory.setSelection(index);
                        }
                    }
                }
            }

            String surgeryID = checkInData.getSurgerySuggested().get(0).getSurgeryTypeID();

            if (!surgeryID.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                if (mArrSurgeryType.size() > 0) {
                    for (int a = 0; a < mArrSurgeryType.size(); a++) {
                        if (mArrSurgeryType.get(a).getSurgeryTypeID()
                                .equalsIgnoreCase(surgeryID)) {
                            int index = a + 1;
                            mSpinnerSurgeryName.setSelection(index, true);
                        }
                    }
                }
            }

            String duration = checkInData.getSurgerySuggested().get(0).getDuration();

            if (!duration.equals(AppConstants.STR_EMPTY_STRING)) {
                if (mArrDuration.size() > 0) {
                    for (int a = 0; a < mArrDuration.size(); a++) {
                        if (mArrDuration.get(a).equalsIgnoreCase(duration)) {
                            int index = a;
                            mSpinnerSurgeryWithinTime.setSelection(index, true);
                        }
                    }
                }

            }

            String eyeid = checkInData.getSurgerySuggested().get(0).getEye();
            if (!eyeid.equals(AppConstants.STR_EMPTY_STRING)) {
                if (mArrEyeName.size() > 0) {
                    for (int a = 0; a < mArrEyeName.size(); a++) {
                        if (mArrEyeName.get(a).
                                equalsIgnoreCase(eyeid)) {
                            //int index=a+1;
                            mSpinnerSurgeryEye.setSelection(a);
                        }
                    }
                }

            }

            //Surgery Two Selected
            String surgery2 = checkInData.getSurgerySuggested().get(0).getSurgeryType2();
            if (!eyeid.equals(AppConstants.STR_EMPTY_STRING)) {
                if (mArrSurgery.size() > 0) {
                    for (int a = 0; a < mArrSurgery.size(); a++) {
                        if (mArrSurgery.get(a).
                                equalsIgnoreCase(surgery2)) {
                            //int index=a+1;
                            mSpinnerSurgeryTwo.setSelection(a);
                        }
                    }
                }

            }

            mEditTextSurgeryRemarks.setText(checkInData.getSurgerySuggested().get(0).getRemarks());


            if (mLinearSurgery.getVisibility() == View.GONE) {
                mImageAddSurgery.setImageResource(R.drawable.ic_up_arrow);
                mLinearSurgery.setVisibility(View.GONE);
            }

            /*}catch (Exception e){
                Common.insertLog(e.getMessage());
            }*/
        }
    }

    public void editMedications(HistoryPreliminaryExaminationModel.Data checkInData) {

        if (checkInData.getMedication().size() > 0) {

            for (int m = 0; m < checkInData.getMedication().size(); m++) {

                HistoryPreliminaryExaminationModel.Medication medication
                        = checkInData.getMedication().get(m);

                try {
                    LayoutInflater layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    final View addView = layoutInflater.inflate(R.layout.row_medication_item, null);

                    // Linear Layouts
                    mLinearDosage = addView.findViewById(R.id.linear_row_add_medication_dosage);
                    mLinearMedicationType = addView.findViewById(R.id.linear_row_add_medication_type);
                    mLinearUOM = addView.findViewById(R.id.linear_row_add_medication_uom);
                    LinearLayout mLinearDosages = addView.findViewById(R.id.linear_medication_item_add_dosage_continer);

                    // Spinners
                    mSpinnerMedication = addView.findViewById(R.id.spinner_row_add_medication);
                    mSpinnerDosage = addView.findViewById(R.id.spinner_row_add_medication_dosage);
                    mSpinnerUOM = addView.findViewById(R.id.spinner_row_add_medication_uom);

                    // Text Input Layouts
                    mTextInputEndDate = addView.findViewById(R.id.text_input_row_add_medication_end_date);
                    mTextInputHowManyTimesADay = addView.findViewById(R.id.text_input_row_add_medication_how_many_times_a_day);
                    mTextInputHowMany = addView.findViewById(R.id.text_input_row_add_medication_how_many);
                    mTextInputDuration = addView.findViewById(R.id.text_input_row_add_medication_duration);

                    // Edit Texts Views
                    final EditText mEditStartDate = addView.findViewById(R.id.edit_row_add_medication_start_date);
                    final EditText mEditEndDate = addView.findViewById(R.id.edit_row_add_medication_end_date);

                    // Views
                    mViewDosage = addView.findViewById(R.id.view_row_add_medication_dosage);
                    mViewEndDate = addView.findViewById(R.id.view_row_add_medication_end_date);
                    mViewHowManyTimesADay = addView.findViewById(R.id.view_row_add_medication_how_many_times_a_day);
                    mViewHowMany = addView.findViewById(R.id.view_row_add_medication_how_many);
                    mViewUOM = addView.findViewById(R.id.view_row_add_medication_uom);
                    mViewDuration = addView.findViewById(R.id.view_row_add_medication_duration);
                    mViewMedicationType = addView.findViewById(R.id.view_row_add_medication_type);

                    // Radio Group
                    mRadioGroupMedicationType = addView.findViewById(R.id.radio_group_row_add_medication_type);

                    // Radio Buttons
                    mRadioNone = addView.findViewById(R.id.radio_row_add_medication_none);
                    mRadioDays = addView.findViewById(R.id.radio_row_add_medication_days);
                    mRadioWeek = addView.findViewById(R.id.radio_row_add_medication_week);

                    // Image Views
                    ImageView mImageRemove = addView.findViewById(R.id.image_row_add_medication_item_remove);
                    ImageView mImageDosage = addView.findViewById(R.id.image_medication_item_add_dosage);


                    // Recycler View
                    mRecyclerView = addView.findViewById(R.id.recycler_view_row_add_medication_special_instructions);

                    // ToDo: Sets the spinner color as per the theme applied
                    mSpinnerMedication.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
                    mSpinnerDosage.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

                    // ToDo: Doctor - Spinner Item Select Listener
                    mSpinnerMedication.setOnItemSelectedListener(onItemSelectedListener);
                    mSpinnerDosage.setOnItemSelectedListener(onItemSelectedListener);

                    // ToDo: Radio Button Click Listeners
                    mRadioGroupMedicationType.setOnCheckedChangeListener(checkedChangeListener);

                    setsMedicationAdapter(mSpinnerMedication);
                    setsUOMAdapter(mSpinnerUOM);
                    setsDosageAdapter();
                    setAdapterData();

                    String medication_name = medication.getMedicationName();

                    if (mArrMedicationList.size() > 0) {
                        for (int n = 0; n < mArrMedicationList.size(); n++) {
                            if (mArrMedicationList.get(n).getMedicationName()
                                    .equalsIgnoreCase(medication_name)) {
                                mSpinnerMedication.setSelection(n + 1);
                            }
                        }
                    }
                    AddEditDosage(mLinearDosages, medication);


                    // Sets up the data
                    mEditStartDate.setText(Common.setCurrentDate(mActivity));

                    mEditEndDate.setText(Common.setCurrentDate(mActivity));

                    // ToDo: Edit Text Start Date Click Listener
                    mEditStartDate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Common.openDatePicker(mActivity, mEditStartDate);
                        }
                    });

                    // ToDo: Edit Text End Date Click Listener
                    mEditEndDate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Common.openDatePicker(mActivity, mEditEndDate);
                        }
                    });

                    // ToDo: Item remove Text End Date Click Listener
                    mImageRemove.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mLinearPrescriptions.removeView(addView);
                            showPrintPrescription();
                        }
                    });

                    // ToDo: Item remove Text End Date Click Listener
                    mImageDosage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // mLinearPrescriptions.removeView(addView);
                            AddDosage(mLinearDosages);
                        }
                    });

                    mLinearPrescriptions.addView(addView);
                    showPrintPrescription();//show Print Option

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }
    }


    /**
     * Set Check Today Preliminary Examination
     */
    private void checkTodayPreliminaryExamination() {
        String currentDate = Common.setCurrentDate(mActivity);
        String covertTreatmentDate = Common.convertDateUsingDateFormat(mActivity, mTreatmentDate,
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen),
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));

        String covertCurrentDate = Common.convertDateUsingDateFormat(mActivity, currentDate,
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen));

        if (!currentDate.equals(covertTreatmentDate)) {
            //mRelativeDataNotFound.setVisibility(View.GONE);
        } else {

            if (mArrHistory.size() > 0) {
                for (int a = 0; a < mArrHistory.size(); a++) {

                    if (mArrHistory.get(a).getTreatmentDate().equals(covertCurrentDate)) {
                        if (mArrHistory.get(a).getAppointmentID().equals(String.valueOf(mAppointmentId))) {
                            editTreatmentSuggested(mArrHistory.get(a));
                        }
                    }
                }
            }
        }
    }

    /**
     * Call To Print API Preliminary Examination
     */
    private void callToPrintPdf(String lang) {
        try {

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPrintReceiptJson(AddNewPreliminaryExaminationFragment.PreliminaryExminationId,
                            String.valueOf(mAppointmentId), false, false, false,
                            false, false, false, false, false,
                            false, false, false, true, lang, false, false, hospital_database));

            Call<PrintReceiptModel> call = RetrofitClient.createService(ApiInterface.class).PrintReceipt(body);
            call.enqueue(new Callback<PrintReceiptModel>() {
                @Override
                public void onResponse(@NonNull Call<PrintReceiptModel> call, @NonNull Response<PrintReceiptModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body() != null) {
                                String url = response.body().getData();
                                callToReceiptPrint(url);
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PrintReceiptModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Call To Receipt Print
     *
     * @param url - String Url Print
     */
    private void callToReceiptPrint(String url) {
        /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);*/
        //  String pdfOpen = AppConstants.PDF_OPEN + url;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mActivity.startActivity(browserIntent);
    }
}
