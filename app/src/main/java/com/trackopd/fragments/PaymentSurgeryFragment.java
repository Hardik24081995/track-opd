package com.trackopd.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.trackopd.R;
import com.trackopd.adapter.ReceptionistPatientDetailPaymentAdapter;
import com.trackopd.model.AddBillingPaymentModel;
import com.trackopd.model.CloseFileModel;
import com.trackopd.model.ReceptionistPaymentModel;
import com.trackopd.model.ServicesPackagesModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.utils.SessionManager;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentSurgeryFragment extends Fragment {

    String PaymentType;
    private View mView;
    private Activity mActivity;

    private String strPatientId, strDoctorId, strTreatment, mFirstName, mLastName, mMobileNo,
            mAppointmentDate, mPatientCode, mAppointmentNo;
    private int strAppointmentID;

    private ImageView mImageAddServices;
    private LinearLayout mLinearContainer, mLinearPayAmount, mLinearCheque, mLinearBankName,
            mLinearAccountNo, mLinearPayCheque;
    private ArrayList<ServicesPackagesModel.Item> mArrService;
    private ArrayList<String> mArrServiceTitle;
    private EditText mEditTotalAmount, mEditDateTime, mEditReceiptNo, mEditCash, mEditCheque, mEditNEFT,
            mEditEWallet, mEditChequeNo, mEditBankName, mEditAccountNo, mEditPayAmount, mEditOutStanding;
    private AppUtil mAppUtils;

    private RadioGroup mRadioGroupStatus;
    private Button mButtonPay;
    private ImageView mImagePrint;

    private ArrayList<ReceptionistPaymentModel> mArrReceptionistPayment;
    private CheckBox mCheckCash, mCheckNeft, mCheckEWallet, mCheckCheque;


    public PaymentSurgeryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_payment_surgery, container, false);

        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        mArrService = new ArrayList<>();
        mArrServiceTitle = new ArrayList<>();
        mAppUtils = new AppUtil(mActivity);
        mArrReceptionistPayment = new ArrayList<>();

        getBundle();
        getIds();
        setData();
        setRegister();
        callToServiceAPI();
        callPaymentListAPI();
        setHasOptionsMenu(true);

        mEditTotalAmount.setFilters(Common.getFilter());
        mEditDateTime.setFilters(Common.getFilter());
        mEditReceiptNo.setFilters(Common.getFilter());
        mEditCash.setFilters(Common.getFilter());
        mEditCheque.setFilters(Common.getFilter());
        mEditNEFT.setFilters(Common.getFilter());
        mEditEWallet.setFilters(Common.getFilter());
        mEditChequeNo.setFilters(Common.getFilter());
        mEditBankName.setFilters(Common.getFilter());
        mEditAccountNo.setFilters(Common.getFilter());
        mEditPayAmount.setFilters(Common.getFilter());
        mEditOutStanding.setFilters(Common.getFilter());

        return  mView;
    }


    /**
     * get Bundle Value Previous
     */
    private void getBundle() {
        try {

            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mFirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mLastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mMobileNo = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);

                strPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                strAppointmentID = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                strDoctorId = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);
                strTreatment = bundle.getString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE);
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }


    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Set Register Widgets
     */
    private void getIds() {
        // Image View
        mImageAddServices = mView.findViewById(R.id.image_surgery_payment_add_services);

        mLinearContainer = mView.findViewById(R.id.linear_surgery_payment_service_container);
        mLinearPayAmount = mView.findViewById(R.id.linear_surgery_payment_pay_amount);
        mLinearAccountNo = mView.findViewById(R.id.linear_surgery_payment_account_no);
        mLinearBankName = mView.findViewById(R.id.linear_surgery_payment_bank_name);
        mLinearCheque = mView.findViewById(R.id.linear_surgery_payment_cheque);
        mLinearPayCheque = mView.findViewById(R.id.linear_surgery_payment_account);

        //Edit Text
        mEditTotalAmount = mView.findViewById(R.id.edit_surgery_payment_total_rate);
        mEditDateTime = mView.findViewById(R.id.edit_surgery_payment_date);
        mEditReceiptNo = mView.findViewById(R.id.edit_surgery_payment_receipt_no);
        mEditCash = mView.findViewById(R.id.edit_surgery_payment_type_cash);
        mEditCheque = mView.findViewById(R.id.edit_surgery_payment_type_cheque);
        mEditChequeNo = mView.findViewById(R.id.edit_surgery_payment_type_cheque_no);
        mEditNEFT = mView.findViewById(R.id.edit_surgery_payment_type_neft);
        mEditEWallet = mView.findViewById(R.id.edit_surgery_payment_type_e_wallet);
        mEditBankName = mView.findViewById(R.id.edit_surgery_payment_type_bank_name);
        mEditAccountNo = mView.findViewById(R.id.edit_surgery_payment_type_account_no);
        mEditPayAmount = mView.findViewById(R.id.edit_surgery_payment_pay_amount);
        mEditOutStanding = mView.findViewById(R.id.edit_surgery_payment_out_standing);


        //Radio Group
        mRadioGroupStatus = mView.findViewById(R.id.radio_group_surgery_payment_status);
        //Button
        mButtonPay = mView.findViewById(R.id.button_surgery_payment_submit);

        mImagePrint = mView.findViewById(R.id.image_surgery_payment_print);

        //Check Box
        mCheckCash = mView.findViewById(R.id.check_box_surgery_payment_cash);
        mCheckCheque = mView.findViewById(R.id.check_box_surgery_payment_cheque);
        mCheckNeft = mView.findViewById(R.id.check_box_surgery_payment_neft);
        mCheckEWallet = mView.findViewById(R.id.check_box_surgery_payment_e_wallet);


        String current_date_time = Common.setCurrentDateTime(mActivity);

        mEditDateTime.setText(current_date_time);
    }

    /**
     * Set Register of Click Listener
     */
    private void setRegister() {
        mImageAddServices.setOnClickListener(clickListener);

        SessionManager manager = new SessionManager(mActivity);
        String closeFile = manager.getPreferences("CloseFiles", "");

        if (closeFile.equalsIgnoreCase("Yes")) {
            mButtonPay.setClickable(false);
            Toast.makeText(mActivity, "Patient File Closed", Toast.LENGTH_SHORT).show();
        } else {
            mButtonPay.setOnClickListener(clickListener);
        }

        mEditDateTime.setOnClickListener(clickListener);
        mImagePrint.setOnClickListener(clickListener);

        mCheckEWallet.setOnCheckedChangeListener(changeListener);
        mCheckNeft.setOnCheckedChangeListener(changeListener);
        mCheckCheque.setOnCheckedChangeListener(changeListener);
        mCheckCash.setOnCheckedChangeListener(changeListener);

        mEditCash.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (chekvalidateAmount()) {
                    updateoustanding();
                    mEditCash.setError(null);
                } else {
                    mEditCash.setError("Enter This Amount is Greater to Total Amount");
                    //mButtonPay.setClickable(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mEditEWallet.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (chekvalidateAmount()) {
                    updateoustanding();
                    mEditEWallet.setError(null);
                } else {
                    // Common.setCustomToast(mActivity.getString(R.string.error_your_));
                    mEditEWallet.setError("Enter This Amount is Greater to Total Amount");
                    //mButtonPay.setClickable(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mEditNEFT.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (chekvalidateAmount()) {
                    updateoustanding();
                    mEditNEFT.setError(null);
                } else {
                    mEditNEFT.setError("Enter This Amount is Greater to Total Amount");
                    //mButtonPay.setClickable(false);
                    // Common.setCustomToast(mActivity.getString(R.string.error_your_));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        mEditCheque.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (chekvalidateAmount()) {
                    updateoustanding();
                    mEditCheque.setError(null);
                } else {
                    mEditCheque.setError("Enter This Amount is Greater to Total Amount");
                    //mButtonPay.setClickable(false);
                    // Common.setCustomToast(mActivity.getString(R.string.error_your_));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setData() {
        mEditCash.setText("0");
        mEditEWallet.setText("0");
        mEditNEFT.setText("0");
        mEditCheque.setText("0");

        mEditTotalAmount.setText("0");
        mEditPayAmount.setText("0");
        mEditOutStanding.setText("0");
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_surgery_payment_add_services:
                    addService();
                    break;
                case R.id.edit_surgery_payment_date:
                    Common.openEditAllDateTimePicker(mActivity, mEditDateTime);
                    break;
                case R.id.button_surgery_payment_submit:

                    if (checkValidation()) {
                        callToAddPaymentAPI();
                    }
                    break;
                case R.id.image_surgery_payment_print:

                    break;
            }
        }
    };

    private void callAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                mActivity);
        builder.setTitle("Language Options");
        builder.setMessage("Choose your desired language");
        builder.setNegativeButton("Hindi",
                (dialog, which) -> callToAddPaymentAPI());
        builder.setPositiveButton("English",
                (dialog, which) -> callToAddPaymentAPI());
        builder.show();
    }

    private CheckBox.OnCheckedChangeListener changeListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()) {
                case R.id.check_box_surgery_payment_cash:
                    if (isChecked) {
                        PaymentType = "Cash";
                        // mEditCash.setEnabled(true);
                        mEditCash.setFocusable(true);
                        mEditCash.setFocusableInTouchMode(true);
                        mEditCash.setClickable(true);
                        mLinearPayCheque.setVisibility(View.GONE);
                    } else {
                        // mEditCash.setEnabled(false);
                        mEditCash.setFocusable(false);
                        mEditCash.setFocusableInTouchMode(false);
                        mEditCash.setClickable(false);
                        mEditCash.setText("0");
                        mLinearPayCheque.setVisibility(View.GONE);
                    }

                    break;
                case R.id.check_box_surgery_payment_e_wallet:
                    if (isChecked) {
                        PaymentType = "E-Wallet";
                        //mEditEWallet.setEnabled(true);
                        mEditEWallet.setFocusable(true);
                        mEditEWallet.setFocusableInTouchMode(true);
                        mEditEWallet.setClickable(true);
                        mLinearPayCheque.setVisibility(View.GONE);
                    } else {
                        //mEditEWallet.setEnabled(false);
                        mEditEWallet.setFocusable(false);
                        mEditEWallet.setFocusableInTouchMode(false);
                        mEditEWallet.setClickable(false);
                        mEditEWallet.setText("0");
                        mLinearPayCheque.setVisibility(View.GONE);
                    }

                    break;
                case R.id.check_box_surgery_payment_neft:
                    if (isChecked) {
                        PaymentType = "NEFT";
                        //mEditNEFT.setEnabled(true);
                        mEditNEFT.setFocusable(true);
                        mEditNEFT.setFocusableInTouchMode(true);
                        mEditNEFT.setClickable(true);
                        mLinearPayCheque.setVisibility(View.GONE);
                    } else {
//                        mEditNEFT.setEnabled(false);
                        mEditNEFT.setFocusable(false);
                        mEditNEFT.setFocusableInTouchMode(false);
                        mEditNEFT.setClickable(false);
                        mEditNEFT.setText("0");
                        mLinearPayCheque.setVisibility(View.GONE);
                    }

                    break;
                case R.id.check_box_surgery_payment_cheque:
                    if (isChecked) {
                        PaymentType = "Cheque";
                        mEditCheque.setFocusable(true);
                        mEditCheque.setFocusableInTouchMode(true);
                        mEditCheque.setClickable(true);
                        mLinearPayCheque.setVisibility(View.VISIBLE);
                    } else {
                        mEditCheque.setFocusable(false);
                        mEditCheque.setFocusableInTouchMode(false);
                        mEditCheque.setClickable(false);
                        mEditCheque.setText("0");
                        mLinearPayCheque.setVisibility(View.GONE);
                    }

                    break;
            }
        }
    };



    private boolean chekvalidateAmount() {
        boolean status = true;
        float total = 0;

        float total_amount = Float.parseFloat(mEditTotalAmount.getText().toString().trim());

        String cash = mEditCash.getText().toString().trim();
        String neft = mEditNEFT.getText().toString().trim();
        String eWallet = mEditEWallet.getText().toString().trim();
        String cheque = mEditCheque.getText().toString().trim();

        if (cash != null && !cash.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            float val_cash = Float.parseFloat(cash);
            total = total + val_cash;
        }
        if (neft != null && !neft.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            float val_neft = Float.parseFloat(neft);
            total = total + val_neft;
        }
        if (eWallet != null && !eWallet.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            float val_ewallet = Float.parseFloat(eWallet);
            total = total + val_ewallet;
        }
        if (cheque != null && !cheque.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            float val_cheque = Float.parseFloat(cheque);
            total = total + val_cheque;
        }

        if (total > total_amount) {
            status = false;
        }
        return status;
    }

    /**
     * Update OutStand Data
     */
    private void updateoustanding() {
        float total = 0;
        float total_amount = Float.parseFloat(mEditTotalAmount.getText().toString().trim());

        String cash = mEditCash.getText().toString().trim();
        String neft = mEditNEFT.getText().toString().trim();
        String eWallet = mEditEWallet.getText().toString().trim();
        String cheque = mEditCheque.getText().toString().trim();

        if (cash != null && !cash.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            float val_cash = Float.parseFloat(cash);
            total = total + val_cash;
        }
        if (neft != null && !neft.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            float val_neft = Float.parseFloat(neft);
            total = total + val_neft;
        }
        if (eWallet != null && !eWallet.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            float val_ewallet = Float.parseFloat(eWallet);
            total = total + val_ewallet;
        }
        if (cheque != null && !cheque.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            float val_cheque = Float.parseFloat(cheque);
            total = total + val_cheque;
        }

        float out_standing_amount = total_amount - total;

        mEditPayAmount.setText(String.valueOf(total));
        mEditOutStanding.setText(String.valueOf(out_standing_amount));
    }


    /**
     * Check Validation
     *
     * @return -return Status
     */
    private boolean checkValidation() {

        boolean status = true;

        String pay_amount = mEditPayAmount.getText().toString();

        String date_time = mEditDateTime.getText().toString();

        if (date_time.equalsIgnoreCase("")) {
            status = false;
            mEditDateTime.setError(mActivity.getString(R.string.error_select_date_time));

        }

        if (mLinearContainer.getChildCount() == 0) {
            status = false;
            Common.setCustomToast(mActivity, mActivity.getString(R.string.error_please_add_service));
        }

        if (status) {
            if (pay_amount == null && pay_amount.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                Common.setCustomToast(mActivity, mActivity.getString(R.string.error_please_enter_pay_amount));
                status = false;
            } else {
                float val = Float.parseFloat(mEditPayAmount.getText().toString().trim());
                if (val < 0.1) {
                    Common.setCustomToast(mActivity,mActivity.getString(R.string.error_please_enter_pay_amount));
                    status = false;
                }
            }

            float amount = 0;

            float total_amount= Float.parseFloat(mEditTotalAmount.getText().toString());

            String NEFT = mEditNEFT.getText().toString();
            String eWallet = mEditEWallet.getText().toString();
            String CashRs = mEditCash.getText().toString();
            String ChequeRs = mEditCheque.getText().toString();


            if (!CashRs.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                    && Float.parseFloat(CashRs) > 0.0) {
                amount = amount + Float.parseFloat(CashRs);
            }

            if (!ChequeRs.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                    && Float.parseFloat(ChequeRs) > 0.0) {
                amount = amount + Float.parseFloat(ChequeRs);
            }

            if (!NEFT.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                    && Float.parseFloat(NEFT) > 0.0) {
                amount = amount + Float.parseFloat(NEFT);
            }

            if (!eWallet.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                    && Float.parseFloat(eWallet) > 0.0) {
                amount = amount+Float.parseFloat(eWallet);
            }

            if (amount>total_amount){
                status=false;
            }

            boolean checked=mCheckCheque.isChecked();
            if (checked){
                if (mEditChequeNo.getText().toString().trim().
                        equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                    status=false;
                    mEditChequeNo.setError(mActivity.getResources().getString(R.string.error_field_required));
                }
                if (mEditAccountNo.getText().toString().trim().
                        equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                    status=false;
                    mEditAccountNo.setError(mActivity.getResources().getString(R.string.error_field_required));
                }
                if (mEditBankName.getText().toString().trim().
                        equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                    status=false;
                    mEditBankName.setError(mActivity.getResources().getString(R.string.error_field_required));
                }
            }

        }
        return status;
    }


    /**
     * Add Services on Layout
     */
    private void addService() {
        try {

            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.row_surgery_payment_services_item);

            TextView mTextHeader = dialog.findViewById(R.id.text_row_surgery_payment_service_item_header);

            Spinner mSpinner = dialog.findViewById(R.id.spinner_row_surgery_payment_service_item);

            EditText mEditAmount = dialog.findViewById(R.id.edit_row_surgery_payment_service_amount);

            Button mButtonSubmit = dialog.findViewById(R.id.button_row_surgery_payment_service_item_submit);


            mTextHeader.setText(mActivity.getResources().getString(R.string.text_add_service));
            setSpinnerAdapter(mSpinner, mSpinner.getSelectedItemPosition());

            mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position > 0) {

                        String amount = mArrService.get(position).getRate();
                        mEditAmount.setText(amount);

                        //updateServiceCalculate();
                    } else {
                        mEditAmount.setText("");
                        //updateServiceCalculate();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            mButtonSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //dialog.dismiss();
                    int selected_index = mSpinner.getSelectedItemPosition();
                    if (selected_index == 0) {
                        Common.setCustomToast(mActivity, mActivity.getString(R.string.error_please_select_service));
                    } else {
                        String data = mArrService.get(selected_index - 1).getServicesName();
                        String price = mEditAmount.getText().toString();

                        dialog.dismiss();
                        AddServiceLayout(data, price, selected_index);
                    }
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void AddServiceLayout(String data, String price, int selected_index) {
        View child_view = LayoutInflater.from(mActivity)
                .inflate(R.layout.row_surgery_payment_service_item, null, false);

        RelativeLayout mRelativeHeader =
                child_view.findViewById(R.id.relative_surgery_payment_item_header);

        int size = mLinearContainer.getChildCount();
        if (size == 0) {
            mRelativeHeader.setVisibility(View.VISIBLE);
        } else {
            mRelativeHeader.setVisibility(View.GONE);
        }

        TextView service_name = child_view.findViewById(R.id.text_surgery_payment_item_service);
        EditText service_price = child_view.findViewById(R.id.text_surgery_payment_item_price);
        ImageView mImageDelete = child_view.findViewById(R.id.image_surgery_payment_item_delete);

        service_name.setText(data);
        service_name.setTag(selected_index);
        service_price.setText(price);

        service_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateServiceCalculate();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mImageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLinearContainer.removeView(child_view);
                updateServiceCalculate();
            }
        });


        mLinearContainer.addView(child_view);
        updateServiceCalculate();
    }


    /**
     * Calculate Services Total Amount
     */
    private void updateServiceCalculate() {

        float total = 0;

        if (mLinearContainer.getChildCount() > 0) {
            for (int a = 0; a < mLinearContainer.getChildCount(); a++) {
                View child_view = mLinearContainer.getChildAt(a);

                TextView mEditAmount = child_view.findViewById(R.id.text_surgery_payment_item_price);


                String amount = mEditAmount.getText().toString();

                if (amount != null && !amount.isEmpty() && !amount.equalsIgnoreCase(
                        AppConstants.STR_EMPTY_SPACE)) {

                    float _amount = Float.parseFloat(amount);
                    total = total + _amount;
                }
            }
        }

        mEditTotalAmount.setText(String.valueOf(total));
        if (mLinearContainer.getChildCount() > 0) {
            mLinearPayAmount.setVisibility(View.VISIBLE);
        } else {
            mLinearPayAmount.setVisibility(View.GONE);
        }
    }


    /**
     * Method should to call Service and Package API
     */
    private void callToServiceAPI() {
        try {

            if (mAppUtils.getConnectionState()) {
                String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

                RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                        APICommonMethods.setPaymentServicesPackageListJson(hospital_database));

                Call<ServicesPackagesModel> call = RetrofitClient.createService(ApiInterface.class).getPaymentServicesPackageList(body);
                call.enqueue(new Callback<ServicesPackagesModel>() {
                    @Override
                    public void onResponse(@NonNull Call<ServicesPackagesModel> call, @NonNull Response<ServicesPackagesModel> response) {
                        Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                        try {

                            if (response.isSuccessful() &&
                                    response.body().getError().equals(AppConstants.API_SUCCESS_ERROR)) {

                                ArrayList<ServicesPackagesModel.Service> services =
                                        (ArrayList<ServicesPackagesModel.Service>) response.body().getService();
                                ArrayList<ServicesPackagesModel.Package> packages =
                                        (ArrayList<ServicesPackagesModel.Package>) response.body().getPackage();

                                bindServices(services);
                            }
                        } catch (Exception e) {
                            Common.insertLog(e.getMessage());
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<ServicesPackagesModel> call, @NonNull Throwable t) {
                        Common.insertLog("Failure:::> " + t.getMessage());
                    }
                });
            }


        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * bind Array List on Spinner
     *
     * @param services
     */
    private void bindServices(ArrayList<ServicesPackagesModel.Service> services) {

        if (services.size() > 0) {

            mArrServiceTitle.add(mActivity.getResources().getString(R.string.spinner_select_service));

            for (int a = 0; a < services.size(); a++) {

                if (services.get(a).getItem().size() > 0) {

                    for (int i = 0; i < services.get(a).getItem().size(); i++) {

                        ServicesPackagesModel.Item item = services.get(a).getItem().get(i);

                        mArrService.add(item);

                        mArrServiceTitle.add(item.getServicesName());
                    }
                }
            }
        }

    }


    /**
     * Set Spinner Adapter
     */
    private void setSpinnerAdapter(Spinner spinnerService, int mSelectedCityIndex) {

        spinnerService.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

        // ToDo: Sets the spinner color as per the theme applied
        ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrServiceTitle) {
            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                // Cast the drop down items (popup items) as text view
                TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                // Set the text color of drop down items
                tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                // If this item is selected item
                if (position == mSelectedCityIndex) {
                    // Set spinner selected popup item's text color
                    tv.setTextColor(Common.setThemeColor(mActivity));
                }
                // Return the modified view
                return tv;
            }
        };
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerService.setAdapter(adapter);
    }

    /***
     * this method call Payment API
     */
    private void callToAddPaymentAPI() {

        try {

            float outstanding = 0;

            String mUserID = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);

            String date = mEditDateTime.getText().toString();

            String paymentDate = Common.setConvertDateFormat(mActivity, date,
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss),
                    mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd_hh_mm_ss));

            String mStatus = AppConstants.STR_STATUS_SUCCESS;
            String subTotal = mEditTotalAmount.getText().toString();
            String total = mEditTotalAmount.getText().toString();
            String strPaymetType = PaymentType;
            String ChequeNo = "";
            String NEFT = mEditNEFT.getText().toString();
            String bankName = mEditBankName.getText().toString();
            String branch = AppConstants.STR_EMPTY_SPACE;
            String account_no = mEditAccountNo.getText().toString();
            String eWallet = mEditEWallet.getText().toString();
            String CashRs = mEditCash.getText().toString();
            String ChequeRs = mEditChequeNo.getText().toString();


            String notes = AppConstants.STR_EMPTY_STRING;
            String utr_no = AppConstants.STR_EMPTY_STRING;
            String ifsc_code = AppConstants.STR_EMPTY_STRING;


            float val_total = Float.parseFloat(total);
            outstanding = Float.parseFloat(mEditOutStanding.getText().toString().trim());


            float received = val_total - outstanding;


            JSONArray arrayItems = getArrPayItem();

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);


            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAddPaymentCheckUpJson(strPatientId, paymentDate, String.valueOf(strAppointmentID), mStatus, notes, mUserID,
                            subTotal, total, String.valueOf(received), String.valueOf(outstanding), arrayItems, PaymentType, ChequeNo, ifsc_code,
                            utr_no, bankName, branch, account_no, hospital_database, CashRs, ChequeRs, NEFT, eWallet));

            Common.insertLog("Request Payment...." + requestBody.toString());

            Common.showLoadingDialog(mActivity, mActivity.getResources().getString(R.string.text_load));
            Call<AddBillingPaymentModel> call = RetrofitClient.createService(ApiInterface.class).addBillingsPayment(requestBody);
            call.enqueue(new Callback<AddBillingPaymentModel>() {
                @Override
                public void onResponse(@NonNull Call<AddBillingPaymentModel> call, @NonNull Response<AddBillingPaymentModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);
                        Common.hideDialog();

                        if (response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            Common.setCustomToast(mActivity, mMessage);

                            AddNewPreliminaryExaminationFragment.changePage(9);
                            if (mArrReceptionistPayment.size() > 0) {
                                mArrReceptionistPayment.clear();
                            }
                            callPaymentListAPI();

                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Common.hideDialog();
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddBillingPaymentModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.hideDialog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Common.hideDialog();
        }
    }


    /**
     * Create Json Array Item List
     *
     * @return
     */
    private JSONArray getArrPayItem() {
        JSONArray jsonArray = new JSONArray();
        try {

            if (mLinearContainer.getChildCount() > 0) {
                for (int a = 0; a < mLinearContainer.getChildCount(); a++) {

                    View child_view = mLinearContainer.getChildAt(a);

                    TextView service_name = child_view.findViewById(R.id.text_surgery_payment_item_service);
                    TextView service_price = child_view.findViewById(R.id.text_surgery_payment_item_price);
                    ImageView mImageDelete = child_view.findViewById(R.id.image_surgery_payment_item_delete);

                    int selected_index_service = Integer.parseInt(service_name.getTag().toString());
                    String discountedPrice = service_price.getText().toString();

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(WebFields.ADD_PAYMENT_LIST.REQUEST_ITEM_Type, "Service");

                    jsonObject.put(WebFields.ADD_PAYMENT_LIST.REQUEST_ITEM_ID,
                            mArrService.get(selected_index_service - 1).getBillingID());

                    jsonObject.put(WebFields.ADD_PAYMENT_LIST.REQUEST_ITEM_PARENT_ID,
                            mArrService.get(selected_index_service - 1).getParentID());

                    jsonObject.put(WebFields.ADD_PAYMENT_LIST.REQUEST_ITEM_TITLE,
                            mArrService.get(selected_index_service - 1).getServicesName());

                    jsonObject.put(WebFields.ADD_PAYMENT_LIST.REQUEST_ITEM_AMOUNT, discountedPrice);

                    jsonObject.put(WebFields.ADD_PAYMENT_LIST.REQUEST_ITEM_ORIGINAL_AMOUNT,
                            mArrService.get(selected_index_service - 1).getRate());


                    jsonArray.put(jsonObject);
                }
            }

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
        return jsonArray;
    }


    public void openDialogTreatmentSuggested() {
        try {

            Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.custom_dialog_history_preliminary);

            TextView mTextTitle = dialog.findViewById(R.id.text_view_custom_dialog_history_preliminary);
            RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
            ImageView mImageClose = dialog.findViewById(R.id.image_custom_dialog_history_preliminary_close);

            RelativeLayout noInternetConnection = dialog.findViewById(R.id.relative_no_internet);
            RelativeLayout noDataFound = dialog.findViewById(R.id.relative_no_data_available);

            mTextTitle.setText(mActivity.getResources().getString(R.string.text_payment));

            mImageClose.setColorFilter(Common.setThemeColor(mActivity));

            mImageClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            LinearLayoutManager manager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(manager);


            ReceptionistPatientDetailPaymentAdapter mAdapterReceptionistPatientDetailPayment =
                    new ReceptionistPatientDetailPaymentAdapter(mActivity, recyclerView, mArrReceptionistPayment, false);
            recyclerView.setAdapter(mAdapterReceptionistPatientDetailPayment);

            if (mArrReceptionistPayment.size() > 0) {
                noDataFound.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            } else {
                noDataFound.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     *OpenDialog for closing patient file
     */
    public void openFileClosingAlert() {


        AlertDialog.Builder builder = new AlertDialog.Builder(
                mActivity);
        builder.setTitle("Close File");
        builder.setMessage("Are you sure you want to close file?");
        builder.setNegativeButton("NO",
                (dialog, which) -> dialog.dismiss());
        builder.setPositiveButton("YES",
                (dialog, which) -> closeFile());
        builder.show();

    }

    private void closeFile() {
        try {
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.closeFileJson(strAppointmentID, mDatabaseName));

            Call<CloseFileModel> call = RetrofitClient.createService(ApiInterface.class).closeFileAPI(body);
            call.enqueue(new Callback<CloseFileModel>() {
                @Override
                public void onResponse(@NonNull Call<CloseFileModel> call, @NonNull Response<CloseFileModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        int mError = response.body().getError();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                Common.insertLog("IF");
                                Common.setCustomToast(mActivity,mMessage);
                            } else {
                                Common.insertLog("ELSE");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CloseFileModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the payment listing for receptionist
     */
    private void callPaymentListAPI() {
        try {
            if (mFirstName == null) {
                mFirstName = AppConstants.STR_EMPTY_STRING;
            }

            if (mPatientCode == null) {
                mPatientCode = AppConstants.STR_EMPTY_STRING;
            }

            if (mMobileNo == null) {
                mMobileNo = AppConstants.STR_EMPTY_STRING;
            }

            if (mAppointmentNo == null) {
                mAppointmentNo = AppConstants.STR_EMPTY_STRING;
            }

            if (mAppointmentDate == null) {
                mAppointmentDate = mActivity.getResources().getString(R.string.date_format_first_time);
            }

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            int currentPageIndex = 1;

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setReceptionistPaymentListJson(currentPageIndex, mFirstName, mPatientCode, mMobileNo, mAppointmentNo, mAppointmentDate, hospital_database));

            Call<ReceptionistPaymentModel> call = RetrofitClient.createService(ApiInterface.class).getReceptionistPaymentList(requestBody);
            call.enqueue(new Callback<ReceptionistPaymentModel>() {
                @Override
                public void onResponse(@NonNull Call<ReceptionistPaymentModel> call, @NonNull Response<ReceptionistPaymentModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {

                        if (response.body().getError() == AppConstants.API_SUCCESS_ERROR) {

                            mArrReceptionistPayment.addAll(response.body().getData());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onFailure(@NonNull Call<ReceptionistPaymentModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
