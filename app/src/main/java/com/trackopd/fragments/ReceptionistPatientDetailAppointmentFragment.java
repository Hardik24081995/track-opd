package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.ReceptionistAppointmentAdapter;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.AppointmentReasonsModel;
import com.trackopd.model.ReceptionistAppointmentModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.utils.RecyclerProgressView;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceptionistPatientDetailAppointmentFragment extends Fragment implements ReceptionistAppointmentAdapter.SingleClickListener {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private AppUtil mAppUtils;
    private SwipeRefreshLayout mSwipeRefreshView;
    private RelativeLayout mRelativeNoData, mRelativeNoInternet;
    private Button mButtonRetry;
    private RecyclerView mRecyclerView;
    private ReceptionistAppointmentAdapter mAdapterReceptionistAppointment;
    private ArrayList<ReceptionistAppointmentModel> mArrReceptionistAppointment;
    ArrayList<AppointmentReasonsModel> mArrAppointmentReasons;
    private boolean isFirstTime = true, isStarted = false, isVisible = false;
    private String mPatientId = "", mName = "", mPatientCode = "", mMobileNo = "",
            mMRDNo = "", mAppointmentNo = "",
            mAppointmentDate = "0000-00-00", mFirstName = "", mLastName = "", mUserType,mProfilePic="";
    private int mDoctorId = -1;

    // Load More Listener Variables
    private int currentPageIndex = 1, lastFetchRecord = 0, totalRecords = 0;
    private boolean isRefresh = false, isLoadMore = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_receptionist_patient_detail_appointment, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrReceptionistAppointment = new ArrayList<>();
        mArrAppointmentReasons = new ArrayList<>();

        Common.insertLog("Tab 2");
        mAppUtils = new AppUtil(mActivity);

        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        getBundle();
        getIds();
        setRegListeners();
        callToGetReasonAPI();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mFirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mLastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mPatientCode = bundle.getString(AppConstants.BUNDLE_PATIENT_CODE);
                mMobileNo = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mMRDNo = bundle.getString(AppConstants.BUNDLE_MRD_NO);
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mName = mFirstName + AppConstants.STR_EMPTY_SPACE + mLastName;
                mProfilePic=bundle.getString(AppConstants.BUNDLE_PATIENT_PROFILE);

            }
            Common.insertLog("Bundle Detail Appo Get::::> " + mFirstName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        this.menu = menu;
        menu.clear();
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.invalidateOptionsMenu();
        mActivity.getMenuInflater().inflate(R.menu.menu_detail_appointment, menu);

        MenuItem menuAdd = menu.findItem(R.id.action_detail_appointment_add);
        menuAdd.setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_detail_appointment_add:
                 callToAddAppointmentFragment();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Recycler View
            mRecyclerView = mView.findViewById(R.id.recycler_view);

            // Swipe Refresh View
            mSwipeRefreshView = mView.findViewById(R.id.swipe_refresh_view);

            // Relative Layouts
            mRelativeNoData = mView.findViewById(R.id.relative_no_data_available);
            mRelativeNoInternet = mView.findViewById(R.id.relative_no_internet);

            // Buttons
            mButtonRetry = mView.findViewById(R.id.button_retry);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Click Listeners
            mButtonRetry.setOnClickListener(clickListener);

            // ToDo: Set On Refresh Listeners
            mSwipeRefreshView.setOnRefreshListener(refreshListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_retry:
                    callShimmerView();
                    break;
            }
        }
    };

    /**
     * Set On Refresh Listeners
     */
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {

        @Override
        public void onRefresh() {
            isFirstTime = true;

            if (!mAppUtils.getConnectionState()) {
                mRelativeNoInternet.setVisibility(View.VISIBLE);
                mSwipeRefreshView.setRefreshing(false);
                mArrReceptionistAppointment.clear();
                setViewVisibility();
            } else {
                mRelativeNoInternet.setVisibility(View.GONE);
                if (!isLoadMore) {
                    isRefresh = true;

                    mAppointmentDate = mActivity.getResources().getString(R.string.date_format_first_time);
                    mDoctorId = -1;

                    if (mArrReceptionistAppointment != null)
                        mArrReceptionistAppointment.clear();
                    if (mAdapterReceptionistAppointment != null)
                        mAdapterReceptionistAppointment.notifyDataSetChanged();
                    currentPageIndex = 1;
                    callShimmerView();
                } else
                    mSwipeRefreshView.setRefreshing(false);
            }
        }
    };

    /**
     * This method is used to call the shimmer effect for recycler view before calling the API
     */
    private void callShimmerView() {
        if (!mAppUtils.getConnectionState()) {
            mRelativeNoInternet.setVisibility(View.VISIBLE);
            mArrReceptionistAppointment.clear();
            setViewVisibility();
        } else {
            mRelativeNoInternet.setVisibility(View.GONE);
            if (isFirstTime) {
                showProgressView();
                isFirstTime = false;
                callAppointmentListAPI();
            } else {
                callAppointmentListAPI();
            }
        }
    }

    /**
     * Show progress view when page load for the first time
     */
    private void showProgressView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.VISIBLE);

                RecyclerProgressView.startProgress((LinearLayout) mView.findViewById(R.id.linear_add_recycler_progress_view),
                        R.layout.row_receptionist_appointment_item, getActivity());
            }
        }, 100);
    }

    /**
     * Call To Get Appointment Reason List API
     */
    private void callToGetReasonAPI() {
        try {
            String hospital_database=GetJsonData.
                    getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAppointmentReason(hospital_database));

            Call<AppointmentReasonsModel> call = RetrofitClient.createService(ApiInterface.class).getReason(requestBody);
            call.enqueue(new Callback<AppointmentReasonsModel>() {
                @Override
                public void onResponse(@NonNull Call<AppointmentReasonsModel> call, @NonNull Response<AppointmentReasonsModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        if (response.isSuccessful() && response.body().getError() == 200) {
                            mArrAppointmentReasons.addAll(response.body().getData());
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AppointmentReasonsModel> call, @NonNull Throwable t) {
                    Common.insertLog(t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the appointment listing for receptionist
     */
    private void callAppointmentListAPI() {
        try {
            if (mName == null) {
                mName = AppConstants.STR_EMPTY_STRING;
            }

            if (mMobileNo == null) {
                mMobileNo = AppConstants.STR_EMPTY_STRING;
            }

            if (mPatientCode == null) {
                mPatientCode = AppConstants.STR_EMPTY_STRING;
            }

            if (mMRDNo == null) {
                mMRDNo = AppConstants.STR_EMPTY_STRING;
            }

            if (mAppointmentNo == null) {
                mAppointmentNo = AppConstants.STR_EMPTY_STRING;
            }

            if (mAppointmentDate == null) {
                mAppointmentDate = mActivity.getResources().getString(R.string.date_format_first_time);
            }

            String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setReceptionistAppointmentListJson(mActivity, currentPageIndex, mUserType, AppConstants.STR_EMPTY_STRING, mPatientCode, mMobileNo, mMRDNo, mAppointmentNo, mAppointmentDate, mDoctorId, mActivity.getResources().getString(R.string.tab_all), mActivity.getResources().getString(R.string.tab_all), String.valueOf(-1), hospital_database));

            Call<ReceptionistAppointmentModel> call = RetrofitClient.createService(ApiInterface.class).getReceptionistAppointmentList(requestBody);
            call.enqueue(new Callback<ReceptionistAppointmentModel>() {
                @Override
                public void onResponse(@NonNull Call<ReceptionistAppointmentModel> call, @NonNull Response<ReceptionistAppointmentModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        stopProgressView();
                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                ArrayList<ReceptionistAppointmentModel> appointmentModel = new ArrayList<>(Arrays
                                        .asList(new GsonBuilder().serializeNulls().create()
                                                .fromJson(new JsonParser().parse(jsonObject.getString(WebFields.DATA)).
                                                        getAsJsonArray(), ReceptionistAppointmentModel[].class)));

                                if (mArrReceptionistAppointment != null && mArrReceptionistAppointment.size() > 0 &&
                                        mAdapterReceptionistAppointment != null) {
                                    mArrReceptionistAppointment.addAll(appointmentModel);
                                    mAdapterReceptionistAppointment.notifyDataSetChanged();
                                    lastFetchRecord = mArrReceptionistAppointment.size();
                                } else {
                                    mArrReceptionistAppointment = appointmentModel;
                                    setAdapterData();
                                    lastFetchRecord = mArrReceptionistAppointment.size();
                                    totalRecords = jsonObject.getInt(WebFields.ROW_COUNT);
                                    setLoadMoreClickListener();

                                    if (mRecyclerView.getVisibility() == View.GONE) {
                                        stopProgressView();
                                    }
                                }
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                            stopProgressView();
                            setViewVisibility();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ReceptionistAppointmentModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    stopProgressView();
                    setViewVisibility();
                }
            });
        } catch (Exception e) {mActivity.openOptionsMenu();
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mGridLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(mGridLayoutManager);

            stopProgressView();
            mAdapterReceptionistAppointment = new ReceptionistAppointmentAdapter(mActivity, mRecyclerView, mArrReceptionistAppointment, mArrAppointmentReasons);
            mRecyclerView.setAdapter(mAdapterReceptionistAppointment);
            mAdapterReceptionistAppointment.setOnItemClickListener(this);

            setViewVisibility();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the view visibility as per the array size
     */
    private void setViewVisibility() {
        if (mArrReceptionistAppointment.size() != 0) {
            mRelativeNoData.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mRelativeNoData.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    /**
     * Show progress view when page load for the first time
     */
    private void stopProgressView() {
        try {
            mSwipeRefreshView.setRefreshing(false);
            isRefresh = false;
            isLoadMore = false;
            RecyclerProgressView.stopProgress();
            mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            hideProgressBar();
        }
    }

    /**
     * This method redirects you to the Add Receptionist Appointment Fragment
     */
    private void callToAddAppointmentFragment() {
        try {
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            }

            Fragment fragment = new AddAppointmentFragment();
            Bundle bundle = new Bundle();
            bundle.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, mFirstName);
            bundle.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, mLastName);
            bundle.putString(AppConstants.BUNDLE_PATIENT_CODE, mPatientCode);
            bundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mMobileNo);
            bundle.putString(AppConstants.BUNDLE_PATIENT_ID, mPatientId);
            bundle.putString(AppConstants.BUNDLE_PATIENT_PROFILE,mProfilePic);

            fragment.setArguments(bundle);
            Common.insertLog("Bundle Detail Appointment Set::::> " + mFirstName);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_receptionist_appointment))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_receptionist_appointment))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Hide the progress bar
     */
    private void hideProgressBar() {
        try {
            if (mArrReceptionistAppointment != null && mArrReceptionistAppointment.size() > 0 &&
                    mArrReceptionistAppointment.get(mArrReceptionistAppointment.size() - 1) == null) {

                mArrReceptionistAppointment.remove(mArrReceptionistAppointment.size() - 1);
                mAdapterReceptionistAppointment.notifyItemRemoved(mArrReceptionistAppointment.size());

                mAdapterReceptionistAppointment.notifyDataSetChanged();
                mAdapterReceptionistAppointment.setLoaded();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RecyclerProgressView.stopProgress();
    }

    /**
     * On load more click listener
     */
    private void setLoadMoreClickListener() {
        mAdapterReceptionistAppointment.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (mArrReceptionistAppointment != null && mArrReceptionistAppointment.size() > 0 && mArrReceptionistAppointment.size() <
                        totalRecords) {
                    if (!mAppUtils.getConnectionState()) {
                        mRelativeNoInternet.setVisibility(View.VISIBLE);
                        mArrReceptionistAppointment.clear();
                        setViewVisibility();
                    } else {
                        mRelativeNoInternet.setVisibility(View.GONE);
                        if (!isRefresh) {
                            isLoadMore = true;
                            mArrReceptionistAppointment.add(null);
                            mAdapterReceptionistAppointment.notifyItemInserted(mArrReceptionistAppointment.size() - 1);

                            currentPageIndex = (mArrReceptionistAppointment.size() / Common.getPageSizeForPagination(mActivity)) + 1;
                            callAppointmentListAPI();
                        }
                    }
                }
            }
        });
    }

    /**
     * Receptionist Patient Detail Appointment Fragment on visible
     *
     * @param isVisibleToUser - This can check the fragment is visible when swiping and will return
     *                        the boolean value based on that
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        isVisible = isVisibleToUser;
        if (isStarted && isVisible) {
            mArrReceptionistAppointment.clear();

            callShimmerView();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
            mArrReceptionistAppointment.clear();
            if (mAdapterReceptionistAppointment != null) {
                mAdapterReceptionistAppointment.notifyDataSetChanged();
            }
            getBundle();
            callShimmerView();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isVisible = false;
        isStarted = false;
    }

    @Override
    public void onItemSelected(String date) {

    }
}