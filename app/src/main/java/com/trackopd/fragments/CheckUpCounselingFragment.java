package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.trackopd.R;
import com.trackopd.adapter.CouselingPackageOptionSugestionAdapter;
import com.trackopd.adapter.HistoryTreatmentSuggestedAdapter;
import com.trackopd.model.AddPatientModel;
import com.trackopd.model.CloseFileModel;
import com.trackopd.model.CouncillorModel;
import com.trackopd.model.CouselingPackageSuggestionModel;
import com.trackopd.model.DoctorModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.KeyboardUtility;
import com.trackopd.utils.SessionManager;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CheckUpCounselingFragment extends Fragment implements
        HistoryTreatmentSuggestedAdapter.onHistoryDiagnosisListener, CouselingPackageOptionSugestionAdapter.customSpinnerListener {

    public ArrayList<PreliminaryExaminationDetailsModel.Data.SurgeryType> mArrSurgeryTypeList;
    public ArrayList<PreliminaryExaminationDetailsModel.Data.Eye> mArrEyeList;
    public ArrayList<PreliminaryExaminationDetailsModel.Data.OptionSuggested> mArrOptionSuggestedList;
    private View mView;
    private Activity mActivity;
    private Button mButtonSubmit;
    private EditText mEditRemarks, mEditAdmissionDate;
    private Spinner mSpinnerDoctor, mSpinnerCouncillor,
            mSpinnerEye, mSpinnerSurgery, mSpinnerSurgeryTwo, mSpinnerOptionSuggested, mSpinnerPatietPreferredOption;
    private RadioGroup mRadioGroupDiseaseExplained, mRadioSurgeryExplained, mRadioGroupMedicaid;
    private RadioButton mRadioDiseaseYes, mRadioDiseaseNo, mRadioSurgeryYes, mRadioSurgeryNo;
    private ArrayList<String> mArrDoctor, mArrCouncillor, mArrSurgeryType, mArrEye,
            mArrOptionSuggested, mArrOptionOpted;
    private ArrayList<DoctorModel> mArrDoctorList;
    private ArrayList<CouncillorModel> mArrCouncillorList;
    private ArrayList<HistoryPreliminaryExaminationModel.Data> mArrHistory;

    private ArrayList<CouselingPackageSuggestionModel> mArrPackageSuggested;

    private String mFirstName, mLastName, mMobileNo, mPatientId, mSurgeryTypeID = "",
            mDiseaseExplained = "Yes", mSurgeryExplained = "Yes", mMediclaim = "", mPatientProfile = "", mDoctorId = "", mCouncillorId = "", mEyeId = "", mOptionSuggested = "",
            mOptionOpted = "", mSurgeryID = "", mTreatmentDate = "";

    private int mSelectedEyeIndex, mSelectedCouncillorIndex, mSelectedDoctorIndex,
            mCurrentPage = 1, mHistoryDoctorID = -1, mSelectedPackagePreferedIndex = 0;

    private RelativeLayout mRelativeNoDataFound;
    private LinearLayout mLinearSugestation;
    private ImageView mImageAddSuggestation;
    private int mAppointmentId;

    private CouselingPackageOptionSugestionAdapter mAdapterPackageSuggesteed;
    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.edit_check_up_counseling_admission_date_time:
                    Common.openPastDateTimePicker(mActivity, mEditAdmissionDate);
                    break;

                case R.id.button_check_up_counseling_submit:
                    doAddInquiryForm();
                    break;
                case R.id.image_check_up_counseling_add_sugestation:
                    //doAddSuggestation(AppConstants.STR_EMPTY_STRING,AppConstants.STR_EMPTY_STRING);
                    break;
            }
        }
    };
    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinner_check_up_counseling_eye:
                    mSelectedEyeIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedEyeIndex != 0) {
                        mEyeId = mArrEyeList.get(mSelectedEyeIndex - 1).getEyeID();
                    }
                    break;

                case R.id.spinner_check_up_counseling_doctor_name:
                    mSelectedDoctorIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedDoctorIndex != 0) {
                        mDoctorId = mArrDoctorList.get(mSelectedDoctorIndex - 1).getDoctorID();
                        String mDoctorName = (mArrDoctorList.get(mSelectedDoctorIndex - 1).getDoctorName());
                        Common.insertLog("mDoctorID::> " + mDoctorId);
                        Common.insertLog("mDoctorName::> " + mDoctorName);
                    }
                    break;

                case R.id.spinner_check_up_counseling_councillor_name:
                    mSelectedCouncillorIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedCouncillorIndex != 0) {
                        mCouncillorId = mArrCouncillorList.get(mSelectedCouncillorIndex - 1).getCouncillorID();
                        String mCouncillorName = (mArrCouncillorList.get(mSelectedCouncillorIndex - 1).getCouncillorName());
                        Common.insertLog("mCouncillorId::> " + mCouncillorId);
                        Common.insertLog("mCouncillorName::> " + mCouncillorName);
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_check_up_counseling, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        mArrCouncillor = new ArrayList<>();
        mArrCouncillorList = new ArrayList<>();
        mArrDoctor = new ArrayList<>();
        mArrDoctorList = new ArrayList<>();
        mArrSurgeryType = new ArrayList<>();
        mArrSurgeryTypeList = new ArrayList<>();
        mArrEye = new ArrayList<>();
        mArrEyeList = new ArrayList<>();
        mArrOptionSuggested = new ArrayList<>();
        mArrOptionSuggestedList = new ArrayList<>();
        mArrOptionOpted = new ArrayList<>();

        mArrHistory = new ArrayList<>();

        mArrPackageSuggested = new ArrayList<>();

        getBundle();
        getIds();
        setRegListeners();
        setData();
        callToDoctorAPI();
        callToCouncillorAPI();
        callToPreliminaryExaminationAPI();
        callToHistoryPreliminaryExamination();
        setHasOptionsMenu(true);

        mEditRemarks.setFilters(Common.getFilter());
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mFirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mLastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mMobileNo = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mPatientProfile = bundle.getString(AppConstants.BUNDLE_PATIENT_PROFILE);
                mTreatmentDate = bundle.getString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE);
                mAppointmentId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);

            }
            Common.insertLog("NEW PATIENT IDDDD::::> " + mPatientId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * get Declare Ids
     */
    private void getIds() {
        try {
            // Edit Texts
            mEditRemarks = mView.findViewById(R.id.edit_check_up_counseling_remarks);
            mEditAdmissionDate = mView.findViewById(R.id.edit_check_up_counseling_admission_date_time);

            // Spinners
            mSpinnerEye = mView.findViewById(R.id.spinner_check_up_counseling_eye);
            mSpinnerDoctor = mView.findViewById(R.id.spinner_check_up_counseling_doctor_name);
            mSpinnerCouncillor = mView.findViewById(R.id.spinner_check_up_counseling_councillor_name);
            mSpinnerOptionSuggested = mView.findViewById(R.id.spinner_check_up_counseling_option_suggested);
            mSpinnerPatietPreferredOption = mView.findViewById(R.id.spinner_check_up_counseling_option_opted);
            mSpinnerSurgery = mView.findViewById(R.id.spinner_check_up_counseling_surgery_name);
            mSpinnerSurgeryTwo = mView.findViewById(R.id.spinner_check_up_counseling_surgery_two);

            // Radio Groups
            mRadioSurgeryExplained = mView.findViewById(R.id.radio_group_check_up_counseling_surgery_explained);
            mRadioGroupMedicaid = mView.findViewById(R.id.radio_group_check_up_counseling_mediclaim);
            mRadioGroupDiseaseExplained = mView.findViewById(R.id.radio_group_check_up_counseling_disease_explained);

            //RadioButton
            mRadioDiseaseYes = mView.findViewById(R.id.radio_button_check_up_counseling_disease_explained_yes);
            mRadioDiseaseNo = mView.findViewById(R.id.radio_button_check_up_counseling_disease_explained_no);
            mRadioSurgeryYes = mView.findViewById(R.id.radio_button_check_up_counseling_surgery_explained_yes);
            mRadioSurgeryNo = mView.findViewById(R.id.radio_button_check_up_counseling_surgery_explained_no);


            // Buttons
            mButtonSubmit = mView.findViewById(R.id.button_check_up_counseling_submit);

            //Relative Layout
            mRelativeNoDataFound = mView.findViewById(R.id.relative_check_up_counseling_no_data_found);

            //Image View
            mImageAddSuggestation = mView.findViewById(R.id.image_check_up_counseling_add_sugestation);
            //Linear Layout
            mLinearSugestation = mView.findViewById(R.id.linear_check_up_counseling_layout);

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Register Listener
     */
    private void setRegListeners() {
        try {
            mEditAdmissionDate.setOnClickListener(clickListener);

            SessionManager manager = new SessionManager(mActivity);
            String closeFile = manager.getPreferences("ClosingFlags", "");

            if (closeFile.equalsIgnoreCase("Yes")) {
                mButtonSubmit.setClickable(false);
                Toast.makeText(mActivity, "Patient File Closed", Toast.LENGTH_SHORT).show();
            } else {
                mButtonSubmit.setOnClickListener(clickListener);
            }


            //mImageAddSuggestation.setOnClickListener(clickListener);

            mSpinnerEye.setOnItemSelectedListener(onItemSelectedListener);
            mSpinnerDoctor.setOnItemSelectedListener(onItemSelectedListener);
            mSpinnerCouncillor.setOnItemSelectedListener(onItemSelectedListener);
            mSpinnerSurgery.setOnItemSelectedListener(onItemSelectedListener);
            mSpinnerSurgeryTwo.setOnItemSelectedListener(onItemSelectedListener);
            mSpinnerPatietPreferredOption.setOnItemSelectedListener(onItemSelectedListener);


            mRadioGroupDiseaseExplained.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                    if (mRadioDiseaseYes.isChecked()) {
                        mDiseaseExplained = "Yes";
                    } else if (mRadioDiseaseNo.isChecked()) {
                        mDiseaseExplained = "No";
                    }

                }
            });

            mRadioSurgeryExplained.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                    if (mRadioSurgeryYes.isChecked()) {
                        mSurgeryExplained = "Yes";
                    } else if (mRadioSurgeryNo.isChecked()) {
                        mSurgeryExplained = "No";
                    }
                }
            });

            mRadioGroupMedicaid.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    int selAppointmentType = group.getCheckedRadioButtonId();
                    if (selAppointmentType == R.id.radio_button_add_inquiry_form_medicaid_yes) {
                        mMediclaim = mActivity.getResources().getString(R.string.text_yes);
                    } else {
                        mMediclaim = mActivity.getResources().getString(R.string.text_no);
                    }
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Sets up the data
     */
    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            // Set current date
            // mEditAdmissionDate.setText(Common.setCurrentDateTime(mActivity));

//            // Set Radio Button
//            mDiseaseExplained = mActivity.getResources().getString(R.string.text_yes);
//            mSurgeryExplained = mActivity.getResources().getString(R.string.text_yes);
            mMediclaim = mActivity.getResources().getString(R.string.text_yes);

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerEye.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerDoctor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerCouncillor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);


            String currentDate = Common.setCurrentDate(mActivity);
            String covertTreatmentDate = Common.convertDateUsingDateFormat(mActivity, mTreatmentDate,
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));

            if (currentDate.equalsIgnoreCase(covertTreatmentDate)) {
                mRelativeNoDataFound.setVisibility(View.GONE);
            } else {
                mRelativeNoDataFound.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method checks the validation first then call the API
     */
    private void doAddInquiryForm() {
        KeyboardUtility.HideKeyboard(mActivity, mEditRemarks);
        if (checkValidation()) {
            callToAddInquiryFormAPI();
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {
        boolean status = true;

        int index_eye = mSpinnerEye.getSelectedItemPosition();
        int index_surgery = mSpinnerSurgery.getSelectedItemPosition();
        int index_doctor = mSpinnerDoctor.getSelectedItemPosition();

        if (index_eye == 0) {
            Common.setCustomToast(mActivity, mActivity.getResources().getString(
                    R.string.error_select_eye));
            status = false;
            return status;
        }

        if (index_doctor == 0) {
            Common.setCustomToast(mActivity, mActivity.getResources().getString(
                    R.string.error_select_doctor));
            status = false;
            return status;
        }
        if (index_surgery == 0) {
            Common.setCustomToast(mActivity, mActivity.getResources().getString(
                    R.string.error_select_surgery));
            status = false;
            return status;
        }

        return status;
    }

    /**
     * This method should call for Councillor Add Inquiry Form API
     */
    private void callToAddInquiryFormAPI() {
        try {

            getServiceSelected();
            String twoSurgery="0";

            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            String mRemarks = mEditRemarks.getText().toString().trim();

            int index = mSpinnerSurgery.getSelectedItemPosition();
            String surgery = mArrSurgeryTypeList.get(index - 1).getSurgeryTypeID();

            int index_suregey_two = mSpinnerSurgeryTwo.getSelectedItemPosition() - 1;
            if (index_suregey_two == -1) {
                twoSurgery = "0";
            } else {
                twoSurgery = mArrSurgeryTypeList.get(index_suregey_two).getSurgeryName();
            }

            int index_option_preffeered = mSpinnerPatietPreferredOption.getSelectedItemPosition() - 1;
            if (index_option_preffeered == -1) {
                mOptionOpted = "0";
            } else {
                mOptionOpted = mArrOptionSuggestedList.get(index_option_preffeered).getOpetionSuggested();
            }


            String today = Common.setCurrentDate(mActivity);
            String councillor_date = Common.convertDateUsingDateFormat(mActivity,
                    today, mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                    mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));


            String admission_date = AppConstants.STR_EMPTY_STRING;
            String admission_time = AppConstants.STR_EMPTY_STRING;

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAddCouncillorFormJson(mPatientId, mCouncillorId, councillor_date,
                            mEyeId, surgery, twoSurgery, mDoctorId, mDiseaseExplained, mSurgeryExplained, mOptionSuggested, mOptionOpted, mMediclaim, admission_date,
                            admission_time, mRemarks, mUserId, mDatabaseName, AddNewPreliminaryExaminationFragment.PreliminaryExminationId));

            Call<AddPatientModel> call = RetrofitClient.createService(ApiInterface.class).addInquiryForm(requestBody);
            call.enqueue(new Callback<AddPatientModel>() {
                @Override
                public void onResponse(@NonNull Call<AddPatientModel> call, @NonNull Response<AddPatientModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                Common.setCustomToast(mActivity, mMessage);

                                if (mArrHistory.size() > 0) {
                                    mArrHistory.clear();
                                }
                                callToHistoryPreliminaryExamination();

//                                redirectedToCouncillorInquiryFormFragment();
                                AddNewPreliminaryExaminationFragment.changePage(9);
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddPatientModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * get Name Service Opted or Suggested
     */
    private void getServiceSelected() {

        mOptionSuggested = AppConstants.STR_EMPTY_STRING;
        if (mLinearSugestation.getChildCount() > 0) {
            for (int a = 0; a < mLinearSugestation.getChildCount(); a++) {
                View child_view = mLinearSugestation.getChildAt(a);
                TextView mTextName = child_view.findViewById
                        (R.id.text_row_add_preliminary_examination_compliance_compliance);

                String item = mTextName.getText().toString();
                if (mOptionSuggested.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                    mOptionSuggested = item;
                } else {
                    mOptionSuggested = mOptionSuggested + "," + item;
                }

            }
        }

    }

    /**
     * This method should call the Doctor API
     */
    private void callToDoctorAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setDoctorListJson(hospital_database));

            Call<DoctorModel> call = RetrofitClient.createService(ApiInterface.class).getDoctorList(body);
            call.enqueue(new Callback<DoctorModel>() {
                @Override
                public void onResponse(@NonNull Call<DoctorModel> call, @NonNull Response<DoctorModel> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrDoctorList.addAll(response.body().getData());
                            }
                            setsDoctorAdapter();
                        } else {
                            setsDoctorAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DoctorModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setsDoctorAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsDoctorAdapter() {
        try {
            mArrDoctor.add(0, mActivity.getResources().getString(R.string.spinner_select_doctor));
            if (mArrDoctorList.size() > 0) {
                for (DoctorModel doctorModel : mArrDoctorList) {
                    mArrDoctor.add(doctorModel.getDoctorName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerDoctor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrDoctor) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedDoctorIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerDoctor.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Preliminary Examination API
     */
    private void callToPreliminaryExaminationAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPreliminaryExaminationDetailsJson(hospital_database));

            Call<PreliminaryExaminationDetailsModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationDetails(body);
            call.enqueue(new Callback<PreliminaryExaminationDetailsModel>() {
                @Override
                public void onResponse(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Response<PreliminaryExaminationDetailsModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()
                                && response.body().getError() == 200) {
                            List<PreliminaryExaminationDetailsModel.Data.SurgeryType>
                                    mListSurgery = response.body().getData().getSurgeryType();

                            mArrSurgeryTypeList.addAll(mListSurgery);
                            setSurgeryTypeAdapter();

                            List<PreliminaryExaminationDetailsModel.Data.Eye>
                                    mListEye = response.body().getData().getEye();

                            mArrEyeList.addAll(mListEye);
                            setEyeSpinnerAdapter();

                            // Option List
                            List<PreliminaryExaminationDetailsModel.Data.OptionSuggested>
                                    mListOption = response.body().getData().getOptionSuggested();

                            mArrOptionSuggestedList.addAll(mListOption);
                            setOptionAdapter();

                            checkTodayPreliminaryExamination();
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Adapter for Option suggested or Patient Option
     */
    private void setOptionAdapter() {
        try {

            if (mArrPackageSuggested.size() > 0) {
                mArrPackageSuggested.clear();
            }

            CouselingPackageSuggestionModel options = new CouselingPackageSuggestionModel();
            options.setStatus("0");
            options.setOpetionSuggested(mActivity.getResources().getString(R.string.spinner_select_package_suggestion));
            options.setOpetionSuggestedID("0");
            options.setChecked(false);
            mArrPackageSuggested.add(options);


            if (mArrOptionSuggestedList.size() > 0) {
                for (int a = 0; a < mArrOptionSuggestedList.size(); a++) {
                    CouselingPackageSuggestionModel option = new CouselingPackageSuggestionModel();
                    option.setStatus(mArrOptionSuggestedList.get(a).getStatus());
                    option.setOpetionSuggested(mArrOptionSuggestedList.get(a).getOpetionSuggested());
                    option.setOpetionSuggestedID(mArrOptionSuggestedList.get(a).getOpetionSuggestedID());
                    option.setChecked(false);
                    mArrPackageSuggested.add(option);
                }
            }
            mAdapterPackageSuggesteed = new CouselingPackageOptionSugestionAdapter
                    (mActivity, mArrPackageSuggested, this);

            mSpinnerOptionSuggested.setAdapter(mAdapterPackageSuggesteed);


            if (mArrOptionOpted.size() > 0) {
                mArrOptionOpted.clear();
            }

            mArrOptionOpted.add(0, mActivity.getResources().getString(R.string.spinner_select_suggestion_opted));
            if (mArrOptionSuggestedList.size() > 0) {
                for (PreliminaryExaminationDetailsModel.Data.OptionSuggested optionModel : mArrOptionSuggestedList) {
                    mArrOptionOpted.add(optionModel.getOpetionSuggested());
                }
            }
            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerPatietPreferredOption.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrOptionOpted) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedPackagePreferedIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerPatietPreferredOption.setAdapter(adapter);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This is bind Eye Spinner Send
     */
    private void setEyeSpinnerAdapter() {
        try {
            mArrEye.add(0, mActivity.getResources().getString(R.string.spinner_select_eye));
            if (mArrEyeList.size() > 0) {
                for (PreliminaryExaminationDetailsModel.Data.Eye eyeModel : mArrEyeList) {
                    mArrEye.add(eyeModel.getEye());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerEye.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrEye) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedEyeIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerEye.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setSurgeryTypeAdapter() {
        try {
            if (mArrSurgeryTypeList != null &&
                    mArrSurgeryTypeList.size() > 0) {

                mArrSurgeryType.add(mActivity.getResources().getString(R.string.spinner_select_surgery));

                for (int i = 0; i < mArrSurgeryTypeList.size(); i++) {
                    mArrSurgeryType.add(mArrSurgeryTypeList.get(i).getSurgeryName());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>
                        (mActivity, android.R.layout.simple_selectable_list_item, mArrSurgeryType);
                mSpinnerSurgery.setAdapter(adapter);

                ArrayAdapter<String> adapter_two = new ArrayAdapter<String>
                        (mActivity, android.R.layout.simple_selectable_list_item, mArrSurgeryType);
                mSpinnerSurgeryTwo.setAdapter(adapter_two);
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Sets up the data to the councillor spinner
     */
    private void callToCouncillorAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setCouncillorListJson(hospital_database));

            Call<CouncillorModel> call = RetrofitClient.createService(ApiInterface.class).getCouncillorList(body);
            call.enqueue(new Callback<CouncillorModel>() {
                @Override
                public void onResponse(@NonNull Call<CouncillorModel> call, @NonNull Response<CouncillorModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrCouncillorList.addAll(response.body().getData());
                            }
                            setsCouncillorAdapter();
                        } else {
                            setsCouncillorAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CouncillorModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setsCouncillorAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsCouncillorAdapter() {
        try {
            mArrCouncillor.add(0, getString(R.string.spinner_select_councillor));
            if (mArrCouncillorList.size() > 0) {
                for (CouncillorModel councillorModel : mArrCouncillorList) {
                    mArrCouncillor.add(councillorModel.getCouncillorName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            //mSpinnerCouncillor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_dropdown_item, mArrCouncillor) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCouncillorIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerCouncillor.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Call To Next Screen
     */
    private void callToNextPage() {
        AddNewPreliminaryExaminationFragment.changePage(8);
    }


    /**
     * Set Call API History Preliminary Examination
     */
    private void callToHistoryPreliminaryExamination() {
        try {

            String defualt_date = "1000-01-01";
            String Mobile = AppConstants.STR_EMPTY_STRING;
            String Type = "CounselingDetails";
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setHistoryPreliminaryExaminationJson(defualt_date, mCurrentPage, mPatientId, Mobile, mHistoryDoctorID, Type, hospital_database));

            Call<HistoryPreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationHistory(body);
            call.enqueue(new Callback<HistoryPreliminaryExaminationModel>() {
                @Override
                public void onResponse(Call<HistoryPreliminaryExaminationModel> call, Response<HistoryPreliminaryExaminationModel> response) {
                    Common.insertLog("Response History::::> " + new Gson().toJson(response.body()));
                    try {
                        if (response.body() != null &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body().getData().size() > 0) {
                                mArrHistory.addAll(response.body().getData());
                                setTodayCounselingData(response.body().getData());
                            }
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<HistoryPreliminaryExaminationModel> call, Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Today Counselling Data
     *
     * @param data
     */
    private void setTodayCounselingData(List<HistoryPreliminaryExaminationModel.Data> data) {
        try {
            String currentDate = Common.setCurrentDate(mActivity);
            String convert_current_date = Common.setConvertDateFormat(mActivity, currentDate,
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                    mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));

            if (data.size() > 0) {
                for (int i = 0; i < data.size(); i++) {
                    if (data.get(i).getTreatmentDate().equalsIgnoreCase(convert_current_date)) {
                        if (data.get(i).getCounselingDetails() != null &&
                                data.get(i).getCounselingDetails().size() > 0) {
                            for (int a = 0; a < data.get(i).getCounselingDetails().size(); a++) {
                                EditCouncillor(data.get(i).getCounselingDetails().get(a));
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     *OpenDialog for closing patient file
     */
    public void openFileClosingAlert() {


        AlertDialog.Builder builder = new AlertDialog.Builder(
                mActivity);
        builder.setTitle("Close File");
        builder.setMessage("Are you sure you want to close file?");
        builder.setNegativeButton("NO",
                (dialog, which) -> dialog.dismiss());
        builder.setPositiveButton("YES",
                (dialog, which) -> closeFile());
        builder.show();

    }

    private void closeFile() {
        try {
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.closeFileJson(mAppointmentId, mDatabaseName));

            Call<CloseFileModel> call = RetrofitClient.createService(ApiInterface.class).closeFileAPI(body);
            call.enqueue(new Callback<CloseFileModel>() {
                @Override
                public void onResponse(@NonNull Call<CloseFileModel> call, @NonNull Response<CloseFileModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        int mError = response.body().getError();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                Common.insertLog("IF");
                                Common.setCustomToast(mActivity,mMessage);
                            } else {
                                Common.insertLog("ELSE");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CloseFileModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open Dialog Treatment Suggested
     */
    public void openDialogTreatmentSuggested() {
        try {
            Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.custom_dialog_history_preliminary);

            TextView mTextTitle = dialog.findViewById(R.id.text_view_custom_dialog_history_preliminary);
            RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
            ImageView mImageClose = dialog.findViewById(R.id.image_custom_dialog_history_preliminary_close);

            RelativeLayout noInternetConnection = dialog.findViewById(R.id.relative_no_internet);
            RelativeLayout noDataFound = dialog.findViewById(R.id.relative_no_data_available);

            mTextTitle.setText(mActivity.getResources().getString(R.string.nav_menu_counselling));

            mImageClose.setColorFilter(Common.setThemeColor(mActivity));

            mImageClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            LinearLayoutManager manager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(manager);

            HistoryTreatmentSuggestedAdapter adapter =
                    new HistoryTreatmentSuggestedAdapter(mActivity, recyclerView, mArrHistory, this) {
                        @Override
                        protected void onSelectedItem(Activity mActivity, HistoryPreliminaryExaminationModel.Data checkInData) {
                            super.onSelectedItem(mActivity, checkInData);
                            dialog.dismiss();
                            mRelativeNoDataFound.setVisibility(View.GONE);
                            if (checkInData.getCounselingDetails() != null &&
                                    checkInData.getCounselingDetails().size() > 0) {
                                for (int a = 0; a < checkInData.getCounselingDetails().size(); a++) {
                                    EditCouncillor(checkInData.getCounselingDetails().get(a));
                                }
                            }
                        }
                    };
            recyclerView.setAdapter(adapter);

            if (mArrHistory.size() > 0) {
                noDataFound.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            } else {
                noDataFound.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Interdface Call back Interface
     *
     * @param mActivity   -
     * @param checkInData - Prelimimnary Data
     */
    @Override
    public void onSelectHistioyDiagnosis(Activity mActivity, HistoryPreliminaryExaminationModel.Data checkInData) {

        mRelativeNoDataFound.setVisibility(View.GONE);
        if (checkInData.getCounselingDetails() != null &&
                checkInData.getCounselingDetails().size() > 0) {

            for (int a = 0; a < checkInData.getCounselingDetails().size(); a++) {
                EditCouncillor(checkInData.getCounselingDetails().get(a));
            }
        }
    }

    /**
     * Set Edit Councilor Detail
     *
     * @param counselingDetail - Councilor Detail
     */
    private void EditCouncillor(HistoryPreliminaryExaminationModel.CounselingDetail counselingDetail) {
        try {

            if (mArrEyeList.size() > 0) {
                for (int a = 0; a < mArrEyeList.size(); a++) {
                    if (mArrEyeList.get(a).getEyeID().equals(counselingDetail.getEyeID())) {
                        int index = a + 1;
                        mSpinnerEye.setSelection(index, true);
                    }
                }
            }

            //Set Surgery
            if (mArrSurgeryTypeList.size() > 0) {
                for (int a = 0; a < mArrSurgeryTypeList.size(); a++) {
                    if (mArrSurgeryTypeList.get(a).getSurgeryName().equals(counselingDetail.getCouncillorSurgeryName())) {
                        int index = a + 1;
                        mSpinnerSurgery.setSelection(index, true);
                    }
                }
            }

            //Set Surgery
            if (mArrSurgeryTypeList.size() > 0) {
                for (int a = 0; a < mArrSurgeryTypeList.size(); a++) {
                    if (mArrSurgeryTypeList.get(a).getSurgeryName().equals(counselingDetail.getSurgeryType2())) {
                        int index = a + 1;
                        mSpinnerSurgeryTwo.setSelection(index, true);
                    }
                }
            }


            if (mArrDoctorList.size() > 0) {
                for (int a = 0; a < mArrDoctorList.size(); a++) {
                    if (mArrDoctorList.get(a).getDoctorID().equals(counselingDetail.getDoctorID())) {
                        int index = a + 1;
                        mSpinnerDoctor.setSelection(index, true);
                    }
                }
            }


            if (mArrCouncillorList.size() > 0) {
                for (int a = 0; a < mArrCouncillorList.size(); a++) {
                    if (mArrCouncillorList.get(a).getCouncillorID().equals(counselingDetail.getCouncillorID())) {
                        int index = a + 1;
                        mSpinnerCouncillor.setSelection(index, true);
                    }
                }
            }


            String package_suggested = counselingDetail.getOpetionSuggested();
            String[] arr_suggested = package_suggested.split(",");
            for (int s = 0; s < arr_suggested.length; s++) {

                String name = arr_suggested[s];
                for (int a = 0; a < mArrPackageSuggested.size(); a++) {
                    if (mArrPackageSuggested.get(a).getOpetionSuggested()
                            .equals(name)) {
                        mArrPackageSuggested.get(a).setChecked(true);
//                        addItems(mArrPackageSuggested.get(a));
                    }
                }
            }

            String package_opted = counselingDetail.getPatientPreferredOption();
            if (mArrPackageSuggested.size() > 0) {
                for (int o = 0; o < mArrPackageSuggested.size(); o++) {
                    if (mArrPackageSuggested.get(o).getOpetionSuggested()
                            .equalsIgnoreCase(package_opted)) {
                        mSpinnerPatietPreferredOption.setSelection(o);
                    }
                }
            }


            if (counselingDetail.getDiseaseExplained().equals(mActivity.getString(R.string.text_yes))) {
                mRadioGroupDiseaseExplained.check(R.id.radio_button_check_up_counseling_disease_explained_yes);
            } else {
                mRadioGroupDiseaseExplained.check(R.id.radio_button_check_up_counseling_disease_explained_no);
            }

            if (counselingDetail.getSurgeryExplained().equals(mActivity.getString(R.string.text_yes))) {
                mRadioSurgeryExplained.check(R.id.radio_button_check_up_counseling_surgery_explained_yes);
            } else {
                mRadioSurgeryExplained.check(R.id.radio_button_check_up_counseling_surgery_explained_no);
            }

            if (counselingDetail.getMeclaim().equals(mActivity.getString(R.string.text_yes))) {
                mRadioGroupMedicaid.check(R.id.radio_button_check_up_counseling_mediclaim_yes);
            } else {
                mRadioGroupMedicaid.check(R.id.radio_button_check_up_counseling_mediclaim_no);
            }

            mEditRemarks.setText(counselingDetail.getCouncillorRemarks());


        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Check day Preliminary Examination
     */
    private void checkTodayPreliminaryExamination() {
        String currentDate = Common.setCurrentDate(mActivity);
        String covertTreatmentDate = Common.convertDateUsingDateFormat(mActivity, mTreatmentDate,
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen),
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));

        String covertCurrentDate = Common.convertDateUsingDateFormat(mActivity, currentDate,
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen));

        if (!currentDate.equals(covertTreatmentDate)) {
            // mRelativeDataNotFound.setVisibility(View.GONE);
        } else {
            // mRelativeDataNotFound.setVisibility(View.GONE);

            if (mArrHistory.size() > 0) {
                for (int a = 0; a < mArrHistory.size(); a++) {

                    if (mArrHistory.get(a).getTreatmentDate().equals(covertCurrentDate)) {
                        if (mArrHistory.get(a).getAppointmentID().equals(String.valueOf(mAppointmentId))) {
                            if (mArrHistory.get(a).getCounselingDetails() != null &&
                                    mArrHistory.get(a).getCounselingDetails().size() > 0) {
                                for (int i = 0; i < mArrHistory.get(a).getCounselingDetails().size(); i++) {
                                    EditCouncillor(mArrHistory.get(a).getCounselingDetails().get(i));
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Spinner Item Updates
     */
    @Override
    public void onItemUpdate() {

        if (mLinearSugestation.getChildCount() > 0) {
            mLinearSugestation.removeAllViews();
        }

        if (mArrPackageSuggested.size() > 0) {

            for (int a = 0; a < mArrPackageSuggested.size(); a++) {
                if (mArrPackageSuggested.get(a).getChecked() == true) {
                    addItems(mArrPackageSuggested.get(a));
                }
            }
        }
    }

    /**
     * Add Item Linear
     *
     * @param couselingPackageSuggestionModel -Package Suggested
     */
    private void addItems(CouselingPackageSuggestionModel couselingPackageSuggestionModel) {
        @SuppressLint("InflateParams")
        View view_container = LayoutInflater.from(mActivity).inflate(R.layout.
                        row_add_preliminary_examination_compliance_item,
                null, false);

        TextView mTextName = view_container.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
        ImageView mImageDelete = view_container.findViewById(R.id.image_row_add_preliminary_examination_compliance_delete);
        mImageDelete.setColorFilter(Common.setThemeColor(mActivity));

        mTextName.setText(couselingPackageSuggestionModel.getOpetionSuggested());

        mImageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLinearSugestation.removeView(view_container);
                couselingPackageSuggestionModel.setChecked(false);
            }
        });

        mLinearSugestation.addView(view_container);
    }
}
