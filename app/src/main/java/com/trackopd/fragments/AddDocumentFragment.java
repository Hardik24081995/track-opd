package com.trackopd.fragments;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.trackopd.R;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppPermissions;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.CameraGalleryPermission;
import com.trackopd.utils.Common;
import com.trackopd.utils.Compressor;
import com.trackopd.utils.FilePath;
import com.trackopd.utils.KeyboardUtility;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddDocumentFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private EditText mEditDocumentTitle;
    private TextView mTextImageName, mTextName, mTextDate;
    private ImageView mImageAdd, mImageDownload, mImageEdit, mImageDelet, mImageAttachType;
    private Button mButtonUpload;
    private String chosenTask = "", MimeType, FileName, mDocumentName, mDocumentId, mAppointmentId;
    private final int REQUEST_CODE_DOC = 101;
    private final int REQUEST_CAMERA = 0;
    private File selectedFile, destination;
    private Bitmap mBitmapDocument;
    private ViewGroup mGroupRevealAnimation;
    private LinearLayout mLinearDocumentDetail;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_add_document, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mGroupRevealAnimation = container;
        mView.post(Common.setAnimation(mActivity, mView, mGroupRevealAnimation));

        getBundle();
        getIds();
        setRegListeners();
        setData();
        setHasOptionsMenu(true);
        return mView;
    }


    private void getBundle() {

        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mDocumentName = bundle.getString(AppConstants.BUNDLE_PIC_NAME);
                mDocumentId = bundle.getString(AppConstants.BUNDLE_DOCUMENT_ID);
                mAppointmentId = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Edit Texts
            mEditDocumentTitle = mView.findViewById(R.id.edit_document_title);

            // Image Views
            mImageAdd = mView.findViewById(R.id.image_select_document);
            mImageDelet = mView.findViewById(R.id.image_add_document_delete);
            mImageEdit = mView.findViewById(R.id.image_add_document_edit);
            mImageDownload = mView.findViewById(R.id.image_add_document_download);
            mImageAttachType = mView.findViewById(R.id.image_add_document_attachment_type);

            // Text Views
            mTextImageName = mView.findViewById(R.id.text_document_name);
            mTextName = mView.findViewById(R.id.text_add_document_name);
            mTextDate = mView.findViewById(R.id.text_add_document_date);

            // Linear Layout
            mLinearDocumentDetail = mView.findViewById(R.id.linear_add_document_details);

            // Buttons
            mButtonUpload = mView.findViewById(R.id.button_add_document);

            // Set Request Focus
            mEditDocumentTitle.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            //TODO: Button Click Listeners
            mButtonUpload.setOnClickListener(clickListener);

            //TODO: Image View Click Listeners
            mImageAdd.setOnClickListener(clickListener);
            mImageDownload.setOnClickListener(clickListener);
            mImageEdit.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_select_document:
                    selectDocument();
                    break;
                case R.id.button_add_document:
                    doUploadDocument();
                    break;
                case R.id.image_add_document_download:
                    downloadDocument();
                    break;
                case R.id.image_add_document_edit:
                    selectDocument();
                    break;
            }
        }
    };


    /**
     * Set Data on Text View
     */
    private void setData() {
        try {

            if (mDocumentName != null && mDocumentId != null) {
                mLinearDocumentDetail.setVisibility(View.VISIBLE);

                mTextName.setText(mDocumentName);
                mTextDate.setText(AppConstants.STR_EMPTY_SPACE);
                mEditDocumentTitle.setText(mDocumentName);

                //Set Document Name and its Type
                if (!TextUtils.isEmpty(mDocumentName)) {
                    // String FileName = EditDocument.getDocumentUrl().substring(EditDocument.getDocumentUrl().lastIndexOf("/") + 1);
                    String FileType = mDocumentName.split("\\.")[1];
                    if (FileType != null) {
                        if (FileType.contains("pdf"))
                            mImageAttachType.setImageResource(R.drawable.ic_pdf);
                        else if (FileType.contains("doc") || FileType.contains("docx"))
                            mImageAttachType.setImageResource(R.drawable.ic_doc);
                        else
                            mImageAttachType.setImageResource(R.drawable.ic_image);
                    }
                }
            } else {
                mLinearDocumentDetail.setVisibility(View.GONE);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * This method checks the validation first then call the API
     */
    private void doUploadDocument() {
        KeyboardUtility.HideKeyboard(mActivity, mEditDocumentTitle);
        if (checkValidation()) {

        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {
        boolean status = true;

        String mTitle = mEditDocumentTitle.getText().toString().trim();
        mEditDocumentTitle.setError(null);

        if (TextUtils.isEmpty(mTitle)) {
            mEditDocumentTitle.setError(getResources().getString(R.string.error_field_required));
            status = false;
        }

        return status;
    }

    /**
     * This method should upload the document
     */
    private void selectDocument() {
        final CharSequence[] menus = {
                mActivity.getResources().getString(R.string.action_capture_image),
                mActivity.getResources().getString(R.string.action_select_from_file),
                mActivity.getResources().getString(R.string.action_cancel)
        };

        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        builder.setTitle(mActivity.getResources().getString(R.string.header_choose_image));
        builder.setItems(menus, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int pos) {

                boolean result = CameraGalleryPermission.checkPermission(mActivity,
                        AppPermissions.ReadWriteExternalStorageRequiredPermission());

                if (menus[pos].equals(mActivity.getResources().getString(R.string.action_capture_image))) {
                    chosenTask = mActivity.getResources().getString(R.string.action_capture_image);
                    if (result) {
                        openCamera();
                    }
                } else if (menus[pos].equals(mActivity.getResources().getString(R.string.action_select_from_file))) {
                    chosenTask = mActivity.getResources().getString(R.string.action_select_from_file);
                    if (result) {
                        getFile();
                    }
                } else if (menus[pos].equals(mActivity.getResources().getString(R.string.action_cancel))) {
                    chosenTask = AppConstants.STR_EMPTY_STRING;
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**
     * Open the camera intent
     */
    private void openCamera() {
        destination = AppUtil.currentTimeStampFile();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
        } else {
            File file = new File(Uri.fromFile(destination).getPath());
            Uri photoUri = FileProvider.getUriForFile(mActivity, mActivity.getPackageName() + AppConstants.STR_PROVIDER, file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        }

        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (intent.resolveActivity(mActivity.getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    /**
     * Get file for upload PDF
     */
    private void getFile() {
        String[] mimeTypes = {"application/pdf", "image/*"};
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        startActivityForResult(intent, REQUEST_CODE_DOC);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[]
            grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean isDownload = false;
        switch (requestCode) {
            case CameraGalleryPermission.INTERNAL_EXTERNAL_PERMISSION:
                Map<String, Integer> perms = new HashMap<>();
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);

                if (isDownload && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager
                        .PERMISSION_GRANTED) {
//                    downloadDocument(EditDocument.getDocumentUrl());
                } else {
                    if (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        if (chosenTask.equals(mActivity.getResources().getString(R.string.action_capture_image)))
                            openCamera();
                        else if (chosenTask.equals(mActivity.getResources().getString(R.string.action_select_from_file)))
                            getFile();
                    } else {
                        Toast.makeText(mActivity, mActivity.getResources().getString(R.string.error_permission_denied), Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {

            switch (requestCode) {
                case REQUEST_CODE_DOC:
                    onFileResult(data);
                    break;

                case REQUEST_CAMERA:
                    onCaptureImageResult();
                    break;
            }
        }
    }

    /**
     * Sets up the file chosen from the file picker and sets the text view
     */
    private void onFileResult(Intent data) {
        try {
            mTextImageName.setText(AppConstants.STR_EMPTY_STRING);
            selectedFile = null;
            Uri filePath = data.getData();

            if (filePath != null) {
                MimeType = FilePath.getMimeType(mActivity, filePath);
            }

            String file = FilePath.getPath(mActivity, filePath);
            if (file != null) {
                FileName = file.substring(file.lastIndexOf(AppConstants.STR_FORWARD_SLASH) + 1);
            }

            String[] mimeTypeArray = FileName.split("\\.");
            if (mimeTypeArray.length > 1) {

                if (MimeType != null && (MimeType.contains(AppConstants.STR_PDF) || MimeType.contains(AppConstants.STR_IMAGE))) {
                    if (!MimeType.contains(AppConstants.STR_IMAGE)) {
                        MimeType = mimeTypeArray[1];
                        mTextImageName.setText(FileName);
                        selectedFile = new File(file);

                        if (selectedFile.length() > 5000000) {
                            selectedFile = null;
                            AppUtil.displaySnackBarWithMessage(mView.findViewById(android.R.id.content),
                                    mActivity.getResources().getString(R.string.error_select_max_file_size));
                        }
                    } else {
                        MimeType = mimeTypeArray[1];
                        mTextImageName.setText(FileName);
                        Compressor compressor = new Compressor(mActivity);
                        selectedFile = compressor.compressToFile(new File(file));
                    }
                } else {
                    AppUtil.displaySnackBarWithMessage(mView.findViewById(android.R.id.content),
                            mActivity.getResources().getString(R.string.error_select_pdf_file_format));
                }
            } else {
                AppUtil.displaySnackBarWithMessage(mView.findViewById(android.R.id.content),
                        mActivity.getResources().getString(R.string.error_check_file_format));
            }
        } catch (Exception e) {
            AppUtil.displaySnackBarWithMessage(mView.findViewById(android.R.id.content),
                    mActivity.getResources().getString(R.string.error_no_file_fetched));
        }
    }

    /**
     * Sets up the captured image and set the text view
     */
    private void onCaptureImageResult() {
        mTextImageName.setText(AppConstants.STR_EMPTY_STRING);
        selectedFile = null;
        Bitmap bit;
        File file;
        Compressor compressor = new Compressor(mActivity);
        try {
            bit = compressor.compressToBitmap(destination);
            file = compressor.compressToFile(destination);
            if (bit != null) {
                selectedFile = file;
                FileName = mActivity.getResources().getString(R.string.text_captured_image_name);
                MimeType = AppConstants.STR_DOC_TYPE_JPG;
                mTextImageName.setText(FileName);
                mBitmapDocument = bit;
            } else
                AppUtil.displaySnackBarWithMessage(mView.findViewById(android.R.id.content),
                        mActivity.getResources().getString(R.string.error_try_later));
        } catch (IOException e) {
            e.printStackTrace();
            AppUtil.displaySnackBarWithMessage(mView.findViewById(android.R.id.content),
                    mActivity.getResources().getString(R.string.error_try_later));
        }
    }

    /**
     * Set Up Download Document
     */
    private void downloadDocument() {
        try {
            if (checkPermission()) {
                File mDownloadedFile = Common.checkIsFileDownloaded(mActivity, mActivity.getResources().getString(R.string.nav_menu_document), mDocumentName);
                if (!(mDownloadedFile.exists())) {
                    callToDownloadDocument();
                } else {
                    Common.openDocument(mActivity, mDownloadedFile, mDocumentName);
                }
            } else {
                requestPermission();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the prescription API to download prescriptions
     */
    private void callToDownloadDocument() {
        try {
            String mUrl = WebFields.API_BASE_URL + WebFields.DOC_URL + mDocumentName;

            Call<ResponseBody> call = RetrofitClient.createService(ApiInterface.class).downloadFile(mUrl);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    try {
                        if (response.isSuccessful()) {
                            boolean isDownloaded = Common.saveFileToStorage(mActivity,mActivity.getResources().getString(R.string.nav_menu_document), response.body(), mDocumentName);

                            // ToDo: Checks if document is downloaded or not, if downloaded then it will open the document
                            File mDownloadedFile = Common.checkIsFileDownloaded(mActivity, mActivity.getResources().getString(R.string.nav_menu_document), mDocumentName);
                            Common.openDocument(mActivity, mDownloadedFile, mDocumentName);
                            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.val_document_downloaded_successfully));
                            Common.insertLog("Prescription downloaded::::> " + isDownloaded);

                            // ToDo: Bind the notification
                            /*File mFile = new File(Environment.getExternalStorageDirectory() + "/"
                                    + Common.getFileSdcardPath(mActivity, mActivity.getResources().getString(R.string.nav_menu_prescriptions)));
                            if (!mFile.exists()) {
                                mFile.mkdirs();
                            }

                            File file = new File(mFile, patientPrescriptionModel.getDocument());
                            String mFilePath = file.toString();
                            Common.bindNotification(mActivity, mFilePath, file, patientPrescriptionModel.getDocument());*/
                        } else {
                            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.error_server));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should check the custom permissions
     */
    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(mActivity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * This method should request the custom permissions
     */
    private void requestPermission() {
        try {
            int PERMISSION_REQUEST_CODE = 1;
            ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
