package com.trackopd.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.trackopd.R;
import com.trackopd.adapter.HistoryPrintAdapter;
import com.trackopd.model.CloseFileModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.PrintReceiptModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.SessionManager;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SurgeryPrintFragment extends Fragment {


    private View mView;
    private Activity mActivity;
    private CheckBox mCheckboxHistoryAndComplaints, mCheckboxPrimaryExamination, mCheckboxDiagnosis, mCheckboxInvestigationSuggested,
            mCheckboxVisionUCVA, mCheckboxVisionFinal, mCheckboxVisionBCVAUndilated, mCheckboxVisionBCVADilated,
            mCheckboxTreatmentSuggested, mCheckboxCounselingDetails, mCheckboxSurgeryDetails, mCheckboxPrescription, mCheckboxPayment, mCheckboxDischarge;
    private boolean mHistoryAndComplaints = false, mVisionUCVA = false, mVisionBCVAUndilated = false, mVisionFinal = false,
            mVisionBCVADilated = false, mPrimaryExamination = false, mDiagnosis = false, mInvestigationSuggested = false,
            mTreatmentSuggested = false, mCounselingDetails = false, mSurgeryDetails = false, mPrescription = false, mPayment = false, mDischarge = false;

    private Button mButtonPrint;
    private String mPatientId, mDoctorId, mTreatmentDate, mFirstName, mPatientCode, mMobileNo, mAppointmentNo, mAppointmentDate;
    private int mAppointmentId, mCurrentPage = 1, mHistoryDoctorID = -1;

    private ArrayList<HistoryPreliminaryExaminationModel.Data> mArrHistory;
    private CheckBox.OnCheckedChangeListener checkBoxListener = new CheckBox.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()) {
                case R.id.check_box_surgery_print_history_and_complaint:
                    mHistoryAndComplaints = isChecked;
                    break;

                /*case R.id.check_box_surgery_print_vision:
                    if (isChecked) {
                        mCheckboxVision.setChecked(true);
                        mCheckboxVisionUCVA.setChecked(true);
                        mCheckboxVisionFinal.setChecked(true);
                        mCheckboxVisionBCVAUndilated.setChecked(true);
                        mCheckboxVisionBCVADilated.setChecked(true);

                        mVisionUCVA = true;
                        mVisionFinal = true;
                        mVisionBCVAUndilated = true;
                        mVisionBCVADilated = true;
                    } else {
                        mCheckboxVision.setChecked(false);
                        mCheckboxVisionUCVA.setChecked(false);
                        mCheckboxVisionFinal.setChecked(false);
                        mCheckboxVisionBCVAUndilated.setChecked(false);
                        mCheckboxVisionBCVADilated.setChecked(false);

                        mVisionUCVA = false;
                        mVisionFinal = false;
                        mVisionBCVAUndilated = false;
                        mVisionBCVADilated = false;
                    }
                    break;*/

                case R.id.check_box_surgery_print_vision_ucva:
                    mVisionUCVA = isChecked;
                    break;

                case R.id.check_box_surgery_print_vision_final:
                    mVisionFinal = isChecked;
                    break;

                case R.id.check_box_surgery_print_vision_bcva_undilated:
                    mVisionBCVAUndilated = isChecked;
                    break;

                case R.id.check_box_surgery_print_vision_bcva_dilated:
                    mVisionBCVADilated = isChecked;
                    break;

                case R.id.check_box_surgery_print_primary_examination:
                    mPrimaryExamination = isChecked;
                    break;

                case R.id.check_box_surgery_print_diagnosis:
                    mDiagnosis = isChecked;
                    break;

                case R.id.check_box_surgery_print_investigation_suggested:
                    mInvestigationSuggested = isChecked;
                    break;

                case R.id.check_box_surgery_print_treatment_suggested:
                    mTreatmentSuggested = isChecked;
                    break;

                case R.id.check_box_surgery_print_counseling_details:
                    mCounselingDetails = isChecked;
                    break;

                case R.id.check_box_surgery_print_surgery_details:
                    mSurgeryDetails = isChecked;
                    break;

                case R.id.check_box_surgery_print_prescription:
                    mPrescription = isChecked;
                    break;

                case R.id.check_box_surgery_print_payment:
                    mPayment = isChecked;
                    break;
                case R.id.check_box_surgery_print_discharge:
                    mDischarge = isChecked;
                    break;
            }
        }
    };
    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_surgery_print_submit:

                    if (checkValidation()) {
                        if (mPrescription || mDischarge) {
                            callAlert();
                        } else {
                            String lang = "";
                            callToPrintPdf(lang);
                        }
                    }
                    break;
            }
        }
    };


    public SurgeryPrintFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_surgery_print, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        mArrHistory = new ArrayList<>();

        getBundle();
        getIds();
        setRegListeners();
        setData();
        callToHistoryPreliminaryExamination();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mAppointmentId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mDoctorId = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);
                mTreatmentDate = bundle.getString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * get Declare Ids
     */
    private void getIds() {
        try {
            // Check Boxes
            mCheckboxHistoryAndComplaints = mView.findViewById(R.id.check_box_surgery_print_history_and_complaint);
            mCheckboxVisionUCVA = mView.findViewById(R.id.check_box_surgery_print_vision_ucva);
            mCheckboxVisionFinal = mView.findViewById(R.id.check_box_surgery_print_vision_final);
            mCheckboxVisionBCVAUndilated = mView.findViewById(R.id.check_box_surgery_print_vision_bcva_undilated);
            mCheckboxVisionBCVADilated = mView.findViewById(R.id.check_box_surgery_print_vision_bcva_dilated);
            mCheckboxPrimaryExamination = mView.findViewById(R.id.check_box_surgery_print_primary_examination);
            mCheckboxDiagnosis = mView.findViewById(R.id.check_box_surgery_print_diagnosis);
            mCheckboxInvestigationSuggested = mView.findViewById(R.id.check_box_surgery_print_investigation_suggested);
            mCheckboxTreatmentSuggested = mView.findViewById(R.id.check_box_surgery_print_treatment_suggested);
            mCheckboxCounselingDetails = mView.findViewById(R.id.check_box_surgery_print_counseling_details);
            mCheckboxSurgeryDetails = mView.findViewById(R.id.check_box_surgery_print_surgery_details);
            mCheckboxPrescription = mView.findViewById(R.id.check_box_surgery_print_prescription);
            mCheckboxPayment = mView.findViewById(R.id.check_box_surgery_print_payment);
            mCheckboxDischarge = mView.findViewById(R.id.check_box_surgery_print_discharge);

            mButtonPrint = mView.findViewById(R.id.button_surgery_print_submit);


        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Register Listener
     */
    private void setRegListeners() {
        try {
            mCheckboxHistoryAndComplaints.setOnCheckedChangeListener(checkBoxListener);
            mCheckboxVisionUCVA.setOnCheckedChangeListener(checkBoxListener);
            mCheckboxVisionFinal.setOnCheckedChangeListener(checkBoxListener);
            mCheckboxVisionBCVAUndilated.setOnCheckedChangeListener(checkBoxListener);
            mCheckboxVisionBCVADilated.setOnCheckedChangeListener(checkBoxListener);
            mCheckboxPrimaryExamination.setOnCheckedChangeListener(checkBoxListener);
            mCheckboxDiagnosis.setOnCheckedChangeListener(checkBoxListener);
            mCheckboxInvestigationSuggested.setOnCheckedChangeListener(checkBoxListener);
            mCheckboxTreatmentSuggested.setOnCheckedChangeListener(checkBoxListener);
            mCheckboxCounselingDetails.setOnCheckedChangeListener(checkBoxListener);
            mCheckboxSurgeryDetails.setOnCheckedChangeListener(checkBoxListener);
            mCheckboxPrescription.setOnCheckedChangeListener(checkBoxListener);
            mCheckboxPayment.setOnCheckedChangeListener(checkBoxListener);
            mCheckboxDischarge.setOnCheckedChangeListener(checkBoxListener);

            SessionManager manager = new SessionManager(mActivity);
            String closeFile = manager.getPreferences("CloseFiles", "");

            if (closeFile.equalsIgnoreCase("Yes")) {
                mButtonPrint.setClickable(false);
                Toast.makeText(mActivity, "Patient File Closed", Toast.LENGTH_SHORT).show();
            } else {
                mButtonPrint.setOnClickListener(clickListener);
            }

            //TODO: Set On Click Listener


        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    private void callAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                mActivity);
        builder.setTitle("Language Options");
        builder.setMessage("Choose your desired language");
        builder.setNegativeButton("Hindi",
                (dialog, which) -> {
                    String lang = "Hindi";
                    callToPrintPdf(lang);
                });
        builder.setPositiveButton("Gujarati",
                (dialog, which) -> {
                    String lang = "Gujarati";
                    callToPrintPdf(lang);
                });
        builder.show();
    }

    /**
     * Check Validation Data
     *
     * @return
     */
    private boolean checkValidation() {
        boolean status = true;

        if (!mHistoryAndComplaints &&
                !mVisionUCVA && !mVisionFinal &&
                !mVisionBCVAUndilated && !mVisionBCVADilated
                && !mPrimaryExamination && !mDiagnosis
                && !mInvestigationSuggested && !mTreatmentSuggested
                && !mCounselingDetails && !mSurgeryDetails
                && !mPrescription &&
                !mPayment && !mDischarge) {

            Common.setCustomToast(mActivity, mActivity.getString(R.string.error_select_print));
            status = false;
        }
        return status;
    }

    /**
     * Call To Print API Preliminary Examination
     */
    private void callToPrintPdf(String lang) {
        try {


            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPrintReceiptJson(AddNewPreliminaryExaminationFragment.PreliminaryExminationId,
                            String.valueOf(mAppointmentId), mHistoryAndComplaints, mVisionUCVA, mVisionBCVAUndilated,
                            mVisionFinal, mVisionBCVADilated, mPrimaryExamination, mDiagnosis, mInvestigationSuggested,
                            mTreatmentSuggested, mCounselingDetails, mSurgeryDetails, mPrescription,lang, mDischarge, mPayment, hospital_database));

            Call<PrintReceiptModel> call = RetrofitClient.createService(ApiInterface.class).PrintReceipt(body);
            call.enqueue(new Callback<PrintReceiptModel>() {
                @Override
                public void onResponse(@NonNull Call<PrintReceiptModel> call, @NonNull Response<PrintReceiptModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body() != null) {
                                String url = response.body().getData();
                                callToReceiptPrint(url);
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PrintReceiptModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Call To Receipt Print
     *
     * @param url - String Url Print
     */
    private void callToReceiptPrint(String url) {
        /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);*/

        //  String pdfOpen = AppConstants.PDF_OPEN + url;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mActivity.startActivity(browserIntent);
    }


    /**
     * Set Call API History Preliminary Examination
     */
    private void callToHistoryPreliminaryExamination() {
        try {

            String defualt_date = "1000-01-01";
            String Mobile = AppConstants.STR_EMPTY_STRING;
            String Type = "all";
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setHistoryPreliminaryExaminationJson(defualt_date, mCurrentPage, mPatientId, Mobile, mHistoryDoctorID, Type, hospital_database));

            Call<HistoryPreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationHistory(body);
            call.enqueue(new Callback<HistoryPreliminaryExaminationModel>() {
                @Override
                public void onResponse(Call<HistoryPreliminaryExaminationModel> call, Response<HistoryPreliminaryExaminationModel> response) {
                    Common.insertLog("Response History::::> " + new Gson().toJson(response.body()));
                    try {
                        if (response.body() != null &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body().getData().size() > 0) {

                                mArrHistory.addAll(response.body().getData());
                            }
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<HistoryPreliminaryExaminationModel> call, Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * OpenDialog for closing patient file
     */
    public void openFileClosingAlert() {


        AlertDialog.Builder builder = new AlertDialog.Builder(
                mActivity);
        builder.setTitle("Close File");
        builder.setMessage("Are you sure you want to close file?");
        builder.setNegativeButton("NO",
                (dialog, which) -> dialog.dismiss());
        builder.setPositiveButton("YES",
                (dialog, which) -> closeFile());
        builder.show();

    }

    private void closeFile() {
        try {
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.closeFileJson(mAppointmentId, mDatabaseName));

            Call<CloseFileModel> call = RetrofitClient.createService(ApiInterface.class).closeFileAPI(body);
            call.enqueue(new Callback<CloseFileModel>() {
                @Override
                public void onResponse(@NonNull Call<CloseFileModel> call, @NonNull Response<CloseFileModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        int mError = response.body().getError();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                Common.insertLog("IF");
                                Common.setCustomToast(mActivity,mMessage);
                            } else {
                                Common.insertLog("ELSE");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CloseFileModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open Dialog Print
     */
    public void openDialogHistoryPrint() {
        try {

            Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.custom_dialog_history_preliminary);

            TextView mTextTitle = dialog.findViewById(R.id.text_view_custom_dialog_history_preliminary);
            RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
            ImageView mImageClose = dialog.findViewById(R.id.image_custom_dialog_history_preliminary_close);

            RelativeLayout noInternetConnection = dialog.findViewById(R.id.relative_no_internet);
            RelativeLayout noDataFound = dialog.findViewById(R.id.relative_no_data_available);

            mTextTitle.setText(mActivity.getResources().getString(R.string.tab_print));

            mImageClose.setColorFilter(Common.setThemeColor(mActivity));

            mImageClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            LinearLayoutManager manager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(manager);


            HistoryPrintAdapter mAdapterPrint =
                    new HistoryPrintAdapter(mActivity, recyclerView, mArrHistory) {
                        @Override
                        protected void onSelectedItem(Activity activity, HistoryPreliminaryExaminationModel.Data data, boolean mHistoryAndComplains, boolean visionUCVA, boolean visionFinal, boolean visionBCVAUndilated, boolean visionBCVADilated, boolean mPrimaryExamination, boolean diagnosis, boolean counselingDetails, boolean investigationSuggested, boolean treatmentSuggested, boolean surgeryDetails, boolean prescription,String lang, boolean discharge, boolean payment) {
                            super.onSelectedItem(activity, data,
                                    mHistoryAndComplains, visionUCVA, visionFinal, visionBCVAUndilated, visionBCVADilated, mPrimaryExamination, diagnosis, counselingDetails, investigationSuggested, treatmentSuggested, surgeryDetails, prescription, lang, discharge, payment);
                            callToPrintFinalVision(data.getAppointmentID(), mHistoryAndComplains, visionUCVA, visionFinal, visionBCVAUndilated, visionBCVADilated, mPrimaryExamination, diagnosis, counselingDetails, investigationSuggested, treatmentSuggested, surgeryDetails, prescription,lang, discharge, payment);
                        }
                    };
            recyclerView.setAdapter(mAdapterPrint);

            if (mArrHistory.size() > 0) {
                noDataFound.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            } else {
                noDataFound.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * @param appointmentID          - Appointment ID
     * @param mHistoryAndComplains
     * @param visionUCVA
     * @param visionFinal
     * @param visionBCVAUndilated
     * @param visionBCVADilated
     * @param mPrimaryExamination
     * @param diagnosis
     * @param counselingDetails
     * @param investigationSuggested
     * @param treatmentSuggested
     * @param surgeryDetails
     * @param prescription
     * @param payment
     */
    private void callToPrintFinalVision(String appointmentID,
                                        boolean mHistoryAndComplains, boolean visionUCVA, boolean visionFinal,
                                        boolean visionBCVAUndilated, boolean visionBCVADilated, boolean mPrimaryExamination,
                                        boolean diagnosis, boolean counselingDetails, boolean investigationSuggested,
                                        boolean treatmentSuggested, boolean surgeryDetails, boolean prescription,String lang, boolean discharge, boolean payment) {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

/*            boolean mHistoryAndComplaints=true,mVisionUCVA=true,mVisionBCVAUndilated=true,mVisionFinal=true,
                    mVisionBCVADilated=true,mPrimaryExamination=true,mDiagnosis=true,mInvestigationSuggested=true,
                    mTreatmentSuggested=true,mCounselingDetails=true,mSurgeryDetails=true,mPrescription=true,
                    mPayment=true;*/

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPrintReceiptJson(AddNewPreliminaryExaminationFragment.PreliminaryExminationId,
                            String.valueOf(appointmentID), mHistoryAndComplains, visionUCVA, visionBCVAUndilated,
                            visionFinal, visionBCVADilated, mPrimaryExamination, diagnosis, investigationSuggested,
                            treatmentSuggested, counselingDetails, surgeryDetails, prescription,lang,discharge, payment, hospital_database));

            Call<PrintReceiptModel> call = RetrofitClient.createService(ApiInterface.class).PrintReceipt(body);
            call.enqueue(new Callback<PrintReceiptModel>() {
                @Override
                public void onResponse(@NonNull Call<PrintReceiptModel> call, @NonNull Response<PrintReceiptModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body() != null) {
                                String url = response.body().getData();
                                callToReceiptPrint(url);
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PrintReceiptModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }
}
