package com.trackopd.fragments;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.model.DoctorModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchReceptionistCheckUpFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private TextInputLayout mInputLayoutPatientName, mInputLayoutPatientCode, mInputLayoutMobileNo;
    private EditText mEditPatientName, mEditPatientCode, mEditMobileNo, mEditAppointmentNumber, mEditMRDNumber, mEditDate;
    private View mViewPatientName, mViewPatientCode, mViewMobileNo;
    private Button mButtonSearch;
    private ImageView mImageDoctor;
    private Spinner mSpinnerDoctor;
    private ArrayList<String> mArrDoctor;
    private ArrayList<DoctorModel> mArrDoctorList;
    private int mSelectedDoctorIndex, mDoctorId;
    private String mUserType, mAppointmentStatus,mIsFrom;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_search_receptionist_appointment, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrDoctor = new ArrayList<>();
        mArrDoctorList = new ArrayList<>();
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        getBundle();
        getIds();
        setData();
        setRegListeners();
        callToDoctorAPI();
        setHasOptionsMenu(true);

        mEditPatientCode.setFilters(Common.getFilter());
        mEditPatientName.setFilters(Common.getFilter());

        return mView;
    }

    /**
     * Set the prepare menu options
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {

                mIsFrom=bundle.getString(AppConstants.BUNDLE_IS_FROM_SEARCH);
//                mAppointmentStatus = bundle.getString(AppConstants.BUNDLE_APPOINTMENT_STATUS);
//                mSelectedDate = bundle.getString(AppConstants.BUNDLE_APPOINTMENT_SEL_DATE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Input Text Layouts
            mInputLayoutPatientName = mView.findViewById(R.id.text_input_search_receptionist_appointment_patient_name);
            mInputLayoutPatientCode = mView.findViewById(R.id.text_input_search_receptionist_appointment_patient_code);
            mInputLayoutMobileNo = mView.findViewById(R.id.text_input_search_receptionist_appointment_patient_mobile);

            // Edit Texts
            mEditPatientName = mView.findViewById(R.id.edit_search_receptionist_appointment_patient_name);
            mEditPatientCode = mView.findViewById(R.id.edit_search_receptionist_appointment_patient_code);
            mEditMobileNo = mView.findViewById(R.id.edit_search_receptionist_appointment_patient_mobile_no);
            mEditAppointmentNumber = mView.findViewById(R.id.edit_search_receptionist_appointment_number);
            mEditMRDNumber = mView.findViewById(R.id.edit_search_receptionist_appointment_mrd_number);

            // Views
            mViewPatientName = mView.findViewById(R.id.view_search_receptionist_appointment_patient_name);
            mViewPatientCode = mView.findViewById(R.id.view_search_receptionist_appointment_patient_code);
            mViewMobileNo = mView.findViewById(R.id.view_search_receptionist_appointment_patient_mobile_no);

            // Text Views
            mEditDate = mView.findViewById(R.id.edit_search_receptionist_appointment_date);

            // Buttons
            mButtonSearch = mView.findViewById(R.id.button_search_receptionist_appointment_search);

            // Image Views
            mImageDoctor = mView.findViewById(R.id.image_search_receptionist_appointment_doctor);

            // Spinners
            mSpinnerDoctor = mView.findViewById(R.id.spinner_search_receptionist_appointment_doctor);

            // Set Request Focus
            mEditPatientName.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            // Set Image Color as per the theme applied
            mImageDoctor.setColorFilter(Common.setThemeColor(mActivity));

            // Sets the edit text visibility as per the login type
            if (mUserType.equalsIgnoreCase(getString(R.string.user_type_receptionist))
                    || mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))
                    || mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {

                mInputLayoutPatientName.setVisibility(View.VISIBLE);
                mInputLayoutPatientCode.setVisibility(View.VISIBLE);
                mInputLayoutMobileNo.setVisibility(View.VISIBLE);
                mViewPatientName.setVisibility(View.VISIBLE);
                mViewPatientCode.setVisibility(View.VISIBLE);
                mViewMobileNo.setVisibility(View.VISIBLE);
            } else {
                mInputLayoutPatientName.setVisibility(View.GONE);
                mInputLayoutPatientCode.setVisibility(View.GONE);
                mInputLayoutMobileNo.setVisibility(View.GONE);
                mViewPatientName.setVisibility(View.GONE);
                mViewPatientCode.setVisibility(View.GONE);
                mViewMobileNo.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Click Listeners
            mButtonSearch.setOnClickListener(clickListener);
            mEditDate.setOnClickListener(clickListener);

            // ToDo: Doctor - Spinner Item Select Listener
            mSpinnerDoctor.setOnItemSelectedListener(onItemSelectedListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.button_search_receptionist_appointment_search:
                       if (checkValidation()) {
                           callToReceptionistCheckUpFragment();
                       }
                    break;

                case R.id.edit_search_receptionist_appointment_date:
                    Common.openDatePickerForAllDates(mActivity, mEditDate);
                    break;
            }
        }
    };

    /**
     * Check Validation
     * @return - status
     */
    private boolean checkValidation() {
        boolean status=true;

        String patient_Name = mEditPatientName.getText().toString().trim();
        String patient_code = mEditPatientCode.getText().toString().trim();
        String patient_mobile = mEditPatientCode.getText().toString().trim();

        int selected_doctor=mSpinnerDoctor.getSelectedItemPosition();

        if (patient_Name.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                && patient_code.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                && patient_mobile.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)
                )
        {
            Common.setCustomToast(mActivity, mActivity.getString(R.string.error_search_patient));
            status = false;
        }

        return status;
    }

    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {
                case R.id.spinner_search_receptionist_appointment_doctor:
                    mSelectedDoctorIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedDoctorIndex != 0) {
                        mDoctorId = (Integer.parseInt(mArrDoctorList.get(mSelectedDoctorIndex - 1).getDoctorID()));
                        String mDoctorName = (mArrDoctorList.get(mSelectedDoctorIndex - 1).getDoctorName());
                        Common.insertLog("mDoctorID::> " + mDoctorId);
                        Common.insertLog("mDoctorName::> " + mDoctorName);
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    /**
     * This method should call the Doctor API
     */
    private void callToDoctorAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setDoctorListJson(hospital_database));

            Call<DoctorModel> call = RetrofitClient.createService(ApiInterface.class).getDoctorList(body);
            call.enqueue(new Callback<DoctorModel>() {
                @Override
                public void onResponse(@NonNull Call<DoctorModel> call, @NonNull Response<DoctorModel> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrDoctorList.addAll(response.body().getData());
                            }
                            setsDoctorAdapter();
                        } else {
                            setsDoctorAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DoctorModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setsDoctorAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsDoctorAdapter() {
        try {
            mArrDoctor.add(0, getString(R.string.spinner_select_doctor));
            if (mArrDoctorList.size() > 0) {
                for (DoctorModel doctorModel : mArrDoctorList) {
                    mArrDoctor.add(doctorModel.getDoctorName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerDoctor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrDoctor) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedDoctorIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerDoctor.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Receptionist Patient Fragment
     */
    private void callToReceptionistCheckUpFragment() {
        try {
            String mAppointmentDate;
            String mName = mEditPatientName.getText().toString().trim();
            String mPatientCode = mEditPatientCode.getText().toString().trim();
            String mMobileNo = mEditMobileNo.getText().toString().trim();
            String mMRDNo = mEditMRDNumber.getText().toString().trim();
            String mAppointmentNo = mEditAppointmentNumber.getText().toString().trim();

            if (mEditDate.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                mAppointmentDate = mActivity.getResources().getString(R.string.date_format_first_time);
            } else {
                mAppointmentDate = Common.convertDateWithoutTime(mActivity, mEditDate.getText().toString().trim(), mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
            }

            if (mSpinnerDoctor.getSelectedItemPosition() == 0) {
                mDoctorId = -1;
            }

            Fragment fragment=null;

            if (mIsFrom.equalsIgnoreCase(mActivity.getResources().getString(R.string.nav_menu_surgery))){
                fragment = new SurgeryListFragment();
                if (mUserType.equalsIgnoreCase(getString(R.string.user_type_receptionist))) {
                    ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_surgery));
                } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
                    ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_surgery));
                } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                    ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_surgery));
                } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_patient))) {
                    ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_surgery));
                } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_councillor))) {
                    ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_surgery));
                }

            }else if (mIsFrom.equalsIgnoreCase(mActivity.getResources().getString(R.string.nav_menu_check_up)))
            {
                fragment = new CheckInFragment();

                if (mUserType.equalsIgnoreCase(getString(R.string.user_type_receptionist))) {
                    ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_check_up));
                } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
                    ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_check_up));
                } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                    ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_check_up));
                } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_patient))) {
                    ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_check_up));
                } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_councillor))) {
                    ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_check_up));
                }
            }


            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_NAME, mName);
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, mPatientCode);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mMobileNo);
            /*args.putString(AppConstants.BUNDLE_MRD_NO, mMRDNo);
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, mAppointmentNo);
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DATE, mAppointmentDate);*/
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, mDoctorId);
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DATE, mAppointmentDate);


            fragment.setArguments(args);

            FragmentManager fragmentManager = null;

            if (mUserType.equalsIgnoreCase(getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_patient))) {
                fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    mActivity.getResources().getString(R.string.nav_menu_check_up)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
