package com.trackopd.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.trackopd.R;
import com.trackopd.model.checkDoctorAvailabiltyModel;
import com.trackopd.model.getReceptionistModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceptionistHomeFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private LinearLayout mLinearAttendee;
    private ArrayList<checkDoctorAvailabiltyModel.Data> mArrDoctorAvailability;
    private AppUtil mAppUtils;
    private ArrayList<Object> mArrDoctor;
    private TextView mTextTodayAppointment, mTextPendingAppointment, mTextConfirmAppointment,
                mTextCompleteAppointment,mTextPaymentReceived,mTextPaymentDue,mTextTotalCheckUp,
                mTextTotalSurgeries ;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_receptionist_home, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;

        mArrDoctorAvailability=new ArrayList<checkDoctorAvailabiltyModel.Data>();
        mArrDoctor=new ArrayList<Object>();

        mActivity = getActivity();
        mAppUtils=new AppUtil(mActivity);
        getIds();
        setData();
        callToReceptionistHomeAPI();
        setHasOptionsMenu(true);
        return mView;
    }



    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Web View
            //mLinearAttendee = mView.findViewById(R.id.linear_dashboard_attendee_dynamic_view);
            //Text View
            mTextTodayAppointment=mView.findViewById(R.id.text_receptionist_home_today_appointment);
            mTextPendingAppointment=mView.findViewById(R.id.text_receptionist_home_pending_appointment);
            mTextConfirmAppointment=mView.findViewById(R.id.text_receptionist_home_confirmed_appointment);
            mTextCompleteAppointment=mView.findViewById(R.id.text_receptionist_home_completed_appointment);
            mTextPaymentReceived=mView.findViewById(R.id.text_receptionist_home_payment_received);
            mTextPaymentDue=mView.findViewById(R.id.text_receptionist_home_payment_due);
            mTextTotalCheckUp=mView.findViewById(R.id.text_receptionist_home_check_up_appointment);
            mTextTotalSurgeries=mView.findViewById(R.id.text_receptionist_home_total_surgeries);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     *
     */
    private void callToReceptionistHomeAPI() {
       if (!mAppUtils.getConnectionState()){

       }else {
           String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

           RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                   APICommonMethods.setReceptionistListJson(hospital_database));

           //Common.showLoadingDialog(mActivity,getString(R.string.action_upload));
           Call<getReceptionistModel> call = RetrofitClient.createService(ApiInterface.class).getReceptionistListApi(requestBody);
           call.enqueue(new Callback<getReceptionistModel>() {
               @Override
               public void onResponse(Call<getReceptionistModel> call, Response<getReceptionistModel> response) {
                   Common.insertLog("Response......"+response.body().toString());
                   if (response.isSuccessful() &&
                           response.body().getError()==AppConstants.API_SUCCESS_ERROR){
                       setResponseData(response.body().getData());
                   }
               }
               @Override
               public void onFailure(Call<getReceptionistModel> call, Throwable t) {
                    Common.insertLog(t.getMessage());
                    Common.setCustomToast(mActivity,t.getMessage());
               }
           });
       }
    }

    /**
     * set Value on Response
     * @param data Data- Get Response Model
     */
    private void setResponseData(List<getReceptionistModel.Data> data) {
        if (data!=null && data.size()>0){

            String today_appointment=data.get(0).getTotalAppointment();
            String pending_appointment=data.get(0).getPending();
            String Confirm_appointment=data.get(0).getConfirmed();
            String Completed_appointment=data.get(0).getCompleted();
            String payment_received=data.get(0).getPaymentReceived();
            String payment_due=data.get(0).getPaymentDue();
            String total_checkUp=data.get(0).getTotalCheckUp();
            String total_surgeries=data.get(0).getTotalCheckUp();

            mTextTodayAppointment.setText("("+today_appointment+")");
            mTextPendingAppointment.setText("("+pending_appointment+")");
            mTextConfirmAppointment.setText("("+Confirm_appointment+")");
            mTextCompleteAppointment.setText("("+Completed_appointment+")");
            mTextPaymentReceived.setText(payment_received);
            mTextPaymentDue.setText(payment_due);
            mTextTotalCheckUp.setText("("+total_checkUp+")");
            mTextTotalSurgeries.setText("("+total_surgeries+")");

        }else {
            mTextTodayAppointment.setText("(0)");
            mTextPendingAppointment.setText("(0)");
            mTextConfirmAppointment.setText("(0)");
            mTextCompleteAppointment.setText("(0)");
            mTextTotalCheckUp.setText("(0)");
            mTextPaymentReceived.setText("0");
            mTextPaymentDue.setText("0");
            mTextTotalSurgeries.setText("(0)");

        }
    }

    /**
     * Set Data
     */
    private void setData() {
       try {
           mTextTodayAppointment.setText("(0)");
           mTextPendingAppointment.setText("(0)");
           mTextConfirmAppointment.setText("(0)");
           mTextCompleteAppointment.setText("(0)");
           mTextPaymentReceived.setText("0");
           mTextPaymentDue.setText("0");
       }catch (Exception e){
           Common.insertLog(e.getMessage());
       }
    }

    /**
     * This method should add the attendee availability view dynamically
     */
    private void setsAttendeeAvailability() {
        try {
            for (int i = 0; i < mArrDoctorAvailability.size(); i++) {
                LayoutInflater layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View addView = layoutInflater.inflate(R.layout.row_doctor_availability_item, null);

                // Text Views
                LinearLayout mLinearAttendeeAvailability = addView.findViewById(R.id.linear_row_doctor_availability_doctor_name);

                // Text Views
                TextView mTextTime = addView.findViewById(R.id.text_row_doctor_availability_time);
                TextView mTextAttendeeName = addView.findViewById(R.id.text_row_doctor_availability_doctor_name);

                // Sets up the data
                //mTextTime.setText(Common.convertTime(mActivity, mArrDoctorAvailability.get(i).getTimeSlot()));

                mArrDoctor = (ArrayList<Object>) mArrDoctorAvailability.get(i).getDoctor();

                if(mArrDoctor.size() != 0) {
                    mLinearAttendeeAvailability.setBackgroundResource(R.color.colorPrimaryRed);
                    List<String> mAttendee = new ArrayList<>();
                    for (int j = 0; j < mArrDoctor.size(); j++) {
                        String mAttendeeName = mArrDoctor.get(j).toString();
                        mAttendee.add(mAttendeeName);
                        mTextAttendeeName.setText(mAttendee.toString().replaceAll("\\[", "").replaceAll("\\]", ""));
                    }
                } else {
                    mLinearAttendeeAvailability.setBackgroundResource(R.color.color_green);
                }

                // Add dynamic view
                mLinearAttendee.addView(addView);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

     /*
     *  This method should call the attendee availability listing
     * */
    private void callAttendeeAvailabilityListAPI() {
        try {
            if (!mAppUtils.getConnectionState()) {
                mArrDoctorAvailability.clear();
            } else {

                String hospital_database= GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);
                RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                        APICommonMethods.setDoctorAvailabiltyListJson(hospital_database));

                Common.showLoadingDialog(mActivity,getString(R.string.action_upload));
                Call<checkDoctorAvailabiltyModel> call = RetrofitClient.createService(ApiInterface.class).getcheckDoctorAvailabiltyListAPI(requestBody);
                call.enqueue(new Callback<checkDoctorAvailabiltyModel>() {
                    @Override
                    public void onResponse(@NonNull Call<checkDoctorAvailabiltyModel> call, @NonNull Response<checkDoctorAvailabiltyModel> response) {
                        Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                        try {
                            int mError = response.body().getError();

                            if (response.isSuccessful() && mError == 200) {
                                Common.hideDialog();
                                if (response.body() != null && response.body().getData() != null) {
                                    mArrDoctorAvailability= (ArrayList<checkDoctorAvailabiltyModel.Data>) response.body().getData();
                                    setsAttendeeAvailability();
                                }
                            } else {
                                Common.hideDialog();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Common.hideDialog();
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<checkDoctorAvailabiltyModel> call, @NonNull Throwable t) {
                        Common.insertLog("Failure:::> " + t.getMessage());
                        Common.setCustomToast(mActivity, t.getMessage());
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}