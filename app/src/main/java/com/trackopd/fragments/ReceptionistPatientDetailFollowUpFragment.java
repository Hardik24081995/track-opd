package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.ReceptionistFollowUpAdapter;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.ReceptionistFollowUpModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.utils.RecyclerProgressView;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceptionistPatientDetailFollowUpFragment extends Fragment implements ReceptionistFollowUpAdapter.SingleClickListener {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private AppUtil mAppUtils;
    private SwipeRefreshLayout mSwipeRefreshView;
    private RelativeLayout mRelativeNoData, mRelativeNoInternet;
    private Button mButtonRetry;
    private RecyclerView mRecyclerView;
    private ReceptionistFollowUpAdapter mAdapterReceptionistFollowUp;
    private ArrayList<ReceptionistFollowUpModel> mArrReceptionistFollowUp;
    private boolean isFirstTime = true, isStarted = false, isVisible = false;
    private String mPatientId = "", mName = "", mFirstName = "", mLastName = "", mPatientCode = "",
            mMobileNo = "", mFollowUpDate = "1970-01-01",mProfilePic=AppConstants.STR_EMPTY_STRING;
    private int mDoctorId = -1, mCouncillorId = -1;

    // Load More Listener Variables
    private int currentPageIndex = 1, lastFetchRecord = 0, totalRecords = 0;
    private boolean isRefresh = false, isLoadMore = false;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_receptionist_patient_detail_follow_up, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mAppUtils = new AppUtil(getActivity());
        mArrReceptionistFollowUp = new ArrayList<>();
        Common.insertLog("Tab 5");

        getBundle();
        getIds();
        setRegListeners();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_detail_follow_up, menu);

        MenuItem menuAdd = menu.findItem(R.id.action_detail_follow_up_add);
        menuAdd.setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_detail_follow_up_add:
                Common.insertLog("TAB PLUS FOLLOW");
                callToAddFollowUpFragment();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mFirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mLastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mPatientCode = bundle.getString(AppConstants.BUNDLE_PATIENT_CODE);
                mMobileNo = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mProfilePic=bundle.getString(AppConstants.BUNDLE_PATIENT_PROFILE);
                mName = mFirstName + AppConstants.STR_EMPTY_SPACE +mLastName;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Recycler View
            mRecyclerView = mView.findViewById(R.id.recycler_view);

            // Swipe Refresh View
            mSwipeRefreshView = mView.findViewById(R.id.swipe_refresh_view);

            // Relative Layouts
            mRelativeNoData = mView.findViewById(R.id.relative_no_data_available);
            mRelativeNoInternet = mView.findViewById(R.id.relative_no_internet);

            // Buttons
            mButtonRetry = mView.findViewById(R.id.button_retry);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Click Listeners
            mButtonRetry.setOnClickListener(clickListener);

            // ToDo: Set On Refresh Listeners
            mSwipeRefreshView.setOnRefreshListener(refreshListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_retry:
                    callShimmerView();
                    break;
            }
        }
    };

    /**
     * Set On Refresh Listeners
     */
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {

        @Override
        public void onRefresh() {
            isFirstTime = true;

            if (!mAppUtils.getConnectionState()) {
                mRelativeNoInternet.setVisibility(View.VISIBLE);
                mSwipeRefreshView.setRefreshing(false);
                mArrReceptionistFollowUp.clear();
                setViewVisibility();
            } else {
                mRelativeNoInternet.setVisibility(View.GONE);
                if (!isLoadMore) {
                    isRefresh = true;

                    mFollowUpDate = "1970-01-01";
                    mDoctorId = -1;
                    mCouncillorId = -1;

                    if (mArrReceptionistFollowUp != null)
                        mArrReceptionistFollowUp.clear();
                    if (mAdapterReceptionistFollowUp != null)
                        mAdapterReceptionistFollowUp.notifyDataSetChanged();
                    currentPageIndex = 1;
                    callShimmerView();
                } else
                    mSwipeRefreshView.setRefreshing(false);
            }
        }
    };

    /**
     * This method is used to call the shimmer effect for recycler view before calling the API
     */
    private void callShimmerView() {
        if (!mAppUtils.getConnectionState()) {
            mRelativeNoInternet.setVisibility(View.VISIBLE);
            mArrReceptionistFollowUp.clear();
            setViewVisibility();
        } else {
            mRelativeNoInternet.setVisibility(View.GONE);
            if (isFirstTime) {
                showProgressView();
                isFirstTime = false;
                callFollowUpListAPI();
            } else {
                callFollowUpListAPI();
            }
        }
    }

    /**
     * Show progress view when page load for the first time
     */
    private void showProgressView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.VISIBLE);

                RecyclerProgressView.startProgress((LinearLayout) mView.findViewById(R.id.linear_add_recycler_progress_view),
                        R.layout.row_receptionist_follow_up_item, getActivity());
            }
        }, 100);
    }

    /**
     * This method should call the Follow Up listing for receptionist
     */
    private void callFollowUpListAPI() {
        try {
            if (mName == null) {
                mName = AppConstants.STR_EMPTY_STRING;
            }

            if (mPatientCode == null) {
                mPatientCode = AppConstants.STR_EMPTY_STRING;
            }

            if (mMobileNo == null) {
                mMobileNo = AppConstants.STR_EMPTY_STRING;
            }

            if (mFollowUpDate == null) {
                mFollowUpDate = "1970-01-01";
            }

            String hospital_database= GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setReceptionistFollowUpListJson(currentPageIndex, AppConstants.STR_EMPTY_STRING, mPatientCode, mMobileNo, mFollowUpDate, mDoctorId, mCouncillorId, mActivity.getResources().getString(R.string.tab_all), hospital_database));

            Call<ReceptionistFollowUpModel> call = RetrofitClient.createService(ApiInterface.class).getReceptionistFollowUpList(requestBody);
            call.enqueue(new Callback<ReceptionistFollowUpModel>() {
                @Override
                public void onResponse(@NonNull Call<ReceptionistFollowUpModel> call, @NonNull Response<ReceptionistFollowUpModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        stopProgressView();
                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                ArrayList<ReceptionistFollowUpModel> followUpModel = new ArrayList<>(Arrays
                                        .asList(new GsonBuilder().serializeNulls().create()
                                                .fromJson(new JsonParser().parse(jsonObject.getString(WebFields.DATA)).
                                                        getAsJsonArray(), ReceptionistFollowUpModel[].class)));

                                if (mArrReceptionistFollowUp != null && mArrReceptionistFollowUp.size() > 0 &&
                                        mAdapterReceptionistFollowUp != null) {
                                    mArrReceptionistFollowUp.addAll(followUpModel);
                                    mAdapterReceptionistFollowUp.notifyDataSetChanged();
                                    lastFetchRecord = mArrReceptionistFollowUp.size();
                                } else {
                                    mArrReceptionistFollowUp = followUpModel;
                                    setAdapterData();
                                    lastFetchRecord = mArrReceptionistFollowUp.size();
                                    totalRecords = jsonObject.getInt(WebFields.ROW_COUNT);
                                    setLoadMoreClickListener();

                                    if (mRecyclerView.getVisibility() == View.GONE) {
                                        stopProgressView();
                                    }
                                }
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                            stopProgressView();
                            setViewVisibility();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ReceptionistFollowUpModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    stopProgressView();
                    setViewVisibility();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mGridLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(mGridLayoutManager);

            stopProgressView();
            mAdapterReceptionistFollowUp = new ReceptionistFollowUpAdapter(mActivity, mRecyclerView, mArrReceptionistFollowUp);
            mRecyclerView.setAdapter(mAdapterReceptionistFollowUp);
            mAdapterReceptionistFollowUp.setOnItemClickListener(this);

            setViewVisibility();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the view visibility as per the array size
     */
    private void setViewVisibility() {
        stopProgressView();
        if (mArrReceptionistFollowUp.size() != 0) {
            mRelativeNoData.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mRelativeNoData.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    /**
     * Show progress view when page load for the first time
     */
    private void stopProgressView() {
        try {
            mSwipeRefreshView.setRefreshing(false);
            isRefresh = false;
            isLoadMore = false;
            RecyclerProgressView.stopProgress();
            mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            hideProgressBar();
        }
    }

    /**
     * This method redirects you to the Add Receptionist Follow Up Fragment
     */
    private void callToAddFollowUpFragment() {
        try {

            ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.nav_menu_follow_up));

            Fragment fragment = new AddFollowUpFragment();

            Bundle bundle = new Bundle();
            bundle.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, mFirstName);
            bundle.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, mLastName);
            bundle.putString(AppConstants.BUNDLE_PATIENT_CODE, mPatientCode);
            bundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mMobileNo);
            bundle.putString(AppConstants.BUNDLE_PATIENT_ID, mPatientId);
            bundle.putString(AppConstants.BUNDLE_PATIENT_PROFILE,mProfilePic);
            fragment.setArguments(bundle);
            FragmentManager fragmentManager =
                    ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_receptionist_follow_up))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_receptionist_follow_up))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClickListener() {
        mArrReceptionistFollowUp.clear();
        callFollowUpListAPI();
    }

    /**
     * Hide the progress bar
     */
    private void hideProgressBar() {
        try {
            if (mArrReceptionistFollowUp != null && mArrReceptionistFollowUp.size() > 0 &&
                    mArrReceptionistFollowUp.get(mArrReceptionistFollowUp.size() - 1) == null) {

                mArrReceptionistFollowUp.remove(mArrReceptionistFollowUp.size() - 1);
                mAdapterReceptionistFollowUp.notifyItemRemoved(mArrReceptionistFollowUp.size());

                mAdapterReceptionistFollowUp.notifyDataSetChanged();
                mAdapterReceptionistFollowUp.setLoaded();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        RecyclerProgressView.stopProgress();
    }

    /**
     * On load more click listener
     */
    private void setLoadMoreClickListener() {
        mAdapterReceptionistFollowUp.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (mArrReceptionistFollowUp != null && mArrReceptionistFollowUp.size() > 0 && mArrReceptionistFollowUp.size() <
                        totalRecords) {
                    if (!mAppUtils.getConnectionState()) {
                        mRelativeNoInternet.setVisibility(View.VISIBLE);
                        mArrReceptionistFollowUp.clear();
                        setViewVisibility();
                    } else {
                        mRelativeNoInternet.setVisibility(View.GONE);
                        if (!isRefresh) {
                            isLoadMore = true;
                            mArrReceptionistFollowUp.add(null);
                            mAdapterReceptionistFollowUp.notifyItemInserted(mArrReceptionistFollowUp.size() - 1);

                            currentPageIndex = (mArrReceptionistFollowUp.size() / Common.getPageSizeForPagination(mActivity)) + 1;
                            callFollowUpListAPI();
                        }
                    }
                }
            }
        });
    }

    /**
     * Receptionist Patient Detail Follow Up Fragment on visible
     *
     * @param isVisibleToUser - This can check the fragment is visible when swiping and will return
     *                        the boolean value based on that
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        isVisible = isVisibleToUser;
        if (isStarted && isVisible) {
            mArrReceptionistFollowUp.clear();

            callShimmerView();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
            mArrReceptionistFollowUp.clear();
            if (mAdapterReceptionistFollowUp != null) {
                mAdapterReceptionistFollowUp.notifyDataSetChanged();
            }
            getBundle();
            callShimmerView();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isVisible = false;
        isStarted = false;
    }
}