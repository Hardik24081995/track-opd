package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.ViewPagerAdapter;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

public class ReceptionistFollowUpFragment extends Fragment {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    public Bundle mBundle;
    private String mName = "", mPatientCode = "", mMobileNo = "", mFollowUpDate = "0000-00-00", mUserType;
    private int mDoctorId = -1, mCouncillorId = -1;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_receptionist_follow_up, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        //Hide Soft Keyboard
        Common.hideSoftKeyboard(mActivity);
        getBundle();
        getIds();
        setRegListeners();
        setBundle();
        setViewPager();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        final MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(false);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mName = bundle.getString(AppConstants.BUNDLE_PATIENT_NAME);
                mPatientCode = bundle.getString(AppConstants.BUNDLE_PATIENT_CODE);
                mMobileNo = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mFollowUpDate = bundle.getString(AppConstants.BUNDLE_PATIENT_FOLLOW_UP_DATE);
                mDoctorId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);
                mCouncillorId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_COUNCILLOR);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Tab Layout
            mTabLayout = mView.findViewById(R.id.tab_layout_receptionist_follow_up);

            // View Pager
            mViewPager = mView.findViewById(R.id.view_pager_receptionist_follow_up);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mTabLayout.setOnTabSelectedListener(onTabSelectedListener(mViewPager));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the bundle data
     */
    private void setBundle() {
        try {
            mBundle = new Bundle();
            mBundle.putString(AppConstants.BUNDLE_PATIENT_NAME, mName);
            mBundle.putString(AppConstants.BUNDLE_PATIENT_CODE, mPatientCode);
            mBundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mMobileNo);
            mBundle.putString(AppConstants.BUNDLE_PATIENT_FOLLOW_UP_DATE, mFollowUpDate);
            mBundle.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, mDoctorId);
            mBundle.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_COUNCILLOR, mCouncillorId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the view pager and binding the tab layouts
     */
    private void setViewPager() {
        ViewPagerAdapter adapter = null;

        if (mUserType.equalsIgnoreCase(getString(R.string.user_type_receptionist))) {
            adapter = new ViewPagerAdapter(((ReceptionistHomeActivity) mActivity).getSupportFragmentManager());
        } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_councillor))) {
            adapter = new ViewPagerAdapter(((CouncillorHomeActivity) mActivity).getSupportFragmentManager());
        } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_doctor))) {
            adapter = new ViewPagerAdapter(((DoctorHomeActivity) mActivity).getSupportFragmentManager());
        } else if (mUserType.equalsIgnoreCase(getString(R.string.user_type_optometrist))) {
            adapter = new ViewPagerAdapter(((OptometristHomeActivity) mActivity).getSupportFragmentManager());
        }

        adapter.addFragment(new ReceptionistPastFollowUpFragment(), mActivity.getResources().getString(R.string.tab_past), mBundle);
        adapter.addFragment(new ReceptionistTodayFollowUpFragment(), mActivity.getResources().getString(R.string.tab_today), mBundle);
        adapter.addFragment(new ReceptionistUpcomingFollowUpFragment(), mActivity.getResources().getString(R.string.tab_upcoming), mBundle);

        mViewPager.setAdapter(adapter);
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.setOffscreenPageLimit(1);
        mViewPager.setCurrentItem(1);

        mTabLayout.setSelectedTabIndicatorColor(Common.setThemeColor(mActivity));
        mTabLayout.setTabTextColors(mActivity.getResources().getColor(R.color.colorBlack), Common.setThemeColor(mActivity));
    }

    private TabLayout.OnTabSelectedListener onTabSelectedListener(final ViewPager viewPager) {

        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }
}