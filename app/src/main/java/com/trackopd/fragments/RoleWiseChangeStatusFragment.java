package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.ViewPagerAdapter;
import com.trackopd.model.ReceptionistAppointmentModel;
import com.trackopd.model.StatusTabModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.StringUtils;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoleWiseChangeStatusFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private ReceptionistAppointmentModel mReceptionistAppointmentModel;
    private ArrayList<StatusTabModel> mArrStatusTab;
    private ViewPagerAdapter mAdapter;
    private String update_status;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_change_status_role, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrStatusTab = new ArrayList<>();

        getBundle();
        getIds();
        setRegListeners();
        callToStatusTabsAPI();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mReceptionistAppointmentModel = (ReceptionistAppointmentModel) bundle.getSerializable(WebFields.RECEPTIONIST_APPOINTMENT_LIST.MODE_RECEPTIONIST);

               if (!StringUtils.isEmpty(bundle.getString(AppConstants.BUNDLE_CHANGE_STATUS))) {
                   update_status=bundle.getString(AppConstants.BUNDLE_CHANGE_STATUS,AppConstants.STR_EMPTY_STRING);
               }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Tab Layout
            mTabLayout = mView.findViewById(R.id.tab_layout_change_status_role);

            // View Pager
            mViewPager = mView.findViewById(R.id.view_pager_change_status_role);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mTabLayout.setOnTabSelectedListener(onTabSelectedListener(mViewPager));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Status Tabs API
     */
    private void callToStatusTabsAPI() {
        try {
            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);

            String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setStatusTabListJson(mUserId,hospital_database));
            Call<StatusTabModel> call = RetrofitClient.createService(ApiInterface.class).getStatusTab(requestBody);
            call.enqueue(new Callback<StatusTabModel>() {
                @Override
                public void onResponse(@NonNull Call<StatusTabModel> call, @NonNull Response<StatusTabModel> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null) {
                                mArrStatusTab.addAll(response.body().getData());
                                setViewPager();
                            }
                        } else {
                            Common.insertLog(mMessage);
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<StatusTabModel> call, @NonNull Throwable t) {
                    Common.insertLog(t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the view pager and binding the tab layouts
     */
    private void setViewPager() {
        try {

            String mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                mAdapter = new ViewPagerAdapter(((DoctorHomeActivity) mActivity).getSupportFragmentManager());
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                mAdapter = new ViewPagerAdapter(((CouncillorHomeActivity) mActivity).getSupportFragmentManager());
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                mAdapter = new ViewPagerAdapter(((ReceptionistHomeActivity) mActivity).getSupportFragmentManager());
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                mAdapter = new ViewPagerAdapter(((OptometristHomeActivity) mActivity).getSupportFragmentManager());
            }

            for (int i = 0; i < mArrStatusTab.size(); i++) {
                Bundle mBundle = new Bundle();
                mBundle.putSerializable(WebFields.RECEPTIONIST_APPOINTMENT_LIST.MODE_RECEPTIONIST, mReceptionistAppointmentModel);
                mBundle.putString(WebFields.GET_STATUS_TAB_LIST.REQUEST_STATUS_ID, mArrStatusTab.get(i).getStatusID());
                mBundle.putString(AppConstants.BUNDLE_LOGIN_TYPE, mUserType);
                mAdapter.addFragment(new RoleWiseDetailsChangeStatusFragment(), mArrStatusTab.get(i).getTitle(), mBundle);
            }
            mViewPager.setAdapter(mAdapter);
            mTabLayout.setupWithViewPager(mViewPager);
            mViewPager.setOffscreenPageLimit(1);

            mTabLayout.setSelectedTabIndicatorColor(Common.setThemeColor(mActivity));
            mTabLayout.setTabTextColors(mActivity.getResources().getColor(R.color.colorBlack), Common.setThemeColor(mActivity));

            if (!StringUtils.isEmpty(update_status)){
                for (int j = 0; j < mArrStatusTab.size(); j++) {
                    if (mArrStatusTab.get(j).getTitle().equalsIgnoreCase(update_status)){
                        mViewPager.setCurrentItem(j);
                    }
               }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   /* @Override
    public void onStart() {
        super.onStart();
        if (mArrStatusTab.size() > 0) {
            mArrStatusTab.clear();
        }
        if (mAdapter != null) {
            mAdapter.notifyDataSetChanged();
            mViewPager.removeAllViews();
        }
        callToStatusTabsAPI();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        //setViewPager();
    }*/

    private TabLayout.OnTabSelectedListener onTabSelectedListener(final ViewPager viewPager) {

        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }
}
