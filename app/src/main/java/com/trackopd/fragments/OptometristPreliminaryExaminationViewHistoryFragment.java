package com.trackopd.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.trackopd.R;
import com.trackopd.model.PreliminaryExaminationViewHistoryModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OptometristPreliminaryExaminationViewHistoryFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private LinearLayout mLinearDynamicView;
    private String mPatientId;
    private ArrayList<PreliminaryExaminationViewHistoryModel> mArrComplaints;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_optometrist_preliminary_examination_view_history, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrComplaints = new ArrayList<>();

        getBundle();
        getIds();
        callToViewHistoryAPI();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Get Bundle Key Value Previous screen
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Linear Layouts
            mLinearDynamicView = mView.findViewById(R.id.linear_optometrist_preliminary_examination_view_history_dynamic_view);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the View History API for Optometrist
     */
    private void callToViewHistoryAPI() {
        try {
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPreliminaryExaminationViewHistoryListJson(mPatientId, mDatabaseName));

            Call<PreliminaryExaminationViewHistoryModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationViewHistoryList(body);
            call.enqueue(new Callback<PreliminaryExaminationViewHistoryModel>() {
                @Override
                public void onResponse(@NonNull Call<PreliminaryExaminationViewHistoryModel> call, @NonNull Response<PreliminaryExaminationViewHistoryModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null) {

                                mArrComplaints.addAll(response.body().getData());
                                bindDynamicView();
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PreliminaryExaminationViewHistoryModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should bind the dynamic view
     */
    private void bindDynamicView() {
        try {
            for(int i=0; i<mArrComplaints.size(); i++) {
                LayoutInflater layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View addView = layoutInflater.inflate(R.layout.row_optometrist_preliminary_examination_view_history_item, null);

                // Text Views
                TextView mTextRightEyeNCT = addView.findViewById(R.id.text_row_optometrist_preliminary_examination_view_history_right_eye_nct);
                TextView mTextLeftEyeNCT = addView.findViewById(R.id.text_row_optometrist_preliminary_examination_view_history_left_eye_nct);
                TextView mTextRightEyeAT = addView.findViewById(R.id.text_row_optometrist_preliminary_examination_view_history_right_eye_at);
                TextView mTextLeftEyeAT = addView.findViewById(R.id.text_row_optometrist_preliminary_examination_view_history_left_eye_at);
                TextView mTextRightEyePachymetry = addView.findViewById(R.id.text_row_optometrist_preliminary_examination_view_history_right_eye_pachymetry);
                TextView mTextLeftEyePachymetry = addView.findViewById(R.id.text_row_optometrist_preliminary_examination_view_history_left_eye_pachymetry);
                TextView mTextRightEyeColorVision = addView.findViewById(R.id.text_row_optometrist_preliminary_examination_view_history_right_eye_color_vision);
                TextView mTextLeftEyeColorVision = addView.findViewById(R.id.text_row_optometrist_preliminary_examination_view_history_left_eye_color_vision);
                TextView mTextRightEyeSyringing = addView.findViewById(R.id.text_row_optometrist_preliminary_examination_view_history_right_eye_syringing);
                TextView mTextLeftEyeSyringing = addView.findViewById(R.id.text_row_optometrist_preliminary_examination_view_history_left_eye_syringing);
                TextView mTextRightEyeSchirmer = addView.findViewById(R.id.text_row_optometrist_preliminary_examination_view_history_right_eye_schirmer);
                TextView mTextLeftEyeSchirmer = addView.findViewById(R.id.text_row_optometrist_preliminary_examination_view_history_left_eye_schirmer);

                mTextRightEyeNCT.setText(mArrComplaints.get(i).getRNCT());
                mTextLeftEyeNCT.setText(mArrComplaints.get(i).getLNCT());
                mTextRightEyeAT.setText(mArrComplaints.get(i).getRAT());
                mTextLeftEyeAT.setText(mArrComplaints.get(i).getRAT());
                mTextRightEyePachymetry.setText(mArrComplaints.get(i).getRPachymetry());
                mTextLeftEyePachymetry.setText(mArrComplaints.get(i).getLPachymetry());
                mTextRightEyeColorVision.setText(mArrComplaints.get(i).getRColorVision());
                mTextLeftEyeColorVision.setText(mArrComplaints.get(i).getLColorVision());
                mTextRightEyeSyringing.setText(mArrComplaints.get(i).getRSyringing());
                mTextLeftEyeSyringing.setText(mArrComplaints.get(i).getLSyringing());
                mTextRightEyeSchirmer.setText(mArrComplaints.get(i).getRSchirmer());
                mTextLeftEyeSchirmer.setText(mArrComplaints.get(i).getLSchirmer());

                // Add dynamic view
                mLinearDynamicView.addView(addView, 0);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}