package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.trackopd.R;
import com.trackopd.adapter.HistoryInvestigationAdapter;
import com.trackopd.model.AddDoodleImageModel;
import com.trackopd.model.CloseFileModel;
import com.trackopd.model.CouselingPackageSuggestionModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppPermissions;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.CameraGalleryPermission;
import com.trackopd.utils.Common;
import com.trackopd.utils.Compressor;
import com.trackopd.utils.FileUtil;
import com.trackopd.utils.ImageUtil;
import com.trackopd.utils.SessionManager;
import com.trackopd.utils.searchablespinnerlibrary.MultiSpinnerSearch;
import com.trackopd.utils.searchablespinnerlibrary.SpinnerListener;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;
import com.yalantis.ucrop.util.FileUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvestigationFragment extends Fragment {

    private static ArrayList<HistoryPreliminaryExaminationModel.Data> mArrHistory;
    RecyclerView listView;
    RelativeLayout mRelativeUploadImagedata;
    String imageName1 = null;
    String imageName2 = null;
    private View mView;
    private Activity mActivity;
    private Button mButtonSubmit;
    private ImageView mImageContainer, mImageInvestigation;
    private LinearLayout mlinearInvestigationDynamicView, mLinearUploadImageData;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.Ocularinvestigation> mArrOcularInvestigation;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.LaboratoryTest> mArrLaboratoryTest;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.Eye> mArrEye;
    private ArrayList<String> mArrEyeName;
    private ArrayList<CouselingPackageSuggestionModel> mOcularInvestigation, mLaboratoryTest;
    private ArrayList<Uri> arrayList = new ArrayList<>();
    private ArrayList<String> StringarrayList = new ArrayList<>();
    private int mSelectedOcularHistoryIndex, mOcularId, mSelectedLaboratoryTestIndex,
            mLaboratoryId, mAppointmentId, mIndexCall = 0;
    private String chosenTask, mFilePath = "", mImagePath, mPatientId, mDoctorId, mTreatmentDate;
    private File destination, mPath;
    private Uri mImageUri;
    private Bitmap mBitmap;
    private RelativeLayout mRelativeNoDataFound;
    // Camera & Gallery Variables
    private int REQUEST_CAMERA = 101, REQUEST_GALLERY = 102, mCurrentPage = 1, mHistoryDoctorID = -1;
    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_investigation_add_container:
                    addInvestigationContainer();
                    break;
                case R.id.button_investigation_examination_submit:
                    SessionManager manager = new SessionManager(mActivity);
                    String closeFile = manager.getPreferences("ClosingFlags", "");

                    if (closeFile.equalsIgnoreCase("Yes")) {
                        mButtonSubmit.setClickable(false);
                        Toast.makeText(mActivity, "Patient File Closed", Toast.LENGTH_SHORT).show();
                    } else {
                        if (checkInvestigation()) {
                            callToAddInvestigationAPI();
                        }
                    }

                    break;
            }
        }
    };
    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinner_row_add_investigation_ocular_investigation:
                    mSelectedOcularHistoryIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedOcularHistoryIndex != 0) {
                        mOcularId = (Integer.parseInt(mArrOcularInvestigation.get(mSelectedOcularHistoryIndex - 1).getOcularInvestigationID()));
                        String mOcularHistoryName = (mArrOcularInvestigation.get(mSelectedOcularHistoryIndex - 1).getOcularinvestigation());
                        Common.insertLog("mOcularId::> " + mOcularId);
                        Common.insertLog("mOcularHistoryName::> " + mOcularHistoryName);
                    }
                    break;

                case R.id.spinner_row_add_investigation_laboratory_test:
                    mSelectedLaboratoryTestIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedLaboratoryTestIndex != 0) {
                        mLaboratoryId = (Integer.parseInt(mArrLaboratoryTest.get(mSelectedLaboratoryTestIndex - 1).getLaboratoryTestID()));
                        String mLaboratoryTestName = (mArrLaboratoryTest.get(mSelectedLaboratoryTestIndex - 1).getLaboratoryTest());
                        Common.insertLog("mLaboratoryId::> " + mLaboratoryId);
                        Common.insertLog("mLaboratoryTestName::> " + mLaboratoryTestName);
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_investigation, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrOcularInvestigation = new ArrayList<>();
        mArrLaboratoryTest = new ArrayList<>();
        mOcularInvestigation = new ArrayList<>();
        mLaboratoryTest = new ArrayList<>();
        mArrEye = new ArrayList<>();
        mArrEyeName = new ArrayList<>();
        mArrHistory = new ArrayList<>();

        getBundle();
        getIds();
        setRegListeners();
        setData();
        callToPreliminaryExaminationAPI();

        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mAppointmentId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mDoctorId = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);
                mTreatmentDate = bundle.getString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Linear Layouts
            mlinearInvestigationDynamicView = mView.findViewById(R.id.linear_investigation_container);

            // Image Views
            mImageContainer = mView.findViewById(R.id.image_investigation_add_container);

            // Buttons
            mButtonSubmit = mView.findViewById(R.id.button_investigation_examination_submit);

            mRelativeNoDataFound = mView.findViewById(R.id.relative_investigation_no_data_found);
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Set Register Listener
     */
    @SuppressLint("ClickableViewAccessibility")
    private void setRegListeners() {
        try {
            //TODO: set On Listener
            mImageContainer.setOnClickListener(clickListener);
            mButtonSubmit.setOnClickListener(clickListener);

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Data Value
     */
    private void setData() {
        String currentDate = Common.setCurrentDate(mActivity);
        String covertTreatmentDate = Common.convertDateUsingDateFormat(mActivity, mTreatmentDate,
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen),
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));

        if (currentDate.equalsIgnoreCase(covertTreatmentDate)) {
            mRelativeNoDataFound.setVisibility(View.GONE);
            //.setDataCompolains();
        } else {
            mRelativeNoDataFound.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Check Investigation A
     *
     * @return
     */
    private boolean checkInvestigation() {
        boolean status = true;
        try {
            if (mlinearInvestigationDynamicView.getChildCount() > 0) {
                for (int a = 0; a < mlinearInvestigationDynamicView.getChildCount(); a++) {
                    View view = mlinearInvestigationDynamicView.getChildAt(a);
                    // Spinners
                    Spinner mSpinnerEye = view.findViewById(R.id.spinner_row_add_investigation_eye);

                    LinearLayout mLinearOcularInvestigation =
                            view.findViewById(R.id.linear_row_add_investigation_item_ocular_investigation);
                    LinearLayout mLinearTest =
                            view.findViewById(R.id.linear_row_add_investigation_item_laboratory_test);

                    //Relative Layout
                    RelativeLayout mRelativeUploadImage = view.findViewById(R.id.relative_row_add_investigation_upload_picture);
                    int pos_eye = mSpinnerEye.getSelectedItemPosition();

                    if (pos_eye == 0) {
                        status = false;
                        Common.setCustomToast(mActivity, mActivity.getString(R.string.error_select_eye));
                    } else if (mLinearOcularInvestigation.getChildCount() == 0) {
                        status = false;
                        Common.setCustomToast(mActivity, mActivity.getString(R.string.error_select_ocular_investigation));

                    }
                }
            } else {
                status = false;
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
        return status;
    }

    /**
     * This method should call the Preliminary Examination API
     */
    private void callToPreliminaryExaminationAPI() {
        try {
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPreliminaryExaminationDetailsJson(mDatabaseName));

            Call<PreliminaryExaminationDetailsModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationDetails(body);
            call.enqueue(new Callback<PreliminaryExaminationDetailsModel>() {
                @Override
                public void onResponse(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Response<PreliminaryExaminationDetailsModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null) {

                                // Laboratory Test
                                if (response.body().getData().getLaboratoryTest() != null
                                        && response.body().getData().getLaboratoryTest().size() > 0) {
                                    mArrLaboratoryTest.addAll(response.body().getData().getLaboratoryTest());
                                }

                                // Ocular Investigation
                                if (response.body().getData().getOcularinvestigation() != null
                                        && response.body().getData().getOcularinvestigation().size() > 0) {
                                    mArrOcularInvestigation.addAll(response.body().getData().getOcularinvestigation());
                                }


                                // Eye
                                if (response.body().getData().getEye() != null
                                        && response.body().getData().getEye().size() > 0) {
                                    mArrEye.addAll(response.body().getData().getEye());
                                }

                                callToHistoryInvestigationAPI();
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                            callToHistoryInvestigationAPI();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    callToHistoryInvestigationAPI();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the view dynamically
     */
    private void addInvestigationContainer() {
        try {

            View view = LayoutInflater.from(mActivity.getBaseContext()).
                    inflate(R.layout.row_add_investigation_item, null, false);

            listView = view.findViewById(R.id.listView);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity, LinearLayoutManager.HORIZONTAL, false);
            listView.setLayoutManager(mLayoutManager);
            listView.setItemAnimator(new DefaultItemAnimator());

            MultiSpinnerSearch searchMultiSpinnerUnlimited = view.findViewById(R.id.spinner_row_add_investigation_ocular_investigation);
            MultiSpinnerSearch mSpinnerLaboratoryTest = view.findViewById(R.id.spinner_row_add_investigation_laboratory_test);

            Spinner mSpinnerEye = view.findViewById(R.id.spinner_row_add_investigation_eye);

            // Image Views
            ImageView mImageRemove = view.findViewById(R.id.image_row_add_investigation_remove);
            ImageView mImageUploadRemove = view.findViewById(R.id.image_row_add_investigation_item_image_remove);
            ImageView mImageUpload = view.findViewById(R.id.image_row_add_investigation_item_picture);

            //Linear Layout
            LinearLayout mLinearUploadImage = view.findViewById(R.id.linear_row_add_investigation_upload_picture);
            LinearLayout mLinearLabotry = view.findViewById(R.id.linear_row_add_investigation_item_laboratory_test);
            LinearLayout mLinearOcularInvestigation = view.findViewById(R.id.linear_row_add_investigation_item_ocular_investigation);
            LinearLayout mLinearOcular = view.findViewById(R.id.linear_row_add_investigation_ocular_investigation);


            //Relative Layout
            RelativeLayout mRelativeUploadImage = view.findViewById(R.id.relative_row_add_investigation_upload_picture);

            int position = mlinearInvestigationDynamicView.indexOfChild(view);

            // Binds the spinner view
            bindOcularInvestigationAdapter(searchMultiSpinnerUnlimited, mOcularInvestigation,
                    mLinearOcularInvestigation);
            bindLaboratoryTestAdapter(mSpinnerLaboratoryTest, position, mLinearLabotry);
            bindEyeAdapter(mSpinnerEye);


            // Set On Click Listener
            mLinearOcular.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //openDialog(mOcularInvestigation,mLinearOcularInvestigation,"Ocular Investigation");
                }
            });

            mImageRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mlinearInvestigationDynamicView.removeView(view);
                }
            });

            mLinearUploadImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mImageInvestigation = mImageUpload;
                    mRelativeUploadImagedata = mRelativeUploadImage;
                    mLinearUploadImageData = mLinearUploadImage;

//                    openMultipleImageUpload();
                    openImageUpload();
                }
            });


            mlinearInvestigationDynamicView.addView(view);

            mImageUploadRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mLinearUploadImage.setVisibility(View.VISIBLE);
                    mRelativeUploadImage.setVisibility(View.GONE);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void openMultipleImageUpload() {
    }

    /**
     * Open popup for camera and gallery
     */
    private void openImageUpload() {

        final CharSequence[] menus = {mActivity.getResources().getString(R.string.action_capture_image), mActivity.getResources().getString(R.string.action_select_from_gallery),
                mActivity.getResources().getString(R.string.action_cancel)};

        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        builder.setTitle(mActivity.getResources().getString(R.string.header_choose_image));
        builder.setItems(menus, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int pos) {

                boolean result = CameraGalleryPermission.checkPermission(mActivity,
                        AppPermissions.ReadWriteExternalStorageRequiredPermission());

                if (pos == 0) {
                    chosenTask = mActivity.getResources().getString(R.string.action_capture_image);
                    if (result) {
                        openCameraIntent();
                    }
                } else if (pos == 1) {
                    chosenTask = mActivity.getResources().getString(R.string.action_select_from_gallery);
                    if (result) {
                        openGalleryIntent();
                    }
                } else if (pos == 2) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**
     * Binds the ocular investigation spinner
     *
     * @param searchMultiSpinnerUnlimited - Spinner
     * @param mArrOcularInvestigation     - mArr Ocular Investigation
     * @param mLinearOcularInvestigation  - Linear Layout
     */
    private void bindOcularInvestigationAdapter(MultiSpinnerSearch searchMultiSpinnerUnlimited,
                                                ArrayList<CouselingPackageSuggestionModel>
                                                        mArrOcularInvestigation,
                                                LinearLayout mLinearOcularInvestigation) {
        try {

            if (mOcularInvestigation.size() > 0) {
                mOcularInvestigation.clear();
            }

            if (this.mArrOcularInvestigation != null && this.mArrOcularInvestigation.size() > 0) {

                for (int i = 0; i < this.mArrOcularInvestigation.size(); i++) {
                    CouselingPackageSuggestionModel model1 = new CouselingPackageSuggestionModel();
                    model1.setChecked(false);
                    model1.setOpetionSuggested(this.mArrOcularInvestigation.get(i).getOcularinvestigation());
                    model1.setOpetionSuggestedID(this.mArrOcularInvestigation.get(i).getOcularInvestigationID());
                    mOcularInvestigation.add(model1);
                }
            }


            if (mLinearOcularInvestigation.getChildCount() > 0) {
                for (int oc = 0; oc < mLinearOcularInvestigation.getChildCount(); oc++) {
                    View view_container = LayoutInflater.from(mActivity).inflate(R.layout.
                                    row_add_preliminary_examination_compliance_item,
                            null, false);

                    TextView mTextName = view_container.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);

                    String text = mTextName.getText().toString();

                    if (mOcularInvestigation.size() > 0) {
                        for (int h = 0; h < mOcularInvestigation.size(); h++) {

                            if (mOcularInvestigation.get(h).getOpetionSuggested()
                                    .equals(text)) {
                                mOcularInvestigation.get(h).setChecked(true);
                            }

                        }
                    }

                }
            }

            searchMultiSpinnerUnlimited.setEmptyTitle("Not Data Found!");
            searchMultiSpinnerUnlimited.setSearchHint("Find Data");
            searchMultiSpinnerUnlimited.setHeader(getResources().getString(R.string.text_ocular_investigation));

            searchMultiSpinnerUnlimited.setItems(mOcularInvestigation,
                    -1,
                    getResources().getString(R.string.spinner_select_ocular_investigation),
                    new SpinnerListener() {

                        @Override
                        public void onItemsSelected(List<CouselingPackageSuggestionModel> items) {


                            UpdateLayout(mOcularInvestigation, mLinearOcularInvestigation);
                        }
                    });

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Update Layout
     *
     * @param mOcularInvestigation
     * @param mLinearOcularInvestigation
     */
    private void UpdateLayout(ArrayList<CouselingPackageSuggestionModel> mOcularInvestigation, LinearLayout mLinearOcularInvestigation) {


        if (mOcularInvestigation.size() > 0) {
            for (int a = 0; a < mOcularInvestigation.size(); a++) {

                if (mOcularInvestigation.get(a).getChecked()
                        && !mOcularInvestigation.get(a).getOpetionSuggested().
                        equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {

                    addItems(mLinearOcularInvestigation, mOcularInvestigation.get(a));
                }
            }
        }
    }

    /**
     * Add Item Linear
     *
     * @param mLinearOcularInvestigation
     * @param couselingPackageSuggestionModel -Package Suggested
     */
    private void addItems(LinearLayout mLinearOcularInvestigation,
                          CouselingPackageSuggestionModel couselingPackageSuggestionModel) {

        @SuppressLint("InflateParams")
        View view_container = LayoutInflater.from(mActivity).inflate(R.layout.
                        row_add_preliminary_examination_compliance_item,
                null, false);

        TextView mTextName = view_container.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
        ImageView mImageDelete = view_container.findViewById(R.id.image_row_add_preliminary_examination_compliance_delete);
        mImageDelete.setColorFilter(Common.setThemeColor(mActivity));

        mTextName.setText(couselingPackageSuggestionModel.getOpetionSuggested());

        mImageDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLinearOcularInvestigation.removeView(view_container);
                couselingPackageSuggestionModel.setChecked(false);
            }
        });

        mLinearOcularInvestigation.addView(view_container);
    }

    /**
     * Binds the laboratory test spinner
     *
     * @param mSpinner       - Spinner Laboratory Test
     * @param position
     * @param mLinearLabotry
     */
    private void bindLaboratoryTestAdapter(MultiSpinnerSearch mSpinner,
                                           int position, LinearLayout mLinearLabotry) {
        try {


            if (mArrLaboratoryTest != null && mArrLaboratoryTest.size() > 0) {

                /*CouselingPackageSuggestionModel model=new CouselingPackageSuggestionModel();
                model.setChecked(false);
                model.setOpetionSuggested(mActivity.getResources().getString(R.string.spinner_select_laboratory_test));
                model.setOpetionSuggestedID("-1");
                mLaboratoryTest.add(model);*/

                if (mLaboratoryTest.size() > 0) {
                    mLaboratoryTest.clear();
                }


                for (int i = 0; i < mArrLaboratoryTest.size(); i++) {
                    CouselingPackageSuggestionModel model1 = new CouselingPackageSuggestionModel();
                    model1.setChecked(false);
                    model1.setOpetionSuggested(mArrLaboratoryTest.get(i).getLaboratoryTest());
                    model1.setOpetionSuggestedID(mArrLaboratoryTest.get(i).getLaboratoryTestID());
                    mLaboratoryTest.add(model1);
                }
            }


            if (mLinearLabotry.getChildCount() > 0) {
                for (int oc = 0; oc < mLinearLabotry.getChildCount(); oc++) {
                    View view_container = LayoutInflater.from(mActivity).inflate(R.layout.
                                    row_add_preliminary_examination_compliance_item,
                            null, false);

                    TextView mTextName = view_container.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);

                    String text = mTextName.getText().toString();

                    if (mLaboratoryTest.size() > 0) {
                        for (int h = 0; h < mLaboratoryTest.size(); h++) {

                            if (mLaboratoryTest.get(h).getOpetionSuggested()
                                    .equals(text)) {
                                mLaboratoryTest.get(h).setChecked(true);
                            }

                        }
                    }

                }
            }

            //Serarch Spinner with Multiple Select
            mSpinner.setEmptyTitle("Not Data Found!");
            mSpinner.setSearchHint("Find Data");
            mSpinner.setHeader(getResources().getString(R.string.text_laboratory_test));

            mSpinner.setItems(mLaboratoryTest,
                    -1, getResources().getString(R.string.spinner_select_laboratory_test), new SpinnerListener() {

                        @Override
                        public void onItemsSelected(List<CouselingPackageSuggestionModel> items) {


                            UpdateLayout(mLaboratoryTest, mLinearLabotry);
                        }
                    });

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }


    /**
     * Binds the Eye test spinner
     *
     * @param mSpinner - Spinner Eye
     */
    private void bindEyeAdapter(Spinner mSpinner) {
        try {
            if (mArrEyeName.size() > 0) {
                mArrEyeName.clear();
            }
            if (mArrEye != null && mArrEye.size() > 0) {

                mArrEyeName.add(mActivity.getResources().getString(R.string.spinner_select_eye));

                for (int i = 0; i < mArrEye.size(); i++) {
                    mArrEyeName.add(mArrEye.get(i).getEye());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>
                        (mActivity, android.R.layout.simple_selectable_list_item, mArrEyeName);

                mSpinner.setAdapter(adapter);
            }
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * OpenDialog for closing patient file
     */
    public void openFileClosingAlert() {


        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(
                mActivity);
        builder.setTitle("Close File");
        builder.setMessage("Are you sure you want to close file?");
        builder.setNegativeButton("NO",
                (dialog, which) -> dialog.dismiss());
        builder.setPositiveButton("YES",
                (dialog, which) -> closeFile());
        builder.show();

    }

    private void closeFile() {
        try {
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.closeFileJson(mAppointmentId, mDatabaseName));

            Call<CloseFileModel> call = RetrofitClient.createService(ApiInterface.class).closeFileAPI(body);
            call.enqueue(new Callback<CloseFileModel>() {
                @Override
                public void onResponse(@NonNull Call<CloseFileModel> call, @NonNull Response<CloseFileModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        int mError = response.body().getError();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                Common.insertLog("IF");
                                Common.setCustomToast(mActivity, mMessage);
                            } else {
                                Common.insertLog("ELSE");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CloseFileModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open Dialog Investigation Some Data
     */
    public void openDialogDiagnosis() {
        try {
            Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.custom_dialog_history_preliminary);

            TextView mTextTitle = dialog.findViewById(R.id.text_view_custom_dialog_history_preliminary);
            RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
            ImageView mImageClose = dialog.findViewById(R.id.image_custom_dialog_history_preliminary_close);

            RelativeLayout noInternetConnection = dialog.findViewById(R.id.relative_no_internet);
            RelativeLayout noDataFound = dialog.findViewById(R.id.relative_no_data_available);

            mTextTitle.setText(mActivity.getResources().getString(R.string.tab_investigation));

            mImageClose.setColorFilter(Common.setThemeColor(mActivity));

            mImageClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            LinearLayoutManager manager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL,
                    false);
            recyclerView.setLayoutManager(manager);

            HistoryInvestigationAdapter adapter = new HistoryInvestigationAdapter(mActivity, recyclerView, mArrHistory) {
                @Override
                protected void onSelectedItem(Activity activity, HistoryPreliminaryExaminationModel.Data data) {
                    super.onSelectedItem(activity, data);
                    dialog.dismiss();
                    mRelativeNoDataFound.setVisibility(View.GONE);
                    if (mlinearInvestigationDynamicView.getChildCount() > 0) {
                        mlinearInvestigationDynamicView.removeAllViews();
                    }

                    if (data.getInvestigation().size() > 0) {

                        for (int a = 0; a < data.getInvestigation().size(); a++) {
                            setEditInvestigation(data.getAppointmentID(), data.getInvestigation().get(a));
                        }
                    }
                }
            };

            recyclerView.setAdapter(adapter);

            if (mArrHistory.size() > 0) {
                recyclerView.setVisibility(View.VISIBLE);
                noDataFound.setVisibility(View.GONE);
            } else {
                recyclerView.setVisibility(View.GONE);
                noDataFound.setVisibility(View.VISIBLE);
            }

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Edit Mode Open Investigation
     *
     * @param appointmentID
     * @param investigation
     */
    public void setEditInvestigation(String appointmentID, HistoryPreliminaryExaminationModel.Investigation investigation) {
        try {

            View view = LayoutInflater.from(mActivity.getBaseContext()).
                    inflate(R.layout.row_add_investigation_item, null, false);

            MultiSpinnerSearch searchMultiSpinnerUnlimited = view.findViewById(R.id.spinner_row_add_investigation_ocular_investigation);
            MultiSpinnerSearch mSpinnerLaboratoryTest = view.findViewById(R.id.spinner_row_add_investigation_laboratory_test);

            Spinner mSpinnerEye = view.findViewById(R.id.spinner_row_add_investigation_eye);

            // Image Views
            ImageView mImageRemove = view.findViewById(R.id.image_row_add_investigation_remove);
            ImageView mImageUploadRemove = view.findViewById(R.id.image_row_add_investigation_item_image_remove);
            ImageView mImageUpload = view.findViewById(R.id.image_row_add_investigation_item_picture);

            //Linear Layout
            LinearLayout mLinearUploadImage = view.findViewById(R.id.linear_row_add_investigation_upload_picture);
            LinearLayout mLinearLabotry = view.findViewById(R.id.linear_row_add_investigation_item_laboratory_test);
            LinearLayout mLinearOcularInvestigation = view.findViewById(R.id.linear_row_add_investigation_item_ocular_investigation);
            LinearLayout mLinearOcular = view.findViewById(R.id.linear_row_add_investigation_ocular_investigation);


            //Relative Layout
            RelativeLayout mRelativeUploadImage = view.findViewById(R.id.relative_row_add_investigation_upload_picture);

            //int position = mlinearInvestigationDynamicView.indexOfChild(view);


            //Linear Layout
            String[] arrayOccular = investigation.getOcularinvestigation().split(",");
            String[] arrayLaboretyTest = investigation.getLaboratoryTest().split(",");

            if (arrayOccular.length > 0) {
                for (String ocular : arrayOccular) {
                    AddLayoutString(ocular, mLinearOcularInvestigation);
                }
            }

            if (arrayLaboretyTest.length > 0) {
                for (String test : arrayLaboretyTest) {
                    AddLayoutString(test, mLinearLabotry);
                }
            }

            // Binds the spinner view
            bindOcularInvestigationAdapter(searchMultiSpinnerUnlimited, mOcularInvestigation,
                    mLinearOcularInvestigation);
            bindLaboratoryTestAdapter(mSpinnerLaboratoryTest, 0, mLinearLabotry);
            bindEyeAdapter(mSpinnerEye);


            if (!investigation.getImage().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                mRelativeUploadImage.setVisibility(View.VISIBLE);
                mLinearUploadImage.setVisibility(View.GONE);
                setGlideImageView(mImageUpload, investigation.getImage(), appointmentID);
            } else {
                mRelativeUploadImage.setVisibility(View.GONE);
                mLinearUploadImage.setVisibility(View.VISIBLE);
            }

            // already Selected Eye.

            String eyeid = investigation.getEyeID();
            if (mArrEye.size() > 0) {
                for (int a = 0; a < mArrEye.size(); a++) {
                    if (mArrEye.get(a).getEyeID().equalsIgnoreCase(eyeid)) {
                        int pos = a + 1;
                        mSpinnerEye.setSelection(pos);
                    }
                }
            }
            //TODO: Set OnClick Listener
            mImageRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mlinearInvestigationDynamicView.removeView(view);
                }
            });

            mLinearUploadImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mImageInvestigation = mImageUpload;
                    mRelativeUploadImagedata = mRelativeUploadImage;
                    mLinearUploadImageData = mLinearUploadImage;

                    openImageUpload();
                }
            });

            mlinearInvestigationDynamicView.addView(view);

            mImageUploadRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    mLinearUploadImage.setVisibility(View.VISIBLE);
                    mRelativeUploadImage.setVisibility(View.GONE);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Call To API for Add Investigation
     */
    private void callToAddInvestigationAPI() {
        try {
            if (mlinearInvestigationDynamicView.getChildCount() > 0) {

                //Show Dialog
                Common.showLoadingDialog(mActivity, mActivity.getString(R.string.text_load));
                mIndexCall = 0;
                for (int a = 0; a < mlinearInvestigationDynamicView.getChildCount(); a++) {
                    View view = mlinearInvestigationDynamicView.getChildAt(a);

                    Spinner mSpinnerEye = view.findViewById(R.id.spinner_row_add_investigation_eye);
                    LinearLayout mLinearOcularInvestigation =
                            view.findViewById(R.id.linear_row_add_investigation_item_ocular_investigation);

                    LinearLayout mLinearLaboretry =
                            view.findViewById(R.id.linear_row_add_investigation_item_laboratory_test);


                    ImageView mImageUpload = view.findViewById(R.id.image_row_add_investigation_item_picture);

                    //Linear Layout
                    LinearLayout mLinearUploadImage = view.findViewById(R.id.linear_row_add_investigation_upload_picture);

                    //Relative Layout
                    RelativeLayout mRelativeUploadImage = view.findViewById(R.id.relative_row_add_investigation_upload_picture);

                    int pos_eye = mSpinnerEye.getSelectedItemPosition() - 1;

                    String eye_id = mArrEye.get(pos_eye).getEyeID();


                    String laboratory_test = AppConstants.STR_EMPTY_STRING;
                    String ocular_investigation = AppConstants.STR_EMPTY_STRING;


                    //get Ocular Investigation
                    if (mLinearOcularInvestigation.getChildCount() > 0) {
                        for (int j = 0; j < mLinearOcularInvestigation.getChildCount(); j++) {
                            View view_laborety_test = mLinearOcularInvestigation.getChildAt(j);

                            TextView data =
                                    view_laborety_test.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);

                            String test = data.getText().toString();
                            if (ocular_investigation.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                                ocular_investigation = test;
                            } else {
                                ocular_investigation = ocular_investigation + "," + test;
                            }
                        }
                    }

                    //get Ocular Laborety Test
                    if (mLinearLaboretry.getChildCount() > 0) {
                        for (int j = 0; j < mLinearLaboretry.getChildCount(); j++) {
                            View view_laborety_test = mLinearLaboretry.getChildAt(j);

                            TextView data =
                                    view_laborety_test.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);

                            String test = data.getText().toString();
                            if (laboratory_test.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                                laboratory_test = test;
                            } else {
                                laboratory_test = laboratory_test + "," + test;
                            }
                        }
                    }

                    callPreliminaryInvestigationAPI(eye_id, ocular_investigation, laboratory_test, mImageUpload, mRelativeUploadImage);
                }
            }

        } catch (Exception e) {

            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Open the camera intent
     */
    private void openCameraIntent() {
        destination = AppUtil.currentTimeStampFile();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
        } else {
            File file = new File(Uri.fromFile(destination).getPath());
            Uri photoUri = FileProvider.getUriForFile(mActivity, mActivity.getPackageName() + ".provider", file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        }
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        if (intent.resolveActivity(mActivity.getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    /**
     * Open the gallery intent
     */
    private void openGalleryIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Show only images, no videos or anything else
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_GALLERY);
    }

    /**
     * Get Activity Result
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA) {
                CropImage.activity(Uri.fromFile(destination)).start(mActivity, this);
            } else if (requestCode == REQUEST_GALLERY) {

                mImageUri = data.getData();

                CropImage.activity(mImageUri).setMinCropResultSize(1800, 1800)
                        .setMaxCropResultSize(1800, 1800).start(mActivity, this);

                mFilePath = Common.getRealPathFromURI(mImageUri, mActivity);
                if (mFilePath == null)
                    mFilePath = mImageUri.getPath(); // from File Manager

                if (mFilePath != null) {
                    File mFile = new File(mFilePath);
                    mBitmap = Common.decodeFile(mFile, AppConstants.PIC_WIDTH,
                            AppConstants.PIC_HEIGHT);
                    mPath = mFile;
                }

            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                mImageUri = result.getUri();
                onSelectFromGalleryResult(mImageUri);
            }
        }
    }

//    private void convertUriToString() {
//        Bitmap bm = null;
//
//        for (int i = 0; i < arrayList.size(); i++) {
//            if (arrayList.get(i) != null) {
//
//                try {
//                    File file = new File(arrayList.get(i).getPath());
//                    mFilePath = Common.getRealPathFromURI(arrayList.get(i), mActivity);
////                    imageName = file.getName();
//                    imageName1 = FileUtils.getPath(mActivity, arrayList.get(i));
//                    mImagePath = arrayList.get(i).getPath();
//                    Compressor compressor = new Compressor(mActivity);
//                    if (ImageUtil.convertURItoFile(mActivity, arrayList.get(i)) != null)
//                        bm = compressor.compressToBitmap(ImageUtil.convertURItoFile(mActivity, arrayList.get(i)));
//
//                    StringarrayList.add(imageName1);
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//    }

    /**
     * Sets the cropped image from the gallery or camera intent
     */
    private void onSelectFromGalleryResult(Uri dataUri) {
        Bitmap bm = null;
        String imageName = null;

        if (dataUri != null) {
            try {
                //  bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), dataUri);
                File file = new File(dataUri.getPath());
                imageName = file.getName();
                mImagePath = dataUri.getPath();
                Compressor compressor = new Compressor(mActivity);
                if (ImageUtil.convertURItoFile(mActivity, dataUri) != null)
                    bm = compressor.compressToBitmap(ImageUtil.convertURItoFile(mActivity, dataUri));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        if (bm != null) {
            bm.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        }
        mImageInvestigation.setImageBitmap(bm);
        mImageInvestigation.setTag(mImagePath);

        mLinearUploadImageData.setVisibility(View.GONE);
        mRelativeUploadImagedata.setVisibility(View.VISIBLE);
    }

    private void callPreliminaryInvestigationAPI(String eye_id, String ocular_investigation, String laboratory_test, ImageView mImageUpload, RelativeLayout RealtiveImageUpload) {
        try {
            int Size = mlinearInvestigationDynamicView.getChildCount();

            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);

            String mDatabase = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            String DefualtImage = "file:///android_asset/no_image_display_pic.png";
            File file = new File((mImagePath != null && !mImagePath.equalsIgnoreCase("")) ? mImagePath : DefualtImage);
            final RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);

            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part body =
                    MultipartBody.Part.createFormData(WebFields.ADD_INVESTIGATION.REQUEST_IMAGE_DATA_1, file.getName(), requestFile);

            // create list of file parts (photo, video, ...)
//            List<MultipartBody.Part> parts = new ArrayList<>();
//
//            if (arrayList != null) {
//                // create part for file (photo, video, ...)
//                for (int i = 0; i < arrayList.size(); i++) {
//                    parts.add(prepareFilePart("image"+i, arrayList.get(i)));
//                }
//            }

            RequestBody method = RequestBody.create(MediaType.parse("text/plain"), WebFields.ADD_INVESTIGATION.MODE);

            RequestBody mPreliminaryID = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(AddNewPreliminaryExaminationFragment.PreliminaryExminationId));
            RequestBody mUserID = RequestBody.create(MediaType.parse("text/plain"), mUserId);
            RequestBody mOcularInvestigation = RequestBody.create(MediaType.parse("text/plain"), ocular_investigation);
            RequestBody mLaboratoryTest = RequestBody.create(MediaType.parse("text/plain"), laboratory_test);
            RequestBody mEyeID = RequestBody.create(MediaType.parse("text/plain"), eye_id);
            RequestBody mAppointmentID = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mAppointmentId));

            RequestBody mDatabaseName = RequestBody.create(MediaType.parse("text/plain"), mDatabase);
            //Set Count Call
            this.mIndexCall = mIndexCall + 1;
            Call<AddDoodleImageModel> callRepos = null;
            if ((mImagePath != null && !mImagePath.equalsIgnoreCase(""))) {
                callRepos = new RetrofitClient().createService(ApiInterface.class)
                        .AddInvestigationWithImage(method, mUserID, mPreliminaryID, mOcularInvestigation, mLaboratoryTest,
                                mEyeID, mDatabaseName, mAppointmentID, body);
            }else {
                callRepos = new RetrofitClient().createService(ApiInterface.class)
                        .AddInvestigationWithImage(method, mUserID, mPreliminaryID, mOcularInvestigation, mLaboratoryTest,
                                mEyeID, mDatabaseName, mAppointmentID, null);
            }


            callRepos.enqueue(new Callback<AddDoodleImageModel>() {
                @Override
                public void onResponse(@NonNull Call<AddDoodleImageModel> call, @NonNull Response<AddDoodleImageModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        StringarrayList.clear();
                        arrayList.clear();
                        int mError = response.body().getError();
                        String message = response.body().getMessage();

                        Common.hideDialog();
                        if (response.isSuccessful() && mError == 200) {


                            if (Size == mIndexCall) {
                                Common.hideDialog();

                                if (mArrHistory.size() > 0) {
                                    mArrHistory.clear();

                                }
                                callToHistoryInvestigationAPI();
                                callToNextPage();
                            }

                        } else {
                            Common.hideDialog();
                            Common.setCustomToast(mActivity, message);
                        }
                    } catch (Exception e) {
                        Common.hideDialog();
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddDoodleImageModel> call, @NonNull Throwable t) {
                    Common.insertLog("Add UploadPics error " + t.getMessage());
                    Common.hideDialog();

                }
            });
        } catch (Exception e) {
            Common.hideDialog();
            Common.insertLog(e.getMessage());
        }
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
        // use the FileUtils to get the actual file by uri
        File file = FileUtil.getFile(mActivity, fileUri);

        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse(FileUtil.MIME_TYPE_IMAGE), file);

        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    /**
     * Set Call To Next Screen
     */
    private void callToNextPage() {
        AddNewPreliminaryExaminationFragment.changePage(6);
    }


    /**
     * get History Investigation
     */
    private void callToHistoryInvestigationAPI() {
        try {

            String defualt_date = "1000-01-01";
            String Mobile = AppConstants.STR_EMPTY_STRING;
            String Type = "Investigation";

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setHistoryPreliminaryExaminationJson(defualt_date, mCurrentPage, mPatientId, Mobile, mHistoryDoctorID, Type, hospital_database));

            Call<HistoryPreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationHistory(body);
            call.enqueue(new Callback<HistoryPreliminaryExaminationModel>() {
                @Override
                public void onResponse(Call<HistoryPreliminaryExaminationModel> call, Response<HistoryPreliminaryExaminationModel> response) {
                    Common.insertLog("Response History::::> " + new Gson().toJson(response.body()));
                    try {
                        if (response.body() != null &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body().getData().size() > 0) {

                                mArrHistory.addAll(response.body().getData());
//                                checkTodayInvestigation();
                            }
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<HistoryPreliminaryExaminationModel> call, Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkTodayInvestigation() {

        try {
            String current_date = Common.setCurrentDate(mActivity);
            String convert_current_date = Common.setConvertDateFormat(mActivity,
                    current_date, mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen));

            if (mArrHistory.size() > 0) {

                if (mlinearInvestigationDynamicView.getChildCount() > 0) {
                    mlinearInvestigationDynamicView.removeAllViews();
                }

                for (int a = 0; a < mArrHistory.size(); a++) {
                    if (mArrHistory.get(a).getTreatmentDate().equalsIgnoreCase(convert_current_date)) {
                        if (mArrHistory.get(a).getAppointmentID()
                                .equalsIgnoreCase(String.valueOf(mAppointmentId))) {
                            for (int j = 0; j < mArrHistory.get(a).getInvestigation().size(); j++) {
                                setEditInvestigation(mArrHistory.get(a).getAppointmentID(), mArrHistory.get(a).getInvestigation().get(j));
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }


    /**
     * This should set the image view using glide
     */
    private void setGlideImageView(ImageView image, String imageName, String AppointmentID) {
        try {
            String mBaseURL = WebFields.API_BASE_URL;
            String mFolderName = WebFields.IMAGE_BASE_FOLDER;
            String mHospitalCode = GetJsonData.getHospitalData(mActivity, WebFields.PASS_CODE.RESPONSE_HOSPITAL_CODE);

            // http://societyfy.in/Track_OPD/assets/uploads/12356/34/Investigation/1568020896_34.jpg
            String url = mBaseURL + mFolderName + mHospitalCode + AppConstants.STR_FORWARD_SLASH + AppointmentID + AppConstants.STR_FORWARD_SLASH +
                    "Investigation" + AppConstants.STR_FORWARD_SLASH + imageName;

            Common.insertLog("Image url " + url);

            Glide.with(mActivity)
                    .load(url)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .placeholder(R.drawable.upload_icon)
                    .into(image);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void AddLayoutString(String text, LinearLayout mLinearLayout) {

        if (!text.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            @SuppressLint("InflateParams")
            View view_container = LayoutInflater.from(mActivity).inflate(R.layout.
                            row_add_preliminary_examination_compliance_item,
                    null, false);

            TextView mTextName = view_container.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
            ImageView mImageDelete = view_container.findViewById(R.id.image_row_add_preliminary_examination_compliance_delete);
            mImageDelete.setColorFilter(Common.setThemeColor(mActivity));

            mTextName.setText(text);

            mImageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLinearLayout.removeView(view_container);
                }
            });


            mLinearLayout.addView(view_container);
        }
    }
}
