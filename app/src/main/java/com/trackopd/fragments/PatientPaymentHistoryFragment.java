package com.trackopd.fragments;


import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.ReceptionistPatientDetailPaymentAdapter;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.PreliminaryExaminationModel;
import com.trackopd.model.PrintReceiptModel;
import com.trackopd.model.ReceptionistPaymentModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PatientPaymentHistoryFragment extends Fragment {

    private ArrayList<ReceptionistPaymentModel> mArrReceptionistPayment;
    private View mView;
    private Activity mActivity;
    private RecyclerView recyclerView;
    private RelativeLayout mRelativeNoData;
    private int mAppointmentId, mCurrentPage = 1, mHistoryDoctorID = -1;
    private ArrayList<PreliminaryExaminationModel> mArrPreliminaryExamination;
    private String mPatientFirstName, mPatientLastName, mPatientMobile, mPatientId, mDoctorId, mTitle, mPatientCode ,mAppointmentDate, AppointmentId;
    private String mUserType, hospital_database;

    HistoryPreliminaryExaminationModel.Data data;

    private boolean mHistoryAndComplaints = false, mVisionUCVA = false, mVisionBCVAUndilated = false, mVisionFinal = false,
            mVisionBCVADilated = false, mPrimaryExamination = false, mDiagnosis = false, mInvestigationSuggested = false,
            mTreatmentSuggested = false, mCounselingDetails = false, mSurgeryDetails = false, mPrescription = false, mPayment = true;

    String PreExaminationID = null;

    public PatientPaymentHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_patient_payment_history, container, false);

        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrPreliminaryExamination = new ArrayList<>();
        mArrReceptionistPayment = new ArrayList<>();

        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);
        hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

        getBundle();
        getIds();
        callPaymentListAPI();
        callToAddPreliminaryExaminationFragment(data);
        setHasOptionsMenu(true);

        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = getArguments();
            if (bundle != null) {
                mPatientFirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mPatientLastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mPatientMobile = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_USER_ID);
                mAppointmentId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mTitle = bundle.getString(AppConstants.STR_TITLE);
                mDoctorId = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIds() {
        try {
            // Recycler View
            recyclerView = mView.findViewById(R.id.recycler_view);

            // Relative Layouts
            mRelativeNoData = mView.findViewById(R.id.relative_no_data_available);

            LinearLayoutManager manager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(manager);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the payment listing for receptionist
     */
    private void callPaymentListAPI() {
        try {
            if (mPatientFirstName == null) {
                mPatientFirstName = AppConstants.STR_EMPTY_STRING;
            }

            if (mPatientCode == null) {
                mPatientCode = AppConstants.STR_EMPTY_STRING;
            }

            if (mPatientMobile == null) {
                mPatientMobile = AppConstants.STR_EMPTY_STRING;
            }

            if (AppointmentId == null) {
                AppointmentId = AppConstants.STR_EMPTY_STRING;
            }

            if (mAppointmentDate == null) {
                mAppointmentDate = mActivity.getResources().getString(R.string.date_format_first_time);
            }

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            int currentPageIndex = 1;

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setReceptionistPaymentListJson(currentPageIndex, mPatientFirstName, mPatientCode, mPatientMobile, String.valueOf(mAppointmentId), mAppointmentDate, hospital_database));

            Call<ReceptionistPaymentModel> call = RetrofitClient.createService(ApiInterface.class).getReceptionistPaymentList(requestBody);
            call.enqueue(new Callback<ReceptionistPaymentModel>() {
                @Override
                public void onResponse(@NonNull Call<ReceptionistPaymentModel> call, @NonNull Response<ReceptionistPaymentModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {

                        if (response.body().getError() == AppConstants.API_SUCCESS_ERROR) {

                            mArrReceptionistPayment.addAll(response.body().getData());
                            setAdapterInvestigation();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ReceptionistPaymentModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setAdapterInvestigation() {

        ReceptionistPatientDetailPaymentAdapter adapter = new ReceptionistPatientDetailPaymentAdapter(mActivity, recyclerView,
                mArrReceptionistPayment,false) {
            @Override
            protected void onSelectedItem(Activity mActivity, ReceptionistPaymentModel data) {
                super.onSelectedItem(mActivity, data);
            }

            @Override
            protected void getPaymentPrint(String AppointmentID) {
                super.getPaymentPrint(AppointmentID);
                getPrint(AppointmentID);
            }
        };
        recyclerView.setAdapter(adapter);
        setVisibility();
    }

    private void getPrint(String appointmentID) {
        try {


            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPrintReceiptJson(AddNewPreliminaryExaminationFragment.PreliminaryExminationId,
                            appointmentID, mHistoryAndComplaints, mVisionUCVA, mVisionBCVAUndilated,
                            mVisionFinal, mVisionBCVADilated, mPrimaryExamination, mDiagnosis, mInvestigationSuggested,
                            mTreatmentSuggested, mCounselingDetails, mSurgeryDetails, mPrescription,"",false, mPayment, hospital_database));

            Call<PrintReceiptModel> call = RetrofitClient.createService(ApiInterface.class).PrintReceipt(body);
            call.enqueue(new Callback<PrintReceiptModel>() {
                @Override
                public void onResponse(@NonNull Call<PrintReceiptModel> call, @NonNull Response<PrintReceiptModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body() != null) {
                                String url = response.body().getData();
                                callToReceiptPrint(url);
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PrintReceiptModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Call To Receipt Print
     *
     * @param url - String Url Print
     */
    private void callToReceiptPrint(String url) {
        /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);*/

        // String pdfOpen = AppConstants.PDF_OPEN + url;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mActivity.startActivity(browserIntent);
    }


    public void setVisibility() {
        if (mArrReceptionistPayment.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
            mRelativeNoData.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.GONE);
            mRelativeNoData.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Sets up the Optometrist Add New Preliminary Examination Fragment
     *
     * @param data Receptionist Patients Model
     */
    private void callToAddPreliminaryExaminationFragment(HistoryPreliminaryExaminationModel.Data data) {

        try {
            Bundle mBundle = new Bundle();

            PreExaminationID = data.getPreliminaryExaminationID();

            mBundle.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, data.getPatientFirstName());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, data.getPatientLastName());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE, data.getPatientMobileNo());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_PROFILE, "");
            mBundle.putString(AppConstants.BUNDLE_PATIENT_ID, data.getPatientID());
            mBundle.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, Integer.parseInt(data.getAppointmentID()));
            mBundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, data.getDoctorID());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE, data.getTreatmentDate());
            mBundle.putString(AppConstants.BUNDLE_PRELIMINARY_EXAMINATION_ID, data.getPreliminaryExaminationID());
            mBundle.putString(AppConstants.BUNDLE_CHECK_TYPE, mActivity.getString(R.string.tab_vision));

            Fragment fragment = new AddNewPreliminaryExaminationFragment(data);
            fragment.setArguments(mBundle);


            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager =
                        ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager =
                        ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager =
                        ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager =
                        ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_preliminary_examination))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_preliminary_examination))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
