package com.trackopd.fragments;


import android.app.Activity;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.ViewPagerAdapter;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;



/**
 * A simple {@link Fragment} subclass.
 */
public class AddBillingFragment extends Fragment {

    private View view;
    private Activity mActivity;
    private ViewPager mViewPager;

    public AddBillingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view=inflater.inflate(R.layout.fragment_add_billing, container, false);
        mActivity=getActivity();

        getIds();
        setViewPager();
        return view;
    }

    /**
     *  get Declare Id on
     */
    private void getIds() {
        try{
          mViewPager=view.findViewById(R.id.view_pager_add_billing_fragment);

        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * set View Pager
     */
    private void setViewPager() {
       try{
           ViewPagerAdapter adapter = new ViewPagerAdapter(((ReceptionistHomeActivity) mActivity).getSupportFragmentManager());
           adapter.addFragment(new AddServiceAndPackage(), AppConstants.STR_EMPTY_STRING);
           adapter.addFragment(new BillingSummaryFragment(), AppConstants.STR_EMPTY_STRING);
           mViewPager.setAdapter(adapter);
       } catch (Exception e){
          Common.insertLog(e.getMessage());
       }
    }
}
