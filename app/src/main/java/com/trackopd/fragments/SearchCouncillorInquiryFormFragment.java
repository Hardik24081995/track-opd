package com.trackopd.fragments;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.model.CouncillorModel;
import com.trackopd.model.DoctorModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchCouncillorInquiryFormFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private EditText mEditPatientName, mEditMobileNo, mEditCouncillorDate;
    private Button mButtonSearch;
    private Spinner mSpinnerCouncillor;
    private ImageView mImageCouncillor;
    private ArrayList<String> mArrCouncillor;
    private ArrayList<CouncillorModel> mArrCouncillorList;
    private int mSelectedCouncillorIndex, mCouncillorId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_search_councillor_inquiry_form, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrCouncillor = new ArrayList<>();
        mArrCouncillorList = new ArrayList<>();

        getIds();
        setData();
        setRegListeners();
        callToCouncillorAPI();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Set the prepare menu options
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Edit Texts
            mEditPatientName = mView.findViewById(R.id.edit_search_councillor_inquiry_form_patient_name);
            mEditMobileNo = mView.findViewById(R.id.edit_search_councillor_inquiry_form_patient_mobile_no);
            mEditCouncillorDate = mView.findViewById(R.id.edit_search_councillor_inquiry_form_councillor_date);

            // Image Views
            mImageCouncillor = mView.findViewById(R.id.image_search_councillor_inquiry_form_councillor);

            // Spinners
            mSpinnerCouncillor = mView.findViewById(R.id.spinner_search_councillor_inquiry_form_councillor);

            // Buttons
            mButtonSearch = mView.findViewById(R.id.button_search_councillor_inquiry_form_search);

            // Set Request Focus
            mEditPatientName.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            // Set Image Color as per the theme applied
            mImageCouncillor.setColorFilter(Common.setThemeColor(mActivity));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Click Listeners
            mButtonSearch.setOnClickListener(clickListener);
            mEditCouncillorDate.setOnClickListener(clickListener);

            // ToDo: Doctor - Spinner Item Select Listener
            mSpinnerCouncillor.setOnItemSelectedListener(onItemSelectedListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.button_search_councillor_inquiry_form_search:
                    callToCouncillorInquiryFormFragment();
                    break;

                case R.id.edit_search_councillor_inquiry_form_councillor_date:
                    Common.openDatePickerForAllDates(mActivity, mEditCouncillorDate);
                    break;
            }
        }
    };

    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {
                case R.id.spinner_search_councillor_inquiry_form_councillor:
                    mSelectedCouncillorIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedCouncillorIndex != 0) {
                        mCouncillorId = (Integer.parseInt(mArrCouncillorList.get(mSelectedCouncillorIndex - 1).getCouncillorID()));
                        String mCouncillorName = (mArrCouncillorList.get(mSelectedCouncillorIndex - 1).getCouncillorName());
                        Common.insertLog("mCouncillorId::> " + mCouncillorId);
                        Common.insertLog("mCouncillorName::> " + mCouncillorName);
                    }
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    /**
     * Sets up the data to the councillor spinner
     */
    private void callToCouncillorAPI() {
        try {

            String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setCouncillorListJson(hospital_database));

            Call<CouncillorModel> call = RetrofitClient.createService(ApiInterface.class).getCouncillorList(body);
            call.enqueue(new Callback<CouncillorModel>() {
                @Override
                public void onResponse(@NonNull Call<CouncillorModel> call, @NonNull Response<CouncillorModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrCouncillorList.addAll(response.body().getData());
                            }
                            setsCouncillorAdapter();
                        } else {
                            setsCouncillorAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CouncillorModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setsCouncillorAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setsCouncillorAdapter() {
        try {
            mArrCouncillor.add(0, getString(R.string.spinner_select_councillor));
            if (mArrCouncillorList.size() > 0) {
                for (CouncillorModel councillorModel : mArrCouncillorList) {
                    mArrCouncillor.add(councillorModel.getCouncillorName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerCouncillor.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_dropdown_item, mArrCouncillor) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCouncillorIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerCouncillor.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Councillor Inquiry Form Fragment
     */
    private void callToCouncillorInquiryFormFragment() {
        try {
            String mTreatmentDate;
            String mName = mEditPatientName.getText().toString().trim();
            String mMobileNo = mEditMobileNo.getText().toString().trim();

            if (mEditCouncillorDate.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
//                mTreatmentDate = mActivity.getResources().getString(R.string.date_format_first_time);
                mTreatmentDate = "1970-01-01";
            } else {
                mTreatmentDate = Common.convertDateWithoutTime(mActivity, mEditCouncillorDate.getText().toString().trim(), mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
            }

            if (mSpinnerCouncillor.getSelectedItemPosition() == 0) {
                mCouncillorId = -1;
            }

            ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.header_counselling));
            Fragment fragment = new CouncillorInquiryFormFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_NAME, mName);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mMobileNo);
            args.putString(AppConstants.BUNDLE_PATIENT_COUNCILLOR_DATE, mTreatmentDate);
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_COUNCILLOR, mCouncillorId);
            fragment.setArguments(args);
            FragmentManager fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    mActivity.getResources().getString(R.string.header_counselling)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
