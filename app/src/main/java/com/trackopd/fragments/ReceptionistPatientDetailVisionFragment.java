package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.trackopd.R;
import com.trackopd.adapter.HistoryVisionAdapter;
import com.trackopd.adapter.VisionIPDAdapter;
import com.trackopd.model.AddPreliminaryExaminationModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.model.PreliminaryExaminationModel;
import com.trackopd.model.PrintReceiptModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceptionistPatientDetailVisionFragment extends Fragment {

    private View mView;
    public Menu menu;
    private Activity mActivity;
    public Bundle mBundle;
    private int mAppointmentId;
    private String mPatientId,mDoctorId,mTreatmentDate;
    private ImageView mImageExpandableArrowUCVA, mImageExpandableArrowBCVA, mImageExpandableArrowRefraction,
            mImageExpandableArrowKeratometry;
    private LinearLayout mLinearExpandableArrowUCVA, mLinearExpandableArrowBCVA, mLinearExpandableArrowRefraction,
            mLinearExpandableArrowKeratometry;
    private ExpandableLayout mExpandableLayoutUCVA, mExpandableLayoutBCVA, mExpandableLayoutRefraction,
            mExpandableLayoutKeratometry;

    private static Spinner mSpinnerUCVARightEyeDistance, mSpinnerUCVALeftEyeDistance, mSpinnerUCVARightEyeNear,
            mSpinnerUCVALeftEyeNear,mSpinnerRefractionUnDilatedRightVA,
            mSpinnerRefractionUnDilatedRightNearVA,mSpinnerRefractionUnDilatedLeftVA,
            mSpinnerRefractionUnDilatedLeftNearVA,mSpinnerRefractionDilatedRightVA,mSpinnerRefractionDilatedRightNearVA,
            mSpinnerRefractionDilatedLeftVA,mSpinnerRefractionDilatedLeftNearVA,mSpinnerRefractionFinalRightVA,
            mSpinnerRefractionFinalRightNearVA,mSpinnerRefractionFinalLeftDistanceVA,mSpinnerRefractionFinalLeftNearVA,
            mSpinnerBCVAUnDilatedRightDistance,mSpinnerBCVAUnDilatedLeftDistance,mSpinnerBCVAUnDilatedLeftNear,
            mSpinnerBCVAUnDilatedRightNear,mSpinnerBCVADilatedRightDistance,mSpinnerBCVADilatedLeftDistance,
            mSpinnerBCVADilatedRightNear,mSpinnerBCVADilatedLeftNear;

    private ArrayList<PreliminaryExaminationDetailsModel.Data.Distance> mArrDistanceList;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.Near> mArrNearList;
    private static ArrayList<PreliminaryExaminationDetailsModel.Data.IPD> mArrIPD;
    private static ArrayList<String> mArrDistance,mArrNear;

    private String hospital_database,mUserType;

    private static final int unselectedItemUCVA = -1, unselectedItemBCVA = -1,
            unselectedItemRefraction = -1,unSelectedItemKeratometry=-1;
    private int selectedItemUCVA = unselectedItemUCVA;
    private int selectedItemBCVA = unselectedItemBCVA;
    private int selectedItemRefraction = unselectedItemRefraction;
    private int selectedItemKeratometry = unSelectedItemKeratometry;
    private Button mButtonSubmit;
    private AppUtil mAppUtils;

    private static EditText
            mEditRefractionUnDilatedRightDistanceSph,mEditRefractionUnDilatedRightDistanceCyl,mEditRefractionUnDilatedRightAxis,
            mEditRefractionUnDilatedRightNearSph,mEditRefractionUnDilatedRightNearCyl,mEditRefractionUnDilatedRightNearAxis,
            mEditRefractionUnDilatedLeftDistanceSph,mEditRefractionUnDilatedLeftDistanceCyl,mEditRefractionUnDilatedLeftAxis,
            mEditRefractionUnDilatedLeftNearSph,mEditRefractionUnDilatedLeftNearCyl,mEditRefractionUnDilatedLeftNearAxis,
            mEditRefractionDilatedRightDistanceSph,mEditRefractionDilatedRightDistanceCyl,mEditRefractionDilatedRightAxis,
            mEditRefractionDilatedRightNearSph,mEditRefractionDilatedRightNearCyl,mEditRefractionDilatedRightNearAxis,
            mEditRefractionDilatedLeftDistanceSph,mEditRefractionDilatedLeftDistanceCyl,mEditRefractionDilatedLeftAxis,
            mEditRefractionDilatedLeftNearSph,mEditRefractionDilatedLeftNearCyl,mEditRefractionDilatedLeftNearAxis,
            mEditRefractionFinalRightDistanceSph,mEditRefractionFinalRightDistanceCyl,mEditRefractionFinalRightAxis,
            mEditRefractionFinalRightNearSph,mEditRefractionFinalRightNearCyl,mEditRefractionFinalRightNearAxis,
            mEditRefractionFinalLeftDistanceSph,mEditRefractionFinalLeftDistanceCyl,mEditRefractionFinalLeftAxis,
            mEditRefractionFinalLeftNearSph,mEditRefractionFinalLeftNearCyl,mEditRefractionFinalLeftNearAxis,mEditUCVARemarks,mEditBCVARemarks,mEditKeratometryKOneRightPower,mEditKeratometryKOneRightAxis,mEditKeratometryKOneLeftPower,mEditKeratometryKOneLeftAxis,
            mEditKeratometryKTwoRightPower,mEditKeratometryKTwoRightAxis,mEditKeratometryKTwoLeftPower,mEditKeratometryKTwoLeftAxis;


    public  ArrayList<HistoryPreliminaryExaminationModel.Data> mArrHistory;
    private boolean isVisible=false,isStarted=false;
    private static RelativeLayout  mRelativeNoData;
    private int mCurrentPage=1,mHistoryDoctorID=-1;
    private RecyclerView mRecyclerViewIPD;

    private static VisionIPDAdapter IpdAdapter;
    private ImageView mImagePrintFinal;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_vision, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mAppUtils=new AppUtil(mActivity);

        mArrDistanceList = new ArrayList<>();
        mArrDistance = new ArrayList<>();
        mArrNearList = new ArrayList<>();
        mArrNear = new ArrayList<>();
        mArrIPD=new ArrayList<>();

        mArrHistory=new ArrayList<>();

        hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

        getBundle();
        getIds();
        setRegListeners();
        callToPreliminaryExaminationAPI();
        callToHistoryVision();

        setHasOptionsMenu(true);
        return mView;
    }



    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        final MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(false);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
               // mAppointmentId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mDoctorId="-1";
                mTreatmentDate= bundle.getString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE);
                mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {

            //Relative Layout
            mRelativeNoData=mView.findViewById(R.id.relative_vision_no_data_found);
            // Image Views
            mImageExpandableArrowUCVA = mView.findViewById(R.id.image_add_vision_ucva_expandable_view);
            mImageExpandableArrowBCVA = mView.findViewById(R.id.image_add_vision_bcva_expandable_view);
            mImageExpandableArrowRefraction = mView.findViewById(R.id.image_add_vision_refraction_expandable_view);
            mImageExpandableArrowKeratometry = mView.findViewById(R.id.image_add_vision_keratometry_expandable_view);
            mImagePrintFinal=mView.findViewById(R.id.image_vision_fianl_print);

            // Linear Layouts
            mLinearExpandableArrowUCVA = mView.findViewById(R.id.linear_add_vision_ucva_expandable_view);
            mLinearExpandableArrowBCVA = mView.findViewById(R.id.linear_add_vision_bcva_expandable_view);
            mLinearExpandableArrowRefraction = mView.findViewById(R.id.linear_add_vision_refraction_expandable_view);
            mLinearExpandableArrowKeratometry = mView.findViewById(R.id.linear_add_vision_keratometry_expandable_view);

            // Expandable Layouts
            this.mExpandableLayoutUCVA = mView.findViewById(R.id.expandable_layout_add_vision_ucva);
            this.mExpandableLayoutBCVA = mView.findViewById(R.id.expandable_layout_add_vision_bcva);
            this.mExpandableLayoutRefraction = mView.findViewById(R.id.expandable_layout_add_vision_refraction);
            this.mExpandableLayoutKeratometry= mView.findViewById(R.id.expandable_layout_add_vision_keratometry);

            //Spinner
            mSpinnerUCVARightEyeDistance=mView.findViewById(R.id.spinner_add_vision_ucva_right_eye_distance);
            mSpinnerUCVALeftEyeDistance=mView.findViewById(R.id.spinner_add_vision_ucva_left_eye_distance);
            mSpinnerUCVARightEyeNear=mView.findViewById(R.id.spinner_add_vision_ucva_right_eye_near);
            mSpinnerUCVALeftEyeNear=mView.findViewById(R.id.spinner_add_vision_ucva_left_eye_near);

            //Button
            mButtonSubmit=mView.findViewById(R.id.button_add_vision_submit);

            //Edit Text
            mSpinnerBCVAUnDilatedRightDistance=mView.findViewById(R.id.spinner_add_vision_bcva_undilated_right_eye_distance);
            mSpinnerBCVADilatedRightDistance=mView.findViewById(R.id.spinner_add_vision_bcva_dilated_right_eye_distance);
            mSpinnerBCVAUnDilatedRightNear=mView.findViewById(R.id.spinner_add_vision_bcva_undilated_right_eye_near);
            mSpinnerBCVADilatedRightNear=mView.findViewById(R.id.spinner_add_vision_bcva_dilated_right_eye_near);

            mSpinnerBCVAUnDilatedLeftDistance=mView.findViewById(R.id.spinner_add_vision_bcva_undilated_left_eye_distance);
            mSpinnerBCVADilatedLeftDistance=mView.findViewById(R.id.spinner_add_vision_bcva_dilated_left_eye_distance);
            mSpinnerBCVAUnDilatedLeftNear=mView.findViewById(R.id.spinner_add_vision_bcva_undilated_left_eye_near);
            mSpinnerBCVADilatedLeftNear=mView.findViewById(R.id.spinner_add_vision_bcva_dilated_left_eye_near);

            // Refraction  Undilated Right Eye Distance
            mEditRefractionUnDilatedRightDistanceSph=mView.findViewById(R.id.edit_add_vision_refraction_undilated_right_eye_distance_sph);
            mEditRefractionUnDilatedRightDistanceCyl=mView.findViewById(R.id.edit_add_vision_refraction_undilated_right_eye_distance_cyl);
            mEditRefractionUnDilatedRightAxis=mView.findViewById(R.id.edit_add_vision_refraction_undilated_right_eye_distance_axis);
            mSpinnerRefractionUnDilatedRightVA=mView.findViewById(R.id.spinner_add_vision_refraction_undilated_right_eye_distance_va);

            // Refraction  Undilated Right Eye Near
            mEditRefractionUnDilatedRightNearSph=mView.findViewById(R.id.edit_add_vision_refraction_undilated_right_eye_near_sph);
            mEditRefractionUnDilatedRightNearCyl=mView.findViewById(R.id.edit_add_vision_refraction_undilated_right_eye_near_cyl);
            mEditRefractionUnDilatedRightNearAxis=mView.findViewById(R.id.edit_add_vision_refraction_undilated_right_eye_near_axis);
            mSpinnerRefractionUnDilatedRightNearVA=mView.findViewById(R.id.spinner_add_vision_refraction_undilated_right_eye_near_va);

            // Refraction  Undilated Left Eye Distance
            mEditRefractionUnDilatedLeftDistanceSph=mView.findViewById(R.id.edit_add_vision_refraction_undilated_left_eye_distance_sph);
            mEditRefractionUnDilatedLeftDistanceCyl=mView.findViewById(R.id.edit_add_vision_refraction_undilated_left_eye_distance_cyl);
            mEditRefractionUnDilatedLeftAxis=mView.findViewById(R.id.edit_add_vision_refraction_undilated_left_eye_distance_axis);
            mSpinnerRefractionUnDilatedLeftVA=mView.findViewById(R.id.spinner_add_vision_refraction_undilated_left_eye_distance_va);

            // Refraction  Undilated Right Eye Near
            mEditRefractionUnDilatedLeftNearSph=mView.findViewById(R.id.edit_add_vision_refraction_undilated_left_eye_near_sph);
            mEditRefractionUnDilatedLeftNearCyl=mView.findViewById(R.id.edit_add_vision_refraction_undilated_left_eye_near_cyl);
            mEditRefractionUnDilatedLeftNearAxis=mView.findViewById(R.id.edit_add_vision_refraction_undilated_left_eye_near_axis);
            mSpinnerRefractionUnDilatedLeftNearVA=mView.findViewById(R.id.spinner_add_vision_refraction_undilated_left_eye_near_va);

            // Refraction  dilated Right Eye Distance
            mEditRefractionDilatedRightDistanceSph=mView.findViewById(R.id.edit_add_vision_refraction_dilated_right_eye_distance_sph);
            mEditRefractionDilatedRightDistanceCyl=mView.findViewById(R.id.edit_add_vision_refraction_dilated_right_eye_distance_cyl);
            mEditRefractionDilatedRightAxis=mView.findViewById(R.id.edit_add_vision_refraction_dilated_right_eye_distance_axis);
            mSpinnerRefractionDilatedRightVA=mView.findViewById(R.id.spinner_add_vision_refraction_dilated_right_eye_distance_va);

            // Refraction  dilated Right Eye Near
            mEditRefractionDilatedRightNearSph=mView.findViewById(R.id.edit_add_vision_refraction_dilated_right_eye_near_sph);
            mEditRefractionDilatedRightNearCyl=mView.findViewById(R.id.edit_add_vision_refraction_dilated_right_eye_near_cyl);
            mEditRefractionDilatedRightNearAxis=mView.findViewById(R.id.edit_add_vision_refraction_dilated_right_eye_near_axis);
            mSpinnerRefractionDilatedRightNearVA=mView.findViewById(R.id.spinner_add_vision_refraction_dilated_right_eye_near_va);

            // Refraction  Dilated Left Eye Distance
            mEditRefractionDilatedLeftDistanceSph=mView.findViewById(R.id.edit_add_vision_refraction_dilated_left_eye_distance_sph);
            mEditRefractionDilatedLeftDistanceCyl=mView.findViewById(R.id.edit_add_vision_refraction_dilated_left_eye_distance_cyl);
            mEditRefractionDilatedLeftAxis=mView.findViewById(R.id.edit_add_vision_refraction_dilated_left_eye_distance_axis);
            mSpinnerRefractionDilatedLeftVA=mView.findViewById(R.id.spinner_add_vision_refraction_dilated_left_eye_distance_va);

            // Refraction  Dilated Right Eye Near
            mEditRefractionDilatedLeftNearSph=mView.findViewById(R.id.edit_add_vision_refraction_dilated_left_eye_near_sph);
            mEditRefractionDilatedLeftNearCyl=mView.findViewById(R.id.edit_add_vision_refraction_dilated_left_eye_near_cyl);
            mEditRefractionDilatedLeftNearAxis=mView.findViewById(R.id.edit_add_vision_refraction_dilated_left_eye_near_axis);
            mSpinnerRefractionDilatedLeftNearVA=mView.findViewById(R.id.spinner_add_vision_refraction_dilated_left_eye_near_va);

            // Refraction Final Right Eye Distance
            mEditRefractionFinalRightDistanceSph=mView.findViewById(R.id.edit_add_vision_refraction_final_right_eye_distance_sph);
            mEditRefractionFinalRightDistanceCyl=mView.findViewById(R.id.edit_add_vision_refraction_final_right_eye_distance_cyl);
            mEditRefractionFinalRightAxis=mView.findViewById(R.id.edit_add_vision_refraction_final_right_eye_distance_axis);
            mSpinnerRefractionFinalRightVA=mView.findViewById(R.id.spinner_add_vision_refraction_final_right_eye_distance_va);

            // Refraction  Final Right Eye Near
            mEditRefractionFinalRightNearSph=mView.findViewById(R.id.edit_add_vision_refraction_final_right_eye_near_sph);
            mEditRefractionFinalRightNearCyl=mView.findViewById(R.id.edit_add_vision_refraction_final_right_eye_near_cyl);
            mEditRefractionFinalRightNearAxis=mView.findViewById(R.id.edit_add_vision_refraction_final_right_eye_near_axis);
            mSpinnerRefractionFinalRightNearVA=mView.findViewById(R.id.spinner_add_vision_refraction_final_right_eye_near_va);

            // Refraction  Final Left Eye Distance
            mEditRefractionFinalLeftDistanceSph=mView.findViewById(R.id.edit_add_vision_refraction_final_left_eye_distance_sph);
            mEditRefractionFinalLeftDistanceCyl=mView.findViewById(R.id.edit_add_vision_refraction_final_left_eye_distance_cyl);
            mEditRefractionFinalLeftAxis=mView.findViewById(R.id.edit_add_vision_refraction_final_left_eye_distance_axis);
            mSpinnerRefractionFinalLeftDistanceVA=mView.findViewById(R.id.spinner_add_vision_refraction_final_left_eye_distance_va);

            // Refraction  Final Left Eye Near
            mEditRefractionFinalLeftNearSph=mView.findViewById(R.id.edit_add_vision_refraction_final_left_eye_near_sph);
            mEditRefractionFinalLeftNearCyl=mView.findViewById(R.id.edit_add_vision_refraction_final_left_eye_near_cyl);
            mEditRefractionFinalLeftNearAxis=mView.findViewById(R.id.edit_add_vision_refraction_final_left_eye_near_axis);
            mSpinnerRefractionFinalLeftNearVA=mView.findViewById(R.id.spinner_add_vision_refraction_final_left_eye_near_va);

            // Keratometry
            mEditKeratometryKOneRightPower=mView.findViewById(R.id.edit_add_vision_keratometry_kone_re_power);
            mEditKeratometryKOneRightAxis=mView.findViewById(R.id.edit_add_vision_keratometry_kone_re_axis);
            mEditKeratometryKOneLeftPower=mView.findViewById(R.id.edit_add_vision_keratometry_kone_le_power);
            mEditKeratometryKOneLeftAxis=mView.findViewById(R.id.edit_add_vision_keratometry_kone_le_axis);

            mEditKeratometryKTwoRightPower=mView.findViewById(R.id.edit_add_vision_keratometry_ktwo_re_power);
            mEditKeratometryKTwoRightAxis=mView.findViewById(R.id.edit_add_vision_keratometry_ktwo_re_axis);
            mEditKeratometryKTwoLeftPower=mView.findViewById(R.id.edit_add_vision_keratometry_ktwo_le_power);
            mEditKeratometryKTwoLeftAxis=mView.findViewById(R.id.edit_add_vision_keratometry_ktwo_le_axis);

            mEditUCVARemarks=mView.findViewById(R.id.edit_text_add_vision_ucva_remarks);
            mEditBCVARemarks=mView.findViewById(R.id.edit_text_add_vision_bcva_remarks);

            mRecyclerViewIPD=mView.findViewById(R.id.receyler_view_vision_ipd);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    @SuppressLint("ClickableViewAccessibility")
    private void setRegListeners() {
        try {
            // ToDo: On Click Listener
            mLinearExpandableArrowUCVA.setOnClickListener(clickListener);
            mLinearExpandableArrowBCVA.setOnClickListener(clickListener);
            mLinearExpandableArrowRefraction.setOnClickListener(clickListener);
            mLinearExpandableArrowKeratometry.setOnClickListener(clickListener);
            mButtonSubmit.setOnClickListener(clickListener);
            mImagePrintFinal.setOnClickListener(clickListener);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = v -> {
        switch (v.getId()) {
            case R.id.linear_add_vision_ucva_expandable_view:
                expandViewUCVA();
                break;

            case R.id.linear_add_vision_bcva_expandable_view:
                expandViewBCVA();
                break;

            case R.id.linear_add_vision_refraction_expandable_view:
                expandViewRefraction();
                break;
            case R.id.linear_add_vision_keratometry_expandable_view:
                expandViewKeratometry();
                break;
            case R.id.button_add_vision_submit:
                  callToAddVision();
                break;
            case R.id.image_vision_fianl_print:
                  callToPrintFinalVision();
                break;
        }
    };

    /**
     *
     */
    private void callToPrintFinalVision() {
        try{
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            boolean mHistoryAndComplaints=false,mVisionUCVA=false,mVisionBCVAUndilated=false,mVisionFinal=true,
                    mVisionBCVADilated=false,mPrimaryExamination=false,mDiagnosis=false,mInvestigationSuggested=false,
                    mTreatmentSuggested=false,mCounselingDetails=false,mSurgeryDetails=false,mPrescription=false,
                    mPayment=false;

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPrintReceiptJson(AddNewPreliminaryExaminationFragment.PreliminaryExminationId,
                            String.valueOf(mAppointmentId),mHistoryAndComplaints,mVisionUCVA, mVisionBCVAUndilated,
                            mVisionFinal, mVisionBCVADilated, mPrimaryExamination , mDiagnosis,mInvestigationSuggested,
                            mTreatmentSuggested, mCounselingDetails,mSurgeryDetails, mPrescription,",",false,mPayment,hospital_database));

            Call<PrintReceiptModel> call = RetrofitClient.createService(ApiInterface.class).PrintReceipt(body);
            call.enqueue(new Callback<PrintReceiptModel>() {
                @Override
                public void onResponse(@NonNull Call<PrintReceiptModel> call, @NonNull Response<PrintReceiptModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() &&
                                response.body().getError()==AppConstants.API_SUCCESS_ERROR) {
                            if (response.body() != null) {
                                String url=response.body().getData();
                                callToReceiptPrint(url);
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PrintReceiptModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Call To Receipt Print
     * @param url - String Url Print
     */
    private void callToReceiptPrint(String url) {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);
    }

    /**
     * Expands view for Keratometry
     */
    private void expandViewKeratometry() {
        try {
            int position = 0;
            bindView(position);

            mLinearExpandableArrowKeratometry.setSelected(false);
            mExpandableLayoutKeratometry.collapse();
            mImageExpandableArrowKeratometry.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));

            if (position == selectedItemKeratometry) {
                selectedItemKeratometry = unSelectedItemKeratometry;
            } else {
                mLinearExpandableArrowKeratometry.setSelected(true);
                mExpandableLayoutKeratometry.expand();
                mImageExpandableArrowKeratometry.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                selectedItemKeratometry = position;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Expands view for UCVA
     */
    private void expandViewUCVA() {
        try {
            int position = 0;
            bindView(position);

            mLinearExpandableArrowUCVA.setSelected(false);
            mExpandableLayoutUCVA.collapse();
            mImageExpandableArrowUCVA.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));

            if (position == selectedItemUCVA) {
                selectedItemUCVA = unselectedItemUCVA;
            } else {
                mLinearExpandableArrowUCVA.setSelected(true);
                mExpandableLayoutUCVA.expand();
                mImageExpandableArrowUCVA.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                selectedItemUCVA = position;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Expands view for BCVA
     */
    private void expandViewBCVA() {
        try {
            int position = 1;
            bindView(position);

            mLinearExpandableArrowBCVA.setSelected(false);
            mExpandableLayoutBCVA.collapse();
            mImageExpandableArrowBCVA.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));

            if (position == selectedItemBCVA) {
                selectedItemBCVA = unselectedItemBCVA;
            } else {
                mLinearExpandableArrowBCVA.setSelected(true);
                mExpandableLayoutBCVA.expand();
                mImageExpandableArrowBCVA.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                selectedItemBCVA = position;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Expands view for Refraction
     */
    private void expandViewRefraction() {
        try {
            int position = 0;
            bindView(position);

            mLinearExpandableArrowRefraction.setSelected(false);
            mExpandableLayoutRefraction.collapse();
            mImageExpandableArrowRefraction.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));

            if (position == selectedItemRefraction) {
                selectedItemRefraction = unselectedItemRefraction;
            } else {
                mLinearExpandableArrowRefraction.setSelected(true);
                mExpandableLayoutRefraction.expand();
                mImageExpandableArrowRefraction.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                selectedItemRefraction = position;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Binds the expandable view
     */
    public void bindView(int position) {
        if (position == 0) {
            boolean isSelected = position == selectedItemUCVA;

            mLinearExpandableArrowUCVA.setSelected(isSelected);
            mExpandableLayoutUCVA.setExpanded(isSelected, false);
        } else if (position == 1) {
            boolean isSelected = position == selectedItemBCVA;

            mLinearExpandableArrowBCVA.setSelected(isSelected);
            mExpandableLayoutBCVA.setExpanded(isSelected, false);
        } else {
            boolean isSelected = position == selectedItemRefraction;

            mLinearExpandableArrowRefraction.setSelected(isSelected);
            mExpandableLayoutRefraction.setExpanded(isSelected, false);
        }
    }

    /**
     * This method should call the Preliminary Examination API
     */
    private void callToPreliminaryExaminationAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPreliminaryExaminationDetailsJson(hospital_database));

            Call<PreliminaryExaminationDetailsModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationDetails(body);
            call.enqueue(new Callback<PreliminaryExaminationDetailsModel>() {
                @Override
                public void onResponse(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Response<PreliminaryExaminationDetailsModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null) {

                                // Distance Binding
                                mArrDistanceList.addAll(response.body().getData().getDistance());
                                bindDistanceAdaper();

                                // Near Binding
                                mArrNearList.addAll(response.body().getData().getNear());
                                bindNearAdaper();
                                //Set IPD
                                mArrIPD=response.body().getData().getIPD();
                                bindIPDAdapter();

                                //Call History Data
                                callToPreliminarHistory();
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set IPD Adapter
     */
    private void bindIPDAdapter() {
       RecyclerView.LayoutManager manager=new LinearLayoutManager(mActivity,LinearLayoutManager.HORIZONTAL,false);
       // RecyclerView.LayoutManager manager=new StaggeredGridLayoutManager(3,StaggeredGridLayoutManager.HORIZONTAL);
       mRecyclerViewIPD.setLayoutManager(manager);

       IpdAdapter=new VisionIPDAdapter(mActivity,mArrIPD);
       mRecyclerViewIPD.setAdapter(IpdAdapter);
    }

    /**
     * Binds the distance data to the adapter
     */
    private void bindDistanceAdaper() {
        try {
            if (mArrDistanceList != null && mArrDistanceList.size() > 0) {

                mArrDistance.add(AppConstants.STR_EMPTY_SPACE);

                for (int i = 0; i < mArrDistanceList.size(); i++) {
                    mArrDistance.add(mArrDistanceList.get(i).getDistance());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<>
                        (mActivity, android.R.layout.simple_selectable_list_item, mArrDistance);
                setsDistanceAdapter(adapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    /**
     * Sets the data to the adapter and bind it to the auto field text view
     * @param adapter - Adapter
     */
    private void setsDistanceAdapter(ArrayAdapter<String> adapter) {
        try {

            // UCVA Distance
            mSpinnerUCVALeftEyeDistance.setAdapter(adapter);
            //mSpinnerUCVALeftEyeDistance.setSelection(0);
            
            mSpinnerUCVARightEyeDistance.setAdapter(adapter);
            //mSpinnerUCVARightEyeDistance.setSelection(0);

            // Bind Refraction Distance
            mSpinnerRefractionUnDilatedRightVA.setAdapter(adapter);
            mSpinnerRefractionUnDilatedLeftVA.setAdapter(adapter);
            mSpinnerRefractionDilatedRightVA.setAdapter(adapter);

            mSpinnerRefractionDilatedLeftVA.setAdapter(adapter);
            mSpinnerRefractionFinalRightVA.setAdapter(adapter);
            mSpinnerRefractionFinalLeftDistanceVA.setAdapter(adapter);

            // Bind BCVA Distance
            mSpinnerBCVAUnDilatedRightDistance.setAdapter(adapter);
            mSpinnerBCVAUnDilatedLeftDistance.setAdapter(adapter);
            mSpinnerBCVADilatedRightDistance.setAdapter(adapter);
            mSpinnerBCVADilatedLeftDistance.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Binds the near data to the adapter
     */
    private void bindNearAdaper() {
        try {
            if (mArrNearList != null && mArrNearList.size() > 0) {

                mArrNear.add(AppConstants.STR_EMPTY_SPACE);
                for (int i = 0; i < mArrNearList.size(); i++) {
                    mArrNear.add(mArrNearList.get(i).getNear());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<>
                        (mActivity, android.R.layout.simple_selectable_list_item, mArrNear);
                setsNearAdapter(adapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data to the adapter and bind it to the auto field text view
     * @param adapter - Adapter
     */
    private void setsNearAdapter(ArrayAdapter<String> adapter) {
        try {

            //UCVA Near
            mSpinnerUCVARightEyeNear.setAdapter(adapter);
            mSpinnerUCVALeftEyeNear.setAdapter(adapter);

            // Bind Refraction Near
            mSpinnerRefractionUnDilatedRightNearVA.setAdapter(adapter);
            mSpinnerRefractionUnDilatedLeftNearVA.setAdapter(adapter);
            mSpinnerRefractionDilatedRightNearVA.setAdapter(adapter);
            mSpinnerRefractionDilatedLeftNearVA.setAdapter(adapter);
            mSpinnerRefractionFinalRightNearVA.setAdapter(adapter);
            mSpinnerRefractionFinalLeftNearVA.setAdapter(adapter);

            // Bind BCVA Distance
            mSpinnerBCVAUnDilatedRightNear.setAdapter(adapter);
            mSpinnerBCVAUnDilatedLeftNear.setAdapter(adapter);
            mSpinnerBCVADilatedRightNear.setAdapter(adapter);
            mSpinnerBCVADilatedLeftNear.setAdapter(adapter);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Call To History Data
     */
    private void callToPreliminarHistory()
    {
        try {
            String defualt_date="1000-01-01";
            String Mobile=AppConstants.STR_EMPTY_STRING;
            String Type="Vision";

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setHistoryPreliminaryExaminationJson(defualt_date,mCurrentPage,mPatientId,Mobile,mHistoryDoctorID,Type,hospital_database));

            Call<HistoryPreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationHistory(body);
            call.enqueue(new Callback<HistoryPreliminaryExaminationModel>() {
                @Override
                public void onResponse(Call<HistoryPreliminaryExaminationModel> call, Response<HistoryPreliminaryExaminationModel> response) {
                    Common.insertLog("Response History::::> " + new Gson().toJson(response.body()));
                    try {
                        if (response.body()!=null &&
                                response.body().getError()==AppConstants.API_SUCCESS_ERROR)
                        {
                            if(response.body().getData().size()>0) {

                                mArrHistory.addAll(response.body().getData());
                                setTodayData();

                            }
                        }
                    }catch (Exception e){
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<HistoryPreliminaryExaminationModel> call, Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Today Already Added Data
     */
    private void setTodayData() {

        String currentDate=Common.setCurrentDate(mActivity);
        String covertCurrent=Common.convertDateUsingDateFormat(mActivity,currentDate,
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen));

        if (mArrHistory.size()>0){
            for (int a=0;a<mArrHistory.size();a++)
            {

                if (mArrHistory.get(a).getTreatmentDate().equalsIgnoreCase(covertCurrent))
                {
                    if (mArrHistory.get(a).getVision().size()>0)
                    {
                        for (int i=0;i<mArrHistory.get(a).getVision().size();i++)
                        {
                            addEditVision(mActivity,mArrHistory.get(a).getVision().get(i),mArrHistory.get(a));
                        }
                    }
                }
            }
        }

    }

    /**
     * Set Today Vision Data
     * @param mActivity
     * @param vision - Vision Data
     * @param data - Prelminary Examination Data
     */
    private void addEditVision(Activity mActivity, HistoryPreliminaryExaminationModel.Vision vision, HistoryPreliminaryExaminationModel.Data data) {
      try {

          Common.insertLog("Vision...."+vision);


          String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
          String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

          String data_save= APICommonMethods.setAddPreliminaryVisionJson(mUserId, mDatabaseName,data.getAppointmentID(),data.getPatientID(),data.getDoctorID(),data.getTreatmentDate(),
                  vision.getUCVADR(),vision.getUCVANR(),vision.getUCVADL(),vision.getUCVANL(),vision.getUCVARemarks(),vision.getBCVAUDR(), vision.getBCVAUNR(),
                  vision.getBCVAUDL(),vision.getBCVAUNL(),vision.getBCVADDR(),vision.getBCVADNR(), vision.getBCVADDL(),
                  vision.getBCVADNL(),vision.getBCVARemarks(),vision.getRefUDRSph(),vision.getRefUDLSph(),vision.getRefUNRSph(), vision.getRefUNLSph(),
                  vision.getRefUDRCyl(),vision.getRefUDLCyl(),vision.getRefUNRCyl(),vision.getRefUNLCyl(),vision.getRefUDRAxis(),vision.getRefUDLAxis(),vision.getRefUNRAxis(),
                  vision.getRefUNLAxis(),vision.getRefUDRVA(),vision.getRefUDLVA(),vision.getRefUNRVA(),vision.getRefUNLVA(),
                  vision.getRefDDRSph(), vision.getRefDDLSph(),vision.getRefDNRSph(),vision.getRefDNLSph(), vision.getRefDDRCyl(),vision.getRefDDLCyl(),vision.getRefDNRCyl(),
                  vision.getRefDNLCyl(),vision.getRefDDRAxis(),vision.getRefDDLAxis(),vision.getRefDNRAxis(),vision.getRefDNLAxis(),vision.getRefDDRVA(), vision.getRefDDLVA(),
                  vision.getRefDNRVA(), vision.getRefDNLVA(),vision.getRefFDRSph(), vision.getRefFDLSph(), vision.getRefFNRSph(),vision.getRefFNLSph(),vision.getRefFDRCyl(),vision.getRefFDLCyl(),vision.getRefFNRCyl(),
                  vision.getRefFNLCyl(),vision.getRefFDRAxis(), vision.getRefFDLAxis(),vision.getRefFNRAxis(),vision.getRefFNLAxis(),vision.getRefFDRVA(),vision.getRefFDLVA(),vision.getRefFNRVA(),vision.getRefFNLVA(),
                  vision.getIPD(),vision.getK1RPower(),vision.getK1RAxis(),vision.getK1LPower(),vision.getK1LAxis(),vision.getK2RPower(),vision.getK2RAxis(),vision.getK2LPower(), vision.getK2LAxis(), AddNewPreliminaryExaminationFragment.PreliminaryExminationId);

          mRelativeNoData.setVisibility(View.GONE);
          Common.insertLog(" vision.."+data_save);
          setUpdate(data_save);

      }catch (Exception e){
          Common.insertLog(" Error Today Vision"+e.getMessage());
      }
    }


    /**
     *  Call To Add Vision
     */
    private void callToAddVision() {
       if (!mAppUtils.getConnectionState()){
           Common.setCustomToast(mActivity,mActivity.getResources().getString(R.string.no_internet_connection));
       }else {
           try {

               int selUCVARNEAR=mSpinnerUCVARightEyeNear.getSelectedItemPosition();
               int selUCVARDIS=mSpinnerUCVARightEyeDistance.getSelectedItemPosition();
               int selUCVALNEAR=mSpinnerUCVALeftEyeNear.getSelectedItemPosition();
               int selUCVALDIS=mSpinnerUCVALeftEyeDistance.getSelectedItemPosition();

               //BCVA
               int selBCVAUDREOD=mSpinnerBCVAUnDilatedRightDistance.getSelectedItemPosition();
               int selBCVAUDLEOD=mSpinnerBCVAUnDilatedLeftDistance.getSelectedItemPosition();
               int selBCVAUNLEOD=mSpinnerBCVAUnDilatedLeftNear.getSelectedItemPosition();
               int selBCVAUNREOD =mSpinnerBCVAUnDilatedRightNear.getSelectedItemPosition();
               int selBCVADDREOD=mSpinnerBCVADilatedRightDistance.getSelectedItemPosition();
               int selBCVADDLEOD=mSpinnerBCVADilatedLeftDistance.getSelectedItemPosition();
               int selBCVADNREOD=mSpinnerBCVADilatedRightNear.getSelectedItemPosition();
               int selBCVADNLEOD=mSpinnerBCVADilatedLeftNear.getSelectedItemPosition();


               ///Refraction VA
               int selREFUDRVA=mSpinnerRefractionUnDilatedRightVA.getSelectedItemPosition();
               int selREFUNRVA=mSpinnerRefractionUnDilatedRightNearVA.getSelectedItemPosition();
               int selREFUDLVA=mSpinnerRefractionUnDilatedLeftVA.getSelectedItemPosition();
               int selREFUNLVA=mSpinnerRefractionUnDilatedLeftNearVA.getSelectedItemPosition();
               int selREFDDRVA=mSpinnerRefractionDilatedRightVA.getSelectedItemPosition();
               int selREFDNRVA=mSpinnerRefractionDilatedRightNearVA.getSelectedItemPosition();
               int selREFDDLVA=mSpinnerRefractionDilatedLeftVA.getSelectedItemPosition();
               int selREFDNLVA=mSpinnerRefractionDilatedLeftNearVA.getSelectedItemPosition();
               int selREFFDRVA=mSpinnerRefractionFinalRightVA.getSelectedItemPosition();
               int selREFFNRVA=mSpinnerRefractionFinalRightNearVA.getSelectedItemPosition();
               int selREFFDLVA=mSpinnerRefractionFinalLeftDistanceVA.getSelectedItemPosition();
               int selREFFNLVA=mSpinnerRefractionFinalLeftNearVA.getSelectedItemPosition();


               String UCVA_RE_DISTANCE=mArrDistance.get(selUCVARNEAR);
               String UCAVNREOD=mArrDistance.get(selUCVARDIS);
               String UCAVDLEOS=mArrDistance.get(selUCVALDIS);
               String UCAVNLEOS=mArrDistance.get(selUCVALNEAR);

               String UCAVRemarks=mEditUCVARemarks.getText().toString();

               String BCVAUndilatedDREOD= mArrDistance.get(selBCVAUDREOD);
               String BCVADilatedDREOD= mArrDistance.get(selBCVADDREOD);
               String BCVAUndilatedNREOD= mArrNear.get(selBCVAUNREOD).trim();
               String BCVADilatedNREOD= mArrNear.get(selBCVADNREOD);

               String BCVAUndilatedDLEOS= mArrDistance.get(selBCVAUDLEOD).trim();
               String BCVADilatedDLEOS= mArrDistance.get(selBCVADDLEOD);
               String BCVAUndilatedNLEOS= mArrNear.get(selBCVAUNLEOD);
               String BCVADilatedNLEOS= mArrNear.get(selBCVADNLEOD);

               String BCVADilatedRemarks=mEditBCVARemarks.getText().toString();

               // Refraction  Undilated Right Eye Distance
               String RefUndilatedRDSph= mEditRefractionUnDilatedRightDistanceSph.getText().toString();
               String RefUndilatedRDCyl=mEditRefractionUnDilatedRightDistanceCyl.getText().toString();
               String RefUndilatedRDAxis=mEditRefractionUnDilatedRightAxis.getText().toString();
               String RefUndilatedRDVA=mArrDistance.get(selREFUDRVA);

               // Refraction  Undilated Right Eye Near
               String RefUndilatedRNSph= mEditRefractionUnDilatedRightNearSph.getText().toString();
               String RefUndilatedRNCyl=mEditRefractionUnDilatedRightNearCyl.getText().toString();
               String RefUndilatedRNAxis=mEditRefractionUnDilatedRightNearAxis.getText().toString();
               String RefUndilatedRNVA= mArrNear.get(selREFUNRVA);

               // Refraction  Undilated Left Eye Distance
               String RefUndilatedLDSph= mEditRefractionUnDilatedLeftDistanceSph.getText().toString();
               String RefUndilatedLDCyl= mEditRefractionUnDilatedLeftDistanceCyl.getText().toString();
               String RefUndilatedLDAxis= mEditRefractionUnDilatedLeftAxis.getText().toString();
               String RefUndilatedLDVA= mArrDistance.get(selREFUDLVA);

               // Refraction  Undilated Left Eye Near
               String RefUndilatedLNSph=mEditRefractionUnDilatedLeftNearSph.getText().toString();
               String RefUndilatedLNCyl=mEditRefractionUnDilatedLeftNearCyl.getText().toString();
               String RefUndilatedLNAxis= mEditRefractionUnDilatedLeftNearAxis.getText().toString();
               String RefUndilatedLNVA= mArrNear.get(selREFUNLVA);

               // Refraction  dilated Right Eye Distance
               String RefDilatedRDSph= mEditRefractionDilatedRightDistanceSph.getText().toString();
               String RefDilatedRDCyl=mEditRefractionDilatedRightDistanceCyl.getText().toString();
               String RefDilatedRDAxis=mEditRefractionDilatedRightAxis.getText().toString();
               String RefDilatedRDVA= mArrDistance.get(selREFDDRVA);

               // Refraction  dilated Right Eye Near
               String RefDilatedRNSph=mEditRefractionDilatedRightNearSph.getText().toString();
               String RefDilatedRNCyl=mEditRefractionDilatedRightNearCyl.getText().toString();
               String RefDilatedRNAxis=mEditRefractionDilatedRightNearAxis.getText().toString();
               String RefDilatedRNVA= mArrNear.get(selREFDNRVA);

               // Refraction  Dilated Left Eye Distance
               String RefDilatedLDSph=mEditRefractionDilatedLeftDistanceSph.getText().toString();
               String RefDilatedLDCyl=mEditRefractionDilatedLeftDistanceCyl.getText().toString();
               String RefDilatedLDAxis=mEditRefractionDilatedLeftAxis.getText().toString();
               String RefDilatedLDVA= mArrDistance.get(selREFDDLVA);

               // Refraction  Dilated Right Eye Near
               String RefDilatedLNSph=mEditRefractionDilatedLeftNearSph.getText().toString();
               String RefDilatedLNCyl=mEditRefractionDilatedLeftNearCyl.getText().toString();
               String RefDilatedLNAxis=mEditRefractionDilatedLeftNearAxis.getText().toString();
               String RefDilatedLNVA= mArrNear.get(selREFDNLVA);

               // Refraction Final Right Eye Distance
               String RefFinalRDSph=mEditRefractionFinalRightDistanceSph.getText().toString();
               String RefFinalRDCyl= mEditRefractionFinalRightDistanceCyl.getText().toString();
               String RefFinalRDAxis=mEditRefractionFinalRightAxis.getText().toString();
               String RefFinalRDVA= mArrDistance.get(selREFFDRVA);

               // Refraction  Final Right Eye Near
               String RefFinalRNSph=mEditRefractionFinalRightNearSph.getText().toString();
               String RefFinalRNCyl=mEditRefractionFinalRightNearCyl.getText().toString();
               String RefFinalRNAxis=mEditRefractionFinalRightNearAxis.getText().toString();
               String RefFinalRNVA= mArrNear.get(selREFFNRVA);

               // Refraction  Final Left Eye Distance
               String RefFinalLDSph= mEditRefractionFinalLeftDistanceSph.getText().toString();
               String RefFinalLDCyl=mEditRefractionFinalLeftDistanceCyl.getText().toString();
               String RefFinalLDAxis=mEditRefractionFinalLeftAxis.getText().toString();
               String RefFinalLDVA= mArrDistance.get(selREFFDLVA);

               // Refraction  Final Right Eye Near
               String RefFinalLNSph=mEditRefractionFinalLeftNearSph.getText().toString();
               String RefFinalLNCyl=mEditRefractionFinalLeftNearCyl.getText().toString();
               String RefFinalLNAxis=mEditRefractionFinalLeftNearAxis.getText().toString();
               String RefFinalLNVA= mArrNear.get(selREFFNLVA);

               String K1RPower=mEditKeratometryKOneRightPower.getText().toString();
               String K1RAxis=mEditKeratometryKOneRightAxis.getText().toString();
               String K1LPower=mEditKeratometryKOneLeftPower.getText().toString();
               String K1LAxis=mEditKeratometryKOneLeftAxis.getText().toString();
               String K2RPower=mEditKeratometryKTwoRightPower.getText().toString();
               String K2RAxis=mEditKeratometryKTwoRightAxis.getText().toString();
               String K2LPower=mEditKeratometryKTwoLeftPower.getText().toString();
               String K2LAxis=mEditKeratometryKTwoLeftAxis.getText().toString();


               String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
               String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

               String currentDate=Common.setCurrentDate(mActivity);
               String strTreatmentDate=Common.convertDateUsingDateFormat(mActivity,currentDate,
                       mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                       mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));

               String RefFinalIPD=AppConstants.STR_EMPTY_STRING;

               if (mArrIPD.size()>0){
                   for (int a=0;a<mArrIPD.size();a++){
                       if (!mArrIPD.get(a).getStatus().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){

                           if (RefFinalIPD.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                              RefFinalIPD=mArrIPD.get(a).getIPD();
                           }else {
                               RefFinalIPD=RefFinalIPD+","+mArrIPD.get(a).getIPD();
                           }
                       }
                   }
               }

               String passIPD=RefFinalIPD;

               RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                       APICommonMethods.setAddPreliminaryVisionJson(mUserId, mDatabaseName, String.valueOf(mAppointmentId),mPatientId,mDoctorId,strTreatmentDate,
                       UCVA_RE_DISTANCE,UCAVNREOD, UCAVDLEOS,UCAVNLEOS,UCAVRemarks,BCVAUndilatedDREOD, BCVAUndilatedNREOD,
                       BCVAUndilatedDLEOS,BCVAUndilatedNLEOS,BCVADilatedDREOD, BCVADilatedNREOD, BCVADilatedDLEOS,
                       BCVADilatedNLEOS,BCVADilatedRemarks,RefUndilatedRDSph,RefUndilatedLDSph,RefUndilatedRNSph, RefUndilatedLNSph,
                       RefUndilatedRDCyl,RefUndilatedLDCyl,RefUndilatedRNCyl, RefUndilatedLNCyl, RefUndilatedRDAxis, RefUndilatedLDAxis, RefUndilatedRNAxis,
                       RefUndilatedLNAxis,RefUndilatedRDVA, RefUndilatedLDVA, RefUndilatedRNVA, RefUndilatedLNVA,
                       RefDilatedRDSph, RefDilatedLDSph, RefDilatedRNSph, RefDilatedLNSph, RefDilatedRDCyl, RefDilatedLDCyl, RefDilatedRNCyl,
                       RefDilatedLNCyl, RefDilatedRDAxis, RefDilatedLDAxis, RefDilatedRNAxis, RefDilatedLNAxis, RefDilatedRDVA, RefDilatedLDVA,
                       RefDilatedRNVA, RefDilatedLNVA, RefFinalRDSph, RefFinalLDSph, RefFinalRNSph, RefFinalLNSph, RefFinalRDCyl, RefFinalLDCyl, RefFinalRNCyl,
                       RefFinalLNCyl, RefFinalRDAxis, RefFinalLDAxis, RefFinalRNAxis, RefFinalLNAxis, RefFinalRDVA, RefFinalLDVA, RefFinalRNVA, RefFinalLNVA,
                       RefFinalIPD, K1RPower, K1RAxis, K1LPower, K1LAxis, K2RPower, K2RAxis, K2LPower, K2LAxis,AddNewPreliminaryExaminationFragment.PreliminaryExminationId));

               Common.insertLog("Request "+requestBody);
               Call<AddPreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).AddPreliminaeyExamination(requestBody);
               call.enqueue(new Callback<AddPreliminaryExaminationModel>() {
                   @Override
                   public void onResponse(@NonNull Call<AddPreliminaryExaminationModel> call, @NonNull Response<AddPreliminaryExaminationModel> response) {

                       Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                       try {
                           String mJson = (new Gson().toJson(response.body()));
                           JSONObject jsonObject = new JSONObject(mJson);
                           String mMessage = jsonObject.getString(WebFields.MESSAGE);
                           int mError = jsonObject.getInt(WebFields.ERROR);
                           Common.insertLog("mMessage:::> " + mMessage);

                           if (response.isSuccessful() && mError == 200) {
                               if (response.body() != null) {

                                  String data_save= APICommonMethods.setAddPreliminaryVisionJson(mUserId, mDatabaseName, String.valueOf(mAppointmentId),mPatientId,mDoctorId,strTreatmentDate,
                                           UCVA_RE_DISTANCE,UCAVNREOD, UCAVDLEOS,UCAVNLEOS,UCAVRemarks,BCVAUndilatedDREOD, BCVAUndilatedNREOD,
                                           BCVAUndilatedDLEOS,BCVAUndilatedNLEOS,BCVADilatedDREOD, BCVADilatedNREOD, BCVADilatedDLEOS,
                                           BCVADilatedNLEOS,BCVADilatedRemarks,RefUndilatedRDSph,RefUndilatedLDSph,RefUndilatedRNSph, RefUndilatedLNSph,
                                           RefUndilatedRDCyl,RefUndilatedLDCyl,RefUndilatedRNCyl, RefUndilatedLNCyl, RefUndilatedRDAxis, RefUndilatedLDAxis, RefUndilatedRNAxis,
                                           RefUndilatedLNAxis,RefUndilatedRDVA, RefUndilatedLDVA, RefUndilatedRNVA, RefUndilatedLNVA,
                                           RefDilatedRDSph, RefDilatedLDSph, RefDilatedRNSph, RefDilatedLNSph, RefDilatedRDCyl, RefDilatedLDCyl, RefDilatedRNCyl,
                                           RefDilatedLNCyl, RefDilatedRDAxis, RefDilatedLDAxis, RefDilatedRNAxis, RefDilatedLNAxis, RefDilatedRDVA, RefDilatedLDVA,
                                           RefDilatedRNVA, RefDilatedLNVA, RefFinalRDSph, RefFinalLDSph, RefFinalRNSph, RefFinalLNSph, RefFinalRDCyl, RefFinalLDCyl, RefFinalRNCyl,
                                           RefFinalLNCyl, RefFinalRDAxis, RefFinalLDAxis, RefFinalRNAxis, RefFinalLNAxis, RefFinalRDVA, RefFinalLDVA, RefFinalRNVA, RefFinalLNVA,
                                           passIPD, K1RPower, K1RAxis, K1LPower, K1LAxis, K2RPower, K2RAxis, K2LPower, K2LAxis, AddNewPreliminaryExaminationFragment.PreliminaryExminationId);


                                   AddNewPreliminaryExaminationFragment.changePage(2);
                               }
                           }
                       } catch (JSONException e) {
                           e.printStackTrace();
                       }
                   }

                   @Override
                   public void onFailure(@NonNull Call<AddPreliminaryExaminationModel> call, @NonNull Throwable t) {
                       Common.insertLog("Failure:::> " + t.getMessage());
                   }
               });

           }catch (Exception e){
               Common.insertLog(e.getMessage());
           }
       }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;
        if (isStarted && isVisible) {
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
    }

    /**
     * Set Preliminary Examination Value
     * @param data_save - String Data
     */
    private static void setUpdate(String data_save) {
        if (data_save!=null
                && !data_save.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){

            try {
                JSONObject jsonObject=new JSONObject(data_save);
                Common.insertLog("JSON Print..."+jsonObject.toString());
                //JSONObject objectMethod=jsonObject.getJSONObject(WebFields.ADD_PRELIMINARY_VISION.MODE);

                JSONObject modeObject=jsonObject.getJSONObject(WebFields.BODY);

                //set Spinner UCVA Distance
                setSpinnerDistanceData(mSpinnerUCVALeftEyeDistance,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_UCAVDLEOS));
                setSpinnerDistanceData(mSpinnerUCVARightEyeDistance,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_UCAVDREOD));

                // set Spinner UCVA Near
                setSpinnerNearData(mSpinnerUCVALeftEyeNear,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_UCAVNLEOS));
                setSpinnerNearData(mSpinnerUCVARightEyeNear,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_UCAVNREOD));
                mEditUCVARemarks.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_UCAV_REMARKS));

                //set Spinner BCVA Distance
                setSpinnerDistanceData(mSpinnerBCVAUnDilatedRightDistance,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_BCVAUNDILATEDDREOD));
                setSpinnerDistanceData(mSpinnerBCVADilatedRightDistance,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_BCVADILATEDDREOD));
                setSpinnerDistanceData(mSpinnerBCVAUnDilatedLeftDistance,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_BCVAUNDILATEDDLEOS));
                setSpinnerDistanceData(mSpinnerBCVADilatedLeftDistance,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_BCVADILATEDDLEOS));

                //set Spinner BCVA Near
                setSpinnerNearData(mSpinnerBCVAUnDilatedRightNear,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_BCVAUNDILATEDNREOD));
                setSpinnerNearData(mSpinnerBCVADilatedRightNear,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_BCVADILATEDNREOD));
                setSpinnerNearData(mSpinnerBCVAUnDilatedLeftNear,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_BCVAUNDILATEDNLEOS));
                setSpinnerNearData(mSpinnerBCVADilatedLeftNear,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_BCVADILATEDNLEOS));

                mEditBCVARemarks.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_BCVADILATEDREMARKS));

                // Refraction  Undilated Right Eye Distance
                mEditRefractionUnDilatedRightDistanceSph.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDRDSPH));
                mEditRefractionUnDilatedRightDistanceCyl.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDRDCYL));
                mEditRefractionUnDilatedRightAxis.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDRDAXIS));
                setSpinnerDistanceData(mSpinnerRefractionUnDilatedRightVA,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDRDVA));
                // mSpinnerRefractionUnDilatedRightVA.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDRDVA));

                // Refraction  Undilated Right Eye Near
                mEditRefractionUnDilatedRightNearSph.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDRNSPH));
                mEditRefractionUnDilatedRightNearCyl.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDRNCYL));
                mEditRefractionUnDilatedRightNearAxis.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDRNAXIS));
                setSpinnerNearData(mSpinnerRefractionUnDilatedRightNearVA,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDRNVA));
                //mSpinnerRefractionUnDilatedRightNearVA.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDRNVA));

                // Refraction  Undilated Left Eye Distance
                mEditRefractionUnDilatedLeftDistanceSph.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDLDSPH));
                mEditRefractionUnDilatedLeftDistanceCyl.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDLDCYL));
                mEditRefractionUnDilatedLeftAxis.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDLDAXIS));
                // mSpinnerRefractionUnDilatedLeftVA.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDLDVA));
                setSpinnerDistanceData(mSpinnerRefractionUnDilatedLeftVA,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDLDVA));

                // Refraction  Undilated Left Eye Near
                mEditRefractionUnDilatedLeftNearSph.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDLNSPH));
                mEditRefractionUnDilatedLeftNearCyl.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDLNCYL));
                mEditRefractionUnDilatedLeftNearAxis.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDLNAXIS));
                //  mSpinnerRefractionUnDilatedLeftNearVA.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDLNVA));
                setSpinnerNearData(mSpinnerRefractionUnDilatedLeftNearVA,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDLNVA));

                // Refraction  dilated Right Eye Distance
                mEditRefractionDilatedRightDistanceSph.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDRDSPH));
                mEditRefractionDilatedRightDistanceCyl.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDRDCYL));
                mEditRefractionDilatedRightAxis.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDRDAXIS));
                // mEditRefractionDilatedRightVA.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDRDVA));
                setSpinnerDistanceData(mSpinnerRefractionDilatedRightVA,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDRDVA));

                // Refraction  dilated Right Eye Near
                mEditRefractionDilatedRightNearSph.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDRNSPH));
                mEditRefractionDilatedRightNearCyl.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDRNCYL));
                mEditRefractionDilatedRightNearAxis.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDRNAXIS));
                //mSpinnerRefractionDilatedRightNearVA.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDRNVA));
                setSpinnerNearData(mSpinnerRefractionDilatedRightNearVA,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDRNVA));

                // Refraction  Dilated Left Eye Distance
                mEditRefractionDilatedLeftDistanceSph.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDLDSPH));
                mEditRefractionDilatedLeftDistanceCyl.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDLDCYL));
                mEditRefractionDilatedLeftAxis.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDLDAXIS));
                //mSpinnerRefractionDilatedLeftVA.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDLDVA));
                setSpinnerDistanceData(mSpinnerRefractionDilatedLeftVA,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDLDVA));

                // Refraction  Dilated Right Eye Near
                mEditRefractionDilatedLeftNearSph.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDLNSPH));
                mEditRefractionDilatedLeftNearCyl.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDLNCYL));
                mEditRefractionDilatedLeftNearAxis.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDLNAXIS));
                //mSpinnerRefractionDilatedLeftNearVA.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDLNVA));
                setSpinnerNearData(mSpinnerRefractionDilatedLeftNearVA,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDLNVA));

                // Refraction Final Right Eye Distance
                mEditRefractionFinalRightDistanceSph.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALRDSPH));
                mEditRefractionFinalRightDistanceCyl.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALRDCYL));
                mEditRefractionFinalRightAxis.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALRDAXIS));
                //mSpinnerRefractionFinalRightVA.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALRDVA));
                setSpinnerDistanceData(mSpinnerRefractionFinalRightVA,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALRDVA));

                // Refraction  Final Right Eye Near
                mEditRefractionFinalRightNearSph.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALRNSPH));
                mEditRefractionFinalRightNearCyl.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALRNCYL));
                mEditRefractionFinalRightNearAxis.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALRNAXIS));
                //mSpinnerRefractionFinalRightNearVA.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALRNVA));
                setSpinnerNearData(mSpinnerRefractionFinalRightNearVA,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALRNVA));

                // Refraction  Final Left Eye Distance
                mEditRefractionFinalLeftDistanceSph.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALLDSPH));
                mEditRefractionFinalLeftDistanceCyl.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALLDCYL));
                mEditRefractionFinalLeftAxis.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALLDAXIS));
                //mSpinnerRefractionFinalLeftDistanceVA.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALLDVA));
                setSpinnerDistanceData(mSpinnerRefractionFinalLeftDistanceVA,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALLDVA));

                // Refraction  Final Right Eye Near
                mEditRefractionFinalLeftNearSph.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALLNSPH));
                mEditRefractionFinalLeftNearCyl.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALLNCYL));
                mEditRefractionFinalLeftNearAxis.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALLNAXIS));
                //mSpinnerRefractionFinalLeftNearVA.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALLNVA));
                setSpinnerNearData(mSpinnerRefractionFinalLeftNearVA,modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALLNVA));

                mEditKeratometryKOneRightPower.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_K1RPOWER));
                mEditKeratometryKOneRightAxis.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_K1RAXIS));
                mEditKeratometryKOneLeftPower.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_K1LPOWER));
                mEditKeratometryKOneLeftAxis.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_K1LAXIS));
                mEditKeratometryKTwoRightPower.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_K2RPOWER));
                mEditKeratometryKTwoRightAxis.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_K2RAXIS));
                mEditKeratometryKTwoLeftPower.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_K2LPOWER));
                mEditKeratometryKTwoLeftAxis.setText(modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_K2LAXIS));


                //set IPD Adapter
                if (!modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALIPD).
                        equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){

                    String[] ipd=modeObject.getString(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALIPD).
                            split(",");

                    if (mArrIPD.size()>0){

                        for (int i=0;i<ipd.length;i++) {
                            for (int a = 0; a < mArrIPD.size(); a++) {
                                if (ipd[i].equals(mArrIPD.get(a).getIPD())){
                                    mArrIPD.get(a).setSelected(true);
                                }
                            }
                        }
                        IpdAdapter.notifyDataSetChanged();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private static void setHistorySelecteData(Context context, PreliminaryExaminationModel checkInData) {

        String mUserId = GetJsonData.getLoginData(context, WebFields.LOGIN.RESPONSE_USER_ID);
        String mDatabaseName = GetJsonData.getHospitalData(context, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

        String data_save= APICommonMethods.setAddPreliminaryVisionJson(mUserId, mDatabaseName,checkInData.getAppointmentID(),checkInData.getPatientID(),checkInData.getDoctorID(),checkInData.getTreatmentDate(),
                checkInData.getUCVADR(),checkInData.getUCVANR(),checkInData.getUCVADL(),checkInData.getUCVANL(),checkInData.getUCVARemarks(),checkInData.getBCVAUDR(), checkInData.getBCVAUNR(),
                checkInData.getBCVAUDL(),checkInData.getBCVAUNL(),checkInData.getBCVADDR(),checkInData.getBCVADNR(), checkInData.getBCVADDL(),
                checkInData.getBCVADNL(),checkInData.getBCVARemarks(),checkInData.getRefUDRSph(),checkInData.getRefUDLSph(),checkInData.getRefUNRSph(), checkInData.getRefUNLSph(),
                checkInData.getRefUDRCyl(),checkInData.getRefUDLCyl(),checkInData.getRefUNRCyl(),checkInData.getRefUNLCyl(),checkInData.getRefUDRAxis(),checkInData.getRefUDLAxis(),checkInData.getRefUNRAxis(),
                checkInData.getRefUNLAxis(),checkInData.getRefUDRVA(),checkInData.getRefUDLVA(),checkInData.getRefUNRVA(),checkInData.getRefUNLVA(),
                checkInData.getRefDDRSph(), checkInData.getRefDDLSph(),checkInData.getRefDNRSph(),checkInData.getRefDNLSph(), checkInData.getRefDDRCyl(),checkInData.getRefDDLCyl(),checkInData.getRefDNRCyl(),
                checkInData.getRefDNLCyl(),checkInData.getRefDDRAxis(),checkInData.getRefDDLAxis(),checkInData.getRefDNRAxis(),checkInData.getRefDNLAxis(),checkInData.getRefDDRVA(), checkInData.getRefDDLVA(),
                checkInData.getRefDNRVA(), checkInData.getRefDNLVA(),checkInData.getRefFDRSph(), checkInData.getRefFDLSph(), checkInData.getRefFNRSph(),checkInData.getRefFNLSph(),checkInData.getRefFDRCyl(),checkInData.getRefFDLCyl(),checkInData.getRefFNRCyl(),
                checkInData.getRefFNLCyl(),checkInData.getRefFDRAxis(), checkInData.getRefFDLAxis(),checkInData.getRefFNRAxis(),checkInData.getRefFNLAxis(),checkInData.getRefFDRVA(),checkInData.getRefFDLVA(),checkInData.getRefFNRVA(),checkInData.getRefFNLVA(),
                checkInData.getIPD(),checkInData.getK1RPower(),checkInData.getK1RAxis(),checkInData.getK1LPower(),checkInData.getK1LAxis(),checkInData.getK2RPower(),checkInData.getK2RAxis(),checkInData.getK2LPower(), checkInData.getK2LAxis(), AddNewPreliminaryExaminationFragment.PreliminaryExminationId);


        mRelativeNoData.setVisibility(View.GONE);
        Common.insertLog(" vision.."+data_save);
        setUpdate(data_save);
    }

    /**
     * Set Spinner Distance
     * @param spinner -Spinner
     * @param string -Distance Value
     */
    private static void setSpinnerDistanceData(Spinner spinner, String string) {
      try {
          if (mArrDistance.size()>0){
              for(int a=0;a<mArrDistance.size();a++){
                  if (mArrDistance.get(a).equals(string))
                  {
                      spinner.setSelection(a,true);
                  }
              }
          }

      }catch (Exception e){
          Common.insertLog(e.getMessage());
      }
    }

    /**
     * Set Spinner Near
     * @param spinner -Spinner
     * @param string -Near Value
     */
    private static void setSpinnerNearData(Spinner spinner, String string) {
        try {
            if (mArrNear.size()>0){
                for(int a=0;a<mArrNear.size();a++){
                    if (mArrNear.get(a).equals(string))
                    {
                        spinner.setSelection(a,true);
                    }
                }
            }
        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Call API History Vision
     */
    private void callToHistoryVision() {
        try {

            String defualt_date="1000-01-01";
            String Mobile=AppConstants.STR_EMPTY_STRING;
            String Type="Diagnosis";

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setHistoryPreliminaryExaminationJson(defualt_date,mCurrentPage,mPatientId,Mobile,mHistoryDoctorID,Type,hospital_database));

            Call<HistoryPreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationHistory(body);
            call.enqueue(new Callback<HistoryPreliminaryExaminationModel>() {
                @Override
                public void onResponse(@NonNull Call<HistoryPreliminaryExaminationModel> call, Response<HistoryPreliminaryExaminationModel> response) {
                    Common.insertLog("Response History::::> " + new Gson().toJson(response.body()));
                    try {
                        if (response.body()!=null &&
                                response.body().getError()==AppConstants.API_SUCCESS_ERROR)
                        {
                            if(response.body().getData().size()>0) {

                                mArrHistory.addAll(response.body().getData());
                                setTodayData();
                            }
                        }
                    }catch (Exception e){
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<HistoryPreliminaryExaminationModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

