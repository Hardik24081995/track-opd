package com.trackopd.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.theartofdev.edmodo.cropper.CropImage;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.ImagePickerActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.model.AddPatientModel;
import com.trackopd.model.AddUploadPicModel;
import com.trackopd.model.AreaModel;
import com.trackopd.model.CityModel;
import com.trackopd.model.CountryModel;
import com.trackopd.model.StateModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppPermissions;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.CameraGalleryPermission;
import com.trackopd.utils.Common;
import com.trackopd.utils.Compressor;
import com.trackopd.utils.ImageUtil;
import com.trackopd.utils.KeyboardUtility;
import com.trackopd.utils.SessionManager;
import com.trackopd.utils.StringUtils;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.trackopd.webServices.APICommonMethods.setReceptionistAddPatientJson;

public class AddPatientFragment extends Fragment {

    // Camera & Gallery Variables
    public static final int REQUEST_CAMERA = 101;
    public static final int REQUEST_GALLERY = 102;
    public static final int REQUEST_IMAGE = 100;
    public String mFilePath = "", mUserType = "";
    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private View mView, mViewState;
    private Menu menu;
    private Activity mActivity;
    private RadioButton mRadioMale, mRadioFemale;
    private EditText mEditFirstName, mEditMiddleName, mEditLastName, mEditMobile, mEditEmail, mEditOccupation, mEditAddress, mEditCity, mEditArea, mEditPinCode, mEditBirthDate, mEditAge;
    private ImageView mImagePatientPicture, mImageState, mImageCity, mImageArea, mImageCountry, mImageRemoveCity, mImageRemoveArea, mImageAddCity, mImageAddArea;
    private Spinner mSpinnerState, mSpinnerCity, mSpinnerArea, mSpinnerCountry, mSpinnerNameTitle;
    private RelativeLayout mRelativeAddNewCity, mRelativeAddNewArea;
    private LinearLayout mLinearUploadPic, mLinearState, mLinearCity, mLinearArea, mLinearCitySpinner, mLinearAreaSpinner;
    private RadioGroup mRadioGender;
    private Button mButtonSubmit;
    private ArrayList<String> mArrCountry, mArrState, mArrCity, mArrArea, mArrPrefix;
    private ArrayList<CountryModel> mArrCountryList;
    private ArrayList<StateModel> mArrStateList;
    private ArrayList<CityModel> mArrCityList;
    private ArrayList<AreaModel> mArrAreaList;
    private int mSelectedCountryIndex, mSelectedStateIndex, mSelectedCityIndex, mSelectedAreaIndex, mCountryId, mStateId, mCityId, mAreaId;
    private String mPatientId, mFirstName, mMiddleName, mLastName, mMobile, mEmail, mPinCode, mOccupation,
            mAddress, mGender, mCity, mArea, mCityName, mAreaName, mImagePath;
    private Boolean image_upload = false;
    private String chosenTask;
    private File destination;
    private Bitmap mBitmap;
    private Uri mImageUri;
    private File mPath;
    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.linear_add_patient_upload_picture:
//                    Dexter.withActivity(getActivity())
//                            .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                            .withListener(new MultiplePermissionsListener() {
//                                @Override
//                                public void onPermissionsChecked(MultiplePermissionsReport report) {
//                                    if (report.areAllPermissionsGranted()) {
//                                        showImagePickerOptions();
//                                    }
//
//                                    if (report.isAnyPermissionPermanentlyDenied()) {
//                                        showSettingsDialog();
//                                    }
//                                }
//
//                                @Override
//                                public void onPermissionRationaleShouldBeShown(List< PermissionRequest > permissions, PermissionToken
//                                        token) {
//                                    token.continuePermissionRequest();
//                                }
//                            }).check();

                    openGalleryPopup();
                    break;

                case R.id.image_add_patient_add_city:
                    addNewCity();
                    break;

                case R.id.image_add_patient_city_remove:
                    removeNewCity();
                    break;

                case R.id.image_add_patient_add_new_area:
                    addNewArea();
                    break;

                case R.id.image_add_patient_area_remove:
                    removeNewArea();
                    break;

                case R.id.button_add_patient_submit:
                    doAddPatient();
                    break;

                case R.id.edit_add_patient_bod:
                    Common.openPastDatePicker(mActivity, mEditBirthDate);
                    break;
            }
        }
    };
    /**
     * Checked Change listeners (Radio Button)
     */
    private RadioGroup.OnCheckedChangeListener checkedChangeListener = new RadioGroup.OnCheckedChangeListener() {

        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            RadioButton mRadioButton = group.findViewById(checkedId);
            mRadioButton.clearFocus();

            switch (mRadioButton.getId()) {
                case R.id.radio_group_add_patient_gender:
                    int selId = group.getCheckedRadioButtonId();
                    if (selId == 1) {
                        mGender = mActivity.getResources().getString(R.string.text_gender_male);
                    } else if (selId == 2) {
                        mGender = mActivity.getResources().getString(R.string.text_gender_female);
                    } else {
                        mGender = mActivity.getResources().getString(R.string.text_gender_others);
                    }
                    break;
            }
        }
    };
    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {

                case R.id.spinner_add_patient_country:
                    mSelectedCountryIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedCountryIndex != 0) {
                        mLinearState.setVisibility(View.VISIBLE);
                        mViewState.setVisibility(View.VISIBLE);
                        mCountryId = (Integer.parseInt(mArrCountryList.get(mSelectedCountryIndex - 1).getCountryID()));
                        String mCountryName = (mArrCountryList.get(mSelectedCountryIndex - 1).getCountryName());
                        Common.insertLog("mCountryId::> " + mCountryId);
                        Common.insertLog("mCountryName::> " + mCountryName);
                        callToStateAPI(mCountryId);
                    } else {
                        mLinearState.setVisibility(View.GONE);
                        mViewState.setVisibility(View.GONE);
                        //mLinearArea.setVisibility(View.GONE);
                        mLinearCity.setVisibility(View.GONE);
                    }
                    break;

                case R.id.spinner_add_patient_state:
                    mSelectedStateIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedStateIndex != 0) {
                        mLinearCity.setVisibility(View.VISIBLE);
                        mStateId = (Integer.parseInt(mArrStateList.get(mSelectedStateIndex - 1).getStateID()));
                        String mStateName = (mArrStateList.get(mSelectedStateIndex - 1).getStateName());
                        Common.insertLog("mStateId::> " + mStateId);
                        Common.insertLog("mStateName::> " + mStateName);
                        callToCityAPI(mStateId);
                    } else {
                        mLinearCity.setVisibility(View.GONE);
                        // mLinearArea.setVisibility(View.GONE);
                    }
                    break;

                case R.id.spinner_add_patient_city:
                    mSelectedCityIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedCityIndex != 0) {
                        mLinearArea.setVisibility(View.VISIBLE);
                        mCityId = (Integer.parseInt(mArrCityList.get(mSelectedCityIndex - 1).getCityID()));
                        String mCityName = (mArrCityList.get(mSelectedCityIndex - 1).getCityName());
                        Common.insertLog("mCityId::> " + mCityId);
                        Common.insertLog("mCityName::> " + mCityName);
                        callToAreaAPI(mCityId);
                    } else {
                        // mLinearArea.setVisibility(View.GONE);
                    }
                    break;

                case R.id.spinner_add_patient_area:
                    mSelectedAreaIndex = adapterView.getSelectedItemPosition();

                    if (mSelectedAreaIndex != 0) {
                        mAreaId = (Integer.parseInt(mArrAreaList.get(mSelectedAreaIndex - 1).getAreaID()));
                        String mAreaName = (mArrAreaList.get(mSelectedAreaIndex - 1).getAreaName());
                        Common.insertLog("mAreaId::> " + mAreaId);
                        Common.insertLog("mAreaName::> " + mAreaName);
                    }
                    break;

            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(mActivity, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

//    @Override
//    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        if (requestCode == REQUEST_IMAGE) {
//            if (resultCode == Activity.RESULT_OK) {
//                Uri uri = data.getParcelableExtra("path");
//                try {
//                    // You can update this bitmap to your server
//                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(mActivity.getContentResolver(), uri);
//
//                    // loading profile image from local cache
//                    loadProfile(uri.toString());
////                    mFilePath = uri.toString();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    }

    private void launchCameraIntent() {
        Intent intent = new Intent(mActivity, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(mActivity, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void loadProfile(String url) {
        Log.d("TAG", "Image cache path: " + url);


        mImagePatientPicture.setVisibility(View.VISIBLE);

        Glide.with(this).load(url)
                .into(mImagePatientPicture);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_add_patient, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrCountry = new ArrayList<>();
        mArrState = new ArrayList<>();
        mArrCity = new ArrayList<>();
        mArrArea = new ArrayList<>();
        mArrCountryList = new ArrayList<>();
        mArrStateList = new ArrayList<>();
        mArrCityList = new ArrayList<>();
        mArrAreaList = new ArrayList<>();
        mArrPrefix = new ArrayList<>();

        getBundle();
        getIds();
        setRegListeners();
        setData();
        callToCountryAPI();
        setHasOptionsMenu(true);

        InputFilter[] fa = new InputFilter[1];
        fa[0] = new InputFilter.LengthFilter(25);
        mEditAddress.setFilters(fa);
        mEditArea.setFilters(fa);

        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);

        MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mUserType = bundle.getString(AppConstants.BUNDLE_LOGIN_TYPE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Edit Texts
            mEditFirstName = mView.findViewById(R.id.edit_add_patient_first_name);
            mEditLastName = mView.findViewById(R.id.edit_add_patient_last_name);
            mEditMobile = mView.findViewById(R.id.edit_add_patient_mobile_no);
            mEditEmail = mView.findViewById(R.id.edit_add_patient_email);
            mEditOccupation = mView.findViewById(R.id.edit_add_patient_occupation);
            mEditAddress = mView.findViewById(R.id.edit_add_patient_address);
            mEditCity = mView.findViewById(R.id.edit_add_patient_add_new_city);
            mEditArea = mView.findViewById(R.id.edit_add_patient_add_new_area);
            mEditPinCode = mView.findViewById(R.id.edit_add_patient_pincode);
            mEditBirthDate = mView.findViewById(R.id.edit_add_patient_bod);
            mEditMiddleName = mView.findViewById(R.id.edit_add_patient_middle_name);
            mEditAge = mView.findViewById(R.id.edit_add_patient_age);

            // Image Views
            mImageState = mView.findViewById(R.id.image_add_patient_state);
            mImageCity = mView.findViewById(R.id.image_add_patient_city);
            mImageArea = mView.findViewById(R.id.image_add_patient_area);
            mImageCountry = mView.findViewById(R.id.image_add_patient_country);
            mImageAddCity = mView.findViewById(R.id.image_add_patient_add_city);
            mImageAddArea = mView.findViewById(R.id.image_add_patient_add_new_area);
            mImageRemoveCity = mView.findViewById(R.id.image_add_patient_city_remove);
            mImageRemoveArea = mView.findViewById(R.id.image_add_patient_area_remove);
            mImagePatientPicture = mView.findViewById(R.id.image_add_patient_picture);

            // Edit Texts
            mSpinnerCountry = mView.findViewById(R.id.spinner_add_patient_country);
            mSpinnerState = mView.findViewById(R.id.spinner_add_patient_state);
            mSpinnerCity = mView.findViewById(R.id.spinner_add_patient_city);
            mSpinnerArea = mView.findViewById(R.id.spinner_add_patient_area);
            mSpinnerNameTitle = mView.findViewById(R.id.spinner_add_patient_name_title);

            // Radio groups
            mRadioGender = mView.findViewById(R.id.radio_group_add_patient_gender);

            // Radio Buttons
            mRadioMale = mView.findViewById(R.id.radio_button_add_patient_gender_male);
            mRadioFemale = mView.findViewById(R.id.radio_button_add_patient_gender_female);

            // Buttons
            mButtonSubmit = mView.findViewById(R.id.button_add_patient_submit);

            // Relative Layouts
            mRelativeAddNewCity = mView.findViewById(R.id.relative_add_patient_add_city);
            mRelativeAddNewArea = mView.findViewById(R.id.relative_add_patient_add_area);

            // Linear Layouts
            mLinearUploadPic = mView.findViewById(R.id.linear_add_patient_upload_picture);
            mLinearCity = mView.findViewById(R.id.linear_add_patient_city);
            mLinearArea = mView.findViewById(R.id.linear_add_patient_area);
            mLinearState = mView.findViewById(R.id.linear_add_patient_state);
            mLinearCitySpinner = mView.findViewById(R.id.linear_add_patient_city_spinner);
            mLinearAreaSpinner = mView.findViewById(R.id.linear_add_patient_area_spinner);

            // Views
            mViewState = mView.findViewById(R.id.view_add_patient_state);

            // Set Request Focus
            mEditFirstName.requestFocus();

            mEditBirthDate.setKeyListener(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Click Listeners
            mLinearUploadPic.setOnClickListener(clickListener);
            mImageAddCity.setOnClickListener(clickListener);
            mImageRemoveCity.setOnClickListener(clickListener);
            mImageAddArea.setOnClickListener(clickListener);
            mImageRemoveArea.setOnClickListener(clickListener);
            mButtonSubmit.setOnClickListener(clickListener);

            // ToDo: Radio Button Click Listeners
            mRadioGender.setOnCheckedChangeListener(checkedChangeListener);

            // ToDo: Doctor - Spinner Item Select Listener
            mSpinnerCountry.setOnItemSelectedListener(onItemSelectedListener);
            mSpinnerState.setOnItemSelectedListener(onItemSelectedListener);
            mSpinnerCity.setOnItemSelectedListener(onItemSelectedListener);
            mSpinnerArea.setOnItemSelectedListener(onItemSelectedListener);

            mEditBirthDate.setOnClickListener(clickListener);

            mEditCity.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    if (!charSequence.toString().equalsIgnoreCase("")) {
                        mLinearArea.setVisibility(View.VISIBLE);
                    } else {
                        // mLinearArea.setVisibility(View.GONE);
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            //Disable Emoji
            mEditAddress.setFilters(Common.getFilter());
            mEditArea.setFilters(Common.getFilter());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            // Set Image Color as per the theme applied
            mImageState.setColorFilter(Common.setThemeColor(mActivity));
            mImageCity.setColorFilter(Common.setThemeColor(mActivity));
            mImageArea.setColorFilter(Common.setThemeColor(mActivity));
            mImageCountry.setColorFilter(Common.setThemeColor(mActivity));

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerState.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerCity.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerArea.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerNameTitle.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);


            // Set Data
            mEditBirthDate.setText(Common.setCurrentDate(mActivity));

            //Set Spinner Name Title
            mArrPrefix.addAll(Arrays.asList(mActivity.getResources().getStringArray(R.array.name_title)));

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerNameTitle.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_dropdown_item_1line, mArrPrefix);
            mSpinnerNameTitle.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Country API
     */
    private void callToCountryAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setCountryListJson(hospital_database));

            Call<CountryModel> call = RetrofitClient.createService(ApiInterface.class).getCountryList(body);
            call.enqueue(new Callback<CountryModel>() {
                @Override
                public void onResponse(@NonNull Call<CountryModel> call, @NonNull Response<CountryModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                mArrCountryList.addAll(response.body().getData());
                                setCountryAdapter();
                            }
                        } else {
                            setCountryAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CountryModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setCountryAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    public void setCountryAdapter() {
        try {
            mArrCountry.add(0, getString(R.string.spinner_select_country));
            if (mArrCountryList.size() > 0) {
                for (CountryModel country : mArrCountryList) {
                    mArrCountry.add(country.getCountryName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerCountry.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, R.layout.support_simple_spinner_dropdown_item, mArrCountry) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCountryIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerCountry.setAdapter(adapter);

            // Default Selected India Country
            for (int c = 0; c < mArrCountry.size(); c++) {
                if (mArrCountry.get(c).equalsIgnoreCase("India")) {
                    mSpinnerCountry.setSelection(c);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the State API
     */
    private void callToStateAPI(int mCountryId) {
        try {
            if (mArrState.size() > 0)
                mArrState.clear();
            if (mArrStateList.size() > 0)
                mArrStateList.clear();

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setStateListJson(String.valueOf(mCountryId), hospital_database));

            Call<StateModel> call = RetrofitClient.createService(ApiInterface.class).getStateList(body);
            call.enqueue(new Callback<StateModel>() {
                @Override
                public void onResponse(@NonNull Call<StateModel> call, @NonNull Response<StateModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                mArrStateList.addAll(response.body().getData());
                                setStateAdapter();
                            }
                        } else {
                            setStateAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<StateModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setStateAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    public void setStateAdapter() {
        try {
            mSpinnerState.setAdapter(null);
            mArrState.add(getString(R.string.spinner_select_state));

            if (mArrStateList.size() > 0) {
                for (StateModel state : mArrStateList) {
                    mArrState.add(state.getStateName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerState.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrState) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedStateIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerState.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the City API
     */
    private void callToCityAPI(int mStateId) {
        try {
            if (mArrCity.size() > 0)
                mArrCity.clear();
            if (mArrCityList.size() > 0)
                mArrCityList.clear();

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setCityListJson(String.valueOf(mStateId), hospital_database));

            Call<CityModel> call = RetrofitClient.createService(ApiInterface.class).getCityList(body);
            call.enqueue(new Callback<CityModel>() {
                @Override
                public void onResponse(@NonNull Call<CityModel> call, @NonNull Response<CityModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                mArrCityList.addAll(response.body().getData());
                                setCityAdapter();
                            }
                        } else {
                            setCityAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CityModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setCityAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    public void setCityAdapter() {
        try {
            mArrCity.add(0, getString(R.string.spinner_select_city));

            if (mArrCityList.size() > 0) {
                for (CityModel cityModel : mArrCityList) {
                    mArrCity.add(cityModel.getCityName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerCity.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrCity) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCityIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerCity.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Area API
     */
    private void callToAreaAPI(int mCityId) {
        try {
            if (mArrAreaList.size() > 0)
                mArrAreaList.clear();
            if (mArrArea.size() > 0)
                mArrArea.clear();

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAreaListJson(String.valueOf(mCityId), hospital_database));

            Call<AreaModel> call = RetrofitClient.createService(ApiInterface.class).getAreaList(body);
            call.enqueue(new Callback<AreaModel>() {
                @Override
                public void onResponse(@NonNull Call<AreaModel> call, @NonNull Response<AreaModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                mArrAreaList.addAll(response.body().getData());
                                setAreaAdapter();
                            }
                        } else {
                            setAreaAdapter();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AreaModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setAreaAdapter();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    public void setAreaAdapter() {
        try {
            mArrArea.add(0, getString(R.string.spinner_select_area));

            if (mArrAreaList.size() > 0) {
                for (AreaModel areaModel : mArrAreaList) {
                    mArrArea.add(areaModel.getAreaName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerArea.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrArea) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedAreaIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerArea.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    /*public void setYearAdapter() {
        try {
            mArrAgeYear.add(0, getString(R.string.spinner_select_year));

            for (int y = 0; y < 100; y++) {
                mArrAgeYear.add(y + AppConstants.STR_EMPTY_STRING);
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerAgeYear.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrAgeYear) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);

                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedAgeYear) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerAgeYear.setAdapter(adapter);
            mSpinnerAgeYear.setSelection(19);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /**
     * Sets the data and bind it to the adapter
     */
    /*public void setAgeMonthsAdapter() {
        try {
            mArrAgeMonth.add(0, getString(R.string.spinner_select_age_month));

            for (int y = 0; y <= 12; y++) {
                mArrAgeMonth.add(y + AppConstants.STR_EMPTY_STRING);
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerAgeMonth.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrAgeMonth) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedAgeMonth) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerAgeMonth.setAdapter(adapter);
            mSpinnerAgeMonth.setSelection(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /**
     * Add new city (Visible the edit text field and disable the spinner layout)
     */
    private void addNewCity() {
        mLinearCitySpinner.setVisibility(View.GONE);
        mRelativeAddNewCity.setVisibility(View.VISIBLE);
        mImageAddCity.setVisibility(View.GONE);
    }

    /**
     * Remove new city (Visible the spinner layout and disable the edit text field)
     */
    private void removeNewCity() {
        mLinearCitySpinner.setVisibility(View.VISIBLE);
        mRelativeAddNewCity.setVisibility(View.GONE);
        mImageAddCity.setVisibility(View.VISIBLE);
        mEditCity.setText(AppConstants.STR_EMPTY_STRING);
    }

    /**
     * Add new area (Visible the edit text field and disable the spinner layout)
     */
    private void addNewArea() {
        mLinearAreaSpinner.setVisibility(View.GONE);
        mRelativeAddNewArea.setVisibility(View.VISIBLE);
        mImageAddArea.setVisibility(View.GONE);
    }

    /**
     * Remove new area (Visible the spinner layout and disable the edit text field)
     */
    private void removeNewArea() {
        mLinearAreaSpinner.setVisibility(View.VISIBLE);
        mRelativeAddNewArea.setVisibility(View.GONE);
        mImageAddArea.setVisibility(View.VISIBLE);
        mEditArea.setText(AppConstants.STR_EMPTY_STRING);
    }

    /**
     * This method checks the validation first then call the API
     */
    private void doAddPatient() {
        KeyboardUtility.HideKeyboard(mActivity, mEditFirstName);
        if (checkValidation()) {
            callToAddPatientAPI();
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {
        boolean status = true;

        SessionManager manager = new SessionManager(mActivity);
        manager.setPreferences(manager.PID, "appointment");

        mFirstName = mEditFirstName.getText().toString().trim();
        mLastName = mEditLastName.getText().toString().trim();
        mMobile = mEditMobile.getText().toString().trim();
        mEmail = mEditEmail.getText().toString().trim();
        mOccupation = mEditOccupation.getText().toString().trim();
        mAddress = mEditAddress.getText().toString().trim();
        mCity = mEditCity.getText().toString().trim();
        mArea = mEditArea.getText().toString().trim();
        mPinCode = mEditPinCode.getText().toString().trim();
        mMiddleName = mEditMiddleName.getText().toString().trim();

        mEditFirstName.setError(null);
        mEditLastName.setError(null);
        mEditMobile.setError(null);
        mEditOccupation.setError(null);
        mEditAddress.setError(null);
        mEditMiddleName.setError(null);

        if (TextUtils.isEmpty(mFirstName)) {
            mEditFirstName.requestFocus();
            mEditFirstName.setError(getResources().getString(R.string.error_field_required));
            status = false;
        }
        if (TextUtils.isEmpty(mLastName)) {
            mEditLastName.requestFocus();
            mEditLastName.setError(getResources().getString(R.string.error_field_required));
            status = false;
        }
        if (TextUtils.isEmpty(mMobile)) {
            mEditMobile.requestFocus();
            mEditMobile.setError(getResources().getString(R.string.error_field_required));
            status = false;
        }
        if (!mEmail.equalsIgnoreCase("")) {
            if (!mEmail.matches(emailPattern)) {
                mEditEmail.requestFocus();
                mEditEmail.setError("Enter Valid Email");
                status = false;
            }
        }


        /*if (TextUtils.isEmpty(mPinCode)) {
            mEditPinCode.requestFocus();
            mEditPinCode.setError(getResources().getString(R.string.error_field_required));
            status = false;
        }*/
        /*if (TextUtils.isEmpty(mOccupation)) {
            mEditOccupation.requestFocus();
            mEditOccupation.setError(getResources().getString(R.string.error_field_required));
            status = false;
        }
        if (TextUtils.isEmpty(mAddress)) {
            mEditAddress.requestFocus();
            mEditAddress.setError(getResources().getString(R.string.error_field_required));
            status = false;
        }
        */
        if (mRadioMale.isChecked()) {
            mGender = mActivity.getResources().getString(R.string.text_gender_male);
        } else if (mRadioFemale.isChecked()) {
            mGender = mActivity.getResources().getString(R.string.text_gender_female);
        } else {
            mGender = mActivity.getResources().getString(R.string.text_gender_others);
        }
        return status;
    }

    /**
     * This method should call Add Patient API
     */
    private void callToAddPatientAPI() {

        try {
            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);

            if (mEditCity.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                mCityName = "";
            } else {
                mCityName = mEditCity.getText().toString();
                mCityId = -1;
            }

            if (mEditArea.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                mAreaName = "";
            } else {
                mAreaName = mEditArea.getText().toString();
                mAreaId = -1;
            }

            // Selected Name Prefix
            int selected_prefix = mSpinnerNameTitle.getSelectedItemPosition();
            String NamePrefix = mArrPrefix.get(selected_prefix);

            String age = Common.getAge(mEditBirthDate.getText().toString());
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    setReceptionistAddPatientJson(mFirstName, mLastName, mEmail, mMobile, mCountryId, mStateId,
                            mCityId, mAreaId, mGender, age,mCityName, mAreaName, mUserId, mAddress, mPinCode, mOccupation, NamePrefix, mMiddleName, hospital_database));
            Call<AddPatientModel> call = RetrofitClient.createService(ApiInterface.class).addReceptionistPatient(requestBody);
            call.enqueue(new Callback<AddPatientModel>() {
                @Override
                public void onResponse(@NonNull Call<AddPatientModel> call, @NonNull Response<AddPatientModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {

                            if (response.body() != null) {
                                mPatientId = response.body().getData().get(0).getID();
                                if (StringUtils.isEmpty(mImagePath)) {
                                    Common.setCustomToast(mActivity, mMessage);
                                    openPopup();
                                } else {
                                    doSubmitProfileDetail(mPatientId);
                                }
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddPatientModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open popup for add an appointment
     */
    private void openPopup() {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setCancelable(false);

            String mMessage;
            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                mMessage = mActivity.getResources().getString(R.string.dialog_add_an_inquiry_form_message);
            } else {
                mMessage = mActivity.getResources().getString(R.string.dialog_add_an_appointment_message);
            }

            builder.setMessage(mMessage)
                    .setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                                redirectedToAddCouncillorInquiryFormFragment();
                            } else {
                                redirectedToAddAppointmentFragment();
                            }
                        }
                    })
                    .setNegativeButton(R.string.action_cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            dialog.dismiss();
                            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                                callToInquiryFormFragment();
                            } else {
                                callToPatientFragment();
                            }
                        }
                    });
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method redirected you to the Councillor Add Inquiry Form Fragment
     */
    private void redirectedToAddCouncillorInquiryFormFragment() {
        try {
            ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.header_counselling));
            Fragment fragment = new AddInquiryFormFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, mFirstName);
            args.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, mLastName);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mMobile);
            args.putString(AppConstants.BUNDLE_PATIENT_ID, mPatientId);
            fragment.setArguments(args);
            FragmentManager fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_councillor_inquiry_form))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_councillor_inquiry_form))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Councillor Inquiry Form Fragment
     */
    private void callToInquiryFormFragment() {
        try {
            ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_counselling));
            Fragment fragment = new CouncillorInquiryFormFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_NAME, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_COUNCILLOR_DATE, "1970-01-01");
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_COUNCILLOR, -1);
            fragment.setArguments(args);
            FragmentManager fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    getResources().getString(R.string.nav_menu_counselling)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Receptionist Patient Fragment
     */
    private void callToPatientFragment() {
        try {
            ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_patient));
            Fragment fragment = new ReceptionistPatientFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_IS_FROM_SEARCH, AppConstants.RECEPTIONIST_PATIENT);
            args.putString(AppConstants.BUNDLE_ADD_PATIENT, "1");
            args.putString(AppConstants.BUNDLE_PATIENT_NAME, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, AppConstants.STR_EMPTY_STRING);
            fragment.setArguments(args);
            FragmentManager fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    getResources().getString(R.string.nav_menu_patient)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method redirected you to the Receptionist Add Appointment Fragment
     */
    private void redirectedToAddAppointmentFragment() {
        try {
            String mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_patient))) {
                ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            }

            Fragment fragment = new AddAppointmentFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, mFirstName);
            args.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, mLastName);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mMobile);
            args.putString(AppConstants.BUNDLE_PATIENT_ID, mPatientId);
            args.putString(AppConstants.BUNDLE_ADD_PATIENT_PHOTO_LOCAL, mImagePath);
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_patient))) {
                fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_patient_appointment))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_patient_appointment))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open popup for camera and gallery
     */
    private void openGalleryPopup() {
        final CharSequence[] menus = {mActivity.getResources().getString(R.string.action_capture_image), mActivity.getResources().getString(R.string.action_select_from_gallery),
                mActivity.getResources().getString(R.string.action_cancel)};

        final AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        builder.setTitle(mActivity.getResources().getString(R.string.header_choose_image));
        builder.setItems(menus, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int pos) {

                boolean result = CameraGalleryPermission.checkPermission(mActivity,
                        AppPermissions.ReadWriteExternalStorageRequiredPermission());

                if (pos == 0) {
                    chosenTask = mActivity.getResources().getString(R.string.action_capture_image);
                    if (result) {
                        openCameraIntent();
                    }
                } else if (pos == 1) {
                    chosenTask = mActivity.getResources().getString(R.string.action_select_from_gallery);
                    if (result) {
                        openGalleryIntent();
                    }
                } else if (pos == 2) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /**
     * Open the camera intent
     */
    private void openCameraIntent() {
        destination = AppUtil.currentTimeStampFile();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(destination));
        } else {
            File file = new File(Uri.fromFile(destination).getPath());
            Uri photoUri = FileProvider.getUriForFile(mActivity, mActivity.getPackageName() + ".provider", file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        }

        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        if (intent.resolveActivity(mActivity.getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_CAMERA);
        }
    }

    /**
     * Open the gallery intent
     */
    private void openGalleryIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Show only images, no videos or anything else
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_GALLERY);
    }

    /**
     * Request camera permission dynamically
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case CameraGalleryPermission.INTERNAL_EXTERNAL_PERMISSION:
                Map<String, Integer> perms = new HashMap<>();
                perms.put(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(android.Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                for (int i = 0; i < permissions.length; i++)
                    perms.put(permissions[i], grantResults[i]);
                if (perms.get(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                        && perms.get(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                    if (chosenTask.equals(mActivity.getResources().getString(R.string.action_capture_image))) {
                        openCameraIntent();
                    } else if (chosenTask.equals(mActivity.getResources().getString(R.string.action_select_from_gallery))) {
                        openGalleryIntent();
                    }
                } else {
                    Toast.makeText(mActivity, mActivity.getResources().getString(R.string.error_permission_denied), Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode,resultCode,data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CAMERA:
                    CropImage.activity(Uri.fromFile(destination)).start(mActivity, this);
                    break;
                case REQUEST_GALLERY:
                    mImageUri = data.getData();

                    CropImage.activity(mImageUri).setMinCropResultSize(1800, 1800)
                            .setMaxCropResultSize(1800, 1800).start(mActivity, this);

                    mFilePath = Common.getRealPathFromURI(mImageUri, mActivity);
                    if (mFilePath == null)
                        mFilePath = mImageUri.getPath(); // from File Manager

                    if (mFilePath != null) {
                        File mFile = new File(mFilePath);
                        mBitmap = Common.decodeFile(mFile, AppConstants.PIC_WIDTH,
                                AppConstants.PIC_HEIGHT);
                        mPath = mFile;
                    }
                    break;
                case CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE:
                    CropImage.ActivityResult result = CropImage.getActivityResult(data);
                    mImageUri = result.getUri();
                    onSelectFromGalleryResult(mImageUri);
                    break;
            }
        }
    }

    /**
     * Sets the cropped image from the gallery or camera intent
     */
    private void onSelectFromGalleryResult(Uri dataUri) {
        Bitmap bm = null;
        String imageName = null;
        if (dataUri != null) {
            try {
                //  bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), dataUri);
                File file = new File(dataUri.getPath());
                imageName = file.getName();
                mImagePath = dataUri.getPath();
                Compressor compressor = new Compressor(mActivity);
                if (ImageUtil.convertURItoFile(mActivity, dataUri) != null)
                    bm = compressor.compressToBitmap(ImageUtil.convertURItoFile(mActivity, dataUri));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        if (bm != null) {
            bm.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        }

        mImagePatientPicture.setImageBitmap(bm);
        mImagePatientPicture.setVisibility(View.VISIBLE);
    }

    /**
     * this method Call Image Upload API Call
     *
     * @param strCustomerId
     */
    private void doSubmitProfileDetail(final String strCustomerId) {
        try {

//            SessionManager mSessionManager = new SessionManager(mActivity);
//            mImagePath = mSessionManager.getPreferences(mSessionManager.SOURCE_PATH, "");

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            //Common.showLoadingDialog(mActivity, getString(R.string.dialog_file_Upload));
            //Multipart
            File file = new File(mImagePath);
            final RequestBody requestFile =
                    RequestBody.create(MediaType.parse("multipart/form-data"), file);

            // MultipartBody.Part is used to send also the actual file name
            MultipartBody.Part body =
                    MultipartBody.Part.createFormData(WebFields.ADD_UPLOAD_PICS.REQUEST_IMAGE_DATA, file.getName(), requestFile);

            // add another part within the multipart request
            RequestBody method =
                    RequestBody.create(MediaType.parse("text/plain"), WebFields.ADD_UPLOAD_PICS.MODE);
            RequestBody AccessType =
                    RequestBody.create(MediaType.parse("text/plain"), "Patient");
            RequestBody UserID =
                    RequestBody.create(MediaType.parse("text/plain"), strCustomerId);
            RequestBody mDatabaseName =
                    RequestBody.create(MediaType.parse("text/plain"), hospital_database);

            Call<AddUploadPicModel> callRepos = new RetrofitClient().createService(ApiInterface.class).addUploadPics(method,
                    AccessType, UserID, mDatabaseName, body);

            callRepos.enqueue(new Callback<AddUploadPicModel>() {
                @Override
                public void onResponse(@NonNull Call<AddUploadPicModel> call, @NonNull Response<AddUploadPicModel> response) {

                    Common.insertLog("Add UploadPics" + call.toString());
                    Common.hideDialog();
                    try {
                        int error = response.body().getError();
                        String message = response.body().getMessage();

                        if (error == AppConstants.API_SUCCESS_ERROR) {
//                            Common.setCustomToast(mActivity, message);
                            openPopup();
                        } else {
//                            Common.setCustomToast(mActivity, message);
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddUploadPicModel> call, @NonNull Throwable t) {
                    Common.insertLog("Add UploadPics error " + t.getMessage());
                    Common.hideDialog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(mActivity);
        builder.setTitle("Grant Permission");
        builder.setMessage("This App Needs Permission To Use This Feature");
        builder.setPositiveButton("Open Settings", (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", mActivity.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }


}
