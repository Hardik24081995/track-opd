package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.pchmn.materialchips.ChipsInput;
import com.pchmn.materialchips.model.ChipInterface;
import com.trackopd.R;
import com.trackopd.adapter.HistoryandComplainsAdapter;
import com.trackopd.model.AddHistoryComplainsModel;
import com.trackopd.model.ChipInputModel;
import com.trackopd.model.CloseFileModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.SessionManager;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryAndComplaintsFragment extends Fragment {

    public ArrayList<HistoryPreliminaryExaminationModel.Data> mArrHistory;
    HashSet<String> hashSetComplaints = new HashSet<String>();
    private View mView;
    private Activity mActivity;
    private ChipsInput mChipsInput;
    private ImageView mImageAddPrimaryComplaints, mImageAddSystemicHistory, mImageAddOcularHistory, mImageAddFamilyHistory;
    private Button mButtonSubmit;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.PrimaryComplain> mArrComplaints;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.SystemicHistory> mArrSystemicHistory;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.OcularHistory> mArrOcularHistory;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.FamilyOcularHistory> mArrFamilyHistory;
    private ArrayList<String> mComplaints = new ArrayList<>();
    private List<ChipInputModel> mComplaintsList, mSystemicHistoryList, mOcularHistoryList, mFamilyHistoryList;
    private String mFinalComplaints, mFinalSystemicHistory, mFinalOcularHistory, mFinalFamilyHistory,
            mSelectedChipValue, mTreatmentDate, mDatabaseName, mTextHistoryComplainsAdd, mPatientId;
    private int mAppointmentId, mCurrentPage = 1;
    private LinearLayout mLinearPrimaryComplains, mLinearSystemicHistory, mLinearOcularHistory,
            mLinearFamilyHistory;
    private RelativeLayout mRelativeNoDataFound;
    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.image_history_and_complaints_add_primary_complaints:
                    openDialogForAddHistoryAndComplaints(mActivity.getResources().getString(R.string.text_primary_complaints));
                    break;

                case R.id.image_history_and_complaints_add_systemic_history:
                    openDialogForAddHistoryAndComplaints(mActivity.getResources().getString(R.string.text_systemic_history));
                    break;

                case R.id.image_history_and_complaints_add_ocular_history:
                    openDialogForAddHistoryAndComplaints(mActivity.getResources().getString(R.string.edit_ocular_history));
                    break;

                case R.id.image_history_and_complaints_add_family_history:
                    openDialogForAddHistoryAndComplaints(mActivity.getResources().getString(R.string.text_family_history));
                    break;
            }
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_history_and_complaints, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrComplaints = new ArrayList<>();
        mComplaintsList = new ArrayList<>();
        mArrSystemicHistory = new ArrayList<>();
        mSystemicHistoryList = new ArrayList<>();
        mArrOcularHistory = new ArrayList<>();
        mOcularHistoryList = new ArrayList<>();
        mArrFamilyHistory = new ArrayList<>();
        mFamilyHistoryList = new ArrayList<>();
        mArrHistory = new ArrayList<>();

        mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

        getBundle();
        getIds();
        setRegListeners();
        setData();
        callToPreliminaryExaminationAPI();

        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);

                mAppointmentId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mTreatmentDate = bundle.getString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Image Views
            mImageAddPrimaryComplaints = mView.findViewById(R.id.image_history_and_complaints_add_primary_complaints);
            mImageAddSystemicHistory = mView.findViewById(R.id.image_history_and_complaints_add_systemic_history);
            mImageAddOcularHistory = mView.findViewById(R.id.image_history_and_complaints_add_ocular_history);
            mImageAddFamilyHistory = mView.findViewById(R.id.image_history_and_complaints_add_family_history);

            //Linear Layout
            mLinearPrimaryComplains = mView.findViewById(R.id.linear_layout_history_and_complaints_primary_complains_container);
            mLinearSystemicHistory = mView.findViewById(R.id.linear_layout_history_and_complaints_systemic_history_container);
            mLinearOcularHistory = mView.findViewById(R.id.linear_layout_history_and_complaints_ocular_history_container);
            mLinearFamilyHistory = mView.findViewById(R.id.linear_layout_history_and_complaints_family_history_container);

            //Relative Layout
            mRelativeNoDataFound = mView.findViewById(R.id.relative_history_and_complains_no_data_found);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            SessionManager manager = new SessionManager(mActivity);
            String closeFile = manager.getPreferences("ClosingFlags", "");

            if (closeFile.equalsIgnoreCase("Yes")) {
                mButtonSubmit.setClickable(false);
            } else {
                mImageAddPrimaryComplaints.setOnClickListener(clickListener);
                mImageAddSystemicHistory.setOnClickListener(clickListener);
                mImageAddOcularHistory.setOnClickListener(clickListener);
                mImageAddFamilyHistory.setOnClickListener(clickListener);
            }


        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Data Value
     */
    private void setData() {
        String currentDate = Common.setCurrentDate(mActivity);
        String covertTreatmentDate = Common.convertDateUsingDateFormat(mActivity, mTreatmentDate,
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen),
                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));

        if (currentDate.equalsIgnoreCase(covertTreatmentDate)) {
            mRelativeNoDataFound.setVisibility(View.GONE);
            //.setDataCompolains();
        } else {
            mRelativeNoDataFound.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Set Today History Complains Data
     */
    private void setTodayHistoryComplainsData() {
        try {
            String currentDate = Common.setCurrentDate(mActivity);
            String covertCurrentDate = Common.convertDateUsingDateFormat(mActivity, currentDate,
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen));

            if (mArrHistory.size() > 0) {

                if (mLinearPrimaryComplains.getChildCount()>0)
                {
                    mLinearPrimaryComplains.removeAllViews();
                }

                for (int a = 0; a < mArrHistory.size(); a++) {

                    if (mArrHistory.get(a)
                            .getTreatmentDate().equals(covertCurrentDate)) {
                        if (mArrHistory.get(a).getAppointmentID()
                                .equalsIgnoreCase(String.valueOf(mAppointmentId))) {
                            for (int h = 0; h < mArrHistory.get(a).getHistoryAndComplains().size(); h++) {
                                setHistorySetData(mActivity, mArrHistory.get(a).getHistoryAndComplains().get(h));
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open up the dialog to add history and complaints
     *
     * @param type - Dialog Type to open which dialog
     */
    private void openDialogForAddHistoryAndComplaints(String type) {

        try {



            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.custom_dialog_add_history_and_complaints);

            // Text Views
            TextView mTextTitle = dialog.findViewById(R.id.text_dialog_add_history_and_complaints_header);

            // Chip Input
            mChipsInput = dialog.findViewById(R.id.chips_input_dialog_add_history_and_complaints);

            // Buttons
            mButtonSubmit = dialog.findViewById(R.id.button_dialog_add_history_and_complaints_submit);

            mChipsInput.setSelected(true);

            // Sets up the data
            mTextTitle.setText(type);

            if (type.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_primary_complaints))) {
                setPrimaryComplaintsAdapterData();
            }

            if (type.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_systemic_history))) {
                setSystemicHistoryAdapterData();
            }

            if (type.equalsIgnoreCase(mActivity.getResources().getString(R.string.edit_ocular_history))) {
                setOcularHistoryAdapterData();
            }

            if (type.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_family_history))) {
                setFamilyHistoryAdapterData();
            }

            mTextHistoryComplainsAdd = AppConstants.STR_EMPTY_STRING;//Set Empty Value

            // Todo: Submit button Click Listener
            mButtonSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideKeyboard(mChipsInput);
                    dialog.dismiss();
                    doHistoryAndComplaints(type);
                }
            });

            // Todo: Chip Input Chip Listener
            mChipsInput.addChipsListener(new ChipsInput.ChipsListener() {
                @Override
                public void onChipAdded(ChipInterface chip, int newSize) {

                    if (mTextHistoryComplainsAdd.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                        mTextHistoryComplainsAdd = chip.getLabel();
                    } else {
                        mTextHistoryComplainsAdd = mTextHistoryComplainsAdd + "," + chip.getLabel();
                    }
                    Common.insertLog("chip added, " + newSize);
                }

                @Override
                public void onChipRemoved(ChipInterface chip, int newSize) {
                    Common.insertLog("chip removed, " + newSize);
                }

                @Override
                public void onTextChanged(CharSequence text) {
                    Common.insertLog("text changed: " + text.toString());
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method checks the validation first then call the API
     */
    private void doHistoryAndComplaints(String mType) {
        if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_primary_complaints))) {
            callToPrimaryComplaintsAPI(mType);
        }

        if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_systemic_history))) {
            callToPrimaryComplaintsAPI(mType);
        }

        if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.edit_ocular_history))) {
            callToPrimaryComplaintsAPI(mType);
        }

        if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_family_history))) {
            callToPrimaryComplaintsAPI(mType);
        }
    }

    /**
     * This Method Call API on History and Complains
     *
     * @param mType -String Type
     */
    private void callToPrimaryComplaintsAPI(String mType) {
        try {

            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
            String preliminaryExaminationId = AddNewPreliminaryExaminationFragment.getPrelimary();

            RequestBody body = null;

            if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_primary_complaints))) {

                if (mLinearPrimaryComplains.getChildCount()>0)
                {
                    for (int a=0;a<mLinearPrimaryComplains.getChildCount();a++){

                        View view=mLinearPrimaryComplains.getChildAt(a);

                        TextView mTextName = view.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
                        String data=mTextName.getText().toString();

                        if(mTextHistoryComplainsAdd.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                            mTextHistoryComplainsAdd=data;
                        }else {
                            mTextHistoryComplainsAdd=mTextHistoryComplainsAdd+","+data;
                        }
                    }
                }


                body = RequestBody.create(APICommonMethods.getMediaType(),
                        APICommonMethods.setHistoryAndComplaintsJson(mActivity, mType, preliminaryExaminationId, mUserId, mDatabaseName, mTextHistoryComplainsAdd.trim()));
            }

            if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_systemic_history))) {

                if (mLinearSystemicHistory.getChildCount()>0)
                {
                    for (int a=0;a<mLinearSystemicHistory.getChildCount();a++){

                        View view=mLinearSystemicHistory.getChildAt(a);

                        TextView mTextName = view.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
                        String data=mTextName.getText().toString();

                        if(mTextHistoryComplainsAdd.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                            mTextHistoryComplainsAdd=data;
                        }else {
                            mTextHistoryComplainsAdd=mTextHistoryComplainsAdd+","+data;
                        }
                    }
                }

                body = RequestBody.create(APICommonMethods.getMediaType(),
                        APICommonMethods.setHistoryAndComplaintsJson(mActivity, mType, preliminaryExaminationId, mUserId, mDatabaseName, mTextHistoryComplainsAdd.trim()));

            }

            if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.edit_ocular_history))) {

                if (mLinearOcularHistory.getChildCount()>0)
                {
                    for (int a=0;a<mLinearOcularHistory.getChildCount();a++){

                        View view=mLinearOcularHistory.getChildAt(a);

                        TextView mTextName = view.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
                        String data=mTextName.getText().toString();

                        if(mTextHistoryComplainsAdd.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                            mTextHistoryComplainsAdd=data;
                        }else {
                            mTextHistoryComplainsAdd=mTextHistoryComplainsAdd+","+data;
                        }
                    }
                }
                body = RequestBody.create(APICommonMethods.getMediaType(),
                        APICommonMethods.setHistoryAndComplaintsJson(mActivity, mType, preliminaryExaminationId, mUserId, mDatabaseName, mTextHistoryComplainsAdd.trim()));
            }

            if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_family_history))) {
                if (mLinearFamilyHistory.getChildCount()>0)
                {
                    for (int a=0;a<mLinearFamilyHistory.getChildCount();a++){

                        View view=mLinearFamilyHistory.getChildAt(a);

                        TextView mTextName = view.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
                        String data=mTextName.getText().toString();

                        if(mTextHistoryComplainsAdd.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                            mTextHistoryComplainsAdd=data;
                        }else {
                            mTextHistoryComplainsAdd=mTextHistoryComplainsAdd+","+data;
                        }
                    }
                }
                body = RequestBody.create(APICommonMethods.getMediaType(),
                        APICommonMethods.setHistoryAndComplaintsJson(mActivity, mType, preliminaryExaminationId, mUserId, mDatabaseName, mTextHistoryComplainsAdd.trim()));
            }

            Common.showLoadingDialog(mActivity, mActivity.getString(R.string.text_load));

            Call<AddHistoryComplainsModel> call = RetrofitClient.createService(ApiInterface.class).
                    AddHistoryComplains(body);
            call.enqueue(new Callback<AddHistoryComplainsModel>() {
                @Override
                public void onResponse(Call<AddHistoryComplainsModel> call, Response<AddHistoryComplainsModel> response) {
                    Common.insertLog(response.body().toString());
                    if (response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                        Common.hideDialog();
                        String message = response.body().getMessage();
                        Common.setCustomToast(mActivity, message);

                        String[] value = mTextHistoryComplainsAdd.split(",");

                       /* if (value.length > 0) {
                            for (String aValue : value) {
                                setHistoryLayout(mActivity, mType, aValue);
                            }
                        }*/
                        //Set Update History Data
                        if (mArrHistory.size() > 0) {
                            mArrHistory.clear();
                        }
                        mCurrentPage = 1;
                        callHistoryAndComplainsApi();

                    } else {
                        Common.hideDialog();
                        String message = response.body().getMessage();
                        Common.setCustomToast(mActivity, message);
                    }
                }

                @Override
                public void onFailure(Call<AddHistoryComplainsModel> call, Throwable t) {
                    Common.hideDialog();
                    String message = t.getMessage();
                    Common.setCustomToast(mActivity, message);
                }
            });

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
            Common.hideDialog();
        }
    }

    /**
     * Set History and Complain Data
     *
     * @param mType -Type
     */
    private void setHistoryAndComplainsLisData(String mType) {


    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {
        boolean status = true;
        Common.insertLog("Validation Size::::> " + mComplaintsList.size());
        if (mComplaintsList.size() == 0) {
            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.val_enter_text));
            status = false;
        }
        return status;
    }

    /**
     * This method should call the Preliminary Examination API
     */
    private void callToPreliminaryExaminationAPI() {
        try {

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPreliminaryExaminationDetailsJson(mDatabaseName));

            Call<PreliminaryExaminationDetailsModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationDetails(body);
            call.enqueue(new Callback<PreliminaryExaminationDetailsModel>() {
                @Override
                public void onResponse(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Response<PreliminaryExaminationDetailsModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null) {

                                // Complaints Array Binding
                                mArrComplaints.addAll(response.body().getData().getPrimaryComplains());

                                // Systemic History Array Binding
                                mArrSystemicHistory.addAll(response.body().getData().getSystemicHistory());

                                // Ocular History Array Binding
                                mArrOcularHistory.addAll(response.body().getData().getOcularHistory());

                                // Family Ocular History Array Binding
                                mArrFamilyHistory.addAll(response.body().getData().getFamilyOcularHistory());


                                callHistoryAndComplainsApi();
//                                setTodayHistoryComplainsData();

                            }
                        } else {
//                            setOcularHistoryAdapterData();
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callHistoryAndComplainsApi() {
        try {

            String defualt_date = "1000-01-01";
            String Mobile = AppConstants.STR_EMPTY_STRING;
            String Type = "HistoryAndComplains";


            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            int mHistoryDoctorID = -1;


            Common.showLoadingDialog(mActivity,mActivity.getResources().getString(R.string.text_load));
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setHistoryPreliminaryExaminationJson(defualt_date, mCurrentPage, mPatientId, Mobile, mHistoryDoctorID, Type, hospital_database));

            Call<HistoryPreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationHistory(body);
            call.enqueue(new Callback<HistoryPreliminaryExaminationModel>() {
                @Override
                public void onResponse(@NonNull Call<HistoryPreliminaryExaminationModel> call, Response<HistoryPreliminaryExaminationModel> response) {
                    Common.insertLog("Response History::::> " + new Gson().toJson(response.body()));
                    try {
                        Common.hideDialog();
                        if (response.body() != null &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body().getData().size() > 0) {


                                mArrHistory.addAll(response.body().getData());

                                setTodayHistoryComplainsData();
                            }
                        }
                    } catch (Exception e) {
                        Common.hideDialog();
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<HistoryPreliminaryExaminationModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.hideDialog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the primary complaints data and bind it to the adapter
     */
    private void setPrimaryComplaintsAdapterData() {

        try {
            mComplaintsList.clear();
            for (int i = 0; i < mArrComplaints.size(); i++) {
                String name = mArrComplaints.get(i).getPrimaryComplains();
                ChipInputModel contactChip = new ChipInputModel(name);
                if (mComplaints.size() > 0) {
                    for (int j = 0; j <= mComplaints.size(); j++) {
                        if (mComplaints.get(j).equalsIgnoreCase(name)) {
                            mComplaintsList.add(contactChip);
                        }
                    }
                } else {
                    mComplaintsList.add(contactChip);
                }
            }
            mChipsInput.setFilterableList(mComplaintsList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the systemic history data and bind it to the adapter
     */
    private void setSystemicHistoryAdapterData() {
        try {
            mSystemicHistoryList.clear();
            for (int i = 0; i < mArrSystemicHistory.size(); i++) {
                String name = mArrSystemicHistory.get(i).getSystemicHistory();
                ChipInputModel contactChip = new ChipInputModel(name);
                mSystemicHistoryList.add(contactChip);
            }
            mChipsInput.setFilterableList(mSystemicHistoryList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the ocular history data and bind it to the adapter
     */
    private void setOcularHistoryAdapterData() {
        try {
            mOcularHistoryList.clear();
            for (int i = 0; i < mArrOcularHistory.size(); i++) {
                String name = mArrOcularHistory.get(i).getOcularHistory();
                ChipInputModel contactChip = new ChipInputModel(name);
                mOcularHistoryList.add(contactChip);
            }

            mChipsInput.setFilterableList(mOcularHistoryList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the family history data and bind it to the adapter
     */
    private void setFamilyHistoryAdapterData() {
        try {
            mFamilyHistoryList.clear();
            for (int i = 0; i < mArrFamilyHistory.size(); i++) {
                String name = mArrFamilyHistory.get(i).getFamilyOcularHistory();
                ChipInputModel contactChip = new ChipInputModel(name);
                mFamilyHistoryList.add(contactChip);
            }

            mChipsInput.setFilterableList(mFamilyHistoryList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    /**
//     * Sets the data and bind it to the adapter
//     */
//    private void setHistoryAndComplaintsData(String mType) {
//        try {
//            // Primary Complaints -> Selected primary complaints sets to the text area
//            if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_primary_complaints))) {
//
//                for (ChipInputModel chip : (List<ChipInputModel>) mChipsInput.getSelectedChipList()) {
//                    String mComplaintsName = chip.getLabel();
//                    mComplaints.add(mComplaintsName);
//                    //set Layout inflater
//                    setLayoutAdapter(mType, mComplaintsName);
//
//                }
//                //.replace(", ", ",")
//                mFinalComplaints = mComplaints.toString().replaceAll("\\[", "").replaceAll("\\]", "");
//                mSelectedChipValue = mFinalComplaints.replace(", ", ",");
//
//                setPrimaryComplaintsAdapterData();
//            }
//
//            // Systemic History -> Selected systemic history sets to the text area
//            if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_systemic_history))) {
//                ArrayList<String> mSystemicHistory = new ArrayList<>();
//                for (ChipInputModel chip : (List<ChipInputModel>) mChipsInput.getSelectedChipList()) {
//                    String mSystemicHistoryName = chip.getLabel();
//                    mSystemicHistory.add(mSystemicHistoryName);
//
//                    //Set Linear Adapter
//                    setLayoutAdapter(mType, mSystemicHistoryName);
//                }
//                mFinalSystemicHistory = mSystemicHistory.toString().replaceAll("\\[", "").replaceAll("\\]", "");
//                mSelectedChipValue = mFinalSystemicHistory.replace(", ", ",");
//            }
//
//            // Ocular History -> Selected ocular history sets to the text area
//            if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.edit_ocular_history))) {
//                ArrayList<String> mOcularHistory = new ArrayList<>();
//                for (ChipInputModel chip : (List<ChipInputModel>) mChipsInput.getSelectedChipList()) {
//                    String mOcularHistoryName = chip.getLabel();
//                    mOcularHistory.add(mOcularHistoryName);
//
//                    //Set Linear Adapter
//                    setLayoutAdapter(mType, mOcularHistoryName);
//                }
//                mFinalOcularHistory = mOcularHistory.toString().replaceAll("\\[", "").replaceAll("\\]", "");
//                mSelectedChipValue = mFinalOcularHistory.replace(", ", ",");
//            }
//
//            // Family History -> Selected family history sets to the text area
//            if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_family_history))) {
//                ArrayList<String> mFamilyHistory = new ArrayList<>();
//                for (ChipInputModel chip : (List<ChipInputModel>) mChipsInput.getSelectedChipList()) {
//                    String mFamilyHistoryName = chip.getLabel();
//                    mFamilyHistory.add(mFamilyHistoryName);
//
//                    //Set Linear Adapter
//                    setLayoutAdapter(mType, mFamilyHistoryName);
//                }
//                mFinalFamilyHistory = mFamilyHistory.toString()
//                        .replaceAll("\\[", "")
//                        .replaceAll("\\]", "");
//                mSelectedChipValue = mFinalFamilyHistory.replace(", ", ",");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    /**
//     * Add History Linear Layout Inflater History List
//     *
//     * @param mType           - String Type
//     * @param mComplaintsName - String Complain Name
//     */
//    private void setLayoutAdapter(String mType, String mComplaintsName) {
//        @SuppressLint("InflateParams")
//        View view_container = LayoutInflater.from(mActivity).inflate(R.layout.row_add_preliminary_examination_compliance_item,
//                null, false);
//
//        TextView mTextName = view_container.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
//        ImageView mImageDelete = view_container.findViewById(R.id.image_row_add_preliminary_examination_compliance_delete);
//        mImageDelete.setColorFilter(Common.setThemeColor(mActivity));
//
//        //Primary Compliance  Linear Layout Compliance
//        if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_primary_complaints))) {
//            mTextName.setText(mComplaintsName);
//            mLinearPrimaryComplains.addView(view_container);
//
//            mImageDelete.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    mLinearPrimaryComplains.removeView(view_container);
//                }
//            });
//        } else if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_systemic_history))) {
//            mTextName.setText(mComplaintsName);
//            mLinearSystemicHistory.addView(view_container);
//
//            mImageDelete.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    mLinearSystemicHistory.removeView(view_container);
//                }
//            });
//        } else if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.edit_ocular_history))) {
//            mTextName.setText(mComplaintsName);
//            mLinearOcularHistory.addView(view_container);
//
//            mImageDelete.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    mLinearOcularHistory.removeView(view_container);
//                }
//            });
//        } else {
//            mTextName.setText(mComplaintsName);
//            mLinearFamilyHistory.addView(view_container);
//
//            mImageDelete.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    mLinearFamilyHistory.removeView(view_container);
//                }
//            });
//        }
//    }


    /**
     * This method should hide the keyboard when input is being entered when called submit button
     */
    private void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     *OpenDialog for closing patient file
     */
    public void openFileClosingAlert() {


        AlertDialog.Builder builder = new AlertDialog.Builder(
                mActivity);
        builder.setTitle("Close File");
        builder.setMessage("Are you sure you want to close file?");
        builder.setNegativeButton("NO",
                (dialog, which) -> dialog.dismiss());
        builder.setPositiveButton("YES",
                (dialog, which) -> closeFile());
        builder.show();

    }

    private void closeFile() {
        try {
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.closeFileJson(mAppointmentId, mDatabaseName));

            Call<CloseFileModel> call = RetrofitClient.createService(ApiInterface.class).closeFileAPI(body);
            call.enqueue(new Callback<CloseFileModel>() {
                @Override
                public void onResponse(@NonNull Call<CloseFileModel> call, @NonNull Response<CloseFileModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        int mError = response.body().getError();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                Common.insertLog("IF");
                                Common.setCustomToast(mActivity,mMessage);
                            } else {
                                Common.insertLog("ELSE");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CloseFileModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open Histroy Dialog
     */
    public void openDialogHistoryandComplainList() {
        try {

            if (mActivity == null) {
                mActivity = getActivity();
            }

            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.custom_dialog_history_preliminary);

            TextView mTextTitle = dialog.findViewById(R.id.text_view_custom_dialog_history_preliminary);
            ImageView mImageClose = dialog.findViewById(R.id.image_custom_dialog_history_preliminary_close);

            RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);

            RelativeLayout noDataFound = dialog.findViewById(R.id.relative_no_data_available);

            mTextTitle.setText(mActivity.getResources().getString(R.string.tab_history_and_complaints));

            mImageClose.setColorFilter(Common.setThemeColor(mActivity));

            mImageClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            LinearLayoutManager manager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(manager);

            HistoryandComplainsAdapter adapter = new HistoryandComplainsAdapter(mActivity, recyclerView,
                    mArrHistory) {
                @Override
                protected void onSelectIten(HistoryPreliminaryExaminationModel.Data checkInData) {
                    super.onSelectIten(checkInData);
                    dialog.dismiss();

                    setHistorySetData(mActivity, checkInData.getHistoryAndComplains().get(0));
                }
            };
            recyclerView.setAdapter(adapter);

            if (mArrHistory.size() > 0) {
                recyclerView.setVisibility(View.VISIBLE);
                noDataFound.setVisibility(View.GONE);
            } else {
                recyclerView.setVisibility(View.GONE);
                noDataFound.setVisibility(View.VISIBLE);
            }

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set History Data list
     *
     * @param activity    -Activity
     * @param checkInData - History Complains
     */
    private void setHistorySetData(Activity activity, HistoryPreliminaryExaminationModel.HistoryAndComplain checkInData) {
        mRelativeNoDataFound.setVisibility(View.GONE);

        if (!checkInData.getPrimaryComplains().trim().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {

           /* if (mLinearPrimaryComplains.getChildCount() > 0) {
                try {
                    mComplaintsList.clear();
                    for (int i = 0; i < mArrComplaints.size(); i++) {
                        String name = mArrComplaints.get(i).getPrimaryComplains();
                        ChipInputModel contactChip = new ChipInputModel(name);
                        if (mComplaints.size() > 0) {
                            for (int j = 0; j <= mComplaints.size(); j++) {
                                if (mComplaints.get(j).equalsIgnoreCase(name)) {
                                    mComplaintsList.remove(contactChip);
                                }
                            }
                        } else {
                            mComplaintsList.add(contactChip);
                        }
                    }
                    mChipsInput.setFilterableList(mComplaintsList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }*/

            if (mLinearPrimaryComplains.getChildCount() > 0) {
                mLinearPrimaryComplains.removeAllViews();
            }

            String[] array = checkInData.getPrimaryComplains().split(",");
            for (String s : array) {
                setHistoryLayout(activity, activity.getResources().getString(R.string.text_primary_complaints), s);

            }
        }
        // Systemic History -> Selected Systemic history sets to the text area
        if (!checkInData.getSystemicHistory().trim().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {

            if (mLinearSystemicHistory.getChildCount() > 0) {
                mLinearSystemicHistory.removeAllViews();
            }

            String[] array = checkInData.getSystemicHistory().split(",");
            for (String s : array) {
                setHistoryLayout(activity, activity.getResources().getString(R.string.text_systemic_history), s);
            }
        }
        // Ocular History -> Selected Ocular History sets to the text area
        if (!checkInData.getOcularHistory().trim().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {

            if (mLinearOcularHistory.getChildCount() > 0) {
                mLinearOcularHistory.removeAllViews();
            }

            String[] array = checkInData.getOcularHistory().split(",");
            for (String s : array) {
                setHistoryLayout(activity, activity.getResources().getString(R.string.text_ocular_investigation), s);
            }
        }
        // Family History-> Selected Family History sets to the text area
        if (!checkInData.getFamilyOcularHistory().trim().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {

            if (mLinearFamilyHistory.getChildCount() > 0) {
                mLinearFamilyHistory.removeAllViews();
            }

            String[] array = checkInData.getFamilyOcularHistory().split(",");
            for (String s : array) {
                setHistoryLayout(activity, activity.getResources().getString(R.string.text_family_history), s);
            }
        }
    }

    public void setHistoryLayout(Activity activity, String mType, String mComplaintsName) {
        @SuppressLint("InflateParams")
        View view_container = LayoutInflater.from(activity).inflate(R.layout.row_add_preliminary_examination_compliance_item,
                null, false);

        TextView mTextName = view_container.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
        ImageView mImageDelete = view_container.findViewById(R.id.image_row_add_preliminary_examination_compliance_delete);
        mImageDelete.setColorFilter(Common.setThemeColor(activity));
        mTextName.setTextColor(mActivity.getResources().getColor(R.color.colorGray));

        //Primary Compliance  Linear Layout Compliance
        if (mType.equalsIgnoreCase(activity.getResources().getString(R.string.text_primary_complaints))) {
            mTextName.setText(mComplaintsName);
            mLinearPrimaryComplains.addView(view_container);
            mImageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLinearPrimaryComplains.removeView(view_container);
                    RemoveUpdateApiData(mType, mComplaintsName);
                }
            });
        } else if (mType.equalsIgnoreCase(activity.getResources().getString(R.string.text_systemic_history))) {
            mTextName.setText(mComplaintsName);
            mLinearSystemicHistory.addView(view_container);

            mImageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLinearSystemicHistory.removeView(view_container);
                    RemoveUpdateApiData(mType, mComplaintsName);
                }
            });
        } else if (mType.equalsIgnoreCase(activity.getResources().getString(R.string.edit_ocular_history))
                || mType.equalsIgnoreCase(activity.getResources().getString(R.string.text_ocular_investigation))) {
            mTextName.setText(mComplaintsName);
            mLinearOcularHistory.addView(view_container);

            mImageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLinearOcularHistory.removeView(view_container);
                    // doHistoryAndComplaints(mType);
                    RemoveUpdateApiData(mType, mComplaintsName);
                }
            });
        } else if (mType.equalsIgnoreCase(activity.getResources().getString(R.string.text_family_history))) {
            mTextName.setText(mComplaintsName);
            mLinearFamilyHistory.addView(view_container);

            mImageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLinearFamilyHistory.removeView(view_container);
                    RemoveUpdateApiData(mType, mComplaintsName);
                }
            });
        }
    }

    /**
     * Remove Item Update API
     *
     * @param mType           - Type
     * @param mComplaintsName - Compliance Name
     */
    private void RemoveUpdateApiData(String mType, String mComplaintsName) {

        mTextHistoryComplainsAdd = AppConstants.STR_EMPTY_SPACE;

        //Primary Compliance  Linear Layout Compliance
        if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_primary_complaints))) {

            if (mLinearPrimaryComplains.getChildCount() > 0) {
                for (int i = 0; i < mLinearPrimaryComplains.getChildCount(); i++) {
                    View childView = mLinearPrimaryComplains.getChildAt(i);
                    TextView text = childView.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
                    if (mTextHistoryComplainsAdd.equalsIgnoreCase(AppConstants.STR_EMPTY_SPACE)) {
                        mTextHistoryComplainsAdd = text.getText().toString();
                    } else {
                        mTextHistoryComplainsAdd = mTextHistoryComplainsAdd + "," + text.getText().toString();
                    }
                }

            }
        } else if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_systemic_history))) {
            if (mLinearSystemicHistory.getChildCount() > 0) {
                for (int i = 0; i < mLinearSystemicHistory.getChildCount(); i++) {
                    View childView = mLinearSystemicHistory.getChildAt(i);
                    TextView text = childView.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
                    if (mTextHistoryComplainsAdd.equalsIgnoreCase(AppConstants.STR_EMPTY_SPACE)) {
                        mTextHistoryComplainsAdd = text.getText().toString();
                    } else {
                        mTextHistoryComplainsAdd = mTextHistoryComplainsAdd + "," + text.getText().toString();
                    }
                }
            }
        } else if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.edit_ocular_history))
                || mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_ocular_history))) {
            if (mLinearOcularHistory.getChildCount() > 0) {
                for (int i = 0; i < mLinearOcularHistory.getChildCount(); i++) {
                    View childView = mLinearOcularHistory.getChildAt(i);
                    TextView text = childView.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
                    if (mTextHistoryComplainsAdd.equalsIgnoreCase(AppConstants.STR_EMPTY_SPACE)) {
                        mTextHistoryComplainsAdd = text.getText().toString();
                    } else {
                        mTextHistoryComplainsAdd = mTextHistoryComplainsAdd + "," + text.getText().toString();
                    }
                }
            }
        } else if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_family_history))) {
            if (mLinearFamilyHistory.getChildCount() > 0) {
                for (int i = 0; i < mLinearFamilyHistory.getChildCount(); i++) {
                    View childView = mLinearFamilyHistory.getChildAt(i);
                    TextView text = childView.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
                    if (mTextHistoryComplainsAdd.equalsIgnoreCase(AppConstants.STR_EMPTY_SPACE)) {
                        mTextHistoryComplainsAdd = text.getText().toString();
                    } else {
                        mTextHistoryComplainsAdd = mTextHistoryComplainsAdd + "," + text.getText().toString();
                    }
                }
            }
        }

        callToRemovePrimaryComplaintsAPI(mType);
    }


    /**
     * This Method Call API on History and Complains
     *
     * @param mType -String Type
     */
    private void callToRemovePrimaryComplaintsAPI(String mType) {
        try {

            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
            String preliminaryExaminationId = AddNewPreliminaryExaminationFragment.getPrelimary();

            RequestBody body = null;

            if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_primary_complaints))) {
                body = RequestBody.create(APICommonMethods.getMediaType(),
                        APICommonMethods.setHistoryAndComplaintsJson(mActivity, mType, preliminaryExaminationId, mUserId, mDatabaseName, mTextHistoryComplainsAdd));
            }

            if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_systemic_history))) {
                body = RequestBody.create(APICommonMethods.getMediaType(),
                        APICommonMethods.setHistoryAndComplaintsJson(mActivity, mType, preliminaryExaminationId, mUserId, mDatabaseName, mTextHistoryComplainsAdd));
            }

            if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_ocular_investigation))) {
                body = RequestBody.create(APICommonMethods.getMediaType(),
                        APICommonMethods.setHistoryAndComplaintsJson(mActivity, mType, preliminaryExaminationId, mUserId, mDatabaseName, mTextHistoryComplainsAdd));
            }

            if (mType.equalsIgnoreCase(mActivity.getResources().getString(R.string.text_family_history))) {
                body = RequestBody.create(APICommonMethods.getMediaType(),
                        APICommonMethods.setHistoryAndComplaintsJson(mActivity, mType, preliminaryExaminationId, mUserId, mDatabaseName, mTextHistoryComplainsAdd));
            }

            Common.showLoadingDialog(mActivity, mActivity.getString(R.string.text_load));

            Call<AddHistoryComplainsModel> call = RetrofitClient.createService(ApiInterface.class).
                    AddHistoryComplains(body);

            call.enqueue(new Callback<AddHistoryComplainsModel>() {
                @Override
                public void onResponse(Call<AddHistoryComplainsModel> call, Response<AddHistoryComplainsModel> response) {
                    Common.insertLog("Response..." + response.body().toString());
                    if (response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                        Common.hideDialog();
                        String message = "Remove Data success";
                        Common.setCustomToast(mActivity, message);

                        if (mArrHistory.size() > 0) {
                            mArrHistory.clear();
                        }

                        mCurrentPage = 1;
                        callHistoryAndComplainsApi();

                    } else {
                        Common.hideDialog();
                        String message = response.body().getMessage();
                        Common.setCustomToast(mActivity, message);
                    }
                }

                @Override
                public void onFailure(Call<AddHistoryComplainsModel> call, Throwable t) {
                    Common.hideDialog();
                    String message = t.getMessage();
                    Common.setCustomToast(mActivity, message);
                }
            });

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
            Common.hideDialog();
        }
    }
}
