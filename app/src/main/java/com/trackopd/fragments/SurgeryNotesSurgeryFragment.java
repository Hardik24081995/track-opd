package com.trackopd.fragments;


import android.app.Activity;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.google.gson.Gson;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.model.AddMediclaimModel;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.model.ReceptionistAppointmentModel;
import com.trackopd.model.ReceptionistPatientModel;
import com.trackopd.model.SurgeryListModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.utils.GenerateTimeSlot;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SurgeryNotesSurgeryFragment extends Fragment {

    private Activity mActivity;
    private View mView;
    private EditText mEditLeftAct,mEditLeftAL,mEditLeftSurgeryNotes,mEditLeftRemarks,
            mEditLeftViscoelastics,mEditLeftIncisionSize,mEditLeftIncisionType,mEditRightAct,
            mEditRightAL,mEditRightSurgeryNotes,mEditRightRemarks,
            mEditRightViscoelastics,mEditRightIncisionSize,mEditRightIncisionType,mEditStatTime,
            mEditEndTime,mEditSurgeryDate;
    private Button mButtonSubmit;
    private AutoCompleteTextView mAutoCompletedSurgeryType;
    private ImageView mImageAddType,mImageRemoveType;
    private RelativeLayout mRelativeSurgeryType;
    private AppUtil mAppUtils;
    private ReceptionistAppointmentModel Appointment;
    private ArrayList<String> mArrSurgeryTypeName;
    public  ArrayList<PreliminaryExaminationDetailsModel.Data.SurgeryType> mArrSurgeryType;
    public String mStrSurgeryID,mUserType,AppointmentID;
    private SurgeryListModel mSurgeryDetail;
    private Spinner mSpinnerSurgeryType;


    public SurgeryNotesSurgeryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView= inflater.inflate(R.layout.fragment_surgery_notes_surgery, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity=getActivity();
        mAppUtils=new AppUtil(mActivity);

        mArrSurgeryTypeName=new ArrayList<>();
        mArrSurgeryType=new ArrayList<>();
        mStrSurgeryID=AppConstants.STR_EMPTY_STRING;
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        getBundle();
        getIds();
        callToPreliminaryExaminationAPI();
        setRegister();
        setData();
        return mView;
    }

    private void setAdapter() {
       try{
          if (mArrSurgeryType!=null &&
                  mArrSurgeryType.size()>0){
              for (int i=0;i<mArrSurgeryType.size();i++){
                  mArrSurgeryTypeName.add(mArrSurgeryType.get(i).getSurgeryName());
              }
          }
       }catch (Exception e){
          Common.insertLog(e.getMessage());
       }
    }

    /**
     * This Method get Bundle Value can be pass
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                if (bundle.getSerializable(AppConstants.BUNDLE_ADD_SURGERY)!=null) {

                    mSurgeryDetail = (SurgeryListModel) bundle.getSerializable(AppConstants.BUNDLE_ADD_SURGERY);
                    AppointmentID = mSurgeryDetail.getAppointmentID();
                }else {
                    AppointmentID=bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_ID);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try{
            //Edit Text
            mEditStatTime=mView.findViewById(R.id.edit_text_surgery_notes_start_time);
            mEditEndTime=mView.findViewById(R.id.edit_text_surgery_notes_end_time);
            mEditSurgeryDate=mView.findViewById(R.id.edit_text_surgery_notes_date);

            //Edit Text Right Eye
            mEditRightAct=mView.findViewById(R.id.edit_add_surgery_notes_right_eye_act);
            mEditRightAL=mView.findViewById(R.id.edit_add_surgery_notes_right_eye_al);
            mEditRightIncisionSize=mView.findViewById(R.id.edit_add_surgery_notes_right_eye_incision_size);
            mEditRightIncisionType=mView.findViewById(R.id.edit_add_surgery_notes_right_eye_incision_type);
            mEditRightSurgeryNotes=mView.findViewById(R.id.edit_add_surgery_notes_right_eye_surgery_notes);
            mEditRightViscoelastics=mView.findViewById(R.id.edit_add_surgery_notes_right_eye_viscoelastics);
            mEditRightRemarks=mView.findViewById(R.id.edit_add_surgery_notes_right_eye_remarks);

            //Edit Text Left Eye
            mEditLeftAct=mView.findViewById(R.id.edit_add_surgery_notes_left_eye_act);
            mEditLeftAL=mView.findViewById(R.id.edit_add_surgery_notes_left_eye_al);
            mEditLeftIncisionSize=mView.findViewById(R.id.edit_add_surgery_notes_left_eye_incision_size);
            mEditLeftIncisionType=mView.findViewById(R.id.edit_add_surgery_notes_left_eye_incision_type);
            mEditLeftSurgeryNotes=mView.findViewById(R.id.edit_add_surgery_notes_left_eye_surgery_notes);
            mEditLeftViscoelastics=mView.findViewById(R.id.edit_add_surgery_notes_left_eye_viscoelastics);
            mEditLeftRemarks=mView.findViewById(R.id.edit_add_surgery_notes_left_eye_remarks);

            mButtonSubmit=mView.findViewById(R.id.button_surgery_notes_submit);

            //Auto Completed Text View
            mAutoCompletedSurgeryType=mView.findViewById(R.id.auto_complete_surgery_notes_surgery_type);

            // Image View
            mImageRemoveType=mView.findViewById(R.id.image_surgery_notes_surgery_type_delete);
            mImageAddType=mView.findViewById(R.id.image_surgery_notes_surgery_type_add);

            // Relative Layout
            mRelativeSurgeryType=mView.findViewById(R.id.relative_surgery_notes_surgery_type);


            //Spinner
            mSpinnerSurgeryType=mView.findViewById(R.id.spinner_surgery_note_surgery_type);

        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Data
     */
    private void setData() {
        try{
            mEditSurgeryDate.setText(Common.setCurrentDate(mActivity));

            if (mSurgeryDetail!=null){

                String date=Common.convertDateUsingDateFormat(mActivity,
                        mSurgeryDetail.getSurgeryDate(),
                        mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen),
                        mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));

                mEditSurgeryDate.setText(date);
                mEditStatTime.setText(mSurgeryDetail.getStartTime());
                mEditEndTime.setText(mSurgeryDetail.getEndTime());

                mStrSurgeryID=mSurgeryDetail.getSurgeryTypeID();
                mRelativeSurgeryType.setVisibility(View.VISIBLE);
                mImageAddType.setVisibility(View.GONE);
                mAutoCompletedSurgeryType.setText(mSurgeryDetail.getSurgeryName());

                //Right Set Value on EdiText
                mEditRightAct.setText(mSurgeryDetail.getRACD());
                mEditRightAL.setText(mSurgeryDetail.getRAL());
                mEditRightIncisionSize.setText(mSurgeryDetail.getLIncisionSize());
                mEditRightIncisionType.setText(mSurgeryDetail.getLIncisionType());
                mEditRightViscoelastics.setText(mSurgeryDetail.getRViscoelastics());
                mEditRightRemarks.setText(mSurgeryDetail.getRRemarks());
                mEditRightSurgeryNotes.setText(mSurgeryDetail.getRSurgeryNotes());

                // Left Set Value on EdiText
                mEditLeftAct.setText(mSurgeryDetail.getLACD());
                mEditLeftAL.setText(mSurgeryDetail.getLAL());
                mEditLeftIncisionSize.setText(mSurgeryDetail.getLIncisionSize());
                mEditLeftIncisionType.setText(mSurgeryDetail.getLIncisionType());
                mEditLeftViscoelastics.setText(mSurgeryDetail.getLViscoelastics());
                mEditLeftRemarks.setText(mSurgeryDetail.getLRemarks());
                mEditLeftSurgeryNotes.setText(mSurgeryDetail.getLSurgeryNotes());

            }

            ArrayAdapter<String> adapter_surgery = new ArrayAdapter<String>
                    (mActivity, android.R.layout.simple_selectable_list_item, mArrSurgeryTypeName);
            mSpinnerSurgeryType.setAdapter(adapter_surgery);

        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Register on Click Listener
     */
    private void setRegister() {
        try{
            //ToDo: Click Listeners
            mEditStatTime.setOnClickListener(clickListener);
            mEditEndTime.setOnClickListener(clickListener);
            mImageAddType.setOnClickListener(clickListener);
            mImageRemoveType.setOnClickListener(clickListener);
            mEditSurgeryDate.setOnClickListener(clickListener);

            mButtonSubmit.setOnClickListener(clickListener);

            mAutoCompletedSurgeryType.setOnItemClickListener(new AdapterView.OnItemClickListener() {
               @Override
               public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                   mStrSurgeryID= mArrSurgeryType.get(position).getSurgeryTypeID();

               }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.edit_text_surgery_notes_start_time:
                    Common.openTimePicker(mActivity, mEditStatTime);
                    break;
                case R.id.edit_text_surgery_notes_end_time:
                    Common.openTimePicker(mActivity, mEditEndTime);
                    break;
                case R.id.edit_text_surgery_notes_date:
                    Common.openDatePicker(mActivity, mEditSurgeryDate);

                    break;
                case R.id.image_surgery_notes_surgery_type_add:
                       mRelativeSurgeryType.setVisibility(View.VISIBLE);
                       mImageAddType.setVisibility(View.GONE);
                    break;
                case R.id.image_surgery_notes_surgery_type_delete:
                      mRelativeSurgeryType.setVisibility(View.GONE);
                      mImageAddType.setVisibility(View.VISIBLE);
                    break;
                case R.id.button_surgery_notes_submit:
                      if (checkValidation()){
                          callToSurgeryNotes();
                      }
                    break;
            }
        }
    };

    /**
     * Check Validation
     * @return -Return True or false
     */
    private boolean checkValidation() {
        boolean status=true;
        String date=mEditSurgeryDate.getText().toString();
        String startTime=mEditStatTime.getText().toString();
        String endTime=mEditEndTime.getText().toString();

        int selected_index_surgery=mSpinnerSurgeryType.getSelectedItemPosition();

        if (date.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
            status=false;
            mEditSurgeryDate.setError(mActivity.getResources().getString(R.string.error_field_required));
        }
        if (startTime.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
            status=false;
            mEditStatTime.setError(mActivity.getResources().getString(R.string.error_field_required));
        }
        if (endTime.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
            status=false;
            mEditEndTime.setError(mActivity.getResources().getString(R.string.error_field_required));
        }

        if (selected_index_surgery==0){
            status=false;
            Common.setCustomToast(mActivity,mActivity.getString(R.string.error_select_surgery));
        }
        return status;
    }

    private void callToSurgeryNotes() {
      try {
         if (!mAppUtils.getConnectionState()){
             Common.setCustomToast(mActivity,mActivity.getResources().getString(R.string.no_internet_connection));
         }else {

             String right_acd=mEditRightAct.getText().toString().trim();
             String right_al=mEditRightAL.getText().toString().trim();
             String right_incision_size=mEditRightIncisionSize.getText().toString().trim();
             String right_incision_type=mEditRightIncisionType.getText().toString().trim();
             String right_surgery_notes=mEditRightSurgeryNotes.getText().toString().trim();
             String right_viscoelastics=mEditRightViscoelastics.getText().toString().trim();
             String right_remarks=mEditRightRemarks.getText().toString().trim();

             String left_act=mEditLeftAct.getText().toString().trim();
             String left_al=mEditLeftAL.getText().toString().trim();
             String left_incision_size=mEditLeftIncisionSize.getText().toString().trim();
             String left_incision_type=mEditLeftIncisionType.getText().toString().trim();
             String left_surgery_notes=mEditLeftSurgeryNotes.getText().toString().trim();
             String left_viscoelastics=mEditLeftViscoelastics.getText().toString().trim();
             String left_remarks=mEditLeftRemarks.getText().toString().trim();

             String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
             String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

             //String mAppointmentID=Appointment.getAppointmentID();

             String strDate=mEditSurgeryDate.getText().toString();
             String startDate=mEditStatTime.getText().toString();
             String endDate=mEditEndTime.getText().toString();


             String date=Common.convertDateUsingDateFormat(mActivity,strDate,
                     mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                     mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));

             String start_time=Common.convertDateUsingDateFormat(mActivity,startDate,
                     mActivity.getResources().getString(R.string.date_format_hh_mm_a),
                     mActivity.getResources().getString(R.string.date_format_hh_mm_ss));
             String end_time=Common.convertDateUsingDateFormat(mActivity,endDate,
                     mActivity.getResources().getString(R.string.date_format_hh_mm_a),
                     mActivity.getResources().getString(R.string.date_format_hh_mm_ss));

             //get Surgery ID
             int select_surgery=mSpinnerSurgeryType.getSelectedItemPosition();

             mStrSurgeryID=mArrSurgeryType.get(select_surgery).getSurgeryTypeID();

             RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                     APICommonMethods.setAddSurgeryNoteJson(AppointmentID,mUserId,mDatabaseName,date,start_time,
                             end_time,mStrSurgeryID,right_acd,left_act,right_al,left_al,right_incision_size,
                             left_incision_size,right_incision_type,left_incision_type,right_surgery_notes,left_surgery_notes,
                             right_viscoelastics,left_viscoelastics,right_remarks,left_remarks));

             Common.insertLog("Request "+requestBody);
             Call<AddMediclaimModel> call = RetrofitClient.createService(ApiInterface.class).AddSurgeryNote(requestBody);
             call.enqueue(new Callback<AddMediclaimModel>() {
                 @Override
                 public void onResponse(@NonNull Call<AddMediclaimModel> call, @NonNull Response<AddMediclaimModel> response) {

                     Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                     try {
                         String mJson = (new Gson().toJson(response.body()));
                         JSONObject jsonObject = new JSONObject(mJson);
                         String mMessage = jsonObject.getString(WebFields.MESSAGE);
                         int mError = jsonObject.getInt(WebFields.ERROR);
                         Common.insertLog("mMessage:::> " + mMessage);

                         if (response.isSuccessful() && mError == 200) {
                             if (response.body() != null) {
                                 /*removeAllFragments();
                                 callToMediclaimList();*/
                                 AddSurgeryFragment.setPage(1);
                             }
                         }
                     } catch (JSONException e) {
                         e.printStackTrace();
                     }
                 }

                 @Override
                 public void onFailure(@NonNull Call<AddMediclaimModel> call, @NonNull Throwable t) {
                     Common.insertLog("Failure:::> " + t.getMessage());
                 }
             });
         }
      }catch (Exception e){
          Common.insertLog(e.getMessage());
      }
    }

    /**
     * This method should call the Preliminary Examination API
     */
    private void callToPreliminaryExaminationAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPreliminaryExaminationDetailsJson(hospital_database));

            Call<PreliminaryExaminationDetailsModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationDetails(body);
            call.enqueue(new Callback<PreliminaryExaminationDetailsModel>() {
                @Override
                public void onResponse(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Response<PreliminaryExaminationDetailsModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()
                                && response.body().getError()==200  ) {
                            {
                                List<PreliminaryExaminationDetailsModel.Data.SurgeryType>
                                        mListSurgery = response.body().getData().getSurgeryType();

                                mArrSurgeryType.addAll(mListSurgery);
                                setAdapter();
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Used to remove all fragment in the fragment manager after logout
     */
    private void removeAllFragments() {
        try {
            List<Fragment> fragments = null;

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragments = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist)))
            {
                fragments = ((OptometristHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragments = ((DoctorHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragments = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager().getFragments();
            }


            if (fragments.size() > 0) {
                for (int i = 0; i < fragments.size(); i++) {
                    if (fragments.get(i) != null) {

                        if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                            ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                            ((OptometristHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                            ((DoctorHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                            ((CouncillorHomeActivity) mActivity).getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                    .commit();
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void callToMediclaimList() {
        try {
            Fragment fragment = new SurgeryListFragment();
            Bundle args = new Bundle();

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
            }

            args.putString(AppConstants.BUNDLE_PATIENT_NAME, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_FOLLOW_UP_DATE, "1970-01-01");
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, -1);
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_COUNCILLOR, -1);
            //fragment.setArguments(args);

            FragmentManager fragmentManager = null;

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.nav_menu_surgery))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
