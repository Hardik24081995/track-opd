package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.trackopd.CouncillorHomeActivity;
import com.trackopd.R;
import com.trackopd.adapter.CouncillorConsultingPatientAdapter;
import com.trackopd.model.CouncillorConsultingPatientModel;
import com.trackopd.utils.AppConstants;

import java.util.ArrayList;

public class CouncillorConsultingPatientFragment extends Fragment {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private SwipeRefreshLayout mSwipeRefreshView;
    private RecyclerView mRecyclerView;
    private ArrayList<CouncillorConsultingPatientModel> mArrCouncillorConsultingPatient;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_councillor_consulting_patient, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrCouncillorConsultingPatient = new ArrayList<>();

        getIds();
        setRegListeners();
        setAdapterData();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Set the prepare menu options
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        final MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(true);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                return true;

            case R.id.action_search:
                callToSearchCouncillorConsultingPatient();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Recycler View
            mRecyclerView = mView.findViewById(R.id.recycler_view);

            // Swipe Refresh View
            mSwipeRefreshView = mView.findViewById(R.id.swipe_refresh_view);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mSwipeRefreshView.setOnRefreshListener(refreshListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mGridLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(mGridLayoutManager);

            for (int i = 1; i <= 10; i++) {
                CouncillorConsultingPatientModel councillorConsultingPatientModel = new CouncillorConsultingPatientModel();
                councillorConsultingPatientModel.setProfilePic(AppConstants.STR_EMPTY_STRING);
                councillorConsultingPatientModel.setFirstName("Nick");
                councillorConsultingPatientModel.setLastName("Johnson " + i);
                councillorConsultingPatientModel.setMobileNo("1234567890");
                councillorConsultingPatientModel.setPatientCode("PATIENT301118" + i);
                councillorConsultingPatientModel.setLastVisitedDate("30/11/2018");
                mArrCouncillorConsultingPatient.add(councillorConsultingPatientModel);
            }

            CouncillorConsultingPatientAdapter mAdapterCouncillorConsultingPatient = new CouncillorConsultingPatientAdapter(mActivity, mRecyclerView, mArrCouncillorConsultingPatient);
            mRecyclerView.setAdapter(mAdapterCouncillorConsultingPatient);

            if (mArrCouncillorConsultingPatient.size() != 0) {
                mRecyclerView.setVisibility(View.VISIBLE);
            } else {
                mRecyclerView.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners (Swipe Refresh)
     */
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            mSwipeRefreshView.setRefreshing(false);
        }
    };

    /**
     * call Search Councillor Consulting Patient fragment
     */
    private void callToSearchCouncillorConsultingPatient() {

        ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                ().getString(R.string.header_search));
        Fragment fragment = new SearchCouncillorConsultingPatientFragment();
        FragmentManager fragmentManager =
                ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
        fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                fragment, mActivity.getResources()
                        .getString(R.string.tag_search_councillor_consulting_patient))
                .addToBackStack(mActivity.getResources()
                        .getString(R.string.back_stack_search_councillor_consulting_patient))
                .commit();
    }
}
