package com.trackopd.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.R;
import com.trackopd.model.AddPackageItemModel;
import com.trackopd.model.AddPatientModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.KeyboardUtility;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPackageFragment extends Fragment {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private EditText mEditTitle, mEditDate, mEditDiscountAmount;
    private ImageView mImageAddItem;
    private LinearLayout mLinearContainer;
    private Spinner mSpinnerItem;
    private Button mButtonSubmit;
    private float amount;
    private String mPackageAmount;
    private TextView mTextTotal;
    private JSONArray jsonArray;
    private ArrayList<String> mArrPackageItem;
    private ArrayList<AddPackageItemModel> mArrPackageItemList;
    private int mSelectedPackageItemIndex, mPackageItemId;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_add_package, container, false);
        mActivity = getActivity();
        mArrPackageItem = new ArrayList<>();
        mArrPackageItemList = new ArrayList<>();

        getIds();
        setRegListeners();
        setData();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);

        MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(false);
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Edit Text Views
            mEditTitle = mView.findViewById(R.id.edit_add_package_title);
            mEditDate = mView.findViewById(R.id.edit_add_package_date);
            mEditDiscountAmount = mView.findViewById(R.id.edit_add_package_discount_amount);

            // Text Views
            mTextTotal = mView.findViewById(R.id.text_add_package_total_package_amount);

            // Image Views
            mImageAddItem = mView.findViewById(R.id.image_add_package_add_item);

            // Linear Layouts
            mLinearContainer = mView.findViewById(R.id.linear_add_package_dynamic_view);

            // Buttons
            mButtonSubmit = mView.findViewById(R.id.button_add_package_submit);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Item Click Listener
            mEditDate.setOnClickListener(clickListener);
            mImageAddItem.setOnClickListener(clickListener);
            mButtonSubmit.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            // Set current date and time
            mEditDate.setText(Common.setCurrentDate(mActivity));

            mTextTotal.setText("0.0");
            amount = 0;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.edit_add_package_date:
                    Common.openDatePicker(mActivity, mEditDate);
                    break;

                case R.id.image_add_package_add_item:
                    doAddMedication();
                    break;

                case R.id.button_add_package_submit:
                    doAddPackage();
                    break;
            }
        }
    };

    /**
     * Spinner on selected item click listeners
     */
    private AdapterView.OnItemSelectedListener onItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
            switch (adapterView.getId()) {
                case R.id.spinner_row_add_package_item:
                    mSelectedPackageItemIndex = adapterView.getSelectedItemPosition();

                    mPackageItemId = (Integer.parseInt(mArrPackageItemList.get(mSelectedPackageItemIndex).getBillingID()));
                    String mPackageItemName = (mArrPackageItemList.get(mSelectedPackageItemIndex).getServicesName());
                    mPackageAmount = (mArrPackageItemList.get(mSelectedPackageItemIndex).getRate());
                    updateTotal();
                    Common.insertLog("mPackageID::> " + mPackageItemId);
                    Common.insertLog("mPackageName::> " + mPackageItemName);
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    /**
     * This method should add the medication dynamically
     */
    private void doAddMedication() {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View addView = layoutInflater.inflate(R.layout.row_package_item, null);

            // Image Views
            ImageView mImageRemove = addView.findViewById(R.id.image_row_add_package_remove_item);
            ImageView mImageItem = addView.findViewById(R.id.image_row_add_package_item);

            // Spinner
            mSpinnerItem = addView.findViewById(R.id.spinner_row_add_package_item);

            // Set Image Color as per the theme applied
            mImageItem.setColorFilter(Common.setThemeColor(mActivity));

            //ToDo: Sets the spinner color as per the theme applied
            mSpinnerItem.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

            // ToDo: Package Item - Spinner Item Select Listener
            mSpinnerItem.setOnItemSelectedListener(onItemSelectedListener);

            callToPackageItemAPI();

            // ToDo: Item Remove Click Listener
            mImageRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mLinearContainer.removeView(addView);
                    updateTotal();
                    bindArrayForPackageItem();
                }
            });

            // Add dynamic view
            mLinearContainer.addView(addView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update Total Val set Text
     */
    @SuppressLint("SetTextI18n")
    private void updateTotal() {
        amount = 0;
        if (mLinearContainer.getChildCount() > 0) {
            for (int i = 0; i < mLinearContainer.getChildCount(); i++) {
                Common.insertLog("IIIIII:> " + i);
                View view = mLinearContainer.getChildAt(i);
                mSpinnerItem = view.findViewById(R.id.spinner_row_add_package_item);
                int pos = mSpinnerItem.getSelectedItemPosition();

                if (mArrPackageItemList.get(pos).getRate().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                    amount = amount + 0;
                } else {
                    amount = amount + Float.parseFloat(mArrPackageItemList.get(pos).getRate());
                }
            }
        }
        mTextTotal.setText(amount + AppConstants.STR_EMPTY_STRING);
    }

    /**
     * Bind Array On Add Package Item List
     */
    public void bindArrayForPackageItem() {
        try {
            jsonArray = new JSONArray();
            for (int j = 0; j < mLinearContainer.getChildCount(); j++) {
                View view = mLinearContainer.getChildAt(j);
                mSpinnerItem = view.findViewById(R.id.spinner_row_add_package_item);
                int pos = mSpinnerItem.getSelectedItemPosition();
                JSONObject jsonObject = new JSONObject();
                jsonObject.put(WebFields.ADD_PACKAGE.REQUEST_ARR_BILLING_ID, mArrPackageItemList.get(pos).getBillingID());
                jsonObject.put(WebFields.ADD_PACKAGE.REQUEST_ARR_AMOUNT, mArrPackageItemList.get(pos).getRate());
                jsonArray.put(jsonObject);
                Common.insertLog("JsonArray:::> " + jsonArray.toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method checks the validation first then call the API
     */
    private void doAddPackage() {
        KeyboardUtility.HideKeyboard(mActivity, mEditDate);
        if (checkValidation()) {
            callToAddPackageAPI();
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {
        boolean status = true;

        String mTitle = mEditTitle.getText().toString().trim();
        String mDiscountAmount = mEditDiscountAmount.getText().toString().trim();

        mEditTitle.setError(null);
        mEditDiscountAmount.setError(null);

        if (TextUtils.isEmpty(mTitle)) {
            mEditTitle.requestFocus();
            mEditTitle.setError(mActivity.getResources().getString(R.string.error_field_required));
            status = false;
        }

        if (TextUtils.isEmpty(mDiscountAmount)) {
            mEditDiscountAmount.requestFocus();
            mEditDiscountAmount.setError(mActivity.getResources().getString(R.string.error_field_required));
            status = false;
        }

        if (status) {
            if (mArrPackageItemList.size() == 0 || mTextTotal.getText().toString().trim().equalsIgnoreCase("0.0")) {
                status = false;
                Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.error_select_item));
            }
            if (!status) {
                return status;
            }
        }
        return status;
    }

    /**
     * This method should call the Package Item API
     */
    private void callToPackageItemAPI() {
        try {
            mArrPackageItemList.clear();
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPackageItemListJson(mDatabaseName));

            Call<AddPackageItemModel> call = RetrofitClient.createService(ApiInterface.class).getPackageItemList(body);
            call.enqueue(new Callback<AddPackageItemModel>() {
                @Override
                public void onResponse(@NonNull Call<AddPackageItemModel> call, @NonNull Response<AddPackageItemModel> response) {
                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            assert response.body() != null;
                            if (response.body().getData() != null) {
                                mArrPackageItemList.addAll(response.body().getData());
                            }
                            setAdapterData();
                        } else
                            setAdapterData();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddPackageItemModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    setAdapterData();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            if (mArrPackageItemList.size() > 0) {
                for (AddPackageItemModel packageModel : mArrPackageItemList) {
                    mArrPackageItem.add(packageModel.getServicesName());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerItem.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity, android.R.layout.simple_spinner_item, mArrPackageItem) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedPackageItemIndex) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerItem.setAdapter(adapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call Add Package API
     */
    private void callToAddPackageAPI() {
        try {
            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
            String mTitle = mEditTitle.getText().toString().trim();
            String mDiscountAmount = mEditDiscountAmount.getText().toString().trim();
            String mConvertedDate = Common.convertDateToServer(mActivity, mEditDate.getText().toString().trim());
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            bindArrayForPackageItem();
            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setCouncillorAddPackageJson(mUserId, mTitle, amount, mDiscountAmount, mConvertedDate, jsonArray, hospital_database));

            Call<AddPatientModel> call = RetrofitClient.createService(ApiInterface.class).addPackage(requestBody);
            call.enqueue(new Callback<AddPatientModel>() {
                @Override
                public void onResponse(@NonNull Call<AddPatientModel> call, @NonNull Response<AddPatientModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                Common.setCustomToast(mActivity, mMessage);
                                redirectedToCouncillorPackageFragment();
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddPatientModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method redirected you to the Councillor Package Fragment
     */
    private void redirectedToCouncillorPackageFragment() {
        try {
            Fragment fragment = new CouncillorPackageFragment();
            ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.nav_menu_package));
            FragmentManager fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.nav_menu_package))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}