package com.trackopd.fragments;


import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.gson.Gson;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.HistoryPreExaminationAdapter;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.PreliminaryExaminationModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class PatientPreExaminationFragment extends Fragment {

    private static ArrayList<HistoryPreliminaryExaminationModel.Data> mArrHistory;
    private View mView;
    private Activity mActivity;
    private RecyclerView recyclerView;
    private RelativeLayout mRelativeNoData;
    private int mAppointmentId, mCurrentPage = 1, mHistoryDoctorID = -1;
    private ArrayList<PreliminaryExaminationModel> mArrPreliminaryExamination;
    private String mPatientFirstName, mPatientLastName, mPatientMobile, mPatientId, mDoctorId, mTitle;
    private String mUserType, hospital_database;

    public PatientPreExaminationFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_partient_pre_examination, container, false);

        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrPreliminaryExamination = new ArrayList<>();
        mArrHistory = new ArrayList<>();

        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);
        hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

        getBundle();
        getIds();
        callToPrelminarHistoryPreExamination();

        setHasOptionsMenu(true);

        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = getArguments();
            if (bundle != null) {
                mPatientFirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mPatientLastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mPatientMobile = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_USER_ID);
                mAppointmentId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mTitle = bundle.getString(AppConstants.STR_TITLE);
                mDoctorId = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIds() {
        try {
            // Recycler View
            recyclerView = mView.findViewById(R.id.recycler_view);

            // Relative Layouts
            mRelativeNoData = mView.findViewById(R.id.relative_no_data_available);

            LinearLayoutManager manager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(manager);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callToPrelminarHistoryPreExamination() {
        try {
            String defualt_date = "1000-01-01";
            String Mobile = AppConstants.STR_EMPTY_STRING;
            String Type = "PreExamination";

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setHistoryPreliminaryExaminationJson(defualt_date, mCurrentPage, mPatientId, Mobile, mHistoryDoctorID, Type, hospital_database));

            Call<HistoryPreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationHistory(body);
            call.enqueue(new Callback<HistoryPreliminaryExaminationModel>() {
                @Override
                public void onResponse(Call<HistoryPreliminaryExaminationModel> call, Response<HistoryPreliminaryExaminationModel> response) {
                    Common.insertLog("Response History::::> " + new Gson().toJson(response.body()));
                    try {
                        if (response.body() != null &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body().getData().size() > 0) {
                                mArrHistory.addAll(response.body().getData());
                            }

                            setAdapterInvestigation();

                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<HistoryPreliminaryExaminationModel> call, Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setAdapterInvestigation() {

        HistoryPreExaminationAdapter adapter = new HistoryPreExaminationAdapter(mActivity, recyclerView,
                mArrHistory,true) {
            @Override
            protected void onSelectedItem(Activity mActivity, HistoryPreliminaryExaminationModel.Data data) {
                super.onSelectedItem(mActivity, data);
                callToAddPreliminaryExaminationFragment(data);

            }

            @Override
            protected void onSelectedGraph(Activity _mActivity,
                                           ArrayList<HistoryPreliminaryExaminationModel.PreExamination> preExamination,
                                           int adapterPosition) {
                super.onSelectedGraph(_mActivity, preExamination, adapterPosition);
                openDialogPreExaminationChart(mActivity,preExamination,adapterPosition);
            }
        };


        recyclerView.setAdapter(adapter);
        setVisibility();
    }

    public void setVisibility() {
        if (mArrHistory.size() > 0) {
            recyclerView.setVisibility(View.VISIBLE);
           // ShowLineChart();
            mRelativeNoData.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.GONE);
           // ShowLineChart();
            mRelativeNoData.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Sets up the Optometrist Add New Preliminary Examination Fragment
     *
     * @param data Receptionist Patients Model
     */
    private void callToAddPreliminaryExaminationFragment(HistoryPreliminaryExaminationModel.Data data) {

        try {
            Bundle mBundle = new Bundle();

            mBundle.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, data.getPatientFirstName());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, data.getPatientLastName());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE, data.getPatientMobileNo());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_PROFILE, "");
            mBundle.putString(AppConstants.BUNDLE_PATIENT_ID, data.getPatientID());
            mBundle.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, Integer.parseInt(data.getAppointmentID()));
            mBundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, data.getDoctorID());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE, data.getTreatmentDate());
            mBundle.putString(AppConstants.BUNDLE_PRELIMINARY_EXAMINATION_ID, data.getPreliminaryExaminationID());
            mBundle.putString(AppConstants.BUNDLE_CHECK_TYPE, mActivity.getString(R.string.tab_vision));

            Fragment fragment = new AddNewPreliminaryExaminationFragment(data);
            fragment.setArguments(mBundle);


            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager =
                        ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager =
                        ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager =
                        ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager =
                        ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_preliminary_examination))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_preliminary_examination))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void ShowLineChart() {

        TextView mTextTitle = mView.
                findViewById(R.id.text_view_custom_dialog_history_pre_examination_title);
        ImageView mImageClose = mView.
                findViewById(R.id.image_custom_dialog_history_pre_examination_graph_close);

        LineChart lineChart = mView.
                findViewById(R.id.line_chart_custom_dialog_history_pre_examination_graph);

//        mTextTitle.setVisibility(View.GONE);
//        mTextTitle.setText(mActivity.getResources().getString(R.string.tab_pre_examination));
//        mImageClose.setColorFilter(Common.setThemeColor(mActivity));

//        mImageClose.setVisibility(View.GONE);


        //****Line Chart Code START **********//
        lineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

            }

            @Override
            public void onNothingSelected() {

            }
        });

        lineChart.setDrawGridBackground(true);
        lineChart.getDescription().setEnabled(true);
        lineChart.setDrawBorders(false);

        lineChart.getAxisLeft().setEnabled(true);
        lineChart.getAxisRight().setDrawAxisLine(true);
        lineChart.getAxisRight().setDrawGridLines(true);
        lineChart.getXAxis().setDrawAxisLine(true);
        lineChart.getXAxis().setDrawGridLines(true);
        lineChart.getXAxis().setGranularity(1f); // minimum axis-step (interval) is 1
        // enable touch gestures
        lineChart.setTouchEnabled(true);
        // enable scaling and dragging
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);
        // if disabled, scaling can be done on x- and y-axis separately
        lineChart.setPinchZoom(true);


        Legend l = lineChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);

        //getData(lineChart, preExamination);
    }


    /**
     * Open Dialog Pre Examination History graph
     *
     * @param mActivity
     * @param preExamination
     * @param adapterPosition
     */
    public void openDialogPreExaminationChart(Activity mActivity,
                                              ArrayList<HistoryPreliminaryExaminationModel.PreExamination> preExamination, int adapterPosition) {
        final Dialog dialog = new Dialog(this.mActivity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_dialog_history_pre_examination_graph);

        TextView mTextTitle = dialog.
                findViewById(R.id.text_view_custom_dialog_history_pre_examination_title);
        ImageView mImageClose = dialog.
                findViewById(R.id.image_custom_dialog_history_pre_examination_graph_close);

        LineChart lineChart = dialog.
                findViewById(R.id.line_chart_custom_dialog_history_pre_examination_graph);

        mTextTitle.setText(this.mActivity.getResources().getString(R.string.tab_pre_examination));

        mImageClose.setColorFilter(Common.setThemeColor(this.mActivity));

        mImageClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        //****Line Chart Code START **********//
        lineChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
            @Override
            public void onValueSelected(Entry e, Highlight h) {

            }

            @Override
            public void onNothingSelected() {

            }
        });

        lineChart.setDrawGridBackground(true);
        lineChart.getDescription().setEnabled(true);
        lineChart.setDrawBorders(false);

        lineChart.getAxisLeft().setEnabled(true);
        lineChart.getAxisRight().setDrawAxisLine(true);
        lineChart.getAxisRight().setDrawGridLines(true);
        lineChart.getXAxis().setDrawAxisLine(true);
        lineChart.getXAxis().setDrawGridLines(true);
        lineChart.getXAxis().setGranularity(1f); // minimum axis-step (interval) is 1
        // enable touch gestures
        lineChart.setTouchEnabled(true);
        // enable scaling and dragging
        lineChart.setDragEnabled(true);
        lineChart.setScaleEnabled(true);
        // if disabled, scaling can be done on x- and y-axis separately
        lineChart.setPinchZoom(true);


        Legend l = lineChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);

        getData(lineChart,preExamination,adapterPosition);

        //****Line Chart Code END **********//
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
    }

    private void getData(LineChart lineChart, ArrayList<HistoryPreliminaryExaminationModel.PreExamination> preExamination,
                         int adapterPosition) {


        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        ArrayList<Entry> LeftAct = new ArrayList<>();
        ArrayList<Entry> RightAct = new ArrayList<>();

        final ArrayList<String> xAxisLabel = new ArrayList<>();


        for (int l = 0; l < preExamination.size(); l++) {


            float pos = l;

            for (int i = 0; i < preExamination.size(); i++) {
                float Left = 0;
                if (!preExamination.get(l).getLNCT()
                        .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {

                    Left = Float.parseFloat(preExamination.get(l).getLNCT());
                }
                LeftAct.add(new Entry(pos, Left));

                float right = 0;
                if (!preExamination.get(l).getRNCT().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {

                    right = Float.parseFloat(preExamination.get(l).getRNCT());
                }
                RightAct.add(new Entry(pos, right));

                xAxisLabel.add(mArrHistory.get(adapterPosition).getTreatmentDate());

                // Date.add(new Entry(pos,mArrHistory.get(l).getTreatmentDate()));
            }
        }


        LineDataSet d = new LineDataSet(LeftAct, "Left NCT");
        d.setLineWidth(3.0f);
        d.setCircleRadius(5f);

        d.setColor(Color.RED);//
        d.setCircleColor(Color.RED);
        dataSets.add(d);
        d.setAxisDependency(YAxis.AxisDependency.LEFT);

        LineDataSet right = new LineDataSet(RightAct, "Right NCT");
        right.setLineWidth(3.0f);
        right.setCircleRadius(5f);

        right.setColor(Color.GREEN);
        right.setCircleColor(Color.GREEN);
        dataSets.add(right);
        right.setAxisDependency(YAxis.AxisDependency.LEFT);


        lineChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(xAxisLabel));

        LineData data = new LineData(dataSets);
        lineChart.setData(data);

        lineChart.invalidate();
    }

}
