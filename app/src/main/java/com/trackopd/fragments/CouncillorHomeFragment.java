package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.model.getCouncillorHomeModel;
import com.trackopd.model.getReceptionistModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;

import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CouncillorHomeFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private TextView mTextAppointment,mTextFollowup;
    private AppUtil mAppUtils;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_councillor_home, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mAppUtils=new AppUtil(mActivity);

        getIds();
        setData();
        callToCouncillorAPI();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Declare Id on Object
     */
    private void getIds() {
        try{
            mTextAppointment=mView.findViewById(R.id.text_councillor_home_today_appointment);
            mTextFollowup=mView.findViewById(R.id.text_councillor_home_today_followup);
        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }

    private void setData() {
        mTextAppointment.setText("(0)");
        mTextFollowup.setText("(0)");
    }


    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    /**
     *  Call To Councillor Home API
     */
    private void callToCouncillorAPI() {
        if (!mAppUtils.getConnectionState()){

        }else {

            String hospital_database= GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);
            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setCouncillorHomeListJson(hospital_database));

            //Common.showLoadingDialog(mActivity,getString(R.string.action_upload));
            Call<getCouncillorHomeModel> call = RetrofitClient.createService(ApiInterface.class)
                    .getCouncillorHomeListApi(requestBody);
            call.enqueue(new Callback<getCouncillorHomeModel>() {
                @Override
                public void onResponse(Call<getCouncillorHomeModel> call, Response<getCouncillorHomeModel> response) {
                    Common.insertLog(response.body().toString());
                    if (response.isSuccessful() &&
                            response.body().getError()==AppConstants.API_SUCCESS_ERROR){
                        setResposneData(response.body().getData());
                    }else {
                        Common.setCustomToast(mActivity,response.body().getMessage());
                    }
                }
                @Override
                public void onFailure(Call<getCouncillorHomeModel> call, Throwable t) {
                    Common.insertLog(t.getMessage());
                    Common.setCustomToast(mActivity,t.getMessage());
                }
            });
        }
    }

    /**
     * this method Ser Response Data
     * @param data -Data Get Councillor Home
     */
    private void setResposneData(List<getCouncillorHomeModel.Data> data) {
        try{
            if (data!=null && data.size()>0){
                String appointment=data.get(0).getTotalAppointment();
                String followup=data.get(0).getFollowUp();

                mTextAppointment.setText("("+appointment+")");
                mTextFollowup.setText("("+followup+")");
            }
        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }
}
