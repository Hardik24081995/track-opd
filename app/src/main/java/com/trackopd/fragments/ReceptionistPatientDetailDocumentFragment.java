package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.ReceptionistPatientDetailDocumentAdapter;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.ReceptionistPatientDetailDocumentModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.utils.RecyclerProgressView;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceptionistPatientDetailDocumentFragment extends Fragment {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private SwipeRefreshLayout mSwipeRefreshView;
    private RecyclerView mRecyclerView;
    private ArrayList<ReceptionistPatientDetailDocumentModel> mArrReceptionistPatientDocument;
    private boolean isFirstTime = true, isStarted = false, isVisible = false, isRefresh = false, isLoadMore = false;
    private AppUtil mAppUtils;
    private RelativeLayout mRelativeNoData, mRelativeNoInternet;
    private ReceptionistPatientDetailDocumentAdapter mAdapterReceptionistPatientDetailDocument;
    private int currentPageIndex = 1, lastFetchRecord = 0, totalRecords = 0;
    private String mAppointmentID, mPatientId;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_receptionist_patient_detail_document, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrReceptionistPatientDocument = new ArrayList<>();
        Common.insertLog("Tab 4");
        mAppUtils = new AppUtil(getActivity());

        getIds();
        setRegListeners();
        getBundle();
        callShimmerView();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.invalidateOptionsMenu();
        mActivity.getMenuInflater().inflate(R.menu.menu_detail_document, menu);

        MenuItem menuAdd = menu.findItem(R.id.action_detail_document_add);
        menuAdd.setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_detail_document_add:
                callToAddDocumentFragment();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = getArguments();
            if (bundle != null) {
                mAppointmentID = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Recycler View
            mRecyclerView = mView.findViewById(R.id.recycler_view);

            // Swipe Refresh View
            mSwipeRefreshView = mView.findViewById(R.id.swipe_refresh_view);

            // Relative Layouts
            mRelativeNoData = mView.findViewById(R.id.relative_no_data_available);
            mRelativeNoInternet = mView.findViewById(R.id.relative_no_internet);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mSwipeRefreshView.setOnRefreshListener(refreshListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_retry:
                    callShimmerView();
                    break;
            }
        }
    };

    /**
     * Set On Refresh Listeners
     */
    private SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {

        @Override
        public void onRefresh() {
            isFirstTime = true;

            if (!mAppUtils.getConnectionState()) {
                mRelativeNoInternet.setVisibility(View.VISIBLE);
                mSwipeRefreshView.setRefreshing(false);
                mArrReceptionistPatientDocument.clear();
                setViewVisibility();
            } else {
                mRelativeNoInternet.setVisibility(View.GONE);
                if (!isLoadMore) {
                    isRefresh = true;

                    if (mArrReceptionistPatientDocument != null)
                        mArrReceptionistPatientDocument.clear();
                    if (mAdapterReceptionistPatientDetailDocument != null)
                        mAdapterReceptionistPatientDetailDocument.notifyDataSetChanged();
                    currentPageIndex = 1;
                    callShimmerView();
                } else
                    mSwipeRefreshView.setRefreshing(false);
            }
        }
    };

    /**
     * This method is used to call the shimmer effect for recycler view before calling the API
     */
    private void callShimmerView() {
        if (!mAppUtils.getConnectionState()) {
            mRelativeNoInternet.setVisibility(View.VISIBLE);
            mArrReceptionistPatientDocument.clear();
            setViewVisibility();
        } else {
            mRelativeNoInternet.setVisibility(View.GONE);
            if (isFirstTime) {
                showProgressView();
                isFirstTime = false;
            }
            callDocumentListAPI();
        }
    }

    /**
     * Show progress view when page load for the first time
     */
    private void showProgressView() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.VISIBLE);

                RecyclerProgressView.startProgress((LinearLayout) mView.findViewById(R.id.linear_add_recycler_progress_view),
                        R.layout.row_receptionist_patient_detail_document_item, getActivity());
            }
        }, 100);
    }

    /**
     * This method should call the Document listing for receptionist
     */
    private void callDocumentListAPI() {
        try {
            RequestBody requestBody = null;

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            if (mAppointmentID != null) {
                requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                        APICommonMethods.setReceptionistDocumentListJson(mAppointmentID, hospital_database));
            } else if (mPatientId != null) {
                requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                        APICommonMethods.setReceptionistPatinetWiseDocumentListJson(mPatientId, hospital_database));
            }

            Call<ReceptionistPatientDetailDocumentModel> call = RetrofitClient.createService(ApiInterface.class).getReceptionistDocumentList(requestBody);
            call.enqueue(new Callback<ReceptionistPatientDetailDocumentModel>() {
                @Override
                public void onResponse(@NonNull Call<ReceptionistPatientDetailDocumentModel> call, @NonNull Response<ReceptionistPatientDetailDocumentModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        stopProgressView();

                        if (response.isSuccessful() && response.body().getError() == 200) {
                            if (mArrReceptionistPatientDocument != null && mArrReceptionistPatientDocument.size() > 0 &&
                                    mAdapterReceptionistPatientDetailDocument != null) {
                                mArrReceptionistPatientDocument.addAll(response.body().getData());
                                mAdapterReceptionistPatientDetailDocument.notifyDataSetChanged();
                                lastFetchRecord = mArrReceptionistPatientDocument.size();
                            } else {
                                mArrReceptionistPatientDocument = (ArrayList<ReceptionistPatientDetailDocumentModel>) response.body().getData();
                                setAdapterData();
                                //lastFetchRecord = mArrReceptionistPatientDocument.size();
                                setLoadMoreClickListener();

                                if (mRecyclerView.getVisibility() == View.GONE) {
                                    stopProgressView();
                                }
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                            stopProgressView();
                            setViewVisibility();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ReceptionistPatientDetailDocumentModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    stopProgressView();
                    setViewVisibility();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData() {
        try {
            mRecyclerView.setHasFixedSize(true);
            GridLayoutManager mGridLayoutManager = new GridLayoutManager(mActivity, 2);
            mRecyclerView.setLayoutManager(mGridLayoutManager);

            mAdapterReceptionistPatientDetailDocument = new ReceptionistPatientDetailDocumentAdapter(mActivity, mRecyclerView, mArrReceptionistPatientDocument);
            mRecyclerView.setAdapter(mAdapterReceptionistPatientDetailDocument);

            setViewVisibility();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the view visibility as per the array size
     */
    private void setViewVisibility() {
        stopProgressView();
        if (mArrReceptionistPatientDocument.size() != 0) {
            mRelativeNoData.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mRelativeNoData.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    /**
     * Show progress view when page load for the first time
     */
    private void stopProgressView() {
        try {
            mSwipeRefreshView.setRefreshing(false);
            isRefresh = false;
            isLoadMore = false;
            RecyclerProgressView.stopProgress();
            mView.findViewById(R.id.linear_add_recycler_progress_view).setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            hideProgressBar();
        }
    }

    /**
     * Hide the progress bar
     */
    private void hideProgressBar() {
        try {
            if (mArrReceptionistPatientDocument != null && mArrReceptionistPatientDocument.size() > 0 &&
                    mArrReceptionistPatientDocument.get(mArrReceptionistPatientDocument.size() - 1) == null) {

                mArrReceptionistPatientDocument.remove(mArrReceptionistPatientDocument.size() - 1);
                mAdapterReceptionistPatientDetailDocument.notifyItemRemoved(mArrReceptionistPatientDocument.size());

                mAdapterReceptionistPatientDetailDocument.notifyDataSetChanged();
                mAdapterReceptionistPatientDetailDocument.setLoaded();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * On load more click listener
     */
    private void setLoadMoreClickListener() {
        mAdapterReceptionistPatientDetailDocument.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (mArrReceptionistPatientDocument != null && mArrReceptionistPatientDocument.size() > 0 && mArrReceptionistPatientDocument.size() <
                        totalRecords) {
                    if (!mAppUtils.getConnectionState()) {
                        mRelativeNoInternet.setVisibility(View.VISIBLE);
                        mArrReceptionistPatientDocument.clear();
                        setViewVisibility();
                    } else {
                        mRelativeNoInternet.setVisibility(View.GONE);
                        if (!isRefresh) {
                            isLoadMore = true;
                            mArrReceptionistPatientDocument.add(null);
                            mAdapterReceptionistPatientDetailDocument.notifyItemInserted(mArrReceptionistPatientDocument.size() - 1);

                            currentPageIndex = (mArrReceptionistPatientDocument.size() / Common.getPageSizeForPagination(mActivity)) + 1;
                            callDocumentListAPI();
                        }
                    }
                }
            }
        });
    }

    /**
     * This method redirects you to the Add Document Fragment
     */
    private void callToAddDocumentFragment() {
        try {
            ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.nav_menu_document));
            Fragment fragment = new AddDocumentFragment();
            FragmentManager fragmentManager =
                    ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_document))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_document))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Receptionist Patient Detail Prescription Fragment on visible
     *
     * @param //isVisibleToUser - This can check the fragment is visible when swiping and will return
     *                          the boolean value based on that
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        isVisible = isVisibleToUser;
        if (isStarted && isVisible) {

            mArrReceptionistPatientDocument.clear();
            if (mAdapterReceptionistPatientDetailDocument != null) {
                mAdapterReceptionistPatientDetailDocument.notifyDataSetChanged();
            }
            getBundle();
            callShimmerView();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        isStarted = true;
        if (isVisible) {
            mArrReceptionistPatientDocument.clear();
            if (mAdapterReceptionistPatientDetailDocument != null) {
                mAdapterReceptionistPatientDetailDocument.notifyDataSetChanged();
            }
            getBundle();
            callShimmerView();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isVisible = false;
        isStarted = false;
    }
}
