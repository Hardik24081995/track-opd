package com.trackopd.fragments;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.trackopd.PhotoEditorActivity;
import com.trackopd.PreviewImageActivity;
import com.trackopd.R;
import com.trackopd.adapter.HistoryDoodleAdapter;
import com.trackopd.model.AddDoodleImageModel;
import com.trackopd.model.CloseFileModel;
import com.trackopd.model.DoodleImageModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.SessionManager;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DoodleFragment extends Fragment {

    private static ArrayList<HistoryPreliminaryExaminationModel.Data> mArrHistory;
    public int WRITE_EXTERNAL_PERMISION_CODE = 109, mCurrentPage = 1, mHistoryDoctorID = -1;
    private View mView;
    private Activity mActivity;
    private ImageView mImageRightEyeAdnex, mImageLeftEyeAdnex, mImageRightEyeAnteriorSegment, mImageLeftEyeAnteriorSegment,
            mImageRightEyeLens, mImageLeftEyeLens, mImageRightEyeFundus, mImageLeftEyeFundus, mImageRightEyeGonioscopy, mImageLeftEyeGonioscopy;
    private LinearLayout mLinearRightEyeAdnexEdit, mLinearRightEyeAdnexDelete, mLinearLeftEyeAdnexEdit, mLinearLeftEyeAdnexDelete,
            mLinearRightEyeAnteriorSegmentEdit, mLinearRightEyeAnteriorSegmentDelete, mLinearLeftEyeAnteriorSegmentEdit,
            mLinearLeftEyeAnteriorSegmentDelete, mLinearRightEyeLensEdit, mLinearRightEyeLensDelete, mLinearLeftEyeLensEdit,
            mLinearLeftEyeLensDelete, mLinearRightEyeFundusEdit, mLinearRightEyeFundusDelete, mLinearLeftEyeFundusEdit,
            mLinearLeftEyeFundusDelete, mLinearRightEyeGonioscopyEdit, mLinearRightEyeGonioscopyDelete, mLinearLeftEyeGonioscopyEdit,
            mLinearLeftEyeGonioscopyDelete;
    private String mDoodleType, mFilePath, mDoodleUrl, mRightAdnex, mLeftAdnex, mRightAnteriorSegment, mLeftAnteriorSegment, mRightLens, mLeftLens,
            mRightFundus, mLeftFundus, mRightGonioscopy, mLeftGonioscopy, mUserId, mSelType, mFinalUrl;
    private int mAppointmentId;
    private byte[] bytes;
    private Bitmap editedBitmap;
    private int drawable;
    private String mPatientId;
    private boolean isPermissionGranted = false;
    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.image_doodle_right_eye_adnex:
                    mDoodleType = mActivity.getResources().getString(R.string.text_right_eye_adnex);
                    callToPreviewImageActivity();
                    break;

                case R.id.image_doodle_left_eye_adnex:
                    mDoodleType = mActivity.getResources().getString(R.string.text_left_eye_adnex);
                    callToPreviewImageActivity();
                    break;

                case R.id.image_doodle_right_eye_anterior_segment:
                    mDoodleType = mActivity.getResources().getString(R.string.text_right_eye_anterior_segment);
                    callToPreviewImageActivity();
                    break;

                case R.id.image_doodle_left_eye_anterior_segment:
                    mDoodleType = mActivity.getResources().getString(R.string.text_left_eye_anterior_segment);
                    callToPreviewImageActivity();
                    break;

                case R.id.image_doodle_right_eye_lens:
                    mDoodleType = mActivity.getResources().getString(R.string.text_right_eye_lens);
                    callToPreviewImageActivity();
                    break;

                case R.id.image_doodle_left_eye_lens:
                    mDoodleType = mActivity.getResources().getString(R.string.text_left_eye_lens);
                    callToPreviewImageActivity();
                    break;

                case R.id.image_doodle_right_eye_fundus:
                    mDoodleType = mActivity.getResources().getString(R.string.text_right_eye_fundus);
                    callToPreviewImageActivity();
                    break;

                case R.id.image_doodle_left_eye_fundus:
                    mDoodleType = mActivity.getResources().getString(R.string.text_left_eye_fundus);
                    callToPreviewImageActivity();
                    break;

                case R.id.image_doodle_right_eye_gonioscopy:
                    mDoodleType = mActivity.getResources().getString(R.string.text_right_eye_gonioscopy);
                    callToPreviewImageActivity();
                    break;

                case R.id.image_doodle_left_eye_gonioscopy:
                    mDoodleType = mActivity.getResources().getString(R.string.text_left_eye_gonioscopy);
                    callToPreviewImageActivity();
                    break;

                case R.id.linear_doodle_right_eye_adnex_edit:
                    mDoodleType = mActivity.getResources().getString(R.string.text_right_eye_adnex);
                    drawable = R.drawable.adnex_icon;
//                    openPhotoEditor();
                    checkPermission();
                    break;

                case R.id.linear_doodle_left_eye_adnex_edit:
                    mDoodleType = mActivity.getResources().getString(R.string.text_left_eye_adnex);
                    drawable = R.drawable.adnex_icon;
//                    openPhotoEditor();
                    checkPermission();
                    break;

                case R.id.linear_doodle_right_eye_anterior_segment_edit:
                    mDoodleType = mActivity.getResources().getString(R.string.text_right_eye_anterior_segment);
                    drawable = R.drawable.anterior_segment_icon;
//                    openPhotoEditor();
                    checkPermission();
                    break;

                case R.id.linear_doodle_left_eye_anterior_segment_edit:
                    mDoodleType = mActivity.getResources().getString(R.string.text_left_eye_anterior_segment);
                    drawable = R.drawable.anterior_segment_icon;
//                    openPhotoEditor();
                    checkPermission();
                    break;

                case R.id.linear_doodle_right_eye_lens_edit:
                    mDoodleType = mActivity.getResources().getString(R.string.text_right_eye_lens);
                    drawable = R.drawable.lens_icon;
//                    openPhotoEditor();
                    checkPermission();
                    break;

                case R.id.linear_doodle_left_eye_lens_edit:
                    mDoodleType = mActivity.getResources().getString(R.string.text_left_eye_lens);
                    drawable = R.drawable.lens_icon;
//                    openPhotoEditor();
                    checkPermission();
                    break;

                case R.id.linear_doodle_right_eye_fundus_edit:
                    mDoodleType = mActivity.getResources().getString(R.string.text_right_eye_fundus);
                    drawable = R.drawable.right_fundus_icon;
                    //openPhotoEditor();
                    checkPermission();
                    break;

                case R.id.linear_doodle_left_eye_fundus_edit:
                    mDoodleType = mActivity.getResources().getString(R.string.text_left_eye_fundus);
                    drawable = R.drawable.left_fundus_icon;
                    //openPhotoEditor();
                    checkPermission();
                    break;

                case R.id.linear_doodle_right_eye_gonioscopy_edit:
                    mDoodleType = mActivity.getResources().getString(R.string.text_right_eye_gonioscopy);
                    drawable = R.drawable.gonioscopy_icon;
                    //openPhotoEditor();
                    checkPermission();
                    break;

                case R.id.linear_doodle_left_eye_gonioscopy_edit:
                    mDoodleType = mActivity.getResources().getString(R.string.text_left_eye_gonioscopy);
                    drawable = R.drawable.gonioscopy_icon;
//                    openPhotoEditor();
                    checkPermission();
                    break;

                case R.id.linear_doodle_right_eye_adnex_delete:
                    mDoodleType = mActivity.getResources().getString(R.string.text_right_eye_adnex);
                    callToConformationRemoveDoodle("Adnexa Right Eye");

                    break;

                case R.id.linear_doodle_left_eye_adnex_delete:
                    mDoodleType = mActivity.getResources().getString(R.string.text_left_eye_adnex);
                    callToConformationRemoveDoodle("Adnexa Right Eye");
                    //callToDeleteDoodleAPI();
                    break;

                case R.id.linear_doodle_right_eye_anterior_segment_delete:
                    mDoodleType = mActivity.getResources().getString(R.string.text_right_eye_anterior_segment);
                    callToConformationRemoveDoodle("Anterior Segment Right Eye");
//                    callToDeleteDoodleAPI();
                    break;

                case R.id.linear_doodle_left_eye_anterior_segment_delete:
                    mDoodleType = mActivity.getResources().getString(R.string.text_left_eye_anterior_segment);
                    callToConformationRemoveDoodle("Anterior Segment Left Eye");
                    //callToDeleteDoodleAPI();
                    break;

                case R.id.linear_doodle_right_eye_lens_delete:
                    mDoodleType = mActivity.getResources().getString(R.string.text_right_eye_lens);
                    callToConformationRemoveDoodle("Lense Right Eye");
                    //callToDeleteDoodleAPI();
                    break;

                case R.id.linear_doodle_left_eye_lens_delete:
                    mDoodleType = mActivity.getResources().getString(R.string.text_left_eye_lens);
                    callToConformationRemoveDoodle("Lense Left Eye");
                    //callToDeleteDoodleAPI();
                    break;

                case R.id.linear_doodle_right_eye_fundus_delete:
                    mDoodleType = mActivity.getResources().getString(R.string.text_right_eye_fundus);
                    callToConformationRemoveDoodle("Fundus Right Eye");
                    //callToDeleteDoodleAPI();
                    break;

                case R.id.linear_doodle_left_eye_fundus_delete:
                    mDoodleType = mActivity.getResources().getString(R.string.text_left_eye_fundus);
                    callToConformationRemoveDoodle("Fundus Left Eye");
                    //callToDeleteDoodleAPI();
                    break;

                case R.id.linear_doodle_right_eye_gonioscopy_delete:
                    mDoodleType = mActivity.getResources().getString(R.string.text_right_eye_gonioscopy);
                    callToConformationRemoveDoodle("Gonioscopy Right Eye");
                    // callToDeleteDoodleAPI();
                    break;

                case R.id.linear_doodle_left_eye_gonioscopy_delete:
                    mDoodleType = mActivity.getResources().getString(R.string.text_left_eye_gonioscopy);
                    callToConformationRemoveDoodle("Gonioscopy Left Eye");
                    //callToDeleteDoodleAPI();
                    break;
            }
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_doodle, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        mArrHistory = new ArrayList<>();

        getBundle();
        getIds();
        setRegListeners();
        callToGetDoodleImagesAPI();
        callToHistoryDoodleAPI();
        setHasOptionsMenu(true);
        return mView;
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mAppointmentId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
            }
            mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Linear Layouts
            mImageRightEyeAdnex = mView.findViewById(R.id.image_doodle_right_eye_adnex);
            mImageLeftEyeAdnex = mView.findViewById(R.id.image_doodle_left_eye_adnex);
            mImageRightEyeAnteriorSegment = mView.findViewById(R.id.image_doodle_right_eye_anterior_segment);
            mImageLeftEyeAnteriorSegment = mView.findViewById(R.id.image_doodle_left_eye_anterior_segment);
            mImageRightEyeLens = mView.findViewById(R.id.image_doodle_right_eye_lens);
            mImageLeftEyeLens = mView.findViewById(R.id.image_doodle_left_eye_lens);
            mImageRightEyeFundus = mView.findViewById(R.id.image_doodle_right_eye_fundus);
            mImageLeftEyeFundus = mView.findViewById(R.id.image_doodle_left_eye_fundus);
            mImageRightEyeGonioscopy = mView.findViewById(R.id.image_doodle_right_eye_gonioscopy);
            mImageLeftEyeGonioscopy = mView.findViewById(R.id.image_doodle_left_eye_gonioscopy);

            // Linear Layouts
            mLinearRightEyeAdnexEdit = mView.findViewById(R.id.linear_doodle_right_eye_adnex_edit);
            mLinearRightEyeAdnexDelete = mView.findViewById(R.id.linear_doodle_right_eye_adnex_delete);
            mLinearLeftEyeAdnexEdit = mView.findViewById(R.id.linear_doodle_left_eye_adnex_edit);
            mLinearLeftEyeAdnexDelete = mView.findViewById(R.id.linear_doodle_left_eye_adnex_delete);
            mLinearRightEyeAnteriorSegmentEdit = mView.findViewById(R.id.linear_doodle_right_eye_anterior_segment_edit);
            mLinearRightEyeAnteriorSegmentDelete = mView.findViewById(R.id.linear_doodle_right_eye_anterior_segment_delete);
            mLinearLeftEyeAnteriorSegmentEdit = mView.findViewById(R.id.linear_doodle_left_eye_anterior_segment_edit);
            mLinearLeftEyeAnteriorSegmentDelete = mView.findViewById(R.id.linear_doodle_left_eye_anterior_segment_delete);
            mLinearRightEyeLensEdit = mView.findViewById(R.id.linear_doodle_right_eye_lens_edit);
            mLinearRightEyeLensDelete = mView.findViewById(R.id.linear_doodle_right_eye_lens_delete);
            mLinearLeftEyeLensEdit = mView.findViewById(R.id.linear_doodle_left_eye_lens_edit);
            mLinearLeftEyeLensDelete = mView.findViewById(R.id.linear_doodle_left_eye_lens_delete);
            mLinearRightEyeFundusEdit = mView.findViewById(R.id.linear_doodle_right_eye_fundus_edit);
            mLinearRightEyeFundusDelete = mView.findViewById(R.id.linear_doodle_right_eye_fundus_delete);
            mLinearLeftEyeFundusEdit = mView.findViewById(R.id.linear_doodle_left_eye_fundus_edit);
            mLinearLeftEyeFundusDelete = mView.findViewById(R.id.linear_doodle_left_eye_fundus_delete);
            mLinearRightEyeGonioscopyEdit = mView.findViewById(R.id.linear_doodle_right_eye_gonioscopy_edit);
            mLinearRightEyeGonioscopyDelete = mView.findViewById(R.id.linear_doodle_right_eye_gonioscopy_delete);
            mLinearLeftEyeGonioscopyEdit = mView.findViewById(R.id.linear_doodle_left_eye_gonioscopy_edit);
            mLinearLeftEyeGonioscopyDelete = mView.findViewById(R.id.linear_doodle_left_eye_gonioscopy_delete);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {

            SessionManager manager = new SessionManager(mActivity);
            String closeFile = manager.getPreferences("ClosingFlags", "");

            if (closeFile.equalsIgnoreCase("Yes")) {
                Toast.makeText(mActivity, "Patient File Closed", Toast.LENGTH_SHORT).show();
            } else {
                mImageRightEyeAdnex.setOnClickListener(clickListener);
                mImageLeftEyeAdnex.setOnClickListener(clickListener);
                mImageRightEyeAnteriorSegment.setOnClickListener(clickListener);
                mImageLeftEyeAnteriorSegment.setOnClickListener(clickListener);
                mImageRightEyeLens.setOnClickListener(clickListener);
                mImageLeftEyeLens.setOnClickListener(clickListener);
                mImageRightEyeFundus.setOnClickListener(clickListener);
                mImageLeftEyeFundus.setOnClickListener(clickListener);
                mImageRightEyeGonioscopy.setOnClickListener(clickListener);
                mImageLeftEyeGonioscopy.setOnClickListener(clickListener);
                mLinearRightEyeAdnexEdit.setOnClickListener(clickListener);
                mLinearRightEyeAdnexDelete.setOnClickListener(clickListener);
                mLinearLeftEyeAdnexEdit.setOnClickListener(clickListener);
                mLinearLeftEyeAdnexDelete.setOnClickListener(clickListener);
                mLinearRightEyeAnteriorSegmentEdit.setOnClickListener(clickListener);
                mLinearRightEyeAnteriorSegmentDelete.setOnClickListener(clickListener);
                mLinearLeftEyeAnteriorSegmentEdit.setOnClickListener(clickListener);
                mLinearLeftEyeAnteriorSegmentDelete.setOnClickListener(clickListener);
                mLinearRightEyeLensEdit.setOnClickListener(clickListener);
                mLinearRightEyeLensDelete.setOnClickListener(clickListener);
                mLinearLeftEyeLensEdit.setOnClickListener(clickListener);
                mLinearLeftEyeLensDelete.setOnClickListener(clickListener);
                mLinearRightEyeFundusEdit.setOnClickListener(clickListener);
                mLinearRightEyeFundusDelete.setOnClickListener(clickListener);
                mLinearLeftEyeFundusEdit.setOnClickListener(clickListener);
                mLinearLeftEyeFundusDelete.setOnClickListener(clickListener);
                mLinearRightEyeGonioscopyEdit.setOnClickListener(clickListener);
                mLinearRightEyeGonioscopyDelete.setOnClickListener(clickListener);
                mLinearLeftEyeGonioscopyEdit.setOnClickListener(clickListener);
                mLinearLeftEyeGonioscopyDelete.setOnClickListener(clickListener);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callToConformationRemoveDoodle(String subMessage) {
        try {

            String message = mActivity.getString(R.string.dialog_remove_doodle_image);

            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
            builder.setMessage(message + " " + subMessage + "?");

            // builder.setMessage(R.string.dialog_reschedule_appointment_message)
            builder.setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    callToDeleteDoodleAPI();
                }
            })
                    .setNegativeButton(R.string.action_cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            dialog.dismiss();
                        }
                    });
            builder.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should redirect you to the Photo Editor activity
     */
    private void callToPreviewImageActivity() {
        try {
            String mUrl = getPreviewURL();
            mSelType = getDoodleType();

            Intent intent = new Intent(mActivity, PreviewImageActivity.class);
            intent.putExtra(AppConstants.BUNDLE_DEFUALT_IMAGE, mDoodleType);
            if (mSelType != null && !mSelType.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                intent.putExtra(AppConstants.BUNDLE_DOODLE_URL, mUrl + mSelType);
            } else {
                intent.putExtra(AppConstants.BUNDLE_DOODLE_URL, AppConstants.STR_EMPTY_STRING);
            }
            startActivityForResult(intent, 200);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void checkPermission() {
        if (ActivityCompat.checkSelfPermission(mActivity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    WRITE_EXTERNAL_PERMISION_CODE);
        } else {
            openPhotoEditor();
        }
    }

    /**
     * This should redirect you to the Photo Editor activity
     */
    private void openPhotoEditor() {
        try {
            mSelType = getDoodleType();
            String mUrl = getPreviewURL();
            mFinalUrl = mUrl + mSelType;

            Common.insertLog("mSelType:::> " + mSelType);

            checkIfFileExists();

            Intent intent = new Intent(mActivity, PhotoEditorActivity.class);
            intent.putExtra(AppConstants.BUNDLE_DOODLE_URL, mFinalUrl);
            intent.putExtra(AppConstants.BUNDLE_DOODLE_SEL_TYPE, mSelType);
            intent.putExtra(AppConstants.BUNDLE_DOODLE_TYPE, mDoodleType);
            intent.putExtra(AppConstants.BUNDLE_DOODLE_IMAGE_PATH, bytes);
            startActivityForResult(intent, 101);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Check Permission
     *
     * @return
     */
    private boolean checkPermision() {

        boolean status = true;
        if (ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            status = false;
        }
        return status;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 109: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    openPhotoEditor();
                } else {
                    Common.setCustomToast(getActivity(), "Please Permission Granted after Perform after Edit doodle Image");
                    checkPermission();
                }
                return;
            }
        }
    }

    /**
     * This should check if file is exists into the folder it will take that filepath otherwise will take the file drawable to edit it
     */
    private void checkIfFileExists() {
        try {
            if (mSelType == null || mSelType.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), drawable);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                bytes = baos.toByteArray();
            } else {
                URL url = new URL(mFinalUrl);
                editedBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                editedBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                bytes = baos.toByteArray();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            mFilePath = data.getStringExtra(AppConstants.BUNDLE_DOODLE_IMAGE_PATH);
            mDoodleType = data.getStringExtra(AppConstants.BUNDLE_DOODLE_FILE_NAME);
            Common.insertLog("101 File Path:::> " + mFilePath);
            Common.insertLog("101 File Name:::> " + mDoodleType);

            if (requestCode == 101) {
                callToUploadDoodleImageAPI();
            } else if (requestCode == 200) {
//                setEditedImage();
            }
        }
    }

    /**
     * This should call the API for upload the doodle image
     */
    private void callToUploadDoodleImageAPI() {
        try {

            getBundle();

            String mDatabase = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);
            File file = new File(mFilePath);
            final RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

            MultipartBody.Part body = MultipartBody.Part.createFormData(WebFields.UPLOAD_DOODLE_PIC.REQUEST_IMAGE,
                    file.getName(), requestFile);
            RequestBody method = RequestBody.create(MediaType.parse("text/plain"), WebFields.UPLOAD_DOODLE_PIC.MODE);

            RequestBody mAppointmentID = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(mAppointmentId));
            RequestBody mType = RequestBody.create(MediaType.parse("text/plain"), mDoodleType);
            RequestBody mDatabaseName = RequestBody.create(MediaType.parse("text/plain"), mDatabase);
            RequestBody mLoginUserId = RequestBody.create(MediaType.parse("text/plain"), mUserId);


            Call<AddDoodleImageModel> callRepos = new RetrofitClient().createService(ApiInterface.class).uploadDoodleImage(method,
                    mAppointmentID, mDatabaseName, mType, mLoginUserId, body);
            callRepos.enqueue(new Callback<AddDoodleImageModel>() {
                @Override
                public void onResponse(@NonNull Call<AddDoodleImageModel> call, @NonNull Response<AddDoodleImageModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        int mError = response.body().getError();
                        String message = response.body().getMessage();

                        Common.hideDialog();
                        if (response.isSuccessful() && mError == 200) {
                            Common.setCustomToast(mActivity, message);
                            callToGetDoodleImagesAPI();
//                            callToHistoryDoodleAPI();
                            AddNewPreliminaryExaminationFragment.changePage(5);

                        } else {
                            Common.setCustomToast(mActivity, message);
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddDoodleImageModel> call, @NonNull Throwable t) {
                    Common.insertLog("Add UploadPics error " + t.getMessage());
                    Common.hideDialog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should call the API for get all the doodle images
     */
    private void callToGetDoodleImagesAPI() {
        try {
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setDoodlingImagesListJson(mAppointmentId, mDatabaseName));

            Call<DoodleImageModel> call = RetrofitClient.createService(ApiInterface.class).getDoodleImages(body);
            call.enqueue(new Callback<DoodleImageModel>() {
                @Override
                public void onResponse(@NonNull Call<DoodleImageModel> call, @NonNull Response<DoodleImageModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        int mError = response.body().getError();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                setsDoodleImages(response);
                                Common.insertLog("IF");
                            } else {
                                Common.insertLog("ELSE");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<DoodleImageModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the edited image to the properly named image view
     */
    private void setsDoodleImages(Response<DoodleImageModel> response) {
        try {
            mRightAdnex = response.body().getData().get(0).getRightAdnex();
            mLeftAdnex = response.body().getData().get(0).getLeftAdnex();
            mRightAnteriorSegment = response.body().getData().get(0).getRightAnteriorSegment();
            mLeftAnteriorSegment = response.body().getData().get(0).getLeftAnteriorSegment();
            mRightLens = response.body().getData().get(0).getRightLens();
            mLeftLens = response.body().getData().get(0).getLeftLens();
            mRightFundus = response.body().getData().get(0).getRightFundus();
            mLeftFundus = response.body().getData().get(0).getLeftFundus();
            mRightGonioscopy = response.body().getData().get(0).getRightGonioscopy();
            mLeftGonioscopy = response.body().getData().get(0).getLeftGonioscopy();

            /*String mJson = (new Gson().toJson(response.body()));
            JSONObject jsonObject = new JSONObject(mJson);
            JSONArray jsonArray = jsonObject.getJSONArray("Data");

            for(int i=0; i<jsonArray.length(); i++) {
                JSONObject jsonData = jsonObject.getJSONArray(0);
            }*/
//            if(jsonObject.has(WebFields.GET_DOODLE_IMAGES.RESPONSE_RIGHT_ADNEX)) {
            if (!mRightAdnex.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                setGlideImageView(mRightAdnex, mImageRightEyeAdnex, mLinearRightEyeAdnexDelete, R.drawable.adnex_icon);
            } else {
                setDeleteIconVisibility(mLinearRightEyeAdnexDelete);
                mImageRightEyeAdnex.setImageResource(R.drawable.adnex_icon);
            }
            /*} else {
                mImageRightEyeAdnex.setImageResource(R.drawable.adnex_icon);
            }*/

            if (!mLeftAdnex.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                setGlideImageView(mLeftAdnex, mImageLeftEyeAdnex, mLinearLeftEyeAdnexDelete, R.drawable.adnex_icon);
            } else {
                setDeleteIconVisibility(mLinearLeftEyeAdnexDelete);
                mImageLeftEyeAdnex.setImageResource(R.drawable.adnex_icon);
            }

            if (!mRightAnteriorSegment.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                setGlideImageView(mRightAnteriorSegment, mImageRightEyeAnteriorSegment, mLinearRightEyeAnteriorSegmentDelete, R.drawable.anterior_segment_icon);
            } else {
                setDeleteIconVisibility(mLinearRightEyeAnteriorSegmentDelete);
                mImageRightEyeAnteriorSegment.setImageResource(R.drawable.anterior_segment_icon);
            }

            if (!mLeftAnteriorSegment.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                setGlideImageView(mLeftAnteriorSegment, mImageLeftEyeAnteriorSegment, mLinearLeftEyeAnteriorSegmentDelete, R.drawable.anterior_segment_icon);
            } else {
                setDeleteIconVisibility(mLinearLeftEyeAnteriorSegmentDelete);
                mImageLeftEyeAnteriorSegment.setImageResource(R.drawable.anterior_segment_icon);
            }

            if (!mRightLens.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                setGlideImageView(mRightLens, mImageRightEyeLens, mLinearRightEyeLensDelete, R.drawable.lens_icon);
            } else {
                setDeleteIconVisibility(mLinearRightEyeLensDelete);
                mImageRightEyeLens.setImageResource(R.drawable.lens_icon);
            }

            if (!mLeftLens.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                setGlideImageView(mLeftLens, mImageLeftEyeLens, mLinearLeftEyeLensDelete, R.drawable.lens_icon);
            } else {
                setDeleteIconVisibility(mLinearLeftEyeLensDelete);
                mImageLeftEyeLens.setImageResource(R.drawable.lens_icon);
            }

            if (!mRightFundus.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                setGlideImageView(mRightFundus, mImageRightEyeFundus, mLinearRightEyeFundusDelete, R.drawable.right_fundus_icon);
            } else {
                setDeleteIconVisibility(mLinearRightEyeFundusDelete);
                mImageRightEyeFundus.setImageResource(R.drawable.right_fundus_icon);
            }

            if (!mLeftFundus.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                setGlideImageView(mLeftFundus, mImageLeftEyeFundus, mLinearLeftEyeFundusDelete, R.drawable.left_fundus_icon);
            } else {
                setDeleteIconVisibility(mLinearLeftEyeFundusDelete);
                mImageLeftEyeFundus.setImageResource(R.drawable.left_fundus_icon);
            }

            if (!mRightGonioscopy.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                setGlideImageView(mRightGonioscopy, mImageRightEyeGonioscopy, mLinearRightEyeGonioscopyDelete, R.drawable.gonioscopy_icon);
            } else {
                setDeleteIconVisibility(mLinearRightEyeGonioscopyDelete);
                mImageRightEyeGonioscopy.setImageResource(R.drawable.gonioscopy_icon);
            }

            if (!mLeftGonioscopy.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                setGlideImageView(mLeftGonioscopy, mImageLeftEyeGonioscopy, mLinearLeftEyeGonioscopyDelete, R.drawable.gonioscopy_icon);
            } else {
                setDeleteIconVisibility(mLinearLeftEyeGonioscopyDelete);
                mImageLeftEyeGonioscopy.setImageResource(R.drawable.gonioscopy_icon);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should set the image view using glide
     *
     * @param mType         - Type
     * @param mImageView    - Image View
     * @param mLinearDelete - Linear Layout Delete
     * @param drawable      - Drawable
     */
    private void setGlideImageView(String mType, ImageView mImageView, LinearLayout mLinearDelete, int drawable) {
        try {
            String mBaseURL = WebFields.API_BASE_URL;
            String mFolderName = WebFields.IMAGE_BASE_FOLDER;
            String mHospitalCode = GetJsonData.getHospitalData(mActivity, WebFields.PASS_CODE.RESPONSE_HOSPITAL_CODE);
            mDoodleUrl = mBaseURL + mFolderName + mHospitalCode + "/" + mAppointmentId + "/DoodleImage/" + mType;
            Common.insertLog("Doodle Url::> " + mDoodleUrl);
            Glide.with(mActivity)
                    .load(mDoodleUrl)
                    .apply(new RequestOptions().error(drawable).
                            placeholder(drawable))
                    .into(mImageView);
            mLinearDelete.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the delete icon visibility
     *
     * @param mLinearDelete - Linear Layout Delete
     */
    private void setDeleteIconVisibility(LinearLayout mLinearDelete) {
        try {
            mLinearDelete.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This should get the image url for previewing an image to the new activity
     *
     * @return - String Url for preview image
     */
    private String getPreviewURL() {
        String mUrl = "";

        String mBaseURL = WebFields.API_BASE_URL;
        String mFolderName = WebFields.IMAGE_BASE_FOLDER;
        String mHospitalCode = GetJsonData.getHospitalData(mActivity, WebFields.PASS_CODE.RESPONSE_HOSPITAL_CODE);
        String mPreviewBaseUrl = mBaseURL + mFolderName + mHospitalCode + "/" + mAppointmentId + "/DoodleImage/";

        if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_right_eye_adnex))) {
            mUrl = mPreviewBaseUrl;
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_left_eye_adnex))) {
            mUrl = mPreviewBaseUrl;
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_right_eye_anterior_segment))) {
            mUrl = mPreviewBaseUrl;
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_left_eye_anterior_segment))) {
            mUrl = mPreviewBaseUrl;
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_right_eye_lens))) {
            mUrl = mPreviewBaseUrl;
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_left_eye_lens))) {
            mUrl = mPreviewBaseUrl;
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_right_eye_fundus))) {
            mUrl = mPreviewBaseUrl;
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_left_eye_fundus))) {
            mUrl = mPreviewBaseUrl;
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_right_eye_gonioscopy))) {
            mUrl = mPreviewBaseUrl;
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_left_eye_gonioscopy))) {
            mUrl = mPreviewBaseUrl;
        }
        return mUrl;
    }

    /**
     * This should get the image url for previewing an image to the new activity
     *
     * @return - String Url for preview image
     */
    private String getDoodleType() {
        String mType = "";

        if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_right_eye_adnex))) {
            mType = mRightAdnex;
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_left_eye_adnex))) {
            mType = mLeftAdnex;
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_right_eye_anterior_segment))) {
            mType = mRightAnteriorSegment;
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_left_eye_anterior_segment))) {
            mType = mLeftAnteriorSegment;
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_right_eye_lens))) {
            mType = mRightLens;
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_left_eye_lens))) {
            mType = mLeftLens;
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_right_eye_fundus))) {
            mType = mRightFundus;
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_left_eye_fundus))) {
            mType = mLeftFundus;
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_right_eye_gonioscopy))) {
            mType = mRightGonioscopy;
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_left_eye_gonioscopy))) {
            mType = mLeftGonioscopy;
        }

        return mType;
    }

    /**
     * This should call the API for delete the selected doodle
     */
    private void callToDeleteDoodleAPI() {
        try {
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);
            String mType = getDoodleType();

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setDeleteDoodleImageJson(mAppointmentId, mType, mDoodleType, mDatabaseName, mUserId));

            Call<AddDoodleImageModel> call = RetrofitClient.createService(ApiInterface.class).deleteDoodleImage(body);
            call.enqueue(new Callback<AddDoodleImageModel>() {
                @Override
                public void onResponse(@NonNull Call<AddDoodleImageModel> call, @NonNull Response<AddDoodleImageModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        int mError = response.body().getError();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                callToGetDoodleImagesAPI();
                                Common.setCustomToast(mActivity, mMessage);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddDoodleImageModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * get History Investigation
     */
    private void callToHistoryDoodleAPI() {
        try {

            String defualt_date = "1000-01-01";
            String Mobile = AppConstants.STR_EMPTY_STRING;
            String Type = "Doodle";

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setHistoryPreliminaryExaminationJson(defualt_date, mCurrentPage, mPatientId, Mobile, mHistoryDoctorID, Type, hospital_database));

            Call<HistoryPreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationHistory(body);
            call.enqueue(new Callback<HistoryPreliminaryExaminationModel>() {
                @Override
                public void onResponse(Call<HistoryPreliminaryExaminationModel> call, Response<HistoryPreliminaryExaminationModel> response) {
                    Common.insertLog("Response History::::> " + new Gson().toJson(response.body()));
                    try {
                        if (response.body() != null &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body().getData().size() > 0) {

                                mArrHistory.addAll(response.body().getData());
//                                checkTodayDoodle();
                            }
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<HistoryPreliminaryExaminationModel> call, Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void checkTodayDoodle() {

        try {
            String current_date = Common.setCurrentDate(mActivity);
            String convert_current_date = Common.setConvertDateFormat(mActivity,
                    current_date, mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen));

            if (mArrHistory.size() > 0) {
                for (int a = 0; a < mArrHistory.size(); a++) {
                    if (mArrHistory.get(a).getTreatmentDate().equalsIgnoreCase(convert_current_date)) {
                        for (int j = 0; j < mArrHistory.get(a).getDoodle().size(); j++) {
                            // setEditInvestigation(mArrHistory.get(a).getInvestigation().get(j));
                        }
                    }
                }
            }

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     *OpenDialog for closing patient file
     */
    public void openFileClosingAlert() {


        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(
                mActivity);
        builder.setTitle("Close File");
        builder.setMessage("Are you sure you want to close file?");
        builder.setNegativeButton("NO",
                (dialog, which) -> dialog.dismiss());
        builder.setPositiveButton("YES",
                (dialog, which) -> closeFile());
        builder.show();

    }

    private void closeFile() {
        try {
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.closeFileJson(mAppointmentId, mDatabaseName));

            Call<CloseFileModel> call = RetrofitClient.createService(ApiInterface.class).closeFileAPI(body);
            call.enqueue(new Callback<CloseFileModel>() {
                @Override
                public void onResponse(@NonNull Call<CloseFileModel> call, @NonNull Response<CloseFileModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        int mError = response.body().getError();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                Common.insertLog("IF");
                                Common.setCustomToast(mActivity,mMessage);
                            } else {
                                Common.insertLog("ELSE");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CloseFileModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * get History Open Dialog
     */
    public void openDialogDoodle() {
        try {
            Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.custom_dialog_history_preliminary);

            TextView mTextTitle = dialog.findViewById(R.id.text_view_custom_dialog_history_preliminary);
            RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
            ImageView mImageClose = dialog.findViewById(R.id.image_custom_dialog_history_preliminary_close);

            RelativeLayout noInternetConnection = dialog.findViewById(R.id.relative_no_internet);
            RelativeLayout noDataFound = dialog.findViewById(R.id.relative_no_data_available);

            mTextTitle.setText(mActivity.getResources().getString(R.string.tab_doodle));

            mImageClose.setColorFilter(Common.setThemeColor(mActivity));

            mImageClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            LinearLayoutManager manager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL,
                    false);
            recyclerView.setLayoutManager(manager);

            HistoryDoodleAdapter adapter = new HistoryDoodleAdapter(mActivity, recyclerView, mArrHistory) {
                @Override
                protected void onSelectedItem(Activity activity, HistoryPreliminaryExaminationModel.Data data) {
                    super.onSelectedItem(activity, data);
                    dialog.dismiss();
                    noDataFound.setVisibility(View.GONE);
                    if (data.getDoodle().size() > 0) {

                        for (int a = 0; a < data.getDoodle().size(); a++) {
                            setEditDoodle(data.getDoodle().get(a));
                        }
                    }
                }
            };

            recyclerView.setAdapter(adapter);

            if (mArrHistory.size() > 0) {
                recyclerView.setVisibility(View.VISIBLE);
                noDataFound.setVisibility(View.GONE);
            } else {
                recyclerView.setVisibility(View.GONE);
                noDataFound.setVisibility(View.VISIBLE);
            }

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Set Doodle Image
     *
     * @param doodle - Doodle
     */
    private void setEditDoodle(HistoryPreliminaryExaminationModel.Doodle doodle) {

        mAppointmentId = Integer.parseInt(doodle.getAppointmentID());

        mRightAdnex = doodle.getRightAdnex();
        mLeftAdnex = doodle.getLeftAdnex();
        mRightAnteriorSegment = doodle.getRightAnteriorSegment();
        mLeftAnteriorSegment = doodle.getLeftAnteriorSegment();
        mRightLens = doodle.getRightLens();
        mLeftLens = doodle.getLeftLens();
        mRightFundus = doodle.getRightFundus();
        mLeftFundus = doodle.getLeftFundus();
        mRightGonioscopy = doodle.getRightGonioscopy();
        mLeftGonioscopy = doodle.getLeftGonioscopy();

        if (!mRightAdnex.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            setGlideImageView(mRightAdnex, mImageRightEyeAdnex, mLinearRightEyeAdnexDelete, R.drawable.adnex_icon);
        } else {
            setDeleteIconVisibility(mLinearRightEyeAdnexDelete);
            mImageRightEyeAdnex.setImageResource(R.drawable.adnex_icon);
        }
            /*} else {
                mImageRightEyeAdnex.setImageResource(R.drawable.adnex_icon);
            }*/

        if (!mLeftAdnex.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            setGlideImageView(mLeftAdnex, mImageLeftEyeAdnex, mLinearLeftEyeAdnexDelete, R.drawable.adnex_icon);
        } else {
            setDeleteIconVisibility(mLinearLeftEyeAdnexDelete);
            mImageLeftEyeAdnex.setImageResource(R.drawable.adnex_icon);
        }

        if (!mRightAnteriorSegment.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            setGlideImageView(mRightAnteriorSegment, mImageRightEyeAnteriorSegment, mLinearRightEyeAnteriorSegmentDelete, R.drawable.anterior_segment_icon);
        } else {
            setDeleteIconVisibility(mLinearRightEyeAnteriorSegmentDelete);
            mImageRightEyeAnteriorSegment.setImageResource(R.drawable.anterior_segment_icon);
        }

        if (!mLeftAnteriorSegment.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            setGlideImageView(mLeftAnteriorSegment, mImageLeftEyeAnteriorSegment, mLinearLeftEyeAnteriorSegmentDelete, R.drawable.anterior_segment_icon);
        } else {
            setDeleteIconVisibility(mLinearLeftEyeAnteriorSegmentDelete);
            mImageLeftEyeAnteriorSegment.setImageResource(R.drawable.anterior_segment_icon);
        }

        if (!mRightLens.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            setGlideImageView(mRightLens, mImageRightEyeLens, mLinearRightEyeLensDelete, R.drawable.lens_icon);
        } else {
            setDeleteIconVisibility(mLinearRightEyeLensDelete);
            mImageRightEyeLens.setImageResource(R.drawable.lens_icon);
        }

        if (!mLeftLens.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            setGlideImageView(mLeftLens, mImageLeftEyeLens, mLinearLeftEyeLensDelete, R.drawable.lens_icon);
        } else {
            setDeleteIconVisibility(mLinearLeftEyeLensDelete);
            mImageLeftEyeLens.setImageResource(R.drawable.lens_icon);
        }

        if (!mRightFundus.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            setGlideImageView(mRightFundus, mImageRightEyeFundus, mLinearRightEyeFundusDelete, R.drawable.right_fundus_icon);
        } else {
            setDeleteIconVisibility(mLinearRightEyeFundusDelete);
            mImageRightEyeFundus.setImageResource(R.drawable.right_fundus_icon);
        }

        if (!mLeftFundus.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            setGlideImageView(mLeftFundus, mImageLeftEyeFundus, mLinearLeftEyeFundusDelete, R.drawable.left_fundus_icon);
        } else {
            setDeleteIconVisibility(mLinearLeftEyeFundusDelete);
            mImageLeftEyeFundus.setImageResource(R.drawable.left_fundus_icon);
        }

        if (!mRightGonioscopy.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            setGlideImageView(mRightGonioscopy, mImageRightEyeGonioscopy, mLinearRightEyeGonioscopyDelete, R.drawable.gonioscopy_icon);
        } else {
            setDeleteIconVisibility(mLinearRightEyeGonioscopyDelete);
            mImageRightEyeGonioscopy.setImageResource(R.drawable.gonioscopy_icon);
        }

        if (!mLeftGonioscopy.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            setGlideImageView(mLeftGonioscopy, mImageLeftEyeGonioscopy, mLinearLeftEyeGonioscopyDelete, R.drawable.gonioscopy_icon);
        } else {
            setDeleteIconVisibility(mLinearLeftEyeGonioscopyDelete);
            mImageLeftEyeGonioscopy.setImageResource(R.drawable.gonioscopy_icon);
        }
    }
}
