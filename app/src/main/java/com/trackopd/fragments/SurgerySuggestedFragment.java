package com.trackopd.fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.trackopd.R;
import com.trackopd.adapter.CouselingPackageOptionSugestionAdapter;
import com.trackopd.adapter.HistoryTreatmentSuggestedAdapter;
import com.trackopd.model.AddMedicationModel;
import com.trackopd.model.CloseFileModel;
import com.trackopd.model.CouselingPackageSuggestionModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.SessionManager;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class SurgerySuggestedFragment extends Fragment implements HistoryTreatmentSuggestedAdapter.onHistoryDiagnosisListener, CouselingPackageOptionSugestionAdapter.customSpinnerListener {

    View mView;
    Activity mActivity;
    private RelativeLayout mRelativeNoDataFound;
    private Spinner mSpinnerCategory, mSpinnerSurgeryName, mSpinnerSurgeryTwo, mSpinnerSurgeryPadEntry, mSpinnerSurgeryWithinTime, mSpinnerSurgeryEye;
    private EditText mEditSurgeryManyDays, mEditTextSurgeryRemarks;
    private Button mButtonSubmit;

    private ArrayList<PreliminaryExaminationDetailsModel.Data.Category> mArrCategory;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.Eye> mArrEye;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.SurgeryType> mArrSurgeryType;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.OptionSuggested> mArrOptionSuggested;
    private ArrayList<HistoryPreliminaryExaminationModel.Data> mArrHistory;

    private ArrayList<CouselingPackageSuggestionModel> mArrPackageSuggested;
    private CouselingPackageOptionSugestionAdapter mAdapterPackageSuggesteed;
    private LinearLayout mLinearSugestation;

    private ArrayList<String> mArrDuration, mArrSurgeryCategory, mArrEyeName,
            mArrSurgery, mArrOption, mArrSurgeryTime, mArrUOM, mArrDosage, mArrMedication;

    private int mSelectedDosageIndex = 0, mSelectedCityIndex = 0, mType, mMedicationId, mDosageId,
            mSelectedUOMIndex, mUOMId, mSelectedMedicationIndex, mAppointmentId, mCurrentPage = 1,
            mHistoryDoctorID = -1, mSelectedCategory = 0, mSelectedEye = 0, mSelectedWithin = 0;

    private String mUserType, mSpecialInstructions, mMedicationType, mSelMedicationType, EndDate,
            mPatientId, mDoctorId, mTreatmentDate, Times;

    public SurgerySuggestedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_surgery_suggested, container, false);
        mActivity = getActivity();

        mArrHistory = new ArrayList<>();
        mArrCategory = new ArrayList<>();
        mArrEye = new ArrayList<>();
        mArrOptionSuggested = new ArrayList<>();
        mArrSurgeryType = new ArrayList<>();

        mArrPackageSuggested = new ArrayList<>();
        mArrSurgery = new ArrayList<>();
        mArrSurgeryCategory = new ArrayList<>();
        mArrEyeName = new ArrayList<>();
        mArrOption = new ArrayList<>();
        mArrDuration = new ArrayList<>();

        String[] strArrDuration = mActivity.getResources().getStringArray(R.array.array_duration);
        mArrDuration = new ArrayList<>(Arrays.asList(strArrDuration)); //new ArrayList is only needed if you absolutely need an ArrayList

        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        getBundle();
        getID();
        getData();
        setData();
        setRegister();

        callToPreliminaryExaminationAPI();

        mEditTextSurgeryRemarks.setFilters(Common.getFilter());
        return mView;
    }

    /**
     * Set Data Value
     */
    private void setData() {

        ArrayAdapter adapter_surgery_within = new ArrayAdapter<String>(mActivity,
                android.R.layout.simple_spinner_dropdown_item, mArrDuration) {
            @Override
            public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                // Cast the drop down items (popup items) as text view
                TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                // Set the text color of drop down items
                tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                // If this item is selected item
                if (position == mSelectedWithin) {
                    // Set spinner selected popup item's text color
                    tv.setTextColor(Common.setThemeColor(mActivity));
                }
                // Return the modified view
                return tv;
            }
        };
        adapter_surgery_within.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinnerSurgeryWithinTime.setAdapter(adapter_surgery_within);
        mSpinnerSurgeryWithinTime.setSelection(0, true);

    }

    /**
     * OpenDialog for closing patient file
     */
    public void openFileClosingAlert() {


        AlertDialog.Builder builder = new AlertDialog.Builder(
                mActivity);
        builder.setTitle("Close File");
        builder.setMessage("Are you sure you want to close file?");
        builder.setNegativeButton("NO",
                (dialog, which) -> dialog.dismiss());
        builder.setPositiveButton("YES",
                (dialog, which) -> closeFile());
        builder.show();

    }

    private void closeFile() {
        try {
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.closeFileJson(mAppointmentId, mDatabaseName));

            Call<CloseFileModel> call = RetrofitClient.createService(ApiInterface.class).closeFileAPI(body);
            call.enqueue(new Callback<CloseFileModel>() {
                @Override
                public void onResponse(@NonNull Call<CloseFileModel> call, @NonNull Response<CloseFileModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        int mError = response.body().getError();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            if (response.body() != null) {
                                Common.insertLog("IF");
                                Common.setCustomToast(mActivity,mMessage);
                            } else {
                                Common.insertLog("ELSE");
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CloseFileModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mAppointmentId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mDoctorId = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);
                mTreatmentDate = bundle.getString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getID() {
        mSpinnerCategory = mView.findViewById(R.id.spinner_treatment_suggested_surgery_suggested_category);
        mSpinnerSurgeryName = mView.findViewById(R.id.spinner_treatment_suggested_surgery_suggested_surgery);
        mSpinnerSurgeryTwo = mView.findViewById(R.id.spinner_treatment_suggested_surgery_suggested_surgery_two);
        mSpinnerSurgeryPadEntry = mView.findViewById(R.id.spinner_treatment_suggested_surgery_suggested_pad_entry);
        mSpinnerSurgeryWithinTime = mView.findViewById(R.id.spinner_treatment_suggested_surgery_suggested_within_time);
        mSpinnerSurgeryEye = mView.findViewById(R.id.spinner_treatment_suggested_surgery_eye);

        mEditSurgeryManyDays = mView.findViewById(R.id.edit_treatment_suggested_many_times);
        mEditTextSurgeryRemarks = mView.findViewById(R.id.spinner_treatment_suggested_surgery_suggested_remarks);

        mRelativeNoDataFound = mView.findViewById(R.id.relative_treatment_suggested_no_data_found);

        mButtonSubmit = mView.findViewById(R.id.button_treatment_suggested_submit);

        //Linear Layout
        mLinearSugestation = mView.findViewById(R.id.linear_check_up_counseling_layout);
    }

    private void getData() {
    }

    private void setRegister() {

        SessionManager manager = new SessionManager(mActivity);
        String closeFile = manager.getPreferences("ClosingFlags", "");

        if (closeFile.equalsIgnoreCase("Yes")) {
            mButtonSubmit.setClickable(false);
            Toast.makeText(mActivity, "Patient File Closed", Toast.LENGTH_SHORT).show();
        } else {
            mButtonSubmit.setOnClickListener(v -> {
                if (checkVadlidationSurgery()) {
                    callToAddSurgery();
                }
            });
        }


    }

    /**
     * This method should call the Preliminary Examination API
     */
    private void callToPreliminaryExaminationAPI() {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPreliminaryExaminationDetailsJson(hospital_database));

            Call<PreliminaryExaminationDetailsModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationDetails(body);
            call.enqueue(new Callback<PreliminaryExaminationDetailsModel>() {
                @Override
                public void onResponse(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Response<PreliminaryExaminationDetailsModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null &&
                                    response.body().getError() == AppConstants.API_SUCCESS_ERROR) {

                                PreliminaryExaminationDetailsModel.Data data =
                                        response.body().getData();

                                mArrCategory.addAll(data.getCategory());
                                setCategoryArrayList();

                                mArrSurgeryType.addAll(data.getSurgeryType());
                                setSurgeryArrayList();

                                mArrEye.addAll(data.getEye());
                                setEyeArrayList();

                                // Option List
                                List<PreliminaryExaminationDetailsModel.Data.OptionSuggested>
                                        mListOption = response.body().getData().getOptionSuggested();
                                mArrOptionSuggested.addAll(mListOption);
                                setOptionSuggestedArrayList();

                                callToHistoryPreliminaryExamination();
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PreliminaryExaminationDetailsModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Category
     */
    private void setCategoryArrayList() {
        try {
            if (mArrCategory != null && mArrCategory.size() > 0) {
                mArrSurgeryCategory.add(mActivity.getString(R.string.spinner_select_category));
                for (int i = 0; i < mArrCategory.size(); i++) {
                    mArrSurgeryCategory.add(mArrCategory.get(i).getCategory());
                }
            }

            // ToDo: Sets the spinner color as per the theme applied
            mSpinnerCategory.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);

            ArrayAdapter adapter = new ArrayAdapter<String>(mActivity,
                    android.R.layout.simple_spinner_dropdown_item, mArrSurgeryCategory) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCategory) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerCategory.setAdapter(adapter);
            mSpinnerCategory.setSelection(0, true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Category
     */
    private void setSurgeryArrayList() {
        try {
            if (mArrSurgeryType != null && mArrSurgeryType.size() > 0) {
                mArrSurgery.add(mActivity.getString(R.string.spinner_select_surgery));
                for (int i = 0; i < mArrSurgeryType.size(); i++) {
                    mArrSurgery.add(mArrSurgeryType.get(i).getSurgeryName());
                }
            }


            mSpinnerSurgeryName.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);
            mSpinnerSurgeryTwo.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);


            ArrayAdapter adapter_surgery_two = new ArrayAdapter<String>(mActivity,
                    android.R.layout.simple_spinner_dropdown_item, mArrSurgery) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCategory) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter_surgery_two.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerSurgeryTwo.setAdapter(adapter_surgery_two);
            mSpinnerSurgeryTwo.setSelection(0, true);


            ArrayAdapter adapter_surgery_name = new ArrayAdapter<String>(mActivity,
                    android.R.layout.simple_spinner_dropdown_item, mArrSurgery) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedCategory) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter_surgery_name.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerSurgeryName.setAdapter(adapter_surgery_name);
            mSpinnerSurgeryName.setSelection(0, true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Array List Eye
     */
    private void setEyeArrayList() {
        try {
            if (mArrEye != null && mArrEye.size() > 0) {
                mArrEyeName.add(mActivity.getString(R.string.spinner_select_eye));
                for (int i = 0; i < mArrEye.size(); i++) {
                    mArrEyeName.add(mArrEye.get(i).getEye());
                }
            }

            mSpinnerSurgeryEye.getBackground().setColorFilter(Common.setThemeColor(mActivity), PorterDuff.Mode.SRC_ATOP);


            ArrayAdapter adapter_surgery_eye = new ArrayAdapter<String>(mActivity,
                    android.R.layout.simple_spinner_dropdown_item, mArrEyeName) {
                @Override
                public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
                    // Cast the drop down items (popup items) as text view
                    TextView tv = (TextView) super.getDropDownView(position, convertView, parent);
                    // Set the text color of drop down items
                    tv.setTextColor(mActivity.getResources().getColor(R.color.colorBlack));
                    // If this item is selected item
                    if (position == mSelectedEye) {
                        // Set spinner selected popup item's text color
                        tv.setTextColor(Common.setThemeColor(mActivity));
                    }
                    // Return the modified view
                    return tv;
                }
            };
            adapter_surgery_eye.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mSpinnerSurgeryEye.setAdapter(adapter_surgery_eye);
            mSpinnerSurgeryEye.setSelection(0, true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Array List Eye
     */
    private void setOptionSuggestedArrayList() {

        try {

            if (mArrPackageSuggested.size() > 0) {
                mArrPackageSuggested.clear();
            }

            CouselingPackageSuggestionModel options = new CouselingPackageSuggestionModel();
            options.setStatus("0");
            options.setOpetionSuggested(mActivity.getResources().getString(R.string.spinner_select_package_suggestion));
            options.setOpetionSuggestedID("0");
            options.setChecked(false);
            mArrPackageSuggested.add(options);


            if (mArrOptionSuggested.size() > 0) {
                for (int a = 0; a < mArrOptionSuggested.size(); a++) {
                    CouselingPackageSuggestionModel option = new CouselingPackageSuggestionModel();
                    option.setStatus(mArrOptionSuggested.get(a).getStatus());
                    option.setOpetionSuggested(mArrOptionSuggested.get(a).getOpetionSuggested());
                    option.setOpetionSuggestedID(mArrOptionSuggested.get(a).getOpetionSuggestedID());
                    option.setChecked(false);
                    mArrPackageSuggested.add(option);
                }
            }

            mAdapterPackageSuggesteed = new CouselingPackageOptionSugestionAdapter
                    (mActivity, mArrPackageSuggested, this);

            mSpinnerSurgeryPadEntry.setAdapter(mAdapterPackageSuggesteed);

        } catch (Exception e) {
            e.printStackTrace();
        }

        /*try {
            if (mArrOptionSuggested != null && mArrOptionSuggested.size() > 0) {

                mArrOption.add(mActivity.getString(R.string.spinner_select_option));
                for (int i = 0; i < mArrOptionSuggested.size(); i++) {
                    mArrOption.add(mArrOptionSuggested.get(i).getOpetionSuggested());
                }
            }

            ArrayAdapter<String> adapter_option = new ArrayAdapter<>
                    (mActivity, android.R.layout.simple_selectable_list_item, mArrOption);
            mSpinnerSurgeryPadEntry.setAdapter(adapter_option);

        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    /**
     * Spinner Item Updates
     */
    @Override
    public void onItemUpdate() {

        if (mLinearSugestation.getChildCount() > 0) {
            mLinearSugestation.removeAllViews();
        }

        if (mArrPackageSuggested.size() > 0) {

            for (int a = 0; a < mArrPackageSuggested.size(); a++) {
                if (mArrPackageSuggested.get(a).getChecked() == true) {
                    addItems(mArrPackageSuggested.get(a));
                }
            }
        }
    }

    /**
     * Add Item Linear
     *
     * @param couselingPackageSuggestionModel -Package Suggested
     */
    private void addItems(CouselingPackageSuggestionModel couselingPackageSuggestionModel) {
        @SuppressLint("InflateParams")
        View view_container = LayoutInflater.from(mActivity).inflate(R.layout.
                        row_add_preliminary_examination_compliance_item,
                null, false);

        TextView mTextName = view_container.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
        ImageView mImageDelete = view_container.findViewById(R.id.image_row_add_preliminary_examination_compliance_delete);
        mImageDelete.setColorFilter(Common.setThemeColor(mActivity));

        mTextName.setText(couselingPackageSuggestionModel.getOpetionSuggested());

        mImageDelete.setOnClickListener(v -> {
            mLinearSugestation.removeView(view_container);
            couselingPackageSuggestionModel.setChecked(false);
        });

        mLinearSugestation.addView(view_container);
    }

    /**
     * Set Call API History Preliminary Examination
     */
    private void callToHistoryPreliminaryExamination() {
        try {

            String defualt_date = "1000-01-01";
            String Mobile = AppConstants.STR_EMPTY_STRING;
            String Type = "TreatmentSuggested";
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setHistoryPreliminaryExaminationJson(defualt_date, mCurrentPage, mPatientId, Mobile, mHistoryDoctorID, Type, hospital_database));

            Call<HistoryPreliminaryExaminationModel> call = RetrofitClient.createService(ApiInterface.class).getPreliminaryExaminationHistory(body);
            call.enqueue(new Callback<HistoryPreliminaryExaminationModel>() {
                @Override
                public void onResponse(Call<HistoryPreliminaryExaminationModel> call, Response<HistoryPreliminaryExaminationModel> response) {
                    Common.insertLog("Response History::::> " + new Gson().toJson(response.body()));
                    try {
                        if (response.body() != null &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body().getData().size() > 0) {

                                mArrHistory.addAll(response.body().getData());
                            }
                        }
                    } catch (Exception e) {
                        Common.insertLog(e.getMessage());
                    }
                }

                @Override
                public void onFailure(Call<HistoryPreliminaryExaminationModel> call, Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Open Dialog Treatment Suggested
     */
    public void openDialogTreatmentSuggested() {
        try {
            Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.custom_dialog_history_preliminary);

            TextView mTextTitle = dialog.findViewById(R.id.text_view_custom_dialog_history_preliminary);
            RecyclerView recyclerView = dialog.findViewById(R.id.recycler_view);
            ImageView mImageClose = dialog.findViewById(R.id.image_custom_dialog_history_preliminary_close);

            RelativeLayout noInternetConnection = dialog.findViewById(R.id.relative_no_internet);
            RelativeLayout noDataFound = dialog.findViewById(R.id.relative_no_data_available);

            mTextTitle.setText(mActivity.getResources().getString(R.string.tab_surgery_suggested));

            mImageClose.setColorFilter(Common.setThemeColor(mActivity));

            mImageClose.setOnClickListener(v -> dialog.dismiss());

            LinearLayoutManager manager = new LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false);
            recyclerView.setLayoutManager(manager);
            HistoryTreatmentSuggestedAdapter adapter = new
                    HistoryTreatmentSuggestedAdapter(mActivity, recyclerView, mArrHistory,
                            this) {
                        @Override
                        protected void onSelectedTreatmentSuggested(Activity mActivity, HistoryPreliminaryExaminationModel.Data checkInData) {
                            super.onSelectedTreatmentSuggested(mActivity, checkInData);
                            dialog.dismiss();
                            mRelativeNoDataFound.setVisibility(View.GONE);

                            editTreatmentSuggested(checkInData);
                        }
                    };

            recyclerView.setAdapter(adapter);

            if (mArrHistory.size() > 0) {
                noDataFound.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            } else {
                noDataFound.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
            }

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Check Validation Surgery Suggested
     *
     * @return
     */
    private boolean checkVadlidationSurgery() {
        boolean status = true;

        String SurgeryTypeID = "";

        int indexCategor = mSpinnerCategory.getSelectedItemPosition();
        int indexSurgery = mSpinnerSurgeryName.getSelectedItemPosition();
        int indexEye = mSpinnerSurgeryEye.getSelectedItemPosition();


        if (indexCategor == 0) {
            status = false;
        }

//        if (SurgeryTypeID.equalsIgnoreCase("") || SurgeryTypeID.equalsIgnoreCase("0")) {
//            status = false;
//        }

        if (indexEye == 0) {
            status = false;
        }

        if (mEditSurgeryManyDays.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            status = false;
        }

        return status;
    }

    /**
     * Check Surgery Validation
     */
    private void callToAddSurgery() {
        try {

            int indexCategor = mSpinnerCategory.getSelectedItemPosition();
            int indexSurgery = mSpinnerSurgeryName.getSelectedItemPosition();
            int indexSurgeryWithin = mSpinnerSurgeryWithinTime.getSelectedItemPosition();
            int indexSurgeryEye = mSpinnerSurgeryEye.getSelectedItemPosition();
            int indexSurgeryTwo = mSpinnerSurgeryTwo.getSelectedItemPosition();

            String CategoryID = mArrCategory.get(indexCategor - 1).getCategoryID();
            String SurgeryTypeID = mArrSurgeryType.get(indexSurgery - 1).getSurgeryTypeID();
            String EyeID = mArrEye.get(indexSurgeryEye - 1).getEyeID();
            String Duration = mArrDuration.get(indexSurgeryWithin);
            String Remarks = mEditTextSurgeryRemarks.getText().toString();
            String SurgeryTwo;
            if (indexSurgeryTwo == 0) {
                SurgeryTwo = "NA";
            } else {
                SurgeryTwo = mArrSurgeryType.get(indexSurgeryTwo - 1).getSurgeryName();
            }
            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            String prelimianaryID = AddNewPreliminaryExaminationFragment.getPrelimary();

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.addSurgerySuggested(mAppointmentId,
                            Integer.parseInt(CategoryID), SurgeryTypeID,
                            EyeID, Duration, Remarks, mUserId, hospital_database, prelimianaryID, SurgeryTwo));

            Call<AddMedicationModel> call = RetrofitClient.createService(ApiInterface.class).addMedication(requestBody);
            call.enqueue(new Callback<AddMedicationModel>() {
                @Override
                public void onResponse(@NonNull Call<AddMedicationModel> call, Response<AddMedicationModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    if (response.isSuccessful() && response.body().getError() == 200) {
                        Common.setCustomToast(mActivity, response.body().getMessage());

                        if (mArrHistory.size() > 0) {
                            mArrHistory.clear();
                        }
                        callToHistoryPreliminaryExamination();
                        callToNextPage();
                    } else {
                        Common.setCustomToast(mActivity, response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<AddMedicationModel> call, Throwable t) {
                    Common.insertLog(t.getMessage());
                    Common.setCustomToast(mActivity, t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set Call To Next Screen
     */
    private void callToNextPage() {
        AddNewPreliminaryExaminationFragment.changePage(8);
    }

    @Override
    public void onSelectHistioyDiagnosis(Activity mActivity, HistoryPreliminaryExaminationModel.Data checkInData) {
        mRelativeNoDataFound.setVisibility(View.GONE);
        editTreatmentSuggested(checkInData);
    }

    public void editTreatmentSuggested(HistoryPreliminaryExaminationModel.Data checkInData) {
        if (checkInData.getSurgerySuggested().size() > 0) {

            String category = checkInData.getSurgerySuggested().get(0).getCategoryID();
            if (!category.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                if (mArrCategory.size() > 0) {
                    for (int a = 0; a < mArrCategory.size(); a++) {
                        if (mArrCategory.get(a).getCategoryID()
                                .equalsIgnoreCase(category)) {
                            int index = a + 1;
                            mSpinnerCategory.setSelection(index);
                        }
                    }
                }
            }

            String surgeryID = checkInData.getSurgerySuggested().get(0).getSurgeryTypeID();

            if (!surgeryID.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                if (mArrSurgeryType.size() > 0) {
                    for (int a = 0; a < mArrSurgeryType.size(); a++) {
                        if (mArrSurgeryType.get(a).getSurgeryTypeID()
                                .equalsIgnoreCase(surgeryID)) {
                            int index = a + 1;
                            mSpinnerSurgeryName.setSelection(index, true);
                        }
                    }
                }
            }

            String duration = checkInData.getSurgerySuggested().get(0).getDuration();

            if (!duration.equals(AppConstants.STR_EMPTY_STRING)) {
                if (mArrDuration.size() > 0) {
                    for (int a = 0; a < mArrDuration.size(); a++) {
                        if (mArrDuration.get(a).equalsIgnoreCase(duration)) {
                            int index = a;
                            mSpinnerSurgeryWithinTime.setSelection(index, true);
                        }
                    }
                }

            }

            String eyeid = checkInData.getSurgerySuggested().get(0).getEye();
            if (!eyeid.equals(AppConstants.STR_EMPTY_STRING)) {
                if (mArrEyeName.size() > 0) {
                    for (int a = 0; a < mArrEyeName.size(); a++) {
                        if (mArrEyeName.get(a).
                                equalsIgnoreCase(eyeid)) {
                            //int index=a+1;
                            mSpinnerSurgeryEye.setSelection(a);
                        }
                    }
                }

            }

            //Surgery Two Selected
            String surgery2 = checkInData.getSurgerySuggested().get(0).getSurgeryType2();
            if (!eyeid.equals(AppConstants.STR_EMPTY_STRING)) {
                if (mArrSurgery.size() > 0) {
                    for (int a = 0; a < mArrSurgery.size(); a++) {
                        if (mArrSurgery.get(a).
                                equalsIgnoreCase(surgery2)) {
                            //int index=a+1;
                            mSpinnerSurgeryTwo.setSelection(a);
                        }
                    }
                }

            }

            mEditTextSurgeryRemarks.setText(checkInData.getSurgerySuggested().get(0).getRemarks());

            /*}catch (Exception e){
                Common.insertLog(e.getMessage());
            }*/
        }
    }
}
