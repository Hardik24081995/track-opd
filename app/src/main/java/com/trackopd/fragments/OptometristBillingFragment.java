package com.trackopd.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.badoualy.stepperindicator.StepperIndicator;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.ViewPagerAdapter;
import com.trackopd.model.BillingListItemModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

import java.util.ArrayList;

public class OptometristBillingFragment extends Fragment {

    private View mView;
    private Activity mActivity;
    private ViewPager mViewPager;
    private StepperIndicator stepperIndicator;
    public static Button mButtonNext;
    public static ArrayList<BillingListItemModel> mArrayBillingItem;
    private String mPatientFirstName,mPatientLastName,mPatientMobile,mPatientId,mAppointmentId;
    private Bundle bundle;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_optometrist_billing, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();
        mArrayBillingItem=new ArrayList<>();

        getBundle();
        getIds();
        setBundle();
        setViewPager();
        setRegister();
        setHasOptionsMenu(true);
        return mView;
    }

    private void getBundle() {
        try{
            Bundle bundle=this.getArguments();
            if (bundle!=null){
                mPatientFirstName=bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mPatientLastName=bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mPatientMobile=bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mPatientId=bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mPatientFirstName=bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
            }
         }
        catch (Exception e){
           Common.insertLog(e.getMessage());
        }
    }


    /**
     *  get Declare Id on
     */
    private void getIds() {
        try{
            stepperIndicator=mView.findViewById(R.id.step_indicator_optometrist_billing);
            mViewPager=mView.findViewById(R.id.view_pager_add_billing_fragment);
            mButtonNext=mView.findViewById(R.id.button_optometrist_billing_action_next);

            mButtonNext.setText(mActivity.getResources().getString(R.string.action_next));
        }catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }

    private void setBundle() {
        bundle=new Bundle();
        bundle.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, mPatientFirstName);
        bundle.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, mPatientLastName);
        bundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mPatientMobile);
        bundle.putString(AppConstants.BUNDLE_PATIENT_ID, mPatientId);
        bundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, String.valueOf(mAppointmentId));

    }

    /**
     * set View Pager
     */
    private void setViewPager() {
        try{

            ViewPagerAdapter adapter=null;
            String mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                adapter = new ViewPagerAdapter(((ReceptionistHomeActivity) mActivity).getSupportFragmentManager());
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                adapter = new ViewPagerAdapter(((OptometristHomeActivity) mActivity).getSupportFragmentManager());
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                adapter = new ViewPagerAdapter(((DoctorHomeActivity) mActivity).getSupportFragmentManager());
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                adapter = new ViewPagerAdapter(((CouncillorHomeActivity) mActivity).getSupportFragmentManager());
            }

            adapter.addFragment(new AddServiceAndPackage(), AppConstants.STR_EMPTY_STRING,bundle);
            adapter.addFragment(new BillingSummaryFragment(), AppConstants.STR_EMPTY_STRING,bundle);

            mViewPager.setAdapter(adapter);

            //set Step Indicator
            stepperIndicator.setViewPager(mViewPager);

        } catch (Exception e){
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        MenuItem menuGrid = menu.findItem(R.id.action_grid);
        menuGrid.setVisible(false);
    }

    public static void setVisibilityNext(Boolean isVisible){
        if (isVisible){
            mButtonNext.setVisibility(View.VISIBLE);
        }else {
            mButtonNext.setVisibility(View.GONE);
        }
    }

    /**
     * Set Register Listener
     */
    private void setRegister() {
        mButtonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mViewPager.getCurrentItem()==0){
                    mViewPager.setCurrentItem(1);
                    mButtonNext.setText(mActivity.getResources().getString(R.string.action_pay_now));
                }else {
                    //mViewPager.setCurrentItem(0);
                    BillingSummaryFragment.mButtonSubmit.callOnClick();
                }
            }
        });

        stepperIndicator.addOnStepClickListener(new StepperIndicator.OnStepClickListener() {
            @Override
            public void onStepClicked(int step) {
                if(step==0){
                    mViewPager.setCurrentItem(0);
                }else {
                    if(mArrayBillingItem.size()>0){
                        mViewPager.setCurrentItem(1);
                    }
                }
            }
        });
    }
}