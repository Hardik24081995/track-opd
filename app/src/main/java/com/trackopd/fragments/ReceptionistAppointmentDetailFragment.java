package com.trackopd.fragments;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.adapter.ViewPagerAdapter;
import com.trackopd.model.ReceptionistAppointmentModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

import java.util.Objects;

public class ReceptionistAppointmentDetailFragment extends Fragment {

    private View mView;
    private Menu menu;
    private Activity mActivity;
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    public String mPatientFirstName="", mPatientLastName="", mPatientMobile="", mPatientCode="",
                  mPatientId="", mUserType="",mFrom,mAppointmentNo="",mAppointmentId="",mBookingType="",
                 mPaymentStatus="",mAppointmentDate="",mDoctorName="";
    public Bundle mBundle;
    public ReceptionistAppointmentModel receptionistAppointmentModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_receptionist_appointment_detail, container, false);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mActivity = getActivity();

        getBundle();
        getIds();
        setRegListeners();
        setBundle();
        setViewPager();
        setHasOptionsMenu(false);
        return mView;
    }

    /**
     * Sets up the actionbar menus
     */
    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        this.menu = menu;
        menu.clear();
        mActivity.getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);

        final MenuItem menuSearch = menu.findItem(R.id.action_search);
        menuSearch.setVisible(false);

        MenuItem menuAdd = menu.findItem(R.id.action_add);
        menuAdd.setVisible(false);
    }

    /**
     * Get the bundle keys from the previous fragment
     */
    private void getBundle() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {

                if (AppConstants.BUNDLE_IS_FROM_SEARCH!=null){
                    mFrom=bundle.getString(AppConstants.BUNDLE_IS_FROM_SEARCH);
                }

                mPatientFirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mPatientLastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mPatientMobile = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mPatientCode = bundle.getString(AppConstants.BUNDLE_PATIENT_CODE);
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mAppointmentId = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_ID);
                mAppointmentNo = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO);
                mBookingType = bundle.getString(AppConstants.BUNDLE_APPOINTMENT_TYPE);
                mPaymentStatus = bundle.getString(AppConstants.BUNDLE_PAYMENT_EDIT_MODE);
                mAppointmentDate = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DATE);
                mDoctorName = bundle.getString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR);
                mUserType = bundle.getString(AppConstants.BUNDLE_LOGIN_TYPE);



                /*mPatientFirstName = bundle.getString(AppConstants.BUNDLE_PATIENT_FIRST_NAME);
                mPatientLastName = bundle.getString(AppConstants.BUNDLE_PATIENT_LAST_NAME);
                mPatientMobile = bundle.getString(AppConstants.BUNDLE_PATIENT_MOBILE);
                mPatientCode = bundle.getString(AppConstants.BUNDLE_PATIENT_CODE);
                mPatientId = bundle.getString(AppConstants.BUNDLE_PATIENT_ID);
                mAppointmentId = bundle.getInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_ID);

               // receptionistAppointmentModel = (ReceptionistAppointmentModel) bundle.getSerializable(AppConstants.BUNDLE_PATIENT_ARRAY);

                mUserType = bundle.getString(AppConstants.BUNDLE_LOGIN_TYPE);*/
            }
            Common.insertLog("Bundle Detail Get::::> " + mAppointmentId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Tab Layout
            mTabLayout = mView.findViewById(R.id.tab_receptionist_appointment_detail);

            // View Pager
            mViewPager = mView.findViewById(R.id.view_pager_receptionist_appointment_detail);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mTabLayout.setOnTabSelectedListener(onTabSelectedListener(mViewPager));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set up the fragment pass data
     */
    private void setBundle() {
        try {
            mBundle = new Bundle();
            mBundle.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, mPatientFirstName);
            mBundle.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, mPatientLastName);
            mBundle.putString(AppConstants.BUNDLE_PATIENT_CODE, mPatientCode);
            mBundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mPatientMobile);
            mBundle.putString(AppConstants.BUNDLE_PATIENT_ID, mPatientId);
            mBundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_ID, mAppointmentId);
            mBundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, mAppointmentNo);
            mBundle.putString(AppConstants.BUNDLE_APPOINTMENT_TYPE, mBookingType);
            mBundle.putString(AppConstants.BUNDLE_PAYMENT_EDIT_MODE, mPaymentStatus);
            mBundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DATE, mAppointmentDate);
            mBundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, mDoctorName);

            Common.insertLog("Bundle Detail Set::::> " + mPatientFirstName);

            mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the view pager and binding the tab layouts
     */
    private void setViewPager() {
        try {
            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_patient))) {
                fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            }

            ViewPagerAdapter adapter = new ViewPagerAdapter(fragmentManager);
            adapter.addFragment(new ReceptionistAppointmentDetailMoreDataFragment(), mActivity.getResources().getString(R.string.text_details), mBundle);
            adapter.addFragment(new ReceptionistAppointmentDetailProcessFragment(), mActivity.getResources().getString(R.string.nav_menu_process), mBundle);
            adapter.addFragment(new ReceptionistPatientDetailPaymentFragment(), mActivity.getResources().getString(R.string.nav_menu_payment), mBundle);
            adapter.addFragment(new ReceptionistPatientDetailPrescriptionFragment(), mActivity.getResources().getString(R.string.nav_menu_prescription), mBundle);

            mViewPager.setAdapter(adapter);
            mTabLayout.setupWithViewPager(mViewPager);
            mViewPager.setOffscreenPageLimit(1);
            mViewPager.setCurrentItem(0);

            Objects.requireNonNull(mTabLayout.getTabAt(0)).setIcon(R.drawable.booking_icon);
            Objects.requireNonNull(mTabLayout.getTabAt(1)).setIcon(R.drawable.ic_process_icon);
            Objects.requireNonNull(mTabLayout.getTabAt(2)).setIcon(R.drawable.amount_icon);
            Objects.requireNonNull(mTabLayout.getTabAt(3)).setIcon(R.drawable.nav_prescription_icon);

            mTabLayout.setSelectedTabIndicatorColor(Common.setThemeColor(mActivity));
            mTabLayout.setTabTextColors(mActivity.getResources().getColor(R.color.colorBlack), Common.setThemeColor(mActivity));


            if (mFrom.equalsIgnoreCase("Payment")){
               mViewPager.setCurrentItem(2);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private TabLayout.OnTabSelectedListener onTabSelectedListener(final ViewPager viewPager) {

        return new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        };
    }
}