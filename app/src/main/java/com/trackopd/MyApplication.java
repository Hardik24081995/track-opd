package com.trackopd;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.trackopd.utils.SessionManager;
import com.trackopd.utils.SharedPrefs;

public class MyApplication extends MultiDexApplication {

    public final static int THEME_RED = 0;
    public final static int THEME_GREEN = 1;
    public final static int THEME_DARK_BLUE = 2;
    public final static int THEME_LIGHT_BLUE = 3;
    public static SessionManager mSharedPref;
    public static SharedPrefs sharedPrefs;
    private static MyApplication mInstance;
    private static Context context;

    public MyApplication() {
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

    /**
     * This method should change the theme and calls the activity accordingly.
     *
     * @param activity     - Activity
     * @param mIsLoginType - Is Login Type
     * @param theme        - Theme
     */
    public static void changeToTheme(Activity activity, String mIsLoginType, int theme) {
        try {
            sharedPrefs = new SharedPrefs(activity);
            mSharedPref = new SessionManager(activity);
            sharedPrefs.setPreferences(sharedPrefs.CurrentTheme, theme);
            activity.finishAffinity();

            if (mIsLoginType.equalsIgnoreCase("1")) {
                activity.startActivity(new Intent(activity, ReceptionistHomeActivity.class));
            } else if (mIsLoginType.equalsIgnoreCase("2")) {
                activity.startActivity(new Intent(activity, OptometristHomeActivity.class));
            } else if (mIsLoginType.equalsIgnoreCase("3")) {
                activity.startActivity(new Intent(activity, DoctorHomeActivity.class));
            } else if (mIsLoginType.equalsIgnoreCase("4")) {
                activity.startActivity(new Intent(activity, PatientHomeActivity.class));
            } else {
                activity.startActivity(new Intent(activity, CouncillorHomeActivity.class));
            }

        /*activity.startActivity(new Intent(activity, activity.getClass()));
        activity.overridePendingTransition(android.R.anim.fade_in,
                android.R.anim.fade_out);*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void onActivityCreateSetTheme(Activity activity) {
        mSharedPref = new SessionManager(activity);
        sharedPrefs = new SharedPrefs(activity);

        switch (sharedPrefs.getPreferences(sharedPrefs.CurrentTheme, THEME_LIGHT_BLUE)) {
            default:

            case THEME_RED:
                activity.setTheme(R.style.ThemeApp_Red);
                break;

            case THEME_GREEN:
                activity.setTheme(R.style.ThemeApp_Green);
                break;

            case THEME_DARK_BLUE:
                activity.setTheme(R.style.ThemeApp_Dark_Blue);
                break;

            case THEME_LIGHT_BLUE:
                activity.setTheme(R.style.ThemeApp_Light_Blue);
                break;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(MyApplication.this);
        mInstance = this;
        context = getApplicationContext();
    }
}
