package com.trackopd;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.trackopd.fragments.AddPreliminaryExaminationFragment;
import com.trackopd.fragments.ChangeStatusFragment;
import com.trackopd.fragments.CheckInFragment;
import com.trackopd.fragments.DoctorHomeFragment;
import com.trackopd.fragments.DoctorReportsFragment;
import com.trackopd.fragments.DoctorSettingsFragment;
import com.trackopd.fragments.DoctorUserProfileFragment;
import com.trackopd.fragments.NotificationFragment;
import com.trackopd.fragments.OptometristPreliminaryExaminationFragment;
import com.trackopd.fragments.ReceptionistAppointmentFragment;
import com.trackopd.fragments.ReceptionistFollowUpFragment;
import com.trackopd.fragments.ReceptionistPatientFragment;
import com.trackopd.fragments.ReceptionistPaymentFragment;
import com.trackopd.fragments.RoleWiseChangeStatusFragment;
import com.trackopd.fragments.SearchOptometristPreliminaryExaminationFragment;
import com.trackopd.fragments.SearchReceptionistPatientFragment;
import com.trackopd.fragments.SurgeryListFragment;
import com.trackopd.utils.AnimUtils;
import com.trackopd.utils.AnimatedExpandableListView;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.SessionManager;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class DoctorHomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private View currentSelectedView;
    private MenuItem mMenuGrid, mMenuSearch;
    private Toolbar mToolbar;
    private LinearLayout mLinearProfile;
    private TextView mTextToolbarHeader, mTextUserName, mTextUserType;
    private RelativeLayout mRelativeImageWithText, mRelativeImageWithoutText;
    private ImageView mImageUserProfilePicWithText, mImageNotification, mImageLogout;
    private CircleImageView mImageUserProfilePicWithoutText;
    public static DrawerLayout mDrawerLayout = null;
    private ActionBarDrawerToggle mToggle;
    private AnimatedExpandableListView mExpandableListView;
    private static ExpandableListAdapter sAdapterExpandableListViewHome;
    private List<GroupItem> mExpandableGroup;
    private long mBackPressed;
    private boolean firstTimeStartup = true, isHighlight;
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.onActivityCreateSetTheme(this);
        setContentView(R.layout.activity_home);

        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        callToDoctorHomeFragmentForFirstTime(savedInstanceState);
        getIds();
        setRegListeners();
        setData();
        setToggleMenuToDrawer();
        prepareListData();
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Tool Bar
            mToolbar = findViewById(R.id.toolbar_nav_drawer);

            // Drawer Layout
            mDrawerLayout = findViewById(R.id.drawer_layout);

            // Expandable List View
            mExpandableListView = findViewById(R.id.expandable_list_view_home);

            // Text Views
            mTextToolbarHeader = mToolbar.findViewById(R.id.text_nav_header);

            // Navigation Drawer View
            NavigationView mNavigationView = findViewById(R.id.nav_view);

            // Navigation Drawer - Linear Layout
            mLinearProfile = mNavigationView.findViewById(R.id.relative_nav_user_login);

            // Navigation Drawer - Relative Layouts
            mRelativeImageWithText = mNavigationView.findViewById(R.id.relative_nav_user_profile_pic_with_text);
            mRelativeImageWithoutText = mNavigationView.findViewById(R.id.relative_nav_user_profile_pic_without_text);

            // Navigation Drawer - Text Views
            mTextUserName = mNavigationView.findViewById(R.id.text_nav_username);
            mTextUserType = mNavigationView.findViewById(R.id.text_nav_user_type);

            // Navigation Drawer - Image Views
            mImageUserProfilePicWithText = mNavigationView.findViewById(R.id.image_nav_user_profile_pic_with_text);
            mImageUserProfilePicWithoutText = mNavigationView.findViewById(R.id.image_nav_user_profile_pic_without_text);
            mImageNotification = mNavigationView.findViewById(R.id.image_nav_notifications);
            mImageLogout = mNavigationView.findViewById(R.id.image_nav_logout);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            // ToDo: Expandable List View Click Listeners
            mExpandableListView.setOnChildClickListener(onChildClickListener);
            mExpandableListView.setOnGroupClickListener(onGroupClickListener);

            // ToDo: Click Listeners
            mLinearProfile.setOnClickListener(clickListener);
            mImageNotification.setOnClickListener(clickListener);
            mImageLogout.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            // ToDo: Sets the tool bar header properties
            mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_appointment));
            mTextToolbarHeader.setSelected(true);

            // ToDo: Sets the action bar
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            // Sets the back icon as requested
//            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//            Drawable icMenu = ContextCompat.getDrawable(this, R.drawable.nav_menu_about_us_white);
//            getSupportActionBar().setHomeAsUpIndicator(icMenu);

            // ToDo: Sets the expandable list view properties
            mExpandableListView
                    .setChildDivider(getResources().getDrawable(android.R.color.transparent));
            mExpandableListView.setDivider(getResources().getDrawable(android.R.color.transparent));
            mExpandableListView.setDividerHeight(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Set the create menu options
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        /*mMenuGrid = menu.findItem(R.id.action_grid);*/

        getMenuInflater().inflate(R.menu.menu_receptionist_patients, menu);
        mMenuSearch = menu.findItem(R.id.action_search);
        mMenuGrid = menu.findItem(R.id.action_add);

        return true;
    }

    @Override
    protected boolean onPrepareOptionsPanel(View view, Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    /**
     * Bind toggle button to drawer
     */
    private void setToggleMenuToDrawer() {
        mToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
                hideKeyboard();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
                hideKeyboard();
            }
        };

        mDrawerLayout.setDrawerListener(mToggle);
        mToggle.syncState();
    }

    private FragmentManager.OnBackStackChangedListener mBackStackChangedListener =
            new FragmentManager.OnBackStackChangedListener() {
                @Override
                public void onBackStackChanged() {
                    updateDrawerToggle();
                }
            };

    /**
     * Used to update drawer toggle
     */
    protected void updateDrawerToggle() {
        if (mToggle == null) {
            return;
        }
        boolean isRoot = getSupportFragmentManager().getBackStackEntryCount() == 0;

        mToggle.setDrawerIndicatorEnabled(isRoot);

        getSupportActionBar().setDisplayShowHomeEnabled(!isRoot);
        getSupportActionBar().setDisplayHomeAsUpEnabled(!isRoot);
        getSupportActionBar().setHomeButtonEnabled(!isRoot);

        if (isRoot) {
            mToggle.syncState();
        }

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    // set back arrow click
                    mToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            onBackPressed();
                        }
                    });
                } else {
                    mToggle.setDrawerIndicatorEnabled(true);
                }
            }
        });
    }

    /**
     * Back press event which flows as below:
     * If any fragment exist in the back stack, that fragment is poped up.
     * If any fragment is not present in the back stack the drawer is opened.
     * If drawer is opened then user is asked for alert to exit from application.
     */
    @Override
    public void onBackPressed() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        hideKeyboard();
        if (count < 1) {
//            if (!mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
//                mDrawerLayout.openDrawer(GravityCompat.START);
//            } else {
            if (mBackPressed + AppConstants.TIME_INTERVAL > System.currentTimeMillis()) {
                super.onBackPressed();
                return;
            } else {
                Toast.makeText(getBaseContext(), "Press back again to exit", Toast.LENGTH_SHORT).show();
            }
            mBackPressed = System.currentTimeMillis();
//            }
        } else {
            super.onBackPressed();
            Fragment currentFrag = getSupportFragmentManager().findFragmentById(
                    R.id.fragment_content_frame);

            Common.insertLog("Current Frag::> " + currentFrag);

            if (currentFrag instanceof DoctorSettingsFragment) {
                mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_settings));
                mMenuGrid.setVisible(false);
                mMenuSearch.setVisible(false);
            } else if (currentFrag instanceof ReceptionistPatientFragment) {
                mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_patient));
                mMenuGrid.setVisible(true);
                mMenuSearch.setVisible(true);
            } else if (currentFrag instanceof ChangeStatusFragment) {
                mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_change_status));
                mMenuGrid.setVisible(false);
                mMenuSearch.setVisible(false);
//                mExpandableListView.setSelectedGroup(1);
            } else if (currentFrag instanceof ReceptionistAppointmentFragment) {
                mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_appointment));
                mMenuGrid.setVisible(false);
                mMenuSearch.setVisible(false);
            } else if (currentFrag instanceof ReceptionistFollowUpFragment) {
                mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_follow_up));
                mMenuGrid.setVisible(false);
                mMenuSearch.setVisible(false);
            } else if (currentFrag instanceof RoleWiseChangeStatusFragment) {
                mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_change_status));
                mMenuGrid.setVisible(false);
                mMenuSearch.setVisible(false);
            } else if (currentFrag instanceof SearchOptometristPreliminaryExaminationFragment) {
                mTextToolbarHeader.setText(getResources().getString(R.string.header_search));
                mMenuGrid.setVisible(false);
                mMenuSearch.setVisible(false);
            } else if (currentFrag instanceof SearchReceptionistPatientFragment) {
                mTextToolbarHeader.setText(getResources().getString(R.string.header_search));
                mMenuGrid.setVisible(false);
                mMenuSearch.setVisible(false);
            } else if (currentFrag instanceof OptometristPreliminaryExaminationFragment) {
                mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_preliminary_examination));
                mMenuGrid.setVisible(true);
                mMenuSearch.setVisible(true);
            } else if (currentFrag instanceof AddPreliminaryExaminationFragment) {
                mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_preliminary_examination));
                mMenuGrid.setVisible(false);
                mMenuSearch.setVisible(false);
            }
        }
    }

    /**
     * Initialization of click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            try {
                hideKeyboard();
                mDrawerLayout.closeDrawer(GravityCompat.START);

                Runnable runnableCode;
                switch (v.getId()) {
                    case R.id.relative_nav_user_login:
                        runnableCode = new Runnable() {
                            @Override
                            public void run() {
                                callUserProfileFragment();
                            }
                        };
                        mHandler.postDelayed(runnableCode, 200);
                        break;

                    case R.id.image_nav_notifications:
                        runnableCode = new Runnable() {
                            @Override
                            public void run() {
                                callNotificationFragment();
                            }
                        };
                        mHandler.postDelayed(runnableCode, 200);
                        break;

                    case R.id.image_nav_logout:
                        runnableCode = new Runnable() {
                            @Override
                            public void run() {
                                openLogoutPopup();
                            }
                        };
                        mHandler.postDelayed(runnableCode, 200);
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * This method is used to hide the keyboard.
     */
    private void hideKeyboard() {
//		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
//		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

        InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        if (getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return true;
    }

    private static class GroupItem {
        String title;
        Drawable img;
        List<ChildItem> mMenuSearches = new ArrayList<>();
    }

    private static class ChildItem {
        String title;
        Drawable img;
    }

    /**
     * Click listener for the child expandable list view
     */
    private ExpandableListView.OnChildClickListener onChildClickListener = new ExpandableListView
            .OnChildClickListener() {

        @Override
        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                    int childPosition, long id) {
            setFragment(groupPosition, childPosition);
            return true;
        }
    };

    /**
     * Click listener for the group expandable list view
     */
    private ExpandableListView.OnGroupClickListener onGroupClickListener = new ExpandableListView
            .OnGroupClickListener() {

        @Override
        public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
            if (mExpandableGroup.get(groupPosition).mMenuSearches.size() == 0) {
                setFragment(groupPosition, 0);
            } else {
                if (mExpandableListView.isGroupExpanded(groupPosition)) {
                    mExpandableListView.collapseGroupWithAnimation(groupPosition);
                } else {
                    mExpandableListView.expandGroupWithAnimation(groupPosition);
                }
            }

            if (firstTimeStartup) {// first time  highlight first row
                currentSelectedView = parent.getChildAt(0);
            }

            firstTimeStartup = false;
            if (currentSelectedView != null && currentSelectedView != v) {
                unhighlightCurrentRow(currentSelectedView);
            }

            currentSelectedView = v;
            highlightCurrentRow(currentSelectedView);
            return true;
        }
    };

    /**
     * Prepares the expandable list data
     */
    private void prepareListData() {
        GroupItem mGroupItemAppointment = new GroupItem();
        GroupItem mGroupItemCheckIn = new GroupItem();
        GroupItem mGroupItemHome = new GroupItem();
        GroupItem mGroupItemPatient = new GroupItem();
        GroupItem mGroupItemConsultingPatient = new GroupItem();
        GroupItem mGroupItemFollowUp = new GroupItem();
        GroupItem mGroupItemReports = new GroupItem();
        GroupItem mGroupItemChangeStatus = new GroupItem();
        GroupItem mGroupItemSettings = new GroupItem();
        GroupItem mGroupItemPreliminaryExamination = new GroupItem();
        GroupItem mGroupItemSurgery = new GroupItem();
        GroupItem mGroupItemPayment = new GroupItem();

//        ChildItem mChildSettings = new ChildItem();

        mExpandableGroup = new ArrayList<>();
        mGroupItemCheckIn.title = getResources().getString(R.string.nav_menu_check_up);
        mGroupItemHome.title = getResources().getString(R.string.nav_menu_home);
        mGroupItemPatient.title = getResources().getString(R.string.nav_menu_patient);
        mGroupItemConsultingPatient.title = getResources().getString(R.string.nav_menu_consulting_patient);
        mGroupItemFollowUp.title = getResources().getString(R.string.nav_menu_follow_up);
        //mGroupItemPreliminaryExamination.title = getResources().getString(R.string.nav_menu_preliminary_examination);
        mGroupItemReports.title = getResources().getString(R.string.nav_menu_reports);
        mGroupItemChangeStatus.title = getResources().getString(R.string.nav_menu_change_status);
        mGroupItemSettings.title = getResources().getString(R.string.nav_menu_settings);
        mGroupItemSurgery.title=getResources().getString(R.string.nav_menu_surgery);
        mGroupItemPayment.title=getResources().getString(R.string.nav_menu_payment);
        mGroupItemAppointment.title=getResources().getString(R.string.nav_menu_appointment);

        mGroupItemAppointment.img = getResources().getDrawable(R.drawable.nav_appointment_icon);
        mGroupItemCheckIn.img = getResources().getDrawable(R.drawable.nav_menu_check_in_icon);
        mGroupItemHome.img = getResources().getDrawable(R.drawable.nav_home_icon);
        mGroupItemPatient.img = getResources().getDrawable(R.drawable.nav_patient_icon);
        mGroupItemConsultingPatient.img = getResources().getDrawable(R.drawable.nav_consulting_patient_icon);
      //  mGroupItemFollowUp.img = getResources().getDrawable(R.drawable.nav_follow_up_icon);
      //  mGroupItemPreliminaryExamination.img = getResources().getDrawable(R.drawable.nav_preliminary_examination_icon);
        mGroupItemSurgery.img=getResources().getDrawable(R.drawable.nav_menu_surgery_icon);
        mGroupItemPayment.img=getResources().getDrawable(R.drawable.nav_payment_icon);
        mGroupItemReports.img = getResources().getDrawable(R.drawable.nav_reports_icon);
      //  mGroupItemChangeStatus.img = getResources().getDrawable(R.drawable.ic_process_icon);
        mGroupItemSettings.img = getResources().getDrawable(R.drawable.nav_settings_icon);

        mExpandableGroup.add(mGroupItemAppointment);
        mExpandableGroup.add(mGroupItemCheckIn);

        mExpandableGroup.add(mGroupItemHome);
        mExpandableGroup.add(mGroupItemPatient);
        mExpandableGroup.add(mGroupItemConsultingPatient);
      //  mExpandableGroup.add(mGroupItemPreliminaryExamination);
        mExpandableGroup.add(mGroupItemSurgery);
        mExpandableGroup.add(mGroupItemPayment);
        mExpandableGroup.add(mGroupItemReports);
        mExpandableGroup.add(mGroupItemSettings);

        sAdapterExpandableListViewHome = new ExpandableListAdapter(this);
        sAdapterExpandableListViewHome.setData(mExpandableGroup);
        mExpandableListView.setAdapter(sAdapterExpandableListViewHome);
        sAdapterExpandableListViewHome.notifyDataSetChanged();
    }

    private static class ChildHolder {
        TextView mTextViewChildItem;
        ImageView mImageNavMenu;
    }

    private static class GroupHolder {
        TextView mTextViewParentHeader;
        ImageView mImageViewExpandableArrow, mImageNavMenu;
    }

    /**
     * Expandable list view adapter
     */
    public class ExpandableListAdapter
            extends AnimatedExpandableListView.AnimatedExpandableListAdapter {
        private LayoutInflater inflater;
        private List<GroupItem> mMenuSearches;

        public ExpandableListAdapter(Context context) {
            inflater = LayoutInflater.from(context);
        }

        public void setData(List<GroupItem> mMenuSearches) {
            this.mMenuSearches = mMenuSearches;
        }

        @Override
        public ChildItem getChild(int groupPosition, int childPosition) {
            return mMenuSearches.get(groupPosition).mMenuSearches.get(childPosition);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild,
                                     View convertView, ViewGroup parent) {
            ChildHolder holder;
            ChildItem mMenuSearch = getChild(groupPosition, childPosition);
            if (convertView == null) {
                holder = new ChildHolder();
                convertView = inflater.inflate(R.layout.row_expandable_child, parent, false);
                holder.mImageNavMenu = convertView.findViewById(R.id.image_view_child_nav_menu);
                holder.mTextViewChildItem = convertView.findViewById(R.id.text_view_child_nav_menu_name);
                convertView.setTag(holder);
            } else {
                holder = (ChildHolder) convertView.getTag();
            }

            holder.mTextViewChildItem.setText(mMenuSearch.title);
            holder.mImageNavMenu.setImageDrawable(mMenuSearch.img);

            return convertView;
        }

        @Override
        public int getRealChildrenCount(int groupPosition) {
            return mMenuSearches.get(groupPosition).mMenuSearches.size();
        }

        @Override
        public GroupItem getGroup(int groupPosition) {
            return mMenuSearches.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return mMenuSearches.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                                 ViewGroup parent) {
            GroupHolder holder;
            GroupItem mMenuSearch = getGroup(groupPosition);
            if (convertView == null) {
                holder = new GroupHolder();
                convertView = inflater.inflate(R.layout.row_expandable_parent, parent, false);

                holder.mImageNavMenu = convertView.findViewById(R.id.image_view_parent_nav_menu);
                holder.mTextViewParentHeader = convertView.findViewById(R.id.text_view_parent_nav_menu_name);
                holder.mImageViewExpandableArrow = convertView.findViewById(R.id.image_view_parent_nav_menu_dropdown_icon);
                convertView.setTag(holder);
            } else {
                holder = (GroupHolder) convertView.getTag();
            }

            if (firstTimeStartup && groupPosition == 0) {
                highlightCurrentRow(convertView);
            } else {
                unhighlightCurrentRow(convertView);
            }

            holder.mTextViewParentHeader.setText(mMenuSearch.title);
            holder.mImageNavMenu.setImageDrawable(mMenuSearch.img);

            if (getChildrenCount(groupPosition) == 0) {
                holder.mImageViewExpandableArrow.setVisibility(View.INVISIBLE);
            } else {
                holder.mImageViewExpandableArrow.setVisibility(View.VISIBLE);
                if (isExpanded) {
                    // up arrow
//                    holder.mImageViewExpandableArrow.setImageDrawable(getResources().
//                            getDrawable(R.drawable.nav_menu_up_arrow_selector));
                }

                if (!isExpanded) {
                    // down arrow
//                    holder.mImageViewExpandableArrow.setImageDrawable(getResources().
//                            getDrawable(R.drawable.nav_menu_down_arrow_selector));
                }
            }
            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public boolean isChildSelectable(int arg0, int arg1) {
            return true;
        }
    }

    /**
     * This method is used to unhighlight the menu drawer item
     *
     * @param rowView - View for selected menu item
     */
    private void unhighlightCurrentRow(View rowView) {
        isHighlight = false;
        setViews(rowView, isHighlight);

    }

    /**
     * This method is used to highlight the menu drawer item
     *
     * @param rowView - View for selected menu item
     */
    private void highlightCurrentRow(View rowView) {
        isHighlight = true;
        setViews(rowView, isHighlight);
    }

    /**
     * This method set up the view for highlight the menu items
     *
     * @param rowView        - View for selected menu item
     * @param isItemSelected - Boolean value to check the menu item is selected or not
     */
    private void setViews(View rowView, boolean isItemSelected) {
        TextView mTextMenuSelected = rowView.findViewById(R.id.text_view_parent_nav_menu_name);
        ImageView mImageMenuSelected = rowView.findViewById(R.id.image_view_parent_nav_menu);

        if (isItemSelected) {
            mTextMenuSelected.setTextColor(Common.setThemeColor(DoctorHomeActivity.this));
            mImageMenuSelected.setColorFilter(Common.setThemeColor(DoctorHomeActivity.this));
        } else {
            mTextMenuSelected.setTextColor(getResources().getColor(R.color.colorControlNormal));
            mImageMenuSelected.setColorFilter(getResources().getColor(R.color.colorControlNormal));
        }
    }

    /**
     * Sets up the fragment as requested - New (With Permission)
     */
    private void setFragment(int grpPos, int childPos) {
        try {
            mDrawerLayout.closeDrawer(GravityCompat.START);

            Runnable runnableCode;
            switch (grpPos) {
                case 0:
                    runnableCode = new Runnable() {
                        @Override
                        public void run() {
                            callToAppointmentFragment();
                        }
                    };
                    mHandler.postDelayed(runnableCode, 200);
                    break;

                case 1:
                    runnableCode = new Runnable() {
                        @Override
                        public void run() {
                            callToCheckInFragment();
                        }
                    };
                    mHandler.postDelayed(runnableCode, 200);
                    break;

                case 2:
                    runnableCode = new Runnable() {
                        @Override
                        public void run() {
                            callToHomeFragment();
                        }
                    };
                    mHandler.postDelayed(runnableCode, 200);
                    break;

                case 3:
                    runnableCode = new Runnable() {
                        @Override
                        public void run() {
                            callToPatientFragment();
                        }
                    };
                    mHandler.postDelayed(runnableCode, 200);
                    break;

                case 4:
                    runnableCode = new Runnable() {
                        @Override
                        public void run() {
                            callToConsultingPatientFragment();
                        }
                    };
                    mHandler.postDelayed(runnableCode, 200);
                    break;

                case 5:
                    runnableCode = new Runnable() {
                        @Override
                        public void run() {
                            callToSurgeryFragment();
                        }
                    };
                    mHandler.postDelayed(runnableCode, 200);
                    break;

                case 6:
                    runnableCode = new Runnable() {
                        @Override
                        public void run() {
                            callToPaymentFragment();
                        }
                    };
                    mHandler.postDelayed(runnableCode, 200);
                    break;


                case 7:
                    runnableCode = new Runnable() {
                        @Override
                        public void run() {
                            callToReportsFragment();
                        }
                    };
                    mHandler.postDelayed(runnableCode, 200);
                    break;

                case 8:
                    runnableCode = new Runnable() {
                        @Override
                        public void run() {
                            callToSettingsFragment();
                        }
                    };
                    mHandler.postDelayed(runnableCode, 200);
                    break;

                case 9:
                    runnableCode = new Runnable() {
                        @Override
                        public void run() {

                        }
                    };
                    mHandler.postDelayed(runnableCode, 200);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the home fragment for the first time
     */
    private void callToDoctorHomeFragmentForFirstTime(Bundle savedInstanceState) {
        try {
            CheckInFragment dashboardFragment = new CheckInFragment();
           // ReceptionistAppointmentFragment dashboardFragment = new ReceptionistAppointmentFragment();
            if (findViewById(R.id.fragment_content_frame) != null) {
                if (savedInstanceState != null) {
                    return;
                }
                getSupportFragmentManager().beginTransaction().add(R.id.fragment_content_frame,
                        dashboardFragment).commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Doctor Home Fragment
     */
    private void callToHomeFragment() {
        try {
            mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_home));
            Fragment fragment = new DoctorHomeFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    getResources().getString(R.string.nav_menu_home)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Doctor Patient Fragment
     */
    private void callToPatientFragment() {
        try {
            mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_patient));
            Fragment fragment = new ReceptionistPatientFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_IS_FROM_SEARCH, AppConstants.RECEPTIONIST_PATIENT);
            args.putString(AppConstants.BUNDLE_ADD_PATIENT, "3");
            args.putString(AppConstants.BUNDLE_PATIENT_NAME, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, AppConstants.STR_EMPTY_STRING);
            fragment.setArguments(args);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    getResources().getString(R.string.nav_menu_patient)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Doctor Consulting Patient Fragment
     */
    private void callToConsultingPatientFragment() {
        try {
            /*mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_consulting_patient));
            Fragment fragment = new DoctorConsultingPatientFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    getResources().getString(R.string.nav_menu_consulting_patient)).commit();*/

            mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_consulting_patient));
            Fragment fragment = new ReceptionistPatientFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_IS_FROM_SEARCH, AppConstants.RECEPTIONIST_PATIENT);
            args.putString(AppConstants.BUNDLE_ADD_PATIENT, "2");
            args.putString(AppConstants.BUNDLE_PATIENT_NAME, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, AppConstants.STR_EMPTY_STRING);
            fragment.setArguments(args);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    getResources().getString(R.string.nav_menu_patient)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Receptionist Appointment Fragment
     */
    private void callToAppointmentFragment() {
        try {
            mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_appointment));

            Fragment fragment = new ReceptionistAppointmentFragment();

            Bundle args = new Bundle();
//            args.putString(AppConstants.BUNDLE_ADD_APPOINTMENT, "1");
            args.putString(AppConstants.BUNDLE_PATIENT_NAME, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_MRD_NO, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DATE, getResources().getString(R.string.date_format_first_time));
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, -1);

            fragment.setArguments(args);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    getResources().getString(R.string.nav_menu_appointment)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Check In Fragment
     */
    private void callToCheckInFragment() {
        try {
            mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_check_up));
            Fragment fragment = new CheckInFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    getResources().getString(R.string.nav_menu_check_up)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Receptionist Payment Fragment
     */
    private void callToSurgeryFragment() {
        try {
            mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_surgery));
            Fragment fragment = new SurgeryListFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    getResources().getString(R.string.nav_menu_surgery)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Receptionist Follow Up Fragment
     */
    private void callToFollowUpFragment() {
        try {
            mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_follow_up));
            Fragment fragment = new ReceptionistFollowUpFragment();
            Bundle args = new Bundle();
//            args.putString(AppConstants.BUNDLE_ADD_FOLLOW_UP, "1");
            args.putString(AppConstants.BUNDLE_PATIENT_NAME, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_CODE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, AppConstants.STR_EMPTY_STRING);
            args.putString(AppConstants.BUNDLE_PATIENT_FOLLOW_UP_DATE, "1970-01-01");
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, -1);
            args.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_COUNCILLOR, -1);
            fragment.setArguments(args);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    getResources().getString(R.string.nav_menu_follow_up)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Optometrist Preliminary Examination Fragment
     */
    private void callToPreliminaryExaminationFragment() {
        try {
            mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_preliminary_examination));
            Fragment fragment = new OptometristPreliminaryExaminationFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    getResources().getString(R.string.nav_menu_preliminary_examination)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Doctor Reports Fragment
     */
    private void callToReportsFragment() {
        try {
            mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_reports));
            Fragment fragment = new DoctorReportsFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    getResources().getString(R.string.nav_menu_reports)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Receptionist Payment Fragment
     */
    private void callToPaymentFragment() {
        try {
            mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_payment));
            Fragment fragment = new ReceptionistPaymentFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    getResources().getString(R.string.nav_menu_payment)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Change Status Fragment
     */
    private void callToChangeStatusFragment() {
        try {
            mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_change_status));
            Fragment fragment = new RoleWiseChangeStatusFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    getResources().getString(R.string.nav_menu_change_status)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Doctor Settings Fragment
     */
    private void callToSettingsFragment() {
        try {
            mTextToolbarHeader.setText(getResources().getString(R.string.nav_menu_settings));
            Fragment fragment = new DoctorSettingsFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    getResources().getString(R.string.nav_menu_settings)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Notification Fragment
     */
    private void callNotificationFragment() {
        try {
            mTextToolbarHeader.setText(getResources().getString(R.string.header_notification));
            Fragment fragment = new NotificationFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    getResources().getString(R.string.header_notification)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the User Profile Fragment
     */
    private void callUserProfileFragment() {
        try {
            mTextToolbarHeader.setText(getResources().getString(R.string.header_user_profile));
            Bundle args = new Bundle();
            Fragment fragment = new DoctorUserProfileFragment();
//            args.putString(AppConstants.BUNDLE_UPDATE, "1");
            fragment.setArguments(args);
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame,
                    fragment, getResources()
                            .getString(R.string.tag_view_user_profile))
//                    .addToBackStack(getResources()
//                            .getString(R.string.back_stack_user_profile))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open popup for logout
     */
    private void openLogoutPopup() {
        try {
            new MaterialDialog.Builder(DoctorHomeActivity.this)
                    .title(R.string.app_name)
                    .content(R.string.dialog_logout_message)
                    .canceledOnTouchOutside(false)
                    .positiveText(R.string.action_ok)
                    .positiveColor(getResources().getColor(R.color.app_main_theme))
                    .negativeText(R.string.action_cancel)
                    .negativeColor(getResources().getColor(R.color.colorControlNormal))

                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            SessionManager mSharedPref = new SessionManager(DoctorHomeActivity.this);
                            mSharedPref.clearAllPreferences();
                            Intent intent = new Intent(DoctorHomeActivity.this, PassCodeActivity.class);
                            startActivity(intent);
                            AnimUtils.activityExitAnim(DoctorHomeActivity.this);
                            finishAffinity();
                        }

                        public void onNegative(MaterialDialog dialog) {
                            dialog.dismiss();
                            mDrawerLayout.closeDrawers();
                        }
                    })
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the fragment if exist
     *
     * @param fragmentExisting Fragment existing to be shown
     * @param fragmentManager  Fragment manager used for fragment transactions
     */
    private void showFragment(Fragment fragmentExisting, FragmentManager fragmentManager) {
        FragmentManager fragmentManagerInternal = getSupportFragmentManager();
        for (int i = 0; i < fragmentManagerInternal.getBackStackEntryCount(); ++i) {
            fragmentManagerInternal.popBackStack();
        }
        fragmentManagerInternal.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

        try {
            List<Fragment> fragments = fragmentManager.getFragments();
            if (fragments.size() > 0) {
                for (int i = 0; i < fragments.size(); i++) {
                    if (fragments.get(i) != null) {
                        fragmentManager.beginTransaction().hide(fragments.get(i)).commit();
                    }
                }
            }
            fragmentManager.beginTransaction().show(fragmentExisting).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Used to remove all fragment in the fragment manager after logout
     */
    private void removeAllFragments() {
        try {
            List<Fragment> fragments = getSupportFragmentManager().getFragments();
            if (fragments.size() > 0) {
                for (int i = 0; i < fragments.size(); i++) {
                    if (fragments.get(i) != null) {
                        getSupportFragmentManager().beginTransaction().remove(fragments.get(i))
                                .commit();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onStart() {
        super.onStart();
//        Common.insertLog("URL " + SessionManager.getUserProfilePic(HomeActivity.this));
        String mFirstName = GetJsonData.getLoginData(DoctorHomeActivity.this, WebFields.LOGIN.RESPONSE_FIRST_NAME);
        String mLastName = GetJsonData.getLoginData(DoctorHomeActivity.this, WebFields.LOGIN.RESPONSE_LAST_NAME);
        String mUserName = mFirstName + AppConstants.STR_EMPTY_SPACE + mLastName;
        String mUserType = GetJsonData.getLoginData(DoctorHomeActivity.this, WebFields.LOGIN.RESPONSE_USER_TYPE);
        mTextUserName.setText(mUserName);
        mTextUserType.setText(mUserType);

        // ToDo: Round with Border
        if (mUserName.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            mRelativeImageWithText.setVisibility(View.GONE);
            mRelativeImageWithoutText.setVisibility(View.VISIBLE);

//            Glide.with(mActivity).load(SessionManager.getBaseUrlForVisitList(mActivity) + productListModel
//                    .getLogoId() + "_" + productListModel.getLogoName() + AppConstants
//                    .STR_FILE_SERVICE_TOKEN + SessionManager
//                    .getToken(mActivity)).into(((CustomViewHolder) holder).mImageRoundProduct);
        } else {
            mRelativeImageWithText.setVisibility(View.VISIBLE);
            mRelativeImageWithoutText.setVisibility(View.GONE);
            mImageUserProfilePicWithText.setImageDrawable(Common.setLabeledImageView(DoctorHomeActivity.this, mFirstName, mLastName));
        }
    }

    @Override
    public void onStop() {
        Common.insertLog("onStop");
        super.onStop();
    }

    /**
     * Sets up the toolbar
     *
     * @param toolbar - Sets the toolbar
     */
    public void setToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportFragmentManager().addOnBackStackChangedListener(mBackStackChangedListener);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * Sets up header text throughout the application
     *
     * @param appTitle Text required to be set for header
     */
    public void setAppHeader(String appTitle) {
        mTextToolbarHeader.setText(appTitle);
    }

    /**
     * Sets up visibility of logo in toolbar
     *
     * @param isVisible Visibility required to be set as View.VISIBLE and so on
     */
    public void setVisibilityHeaderIcon(int isVisible) {
//        sImageViewRightIcon.setVisibility(isVisible);
    }

    /**
     * Notify the expandable list
     */
    public void notifyExpandableListView() {
        sAdapterExpandableListViewHome.notifyDataSetChanged();
    }

    /**
     * Sets up header text throughout the application
     *
     * @param isSingleLine Text required to be set for header single line
     */
    public void setAppHeaderSingleLine(boolean isSingleLine) {
        mTextToolbarHeader.setSingleLine(isSingleLine);
    }

    /**
     * Sets up header text throughout the application
     *
     * @param isEllipsize Text required to be set for header ellipsize
     */
    public void setAppHeaderEllipsize(TextUtils.TruncateAt isEllipsize) {
        mTextToolbarHeader.setEllipsize(isEllipsize);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        Common.insertLog("HOME :: Permission CALL !!! " + requestCode);
//
//        Common.insertLog("permission length:>> " + permissions.length);
//        if (grantResults.length > 0
//                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            Common.insertLog("HOME :: Permission granted");
//            Intent intent;
//            switch (requestCode) {
//                case AppConstants.PERMISSION_CAMERA:
//                    intent = new Intent(AppConstants.BROADCAST_CUSTOM_PERMISSION_TYPE);
//                    intent.putExtra(AppConstants.STR_CUSTOM_PERMISSION_TYPE, AppConstants.PERMISSION_CAMERA);
//                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
//                    break;
//
//                case AppConstants.PERMISSION_STORAGE:
//                    intent = new Intent(AppConstants.BROADCAST_CUSTOM_PERMISSION_TYPE);
//                    intent.putExtra(AppConstants.STR_CUSTOM_PERMISSION_TYPE, AppConstants.PERMISSION_STORAGE);
//                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
//                    break;
//
//                case AppConstants.PERMISSION_LOCATION:
//                    intent = new Intent(AppConstants.BROADCAST_CUSTOM_PERMISSION_TYPE);
//                    intent.putExtra(AppConstants.STR_CUSTOM_PERMISSION_TYPE, AppConstants.PERMISSION_LOCATION);
//                    LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
//                    break;
//            }
//        } else {
//            Common.insertLog("HOME :: Permission denied");
//        }
    }
}