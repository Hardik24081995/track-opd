package com.trackopd;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nightonke.blurlockview.BlurLockView;
import com.nightonke.blurlockview.Password;
import com.trackopd.model.ConfigurationModel;
import com.trackopd.model.PassCodeModel;
import com.trackopd.utils.AppCallbackListener;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.utils.SessionManager;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PassCodeActivity extends BaseActivity implements BlurLockView.OnPasswordInputListener, AppCallbackListener.CallBackListener {

    private BlurLockView mBlurLockView;
    private SessionManager mSessionManager;
    private String mPassCode, mInputPassCode, mUserType;
    private Intent intent;
    private AppUtil mAppUtils;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.onActivityCreateSetTheme(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_passcode);
        mAppUtils = new AppUtil(this);

        getIds();
        setData();
        setRegListeners();
        callConfigurationAPI();
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Blur Lock View (Pass Code)
            mBlurLockView = findViewById(R.id.blur_lock_view);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mBlurLockView.setOnPasswordInputListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            mSessionManager = new SessionManager(PassCodeActivity.this);
            mPassCode = mSessionManager.getPreferences(mSessionManager.APP_PASS_CODE, "");
            mUserType = GetJsonData.getLoginData(PassCodeActivity.this, WebFields.LOGIN.RESPONSE_USER_TYPE);

            setBlurLockView();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the blur lock view properties
     */
    private void setBlurLockView() {
        try {
            // ToDo: Sets up the pass code properties
            mBlurLockView.setType(Password.NUMBER, false);
            mBlurLockView.setTitle(getString(R.string.header_pass_code));
            mBlurLockView.setRightButton(getString(R.string.action_delete));
            mBlurLockView.setLeftButton(AppConstants.STR_EMPTY_STRING);
            mBlurLockView.setVisibility(View.VISIBLE);
            mBlurLockView.setPasswordLength(5);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void correct(String inputPassword) {
        try {
            mInputPassCode = inputPassword;

            // Clear Credentials
            if (!mAppUtils.getConnectionState()) {
                // ToDo: Sets up the pass code properties
                setBlurLockView();
                mAppUtils.displayNoInternetSnackBar(mBlurLockView, new AppCallbackListener(this));
            } else {
                showDialog(PassCodeActivity.this, false);
                callPassCodeAPI();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void incorrect(String inputPassword) {
        try {
            Toast.makeText(this, "Wrong passcode.", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void input(String inputPassword) {
    }

    /**
     * This method should call the Pass Code API
     */
    private void callPassCodeAPI() {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.setPassCodeJson(mInputPassCode));

            Call<PassCodeModel> call = RetrofitClient.createService(ApiInterface.class).passCodeAPI(body);
            call.enqueue(new Callback<PassCodeModel>() {
                @Override
                public void onResponse(@NonNull Call<PassCodeModel> call, @NonNull Response<PassCodeModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));

                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            //Toast.makeText(PassCodeActivity.this, mMessage, Toast.LENGTH_LONG).show();
                            hideDialog();

                            //Set Passcode
                            mSessionManager.setPreferences(mSessionManager.APP_PASS_CODE, mInputPassCode);
                            //set Hospital Database

                            JSONArray jsonArray = jsonObject.getJSONArray(WebFields.DATA);

                            String mConfigurationData = String.valueOf(jsonArray.get(0));
                            mSessionManager.setPreferences(mSessionManager.KEY_HOSPITAL_DATA, mConfigurationData);

                            if (mPassCode.equalsIgnoreCase(mInputPassCode)) {
                                if (mUserType.equalsIgnoreCase(getResources().getString(R.string.user_type_receptionist))) {
                                    intent = new Intent(PassCodeActivity.this, ReceptionistHomeActivity.class);
                                } else if (mUserType.equalsIgnoreCase(getResources().getString(R.string.user_type_optometrist))) {
                                    intent = new Intent(PassCodeActivity.this, OptometristHomeActivity.class);
                                } else if (mUserType.equalsIgnoreCase(getResources().getString(R.string.user_type_doctor))) {
                                    intent = new Intent(PassCodeActivity.this, DoctorHomeActivity.class);
                                } else if (mUserType.equalsIgnoreCase(getResources().getString(R.string.user_type_patient))) {
                                    intent = new Intent(PassCodeActivity.this, PatientHomeActivity.class);
                                } else if (mUserType.equalsIgnoreCase(getResources().getString(R.string.user_type_councillor))) {
                                    intent = new Intent(PassCodeActivity.this, CouncillorHomeActivity.class);
                                } else {
                                    intent = new Intent(PassCodeActivity.this, LoginActivity.class);
                                }
                            } else {
                                intent = new Intent(PassCodeActivity.this, LoginActivity.class);
                            }
                            startActivity(intent);
                            finish();
                        } else {
                            hideDialog();
                            Toast.makeText(PassCodeActivity.this, mMessage, Toast.LENGTH_LONG).show();
                            mBlurLockView.clearWrongPassWord();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PassCodeModel> call, @NonNull Throwable t) {
                    hideDialog();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onAppCallback(int Code) {
        setBlurLockView();
    }

    /**
     * This method should call the Configuration API
     */
    private void callConfigurationAPI() {

        try {
            showDialog(this, false);

            String hospital_database = GetJsonData.getHospitalData(PassCodeActivity.this, mSessionManager.KEY_HOSPITAL_DATABASE_NAME);
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.setConfigurationJson(hospital_database));
            Call<ConfigurationModel> call = RetrofitClient.createService(ApiInterface.class).configurationAPI(body);
            call.enqueue(new Callback<ConfigurationModel>() {
                @Override
                public void onResponse(@NonNull Call<ConfigurationModel> call, @NonNull Response<ConfigurationModel> response) {

                    hideDialog();

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            hideDialog();
                            // Common.setCustomToast(LoginActivity.this, mMessage);

                            if (jsonObject.has(WebFields.DATA)) {
                                String mConfigurationData = jsonObject.getString(WebFields.DATA);

                                String EndTime = response.body().getData().getEndTime();

                               /* Date c = Calendar.getInstance().getTime();
                                System.out.println("Current time => " + c);

                                SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
                                String formattedDate = df.format(c);

                                String finaldate = formattedDate + " " +"09:00:00";
                               // long millis = date.getTime();

                                Date date = df.parse(finaldate);
                                long millis = date.getTime();*/


                                final String str = EndTime;
                                final Timestamp timestamp =
                                        Timestamp.valueOf(
                                                new SimpleDateFormat("yyyy-MM-dd ")
                                                        .format(new Date()) // get the current date as String
                                                        .concat(str)        // and append the time
                                        );
                                System.out.println(timestamp);

                                long millis = timestamp.getTime();

                                Log.d("endtime", "" + millis);
                                //  LocalTime localTime = LocalTime.parse(finaldate);
                                // int millis = localTime.toSecondOfDay() * 1000;
                                mSessionManager.setPreferences(mSessionManager.KEY_CONFIGURATION_DATA, mConfigurationData);
                                if (System.currentTimeMillis() > millis) {
                                    new AlertDialog.Builder(PassCodeActivity.this)
                                            .setTitle(getString(R.string.app_name))
                                            .setMessage("You can't use App after office hours")
                                            .setCancelable(false)

                                            // Specifying a listener allows you to take an action before dismissing the dialog.
                                            // The dialog is automatically dismissed when a dialog button is clicked.
                                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                    finish();
                                                }
                                            })

                                            .setIcon(android.R.drawable.ic_dialog_alert)
                                            .show();
                                }

                            }
                        } else {
                            hideDialog();
                            Common.setCustomToast(PassCodeActivity.this, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ConfigurationModel> call, @NonNull Throwable t) {
                    hideDialog();
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
