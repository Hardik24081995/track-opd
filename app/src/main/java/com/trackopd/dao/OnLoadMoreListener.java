package com.trackopd.dao;

public interface OnLoadMoreListener {
    void onLoadMore();
}
