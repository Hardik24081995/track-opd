package com.trackopd;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.trackopd.model.ForgotPasswordModel;
import com.trackopd.utils.AppCallbackListener;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.utils.StringUtils;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends BaseActivity implements AppCallbackListener.CallBackListener {

    private LinearLayout mLinearMainView;
    private EditText mEditEmail;
    private Button mButtonSubmit;
    private AppUtil mAppUtils;
    private String mEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        MyApplication.onActivityCreateSetTheme(this);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_forgot_password);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;

//        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        ((TextView) findViewById(R.id.text_toolbar_header)).setText(getString(R.string.header_forgot_password));
        ((TextView) findViewById(R.id.text_toolbar_header)).setTextColor(getResources().getColor(R.color.color_login));
//        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mAppUtils = new AppUtil(this);

        getIds();
        setRegListeners();
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Linear Layouts
            mLinearMainView = findViewById(R.id.linear_forgot_pass_main_view);

            // Edit Texts
            mEditEmail = findViewById(R.id.edit_forgot_pass_email);

            // Buttons
            mButtonSubmit = findViewById(R.id.button_forgot_pass_submit);

            // Set Request Focus
            mEditEmail.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mButtonSubmit.setOnClickListener(clickListener);

            mEditEmail.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {
                        doForgotPassword();
                    }
                    return false;
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_forgot_pass_submit:
                    doForgotPassword();
                    break;
            }
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * This method checks the function first then call the API
     */
    private void doForgotPassword() {
        hideSoftKeyboard();
        if (checkValidation()) {
            if (!mAppUtils.getConnectionState()) {
                mAppUtils.displayNoInternetSnackBar(mLinearMainView, new AppCallbackListener(this));
            } else {
                showDialog(ForgotPasswordActivity.this, false);
                callForgotPasswordAPI();
            }
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {

        mEmail = mEditEmail.getText().toString().trim();
        mEditEmail.setError(null);

        if (TextUtils.isEmpty(mEmail)) {
            mEditEmail.setError(getResources().getString(R.string.error_field_required));
            return false;
        } else if (!StringUtils.isEmailValid(mEmail)) {
            mEditEmail.setError(getResources().getString(R.string.error_invalid_email));
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onAppCallback(int Code) {
        if (Code == AppConstants.SNACK_BAR_RETRY) {
            doForgotPassword();
        }
    }

    /**
     * This method should call the Forgot Password API
     */
    private void callForgotPasswordAPI() {
        try {
            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.setForgotPasswordJson(mEmail));

            Call<ForgotPasswordModel> call = RetrofitClient.createService(ApiInterface.class).forgotPasswordAPI(body);
            call.enqueue(new Callback<ForgotPasswordModel>() {
                @Override
                public void onResponse(@NonNull Call<ForgotPasswordModel> call, @NonNull Response<ForgotPasswordModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            hideDialog();
                            Common.setCustomToast(ForgotPasswordActivity.this, mMessage);
                            mEditEmail.setText("");
                        } else {
                            hideDialog();
                            Common.setCustomToast(ForgotPasswordActivity.this, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ForgotPasswordModel> call, @NonNull Throwable t) {
                    hideDialog();
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
