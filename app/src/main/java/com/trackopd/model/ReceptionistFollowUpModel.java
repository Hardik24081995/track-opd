package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ReceptionistFollowUpModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("rowCount")
    @Expose
    private String rowCount;
    @SerializedName("data")
    @Expose
    private ArrayList<ReceptionistFollowUpModel> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRowCount() {
        return rowCount;
    }

    public void setRowCount(String rowCount) {
        this.rowCount = rowCount;
    }

    public ArrayList<ReceptionistFollowUpModel> getData() {
        return data;
    }

    public void setData(ArrayList<ReceptionistFollowUpModel> data) {
        this.data = data;
    }

    @SerializedName("FollowupID")
    @Expose
    private String followupID;
    @SerializedName("PatientFirstName")
    @Expose
    private String patientFirstName;
    @SerializedName("PatientLastName")
    @Expose
    private String patientLastName;
    @SerializedName("PatientMobileNo")
    @Expose
    private String patientMobileNo;
    @SerializedName("DoctorFirstName")
    @Expose
    private String doctorFirstName;
    @SerializedName("DoctorLastName")
    @Expose
    private String doctorLastName;
    @SerializedName("DoctorName")
    @Expose
    private String doctorName;
    @SerializedName("CouncillorName")
    @Expose
    private String councillorName;
    @SerializedName("CouncillorFirstName")
    @Expose
    private String councillorFirstName;
    @SerializedName("CouncillorLastName")
    @Expose
    private String councillorLastName;
    @SerializedName("FollowupDate")
    @Expose
    private String followupDate;
    @SerializedName("Notes")
    @Expose
    private String notes;
    @SerializedName("DoctorID")
    @Expose
    private String doctorID;
    @SerializedName("CouncillorID")
    @Expose
    private String councillorID;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("PatientID")
    @Expose
    private String patientID;
    @SerializedName("Rno")
    @Expose
    private String rno;
    @SerializedName("rowcount")
    @Expose
    private String rowcount;

    public String getFollowupID() {
        return followupID;
    }

    public void setFollowupID(String followupID) {
        this.followupID = followupID;
    }

    public String getPatientFirstName() {
        return patientFirstName;
    }

    public void setPatientFirstName(String patientFirstName) {
        this.patientFirstName = patientFirstName;
    }

    public String getPatientLastName() {
        return patientLastName;
    }

    public void setPatientLastName(String patientLastName) {
        this.patientLastName = patientLastName;
    }

    public String getPatientMobileNo() {
        return patientMobileNo;
    }

    public void setPatientMobileNo(String patientMobileNo) {
        this.patientMobileNo = patientMobileNo;
    }

    public String getDoctorFirstName() {
        return doctorFirstName;
    }

    public void setDoctorFirstName(String doctorFirstName) {
        this.doctorFirstName = doctorFirstName;
    }

    public String getDoctorLastName() {
        return doctorLastName;
    }

    public void setDoctorLastName(String doctorLastName) {
        this.doctorLastName = doctorLastName;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getCouncillorName() {
        return councillorName;
    }

    public void setCouncillorName(String councillorName) {
        this.councillorName = councillorName;
    }

    public String getCouncillorFirstName() {
        return councillorFirstName;
    }

    public void setCouncillorFirstName(String councillorFirstName) {
        this.councillorFirstName = councillorFirstName;
    }

    public String getCouncillorLastName() {
        return councillorLastName;
    }

    public void setCouncillorLastName(String councillorLastName) {
        this.councillorLastName = councillorLastName;
    }

    public String getFollowupDate() {
        return followupDate;
    }

    public void setFollowupDate(String followupDate) {
        this.followupDate = followupDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(String doctorID) {
        this.doctorID = doctorID;
    }

    public String getCouncillorID() {
        return councillorID;
    }

    public void setCouncillorID(String councillorID) {
        this.councillorID = councillorID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public String getRno() {
        return rno;
    }

    public void setRno(String rno) {
        this.rno = rno;
    }

    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }
}