package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SurgeryListModel implements Serializable {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("rowCount")
    @Expose
    private String rowCount;
    @SerializedName("data")
    @Expose
    private List<SurgeryListModel> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRowCount() {
        return rowCount;
    }

    public void setRowCount(String rowCount) {
        this.rowCount = rowCount;
    }

    public List<SurgeryListModel> getData() {
        return data;
    }

    public void setData(List<SurgeryListModel> data) {
        this.data = data;
    }

    @SerializedName("SurgeryNotesID")
    @Expose
    private String surgeryNotesID;
    @SerializedName("AppointmentID")
    @Expose
    private String appointmentID;
    @SerializedName("SurgeryDate")
    @Expose
    private String surgeryDate;
    @SerializedName("StartTime")
    @Expose
    private String startTime;
    @SerializedName("EndTime")
    @Expose
    private String endTime;
    @SerializedName("SurgeryTypeID")
    @Expose
    private String surgeryTypeID;
    @SerializedName("RACD")
    @Expose
    private String rACD;
    @SerializedName("LACD")
    @Expose
    private String lACD;
    @SerializedName("RAL")
    @Expose
    private String rAL;
    @SerializedName("LAL")
    @Expose
    private String lAL;
    @SerializedName("RIncisionSize")
    @Expose
    private String rIncisionSize;
    @SerializedName("LIncisionSize")
    @Expose
    private String lIncisionSize;
    @SerializedName("RIncisionType")
    @Expose
    private String rIncisionType;
    @SerializedName("LIncisionType")
    @Expose
    private String lIncisionType;
    @SerializedName("RSurgeryNotes")
    @Expose
    private String rSurgeryNotes;
    @SerializedName("LSurgeryNotes")
    @Expose
    private String lSurgeryNotes;
    @SerializedName("RViscoelastics")
    @Expose
    private String rViscoelastics;
    @SerializedName("LViscoelastics")
    @Expose
    private String lViscoelastics;
    @SerializedName("RRemarks")
    @Expose
    private String rRemarks;
    @SerializedName("LRemarks")
    @Expose
    private String lRemarks;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("ImplantBrand")
    @Expose
    private String implantBrand;
    @SerializedName("ImplantName")
    @Expose
    private String implantName;
    @SerializedName("A_Constant")
    @Expose
    private String aConstant;
    @SerializedName("ImplantPower")
    @Expose
    private String implantPower;
    @SerializedName("ImplantPlacement")
    @Expose
    private String implantPlacement;
    @SerializedName("ImplantExpiry")
    @Expose
    private String implantExpiry;
    @SerializedName("ImplantSlNo")
    @Expose
    private String implantSlNo;
    @SerializedName("Notes")
    @Expose
    private String notes;
    @SerializedName("Pulse")
    @Expose
    private String pulse;
    @SerializedName("BP")
    @Expose
    private String bP;
    @SerializedName("SPO2")
    @Expose
    private String sPO2;
    @SerializedName("RBS")
    @Expose
    private String rBS;
    @SerializedName("EyeID")
    @Expose
    private String eyeID;
    @SerializedName("AnaesthesiaTypeID")
    @Expose
    private String anaesthesiaTypeID;
    @SerializedName("AnaesthesiaMedicine")
    @Expose
    private String anaesthesiaMedicine;
    @SerializedName("Company")
    @Expose
    private String company;
    @SerializedName("TPA")
    @Expose
    private String tPA;
    @SerializedName("PolicyNo")
    @Expose
    private String policyNo;
    @SerializedName("ExpiryDate")
    @Expose
    private String expiryDate;
    @SerializedName("SettlementReceived")
    @Expose
    private String settlementReceived;
    @SerializedName("SettlementAmount")
    @Expose
    private String settlementAmount;
    @SerializedName("SurgeryName")
    @Expose
    private String surgeryName;
    @SerializedName("Eye")
    @Expose
    private String eye;
    @SerializedName("AnaesthesiaType")
    @Expose
    private String anaesthesiaType;
    @SerializedName("PatientFirstName")
    @Expose
    private String patientFirstName;
    @SerializedName("PatientLastName")
    @Expose
    private String patientLastName;
    @SerializedName("PatientMobileNo")
    @Expose
    private String patientMobileNo;
    @SerializedName("PatientCode")
    @Expose
    private String patientCode;
    @SerializedName("ProfileImage")
    @Expose
    private String profileImage;
    @SerializedName("TicketNumber")
    @Expose
    private String ticketNumber;
    @SerializedName("Rno")
    @Expose
    private String rno;
    @SerializedName("rowcount")
    @Expose
    private String rowcount;

    @SerializedName("DoctorID")
    @Expose
    private String doctorId;
    @SerializedName("DoctorName")
    @Expose
    private String DoctorName;
    @SerializedName("PatientID")
    @Expose
    private String PatientID;



    public String getSurgeryNotesID() {
        return surgeryNotesID;
    }

    public void setSurgeryNotesID(String surgeryNotesID) {
        this.surgeryNotesID = surgeryNotesID;
    }

    public String getAppointmentID() {
        return appointmentID;
    }

    public void setAppointmentID(String appointmentID) {
        this.appointmentID = appointmentID;
    }

    public String getSurgeryDate() {
        return surgeryDate;
    }

    public void setSurgeryDate(String surgeryDate) {
        this.surgeryDate = surgeryDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getSurgeryTypeID() {
        return surgeryTypeID;
    }

    public void setSurgeryTypeID(String surgeryTypeID) {
        this.surgeryTypeID = surgeryTypeID;
    }

    public String getRACD() {
        return rACD;
    }

    public void setRACD(String rACD) {
        this.rACD = rACD;
    }

    public String getLACD() {
        return lACD;
    }

    public void setLACD(String lACD) {
        this.lACD = lACD;
    }

    public String getRAL() {
        return rAL;
    }

    public void setRAL(String rAL) {
        this.rAL = rAL;
    }

    public String getLAL() {
        return lAL;
    }

    public void setLAL(String lAL) {
        this.lAL = lAL;
    }

    public String getRIncisionSize() {
        return rIncisionSize;
    }

    public void setRIncisionSize(String rIncisionSize) {
        this.rIncisionSize = rIncisionSize;
    }

    public String getLIncisionSize() {
        return lIncisionSize;
    }

    public void setLIncisionSize(String lIncisionSize) {
        this.lIncisionSize = lIncisionSize;
    }

    public String getRIncisionType() {
        return rIncisionType;
    }

    public void setRIncisionType(String rIncisionType) {
        this.rIncisionType = rIncisionType;
    }

    public String getLIncisionType() {
        return lIncisionType;
    }

    public void setLIncisionType(String lIncisionType) {
        this.lIncisionType = lIncisionType;
    }

    public String getRSurgeryNotes() {
        return rSurgeryNotes;
    }

    public void setRSurgeryNotes(String rSurgeryNotes) {
        this.rSurgeryNotes = rSurgeryNotes;
    }

    public String getLSurgeryNotes() {
        return lSurgeryNotes;
    }

    public void setLSurgeryNotes(String lSurgeryNotes) {
        this.lSurgeryNotes = lSurgeryNotes;
    }

    public String getRViscoelastics() {
        return rViscoelastics;
    }

    public void setRViscoelastics(String rViscoelastics) {
        this.rViscoelastics = rViscoelastics;
    }

    public String getLViscoelastics() {
        return lViscoelastics;
    }

    public void setLViscoelastics(String lViscoelastics) {
        this.lViscoelastics = lViscoelastics;
    }

    public String getRRemarks() {
        return rRemarks;
    }

    public void setRRemarks(String rRemarks) {
        this.rRemarks = rRemarks;
    }

    public String getLRemarks() {
        return lRemarks;
    }

    public void setLRemarks(String lRemarks) {
        this.lRemarks = lRemarks;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImplantBrand() {
        return implantBrand;
    }

    public void setImplantBrand(String implantBrand) {
        this.implantBrand = implantBrand;
    }

    public String getImplantName() {
        return implantName;
    }

    public void setImplantName(String implantName) {
        this.implantName = implantName;
    }

    public String getAConstant() {
        return aConstant;
    }

    public void setAConstant(String aConstant) {
        this.aConstant = aConstant;
    }

    public String getImplantPower() {
        return implantPower;
    }

    public void setImplantPower(String implantPower) {
        this.implantPower = implantPower;
    }

    public String getImplantPlacement() {
        return implantPlacement;
    }

    public void setImplantPlacement(String implantPlacement) {
        this.implantPlacement = implantPlacement;
    }

    public String getImplantExpiry() {
        return implantExpiry;
    }

    public void setImplantExpiry(String implantExpiry) {
        this.implantExpiry = implantExpiry;
    }

    public String getImplantSlNo() {
        return implantSlNo;
    }

    public void setImplantSlNo(String implantSlNo) {
        this.implantSlNo = implantSlNo;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getPulse() {
        return pulse;
    }

    public void setPulse(String pulse) {
        this.pulse = pulse;
    }

    public String getBP() {
        return bP;
    }

    public void setBP(String bP) {
        this.bP = bP;
    }

    public String getSPO2() {
        return sPO2;
    }

    public void setSPO2(String sPO2) {
        this.sPO2 = sPO2;
    }

    public String getRBS() {
        return rBS;
    }

    public void setRBS(String rBS) {
        this.rBS = rBS;
    }

    public String getEyeID() {
        return eyeID;
    }

    public void setEyeID(String eyeID) {
        this.eyeID = eyeID;
    }

    public String getAnaesthesiaTypeID() {
        return anaesthesiaTypeID;
    }

    public void setAnaesthesiaTypeID(String anaesthesiaTypeID) {
        this.anaesthesiaTypeID = anaesthesiaTypeID;
    }

    public String getAnaesthesiaMedicine() {
        return anaesthesiaMedicine;
    }

    public void setAnaesthesiaMedicine(String anaesthesiaMedicine) {
        this.anaesthesiaMedicine = anaesthesiaMedicine;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getTPA() {
        return tPA;
    }

    public void setTPA(String tPA) {
        this.tPA = tPA;
    }

    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getSettlementReceived() {
        return settlementReceived;
    }

    public void setSettlementReceived(String settlementReceived) {
        this.settlementReceived = settlementReceived;
    }

    public String getSettlementAmount() {
        return settlementAmount;
    }

    public void setSettlementAmount(String settlementAmount) {
        this.settlementAmount = settlementAmount;
    }

    public String getSurgeryName() {
        return surgeryName;
    }

    public void setSurgeryName(String surgeryName) {
        this.surgeryName = surgeryName;
    }

    public String getEye() {
        return eye;
    }

    public void setEye(String eye) {
        this.eye = eye;
    }

    public String getAnaesthesiaType() {
        return anaesthesiaType;
    }

    public void setAnaesthesiaType(String anaesthesiaType) {
        this.anaesthesiaType = anaesthesiaType;
    }

    public String getPatientFirstName() {
        return patientFirstName;
    }

    public void setPatientFirstName(String patientFirstName) {
        this.patientFirstName = patientFirstName;
    }

    public String getPatientLastName() {
        return patientLastName;
    }

    public void setPatientLastName(String patientLastName) {
        this.patientLastName = patientLastName;
    }

    public String getPatientMobileNo() {
        return patientMobileNo;
    }

    public void setPatientMobileNo(String patientMobileNo) {
        this.patientMobileNo = patientMobileNo;
    }

    public String getPatientCode() {
        return patientCode;
    }

    public void setPatientCode(String patientCode) {
        this.patientCode = patientCode;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getRno() {
        return rno;
    }

    public void setRno(String rno) {
        this.rno = rno;
    }

    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }
    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorName() {
        return DoctorName;
    }

    public void setDoctorName(String doctorName) {
        DoctorName = doctorName;
    }

    public String getPatientID() {
        return PatientID;
    }

    public void setPatientID(String patientID) {
        PatientID = patientID;
    }
}
