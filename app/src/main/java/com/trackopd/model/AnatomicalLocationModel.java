package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AnatomicalLocationModel {

    private String name;
    private Boolean status = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    // Anatomical Location List
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<AnatomicalLocationModel> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AnatomicalLocationModel> getData() {
        return data;
    }

    public void setData(List<AnatomicalLocationModel> data) {
        this.data = data;
    }
    @SerializedName("AnatomicalLocationID")
    @Expose
    private String anatomicalLocationID;
    @SerializedName("CategoryID")
    @Expose
    private String categoryID;
    @SerializedName("Category")
    @Expose
    private String category;
    @SerializedName("AnatomicalLocation")
    @Expose
    private String anatomicalLocation;
    @SerializedName("Status")
    @Expose
    private String status_data;

    public String getAnatomicalLocationID() {
        return anatomicalLocationID;
    }

    public void setAnatomicalLocationID(String anatomicalLocationID) {
        this.anatomicalLocationID = anatomicalLocationID;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAnatomicalLocation() {
        return anatomicalLocation;
    }

    public void setAnatomicalLocation(String anatomicalLocation) {
        this.anatomicalLocation = anatomicalLocation;
    }

    public String getStatus() {
        return status_data;
    }

    public void setStatus(String status) {
        this.status_data = status;
    }
}
