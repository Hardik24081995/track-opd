package com.trackopd.model;

import android.graphics.drawable.Drawable;
import android.net.Uri;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pchmn.materialchips.model.ChipInterface;

import java.util.ArrayList;

public class ChipInputModel implements ChipInterface {

    private String name;

    public ChipInputModel(String name) {
        this.name = name;
    }

    @Override
    public Object getId() {
        return null;
    }

    @Override
    public Uri getAvatarUri() {
        return null;
    }

    @Override
    public Drawable getAvatarDrawable() {
        return null;
    }

    @Override
    public String getLabel() {
        return name;
    }

    @Override
    public String getInfo() {
        return null;
    }
}
