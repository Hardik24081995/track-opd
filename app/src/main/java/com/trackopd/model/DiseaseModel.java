package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DiseaseModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<DiseaseModel> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<DiseaseModel> getData() {
        return data;
    }

    public void setData(ArrayList<DiseaseModel> data) {
        this.data = data;
    }

    @SerializedName("DiseaseID")
    @Expose
    private String diseaseID;
    @SerializedName("AnatomicalLocationID")
    @Expose
    private String anatomicalLocationID;
    @SerializedName("Disease")
    @Expose
    private String disease;
    @SerializedName("AnatomicalLocation")
    @Expose
    private String anatomicalLocation;
    @SerializedName("Status")
    @Expose
    private String status;

    public String getDiseaseID() {
        return diseaseID;
    }

    public void setDiseaseID(String diseaseID) {
        this.diseaseID = diseaseID;
    }

    public String getAnatomicalLocationID() {
        return anatomicalLocationID;
    }

    public void setAnatomicalLocationID(String anatomicalLocationID) {
        this.anatomicalLocationID = anatomicalLocationID;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public String getAnatomicalLocation() {
        return anatomicalLocation;
    }

    public void setAnatomicalLocation(String anatomicalLocation) {
        this.anatomicalLocation = anatomicalLocation;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}


