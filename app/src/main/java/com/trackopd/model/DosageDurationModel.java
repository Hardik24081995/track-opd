package com.trackopd.model;

import java.io.Serializable;

public class DosageDurationModel implements Serializable
{
    String dosage,duration;

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
