package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class checkDoctorAvailabiltyModel {
    @SerializedName("Error")
    @Expose
    private Integer error;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Data> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }
    public class Data {

        @SerializedName("TimeSlot")
        @Expose
        private String timeSlot;
        @SerializedName("Doctor")
        @Expose
        private List<Object> doctor = null;

        public String getTimeSlot() {
            return timeSlot;
        }

        public void setTimeSlot(String timeSlot) {
            this.timeSlot = timeSlot;
        }

        public List<Object> getDoctor() {
            return doctor;
        }

        public void setDoctor(List<Object> doctor) {
            this.doctor = doctor;
        }
    }
}