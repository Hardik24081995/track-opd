package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DoodleImageModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<DoodleImageModel> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<DoodleImageModel> getData() {
        return data;
    }

    public void setData(ArrayList<DoodleImageModel> data) {
        this.data = data;
    }

    @SerializedName("DoodleImageID")
    @Expose
    private String doodleImageID;
    @SerializedName("AppointmentID")
    @Expose
    private String appointmentID;
    @SerializedName("LeftAdnex")
    @Expose
    private String leftAdnex;
    @SerializedName("RightAdnex")
    @Expose
    private String rightAdnex;
    @SerializedName("LeftAnteriorSegment")
    @Expose
    private String leftAnteriorSegment;
    @SerializedName("RightAnteriorSegment")
    @Expose
    private String rightAnteriorSegment;
    @SerializedName("LeftLens")
    @Expose
    private String leftLens;
    @SerializedName("RightLens")
    @Expose
    private String rightLens;
    @SerializedName("LeftFundus")
    @Expose
    private String leftFundus;
    @SerializedName("RightFundus")
    @Expose
    private String rightFundus;
    @SerializedName("LeftGonioscopy")
    @Expose
    private String leftGonioscopy;
    @SerializedName("RightGonioscopy")
    @Expose
    private String rightGonioscopy;
    @SerializedName("Status")
    @Expose
    private String status;

    public String getDoodleImageID() {
        return doodleImageID;
    }

    public void setDoodleImageID(String doodleImageID) {
        this.doodleImageID = doodleImageID;
    }

    public String getAppointmentID() {
        return appointmentID;
    }

    public void setAppointmentID(String appointmentID) {
        this.appointmentID = appointmentID;
    }

    public String getLeftAdnex() {
        return leftAdnex;
    }

    public void setLeftAdnex(String leftAdnex) {
        this.leftAdnex = leftAdnex;
    }

    public String getRightAdnex() {
        return rightAdnex;
    }

    public void setRightAdnex(String rightAdnex) {
        this.rightAdnex = rightAdnex;
    }

    public String getLeftAnteriorSegment() {
        return leftAnteriorSegment;
    }

    public void setLeftAnteriorSegment(String leftAnteriorSegment) {
        this.leftAnteriorSegment = leftAnteriorSegment;
    }

    public String getRightAnteriorSegment() {
        return rightAnteriorSegment;
    }

    public void setRightAnteriorSegment(String rightAnteriorSegment) {
        this.rightAnteriorSegment = rightAnteriorSegment;
    }

    public String getLeftLens() {
        return leftLens;
    }

    public void setLeftLens(String leftLens) {
        this.leftLens = leftLens;
    }

    public String getRightLens() {
        return rightLens;
    }

    public void setRightLens(String rightLens) {
        this.rightLens = rightLens;
    }

    public String getLeftFundus() {
        return leftFundus;
    }

    public void setLeftFundus(String leftFundus) {
        this.leftFundus = leftFundus;
    }

    public String getRightFundus() {
        return rightFundus;
    }

    public void setRightFundus(String rightFundus) {
        this.rightFundus = rightFundus;
    }

    public String getLeftGonioscopy() {
        return leftGonioscopy;
    }

    public void setLeftGonioscopy(String leftGonioscopy) {
        this.leftGonioscopy = leftGonioscopy;
    }

    public String getRightGonioscopy() {
        return rightGonioscopy;
    }

    public void setRightGonioscopy(String rightGonioscopy) {
        this.rightGonioscopy = rightGonioscopy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
