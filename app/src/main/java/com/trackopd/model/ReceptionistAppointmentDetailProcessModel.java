package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReceptionistAppointmentDetailProcessModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("rowCount")
    @Expose
    private String rowCount;
    @SerializedName("data")
    @Expose
    private List<Data> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRowCount() {
        return rowCount;
    }

    public void setRowCount(String rowCount) {
        this.rowCount = rowCount;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("HistoryID")
        @Expose
        private String historyID;
        @SerializedName("FirstName")
        @Expose
        private String firstName;
        @SerializedName("LastName")
        @Expose
        private String lastName;
        @SerializedName("Description")
        @Expose
        private String description;
        @SerializedName("ProcessDateTime")
        @Expose
        private String processDateTime;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;

        public String getHistoryID() {
            return historyID;
        }

        public void setHistoryID(String historyID) {
            this.historyID = historyID;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getProcessDateTime() {
            return processDateTime;
        }

        public void setProcessDateTime(String processDateTime) {
            this.processDateTime = processDateTime;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }
    }
}
