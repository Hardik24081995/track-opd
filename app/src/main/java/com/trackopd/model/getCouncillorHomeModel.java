package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class getCouncillorHomeModel {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Data> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("TotalAppointment")
        @Expose
        private String totalAppointment;
        @SerializedName("FollowUp")
        @Expose
        private String followUp;

        public String getTotalAppointment() {
            return totalAppointment;
        }

        public void setTotalAppointment(String totalAppointment) {
            this.totalAppointment = totalAppointment;
        }

        public String getFollowUp() {
            return followUp;
        }

        public void setFollowUp(String followUp) {
            this.followUp = followUp;
        }

    }
}
