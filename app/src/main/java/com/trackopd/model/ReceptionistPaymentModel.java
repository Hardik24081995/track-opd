package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ReceptionistPaymentModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("rowCount")
    @Expose
    private String rowCount;
    @SerializedName("data")
    @Expose
    private ArrayList<ReceptionistPaymentModel> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRowCount() {
        return rowCount;
    }

    public void setRowCount(String rowCount) {
        this.rowCount = rowCount;
    }

    public ArrayList<ReceptionistPaymentModel> getData() {
        return data;
    }

    public void setData(ArrayList<ReceptionistPaymentModel> data) {
        this.data = data;
    }

    @SerializedName("PaymentHistoryID")
    @Expose
    private String paymentHistoryID;
    @SerializedName("Amount")
    @Expose
    private String amount;
    @SerializedName("AppointmentID")
    @Expose
    private String appointmentID;
    @SerializedName("TicketNumber")
    @Expose
    private String ticketNumber;
    @SerializedName("PatientFirstName")
    @Expose
    private String patientFirstName;
    @SerializedName("PatientLastName")
    @Expose
    private String patientLastName;
    @SerializedName("PatientMobileNo")
    @Expose
    private String patientMobileNo;
    @SerializedName("AppointmentDate")
    @Expose
    private String appointmentDate;
    @SerializedName("PaymentDate")
    @Expose
    private String paymentDate;
    @SerializedName("PatientCode")
    @Expose
    private String patientCode;
    @SerializedName("ProfileImage")
    @Expose
    private String profileImage;
    @SerializedName("OrderNo")
    @Expose
    private String orderNo;
    @SerializedName("CashRs")
    @Expose
    private String cashRs;
    @SerializedName("ChequeRs")
    @Expose
    private String chequeRs;
    @SerializedName("NEFTRs")
    @Expose
    private String nEFTRs;
    @SerializedName("EWalletRs")
    @Expose
    private String eWalletRs;
    @SerializedName("ChequeNo")
    @Expose
    private String chequeNo;
    @SerializedName("IfscCode")
    @Expose
    private String ifscCode;
    @SerializedName("Utr")
    @Expose
    private String utr;
    @SerializedName("BankName")
    @Expose
    private String bankName;
    @SerializedName("Branch")
    @Expose
    private String branch;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("PatientID")
    @Expose
    private String patientID;
    @SerializedName("Rno")
    @Expose
    private String rno;
    @SerializedName("rowcount")
    @Expose
    private String rowcount;
    @SerializedName("CheckUpType")
    @Expose
    private String CheckUpType;

    public String getCheckUpType() {
        return CheckUpType;
    }

    public void setCheckUpType(String checkUpType) {
        CheckUpType = checkUpType;
    }

    public String getPaymentHistoryID() {
        return paymentHistoryID;
    }

    public void setPaymentHistoryID(String paymentHistoryID) {
        this.paymentHistoryID = paymentHistoryID;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAppointmentID() {
        return appointmentID;
    }

    public void setAppointmentID(String appointmentID) {
        this.appointmentID = appointmentID;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getPatientFirstName() {
        return patientFirstName;
    }

    public void setPatientFirstName(String patientFirstName) {
        this.patientFirstName = patientFirstName;
    }

    public String getPatientLastName() {
        return patientLastName;
    }

    public void setPatientLastName(String patientLastName) {
        this.patientLastName = patientLastName;
    }

    public String getPatientMobileNo() {
        return patientMobileNo;
    }

    public void setPatientMobileNo(String patientMobileNo) {
        this.patientMobileNo = patientMobileNo;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPatientCode() {
        return patientCode;
    }

    public void setPatientCode(String patientCode) {
        this.patientCode = patientCode;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getCashRs() {
        return cashRs;
    }

    public void setCashRs(String cashRs) {
        this.cashRs = cashRs;
    }

    public String getChequeRs() {
        return chequeRs;
    }

    public void setChequeRs(String chequeRs) {
        this.chequeRs = chequeRs;
    }

    public String getNEFTRs() {
        return nEFTRs;
    }

    public void setNEFTRs(String nEFTRs) {
        this.nEFTRs = nEFTRs;
    }

    public String getEWalletRs() {
        return eWalletRs;
    }

    public void setEWalletRs(String eWalletRs) {
        this.eWalletRs = eWalletRs;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public void setIfscCode(String ifscCode) {
        this.ifscCode = ifscCode;
    }

    public String getUtr() {
        return utr;
    }

    public void setUtr(String utr) {
        this.utr = utr;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public String getRno() {
        return rno;
    }

    public void setRno(String rno) {
        this.rno = rno;
    }

    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }
}
