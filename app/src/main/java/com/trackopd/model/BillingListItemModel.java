package com.trackopd.model;

import java.io.Serializable;

public class BillingListItemModel implements Serializable {

    public BillingListItemModel(String Name,String Id,String parentId,String Rate,String Type){
        this.Name=Name;
        this.Id=Id;
        this.ParentId=parentId;
        this.Rate=Rate;
        this.Original_Price=Rate;
        this.Type=Type;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getParentId() {
        return ParentId;
    }

    public void setParentId(String parentId) {
        ParentId = parentId;
    }

    public String getRate() {
        return Rate;
    }

    public void setRate(String rate) {
        Rate = rate;
    }
    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }
    public String getOriginal_Price() {
        return Original_Price;
    }

    public void setOriginal_Price(String original_Price) {
        Original_Price = original_Price;
    }

    String Name;
    String Id;
    String ParentId;
    String Rate;
    String Type;
    String Original_Price;
}
