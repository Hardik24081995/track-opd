package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.trackopd.utils.AppConstants;

import java.util.List;

public class AppointmentReasonsModel {

    private int id;
    private String reason;
    private boolean isSelected;

    public AppointmentReasonsModel() {
        id = 0;
        reason = AppConstants.STR_EMPTY_STRING;
        isSelected = false;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public boolean getSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<AppointmentReasonsModel> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AppointmentReasonsModel> getData() {
        return data;
    }

    public void setData(List<AppointmentReasonsModel> data) {
        this.data = data;
    }

    @SerializedName("ReasonID")
    @Expose
    private String reasonID;
    @SerializedName("Reason")
    @Expose
    private String reasons;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Rno")
    @Expose
    private String rno;
    @SerializedName("rowcount")
    @Expose
    private String rowcount;

    public String getReasonID() {
        return reasonID;
    }

    public void setReasonID(String reasonID) {
        this.reasonID = reasonID;
    }

    public String getReasons() {
        return reasons;
    }

    public void setReasons(String reason) {
        this.reasons = reason;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRno() {
        return rno;
    }

    public void setRno(String rno) {
        this.rno = rno;
    }

    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }
}
