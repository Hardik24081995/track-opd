package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AddPackageItemModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<AddPackageItemModel> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<AddPackageItemModel> getData() {
        return data;
    }

    public void setData(ArrayList<AddPackageItemModel> data) {
        this.data = data;
    }

    @SerializedName("BillingID")
    @Expose
    private String billingID;
    @SerializedName("ServicesName")
    @Expose
    private String servicesName;
    @SerializedName("ParentServicesName")
    @Expose
    private String parentServicesName;
    @SerializedName("Rate")
    @Expose
    private String rate;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Rno")
    @Expose
    private String rno;
    @SerializedName("rowcount")
    @Expose
    private String rowcount;

    public String getBillingID() {
        return billingID;
    }

    public void setBillingID(String billingID) {
        this.billingID = billingID;
    }

    public String getServicesName() {
        return servicesName;
    }

    public void setServicesName(String servicesName) {
        this.servicesName = servicesName;
    }

    public String getParentServicesName() {
        return parentServicesName;
    }

    public void setParentServicesName(String parentServicesName) {
        this.parentServicesName = parentServicesName;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRno() {
        return rno;
    }

    public void setRno(String rno) {
        this.rno = rno;
    }

    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }
}
