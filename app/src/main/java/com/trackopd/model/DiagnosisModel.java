package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * This Model Class Use For History
 */
public class DiagnosisModel
{
    @SerializedName("PDiagnosisID")
    @Expose
    private String pDiagnosisID;
    @SerializedName("PreliminaryExaminationID")
    @Expose
    private String preliminaryExaminationID;
    @SerializedName("CategoryID")
    @Expose
    private String categoryID;
    @SerializedName("AnatomicalLocationID")
    @Expose
    private String anatomicalLocationID;
    @SerializedName("EyeID")
    @Expose
    private String eyeID;
    @SerializedName("DiseaseID")
    @Expose
    private String diseaseID;
    @SerializedName("EyeLenseID")
    @Expose
    private String eyeLenseID;
    @SerializedName("CataracttypeID")
    @Expose
    private String cataracttypeID;
    @SerializedName("StageID")
    @Expose
    private String stageID;
    @SerializedName("SideofbrainID")
    @Expose
    private String sideofbrainID;
    @SerializedName("PatterntypeID")
    @Expose
    private String patterntypeID;
    @SerializedName("Category")
    @Expose
    private String category;
    @SerializedName("AnatomicalLocation")
    @Expose
    private String anatomicalLocation;
    @SerializedName("Eye")
    @Expose
    private String eye;
    @SerializedName("EyeLense")
    @Expose
    private String eyeLense;
    @SerializedName("Disease")
    @Expose
    private String disease;
    @SerializedName("Cataracttype")
    @Expose
    private String cataracttype;
    @SerializedName("Stage")
    @Expose
    private String stage;
    @SerializedName("Sideofbrain")
    @Expose
    private String sideofbrain;
    @SerializedName("Patterntype")
    @Expose
    private String patterntype;

    public String getPDiagnosisID() {
        return pDiagnosisID;
    }

    public void setPDiagnosisID(String pDiagnosisID) {
        this.pDiagnosisID = pDiagnosisID;
    }

    public String getPreliminaryExaminationID() {
        return preliminaryExaminationID;
    }

    public void setPreliminaryExaminationID(String preliminaryExaminationID) {
        this.preliminaryExaminationID = preliminaryExaminationID;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getAnatomicalLocationID() {
        return anatomicalLocationID;
    }

    public void setAnatomicalLocationID(String anatomicalLocationID) {
        this.anatomicalLocationID = anatomicalLocationID;
    }

    public String getEyeID() {
        return eyeID;
    }

    public void setEyeID(String eyeID) {
        this.eyeID = eyeID;
    }

    public String getDiseaseID() {
        return diseaseID;
    }

    public void setDiseaseID(String diseaseID) {
        this.diseaseID = diseaseID;
    }

    public String getEyeLenseID() {
        return eyeLenseID;
    }

    public void setEyeLenseID(String eyeLenseID) {
        this.eyeLenseID = eyeLenseID;
    }

    public String getCataracttypeID() {
        return cataracttypeID;
    }

    public void setCataracttypeID(String cataracttypeID) {
        this.cataracttypeID = cataracttypeID;
    }

    public String getStageID() {
        return stageID;
    }

    public void setStageID(String stageID) {
        this.stageID = stageID;
    }

    public String getSideofbrainID() {
        return sideofbrainID;
    }

    public void setSideofbrainID(String sideofbrainID) {
        this.sideofbrainID = sideofbrainID;
    }

    public String getPatterntypeID() {
        return patterntypeID;
    }

    public void setPatterntypeID(String patterntypeID) {
        this.patterntypeID = patterntypeID;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getAnatomicalLocation() {
        return anatomicalLocation;
    }

    public void setAnatomicalLocation(String anatomicalLocation) {
        this.anatomicalLocation = anatomicalLocation;
    }

    public String getEye() {
        return eye;
    }

    public void setEye(String eye) {
        this.eye = eye;
    }

    public String getEyeLense() {
        return eyeLense;
    }

    public void setEyeLense(String eyeLense) {
        this.eyeLense = eyeLense;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public String getCataracttype() {
        return cataracttype;
    }

    public void setCataracttype(String cataracttype) {
        this.cataracttype = cataracttype;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getSideofbrain() {
        return sideofbrain;
    }

    public void setSideofbrain(String sideofbrain) {
        this.sideofbrain = sideofbrain;
    }

    public String getPatterntype() {
        return patterntype;
    }

    public void setPatterntype(String patterntype) {
        this.patterntype = patterntype;
    }
}
