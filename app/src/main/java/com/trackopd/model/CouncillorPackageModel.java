package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CouncillorPackageModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("rowCount")
    @Expose
    private String rowCount;
    @SerializedName("data")
    @Expose
    private ArrayList<CouncillorPackageModel> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRowCount() {
        return rowCount;
    }

    public void setRowCount(String rowCount) {
        this.rowCount = rowCount;
    }

    public ArrayList<CouncillorPackageModel> getData() {
        return data;
    }

    public void setData(ArrayList<CouncillorPackageModel> data) {
        this.data = data;
    }

    //--------  Declare List Package geter setter Value
    @SerializedName("PackageID")
    @Expose
    private String packageID;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Amount")
    @Expose
    private String amount;
    @SerializedName("DiscountAmount")
    @Expose
    private String discountAmount;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("PackageDate")
    @Expose
    private String packageDate;
    @SerializedName("Rno")
    @Expose
    private String rno;
    @SerializedName("rowcount")
    @Expose
    private String rowcount;
    @SerializedName("Item")
    @Expose
    private ArrayList<Item> item = null;

    public String getPackageID() {
        return packageID;
    }

    public void setPackageID(String packageID) {
        this.packageID = packageID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPackageDate() {
        return packageDate;
    }

    public void setPackageDate(String packageDate) {
        this.packageDate = packageDate;
    }

    public String getRno() {
        return rno;
    }

    public void setRno(String rno) {
        this.rno = rno;
    }

    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }

    public ArrayList<Item> getItem() {
        return item;
    }

    public void setItem(ArrayList<Item> item) {
        this.item = item;
    }

    public static class Item {

        @SerializedName("PacakageItemID")
        @Expose
        private String pacakageItemID;
        @SerializedName("Title")
        @Expose
        private String title;
        @SerializedName("Amount")
        @Expose
        private String amount;
        @SerializedName("Status")
        @Expose
        private String status;

        public String getPacakageItemID() {
            return pacakageItemID;
        }

        public void setPacakageItemID(String pacakageItemID) {
            this.pacakageItemID = pacakageItemID;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
