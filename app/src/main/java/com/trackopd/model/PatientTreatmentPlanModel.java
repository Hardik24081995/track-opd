package com.trackopd.model;

import java.util.ArrayList;
import java.util.List;

public class PatientTreatmentPlanModel {

    private String id;
    private String medicineName;
    private String lastVisitedDate;
    private String treatmentName;
    private List<PatientTreatmentPlanMedicine> listPatientTreatmentPlanMedicine;

    public PatientTreatmentPlanModel(String _id, String treatment_name, String _lastVisited, ArrayList<PatientTreatmentPlanMedicine> mArrPatientTreatmentMedicine) {
        this.id = _id;
        this.treatmentName = treatment_name;
        this.lastVisitedDate = _lastVisited;
        this.listPatientTreatmentPlanMedicine = mArrPatientTreatmentMedicine;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMedicineName() {
        return medicineName;
    }

    public void setMedicineName(String medicineName) {
        this.medicineName = medicineName;
    }

    public String getLastVisitedDate() {
        return lastVisitedDate;
    }

    public void setLastVisitedDate(String lastVisitedDate) {
        this.lastVisitedDate = lastVisitedDate;
    }

    public List<PatientTreatmentPlanMedicine> getListPatientTreatmentPlanMedicine() {
        return listPatientTreatmentPlanMedicine;
    }

    public void setListPatientTreatmentPlanMedicine(List<PatientTreatmentPlanMedicine> listPatientTreatmentPlanMedicine) {
        this.listPatientTreatmentPlanMedicine = listPatientTreatmentPlanMedicine;
    }

    public PatientTreatmentPlanModel(String id, String medicineName) {
        this.id = id;
        this.medicineName = medicineName;
    }

    public String getTreatmentName() {
        return treatmentName;
    }

    public void setTreatmentName(String treatmentName) {
        this.treatmentName = treatmentName;
    }

    //Medicine List
    public static class PatientTreatmentPlanMedicine {
        String medicineName;
        String morning_dosage;
        String afternoon_dosage;
        String evening_dosage;

        public String getMedicineName() {
            return medicineName;
        }

        public void setMedicineName(String medicineName) {
            this.medicineName = medicineName;
        }

        public String getMorning_dosage() {
            return morning_dosage;
        }

        public void setMorning_dosage(String morning_dosage) {
            this.morning_dosage = morning_dosage;
        }

        public String getAfternoon_dosage() {
            return afternoon_dosage;
        }

        public void setAfternoon_dosage(String afternoon_dosage) {
            this.afternoon_dosage = afternoon_dosage;
        }

        public String getEvening_dosage() {
            return evening_dosage;
        }

        public void setEvening_dosage(String evening_dosage) {
            this.evening_dosage = evening_dosage;
        }
    }
}
