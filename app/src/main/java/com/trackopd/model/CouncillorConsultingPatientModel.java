package com.trackopd.model;

public class CouncillorConsultingPatientModel {

    private String id;
    private String profilePic;
    private String firstName;
    private String lastName;
    private String mobileNo;
    private String patientCode;
    private String lastVisitedDate;

    public CouncillorConsultingPatientModel() {

    }

    public CouncillorConsultingPatientModel(String id, String profilePic, String firstName, String lastName, String mobileNo, String patientCode, String lastVisitedDate) {
        this.id = id;
        this.profilePic = profilePic;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mobileNo = mobileNo;
        this.patientCode = patientCode;
        this.lastVisitedDate = lastVisitedDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPatientCode() {
        return patientCode;
    }

    public void setPatientCode(String patientCode) {
        this.patientCode = patientCode;
    }

    public String getLastVisitedDate() {
        return lastVisitedDate;
    }

    public void setLastVisitedDate(String lastVisitedDate) {
        this.lastVisitedDate = lastVisitedDate;
    }
}
