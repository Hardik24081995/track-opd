package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReceptionistPatientDetailDocumentModel {


    @SerializedName("data")
    @Expose
    private List<ReceptionistPatientDetailDocumentModel> data = null;
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;

    public List<ReceptionistPatientDetailDocumentModel> getData() {
        return data;
    }

    public void setData(List<ReceptionistPatientDetailDocumentModel> data) {
        this.data = data;
    }

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @SerializedName("AppointmentID")
    @Expose
    private String appointmentID;
    @SerializedName("MyMedicalReportID")
    @Expose
    private String myMedicalReportID;
    @SerializedName("Report")
    @Expose
    private String report;

    public String getAppointmentID() {
        return appointmentID;
    }

    public void setAppointmentID(String appointmentID) {
        this.appointmentID = appointmentID;
    }

    public String getMyMedicalReportID() {
        return myMedicalReportID;
    }

    public void setMyMedicalReportID(String myMedicalReportID) {
        this.myMedicalReportID = myMedicalReportID;
    }

    public String getReport() {
        return report;
    }

    public void setReport(String report) {
        this.report = report;
    }
}
