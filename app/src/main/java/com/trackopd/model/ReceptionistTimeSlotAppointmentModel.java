package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class ReceptionistTimeSlotAppointmentModel implements Serializable {
    @SerializedName("Error")
    @Expose
    private Integer error;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Data> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public class Data implements Serializable {

        @SerializedName("TimeSlot")
        @Expose
        private String timeSlot;
        @SerializedName("Appointment")
        @Expose
        private List<ReceptionistAppointmentModel> appointment = null;

        public String getTimeSlot() {
            return timeSlot;
        }

        public void setTimeSlot(String timeSlot) {
            this.timeSlot = timeSlot;
        }

        public List<ReceptionistAppointmentModel> getAppointment() {
            return appointment;
        }

        public void setAppointment(List<ReceptionistAppointmentModel> appointment) {
            this.appointment = appointment;
        }
    }

   /* public class Appointment implements Serializable {

        @SerializedName("AppointmentID")
        @Expose
        private String appointmentID;
        @SerializedName("PatientFirstName")
        @Expose
        private String patientFirstName;
        @SerializedName("PatientLastName")
        @Expose
        private String patientLastName;
        @SerializedName("PatientMobileNo")
        @Expose
        private String patientMobileNo;
        @SerializedName("DoctorFirstName")
        @Expose
        private String doctorFirstName;
        @SerializedName("DoctorLastName")
        @Expose
        private String doctorLastName;
        @SerializedName("TicketNumber")
        @Expose
        private String ticketNumber;
        @SerializedName("AppointmentDate")
        @Expose
        private String appointmentDate;
        @SerializedName("PatientCode")
        @Expose
        private String patientCode;
        @SerializedName("AppointmentStatus")
        @Expose
        private String appointmentStatus;
        @SerializedName("PaymentStatus")
        @Expose
        private String paymentStatus;
        @SerializedName("AppointmentType")
        @Expose
        private String appointmentType;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("CurrentStatus")
        @Expose
        private String currentStatus;
        @SerializedName("AsigneeName")
        @Expose
        private String asigneeName;
        @SerializedName("PatientID")
        @Expose
        private String patientID;
        @SerializedName("Reason")
        @Expose
        private String reason;
        @SerializedName("Document")
        @Expose
        private String document;
        @SerializedName("ReasonID")
        @Expose
        private String reasonID;
        @SerializedName("StatusTitle")
        @Expose
        private String statusTitle;
        @SerializedName("DoctorID")
        @Expose
        private String doctorID;
        @SerializedName("AppointmentLocation")
        @Expose
        private String appointmentLocation;
        @SerializedName("Location")
        @Expose
        private String location;
        @SerializedName("MRDNo")
        @Expose
        private String mRDNo;
        @SerializedName("CheckINDate")
        @Expose
        private String checkINDate;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;

        public String getAppointmentID() {
            return appointmentID;
        }

        public void setAppointmentID(String appointmentID) {
            this.appointmentID = appointmentID;
        }

        public String getPatientFirstName() {
            return patientFirstName;
        }

        public void setPatientFirstName(String patientFirstName) {
            this.patientFirstName = patientFirstName;
        }

        public String getPatientLastName() {
            return patientLastName;
        }

        public void setPatientLastName(String patientLastName) {
            this.patientLastName = patientLastName;
        }

        public String getPatientMobileNo() {
            return patientMobileNo;
        }

        public void setPatientMobileNo(String patientMobileNo) {
            this.patientMobileNo = patientMobileNo;
        }

        public String getDoctorFirstName() {
            return doctorFirstName;
        }

        public void setDoctorFirstName(String doctorFirstName) {
            this.doctorFirstName = doctorFirstName;
        }

        public String getDoctorLastName() {
            return doctorLastName;
        }

        public void setDoctorLastName(String doctorLastName) {
            this.doctorLastName = doctorLastName;
        }

        public String getTicketNumber() {
            return ticketNumber;
        }

        public void setTicketNumber(String ticketNumber) {
            this.ticketNumber = ticketNumber;
        }

        public String getAppointmentDate() {
            return appointmentDate;
        }

        public void setAppointmentDate(String appointmentDate) {
            this.appointmentDate = appointmentDate;
        }

        public String getPatientCode() {
            return patientCode;
        }

        public void setPatientCode(String patientCode) {
            this.patientCode = patientCode;
        }

        public String getAppointmentStatus() {
            return appointmentStatus;
        }

        public void setAppointmentStatus(String appointmentStatus) {
            this.appointmentStatus = appointmentStatus;
        }

        public String getPaymentStatus() {
            return paymentStatus;
        }

        public void setPaymentStatus(String paymentStatus) {
            this.paymentStatus = paymentStatus;
        }

        public String getAppointmentType() {
            return appointmentType;
        }

        public void setAppointmentType(String appointmentType) {
            this.appointmentType = appointmentType;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCurrentStatus() {
            return currentStatus;
        }

        public void setCurrentStatus(String currentStatus) {
            this.currentStatus = currentStatus;
        }

        public String getAsigneeName() {
            return asigneeName;
        }

        public void setAsigneeName(String asigneeName) {
            this.asigneeName = asigneeName;
        }

        public String getPatientID() {
            return patientID;
        }

        public void setPatientID(String patientID) {
            this.patientID = patientID;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getDocument() {
            return document;
        }

        public void setDocument(String document) {
            this.document = document;
        }

        public String getReasonID() {
            return reasonID;
        }

        public void setReasonID(String reasonID) {
            this.reasonID = reasonID;
        }

        public String getStatusTitle() {
            return statusTitle;
        }

        public void setStatusTitle(String statusTitle) {
            this.statusTitle = statusTitle;
        }

        public String getDoctorID() {
            return doctorID;
        }

        public void setDoctorID(String doctorID) {
            this.doctorID = doctorID;
        }

        public String getAppointmentLocation() {
            return appointmentLocation;
        }

        public void setAppointmentLocation(String appointmentLocation) {
            this.appointmentLocation = appointmentLocation;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getMRDNo() {
            return mRDNo;
        }

        public void setMRDNo(String mRDNo) {
            this.mRDNo = mRDNo;
        }

        public String getCheckINDate() {
            return checkINDate;
        }

        public void setCheckINDate(String checkINDate) {
            this.checkINDate = checkINDate;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }
    }*/
}
