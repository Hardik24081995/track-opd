package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PreliminaryExaminationModel implements Serializable {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("rowCount")
    @Expose
    private String rowCount;
    @SerializedName("data")
    @Expose
    private ArrayList<PreliminaryExaminationModel> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRowCount() {
        return rowCount;
    }

    public void setRowCount(String rowCount) {
        this.rowCount = rowCount;
    }

    public ArrayList<PreliminaryExaminationModel> getData() {
        return data;
    }

    public void setData(ArrayList<PreliminaryExaminationModel> data) {
        this.data = data;
    }

    @SerializedName("PreliminaryExaminationID")
    @Expose
    private String preliminaryExaminationID;
    @SerializedName("PatientFirstName")
    @Expose
    private String patientFirstName;
    @SerializedName("PatientLastName")
    @Expose
    private String patientLastName;
    @SerializedName("PatientMobileNo")
    @Expose
    private String patientMobileNo;
    @SerializedName("DoctorFirstName")
    @Expose
    private String doctorFirstName;
    @SerializedName("DoctorLastName")
    @Expose
    private String doctorLastName;
    @SerializedName("TreatmentDate")
    @Expose
    private String treatmentDate;
    @SerializedName("PatientID")
    @Expose
    private String patientID;
    @SerializedName("DoctorID")
    @Expose
    private String doctorID;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("AppointmentID")
    @Expose
    private String appointmentID;
    @SerializedName("TicketNumber")
    @Expose
    private String ticketNumber;
    @SerializedName("AppointmentDate")
    @Expose
    private String appointmentDate;
    @SerializedName("PatientCode")
    @Expose
    private String patientCode;
    @SerializedName("PrimaryComplains")
    @Expose
    private String primaryComplains;
    @SerializedName("OcularHistory")
    @Expose
    private String ocularHistory;
    @SerializedName("SystemicHistory")
    @Expose
    private String systemicHistory;
    @SerializedName("FamilyOcularHistory")
    @Expose
    private String familyOcularHistory;
    @SerializedName("UCVADR")
    @Expose
    private String uCVADR;
    @SerializedName("UCVADL")
    @Expose
    private String uCVADL;
    @SerializedName("UCVANR")
    @Expose
    private String uCVANR;
    @SerializedName("UCVANL")
    @Expose
    private String uCVANL;
    @SerializedName("UCVARemarks")
    @Expose
    private String uCVARemarks;
    @SerializedName("BCVAUDR")
    @Expose
    private String bCVAUDR;
    @SerializedName("BCVAUNR")
    @Expose
    private String bCVAUNR;
    @SerializedName("BCVAUDL")
    @Expose
    private String bCVAUDL;
    @SerializedName("BCVAUNL")
    @Expose
    private String bCVAUNL;
    @SerializedName("BCVADDR")
    @Expose
    private String bCVADDR;
    @SerializedName("BCVADNR")
    @Expose
    private String bCVADNR;
    @SerializedName("BCVADDL")
    @Expose
    private String bCVADDL;
    @SerializedName("BCVADNL")
    @Expose
    private String bCVADNL;
    @SerializedName("BCVARemarks")
    @Expose
    private String bCVARemarks;
    @SerializedName("RefUDRSph")
    @Expose
    private String refUDRSph;
    @SerializedName("RefUDLSph")
    @Expose
    private String refUDLSph;
    @SerializedName("RefUNRSph")
    @Expose
    private String refUNRSph;
    @SerializedName("RefUNLSph")
    @Expose
    private String refUNLSph;
    @SerializedName("RefUDRCyl")
    @Expose
    private String refUDRCyl;
    @SerializedName("RefUNRCyl")
    @Expose
    private String refUNRCyl;
    @SerializedName("RefUDLCyl")
    @Expose
    private String refUDLCyl;
    @SerializedName("RefUNLCyl")
    @Expose
    private String refUNLCyl;
    @SerializedName("RefUDRAxis")
    @Expose
    private String refUDRAxis;
    @SerializedName("RefUDLAxis")
    @Expose
    private String refUDLAxis;
    @SerializedName("RefUNRAxis")
    @Expose
    private String refUNRAxis;
    @SerializedName("RefUNLAxis")
    @Expose
    private String refUNLAxis;
    @SerializedName("RefUDRVA")
    @Expose
    private String refUDRVA;
    @SerializedName("RefUDLVA")
    @Expose
    private String refUDLVA;
    @SerializedName("RefUNRVA")
    @Expose
    private String refUNRVA;
    @SerializedName("RefUNLVA")
    @Expose
    private String refUNLVA;
    @SerializedName("RefDDRSph")
    @Expose
    private String refDDRSph;
    @SerializedName("RefDDLSph")
    @Expose
    private String refDDLSph;
    @SerializedName("RefDNRSph")
    @Expose
    private String refDNRSph;
    @SerializedName("RefDNLSph")
    @Expose
    private String refDNLSph;
    @SerializedName("RefDDRCyl")
    @Expose
    private String refDDRCyl;
    @SerializedName("RefDNRCyl")
    @Expose
    private String refDNRCyl;
    @SerializedName("RefDDLCyl")
    @Expose
    private String refDDLCyl;
    @SerializedName("RefDNLCyl")
    @Expose
    private String refDNLCyl;
    @SerializedName("RefDDRAxis")
    @Expose
    private String refDDRAxis;
    @SerializedName("RefDDLAxis")
    @Expose
    private String refDDLAxis;
    @SerializedName("RefDNRAxis")
    @Expose
    private String refDNRAxis;
    @SerializedName("RefDNLAxis")
    @Expose
    private String refDNLAxis;
    @SerializedName("RefDDRVA")
    @Expose
    private String refDDRVA;
    @SerializedName("RefDDLVA")
    @Expose
    private String refDDLVA;
    @SerializedName("RefDNRVA")
    @Expose
    private String refDNRVA;
    @SerializedName("RefDNLVA")
    @Expose
    private String refDNLVA;
    @SerializedName("RefFDRSph")
    @Expose
    private String refFDRSph;
    @SerializedName("RefFDLSph")
    @Expose
    private String refFDLSph;
    @SerializedName("RefFNRSph")
    @Expose
    private String refFNRSph;
    @SerializedName("RefFNLSph")
    @Expose
    private String refFNLSph;
    @SerializedName("RefFDRCyl")
    @Expose
    private String refFDRCyl;
    @SerializedName("RefFNRCyl")
    @Expose
    private String refFNRCyl;
    @SerializedName("RefFDLCyl")
    @Expose
    private String refFDLCyl;
    @SerializedName("RefFNLCyl")
    @Expose
    private String refFNLCyl;
    @SerializedName("RefFDRAxis")
    @Expose
    private String refFDRAxis;
    @SerializedName("RefFDLAxis")
    @Expose
    private String refFDLAxis;
    @SerializedName("RefFNRAxis")
    @Expose
    private String refFNRAxis;
    @SerializedName("RefFNLAxis")
    @Expose
    private String refFNLAxis;
    @SerializedName("RefFDRVA")
    @Expose
    private String refFDRVA;
    @SerializedName("RefFDLVA")
    @Expose
    private String refFDLVA;
    @SerializedName("RefFNRVA")
    @Expose
    private String refFNRVA;
    @SerializedName("RefFNLVA")
    @Expose
    private String refFNLVA;
    @SerializedName("IPD")
    @Expose
    private String iPD;
    @SerializedName("K1RPower")
    @Expose
    private String k1RPower;
    @SerializedName("K1RAxis")
    @Expose
    private String k1RAxis;
    @SerializedName("K1LPower")
    @Expose
    private String k1LPower;
    @SerializedName("K1LAxis")
    @Expose
    private String k1LAxis;
    @SerializedName("K2RPower")
    @Expose
    private String k2RPower;
    @SerializedName("K2RAxis")
    @Expose
    private String k2RAxis;
    @SerializedName("K2LPower")
    @Expose
    private String k2LPower;
    @SerializedName("K2LAxis")
    @Expose
    private String k2LAxis;
    @SerializedName("RNCT")
    @Expose
    private String rNCT;
    @SerializedName("LNCT")
    @Expose
    private String lNCT;
    @SerializedName("RAT")
    @Expose
    private String rAT;
    @SerializedName("LAT")
    @Expose
    private String lAT;
    @SerializedName("RPachymetry")
    @Expose
    private String rPachymetry;
    @SerializedName("LPachymetry")
    @Expose
    private String lPachymetry;
    @SerializedName("RSchirmer")
    @Expose
    private String rSchirmer;
    @SerializedName("LSchirmer")
    @Expose
    private String lSchirmer;
    @SerializedName("Rno")
    @Expose
    private String rno;
    @SerializedName("rowcount")
    @Expose
    private String rowcount;
    @SerializedName("medication")
    @Expose
    private List<Medication> medication = null;

    @SerializedName("DiagnosisModel")
    @Expose
    private List<Diagnosis> diagnosis = null;

    public String getPreliminaryExaminationID() {
        return preliminaryExaminationID;
    }

    public void setPreliminaryExaminationID(String preliminaryExaminationID) {
        this.preliminaryExaminationID = preliminaryExaminationID;
    }

    public String getPatientFirstName() {
        return patientFirstName;
    }

    public void setPatientFirstName(String patientFirstName) {
        this.patientFirstName = patientFirstName;
    }

    public String getPatientLastName() {
        return patientLastName;
    }

    public void setPatientLastName(String patientLastName) {
        this.patientLastName = patientLastName;
    }

    public String getPatientMobileNo() {
        return patientMobileNo;
    }

    public void setPatientMobileNo(String patientMobileNo) {
        this.patientMobileNo = patientMobileNo;
    }

    public String getDoctorFirstName() {
        return doctorFirstName;
    }

    public void setDoctorFirstName(String doctorFirstName) {
        this.doctorFirstName = doctorFirstName;
    }

    public String getDoctorLastName() {
        return doctorLastName;
    }

    public void setDoctorLastName(String doctorLastName) {
        this.doctorLastName = doctorLastName;
    }

    public String getTreatmentDate() {
        return treatmentDate;
    }

    public void setTreatmentDate(String treatmentDate) {
        this.treatmentDate = treatmentDate;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public String getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(String doctorID) {
        this.doctorID = doctorID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAppointmentID() {
        return appointmentID;
    }

    public void setAppointmentID(String appointmentID) {
        this.appointmentID = appointmentID;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getPatientCode() {
        return patientCode;
    }

    public void setPatientCode(String patientCode) {
        this.patientCode = patientCode;
    }

    public String getPrimaryComplains() {
        return primaryComplains;
    }

    public void setPrimaryComplains(String primaryComplains) {
        this.primaryComplains = primaryComplains;
    }

    public String getOcularHistory() {
        return ocularHistory;
    }

    public void setOcularHistory(String ocularHistory) {
        this.ocularHistory = ocularHistory;
    }

    public String getSystemicHistory() {
        return systemicHistory;
    }

    public void setSystemicHistory(String systemicHistory) {
        this.systemicHistory = systemicHistory;
    }

    public String getFamilyOcularHistory() {
        return familyOcularHistory;
    }

    public void setFamilyOcularHistory(String familyOcularHistory) {
        this.familyOcularHistory = familyOcularHistory;
    }

    public String getUCVADR() {
        return uCVADR;
    }

    public void setUCVADR(String uCVADR) {
        this.uCVADR = uCVADR;
    }

    public String getUCVADL() {
        return uCVADL;
    }

    public void setUCVADL(String uCVADL) {
        this.uCVADL = uCVADL;
    }

    public String getUCVANR() {
        return uCVANR;
    }

    public void setUCVANR(String uCVANR) {
        this.uCVANR = uCVANR;
    }

    public String getUCVANL() {
        return uCVANL;
    }

    public void setUCVANL(String uCVANL) {
        this.uCVANL = uCVANL;
    }

    public String getUCVARemarks() {
        return uCVARemarks;
    }

    public void setUCVARemarks(String uCVARemarks) {
        this.uCVARemarks = uCVARemarks;
    }

    public String getBCVAUDR() {
        return bCVAUDR;
    }

    public void setBCVAUDR(String bCVAUDR) {
        this.bCVAUDR = bCVAUDR;
    }

    public String getBCVAUNR() {
        return bCVAUNR;
    }

    public void setBCVAUNR(String bCVAUNR) {
        this.bCVAUNR = bCVAUNR;
    }

    public String getBCVAUDL() {
        return bCVAUDL;
    }

    public void setBCVAUDL(String bCVAUDL) {
        this.bCVAUDL = bCVAUDL;
    }

    public String getBCVAUNL() {
        return bCVAUNL;
    }

    public void setBCVAUNL(String bCVAUNL) {
        this.bCVAUNL = bCVAUNL;
    }

    public String getBCVADDR() {
        return bCVADDR;
    }

    public void setBCVADDR(String bCVADDR) {
        this.bCVADDR = bCVADDR;
    }

    public String getBCVADNR() {
        return bCVADNR;
    }

    public void setBCVADNR(String bCVADNR) {
        this.bCVADNR = bCVADNR;
    }

    public String getBCVADDL() {
        return bCVADDL;
    }

    public void setBCVADDL(String bCVADDL) {
        this.bCVADDL = bCVADDL;
    }

    public String getBCVADNL() {
        return bCVADNL;
    }

    public void setBCVADNL(String bCVADNL) {
        this.bCVADNL = bCVADNL;
    }

    public String getBCVARemarks() {
        return bCVARemarks;
    }

    public void setBCVARemarks(String bCVARemarks) {
        this.bCVARemarks = bCVARemarks;
    }

    public String getRefUDRSph() {
        return refUDRSph;
    }

    public void setRefUDRSph(String refUDRSph) {
        this.refUDRSph = refUDRSph;
    }

    public String getRefUDLSph() {
        return refUDLSph;
    }

    public void setRefUDLSph(String refUDLSph) {
        this.refUDLSph = refUDLSph;
    }

    public String getRefUNRSph() {
        return refUNRSph;
    }

    public void setRefUNRSph(String refUNRSph) {
        this.refUNRSph = refUNRSph;
    }

    public String getRefUNLSph() {
        return refUNLSph;
    }

    public void setRefUNLSph(String refUNLSph) {
        this.refUNLSph = refUNLSph;
    }

    public String getRefUDRCyl() {
        return refUDRCyl;
    }

    public void setRefUDRCyl(String refUDRCyl) {
        this.refUDRCyl = refUDRCyl;
    }

    public String getRefUNRCyl() {
        return refUNRCyl;
    }

    public void setRefUNRCyl(String refUNRCyl) {
        this.refUNRCyl = refUNRCyl;
    }

    public String getRefUDLCyl() {
        return refUDLCyl;
    }

    public void setRefUDLCyl(String refUDLCyl) {
        this.refUDLCyl = refUDLCyl;
    }

    public String getRefUNLCyl() {
        return refUNLCyl;
    }

    public void setRefUNLCyl(String refUNLCyl) {
        this.refUNLCyl = refUNLCyl;
    }

    public String getRefUDRAxis() {
        return refUDRAxis;
    }

    public void setRefUDRAxis(String refUDRAxis) {
        this.refUDRAxis = refUDRAxis;
    }

    public String getRefUDLAxis() {
        return refUDLAxis;
    }

    public void setRefUDLAxis(String refUDLAxis) {
        this.refUDLAxis = refUDLAxis;
    }

    public String getRefUNRAxis() {
        return refUNRAxis;
    }

    public void setRefUNRAxis(String refUNRAxis) {
        this.refUNRAxis = refUNRAxis;
    }

    public String getRefUNLAxis() {
        return refUNLAxis;
    }

    public void setRefUNLAxis(String refUNLAxis) {
        this.refUNLAxis = refUNLAxis;
    }

    public String getRefUDRVA() {
        return refUDRVA;
    }

    public void setRefUDRVA(String refUDRVA) {
        this.refUDRVA = refUDRVA;
    }

    public String getRefUDLVA() {
        return refUDLVA;
    }

    public void setRefUDLVA(String refUDLVA) {
        this.refUDLVA = refUDLVA;
    }

    public String getRefUNRVA() {
        return refUNRVA;
    }

    public void setRefUNRVA(String refUNRVA) {
        this.refUNRVA = refUNRVA;
    }

    public String getRefUNLVA() {
        return refUNLVA;
    }

    public void setRefUNLVA(String refUNLVA) {
        this.refUNLVA = refUNLVA;
    }

    public String getRefDDRSph() {
        return refDDRSph;
    }

    public void setRefDDRSph(String refDDRSph) {
        this.refDDRSph = refDDRSph;
    }

    public String getRefDDLSph() {
        return refDDLSph;
    }

    public void setRefDDLSph(String refDDLSph) {
        this.refDDLSph = refDDLSph;
    }

    public String getRefDNRSph() {
        return refDNRSph;
    }

    public void setRefDNRSph(String refDNRSph) {
        this.refDNRSph = refDNRSph;
    }

    public String getRefDNLSph() {
        return refDNLSph;
    }

    public void setRefDNLSph(String refDNLSph) {
        this.refDNLSph = refDNLSph;
    }

    public String getRefDDRCyl() {
        return refDDRCyl;
    }

    public void setRefDDRCyl(String refDDRCyl) {
        this.refDDRCyl = refDDRCyl;
    }

    public String getRefDNRCyl() {
        return refDNRCyl;
    }

    public void setRefDNRCyl(String refDNRCyl) {
        this.refDNRCyl = refDNRCyl;
    }

    public String getRefDDLCyl() {
        return refDDLCyl;
    }

    public void setRefDDLCyl(String refDDLCyl) {
        this.refDDLCyl = refDDLCyl;
    }

    public String getRefDNLCyl() {
        return refDNLCyl;
    }

    public void setRefDNLCyl(String refDNLCyl) {
        this.refDNLCyl = refDNLCyl;
    }

    public String getRefDDRAxis() {
        return refDDRAxis;
    }

    public void setRefDDRAxis(String refDDRAxis) {
        this.refDDRAxis = refDDRAxis;
    }

    public String getRefDDLAxis() {
        return refDDLAxis;
    }

    public void setRefDDLAxis(String refDDLAxis) {
        this.refDDLAxis = refDDLAxis;
    }

    public String getRefDNRAxis() {
        return refDNRAxis;
    }

    public void setRefDNRAxis(String refDNRAxis) {
        this.refDNRAxis = refDNRAxis;
    }

    public String getRefDNLAxis() {
        return refDNLAxis;
    }

    public void setRefDNLAxis(String refDNLAxis) {
        this.refDNLAxis = refDNLAxis;
    }

    public String getRefDDRVA() {
        return refDDRVA;
    }

    public void setRefDDRVA(String refDDRVA) {
        this.refDDRVA = refDDRVA;
    }

    public String getRefDDLVA() {
        return refDDLVA;
    }

    public void setRefDDLVA(String refDDLVA) {
        this.refDDLVA = refDDLVA;
    }

    public String getRefDNRVA() {
        return refDNRVA;
    }

    public void setRefDNRVA(String refDNRVA) {
        this.refDNRVA = refDNRVA;
    }

    public String getRefDNLVA() {
        return refDNLVA;
    }

    public void setRefDNLVA(String refDNLVA) {
        this.refDNLVA = refDNLVA;
    }

    public String getRefFDRSph() {
        return refFDRSph;
    }

    public void setRefFDRSph(String refFDRSph) {
        this.refFDRSph = refFDRSph;
    }

    public String getRefFDLSph() {
        return refFDLSph;
    }

    public void setRefFDLSph(String refFDLSph) {
        this.refFDLSph = refFDLSph;
    }

    public String getRefFNRSph() {
        return refFNRSph;
    }

    public void setRefFNRSph(String refFNRSph) {
        this.refFNRSph = refFNRSph;
    }

    public String getRefFNLSph() {
        return refFNLSph;
    }

    public void setRefFNLSph(String refFNLSph) {
        this.refFNLSph = refFNLSph;
    }

    public String getRefFDRCyl() {
        return refFDRCyl;
    }

    public void setRefFDRCyl(String refFDRCyl) {
        this.refFDRCyl = refFDRCyl;
    }

    public String getRefFNRCyl() {
        return refFNRCyl;
    }

    public void setRefFNRCyl(String refFNRCyl) {
        this.refFNRCyl = refFNRCyl;
    }

    public String getRefFDLCyl() {
        return refFDLCyl;
    }

    public void setRefFDLCyl(String refFDLCyl) {
        this.refFDLCyl = refFDLCyl;
    }

    public String getRefFNLCyl() {
        return refFNLCyl;
    }

    public void setRefFNLCyl(String refFNLCyl) {
        this.refFNLCyl = refFNLCyl;
    }

    public String getRefFDRAxis() {
        return refFDRAxis;
    }

    public void setRefFDRAxis(String refFDRAxis) {
        this.refFDRAxis = refFDRAxis;
    }

    public String getRefFDLAxis() {
        return refFDLAxis;
    }

    public void setRefFDLAxis(String refFDLAxis) {
        this.refFDLAxis = refFDLAxis;
    }

    public String getRefFNRAxis() {
        return refFNRAxis;
    }

    public void setRefFNRAxis(String refFNRAxis) {
        this.refFNRAxis = refFNRAxis;
    }

    public String getRefFNLAxis() {
        return refFNLAxis;
    }

    public void setRefFNLAxis(String refFNLAxis) {
        this.refFNLAxis = refFNLAxis;
    }

    public String getRefFDRVA() {
        return refFDRVA;
    }

    public void setRefFDRVA(String refFDRVA) {
        this.refFDRVA = refFDRVA;
    }

    public String getRefFDLVA() {
        return refFDLVA;
    }

    public void setRefFDLVA(String refFDLVA) {
        this.refFDLVA = refFDLVA;
    }

    public String getRefFNRVA() {
        return refFNRVA;
    }

    public void setRefFNRVA(String refFNRVA) {
        this.refFNRVA = refFNRVA;
    }

    public String getRefFNLVA() {
        return refFNLVA;
    }

    public void setRefFNLVA(String refFNLVA) {
        this.refFNLVA = refFNLVA;
    }

    public String getIPD() {
        return iPD;
    }

    public void setIPD(String iPD) {
        this.iPD = iPD;
    }

    public String getK1RPower() {
        return k1RPower;
    }

    public void setK1RPower(String k1RPower) {
        this.k1RPower = k1RPower;
    }

    public String getK1RAxis() {
        return k1RAxis;
    }

    public void setK1RAxis(String k1RAxis) {
        this.k1RAxis = k1RAxis;
    }

    public String getK1LPower() {
        return k1LPower;
    }

    public void setK1LPower(String k1LPower) {
        this.k1LPower = k1LPower;
    }

    public String getK1LAxis() {
        return k1LAxis;
    }

    public void setK1LAxis(String k1LAxis) {
        this.k1LAxis = k1LAxis;
    }

    public String getK2RPower() {
        return k2RPower;
    }

    public void setK2RPower(String k2RPower) {
        this.k2RPower = k2RPower;
    }

    public String getK2RAxis() {
        return k2RAxis;
    }

    public void setK2RAxis(String k2RAxis) {
        this.k2RAxis = k2RAxis;
    }

    public String getK2LPower() {
        return k2LPower;
    }

    public void setK2LPower(String k2LPower) {
        this.k2LPower = k2LPower;
    }

    public String getK2LAxis() {
        return k2LAxis;
    }

    public void setK2LAxis(String k2LAxis) {
        this.k2LAxis = k2LAxis;
    }

    public String getRNCT() {
        return rNCT;
    }

    public void setRNCT(String rNCT) {
        this.rNCT = rNCT;
    }

    public String getLNCT() {
        return lNCT;
    }

    public void setLNCT(String lNCT) {
        this.lNCT = lNCT;
    }

    public String getRAT() {
        return rAT;
    }

    public void setRAT(String rAT) {
        this.rAT = rAT;
    }

    public String getLAT() {
        return lAT;
    }

    public void setLAT(String lAT) {
        this.lAT = lAT;
    }

    public String getRPachymetry() {
        return rPachymetry;
    }

    public void setRPachymetry(String rPachymetry) {
        this.rPachymetry = rPachymetry;
    }

    public String getLPachymetry() {
        return lPachymetry;
    }

    public void setLPachymetry(String lPachymetry) {
        this.lPachymetry = lPachymetry;
    }

    public String getRSchirmer() {
        return rSchirmer;
    }

    public void setRSchirmer(String rSchirmer) {
        this.rSchirmer = rSchirmer;
    }

    public String getLSchirmer() {
        return lSchirmer;
    }

    public void setLSchirmer(String lSchirmer) {
        this.lSchirmer = lSchirmer;
    }

    public String getRno() {
        return rno;
    }

    public void setRno(String rno) {
        this.rno = rno;
    }

    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }

    public List<Medication> getMedication() {
        return medication;
    }

    public void setMedication(List<Medication> medication) {
        this.medication = medication;
    }

    public List<Diagnosis> getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(List<Diagnosis> diagnosis) {
        this.diagnosis = diagnosis;
    }

    public class Medication implements Serializable {

        @SerializedName("MedicationID")
        @Expose
        private String medicationID;
        @SerializedName("AppointmentID")
        @Expose
        private String appointmentID;
        @SerializedName("MedicationName")
        @Expose
        private String medicationName;
        @SerializedName("dosage")
        @Expose
        private List<Dosage> dosage = null;

        public String getMedicationID() {
            return medicationID;
        }

        public void setMedicationID(String medicationID) {
            this.medicationID = medicationID;
        }

        public String getAppointmentID() {
            return appointmentID;
        }

        public void setAppointmentID(String appointmentID) {
            this.appointmentID = appointmentID;
        }

        public String getMedicationName() {
            return medicationName;
        }

        public void setMedicationName(String medicationName) {
            this.medicationName = medicationName;
        }

        public List<Dosage> getDosage() {
            return dosage;
        }

        public void setDosage(List<Dosage> dosage) {
            this.dosage = dosage;
        }
    }

    public class Dosage implements Serializable {

        @SerializedName("Dosage")
        @Expose
        private String dosage;
        @SerializedName("Description")
        @Expose
        private String description;

        public String getDosage() {
            return dosage;
        }

        public void setDosage(String dosage) {
            this.dosage = dosage;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }

    public class Diagnosis {

        @SerializedName("PDiagnosisID")
        @Expose
        private String pDiagnosisID;
        @SerializedName("PreliminaryExaminationID")
        @Expose
        private String preliminaryExaminationID;
        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("AnatomicalLocationID")
        @Expose
        private String anatomicalLocationID;
        @SerializedName("EyeID")
        @Expose
        private String eyeID;
        @SerializedName("DiseaseID")
        @Expose
        private String diseaseID;
        @SerializedName("EyeLenseID")
        @Expose
        private String eyeLenseID;
        @SerializedName("CataracttypeID")
        @Expose
        private String cataracttypeID;
        @SerializedName("StageID")
        @Expose
        private String stageID;
        @SerializedName("SideofbrainID")
        @Expose
        private String sideofbrainID;
        @SerializedName("PatterntypeID")
        @Expose
        private String patterntypeID;
        @SerializedName("Category")
        @Expose
        private String category;
        @SerializedName("AnatomicalLocation")
        @Expose
        private String anatomicalLocation;
        @SerializedName("Eye")
        @Expose
        private String eye;
        @SerializedName("EyeLense")
        @Expose
        private String eyeLense;
        @SerializedName("Disease")
        @Expose
        private String disease;
        @SerializedName("Cataracttype")
        @Expose
        private String cataracttype;
        @SerializedName("Stage")
        @Expose
        private String stage;
        @SerializedName("Sideofbrain")
        @Expose
        private String sideofbrain;
        @SerializedName("Patterntype")
        @Expose
        private String patterntype;

        public String getPDiagnosisID() {
            return pDiagnosisID;
        }

        public void setPDiagnosisID(String pDiagnosisID) {
            this.pDiagnosisID = pDiagnosisID;
        }

        public String getPreliminaryExaminationID() {
            return preliminaryExaminationID;
        }

        public void setPreliminaryExaminationID(String preliminaryExaminationID) {
            this.preliminaryExaminationID = preliminaryExaminationID;
        }

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public String getAnatomicalLocationID() {
            return anatomicalLocationID;
        }

        public void setAnatomicalLocationID(String anatomicalLocationID) {
            this.anatomicalLocationID = anatomicalLocationID;
        }

        public String getEyeID() {
            return eyeID;
        }

        public void setEyeID(String eyeID) {
            this.eyeID = eyeID;
        }

        public String getDiseaseID() {
            return diseaseID;
        }

        public void setDiseaseID(String diseaseID) {
            this.diseaseID = diseaseID;
        }

        public String getEyeLenseID() {
            return eyeLenseID;
        }

        public void setEyeLenseID(String eyeLenseID) {
            this.eyeLenseID = eyeLenseID;
        }

        public String getCataracttypeID() {
            return cataracttypeID;
        }

        public void setCataracttypeID(String cataracttypeID) {
            this.cataracttypeID = cataracttypeID;
        }

        public String getStageID() {
            return stageID;
        }

        public void setStageID(String stageID) {
            this.stageID = stageID;
        }

        public String getSideofbrainID() {
            return sideofbrainID;
        }

        public void setSideofbrainID(String sideofbrainID) {
            this.sideofbrainID = sideofbrainID;
        }

        public String getPatterntypeID() {
            return patterntypeID;
        }

        public void setPatterntypeID(String patterntypeID) {
            this.patterntypeID = patterntypeID;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getAnatomicalLocation() {
            return anatomicalLocation;
        }

        public void setAnatomicalLocation(String anatomicalLocation) {
            this.anatomicalLocation = anatomicalLocation;
        }

        public String getEye() {
            return eye;
        }

        public void setEye(String eye) {
            this.eye = eye;
        }

        public String getEyeLense() {
            return eyeLense;
        }

        public void setEyeLense(String eyeLense) {
            this.eyeLense = eyeLense;
        }

        public String getDisease() {
            return disease;
        }

        public void setDisease(String disease) {
            this.disease = disease;
        }

        public String getCataracttype() {
            return cataracttype;
        }

        public void setCataracttype(String cataracttype) {
            this.cataracttype = cataracttype;
        }

        public String getStage() {
            return stage;
        }

        public void setStage(String stage) {
            this.stage = stage;
        }

        public String getSideofbrain() {
            return sideofbrain;
        }

        public void setSideofbrain(String sideofbrain) {
            this.sideofbrain = sideofbrain;
        }

        public String getPatterntype() {
            return patterntype;
        }

        public void setPatterntype(String patterntype) {
            this.patterntype = patterntype;
        }
    }
}
