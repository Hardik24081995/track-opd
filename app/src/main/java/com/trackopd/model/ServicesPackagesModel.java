package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ServicesPackagesModel {
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("service")
    @Expose
    private List<Service> service = null;
    @SerializedName("package")
    @Expose
    private List<Package> _package = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Service> getService() {
        return service;
    }

    public void setService(List<Service> service) {
        this.service = service;
    }

    public List<Package> getPackage() {
        return _package;
    }

    public void setPackage(List<Package> _package) {
        this._package = _package;
    }

    public class Service {

        @SerializedName("BillingID")
        @Expose
        private String billingID;
        @SerializedName("ServicesName")
        @Expose
        private String servicesName;
        @SerializedName("ParentID")
        @Expose
        private String parentID;
        @SerializedName("Rate")
        @Expose
        private String rate;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Item")
        @Expose
        private List<Item> item = null;


        public String getBillingID() {
            return billingID;
        }

        public void setBillingID(String billingID) {
            this.billingID = billingID;
        }

        public String getServicesName() {
            return servicesName;
        }

        public void setServicesName(String servicesName) {
            this.servicesName = servicesName;
        }

        public String getParentID() {
            return parentID;
        }

        public void setParentID(String parentID) {
            this.parentID = parentID;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public List<Item> getItem() {
            return item;
        }

        public void setItem(List<Item> item) {
            this.item = item;
        }
    }
    public class Item {

        @SerializedName("BillingID")
        @Expose
        private String billingID;
        @SerializedName("ServicesName")
        @Expose
        private String servicesName;
        @SerializedName("ParentID")
        @Expose
        private String parentID;
        @SerializedName("Rate")
        @Expose
        private String rate;
        @SerializedName("Status")
        @Expose
        private String status;
        private boolean isSelected=false;

        public String getBillingID() {
            return billingID;
        }

        public void setBillingID(String billingID) {
            this.billingID = billingID;
        }

        public String getServicesName() {
            return servicesName;
        }

        public void setServicesName(String servicesName) {
            this.servicesName = servicesName;
        }

        public String getParentID() {
            return parentID;
        }

        public void setParentID(String parentID) {
            this.parentID = parentID;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }

    }

    public class Package {

        @SerializedName("PackageID")
        @Expose
        private String packageID;
        @SerializedName("Title")
        @Expose
        private String title;
        @SerializedName("Amount")
        @Expose
        private String amount;
        @SerializedName("DiscountAmount")
        @Expose
        private String discountAmount;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("PackageDate")
        @Expose
        private String packageDate;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;
        @SerializedName("Item")
        @Expose
        private List<Item_> item = null;

        private boolean isSelected=false;

        public String getPackageID() {
            return packageID;
        }

        public void setPackageID(String packageID) {
            this.packageID = packageID;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getDiscountAmount() {
            return discountAmount;
        }

        public void setDiscountAmount(String discountAmount) {
            this.discountAmount = discountAmount;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPackageDate() {
            return packageDate;
        }

        public void setPackageDate(String packageDate) {
            this.packageDate = packageDate;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

        public List<Item_> getItem() {
            return item;
        }

        public void setItem(List<Item_> item) {
            this.item = item;
        }
        public boolean isSelected() {
            return isSelected;
        }

        public void setSelected(boolean selected) {
            isSelected = selected;
        }
    }
    public class Item_ {

        @SerializedName("PacakageItemID")
        @Expose
        private String pacakageItemID;
        @SerializedName("BillingID")
        @Expose
        private String billingID;
        @SerializedName("Title")
        @Expose
        private String title;
        @SerializedName("Amount")
        @Expose
        private String amount;
        @SerializedName("Status")
        @Expose
        private String status;

        public String getPacakageItemID() {
            return pacakageItemID;
        }

        public void setPacakageItemID(String pacakageItemID) {
            this.pacakageItemID = pacakageItemID;
        }

        public String getBillingID() {
            return billingID;
        }

        public void setBillingID(String billingID) {
            this.billingID = billingID;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }
}
