package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetPatientHomeModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("TotalAppointment")
    @Expose
    private List<TotalAppointment> totalAppointment = null;
    @SerializedName("LastVisit")
    @Expose
    private List<LastVisit> lastVisit = null;
    @SerializedName("NextVisit")
    @Expose
    private List<NextVisit> nextVisit = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<TotalAppointment> getTotalAppointment() {
        return totalAppointment;
    }

    public void setTotalAppointment(List<TotalAppointment> totalAppointment) {
        this.totalAppointment = totalAppointment;
    }

    public List<LastVisit> getLastVisit() {
        return lastVisit;
    }

    public void setLastVisit(List<LastVisit> lastVisit) {
        this.lastVisit = lastVisit;
    }

    public List<NextVisit> getNextVisit() {
        return nextVisit;
    }

    public void setNextVisit(List<NextVisit> nextVisit) {
        this.nextVisit = nextVisit;
    }

    public class LastVisit {

        @SerializedName("AppoinmentDateTime")
        @Expose
        private String appoinmentDateTime;
        @SerializedName("TicketNumber")
        @Expose
        private String ticketNumber;
        @SerializedName("AppointmentType")
        @Expose
        private String appointmentType;
        @SerializedName("PaymentStatus")
        @Expose
        private String paymentStatus;
        @SerializedName("DoctorName")
        @Expose
        private String doctorName;

        public String getAppoinmentDateTime() {
            return appoinmentDateTime;
        }

        public void setAppoinmentDateTime(String appoinmentDateTime) {
            this.appoinmentDateTime = appoinmentDateTime;
        }

        public String getTicketNumber() {
            return ticketNumber;
        }

        public void setTicketNumber(String ticketNumber) {
            this.ticketNumber = ticketNumber;
        }

        public String getAppointmentType() {
            return appointmentType;
        }

        public void setAppointmentType(String appointmentType) {
            this.appointmentType = appointmentType;
        }

        public String getPaymentStatus() {
            return paymentStatus;
        }

        public void setPaymentStatus(String paymentStatus) {
            this.paymentStatus = paymentStatus;
        }

        public String getDoctorName() {
            return doctorName;
        }

        public void setDoctorName(String doctorName) {
            this.doctorName = doctorName;
        }

    }

    public class NextVisit {

        @SerializedName("AppoinmentDateTime")
        @Expose
        private String appoinmentDateTime;
        @SerializedName("TicketNumber")
        @Expose
        private String ticketNumber;
        @SerializedName("AppointmentType")
        @Expose
        private String appointmentType;
        @SerializedName("PaymentStatus")
        @Expose
        private String paymentStatus;
        @SerializedName("DoctorName")
        @Expose
        private String doctorName;

        public String getAppoinmentDateTime() {
            return appoinmentDateTime;
        }

        public void setAppoinmentDateTime(String appoinmentDateTime) {
            this.appoinmentDateTime = appoinmentDateTime;
        }

        public String getTicketNumber() {
            return ticketNumber;
        }

        public void setTicketNumber(String ticketNumber) {
            this.ticketNumber = ticketNumber;
        }

        public String getAppointmentType() {
            return appointmentType;
        }

        public void setAppointmentType(String appointmentType) {
            this.appointmentType = appointmentType;
        }

        public String getPaymentStatus() {
            return paymentStatus;
        }

        public void setPaymentStatus(String paymentStatus) {
            this.paymentStatus = paymentStatus;
        }

        public String getDoctorName() {
            return doctorName;
        }

        public void setDoctorName(String doctorName) {
            this.doctorName = doctorName;
        }
    }

    public class TotalAppointment {

        @SerializedName("Appointment")
        @Expose
        private String appointment;

        public String getAppointment() {
            return appointment;
        }
        public void setAppointment(String appointment) {
            this.appointment = appointment;
        }
    }
}
