package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TempleteModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }


    public class Datum {

        @SerializedName("TemplateID")
        @Expose
        private String templateID;
        @SerializedName("TemplateName")
        @Expose
        private String templateName;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;
        @SerializedName("Item")
        @Expose
        private List<Item> item = null;

        public String getTemplateID() {
            return templateID;
        }

        public void setTemplateID(String templateID) {
            this.templateID = templateID;
        }

        public String getTemplateName() {
            return templateName;
        }

        public void setTemplateName(String templateName) {
            this.templateName = templateName;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

        public List<Item> getItem() {
            return item;
        }

        public void setItem(List<Item> item) {
            this.item = item;
        }

        public class Item {

            @SerializedName("TemplateMedicationID")
            @Expose
            private String templateMedicationID;
            @SerializedName("TemplateID")
            @Expose
            private String templateID;
            @SerializedName("MedicationID")
            @Expose
            private String medicationID;
            @SerializedName("MedicationName")
            @Expose
            private String medicationName;
            @SerializedName("TotalTimes")
            @Expose
            private String totalTimes;
            @SerializedName("TotalDuration")
            @Expose
            private String totalDuration;
            @SerializedName("Type")
            @Expose
            private String type;
            @SerializedName("Status")
            @Expose
            private String status;

            public String getTemplateMedicationID() {
                return templateMedicationID;
            }

            public void setTemplateMedicationID(String templateMedicationID) {
                this.templateMedicationID = templateMedicationID;
            }

            public String getTemplateID() {
                return templateID;
            }

            public void setTemplateID(String templateID) {
                this.templateID = templateID;
            }

            public String getMedicationID() {
                return medicationID;
            }

            public void setMedicationID(String medicationID) {
                this.medicationID = medicationID;
            }

            public String getMedicationName() {
                return medicationName;
            }

            public void setMedicationName(String medicationName) {
                this.medicationName = medicationName;
            }

            public String getTotalTimes() {
                return totalTimes;
            }

            public void setTotalTimes(String totalTimes) {
                this.totalTimes = totalTimes;
            }

            public String getTotalDuration() {
                return totalDuration;
            }

            public void setTotalDuration(String totalDuration) {
                this.totalDuration = totalDuration;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

        }

    }


}
