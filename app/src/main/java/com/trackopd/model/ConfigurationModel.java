package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ConfigurationModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {
        @SerializedName("ConfigID")
        @Expose
        private String configID;
        @SerializedName("SupportEmail")
        @Expose
        private String supportEmail;
        @SerializedName("TimeZone")
        @Expose
        private String timeZone;
        @SerializedName("MailFromName")
        @Expose
        private String mailFromName;
        @SerializedName("AppVersionIOS")
        @Expose
        private String appVersionIOS;
        @SerializedName("AppVersionAndroid")
        @Expose
        private String appVersionAndroid;
        @SerializedName("TimeSlotDuration")
        @Expose
        private String timeSlotDuration;
        @SerializedName("ConsultancyFees1")
        @Expose
        private String consultancyFees1;
        @SerializedName("ConsultancyFees2")
        @Expose
        private String consultancyFees2;
        @SerializedName("PatientCodePrefix")
        @Expose
        private String patientCodePrefix;
        @SerializedName("PatientCodeStartFrom")
        @Expose
        private String patientCodeStartFrom;
        @SerializedName("CancelEmailID")
        @Expose
        private String cancelEmailID;
        @SerializedName("NoOfPatientAtATime")
        @Expose
        private String noOfPatientAtATime;
        @SerializedName("PageSize")
        @Expose
        private String pageSize;
        @SerializedName("AppointmentConfiguration")
        @Expose
        private String appointmentConfiguration;
        @SerializedName("EndTime")
        private String EndTime;

        public String getEndTime() {
            return EndTime;
        }

        public void setEndTime(String endTime) {
            EndTime = endTime;
        }

        public String getConfigID() {
            return configID;
        }

        public void setConfigID(String configID) {
            this.configID = configID;
        }

        public String getSupportEmail() {
            return supportEmail;
        }

        public void setSupportEmail(String supportEmail) {
            this.supportEmail = supportEmail;
        }

        public String getTimeZone() {
            return timeZone;
        }

        public void setTimeZone(String timeZone) {
            this.timeZone = timeZone;
        }

        public String getMailFromName() {
            return mailFromName;
        }

        public void setMailFromName(String mailFromName) {
            this.mailFromName = mailFromName;
        }

        public String getAppVersionIOS() {
            return appVersionIOS;
        }

        public void setAppVersionIOS(String appVersionIOS) {
            this.appVersionIOS = appVersionIOS;
        }

        public String getAppVersionAndroid() {
            return appVersionAndroid;
        }

        public void setAppVersionAndroid(String appVersionAndroid) {
            this.appVersionAndroid = appVersionAndroid;
        }

        public String getTimeSlotDuration() {
            return timeSlotDuration;
        }

        public void setTimeSlotDuration(String timeSlotDuration) {
            this.timeSlotDuration = timeSlotDuration;
        }

        public String getConsultancyFees1() {
            return consultancyFees1;
        }

        public void setConsultancyFees1(String consultancyFees1) {
            this.consultancyFees1 = consultancyFees1;
        }

        public String getConsultancyFees2() {
            return consultancyFees2;
        }

        public void setConsultancyFees2(String consultancyFees2) {
            this.consultancyFees2 = consultancyFees2;
        }

        public String getPatientCodePrefix() {
            return patientCodePrefix;
        }

        public void setPatientCodePrefix(String patientCodePrefix) {
            this.patientCodePrefix = patientCodePrefix;
        }

        public String getPatientCodeStartFrom() {
            return patientCodeStartFrom;
        }

        public void setPatientCodeStartFrom(String patientCodeStartFrom) {
            this.patientCodeStartFrom = patientCodeStartFrom;
        }

        public String getCancelEmailID() {
            return cancelEmailID;
        }

        public void setCancelEmailID(String cancelEmailID) {
            this.cancelEmailID = cancelEmailID;
        }

        public String getNoOfPatientAtATime() {
            return noOfPatientAtATime;
        }

        public void setNoOfPatientAtATime(String noOfPatientAtATime) {
            this.noOfPatientAtATime = noOfPatientAtATime;
        }

        public String getPageSize() {
            return pageSize;
        }

        public void setPageSize(String pageSize) {
            this.pageSize = pageSize;
        }

        public String getAppointmentConfiguration() {
            return appointmentConfiguration;
        }

        public void setAppointmentConfiguration(String appointmentConfiguration) {
            this.appointmentConfiguration = appointmentConfiguration;
        }
    }
}


