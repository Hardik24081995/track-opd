package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PreliminaryExaminationViewHistoryModel implements Serializable {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<PreliminaryExaminationViewHistoryModel> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<PreliminaryExaminationViewHistoryModel> getData() {
        return data;
    }

    public void setData(ArrayList<PreliminaryExaminationViewHistoryModel> data) {
        this.data = data;
    }

    @SerializedName("LNCT")
    @Expose
    private String lNCT;
    @SerializedName("RNCT")
    @Expose
    private String rNCT;
    @SerializedName("LAT")
    @Expose
    private String lAT;
    @SerializedName("RAT")
    @Expose
    private String rAT;
    @SerializedName("LPachymetry")
    @Expose
    private String lPachymetry;
    @SerializedName("RPachymetry")
    @Expose
    private String rPachymetry;
    @SerializedName("LColorVision")
    @Expose
    private String lColorVision;
    @SerializedName("RColorVision")
    @Expose
    private String rColorVision;
    @SerializedName("LSyringing")
    @Expose
    private String lSyringing;
    @SerializedName("RSyringing")
    @Expose
    private String rSyringing;
    @SerializedName("LSchirmer")
    @Expose
    private String lSchirmer;
    @SerializedName("RSchirmer")
    @Expose
    private String rSchirmer;

    public String getLNCT() {
        return lNCT;
    }

    public void setLNCT(String lNCT) {
        this.lNCT = lNCT;
    }

    public String getRNCT() {
        return rNCT;
    }

    public void setRNCT(String rNCT) {
        this.rNCT = rNCT;
    }

    public String getLAT() {
        return lAT;
    }

    public void setLAT(String lAT) {
        this.lAT = lAT;
    }

    public String getRAT() {
        return rAT;
    }

    public void setRAT(String rAT) {
        this.rAT = rAT;
    }

    public String getLPachymetry() {
        return lPachymetry;
    }

    public void setLPachymetry(String lPachymetry) {
        this.lPachymetry = lPachymetry;
    }

    public String getRPachymetry() {
        return rPachymetry;
    }

    public void setRPachymetry(String rPachymetry) {
        this.rPachymetry = rPachymetry;
    }

    public String getLColorVision() {
        return lColorVision;
    }

    public void setLColorVision(String lColorVision) {
        this.lColorVision = lColorVision;
    }

    public String getRColorVision() {
        return rColorVision;
    }

    public void setRColorVision(String rColorVision) {
        this.rColorVision = rColorVision;
    }

    public String getLSyringing() {
        return lSyringing;
    }

    public void setLSyringing(String lSyringing) {
        this.lSyringing = lSyringing;
    }

    public String getRSyringing() {
        return rSyringing;
    }

    public void setRSyringing(String rSyringing) {
        this.rSyringing = rSyringing;
    }

    public String getLSchirmer() {
        return lSchirmer;
    }

    public void setLSchirmer(String lSchirmer) {
        this.lSchirmer = lSchirmer;
    }

    public String getRSchirmer() {
        return rSchirmer;
    }

    public void setRSchirmer(String rSchirmer) {
        this.rSchirmer = rSchirmer;
    }
}
