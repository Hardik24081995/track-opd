package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Collection;
import java.util.List;

public class DosageModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<DosageModel> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<DosageModel> getData() {
        return data;
    }

    public void setData(List<DosageModel> data) {
        this.data = data;
    }

    @SerializedName("DosageID")
    @Expose
    private String dosageID;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("Status")
    @Expose
    private String status;

    public String getDosageID() {
        return dosageID;
    }

    public void setDosageID(String dosageID) {
        this.dosageID = dosageID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
