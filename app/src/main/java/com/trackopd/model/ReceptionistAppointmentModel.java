package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

public class ReceptionistAppointmentModel implements Serializable {

    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("rowCount")
    @Expose
    private String rowCount;
    @SerializedName("data")
    @Expose
    private ArrayList<ReceptionistAppointmentModel> data = null;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRowCount() {
        return rowCount;
    }

    public void setRowCount(String rowCount) {
        this.rowCount = rowCount;
    }

    public ArrayList<ReceptionistAppointmentModel> getData() {
        return data;
    }

    public void setData(ArrayList<ReceptionistAppointmentModel> data) {
        this.data = data;
    }

    @SerializedName("AppointmentID")
    @Expose
    private String appointmentID;
    @SerializedName("PatientFirstName")
    @Expose
    private String patientFirstName;
    @SerializedName("PatientLastName")
    @Expose
    private String patientLastName;
    @SerializedName("PatientMobileNo")
    @Expose
    private String patientMobileNo;
    @SerializedName("MRDNo")
    @Expose
    private String MRDNo;
    @SerializedName("DoctorFirstName")
    @Expose
    private String doctorFirstName;
    @SerializedName("DoctorLastName")
    @Expose
    private String doctorLastName;
    @SerializedName("DoctorID")
    @Expose
    private String doctorID;
    @SerializedName("TicketNumber")
    @Expose
    private String ticketNumber;
    @SerializedName("AppointmentDate")
    @Expose
    private String appointmentDate;
    @SerializedName("PatientCode")
    @Expose
    private String patientCode;
    @SerializedName("AppointmentStatus")
    @Expose
    private String appointmentStatus;
    @SerializedName("PaymentStatus")
    @Expose
    private String paymentStatus;
    @SerializedName("AppointmentType")
    @Expose
    private String appointmentType;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("CurrentStatus")
    @Expose
    private String currentStatus;
    @SerializedName("AsigneeName")
    @Expose
    private String asigneeName;
    @SerializedName("PatientID")
    @Expose
    private String patientID;
    @SerializedName("Reason")
    @Expose
    private String reason;
    @SerializedName("ReasonID")
    @Expose
    private String reasonID;
    @SerializedName("StatusTitle")
    @Expose
    private String statusTitle;
    @SerializedName("AppointmentLocation")
    @Expose
    private String appointmentLocation;
    @SerializedName("Location")
    @Expose
    private String location;
    @SerializedName("CheckINDate")
    @Expose
    private String checkINDate;
    @SerializedName("Rno")
    @Expose
    private String rno;
    @SerializedName("rowcount")
    @Expose
    private String rowcount;
    @SerializedName("ProfileImage")
    @Expose
    private String profileImage;
    @SerializedName("CheckUpType")
    @Expose
    private String checkUpType;

    private String TimeRemaining;


    public String getAppointmentID() {
        return appointmentID;
    }

    public void setAppointmentID(String appointmentID) {
        this.appointmentID = appointmentID;
    }

    public String getPatientFirstName() {
        return patientFirstName;
    }

    public void setPatientFirstName(String patientFirstName) {
        this.patientFirstName = patientFirstName;
    }

    public String getPatientLastName() {
        return patientLastName;
    }

    public void setPatientLastName(String patientLastName) {
        this.patientLastName = patientLastName;
    }

    public String getPatientMobileNo() {
        return patientMobileNo;
    }

    public void setPatientMobileNo(String patientMobileNo) {
        this.patientMobileNo = patientMobileNo;
    }

    public String getMRDNo() {
        return MRDNo;
    }

    public void setMRDNo(String MRDNo) {
        this.MRDNo = MRDNo;
    }

    public String getDoctorFirstName() {
        return doctorFirstName;
    }

    public void setDoctorFirstName(String doctorFirstName) {
        this.doctorFirstName = doctorFirstName;
    }

    public String getDoctorLastName() {
        return doctorLastName;
    }

    public void setDoctorLastName(String doctorLastName) {
        this.doctorLastName = doctorLastName;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getPatientCode() {
        return patientCode;
    }

    public void setPatientCode(String patientCode) {
        this.patientCode = patientCode;
    }

    public String getAppointmentStatus() {
        return appointmentStatus;
    }

    public void setAppointmentStatus(String appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(String appointmentType) {
        this.appointmentType = appointmentType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getAsigneeName() {
        return asigneeName;
    }

    public void setAsigneeName(String asigneeName) {
        this.asigneeName = asigneeName;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public String getAppointmentLocation() {
        return appointmentLocation;
    }

    public void setAppointmentLocation(String appointmentLocation) {
        this.appointmentLocation = appointmentLocation;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCheckINDate() {
        return checkINDate;
    }

    public void setCheckINDate(String checkINDate) {
        this.checkINDate = checkINDate;
    }

    public String getRno() {
        return rno;
    }

    public void setRno(String rno) {
        this.rno = rno;
    }

    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReasonID() {
        return reasonID;
    }

    public void setReasonID(String reasonID) {
        this.reasonID = reasonID;
    }

    public String getStatusTitle() {
        return statusTitle;
    }

    public void setStatusTitle(String statusTitle) {
        this.statusTitle = statusTitle;
    }

    public String getDoctorID() {
        return doctorID;
    }

    public void setDoctorID(String doctorID) {
        this.doctorID = doctorID;
    }

    public String getTimeRemaining() {
        return TimeRemaining;
    }

    public void setTimeRemaining(String timeRemaining) {
        TimeRemaining = timeRemaining;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getCheckUpType() {
        return checkUpType;
    }

    public void setCheckUpType(String checkUpType) {
        this.checkUpType = checkUpType;
    }
}