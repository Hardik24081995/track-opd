package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CouncillorInquiryFormsModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("rowCount")
    @Expose
    private String rowCount;
    @SerializedName("data")
    @Expose
    private ArrayList<CouncillorInquiryFormsModel> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRowCount() {
        return rowCount;
    }

    public void setRowCount(String rowCount) {
        this.rowCount = rowCount;
    }

    public ArrayList<CouncillorInquiryFormsModel> getData() {
        return data;
    }

    public void setData(ArrayList<CouncillorInquiryFormsModel> data) {
        this.data = data;
    }

    @SerializedName("CouncillorFormID")
    @Expose
    private String councillorFormID;
    @SerializedName("PatientFirstName")
    @Expose
    private String patientFirstName;
    @SerializedName("PatientLastName")
    @Expose
    private String patientLastName;
    @SerializedName("PatientMobileNo")
    @Expose
    private String patientMobileNo;
    @SerializedName("CouncillorName")
    @Expose
    private String councillorName;
    @SerializedName("CouncillorFirstName")
    @Expose
    private String councillorFirstName;
    @SerializedName("CouncillorLastName")
    @Expose
    private String councillorLastName;
    @SerializedName("CouncillorDate")
    @Expose
    private String councillorDate;
    @SerializedName("CouncillorFor")
    @Expose
    private String councillorFor;
    @SerializedName("RecommandBy")
    @Expose
    private String recommandBy;
    @SerializedName("Remarks")
    @Expose
    private String remarks;
    @SerializedName("CouncillorID")
    @Expose
    private String councillorID;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("PatientID")
    @Expose
    private String patientID;
    @SerializedName("Rno")
    @Expose
    private String rno;
    @SerializedName("rowcount")
    @Expose
    private String rowcount;

    public String getCouncillorFormID() {
        return councillorFormID;
    }

    public void setCouncillorFormID(String councillorFormID) {
        this.councillorFormID = councillorFormID;
    }

    public String getPatientFirstName() {
        return patientFirstName;
    }

    public void setPatientFirstName(String patientFirstName) {
        this.patientFirstName = patientFirstName;
    }

    public String getPatientLastName() {
        return patientLastName;
    }

    public void setPatientLastName(String patientLastName) {
        this.patientLastName = patientLastName;
    }

    public String getPatientMobileNo() {
        return patientMobileNo;
    }

    public void setPatientMobileNo(String patientMobileNo) {
        this.patientMobileNo = patientMobileNo;
    }

    public String getCouncillorName() {
        return councillorName;
    }

    public void setCouncillorName(String councillorName) {
        this.councillorName = councillorName;
    }

    public String getCouncillorFirstName() {
        return councillorFirstName;
    }

    public void setCouncillorFirstName(String councillorFirstName) {
        this.councillorFirstName = councillorFirstName;
    }

    public String getCouncillorLastName() {
        return councillorLastName;
    }

    public void setCouncillorLastName(String councillorLastName) {
        this.councillorLastName = councillorLastName;
    }

    public String getCouncillorDate() {
        return councillorDate;
    }

    public void setCouncillorDate(String councillorDate) {
        this.councillorDate = councillorDate;
    }

    public String getCouncillorFor() {
        return councillorFor;
    }

    public void setCouncillorFor(String councillorFor) {
        this.councillorFor = councillorFor;
    }

    public String getRecommandBy() {
        return recommandBy;
    }

    public void setRecommandBy(String recommandBy) {
        this.recommandBy = recommandBy;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCouncillorID() {
        return councillorID;
    }

    public void setCouncillorID(String councillorID) {
        this.councillorID = councillorID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public String getRno() {
        return rno;
    }

    public void setRno(String rno) {
        this.rno = rno;
    }

    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }
}
