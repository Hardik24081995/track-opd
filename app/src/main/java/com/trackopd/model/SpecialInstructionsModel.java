package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SpecialInstructionsModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<SpecialInstructionsModel> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<SpecialInstructionsModel> getData() {
        return data;
    }

    public void setData(ArrayList<SpecialInstructionsModel> data) {
        this.data = data;
    }

    @SerializedName("SpecialInstuctionID")
    @Expose
    private String specialInstuctionID;
    @SerializedName("SpecialInstruction")
    @Expose
    private String specialInstruction;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("Rno")
    @Expose
    private String rno;
    @SerializedName("rowcount")
    @Expose
    private String rowcount;

    public String getSpecialInstuctionID() {
        return specialInstuctionID;
    }

    public void setSpecialInstuctionID(String specialInstuctionID) {
        this.specialInstuctionID = specialInstuctionID;
    }

    public String getSpecialInstruction() {
        return specialInstruction;
    }

    public void setSpecialInstruction(String specialInstruction) {
        this.specialInstruction = specialInstruction;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRno() {
        return rno;
    }

    public void setRno(String rno) {
        this.rno = rno;
    }

    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }
}
