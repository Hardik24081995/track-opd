package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AppointmentValidityModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<AppointmentValidityModel> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<AppointmentValidityModel> getData() {
        return data;
    }

    public void setData(ArrayList<AppointmentValidityModel> data) {
        this.data = data;
    }

    @SerializedName("TotalAppointment")
    @Expose
    private String totalAppointment;

    public String getTotalAppointment() {
        return totalAppointment;
    }

    public void setTotalAppointment(String totalAppointment) {
        this.totalAppointment = totalAppointment;
    }
}
