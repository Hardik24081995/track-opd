package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ALL")
public class HistoryPreliminaryExaminationModel implements Serializable {

    public static Object Data;
    @SerializedName("data")
    @Expose
    private ArrayList<Data> data = null;
    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("rowCount")
    @Expose
    private String rowCount;

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRowCount() {
        return rowCount;
    }

    public void setRowCount(String rowCount) {
        this.rowCount = rowCount;
    }
    public class Data implements Serializable {

        @SerializedName("PreliminaryExaminationID")
        @Expose
        private String preliminaryExaminationID;
        @SerializedName("PatientFirstName")
        @Expose
        private String patientFirstName;
        @SerializedName("PatientLastName")
        @Expose
        private String patientLastName;
        @SerializedName("PatientMobileNo")
        @Expose
        private String patientMobileNo;
        @SerializedName("DoctorFirstName")
        @Expose
        private String doctorFirstName;
        @SerializedName("DoctorLastName")
        @Expose
        private String doctorLastName;
        @SerializedName("TreatmentDate")
        @Expose
        private String treatmentDate;
        @SerializedName("PatientID")
        @Expose
        private String patientID;
        @SerializedName("DoctorID")
        @Expose
        private String doctorID;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("AppointmentID")
        @Expose
        private String appointmentID;
        @SerializedName("TicketNumber")
        @Expose
        private String ticketNumber;
        @SerializedName("AppointmentDate")
        @Expose
        private String appointmentDate;
        @SerializedName("PatientCode")
        @Expose
        private String patientCode;
        @SerializedName("Rno")
        @Expose
        private String rno;
        @SerializedName("rowcount")
        @Expose
        private String rowcount;



        //Set Diagnosis
        @SerializedName("Diagnosis")
        @Expose
        private ArrayList<Diagnosi> diagnosis = null;

        //Surgery Suggested
        @SerializedName("SurgerySuggested")
        @Expose
        private ArrayList<SurgerySuggested> surgerySuggested = null;

        @SerializedName("Investigation")
        @Expose
        private ArrayList<Investigation> investigation = null;

        @SerializedName("CounselingDetails")
        @Expose
        private ArrayList<CounselingDetail> counselingDetails = null;

        @SerializedName("Surgery")
        @Expose
        private ArrayList<Surgery> surgery = null;


        @SerializedName("Vision")
        @Expose
        private ArrayList<Vision> vision = null;

        @SerializedName("PreExamination")
        @Expose
        private ArrayList<PreExamination> preExamination = null;


        @SerializedName("HistoryAndComplains")
        @Expose
        private ArrayList<HistoryAndComplain> historyAndComplains = null;

        // Doodle List
        @SerializedName("Doodle")
        @Expose
        private List<Doodle> doodle = null;
        @SerializedName("medication")
        @Expose
        private ArrayList<Medication> medication = null;


        public String getPreliminaryExaminationID() {
            return preliminaryExaminationID;
        }

        public void setPreliminaryExaminationID(String preliminaryExaminationID) {
            this.preliminaryExaminationID = preliminaryExaminationID;
        }

        public String getPatientFirstName() {
            return patientFirstName;
        }

        public void setPatientFirstName(String patientFirstName) {
            this.patientFirstName = patientFirstName;
        }

        public String getPatientLastName() {
            return patientLastName;
        }

        public void setPatientLastName(String patientLastName) {
            this.patientLastName = patientLastName;
        }

        public String getPatientMobileNo() {
            return patientMobileNo;
        }

        public void setPatientMobileNo(String patientMobileNo) {
            this.patientMobileNo = patientMobileNo;
        }

        public String getDoctorFirstName() {
            return doctorFirstName;
        }

        public void setDoctorFirstName(String doctorFirstName) {
            this.doctorFirstName = doctorFirstName;
        }

        public String getDoctorLastName() {
            return doctorLastName;
        }

        public void setDoctorLastName(String doctorLastName) {
            this.doctorLastName = doctorLastName;
        }

        public String getTreatmentDate() {
            return treatmentDate;
        }

        public void setTreatmentDate(String treatmentDate) {
            this.treatmentDate = treatmentDate;
        }

        public String getPatientID() {
            return patientID;
        }

        public void setPatientID(String patientID) {
            this.patientID = patientID;
        }

        public String getDoctorID() {
            return doctorID;
        }

        public void setDoctorID(String doctorID) {
            this.doctorID = doctorID;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getAppointmentID() {
            return appointmentID;
        }

        public void setAppointmentID(String appointmentID) {
            this.appointmentID = appointmentID;
        }

        public String getTicketNumber() {
            return ticketNumber;
        }

        public void setTicketNumber(String ticketNumber) {
            this.ticketNumber = ticketNumber;
        }

        public String getAppointmentDate() {
            return appointmentDate;
        }

        public void setAppointmentDate(String appointmentDate) {
            this.appointmentDate = appointmentDate;
        }

        public String getPatientCode() {
            return patientCode;
        }

        public void setPatientCode(String patientCode) {
            this.patientCode = patientCode;
        }

        public String getRno() {
            return rno;
        }

        public void setRno(String rno) {
            this.rno = rno;
        }

        public String getRowcount() {
            return rowcount;
        }

        public void setRowcount(String rowcount) {
            this.rowcount = rowcount;
        }

        public ArrayList<Diagnosi> getDiagnosis() {
            return diagnosis;
        }

        public void setDiagnosis(ArrayList<Diagnosi> diagnosis) {
            this.diagnosis = diagnosis;
        }


        public ArrayList<SurgerySuggested> getSurgerySuggested() {
            return surgerySuggested;
        }

        public void setSurgerySuggested(ArrayList<SurgerySuggested> surgerySuggested) {
            this.surgerySuggested = surgerySuggested;
        }

        //Set Investigation
        public ArrayList<Investigation> getInvestigation() {
            return investigation;
        }

        public void setInvestigation(ArrayList<Investigation> investigation) {
            this.investigation = investigation;
        }

        //Counseling Detail
        public ArrayList<CounselingDetail> getCounselingDetails() {
            return counselingDetails;
        }

        public void setCounselingDetails(ArrayList<CounselingDetail> counselingDetails) {
            this.counselingDetails = counselingDetails;
        }

        public ArrayList<Surgery> getSurgery() {
            return surgery;
        }

        public void setSurgery(ArrayList<Surgery> surgery) {
            this.surgery = surgery;
        }


        //get set Vision
        public ArrayList<Vision> getVision() {
            return vision;
        }

        public void setVision(ArrayList<Vision> vision) {
            this.vision = vision;
        }

        public ArrayList<PreExamination> getPreExamination() {
            return preExamination;
        }

        public void setPreExamination(ArrayList<PreExamination> preExamination) {
            this.preExamination = preExamination;
        }

        public ArrayList<HistoryAndComplain> getHistoryAndComplains() {
            return historyAndComplains;
        }

        public void setHistoryAndComplains(ArrayList<HistoryAndComplain> historyAndComplains) {
            this.historyAndComplains = historyAndComplains;
        }

        public List<Doodle> getDoodle() {
            return doodle;
        }

        public void setDoodle(List<Doodle> doodle) {
            this.doodle = doodle;
        }

        public ArrayList<Medication> getMedication() {
            return medication;
        }

        public void setMedication(ArrayList<Medication> medication) {
            this.medication = medication;
        }
    }

    public class Diagnosi implements Serializable {

        @SerializedName("PDiagnosisID")
        @Expose
        private String pDiagnosisID;
        @SerializedName("PreliminaryExaminationID")
        @Expose
        private String preliminaryExaminationID;
        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("AnatomicalLocationID")
        @Expose
        private String anatomicalLocationID;
        @SerializedName("EyeID")
        @Expose
        private String eyeID;
        @SerializedName("DiseaseID")
        @Expose
        private String diseaseID;
        @SerializedName("EyeLenseID")
        @Expose
        private String eyeLenseID;
        @SerializedName("CataracttypeID")
        @Expose
        private String cataracttypeID;
        @SerializedName("StageID")
        @Expose
        private String stageID;
        @SerializedName("SideofbrainID")
        @Expose
        private String sideofbrainID;
        @SerializedName("PatterntypeID")
        @Expose
        private String patterntypeID;
        @SerializedName("Category")
        @Expose
        private String category;
        @SerializedName("AnatomicalLocation")
        @Expose
        private String anatomicalLocation;
        @SerializedName("Eye")
        @Expose
        private String eye;
        @SerializedName("EyeLense")
        @Expose
        private String eyeLense;
        @SerializedName("Disease")
        @Expose
        private String disease;
        @SerializedName("Cataracttype")
        @Expose
        private String cataracttype;
        @SerializedName("Stage")
        @Expose
        private String stage;
        @SerializedName("Sideofbrain")
        @Expose
        private String sideofbrain;
        @SerializedName("Patterntype")
        @Expose
        private String patterntype;

        public String getPDiagnosisID() {
            return pDiagnosisID;
        }

        public void setPDiagnosisID(String pDiagnosisID) {
            this.pDiagnosisID = pDiagnosisID;
        }

        public String getPreliminaryExaminationID() {
            return preliminaryExaminationID;
        }

        public void setPreliminaryExaminationID(String preliminaryExaminationID) {
            this.preliminaryExaminationID = preliminaryExaminationID;
        }

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public String getAnatomicalLocationID() {
            return anatomicalLocationID;
        }

        public void setAnatomicalLocationID(String anatomicalLocationID) {
            this.anatomicalLocationID = anatomicalLocationID;
        }

        public String getEyeID() {
            return eyeID;
        }

        public void setEyeID(String eyeID) {
            this.eyeID = eyeID;
        }

        public String getDiseaseID() {
            return diseaseID;
        }

        public void setDiseaseID(String diseaseID) {
            this.diseaseID = diseaseID;
        }

        public String getEyeLenseID() {
            return eyeLenseID;
        }

        public void setEyeLenseID(String eyeLenseID) {
            this.eyeLenseID = eyeLenseID;
        }

        public String getCataracttypeID() {
            return cataracttypeID;
        }

        public void setCataracttypeID(String cataracttypeID) {
            this.cataracttypeID = cataracttypeID;
        }

        public String getStageID() {
            return stageID;
        }

        public void setStageID(String stageID) {
            this.stageID = stageID;
        }

        public String getSideofbrainID() {
            return sideofbrainID;
        }

        public void setSideofbrainID(String sideofbrainID) {
            this.sideofbrainID = sideofbrainID;
        }

        public String getPatterntypeID() {
            return patterntypeID;
        }

        public void setPatterntypeID(String patterntypeID) {
            this.patterntypeID = patterntypeID;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getAnatomicalLocation() {
            return anatomicalLocation;
        }

        public void setAnatomicalLocation(String anatomicalLocation) {
            this.anatomicalLocation = anatomicalLocation;
        }

        public String getEye() {
            return eye;
        }

        public void setEye(String eye) {
            this.eye = eye;
        }

        public String getEyeLense() {
            return eyeLense;
        }

        public void setEyeLense(String eyeLense) {
            this.eyeLense = eyeLense;
        }

        public String getDisease() {
            return disease;
        }

        public void setDisease(String disease) {
            this.disease = disease;
        }

        public String getCataracttype() {
            return cataracttype;
        }

        public void setCataracttype(String cataracttype) {
            this.cataracttype = cataracttype;
        }

        public String getStage() {
            return stage;
        }

        public void setStage(String stage) {
            this.stage = stage;
        }

        public String getSideofbrain() {
            return sideofbrain;
        }

        public void setSideofbrain(String sideofbrain) {
            this.sideofbrain = sideofbrain;
        }

        public String getPatterntype() {
            return patterntype;
        }

        public void setPatterntype(String patterntype) {
            this.patterntype = patterntype;
        }
    }

    public class SurgerySuggested {

        @SerializedName("CategoryID")
        @Expose
        private String categoryID;
        @SerializedName("SurgeryTypeID")
        @Expose
        private String surgeryTypeID;
        @SerializedName("SurgeryType2")
        @Expose
        private String SurgeryType2;
        @SerializedName("EyeID")
        @Expose
        private String eyeID;
        @SerializedName("Duration")
        @Expose
        private String duration;
        @SerializedName("SurgeryName")
        @Expose
        private String surgeryName;
        @SerializedName("Category")
        @Expose
        private String category;
        @SerializedName("Eye")
        @Expose
        private String eye;
        @SerializedName("Remarks")
        @Expose
        private String remarks;

        public String getCategoryID() {
            return categoryID;
        }

        public void setCategoryID(String categoryID) {
            this.categoryID = categoryID;
        }

        public String getSurgeryTypeID() {
            return surgeryTypeID;
        }

        public void setSurgeryTypeID(String surgeryTypeID) {
            this.surgeryTypeID = surgeryTypeID;
        }


        public String getSurgeryType2() {
            return SurgeryType2;
        }

        public void setSurgeryType2(String surgeryType2) {
            SurgeryType2 = surgeryType2;
        }


        public String getEyeID() {
            return eyeID;
        }

        public void setEyeID(String eyeID) {
            this.eyeID = eyeID;
        }

        public String getDuration() {
            return duration;
        }

        public void setDuration(String duration) {
            this.duration = duration;
        }

        public String getSurgeryName() {
            return surgeryName;
        }

        public void setSurgeryName(String surgeryName) {
            this.surgeryName = surgeryName;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getEye() {
            return eye;
        }

        public void setEye(String eye) {
            this.eye = eye;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }
    }

    public class Investigation implements Serializable{

        @SerializedName("EyeID")
        @Expose
        private String eyeID;
        @SerializedName("Image")
        @Expose
        private String image;
        @SerializedName("InvestigationEye")
        @Expose
        private String investigationEye;
        @SerializedName("InvestigationID")
        @Expose
        private String investigationID;
        @SerializedName("LaboratoryTest")
        @Expose
        private String laboratoryTest;
        @SerializedName("LaboratoryTestID")
        @Expose
        private String laboratoryTestID;
        @SerializedName("OcularInvestigationID")
        @Expose
        private String ocularInvestigationID;
        @SerializedName("Ocularinvestigation")
        @Expose
        private String ocularinvestigation;

        public String getEyeID() {
            return eyeID;
        }

        public void setEyeID(String eyeID) {
            this.eyeID = eyeID;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getInvestigationEye() {
            return investigationEye;
        }

        public void setInvestigationEye(String investigationEye) {
            this.investigationEye = investigationEye;
        }

        public String getInvestigationID() {
            return investigationID;
        }

        public void setInvestigationID(String investigationID) {
            this.investigationID = investigationID;
        }

        public String getLaboratoryTest() {
            return laboratoryTest;
        }

        public void setLaboratoryTest(String laboratoryTest) {
            this.laboratoryTest = laboratoryTest;
        }

        public String getLaboratoryTestID() {
            return laboratoryTestID;
        }

        public void setLaboratoryTestID(String laboratoryTestID) {
            this.laboratoryTestID = laboratoryTestID;
        }

        public String getOcularInvestigationID() {
            return ocularInvestigationID;
        }

        public void setOcularInvestigationID(String ocularInvestigationID) {
            this.ocularInvestigationID = ocularInvestigationID;
        }

        public String getOcularinvestigation() {
            return ocularinvestigation;
        }

        public void setOcularinvestigation(String ocularinvestigation) {
            this.ocularinvestigation = ocularinvestigation;
        }
    }


    public class CounselingDetail implements Serializable{

        @SerializedName("CouncillorFormID")
        @Expose
        private String councillorFormID;
        @SerializedName("PatientFirstName")
        @Expose
        private String patientFirstName;
        @SerializedName("PatientLastName")
        @Expose
        private String patientLastName;
        @SerializedName("PatientMobileNo")
        @Expose
        private String patientMobileNo;
        @SerializedName("CouncillorName")
        @Expose
        private String councillorName;
        @SerializedName("CouncillorFirstName")
        @Expose
        private String councillorFirstName;
        @SerializedName("CouncillorLastName")
        @Expose
        private String councillorLastName;
        @SerializedName("CouncillorDate")
        @Expose
        private String councillorDate;
        @SerializedName("EyeID")
        @Expose
        private String eyeID;
        @SerializedName("DoctorID")
        @Expose
        private String doctorID;
        @SerializedName("CouncillorRemarks")
        @Expose
        private String councillorRemarks;
        @SerializedName("CouncillorID")
        @Expose
        private String councillorID;
        @SerializedName("Status")
        @Expose
        private String status;
        @SerializedName("PatientID")
        @Expose
        private String patientID;
        @SerializedName("DiseaseExplained")
        @Expose
        private String diseaseExplained;
        @SerializedName("SurgeryExplained")
        @Expose
        private String surgeryExplained;
        @SerializedName("OpetionSuggestedID")
        @Expose
        private String opetionSuggestedID;
        @SerializedName("PatientPreferredOptionID")
        @Expose
        private String patientPreferredOptionID;
        @SerializedName("Meclaim")
        @Expose
        private String meclaim;
        @SerializedName("AdmissionTime")
        @Expose
        private String admissionTime;
        @SerializedName("AdmissionDate")
        @Expose
        private String admissionDate;
        @SerializedName("ProfileImage")
        @Expose
        private String profileImage;
        @SerializedName("DoctorName")
        @Expose
        private String doctorName;
        @SerializedName("CouncillorSurgeryName")
        @Expose
        private String councillorSurgeryName;
        @SerializedName("OpetionSuggested")
        @Expose
        private String opetionSuggested;
        @SerializedName("PatientPreferredOption")
        @Expose
        private String patientPreferredOption;
        @SerializedName("CouncillorEye")
        @Expose
        private String councillorEye;

        @SerializedName("SurgeryType2")
        @Expose
        private String SurgeryType2;

        public String getCouncillorFormID() {
            return councillorFormID;
        }

        public void setCouncillorFormID(String councillorFormID) {
            this.councillorFormID = councillorFormID;
        }

        public String getPatientFirstName() {
            return patientFirstName;
        }

        public void setPatientFirstName(String patientFirstName) {
            this.patientFirstName = patientFirstName;
        }

        public String getPatientLastName() {
            return patientLastName;
        }

        public void setPatientLastName(String patientLastName) {
            this.patientLastName = patientLastName;
        }

        public String getPatientMobileNo() {
            return patientMobileNo;
        }

        public void setPatientMobileNo(String patientMobileNo) {
            this.patientMobileNo = patientMobileNo;
        }

        public String getCouncillorName() {
            return councillorName;
        }

        public void setCouncillorName(String councillorName) {
            this.councillorName = councillorName;
        }

        public String getCouncillorFirstName() {
            return councillorFirstName;
        }

        public void setCouncillorFirstName(String councillorFirstName) {
            this.councillorFirstName = councillorFirstName;
        }

        public String getCouncillorLastName() {
            return councillorLastName;
        }

        public void setCouncillorLastName(String councillorLastName) {
            this.councillorLastName = councillorLastName;
        }

        public String getCouncillorDate() {
            return councillorDate;
        }

        public void setCouncillorDate(String councillorDate) {
            this.councillorDate = councillorDate;
        }

        public String getEyeID() {
            return eyeID;
        }

        public void setEyeID(String eyeID) {
            this.eyeID = eyeID;
        }

        public String getDoctorID() {
            return doctorID;
        }

        public void setDoctorID(String doctorID) {
            this.doctorID = doctorID;
        }

        public String getCouncillorRemarks() {
            return councillorRemarks;
        }

        public void setCouncillorRemarks(String councillorRemarks) {
            this.councillorRemarks = councillorRemarks;
        }

        public String getCouncillorID() {
            return councillorID;
        }

        public void setCouncillorID(String councillorID) {
            this.councillorID = councillorID;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPatientID() {
            return patientID;
        }

        public void setPatientID(String patientID) {
            this.patientID = patientID;
        }

        public String getDiseaseExplained() {
            return diseaseExplained;
        }

        public void setDiseaseExplained(String diseaseExplained) {
            this.diseaseExplained = diseaseExplained;
        }

        public String getSurgeryExplained() {
            return surgeryExplained;
        }

        public void setSurgeryExplained(String surgeryExplained) {
            this.surgeryExplained = surgeryExplained;
        }

        public String getOpetionSuggestedID() {
            return opetionSuggestedID;
        }

        public void setOpetionSuggestedID(String opetionSuggestedID) {
            this.opetionSuggestedID = opetionSuggestedID;
        }

        public String getPatientPreferredOptionID() {
            return patientPreferredOptionID;
        }

        public void setPatientPreferredOptionID(String patientPreferredOptionID) {
            this.patientPreferredOptionID = patientPreferredOptionID;
        }

        public String getMeclaim() {
            return meclaim;
        }

        public void setMeclaim(String meclaim) {
            this.meclaim = meclaim;
        }

        public String getAdmissionTime() {
            return admissionTime;
        }

        public void setAdmissionTime(String admissionTime) {
            this.admissionTime = admissionTime;
        }

        public String getAdmissionDate() {
            return admissionDate;
        }

        public void setAdmissionDate(String admissionDate) {
            this.admissionDate = admissionDate;
        }

        public String getProfileImage() {
            return profileImage;
        }

        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        public String getDoctorName() {
            return doctorName;
        }

        public void setDoctorName(String doctorName) {
            this.doctorName = doctorName;
        }

        public String getCouncillorSurgeryName() {
            return councillorSurgeryName;
        }

        public void setCouncillorSurgeryName(String councillorSurgeryName) {
            this.councillorSurgeryName = councillorSurgeryName;
        }

        public String getOpetionSuggested() {
            return opetionSuggested;
        }

        public void setOpetionSuggested(String opetionSuggested) {
            this.opetionSuggested = opetionSuggested;
        }

        public String getPatientPreferredOption() {
            return patientPreferredOption;
        }

        public void setPatientPreferredOption(String patientPreferredOption) {
            this.patientPreferredOption = patientPreferredOption;
        }

        public String getCouncillorEye() {
            return councillorEye;
        }

        public void setCouncillorEye(String councillorEye) {
            this.councillorEye = councillorEye;
        }

        public String getSurgeryType2() {
            return SurgeryType2;
        }

        public void setSurgeryType2(String surgeryType2) {
            SurgeryType2 = surgeryType2;
        }
    }

    /**
     * Surgery Item
     */
    public class Surgery implements  Serializable{

        @SerializedName("SurgeryDate")
        @Expose
        private String surgeryDate;
        @SerializedName("EndDate")
        @Expose
        private String endDate;
        @SerializedName("StartTime")
        @Expose
        private String startTime;
        @SerializedName("EndTime")
        @Expose
        private String endTime;
        @SerializedName("SurgeryTypeID")
        @Expose
        private String surgeryTypeID;
        @SerializedName("SurgeryType2")
        @Expose
        private String surgeryType2;
        @SerializedName("ACD")
        @Expose
        private String aCD;
        @SerializedName("AL")
        @Expose
        private String aL;
        @SerializedName("IncisionSize")
        @Expose
        private String incisionSize;
        @SerializedName("IncisionType")
        @Expose
        private String incisionType;
        @SerializedName("SurgeryNotes")
        @Expose
        private String surgeryNotes;
        @SerializedName("Viscoelastics")
        @Expose
        private String viscoelastics;
        @SerializedName("SurgeryEyeID")
        @Expose
        private String surgeryEyeID;
        @SerializedName("Remarks")
        @Expose
        private String remarks;
        @SerializedName("SurgeryEye")
        @Expose
        private String surgeryEye;
        @SerializedName("SurgeryName")
        @Expose
        private String surgeryName;
        @SerializedName("Company")
        @Expose
        private String company;
        @SerializedName("TPA")
        @Expose
        private String tPA;
        @SerializedName("PolicyNo")
        @Expose
        private String policyNo;
        @SerializedName("ExpiryDate")
        @Expose
        private String expiryDate;
        @SerializedName("SettlementReceived")
        @Expose
        private String settlementReceived;
        @SerializedName("SettlementAmount")
        @Expose
        private String settlementAmount;
        @SerializedName("Pulse")
        @Expose
        private String pulse;
        @SerializedName("SystolicBP")
        @Expose
        private String systolicBP;
        @SerializedName("DiastolicBP")
        @Expose
        private String diastolicBP;
        @SerializedName("SPO2")
        @Expose
        private String sPO2;
        @SerializedName("RBS")
        @Expose
        private String rBS;
        @SerializedName("EyeID")
        @Expose
        private String eyeID;
        @SerializedName("AnaesthesiaEyeID")
        @Expose
        private String anaesthesiaEyeID;
        @SerializedName("Eye")
        @Expose
        private String eye;
        @SerializedName("AnaesthesiaTypeID")
        @Expose
        private String anaesthesiaTypeID;
        @SerializedName("AnaesthesiaType")
        @Expose
        private String anaesthesiaType;
        @SerializedName("AnaesthesiaMedicine")
        @Expose
        private String anaesthesiaMedicine;
        @SerializedName("ImplantBrand")
        @Expose
        private String implantBrand;
        @SerializedName("ImplantName")
        @Expose
        private String implantName;
        @SerializedName("A_Constant")
        @Expose
        private String aConstant;
        @SerializedName("ImplantPower")
        @Expose
        private String implantPower;
        @SerializedName("ImplantPlacement")
        @Expose
        private String implantPlacement;
        @SerializedName("ImplantExpiry")
        @Expose
        private String implantExpiry;
        @SerializedName("ImplantSlNo")
        @Expose
        private String implantSlNo;
        @SerializedName("Notes")
        @Expose
        private String notes;

        public String getSurgeryDate() {
            return surgeryDate;
        }

        public void setSurgeryDate(String surgeryDate) {
            this.surgeryDate = surgeryDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public String getSurgeryTypeID() {
            return surgeryTypeID;
        }

        public void setSurgeryTypeID(String surgeryTypeID) {
            this.surgeryTypeID = surgeryTypeID;
        }

        public String getSurgeryType2() {
            return surgeryType2;
        }

        public void setSurgeryType2(String surgeryType2) {
            this.surgeryType2 = surgeryType2;
        }

        public String getACD() {
            return aCD;
        }

        public void setACD(String aCD) {
            this.aCD = aCD;
        }

        public String getAL() {
            return aL;
        }

        public void setAL(String aL) {
            this.aL = aL;
        }

        public String getIncisionSize() {
            return incisionSize;
        }

        public void setIncisionSize(String incisionSize) {
            this.incisionSize = incisionSize;
        }

        public String getIncisionType() {
            return incisionType;
        }

        public void setIncisionType(String incisionType) {
            this.incisionType = incisionType;
        }

        public String getSurgeryNotes() {
            return surgeryNotes;
        }

        public void setSurgeryNotes(String surgeryNotes) {
            this.surgeryNotes = surgeryNotes;
        }

        public String getViscoelastics() {
            return viscoelastics;
        }

        public void setViscoelastics(String viscoelastics) {
            this.viscoelastics = viscoelastics;
        }

        public String getSurgeryEyeID() {
            return surgeryEyeID;
        }

        public void setSurgeryEyeID(String surgeryEyeID) {
            this.surgeryEyeID = surgeryEyeID;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public String getSurgeryEye() {
            return surgeryEye;
        }

        public void setSurgeryEye(String surgeryEye) {
            this.surgeryEye = surgeryEye;
        }

        public String getSurgeryName() {
            return surgeryName;
        }

        public void setSurgeryName(String surgeryName) {
            this.surgeryName = surgeryName;
        }

        public String getCompany() {
            return company;
        }

        public void setCompany(String company) {
            this.company = company;
        }

        public String getTPA() {
            return tPA;
        }

        public void setTPA(String tPA) {
            this.tPA = tPA;
        }

        public String getPolicyNo() {
            return policyNo;
        }

        public void setPolicyNo(String policyNo) {
            this.policyNo = policyNo;
        }

        public String getExpiryDate() {
            return expiryDate;
        }

        public void setExpiryDate(String expiryDate) {
            this.expiryDate = expiryDate;
        }

        public String getSettlementReceived() {
            return settlementReceived;
        }

        public void setSettlementReceived(String settlementReceived) {
            this.settlementReceived = settlementReceived;
        }

        public String getSettlementAmount() {
            return settlementAmount;
        }

        public void setSettlementAmount(String settlementAmount) {
            this.settlementAmount = settlementAmount;
        }

        public String getPulse() {
            return pulse;
        }

        public void setPulse(String pulse) {
            this.pulse = pulse;
        }

        public String getSystolicBP() {
            return systolicBP;
        }

        public void setSystolicBP(String systolicBP) {
            this.systolicBP = systolicBP;
        }

        public String getDiastolicBP() {
            return diastolicBP;
        }

        public void setDiastolicBP(String diastolicBP) {
            this.diastolicBP = diastolicBP;
        }

        public String getSPO2() {
            return sPO2;
        }

        public void setSPO2(String sPO2) {
            this.sPO2 = sPO2;
        }

        public String getRBS() {
            return rBS;
        }

        public void setRBS(String rBS) {
            this.rBS = rBS;
        }

        public String getEyeID() {
            return eyeID;
        }

        public void setEyeID(String eyeID) {
            this.eyeID = eyeID;
        }

        public String getAnaesthesiaEyeID() {
            return anaesthesiaEyeID;
        }

        public void setAnaesthesiaEyeID(String anaesthesiaEyeID) {
            this.anaesthesiaEyeID = anaesthesiaEyeID;
        }

        public String getEye() {
            return eye;
        }

        public void setEye(String eye) {
            this.eye = eye;
        }

        public String getAnaesthesiaTypeID() {
            return anaesthesiaTypeID;
        }

        public void setAnaesthesiaTypeID(String anaesthesiaTypeID) {
            this.anaesthesiaTypeID = anaesthesiaTypeID;
        }

        public String getAnaesthesiaType() {
            return anaesthesiaType;
        }

        public void setAnaesthesiaType(String anaesthesiaType) {
            this.anaesthesiaType = anaesthesiaType;
        }

        public String getAnaesthesiaMedicine() {
            return anaesthesiaMedicine;
        }

        public void setAnaesthesiaMedicine(String anaesthesiaMedicine) {
            this.anaesthesiaMedicine = anaesthesiaMedicine;
        }

        public String getImplantBrand() {
            return implantBrand;
        }

        public void setImplantBrand(String implantBrand) {
            this.implantBrand = implantBrand;
        }

        public String getImplantName() {
            return implantName;
        }

        public void setImplantName(String implantName) {
            this.implantName = implantName;
        }

        public String getAConstant() {
            return aConstant;
        }

        public void setAConstant(String aConstant) {
            this.aConstant = aConstant;
        }

        public String getImplantPower() {
            return implantPower;
        }

        public void setImplantPower(String implantPower) {
            this.implantPower = implantPower;
        }

        public String getImplantPlacement() {
            return implantPlacement;
        }

        public void setImplantPlacement(String implantPlacement) {
            this.implantPlacement = implantPlacement;
        }

        public String getImplantExpiry() {
            return implantExpiry;
        }

        public void setImplantExpiry(String implantExpiry) {
            this.implantExpiry = implantExpiry;
        }

        public String getImplantSlNo() {
            return implantSlNo;
        }

        public void setImplantSlNo(String implantSlNo) {
            this.implantSlNo = implantSlNo;
        }

        public String getNotes() {
            return notes;
        }

        public void setNotes(String notes) {
            this.notes = notes;
        }
    }

    public class Vision implements Serializable{

        @SerializedName("UCVADR")
        @Expose
        private String uCVADR;
        @SerializedName("UCVADL")
        @Expose
        private String uCVADL;
        @SerializedName("UCVANR")
        @Expose
        private String uCVANR;
        @SerializedName("UCVANL")
        @Expose
        private String uCVANL;
        @SerializedName("UCVARemarks")
        @Expose
        private String uCVARemarks;
        @SerializedName("BCVAUDR")
        @Expose
        private String bCVAUDR;
        @SerializedName("BCVAUNR")
        @Expose
        private String bCVAUNR;
        @SerializedName("BCVAUDL")
        @Expose
        private String bCVAUDL;
        @SerializedName("BCVAUNL")
        @Expose
        private String bCVAUNL;
        @SerializedName("BCVADDR")
        @Expose
        private String bCVADDR;
        @SerializedName("BCVADNR")
        @Expose
        private String bCVADNR;
        @SerializedName("BCVADDL")
        @Expose
        private String bCVADDL;
        @SerializedName("BCVADNL")
        @Expose
        private String bCVADNL;
        @SerializedName("BCVARemarks")
        @Expose
        private String bCVARemarks;
        @SerializedName("RefUDRSph")
        @Expose
        private String refUDRSph;
        @SerializedName("RefUDLSph")
        @Expose
        private String refUDLSph;
        @SerializedName("RefUNRSph")
        @Expose
        private String refUNRSph;
        @SerializedName("RefUNLSph")
        @Expose
        private String refUNLSph;
        @SerializedName("RefUDRCyl")
        @Expose
        private String refUDRCyl;
        @SerializedName("RefUNRCyl")
        @Expose
        private String refUNRCyl;
        @SerializedName("RefUDLCyl")
        @Expose
        private String refUDLCyl;
        @SerializedName("RefUNLCyl")
        @Expose
        private String refUNLCyl;
        @SerializedName("RefUDRAxis")
        @Expose
        private String refUDRAxis;
        @SerializedName("RefUDLAxis")
        @Expose
        private String refUDLAxis;
        @SerializedName("RefUNRAxis")
        @Expose
        private String refUNRAxis;
        @SerializedName("RefUNLAxis")
        @Expose
        private String refUNLAxis;
        @SerializedName("RefUDRVA")
        @Expose
        private String refUDRVA;
        @SerializedName("RefUDLVA")
        @Expose
        private String refUDLVA;
        @SerializedName("RefUNRVA")
        @Expose
        private String refUNRVA;
        @SerializedName("RefUNLVA")
        @Expose
        private String refUNLVA;
        @SerializedName("RefDDRSph")
        @Expose
        private String refDDRSph;
        @SerializedName("RefDDLSph")
        @Expose
        private String refDDLSph;
        @SerializedName("RefDNRSph")
        @Expose
        private String refDNRSph;
        @SerializedName("RefDNLSph")
        @Expose
        private String refDNLSph;
        @SerializedName("RefDDRCyl")
        @Expose
        private String refDDRCyl;
        @SerializedName("RefDNRCyl")
        @Expose
        private String refDNRCyl;
        @SerializedName("RefDDLCyl")
        @Expose
        private String refDDLCyl;
        @SerializedName("RefDNLCyl")
        @Expose
        private String refDNLCyl;
        @SerializedName("RefDDRAxis")
        @Expose
        private String refDDRAxis;
        @SerializedName("RefDDLAxis")
        @Expose
        private String refDDLAxis;
        @SerializedName("RefDNRAxis")
        @Expose
        private String refDNRAxis;
        @SerializedName("RefDNLAxis")
        @Expose
        private String refDNLAxis;
        @SerializedName("RefDDRVA")
        @Expose
        private String refDDRVA;
        @SerializedName("RefDDLVA")
        @Expose
        private String refDDLVA;
        @SerializedName("RefDNRVA")
        @Expose
        private String refDNRVA;
        @SerializedName("RefDNLVA")
        @Expose
        private String refDNLVA;
        @SerializedName("RefFDRSph")
        @Expose
        private String refFDRSph;
        @SerializedName("RefFDLSph")
        @Expose
        private String refFDLSph;
        @SerializedName("RefFNRSph")
        @Expose
        private String refFNRSph;
        @SerializedName("RefFNLSph")
        @Expose
        private String refFNLSph;
        @SerializedName("RefFDRCyl")
        @Expose
        private String refFDRCyl;
        @SerializedName("RefFNRCyl")
        @Expose
        private String refFNRCyl;
        @SerializedName("RefFDLCyl")
        @Expose
        private String refFDLCyl;
        @SerializedName("RefFNLCyl")
        @Expose
        private String refFNLCyl;
        @SerializedName("RefFDRAxis")
        @Expose
        private String refFDRAxis;
        @SerializedName("RefFDLAxis")
        @Expose
        private String refFDLAxis;
        @SerializedName("RefFNRAxis")
        @Expose
        private String refFNRAxis;
        @SerializedName("RefFNLAxis")
        @Expose
        private String refFNLAxis;
        @SerializedName("RefFDRVA")
        @Expose
        private String refFDRVA;
        @SerializedName("RefFDLVA")
        @Expose
        private String refFDLVA;
        @SerializedName("RefFNRVA")
        @Expose
        private String refFNRVA;
        @SerializedName("RefFNLVA")
        @Expose
        private String refFNLVA;
        @SerializedName("IPD")
        @Expose
        private String iPD;
        @SerializedName("K1RPower")
        @Expose
        private String k1RPower;
        @SerializedName("K1RAxis")
        @Expose
        private String k1RAxis;
        @SerializedName("K1LPower")
        @Expose
        private String k1LPower;
        @SerializedName("K1LAxis")
        @Expose
        private String k1LAxis;
        @SerializedName("K2RPower")
        @Expose
        private String k2RPower;
        @SerializedName("K2RAxis")
        @Expose
        private String k2RAxis;
        @SerializedName("K2LPower")
        @Expose
        private String k2LPower;
        @SerializedName("K2LAxis")
        @Expose
        private String k2LAxis;

        public String getUCVADR() {
            return uCVADR;
        }

        public void setUCVADR(String uCVADR) {
            this.uCVADR = uCVADR;
        }

        public String getUCVADL() {
            return uCVADL;
        }

        public void setUCVADL(String uCVADL) {
            this.uCVADL = uCVADL;
        }

        public String getUCVANR() {
            return uCVANR;
        }

        public void setUCVANR(String uCVANR) {
            this.uCVANR = uCVANR;
        }

        public String getUCVANL() {
            return uCVANL;
        }

        public void setUCVANL(String uCVANL) {
            this.uCVANL = uCVANL;
        }

        public String getUCVARemarks() {
            return uCVARemarks;
        }

        public void setUCVARemarks(String uCVARemarks) {
            this.uCVARemarks = uCVARemarks;
        }

        public String getBCVAUDR() {
            return bCVAUDR;
        }

        public void setBCVAUDR(String bCVAUDR) {
            this.bCVAUDR = bCVAUDR;
        }

        public String getBCVAUNR() {
            return bCVAUNR;
        }

        public void setBCVAUNR(String bCVAUNR) {
            this.bCVAUNR = bCVAUNR;
        }

        public String getBCVAUDL() {
            return bCVAUDL;
        }

        public void setBCVAUDL(String bCVAUDL) {
            this.bCVAUDL = bCVAUDL;
        }

        public String getBCVAUNL() {
            return bCVAUNL;
        }

        public void setBCVAUNL(String bCVAUNL) {
            this.bCVAUNL = bCVAUNL;
        }

        public String getBCVADDR() {
            return bCVADDR;
        }

        public void setBCVADDR(String bCVADDR) {
            this.bCVADDR = bCVADDR;
        }

        public String getBCVADNR() {
            return bCVADNR;
        }

        public void setBCVADNR(String bCVADNR) {
            this.bCVADNR = bCVADNR;
        }

        public String getBCVADDL() {
            return bCVADDL;
        }

        public void setBCVADDL(String bCVADDL) {
            this.bCVADDL = bCVADDL;
        }

        public String getBCVADNL() {
            return bCVADNL;
        }

        public void setBCVADNL(String bCVADNL) {
            this.bCVADNL = bCVADNL;
        }

        public String getBCVARemarks() {
            return bCVARemarks;
        }

        public void setBCVARemarks(String bCVARemarks) {
            this.bCVARemarks = bCVARemarks;
        }

        public String getRefUDRSph() {
            return refUDRSph;
        }

        public void setRefUDRSph(String refUDRSph) {
            this.refUDRSph = refUDRSph;
        }

        public String getRefUDLSph() {
            return refUDLSph;
        }

        public void setRefUDLSph(String refUDLSph) {
            this.refUDLSph = refUDLSph;
        }

        public String getRefUNRSph() {
            return refUNRSph;
        }

        public void setRefUNRSph(String refUNRSph) {
            this.refUNRSph = refUNRSph;
        }

        public String getRefUNLSph() {
            return refUNLSph;
        }

        public void setRefUNLSph(String refUNLSph) {
            this.refUNLSph = refUNLSph;
        }

        public String getRefUDRCyl() {
            return refUDRCyl;
        }

        public void setRefUDRCyl(String refUDRCyl) {
            this.refUDRCyl = refUDRCyl;
        }

        public String getRefUNRCyl() {
            return refUNRCyl;
        }

        public void setRefUNRCyl(String refUNRCyl) {
            this.refUNRCyl = refUNRCyl;
        }

        public String getRefUDLCyl() {
            return refUDLCyl;
        }

        public void setRefUDLCyl(String refUDLCyl) {
            this.refUDLCyl = refUDLCyl;
        }

        public String getRefUNLCyl() {
            return refUNLCyl;
        }

        public void setRefUNLCyl(String refUNLCyl) {
            this.refUNLCyl = refUNLCyl;
        }

        public String getRefUDRAxis() {
            return refUDRAxis;
        }

        public void setRefUDRAxis(String refUDRAxis) {
            this.refUDRAxis = refUDRAxis;
        }

        public String getRefUDLAxis() {
            return refUDLAxis;
        }

        public void setRefUDLAxis(String refUDLAxis) {
            this.refUDLAxis = refUDLAxis;
        }

        public String getRefUNRAxis() {
            return refUNRAxis;
        }

        public void setRefUNRAxis(String refUNRAxis) {
            this.refUNRAxis = refUNRAxis;
        }

        public String getRefUNLAxis() {
            return refUNLAxis;
        }

        public void setRefUNLAxis(String refUNLAxis) {
            this.refUNLAxis = refUNLAxis;
        }

        public String getRefUDRVA() {
            return refUDRVA;
        }

        public void setRefUDRVA(String refUDRVA) {
            this.refUDRVA = refUDRVA;
        }

        public String getRefUDLVA() {
            return refUDLVA;
        }

        public void setRefUDLVA(String refUDLVA) {
            this.refUDLVA = refUDLVA;
        }

        public String getRefUNRVA() {
            return refUNRVA;
        }

        public void setRefUNRVA(String refUNRVA) {
            this.refUNRVA = refUNRVA;
        }

        public String getRefUNLVA() {
            return refUNLVA;
        }

        public void setRefUNLVA(String refUNLVA) {
            this.refUNLVA = refUNLVA;
        }

        public String getRefDDRSph() {
            return refDDRSph;
        }

        public void setRefDDRSph(String refDDRSph) {
            this.refDDRSph = refDDRSph;
        }

        public String getRefDDLSph() {
            return refDDLSph;
        }

        public void setRefDDLSph(String refDDLSph) {
            this.refDDLSph = refDDLSph;
        }

        public String getRefDNRSph() {
            return refDNRSph;
        }

        public void setRefDNRSph(String refDNRSph) {
            this.refDNRSph = refDNRSph;
        }

        public String getRefDNLSph() {
            return refDNLSph;
        }

        public void setRefDNLSph(String refDNLSph) {
            this.refDNLSph = refDNLSph;
        }

        public String getRefDDRCyl() {
            return refDDRCyl;
        }

        public void setRefDDRCyl(String refDDRCyl) {
            this.refDDRCyl = refDDRCyl;
        }

        public String getRefDNRCyl() {
            return refDNRCyl;
        }

        public void setRefDNRCyl(String refDNRCyl) {
            this.refDNRCyl = refDNRCyl;
        }

        public String getRefDDLCyl() {
            return refDDLCyl;
        }

        public void setRefDDLCyl(String refDDLCyl) {
            this.refDDLCyl = refDDLCyl;
        }

        public String getRefDNLCyl() {
            return refDNLCyl;
        }

        public void setRefDNLCyl(String refDNLCyl) {
            this.refDNLCyl = refDNLCyl;
        }

        public String getRefDDRAxis() {
            return refDDRAxis;
        }

        public void setRefDDRAxis(String refDDRAxis) {
            this.refDDRAxis = refDDRAxis;
        }

        public String getRefDDLAxis() {
            return refDDLAxis;
        }

        public void setRefDDLAxis(String refDDLAxis) {
            this.refDDLAxis = refDDLAxis;
        }

        public String getRefDNRAxis() {
            return refDNRAxis;
        }

        public void setRefDNRAxis(String refDNRAxis) {
            this.refDNRAxis = refDNRAxis;
        }

        public String getRefDNLAxis() {
            return refDNLAxis;
        }

        public void setRefDNLAxis(String refDNLAxis) {
            this.refDNLAxis = refDNLAxis;
        }

        public String getRefDDRVA() {
            return refDDRVA;
        }

        public void setRefDDRVA(String refDDRVA) {
            this.refDDRVA = refDDRVA;
        }

        public String getRefDDLVA() {
            return refDDLVA;
        }

        public void setRefDDLVA(String refDDLVA) {
            this.refDDLVA = refDDLVA;
        }

        public String getRefDNRVA() {
            return refDNRVA;
        }

        public void setRefDNRVA(String refDNRVA) {
            this.refDNRVA = refDNRVA;
        }

        public String getRefDNLVA() {
            return refDNLVA;
        }

        public void setRefDNLVA(String refDNLVA) {
            this.refDNLVA = refDNLVA;
        }

        public String getRefFDRSph() {
            return refFDRSph;
        }

        public void setRefFDRSph(String refFDRSph) {
            this.refFDRSph = refFDRSph;
        }

        public String getRefFDLSph() {
            return refFDLSph;
        }

        public void setRefFDLSph(String refFDLSph) {
            this.refFDLSph = refFDLSph;
        }

        public String getRefFNRSph() {
            return refFNRSph;
        }

        public void setRefFNRSph(String refFNRSph) {
            this.refFNRSph = refFNRSph;
        }

        public String getRefFNLSph() {
            return refFNLSph;
        }

        public void setRefFNLSph(String refFNLSph) {
            this.refFNLSph = refFNLSph;
        }

        public String getRefFDRCyl() {
            return refFDRCyl;
        }

        public void setRefFDRCyl(String refFDRCyl) {
            this.refFDRCyl = refFDRCyl;
        }

        public String getRefFNRCyl() {
            return refFNRCyl;
        }

        public void setRefFNRCyl(String refFNRCyl) {
            this.refFNRCyl = refFNRCyl;
        }

        public String getRefFDLCyl() {
            return refFDLCyl;
        }

        public void setRefFDLCyl(String refFDLCyl) {
            this.refFDLCyl = refFDLCyl;
        }

        public String getRefFNLCyl() {
            return refFNLCyl;
        }

        public void setRefFNLCyl(String refFNLCyl) {
            this.refFNLCyl = refFNLCyl;
        }

        public String getRefFDRAxis() {
            return refFDRAxis;
        }

        public void setRefFDRAxis(String refFDRAxis) {
            this.refFDRAxis = refFDRAxis;
        }

        public String getRefFDLAxis() {
            return refFDLAxis;
        }

        public void setRefFDLAxis(String refFDLAxis) {
            this.refFDLAxis = refFDLAxis;
        }

        public String getRefFNRAxis() {
            return refFNRAxis;
        }

        public void setRefFNRAxis(String refFNRAxis) {
            this.refFNRAxis = refFNRAxis;
        }

        public String getRefFNLAxis() {
            return refFNLAxis;
        }

        public void setRefFNLAxis(String refFNLAxis) {
            this.refFNLAxis = refFNLAxis;
        }

        public String getRefFDRVA() {
            return refFDRVA;
        }

        public void setRefFDRVA(String refFDRVA) {
            this.refFDRVA = refFDRVA;
        }

        public String getRefFDLVA() {
            return refFDLVA;
        }

        public void setRefFDLVA(String refFDLVA) {
            this.refFDLVA = refFDLVA;
        }

        public String getRefFNRVA() {
            return refFNRVA;
        }

        public void setRefFNRVA(String refFNRVA) {
            this.refFNRVA = refFNRVA;
        }

        public String getRefFNLVA() {
            return refFNLVA;
        }

        public void setRefFNLVA(String refFNLVA) {
            this.refFNLVA = refFNLVA;
        }

        public String getIPD() {
            return iPD;
        }

        public void setIPD(String iPD) {
            this.iPD = iPD;
        }

        public String getK1RPower() {
            return k1RPower;
        }

        public void setK1RPower(String k1RPower) {
            this.k1RPower = k1RPower;
        }

        public String getK1RAxis() {
            return k1RAxis;
        }

        public void setK1RAxis(String k1RAxis) {
            this.k1RAxis = k1RAxis;
        }

        public String getK1LPower() {
            return k1LPower;
        }

        public void setK1LPower(String k1LPower) {
            this.k1LPower = k1LPower;
        }

        public String getK1LAxis() {
            return k1LAxis;
        }

        public void setK1LAxis(String k1LAxis) {
            this.k1LAxis = k1LAxis;
        }

        public String getK2RPower() {
            return k2RPower;
        }

        public void setK2RPower(String k2RPower) {
            this.k2RPower = k2RPower;
        }

        public String getK2RAxis() {
            return k2RAxis;
        }

        public void setK2RAxis(String k2RAxis) {
            this.k2RAxis = k2RAxis;
        }

        public String getK2LPower() {
            return k2LPower;
        }

        public void setK2LPower(String k2LPower) {
            this.k2LPower = k2LPower;
        }

        public String getK2LAxis() {
            return k2LAxis;
        }

        public void setK2LAxis(String k2LAxis) {
            this.k2LAxis = k2LAxis;
        }
    }


    /**
     * Set Pre Examination Data
     */
    public class PreExamination implements Serializable {

        @SerializedName("RNCT")
        @Expose
        private String rNCT;
        @SerializedName("LNCT")
        @Expose
        private String lNCT;
        @SerializedName("RAT")
        @Expose
        private String rAT;
        @SerializedName("LAT")
        @Expose
        private String lAT;
        @SerializedName("RPachymetry")
        @Expose
        private String rPachymetry;
        @SerializedName("LPachymetry")
        @Expose
        private String lPachymetry;
        @SerializedName("RSchirmer")
        @Expose
        private String rSchirmer;
        @SerializedName("LSchirmer")
        @Expose
        private String lSchirmer;

        public String getRNCT() {
            return rNCT;
        }

        public void setRNCT(String rNCT) {
            this.rNCT = rNCT;
        }

        public String getLNCT() {
            return lNCT;
        }

        public void setLNCT(String lNCT) {
            this.lNCT = lNCT;
        }

        public String getRAT() {
            return rAT;
        }

        public void setRAT(String rAT) {
            this.rAT = rAT;
        }

        public String getLAT() {
            return lAT;
        }

        public void setLAT(String lAT) {
            this.lAT = lAT;
        }

        public String getRPachymetry() {
            return rPachymetry;
        }

        public void setRPachymetry(String rPachymetry) {
            this.rPachymetry = rPachymetry;
        }

        public String getLPachymetry() {
            return lPachymetry;
        }

        public void setLPachymetry(String lPachymetry) {
            this.lPachymetry = lPachymetry;
        }

        public String getRSchirmer() {
            return rSchirmer;
        }

        public void setRSchirmer(String rSchirmer) {
            this.rSchirmer = rSchirmer;
        }

        public String getLSchirmer() {
            return lSchirmer;
        }

        public void setLSchirmer(String lSchirmer) {
            this.lSchirmer = lSchirmer;
        }
    }

    public class HistoryAndComplain {

        @SerializedName("PrimaryComplains")
        @Expose
        private String primaryComplains;
        @SerializedName("OcularHistory")
        @Expose
        private String ocularHistory;
        @SerializedName("SystemicHistory")
        @Expose
        private String systemicHistory;
        @SerializedName("FamilyOcularHistory")
        @Expose
        private String familyOcularHistory;

        public String getPrimaryComplains() {
            return primaryComplains;
        }

        public void setPrimaryComplains(String primaryComplains) {
            this.primaryComplains = primaryComplains;
        }

        public String getOcularHistory() {
            return ocularHistory;
        }

        public void setOcularHistory(String ocularHistory) {
            this.ocularHistory = ocularHistory;
        }

        public String getSystemicHistory() {
            return systemicHistory;
        }

        public void setSystemicHistory(String systemicHistory) {
            this.systemicHistory = systemicHistory;
        }

        public String getFamilyOcularHistory() {
            return familyOcularHistory;
        }

        public void setFamilyOcularHistory(String familyOcularHistory) {
            this.familyOcularHistory = familyOcularHistory;
        }
    }

    public class Doodle {

        @SerializedName("DoodleImageID")
        @Expose
        private String doodleImageID;
        @SerializedName("AppointmentID")
        @Expose
        private String appointmentID;
        @SerializedName("LeftAdnex")
        @Expose
        private String leftAdnex;
        @SerializedName("RightAdnex")
        @Expose
        private String rightAdnex;
        @SerializedName("LeftAnteriorSegment")
        @Expose
        private String leftAnteriorSegment;
        @SerializedName("RightAnteriorSegment")
        @Expose
        private String rightAnteriorSegment;
        @SerializedName("LeftLens")
        @Expose
        private String leftLens;
        @SerializedName("RightLens")
        @Expose
        private String rightLens;
        @SerializedName("LeftFundus")
        @Expose
        private String leftFundus;
        @SerializedName("RightFundus")
        @Expose
        private String rightFundus;
        @SerializedName("LeftGonioscopy")
        @Expose
        private String leftGonioscopy;
        @SerializedName("RightGonioscopy")
        @Expose
        private String rightGonioscopy;
        @SerializedName("Status")
        @Expose
        private String status;

        public String getDoodleImageID() {
            return doodleImageID;
        }

        public void setDoodleImageID(String doodleImageID) {
            this.doodleImageID = doodleImageID;
        }

        public String getAppointmentID() {
            return appointmentID;
        }

        public void setAppointmentID(String appointmentID) {
            this.appointmentID = appointmentID;
        }

        public String getLeftAdnex() {
            return leftAdnex;
        }

        public void setLeftAdnex(String leftAdnex) {
            this.leftAdnex = leftAdnex;
        }

        public String getRightAdnex() {
            return rightAdnex;
        }

        public void setRightAdnex(String rightAdnex) {
            this.rightAdnex = rightAdnex;
        }

        public String getLeftAnteriorSegment() {
            return leftAnteriorSegment;
        }

        public void setLeftAnteriorSegment(String leftAnteriorSegment) {
            this.leftAnteriorSegment = leftAnteriorSegment;
        }

        public String getRightAnteriorSegment() {
            return rightAnteriorSegment;
        }

        public void setRightAnteriorSegment(String rightAnteriorSegment) {
            this.rightAnteriorSegment = rightAnteriorSegment;
        }

        public String getLeftLens() {
            return leftLens;
        }

        public void setLeftLens(String leftLens) {
            this.leftLens = leftLens;
        }

        public String getRightLens() {
            return rightLens;
        }

        public void setRightLens(String rightLens) {
            this.rightLens = rightLens;
        }

        public String getLeftFundus() {
            return leftFundus;
        }

        public void setLeftFundus(String leftFundus) {
            this.leftFundus = leftFundus;
        }

        public String getRightFundus() {
            return rightFundus;
        }

        public void setRightFundus(String rightFundus) {
            this.rightFundus = rightFundus;
        }

        public String getLeftGonioscopy() {
            return leftGonioscopy;
        }

        public void setLeftGonioscopy(String leftGonioscopy) {
            this.leftGonioscopy = leftGonioscopy;
        }

        public String getRightGonioscopy() {
            return rightGonioscopy;
        }

        public void setRightGonioscopy(String rightGonioscopy) {
            this.rightGonioscopy = rightGonioscopy;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    public class Medication {

        @SerializedName("MedicationID")
        @Expose
        private String medicationID;
        @SerializedName("AppointmentID")
        @Expose
        private String appointmentID;
        @SerializedName("MedicationName")
        @Expose
        private String medicationName;
        @SerializedName("TotalTimes")
        @Expose
        private String totalTimes;
        @SerializedName("Type")
        @Expose
        private String type;
        @SerializedName("StartDate")
        @Expose
        private String startDate;
        @SerializedName("EndDate")
        @Expose
        private String endDate;
        @SerializedName("Description")
        @Expose
        private String description;
        @SerializedName("dosage")
        @Expose
        private List<Dosage> dosage = null;

        public String getMedicationID() {
            return medicationID;
        }

        public void setMedicationID(String medicationID) {
            this.medicationID = medicationID;
        }

        public String getAppointmentID() {
            return appointmentID;
        }

        public void setAppointmentID(String appointmentID) {
            this.appointmentID = appointmentID;
        }

        public String getMedicationName() {
            return medicationName;
        }

        public void setMedicationName(String medicationName) {
            this.medicationName = medicationName;
        }

        public String getTotalTimes() {
            return totalTimes;
        }

        public void setTotalTimes(String totalTimes) {
            this.totalTimes = totalTimes;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getStartDate() {
            return startDate;
        }

        public void setStartDate(String startDate) {
            this.startDate = startDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public List<Dosage> getDosage() {
            return dosage;
        }

        public void setDosage(List<Dosage> dosage) {
            this.dosage = dosage;
        }
    }

    public class Dosage {

        @SerializedName("Dosage")
        @Expose
        private String dosage;
        @SerializedName("Description")
        @Expose
        private String description;

        public String getDosage() {
            return dosage;
        }

        public void setDosage(String dosage) {
            this.dosage = dosage;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

    }
}
