package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PreliminaryExaminationDetailsModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data implements Serializable {

        @SerializedName("FamilyOcularHistory")
        @Expose
        private ArrayList<FamilyOcularHistory> familyOcularHistory = null;
        @SerializedName("SystemicHistory")
        @Expose
        private ArrayList<SystemicHistory> systemicHistory = null;
        @SerializedName("PrimaryComplains")
        @Expose
        private ArrayList<PrimaryComplain> primaryComplains = null;
        @SerializedName("AnatomicalLocation")
        @Expose
        private ArrayList<AnatomicalLocation> anatomicalLocation = null;
        @SerializedName("Eye")
        @Expose
        private ArrayList<Eye> eye = null;
        @SerializedName("EyeLence")
        @Expose
        private ArrayList<EyeLence> eyeLence = null;
        @SerializedName("Type")
        @Expose
        private ArrayList<Type> type = null;
        @SerializedName("OcularHistory")
        @Expose
        private ArrayList<OcularHistory> ocularHistory = null;
        @SerializedName("IPD")
        @Expose
        private ArrayList<IPD> ipd = null;
        @SerializedName("Distance")
        @Expose
        private ArrayList<Distance> distance = null;
        @SerializedName("Near")
        @Expose
        private ArrayList<Near> near = null;
        @SerializedName("AnaesthesiaType")
        @Expose
        private List<AnaesthesiaType> anaesthesiaType = null;

        @SerializedName("SurgeryType")
        @Expose
        private List<SurgeryType> surgeryType = null;

        @SerializedName("OptionSuggested")
        @Expose
        private List<OptionSuggested> optionSuggested = null;

        @SerializedName("Category")
        @Expose
        private List<Category> category = null;
        @SerializedName("PatternType")
        @Expose
        private List<PatternType> patternType = null;
        @SerializedName("Stage")
        @Expose
        private List<Stage> stage = null;
        @SerializedName("SideOfBrain")
        @Expose
        private List<SideOfBrain> sideOfBrain = null;
        @SerializedName("CataractType")
        @Expose
        private List<CataractType> cataractType = null;
        @SerializedName("LaboratoryTest")
        @Expose
        private List<LaboratoryTest> laboratoryTest = null;
        @SerializedName("Ocularinvestigation")
        @Expose
        private List<Ocularinvestigation> ocularinvestigation = null;

        @SerializedName("ImplantName")
        @Expose
        private List<ImplantName> implantName = null;
        @SerializedName("ImplantBrand")
        @Expose
        private List<ImplantBrand> implantBrand = null;

        public List<ImplantName> getImplantName() {
            return implantName;
        }

        public void setImplantName(List<ImplantName> implantName) {
            this.implantName = implantName;
        }

        public List<ImplantBrand> getImplantBrand() {
            return implantBrand;
        }

        public void setImplantBrand(List<ImplantBrand> implantBrand) {
            this.implantBrand = implantBrand;
        }

        public ArrayList<FamilyOcularHistory> getFamilyOcularHistory() {
            return familyOcularHistory;
        }

        public void setFamilyOcularHistory(ArrayList<FamilyOcularHistory> familyOcularHistory) {
            this.familyOcularHistory = familyOcularHistory;
        }

        public ArrayList<SystemicHistory> getSystemicHistory() {
            return systemicHistory;
        }

        public void setSystemicHistory(ArrayList<SystemicHistory> systemicHistory) {
            this.systemicHistory = systemicHistory;
        }

        public ArrayList<PrimaryComplain> getPrimaryComplains() {
            return primaryComplains;
        }

        public void setPrimaryComplains(ArrayList<PrimaryComplain> primaryComplains) {
            this.primaryComplains = primaryComplains;
        }

        public ArrayList<AnatomicalLocation> getAnatomicalLocation() {
            return anatomicalLocation;
        }

        public void setAnatomicalLocation(ArrayList<AnatomicalLocation> anatomicalLocation) {
            this.anatomicalLocation = anatomicalLocation;
        }

        public ArrayList<Eye> getEye() {
            return eye;
        }

        public void setEye(ArrayList<Eye> eye) {
            this.eye = eye;
        }

        public ArrayList<EyeLence> getEyeLence() {
            return eyeLence;
        }

        public void setEyeLence(ArrayList<EyeLence> eyeLence) {
            this.eyeLence = eyeLence;
        }

        public ArrayList<Type> getType() {
            return type;
        }

        public void setType(ArrayList<Type> type) {
            this.type = type;
        }

        public ArrayList<OcularHistory> getOcularHistory() {
            return ocularHistory;
        }

        public void setOcularHistory(ArrayList<OcularHistory> ocularHistory) {
            this.ocularHistory = ocularHistory;
        }

        public ArrayList<IPD> getIPD() {
            return ipd;
        }

        public void setIPD(ArrayList<IPD> ipd) {
            this.ipd = ipd;
        }

        public ArrayList<Distance> getDistance() {
            return distance;
        }

        public void setDistance(ArrayList<Distance> distance) {
            this.distance = distance;
        }

        public ArrayList<Near> getNear() {
            return near;
        }

        public void setNear(ArrayList<Near> near) {
            this.near = near;
        }

        public List<AnaesthesiaType> getAnaesthesiaType() {
            return anaesthesiaType;
        }

        public void setAnaesthesiaType(List<AnaesthesiaType> anaesthesiaType) {
            this.anaesthesiaType = anaesthesiaType;
        }

        public List<SurgeryType> getSurgeryType() {
            return surgeryType;
        }

        public void setSurgeryType(List<SurgeryType> surgeryType) {
            this.surgeryType = surgeryType;
        }

        public List<OptionSuggested> getOptionSuggested() {
            return optionSuggested;
        }

        public void setOptionSuggested(List<OptionSuggested> optionSuggested) {
            this.optionSuggested = optionSuggested;
        }


        public List<Category> getCategory() {
            return category;
        }

        public void setCategory(List<Category> category) {
            this.category = category;
        }

        public List<PatternType> getPatternType() {
            return patternType;
        }

        public void setPatternType(List<PatternType> patternType) {
            this.patternType = patternType;
        }

        public List<Stage> getStage() { return stage; }

        public void setStage(List<Stage> stage) { this.stage = stage; }

        public List<SideOfBrain> getSideOfBrain() {
            return sideOfBrain;
        }

        public void setSideOfBrain(List<SideOfBrain> sideOfBrain) {
            this.sideOfBrain = sideOfBrain;
        }

        public List<CataractType> getCataractType() {
            return cataractType;
        }

        public void setCataractType(List<CataractType> cataractType) {
            this.cataractType = cataractType;
        }

        public List<LaboratoryTest> getLaboratoryTest() {
            return laboratoryTest;
        }

        public void setLaboratoryTest(List<LaboratoryTest> laboratoryTest) {
            this.laboratoryTest = laboratoryTest;
        }

        public List<Ocularinvestigation> getOcularinvestigation() {
            return ocularinvestigation;
        }

        public void setOcularinvestigation(List<Ocularinvestigation> ocularinvestigation) {
            this.ocularinvestigation = ocularinvestigation;
        }

        public class FamilyOcularHistory implements Serializable {

            @SerializedName("FamilyOcularyHistoryID")
            @Expose
            private String familyOcularyHistoryID;
            @SerializedName("FamilyOcularHistory")
            @Expose
            private String familyOcularHistory;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;
            private boolean isSelected;
            private Boolean selStatus = false;

            public String getFamilyOcularyHistoryID() {
                return familyOcularyHistoryID;
            }

            public void setFamilyOcularyHistoryID(String familyOcularyHistoryID) {
                this.familyOcularyHistoryID = familyOcularyHistoryID;
            }

            public String getFamilyOcularHistory() {
                return familyOcularHistory;
            }

            public void setFamilyOcularHistory(String familyOcularHistory) {
                this.familyOcularHistory = familyOcularHistory;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }

            public boolean isSelected() {
                return isSelected;
            }

            public void setSelected(boolean selected) {
                isSelected = selected;
            }

            public Boolean isStatus() {
                return selStatus;
            }

            public void setStatus(Boolean selStatus) {
                this.selStatus = selStatus;
            }
        }

        public class SystemicHistory implements Serializable {

            @SerializedName("SystemicHistoryID")
            @Expose
            private String systemicHistoryID;
            @SerializedName("SystemicHistory")
            @Expose
            private String systemicHistory;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;
            private boolean isSelected;

            public String getSystemicHistoryID() {
                return systemicHistoryID;
            }

            public void setSystemicHistoryID(String systemicHistoryID) {
                this.systemicHistoryID = systemicHistoryID;
            }

            public String getSystemicHistory() {
                return systemicHistory;
            }

            public void setSystemicHistory(String systemicHistory) {
                this.systemicHistory = systemicHistory;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }

            public boolean isSelected() {
                return isSelected;
            }

            public void setSelected(boolean selected) {
                isSelected = selected;
            }
        }

        public class PrimaryComplain implements Serializable {

            @SerializedName("PrimaryComplainsID")
            @Expose
            private String primaryComplainsID;
            @SerializedName("PrimaryComplains")
            @Expose
            private String primaryComplains;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;
            private boolean isSelected;

            public String getPrimaryComplainsID() {
                return primaryComplainsID;
            }

            public void setPrimaryComplainsID(String primaryComplainsID) {
                this.primaryComplainsID = primaryComplainsID;
            }

            public String getPrimaryComplains() {
                return primaryComplains;
            }

            public void setPrimaryComplains(String primaryComplains) {
                this.primaryComplains = primaryComplains;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }

            public boolean isSelected() {
                return isSelected;
            }

            public void setSelected(boolean selected) {
                isSelected = selected;
            }
        }

        public class IPD implements Serializable {

            @SerializedName("IPDID")
            @Expose
            private String iPDID;
            @SerializedName("IPD")
            @Expose
            private String iPD;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            private boolean isSelected=false;

            public String getIPDID() {
                return iPDID;
            }

            public void setIPDID(String iPDID) {
                this.iPDID = iPDID;
            }

            public String getIPD() {
                return iPD;
            }

            public void setIPD(String iPD) {
                this.iPD = iPD;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }

            public boolean isSelected() {
                return isSelected;
            }

            public void setSelected(boolean selected) {
                this.isSelected = selected;
            }
        }

        public class AnatomicalLocation implements Serializable {

            @SerializedName("AnatomicalLocationID")
            @Expose
            private String anatomicalLocationID;
            @SerializedName("AnatomicalLocation")
            @Expose
            private String anatomicalLocation;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getAnatomicalLocationID() {
                return anatomicalLocationID;
            }

            public void setAnatomicalLocationID(String anatomicalLocationID) {
                this.anatomicalLocationID = anatomicalLocationID;
            }

            public String getAnatomicalLocation() {
                return anatomicalLocation;
            }

            public void setAnatomicalLocation(String anatomicalLocation) {
                this.anatomicalLocation = anatomicalLocation;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }
        }

        public class Eye implements Serializable {

            @SerializedName("EyeID")
            @Expose
            private String eyeID;
            @SerializedName("Eye")
            @Expose
            private String eye;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getEyeID() {
                return eyeID;
            }

            public void setEyeID(String eyeID) {
                this.eyeID = eyeID;
            }

            public String getEye() {
                return eye;
            }

            public void setEye(String eye) {
                this.eye = eye;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }
        }

        public class EyeLence implements Serializable {

            @SerializedName("EyeLenseID")
            @Expose
            private String eyeLenseID;
            @SerializedName("EyeLense")
            @Expose
            private String eyeLense;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getEyeLenseID() {
                return eyeLenseID;
            }

            public void setEyeLenseID(String eyeLenseID) {
                this.eyeLenseID = eyeLenseID;
            }

            public String getEyeLense() {
                return eyeLense;
            }

            public void setEyeLense(String eyeLense) {
                this.eyeLense = eyeLense;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }
        }

        public class Type implements Serializable {

            @SerializedName("TypeID")
            @Expose
            private String typeID;
            @SerializedName("Type")
            @Expose
            private String type;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getTypeID() {
                return typeID;
            }

            public void setTypeID(String typeID) {
                this.typeID = typeID;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }
        }

        public class OcularHistory implements Serializable {

            @SerializedName("OcularHistoryID")
            @Expose
            private String ocularHistoryID;
            @SerializedName("OcularHistory")
            @Expose
            private String ocularHistory;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getOcularHistoryID() {
                return ocularHistoryID;
            }

            public void setOcularHistoryID(String ocularHistoryID) {
                this.ocularHistoryID = ocularHistoryID;
            }

            public String getOcularHistory() {
                return ocularHistory;
            }

            public void setOcularHistory(String ocularHistory) {
                this.ocularHistory = ocularHistory;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }
        }

        public class Distance implements Serializable {

            @SerializedName("DistanceID")
            @Expose
            private String distanceID;
            @SerializedName("Distance")
            @Expose
            private String distance;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getDistanceID() {
                return distanceID;
            }

            public void setDistanceID(String distanceID) {
                this.distanceID = distanceID;
            }

            public String getDistance() {
                return distance;
            }

            public void setDistance(String distance) {
                this.distance = distance;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }
        }

        public class Near implements Serializable {

            @SerializedName("NearID")
            @Expose
            private String nearID;
            @SerializedName("Near")
            @Expose
            private String near;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getNearID() {
                return nearID;
            }

            public void setNearID(String nearID) {
                this.nearID = nearID;
            }

            public String getNear() {
                return near;
            }

            public void setNear(String near) {
                this.near = near;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }
        }

        public class AnaesthesiaType {

            @SerializedName("AnaesthesiaTypeID")
            @Expose
            private String anaesthesiaTypeID;
            @SerializedName("AnaesthesiaType")
            @Expose
            private String anaesthesiaType;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getAnaesthesiaTypeID() {
                return anaesthesiaTypeID;
            }

            public void setAnaesthesiaTypeID(String anaesthesiaTypeID) {
                this.anaesthesiaTypeID = anaesthesiaTypeID;
            }

            public String getAnaesthesiaType() {
                return anaesthesiaType;
            }

            public void setAnaesthesiaType(String anaesthesiaType) {
                this.anaesthesiaType = anaesthesiaType;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }
        }

        public class SurgeryType {

            @SerializedName("SurgeryTypeID")
            @Expose
            private String surgeryTypeID;
            @SerializedName("SurgeryName")
            @Expose
            private String surgeryName;
            @SerializedName("ParentSurgeryName")
            @Expose
            private String parentSurgeryName;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getSurgeryTypeID() {
                return surgeryTypeID;
            }

            public void setSurgeryTypeID(String surgeryTypeID) {
                this.surgeryTypeID = surgeryTypeID;
            }

            public String getSurgeryName() {
                return surgeryName;
            }

            public void setSurgeryName(String surgeryName) {
                this.surgeryName = surgeryName;
            }

            public String getParentSurgeryName() {
                return parentSurgeryName;
            }

            public void setParentSurgeryName(String parentSurgeryName) {
                this.parentSurgeryName = parentSurgeryName;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }
        }

        public class OptionSuggested {

            @SerializedName("OpetionSuggestedID")
            @Expose
            private String opetionSuggestedID;
            @SerializedName("OpetionSuggested")
            @Expose
            private String opetionSuggested;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getOpetionSuggestedID() {
                return opetionSuggestedID;
            }

            public void setOpetionSuggestedID(String opetionSuggestedID) {
                this.opetionSuggestedID = opetionSuggestedID;
            }

            public String getOpetionSuggested() {
                return opetionSuggested;
            }

            public void setOpetionSuggested(String opetionSuggested) {
                this.opetionSuggested = opetionSuggested;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }

        }
        public class CataractType {

            @SerializedName("CataracttypeID")
            @Expose
            private String cataracttypeID;
            @SerializedName("Cataracttype")
            @Expose
            private String cataracttype;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getCataracttypeID() { return cataracttypeID; }

            public void setCataracttypeID(String cataracttypeID) { this.cataracttypeID = cataracttypeID; }

            public String getCataracttype() { return cataracttype; }

            public void setCataracttype(String cataracttype) { this.cataracttype = cataracttype; }

            public String getStatus() { return status; }

            public void setStatus(String status) { this.status = status; }

            public String getRno() { return rno; }

            public void setRno(String rno) { this.rno = rno; }

            public String getRowcount() { return rowcount; }

            public void setRowcount(String rowcount) { this.rowcount = rowcount; }
        }

        public class Category {
            @SerializedName("CategoryID")
            @Expose
            private String categoryID;
            @SerializedName("Category")
            @Expose
            private String category;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;
            public String getCategoryID() { return categoryID; }

            public void setCategoryID(String categoryID) { this.categoryID = categoryID; }

            public String getCategory() { return category; }

            public void setCategory(String category) { this.category = category; }

            public String getStatus() { return status; }
            public void setStatus(String status) { this.status = status; }

            public String getRno() { return rno; }

            public void setRno(String rno) { this.rno = rno; }

            public String getRowcount() { return rowcount; }

            public void setRowcount(String rowcount) { this.rowcount = rowcount; }
        }
        public class PatternType {

            @SerializedName("PatterntypeID")
            @Expose
            private String patterntypeID;
            @SerializedName("Patterntype")
            @Expose
            private String patterntype;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getPatterntypeID() { return patterntypeID; }

            public void setPatterntypeID(String patterntypeID) { this.patterntypeID = patterntypeID; }

            public String getPatterntype() { return patterntype; }

            public void setPatterntype(String patterntype) { this.patterntype = patterntype; }

            public String getStatus() { return status; }

            public void setStatus(String status) { this.status = status; }

            public String getRno() { return rno; }

            public void setRno(String rno) { this.rno = rno; }
            public String getRowcount() { return rowcount; }

            public void setRowcount(String rowcount) { this.rowcount = rowcount; }
        }
        public class SideOfBrain {

            @SerializedName("SideofbrainID")
            @Expose
            private String sideofbrainID;
            @SerializedName("Sideofbrain")
            @Expose
            private String sideofbrain;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getSideofbrainID() { return sideofbrainID; }

            public void setSideofbrainID(String sideofbrainID) { this.sideofbrainID = sideofbrainID; }

            public String getSideofbrain() { return sideofbrain; }

            public void setSideofbrain(String sideofbrain) { this.sideofbrain = sideofbrain; }

            public String getStatus() { return status; }

            public void setStatus(String status) { this.status = status; }

            public String getRno() { return rno; }

            public void setRno(String rno) { this.rno = rno; }

            public String getRowcount() { return rowcount; }

            public void setRowcount(String rowcount) { this.rowcount = rowcount; }
        }

        public class Stage {

            @SerializedName("StageID")
            @Expose
            private String stageID;
            @SerializedName("Stage")
            @Expose
            private String stage;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getStageID() { return stageID; }

            public void setStageID(String stageID) { this.stageID = stageID; }

            public String getStage() { return stage; }

            public void setStage(String stage) { this.stage = stage; }

            public String getStatus() { return status; }

            public void setStatus(String status) { this.status = status; }

            public String getRno() { return rno; }

            public void setRno(String rno) { this.rno = rno; }

            public String getRowcount() { return rowcount; }

            public void setRowcount(String rowcount) { this.rowcount = rowcount; }
        }

        public class LaboratoryTest {

            @SerializedName("LaboratoryTestID")
            @Expose
            private String laboratoryTestID;
            @SerializedName("LaboratoryTest")
            @Expose
            private String laboratoryTest;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getLaboratoryTestID() {
                return laboratoryTestID;
            }

            public void setLaboratoryTestID(String laboratoryTestID) {
                this.laboratoryTestID = laboratoryTestID;
            }

            public String getLaboratoryTest() {
                return laboratoryTest;
            }

            public void setLaboratoryTest(String laboratoryTest) {
                this.laboratoryTest = laboratoryTest;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }
        }

        public class Ocularinvestigation {

            @SerializedName("OcularInvestigationID")
            @Expose
            private String ocularInvestigationID;
            @SerializedName("Ocularinvestigation")
            @Expose
            private String ocularinvestigation;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getOcularInvestigationID() {
                return ocularInvestigationID;
            }

            public void setOcularInvestigationID(String ocularInvestigationID) {
                this.ocularInvestigationID = ocularInvestigationID;
            }

            public String getOcularinvestigation() {
                return ocularinvestigation;
            }

            public void setOcularinvestigation(String ocularinvestigation) {
                this.ocularinvestigation = ocularinvestigation;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }
        }

        public class ImplantName {

            @SerializedName("ImplantNameID")
            @Expose
            private String implantNameID;
            @SerializedName("ImplantName")
            @Expose
            private String implantName;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getImplantNameID() {
                return implantNameID;
            }

            public void setImplantNameID(String implantNameID) {
                this.implantNameID = implantNameID;
            }

            public String getImplantName() {
                return implantName;
            }

            public void setImplantName(String implantName) {
                this.implantName = implantName;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }
        }

        public class ImplantBrand {

            @SerializedName("ImplantBrandID")
            @Expose
            private String implantBrandID;
            @SerializedName("ImplantBrand")
            @Expose
            private String implantBrand;
            @SerializedName("Status")
            @Expose
            private String status;
            @SerializedName("Rno")
            @Expose
            private String rno;
            @SerializedName("rowcount")
            @Expose
            private String rowcount;

            public String getImplantBrandID() {
                return implantBrandID;
            }

            public void setImplantBrandID(String implantBrandID) {
                this.implantBrandID = implantBrandID;
            }

            public String getImplantBrand() {
                return implantBrand;
            }

            public void setImplantBrand(String implantBrand) {
                this.implantBrand = implantBrand;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getRno() {
                return rno;
            }

            public void setRno(String rno) {
                this.rno = rno;
            }

            public String getRowcount() {
                return rowcount;
            }

            public void setRowcount(String rowcount) {
                this.rowcount = rowcount;
            }

        }
    }
}


