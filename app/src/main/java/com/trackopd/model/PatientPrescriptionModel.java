package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PatientPrescriptionModel implements Serializable {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("rowCount")
    @Expose
    private String rowCount;
    @SerializedName("data")
    @Expose
    private ArrayList<PatientPrescriptionModel> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRowCount() {
        return rowCount;
    }

    public void setRowCount(String rowCount) {
        this.rowCount = rowCount;
    }

    public ArrayList<PatientPrescriptionModel> getData() {
        return data;
    }

    public void setData(ArrayList<PatientPrescriptionModel> data) {
        this.data = data;
    }

    @SerializedName("AppointmentID")
    @Expose
    private String appointmentID;
    @SerializedName("PatientFirstName")
    @Expose
    private String patientFirstName;
    @SerializedName("PatientLastName")
    @Expose
    private String patientLastName;
    @SerializedName("PatientMobileNo")
    @Expose
    private String patientMobileNo;
    @SerializedName("DoctorFirstName")
    @Expose
    private String doctorFirstName;
    @SerializedName("DoctorLastName")
    @Expose
    private String doctorLastName;
    @SerializedName("TicketNumber")
    @Expose
    private String ticketNumber;
    @SerializedName("AppointmentDate")
    @Expose
    private String appointmentDate;
    @SerializedName("PatientCode")
    @Expose
    private String patientCode;
    @SerializedName("AppointmentStatus")
    @Expose
    private String appointmentStatus;
    @SerializedName("PaymentStatus")
    @Expose
    private String paymentStatus;
    @SerializedName("AppointmentType")
    @Expose
    private String appointmentType;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("CurrentStatus")
    @Expose
    private String currentStatus;
    @SerializedName("AsigneeName")
    @Expose
    private String asigneeName;
    @SerializedName("PatientID")
    @Expose
    private String patientID;
    @SerializedName("Reason")
    @Expose
    private String reason;
    @SerializedName("Document")
    @Expose
    private String document;
    @SerializedName("Rno")
    @Expose
    private String rno;
    @SerializedName("rowcount")
    @Expose
    private String rowcount;
    @SerializedName("medication")
    @Expose
    private List<Medication> medication = null;

    public String getAppointmentID() {
        return appointmentID;
    }

    public void setAppointmentID(String appointmentID) {
        this.appointmentID = appointmentID;
    }

    public String getPatientFirstName() {
        return patientFirstName;
    }

    public void setPatientFirstName(String patientFirstName) {
        this.patientFirstName = patientFirstName;
    }

    public String getPatientLastName() {
        return patientLastName;
    }

    public void setPatientLastName(String patientLastName) {
        this.patientLastName = patientLastName;
    }

    public String getPatientMobileNo() {
        return patientMobileNo;
    }

    public void setPatientMobileNo(String patientMobileNo) {
        this.patientMobileNo = patientMobileNo;
    }

    public String getDoctorFirstName() {
        return doctorFirstName;
    }

    public void setDoctorFirstName(String doctorFirstName) {
        this.doctorFirstName = doctorFirstName;
    }

    public String getDoctorLastName() {
        return doctorLastName;
    }

    public void setDoctorLastName(String doctorLastName) {
        this.doctorLastName = doctorLastName;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getPatientCode() {
        return patientCode;
    }

    public void setPatientCode(String patientCode) {
        this.patientCode = patientCode;
    }

    public String getAppointmentStatus() {
        return appointmentStatus;
    }

    public void setAppointmentStatus(String appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(String appointmentType) {
        this.appointmentType = appointmentType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public String getAsigneeName() {
        return asigneeName;
    }

    public void setAsigneeName(String asigneeName) {
        this.asigneeName = asigneeName;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getRno() {
        return rno;
    }

    public void setRno(String rno) {
        this.rno = rno;
    }

    public String getRowcount() {
        return rowcount;
    }

    public void setRowcount(String rowcount) {
        this.rowcount = rowcount;
    }

    public List<Medication> getMedication() {
        return medication;
    }

    public void setMedication(List<Medication> medication) {
        this.medication = medication;
    }

    public class Medication implements Serializable {

        @SerializedName("MedicationID")
        @Expose
        private String medicationID;
        @SerializedName("AppointmentID")
        @Expose
        private String appointmentID;
        @SerializedName("MedicationName")
        @Expose
        private String medicationName;
        @SerializedName("dosage")
        @Expose
        private List<Dosage> dosage = null;

        public String getMedicationID() {
            return medicationID;
        }

        public void setMedicationID(String medicationID) {
            this.medicationID = medicationID;
        }

        public String getAppointmentID() {
            return appointmentID;
        }

        public void setAppointmentID(String appointmentID) {
            this.appointmentID = appointmentID;
        }

        public String getMedicationName() {
            return medicationName;
        }

        public void setMedicationName(String medicationName) {
            this.medicationName = medicationName;
        }

        public List<Dosage> getDosage() {
            return dosage;
        }

        public void setDosage(List<Dosage> dosage) {
            this.dosage = dosage;
        }
    }

    public class Dosage implements Serializable {

        @SerializedName("Dosage")
        @Expose
        private String dosage;
        @SerializedName("Description")
        @Expose
        private String description;

        public String getDosage() {
            return dosage;
        }

        public void setDosage(String dosage) {
            this.dosage = dosage;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }
    }
}
