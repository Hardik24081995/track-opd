package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class getReceptionistModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Data> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("TotalAppointment")
        @Expose
        private String totalAppointment;
        @SerializedName("Confirmed")
        @Expose
        private String confirmed;
        @SerializedName("Completed")
        @Expose
        private String completed;
        @SerializedName("Pending")
        @Expose
        private String pending;
        @SerializedName("PaymentReceived")
        @Expose
        private String paymentReceived;
        @SerializedName("PaymentDue")
        @Expose
        private String paymentDue;
        @SerializedName("TotalCheckUp")
        @Expose
        private String totalCheckUp;
        @SerializedName("TotalSurgeries")
        @Expose
        private String totalSurgeries;


        public String getTotalAppointment() {
            return totalAppointment;
        }

        public void setTotalAppointment(String totalAppointment) {
            this.totalAppointment = totalAppointment;
        }

        public String getConfirmed() {
            return confirmed;
        }

        public void setConfirmed(String confirmed) {
            this.confirmed = confirmed;
        }

        public String getCompleted() {
            return completed;
        }

        public void setCompleted(String completed) {
            this.completed = completed;
        }

        public String getPending() {
            return pending;
        }

        public void setPending(String pending) {
            this.pending = pending;
        }

        public String getPaymentReceived() {
            return paymentReceived;
        }

        public void setPaymentReceived(String paymentReceived) {
            this.paymentReceived = paymentReceived;
        }

        public String getPaymentDue() {
            return paymentDue;
        }

        public void setPaymentDue(String paymentDue) {
            this.paymentDue = paymentDue;
        }

        public String getTotalCheckUp() {
            return totalCheckUp;
        }

        public void setTotalCheckUp(String totalCheckUp) {
            this.totalCheckUp = totalCheckUp;
        }

        public String getTotalSurgeries() {
            return totalSurgeries;
        }

        public void setTotalSurgeries(String totalSurgeries) {
            this.totalSurgeries = totalSurgeries;
        }
    }
}
