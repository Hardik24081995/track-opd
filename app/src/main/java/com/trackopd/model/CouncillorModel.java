package com.trackopd.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CouncillorModel {

    @SerializedName("error")
    @Expose
    private Integer error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private ArrayList<CouncillorModel> data = null;

    public Integer getError() {
        return error;
    }

    public void setError(Integer error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CouncillorModel> getData() {
        return data;
    }

    public void setData(ArrayList<CouncillorModel> data) {
        this.data = data;
    }

    @SerializedName("CouncillorID")
    @Expose
    private String councillorID;
    @SerializedName("CouncillorName")
    @Expose
    private String councillorName;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;

    public String getCouncillorID() {
        return councillorID;
    }

    public void setCouncillorID(String councillorID) {
        this.councillorID = councillorID;
    }

    public String getCouncillorName() {
        return councillorName;
    }

    public void setCouncillorName(String councillorName) {
        this.councillorName = councillorName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
