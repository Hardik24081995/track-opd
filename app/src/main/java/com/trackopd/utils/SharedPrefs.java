package com.trackopd.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefs {
    public static final String CurrentTheme = "TRACKOPD";
    private SharedPreferences settings;
    private SharedPreferences.Editor editor;

    public final String THEME = "theme";

    public SharedPrefs(Context context) {
        Context mContext = context;
        settings = mContext.getSharedPreferences(CurrentTheme, Context.MODE_PRIVATE);
        editor = settings.edit();
    }

    // Set the Access Token and Other Important Keys in Shared Preferences
    public void setPreferences(String Key, String Value) {
        editor.putString(Key, Value);
        editor.commit();
    }

    // get the Access Token and Other Important Keys in Shared Preferences
    public String getPreferences(String Key, String Value) {
        return settings.getString(Key, Value);
    }

    // Set the Access Token and Other Important Keys in Shared Preferences
    public void setPreferences(String Key, int Value) {
        editor.putInt(Key, Value);
        editor.apply();
    }

    // get the Access Token and Other Important Keys in Shared Preferences
    public int getPreferences(String Key, int Value) {
        return settings.getInt(Key, Value);
    }
}
