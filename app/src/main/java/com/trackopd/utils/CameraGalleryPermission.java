package com.trackopd.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public class CameraGalleryPermission {

    private static List<String> permissionsList;
    public static final int INTERNAL_EXTERNAL_PERMISSION = 123;

    /**
     * @param context             - Context
     * @param fragment            - Fragment
     * @param permissionArrayList - Permission Array List
     * @return - Return Permission Array List
     */
    public static boolean checkPermissionFragment(final Context context, Fragment fragment, ArrayList<String> permissionArrayList) {

        permissionsList = new ArrayList<>();
        for (String strPermission : permissionArrayList)
            addPermission(context, strPermission);

        if (permissionsList.size() > 0) {
            fragment.requestPermissions(permissionsList.toArray(new String[permissionsList.size()]),
                    INTERNAL_EXTERNAL_PERMISSION);
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param context             - Context
     * @param permissionArrayList - Permission Array List
     * @return - Return Permission Array List
     */
    public static boolean checkPermission(final Context context, ArrayList<String> permissionArrayList) {

        permissionsList = new ArrayList<>();
        for (String strPermission : permissionArrayList)
            addPermission(context, strPermission);

        if (permissionsList.size() > 0) {
            ActivityCompat.requestPermissions((Activity) context, permissionsList.toArray(new String[permissionsList.size()]),
                    INTERNAL_EXTERNAL_PERMISSION);
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param context    - Context
     * @param permission - Permission
     * @return - Return boolean value of permission
     */
    private static boolean addPermission(Context context, String permission) {
        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            if (!ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permission)) {
                return false;
            }
        }
        return true;
    }
}

