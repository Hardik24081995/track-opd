package com.trackopd.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionManager {

    private static final String PREF_NAME = "PREF_TRACK_OPD";
    private static final String PREF_NAME_THEME  = "PREF_TRACK_OPD_THEME";
    private static final String PREF_APP_PERMISSIONS = "PREF_TRACK_OPD_PERMISSIONS";
    private static final String PREF_ODIN_REMEMBER_ME = "PREF_TRACK_OPD_REMEMBER_ME";

    private SharedPreferences settings;
    private SharedPreferences.Editor editor;
    public final String APP_PASS_CODE = "app_pass_code";
//    public final String CurrentTheme = "CurrentTheme";
    public final String KEY_LOGIN_USER_DATA = "loginUserData";
    public final String KEY_CONFIGURATION_DATA = "configurationData";
    public final String KEY_HOSPITAL_DATA = "HospitalData";
    public final String KEY_ROLE = "key_role";

    public final String KEY_HOSPITAL_DATABASE_NAME = "DatabaseName";

    public final String PATIENT_USERID = "userID";
    public final String PID = "pid";

    public final String SOURCE_PATH = "imagepath";

    public final String FIRSTNAME = "firstname";
    public final String LASTNAME = "lastname";
    public final String MOBILE = "mobile";


    public SessionManager(Context context) {
        Context mContext = context;
        settings = mContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
    }

    // Set the Access Token and Other Important Keys in Shared Preferences
    public void setPreferences(String Key, String Value) {
        editor.putString(Key, Value);
        editor.commit();
    }

    // get the Access Token and Other Important Keys in Shared Preferences
    /*public String getPreferences(String Key, String Value) {
        return settings.getString(Key, Value);
    }*/

    // Set the Access Token and Other Important Keys in Shared Preferences
    public void setPreferences(String Key, int Value) {
        editor.putInt(Key, Value);
        editor.apply();
    }

    // get the Access Token and Other Important Keys in Shared Preferences
    public int getPreferences(String Key, int Value) {
        return settings.getInt(Key, Value);
    }


    // get the Access Token and Other Important Keys in Shared Preferences
    public String getPreferences(String Key, String Value) {
        return settings.getString(Key, Value);
    }

    public void setPreferences(String Key, boolean Value) {
        editor.putBoolean(Key, Value);
        editor.apply();
    }

    // get the Access Token and Other Important Keys in Shared Preferences
    public boolean getPreferences(String Key, boolean Value) {
        return settings.getBoolean(Key, Value);
    }

/*    public RoleModal getRole(){
        Gson gson = new Gson();
        RoleModal  roleList = new RoleModal();
        String json2 = getPreferences(KEY_ROLE, "");
        roleList = gson.fromJson(json2, RoleModal.class);
        return  roleList;
    }*/

    /**
     * Clear all the data stored in shared preferences
     */
    public void clearAllPreferences() {
        editor.clear().apply();
    }
}
