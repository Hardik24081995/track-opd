package com.trackopd.utils;

import android.os.Environment;

import com.google.firebase.crash.FirebaseCrash;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class ErrorLog {

    /**
     * Save Error Log in SD card
     *
     * @param exception - Exception
     */
    public static void SaveErrorLog(Exception exception) {
        SendErrorReport(exception);
        exception.printStackTrace();
        if (AppConstants.IS_ERROR_LOG_SAVE) {
            File dir = new File(Environment.getExternalStorageDirectory() + "/TrackOPD/Log.txt");
            try {
                PrintWriter pw = new PrintWriter(new FileOutputStream(dir.toString(), true));
                exception.printStackTrace(pw);
                pw.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Send handled Exception on fire base
     *
     * @param exception - Exception
     */
    private static void SendErrorReport(Exception exception) {
        FirebaseCrash.report(new Exception(exception));
    }
}
