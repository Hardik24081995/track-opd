package com.trackopd.utils.searchablespinnerlibrary;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatSpinner;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.model.CouselingPackageSuggestionModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import java.util.ArrayList;
import java.util.List;

public class MultiSpinnerSearch extends AppCompatSpinner implements OnCancelListener {


    private static final String TAG = MultiSpinnerSearch.class.getSimpleName();

    public static AlertDialog.Builder builder;
    public static AlertDialog ad;

    private int limit = -1;
    private int selected = 0;
    private String defaultText = "";
    private String strHeader = "";
    private String spinnerTitle = "";
    private String defualtSelect = "";
    private String emptyTitle = "Not Found!";
    private String searchHint = "Type to search";
    private boolean colorSeparation = false;

    private SpinnerListener listener;
    private LimitExceedListener limitListener;

    private MyAdapter adapter;
    private List<CouselingPackageSuggestionModel> items;

    public MultiSpinnerSearch(Context context) {
        super(context);
    }

    public MultiSpinnerSearch(Context arg0, AttributeSet arg1) {
        super(arg0, arg1);
        TypedArray a = arg0.obtainStyledAttributes(arg1, R.styleable.SearchableSpinner);
        for (int i = 0; i < a.getIndexCount(); ++i) {
            int attr = a.getIndex(i);
            if (attr == R.styleable.MultiSpinnerSearch_hintText) {
                this.setHintText(a.getString(attr));
                spinnerTitle = this.getHintText();
                defaultText = spinnerTitle;
                break;
            }
        }
        //Log.i(TAG, "spinnerTitle: " + spinnerTitle);
        a.recycle();
    }

    public MultiSpinnerSearch(Context arg0, AttributeSet arg1, int arg2) {
        super(arg0, arg1, arg2);
    }

    public boolean isColorSeparation() {
        return colorSeparation;
    }

    public void setColorSeparation(boolean colorSeparation) {
        this.colorSeparation = colorSeparation;
    }

    public String getHintText() {
        return this.spinnerTitle;
    }

    public void setHintText(String hintText) {
        this.spinnerTitle = hintText;
    }


    public void setLimit(int limit, LimitExceedListener listener) {
        this.limit = limit;
        this.limitListener = listener;
    }

    public List<CouselingPackageSuggestionModel> getSelectedItems() {
        List<CouselingPackageSuggestionModel> selectedItems = new ArrayList<>();
        for (CouselingPackageSuggestionModel item : items) {
            if (item.getChecked()) {
                selectedItems.add(item);
            }
        }
        return selectedItems;
    }

    public List<String> getSelectedIds() {
        List<String> selectedItemsIds = new ArrayList<>();
        for (CouselingPackageSuggestionModel item : items) {
            if (item.getChecked()) {
                selectedItemsIds.add(item.getOpetionSuggestedID());
            }
        }
        return selectedItemsIds;
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        // refresh text on spinner

        StringBuilder spinnerBuffer = new StringBuilder();

        /*for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getChecked()) {
                spinnerBuffer.append(items.get(i).getOpetionSuggested());
                spinnerBuffer.append(", ");
            }
        }


        if (spinnerText.length() > 2)
            spinnerText = spinnerText.substring(0, spinnerText.length() - 2);
        else*/

        String spinnerText=AppConstants.STR_EMPTY_STRING;

        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getChecked()) {
                spinnerText=items.get(i).getOpetionSuggested();
            }
        }

       /* if (spinnerText.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
            spinnerText=defualtSelect;
        }*/

        ArrayAdapter<String> adapterSpinner = new ArrayAdapter<>(getContext(),
                R.layout.textview_for_spinner, new String[]{spinnerText});
        setAdapter(adapterSpinner);

        if (adapter != null)
            adapter.notifyDataSetChanged();

        listener.onItemsSelected(items);
    }

    @Override
    public boolean performClick() {

        super.performClick();
        builder = new AlertDialog.Builder(getContext());
        builder.setTitle(spinnerTitle);


        final LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View view = inflater.inflate(R.layout.alert_dialog_listview_search, null);
        builder.setView(view);

        TextView mTextHeader=view.findViewById(R.id.text_alert_dialog_list_view_search_header);
        mTextHeader.setText(strHeader);

        final ListView listView = view.findViewById(R.id.alertSearchListView);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        listView.setFastScrollEnabled(false);
        adapter = new MyAdapter(getContext(), items);
        listView.setAdapter(adapter);

        final TextView emptyText = view.findViewById(R.id.empty);
        emptyText.setText(emptyTitle);
        listView.setEmptyView(emptyText);

        final EditText editText = view.findViewById(R.id.alertSearchEditText);
        editText.setHint(searchHint);
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
       /* *//*
        Added Select all Dialog Button.
         *//*
        builder.setNeutralButton(android.R.string.selectAll, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                adapter.arrayList = adapter.mOriginalValues;
                for (int i = 0; i < adapter.mOriginalValues.size(); i++) {
                    adapter.arrayList.get(i).setChecked(true);
                    //Log.i(TAG, adapter.mOriginalValues.get(i).getOpetionSuggested());
                }
                adapter.notifyDataSetChanged();
            }
        });*/

        builder.setPositiveButton(getResources().getString(R.string.save), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                //Log.i(TAG, " ITEMS : " + items.size());
                updateList();
                dialog.cancel();

            }
        });

        builder.setOnCancelListener(this);
        ad = builder.show();
        ad.getWindow().setLayout(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        return true;
    }

    /**
     * Update List
     */
    protected void updateList(){

    }

    public void setItems(List<CouselingPackageSuggestionModel> items,
                         int position, String string, SpinnerListener listener) {

        this.items = items;
        this.listener = listener;

        StringBuilder spinnerBuffer = new StringBuilder();


        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getChecked()) {
                defaultText=items.get(i).getOpetionSuggested();
                return;
            }
        }

        if (defaultText.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
            defaultText=string;
        }

        defualtSelect=string;

        /*for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getChecked()) {
                spinnerBuffer.append(items.get(i).getOpetionSuggested());
                spinnerBuffer.append(", ");
            }
        }
        if (spinnerBuffer.length() > 2)
            defaultText = spinnerBuffer.toString().substring(0, spinnerBuffer.toString().length() - 2);*/

        ArrayAdapter<String> adapterSpinner = new ArrayAdapter<>(getContext(),
                R.layout.textview_for_spinner, new String[]{defaultText});
        setAdapter(adapterSpinner);

        if (position != -1) {
            items.get(position).setChecked(false);
            listener.onItemsSelected(items);
            onCancel(null);
        }else {
            listener.onItemsSelected(items);
        }
    }

    public void setEmptyTitle(String emptyTitle) {
        this.emptyTitle = emptyTitle;
    }

    public void setSearchHint(String searchHint) {
        this.searchHint = searchHint;
    }

    public void setHeader(String string) {
        this.strHeader=string;
    }

    public interface LimitExceedListener {
        void onLimitListener(CouselingPackageSuggestionModel data);
    }

    //Adapter Class
    public class MyAdapter extends BaseAdapter implements Filterable {

        List<CouselingPackageSuggestionModel> arrayList;
        List<CouselingPackageSuggestionModel> mOriginalValues; // Original Values
        LayoutInflater inflater;

        MyAdapter(Context context, List<CouselingPackageSuggestionModel> arrayList) {
            this.arrayList = arrayList;
            this.mOriginalValues = arrayList;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
//            //Log.i(TAG, "getView() enter");
            ViewHolder holder;

            if (convertView == null) {
                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.item_listview_multiple, parent, false);
                holder.textView = convertView.findViewById(R.id.alertTextView);
                holder.checkBox = convertView.findViewById(R.id.alertCheckbox);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            int background = R.color.white;
            if (colorSeparation) {
                final int backgroundColor = (position % 2 == 0) ? R.color.white : R.color.white;
                background = backgroundColor;
                convertView.setBackgroundColor(ContextCompat.getColor(getContext(), backgroundColor));
            }


            final CouselingPackageSuggestionModel data = arrayList.get(position);

            holder.textView.setText(data.getOpetionSuggested());
            holder.textView.setTypeface(null, Typeface.NORMAL);
            holder.checkBox.setChecked(data.getChecked());

            convertView.setOnClickListener(new OnClickListener() {
                public void onClick(View v) {


                    if (data.getChecked()) { // deselect
                        selected--;
                       // data.setChecked(false);
                    } else if (selected == limit) { // select with limit
                        if (limitListener != null)
                            limitListener.onLimitListener(data);
                        return;
                    } else { // selected
                        selected++;
                    }

                    final ViewHolder temp = (ViewHolder) v.getTag();
                    temp.checkBox.setChecked(!temp.checkBox.isChecked());

                    data.setChecked(!data.getChecked());
                    notifyDataSetChanged();
                }
            });

            if (data.getChecked()) {
                holder.textView.setTypeface(null, Typeface.BOLD);
                holder.textView.setTextColor(Common.setThemeColor(getContext()));

                //convertView.setBackgroundColor(ContextCompat.getColor(getContext(), R));
            } else {
                holder.textView.setTypeface(null, Typeface.NORMAL);
                holder.textView.setTextColor(getResources().getColor(R.color.colorBlack));
               // convertView.setBackgroundColor(ContextCompat.getColor(getContext(), background));
            }
            holder.checkBox.setTag(holder);

            return convertView;
        }

        @SuppressLint("DefaultLocale")
        @Override
        public Filter getFilter() {
            return new Filter() {

                @SuppressWarnings("unchecked")
                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {

                    arrayList = (List<CouselingPackageSuggestionModel>) results.values; // has the filtered values
                    notifyDataSetChanged();  // notifies the data with new filtered values
                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                    List<CouselingPackageSuggestionModel> FilteredArrList = new ArrayList<>();


                    /*
                     *
                     *  If constraint(CharSequence that is received) is null returns the mOriginalValues(Original) values
                     *  else does the Filtering and returns FilteredArrList(Filtered)
                     *
                     **/
                    if (constraint == null || constraint.length() == 0) {

                        // set the Original result to return
                        results.count = mOriginalValues.size();
                        results.values = mOriginalValues;
                    } else {
                        constraint = constraint.toString().toLowerCase();
                        for (int i = 0; i < mOriginalValues.size(); i++) {
                            //Log.i(TAG, "Filter : " + mOriginalValues.get(i).getOpetionSuggested() + " -> " + mOriginalValues.get(i).getChecked()); 
                            String data = mOriginalValues.get(i).getOpetionSuggested();
                            if (data.toLowerCase().contains(constraint.toString())) {
                                FilteredArrList.add(mOriginalValues.get(i));
                            }
                        }
                        // set the Filtered result to return
                        results.count = FilteredArrList.size();
                        results.values = FilteredArrList;
                    }
                    return results;
                }
            };
        }

        private class ViewHolder {
            TextView textView;
            CheckBox checkBox;
        }
    }
}
