package com.trackopd.utils.searchablespinnerlibrary;

import com.trackopd.model.CouselingPackageSuggestionModel;

import java.util.List;

public interface SpinnerListener {
    void onItemsSelected(List<CouselingPackageSuggestionModel> items);
}
