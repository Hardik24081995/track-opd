package com.trackopd.utils;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

import com.trackopd.R;

public class AnimUtils {

    public static void activityEnterAnim(Context context) {
        ((Activity) context).overridePendingTransition(R.anim.activity_slide_in_left, R.anim.activity_slide_out_left);
    }

    public static void activityExitAnim(Context context) {
        ((Activity) context).overridePendingTransition(R.anim.activity_slide_in_right, R.anim.activity_slide_out_right);
    }

    public static void activitySlideUpAnim(Context context) {
        ((Activity) context).overridePendingTransition(R.anim.activity_slide_up, android.R.anim.fade_out);

    }

    public static void activitySlideDownAnim(Context context) {
        ((Activity) context).overridePendingTransition(android.R.anim.fade_in, R.anim.activity_slide_down);
    }

    public static void imageRotedYAxisAnim(ImageView view){
        ObjectAnimator animation = ObjectAnimator.ofFloat(view, "rotationY", 0.0f, 360f);
        animation.setDuration(3600);
        animation.setRepeatCount(ObjectAnimator.INFINITE);
        animation.setInterpolator(new AccelerateDecelerateInterpolator());
        animation.start();
    }
}
