package com.trackopd.utils;

/**
 * This Interface is used for the Callback listener for the API call
 */
public class AppCallbackListener {

    private CallBackListener listener;

    public interface CallBackListener {

        /***
         * Its a common listener for the application
         * @param Code - Code
         */
        void onAppCallback(int Code);
    }

    public AppCallbackListener(CallBackListener listener) {
        this.listener = listener;
    }

    /***
     * Its a common listener for the application
     * @param Code - Code
     */
    public void onAppCallback(int Code) {
        listener.onAppCallback(Code);
    }
}
