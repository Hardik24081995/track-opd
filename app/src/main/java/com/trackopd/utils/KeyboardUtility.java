package com.trackopd.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

public class KeyboardUtility {

	/**
	 * This method should show the keyboard
	 * @param context - Context
	 * @param etEditText - Edit Text View
	 */
	public static void ShowKeyboard(Context context, EditText etEditText) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(etEditText, InputMethodManager.SHOW_IMPLICIT);
	}

	/**
	 * This method should hide the keyboard
	 *
	 * @param mActivity - Activity
	 * @param view    - View
	 */
	public static void HideKeyboard(Activity mActivity, View view) {
//		InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
//		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

		InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(mActivity.INPUT_METHOD_SERVICE);
		if (mActivity.getCurrentFocus() != null)
			imm.hideSoftInputFromWindow(mActivity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}
}
