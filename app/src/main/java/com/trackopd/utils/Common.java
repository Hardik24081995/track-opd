package com.trackopd.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ParseException;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.ColorInt;
import android.support.v4.content.FileProvider;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.amulyakhare.textdrawable.TextDrawable;
import com.trackopd.BuildConfig;
import com.trackopd.R;
import com.trackopd.model.PatientPrescriptionModel;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;

public class Common {

    private static View mViewAnimation;
    private static ProgressDialog mProgressDialog;

    /**
     * Insert Log
     *
     * @param mMessage - Message of the defined log
     */
    public static void insertLog(String mMessage) {
        Log.e("Tag", mMessage);
    }

    /**
     * Get file Path from SD Card
     *
     * @return - Return file path
     */
    public static String getFileSdcardPath(Context context, String mFolderName) {
        String sdCardRoot = "/" + context.getResources().getString(R.string.app_name) + "/" + mFolderName;
        Common.insertLog("Storage Path : " + sdCardRoot);
        return sdCardRoot;
    }

    /**
     * Open up the default dialog from bottom
     *
     * @param context       - Context
     * @param mDownloadFile - Download File
     * @param mDocumentName - Document Name
     */
    public static void openDocument(Context context, File mDownloadFile, String mDocumentName) {
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            Uri mUri = FileProvider.getUriForFile(context,
                    BuildConfig.APPLICATION_ID + ".provider",
                    mDownloadFile);
            setDocumentDataType(context, mUri, mDocumentName, intent);
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Common.setCustomToast(context, context.getResources().getString(R.string.no_file_found));
        }
    }

    /**
     * Sets up the document type as per the document name received
     *
     * @param context       - Context
     * @param mUri          - URI
     * @param mDocumentName - Document Name
     * @param intent        - Intent
     */
    public static void setDocumentDataType(Context context, Uri mUri, String mDocumentName, Intent intent) {
        try {
            String[] splitDocName = mDocumentName.split("\\.");
            String mExtension = "." + splitDocName[1];
            if (mExtension.equalsIgnoreCase(AppConstants.STR_EXT_JPG) || mExtension.equalsIgnoreCase(AppConstants
                    .STR_EXT_JPEG) || mExtension.equalsIgnoreCase(AppConstants.STR_EXT_PNG)) {
                intent.setDataAndType(mUri, AppConstants.STR_DOC_TYPE_JPG);     // JPG file
            } else if (mExtension.equalsIgnoreCase(AppConstants.STR_EXT_GIF)) {
                intent.setDataAndType(mUri, AppConstants.STR_DOC_TYPE_GIF);     // GIF file
            } else if (mExtension.equalsIgnoreCase(AppConstants.STR_EXT_PDF)) {
                intent.setDataAndType(mUri, AppConstants.STR_DOC_TYPE_PDF);     // PDF file
            } else if (mExtension.equalsIgnoreCase(AppConstants.STR_EXT_TXT)) {
                intent.setDataAndType(mUri, AppConstants.STR_DOC_TYPE_TXT);     // Text file
            } else if (mExtension.equalsIgnoreCase(AppConstants.STR_EXT_DOC) || mExtension.equalsIgnoreCase
                    (AppConstants.STR_EXT_DOCX)) {
                intent.setDataAndType(mUri, AppConstants.STR_DOC_TYPE_DOC);     // Word File
            } else if (mExtension.equalsIgnoreCase(AppConstants.STR_EXT_XLS) || mExtension.equalsIgnoreCase
                    (AppConstants.STR_EXT_XLSX)) {
                intent.setDataAndType(mUri, AppConstants.STR_DOC_TYPE_EXCEL);   // Excel file
            } else if (mExtension.equalsIgnoreCase(AppConstants.STR_EXT_PPT) || mExtension.equalsIgnoreCase
                    (AppConstants.STR_EXT_PPTX)) {
                intent.setDataAndType(mUri, AppConstants.STR_DOC_TYPE_PPT);     // Powerpoint File
            } else {
                intent.setDataAndType(mUri, AppConstants.STR_EXT_ALL);
            }
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } catch (ActivityNotFoundException e) {
            Common.setCustomToast(context, context.getResources().getString(R.string.no_file_found));
        }
    }

    /**
     * This method should set the labeled image view
     *
     * @param context    - Context
     * @param mFirstName - Label First Name
     * @param mLastName  - Label Last Name
     * @return - Return Drawable Builder
     */
    public static Drawable setLabeledImageView(Context context, String mFirstName, String mLastName) {
        TextDrawable.IBuilder mDrawableBuilder = TextDrawable.builder().round();

        String LastName = TextUtils.isEmpty(mLastName) ? AppConstants.STR_EMPTY_STRING : String.valueOf(mLastName.charAt(0)).toUpperCase();
        return mDrawableBuilder.build(String.valueOf(mFirstName.charAt(0)).toUpperCase() + LastName, setThemeColor(context));
    }

    /**
     * This method should set the labeled image view
     *
     * @param context    - Context
     * @param mFirstName - Label First Name
     * @param mLastName  - Label Last Name
     * @return - Return Drawable Builder
     */
    public static Drawable setLabeledDefaultImageView(Context context, String mFirstName, String mLastName) {
        String LastName = TextUtils.isEmpty(mLastName) ? AppConstants.STR_EMPTY_STRING : String.valueOf(mLastName.charAt(0)).toUpperCase();

        int color1 = Common.setThemeColor(context);
        return TextDrawable.builder()
                .beginConfig()
                .textColor(color1)
                .useFont(Typeface.DEFAULT)
                .toUpperCase()
                .endConfig()
                .buildRound(String.valueOf(mFirstName.charAt(0)).toUpperCase() + LastName + "", Color.WHITE);
    }

    /**
     * Sets the theme color
     *
     * @param context - Context
     * @return - Return color as per the theme applied
     */
    public static int setThemeColor(Context context) {
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = context.getTheme();
        theme.resolveAttribute(R.attr.colorPrimary, typedValue, true);
        @ColorInt int mColor = typedValue.data;
        return mColor;
    }

    /**
     * This method should convert the date without time format to the given date format
     *
     * @param context    - Context
     * @param date       - Date
     * @param dateFormat - Date Format
     * @return - Return particular format for the given date
     */
    public static String convertDateWithoutTime(Context context, String date, String dateFormat) {
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy));
            Date d = format.parse(date);
            @SuppressLint("SimpleDateFormat") SimpleDateFormat serverFormat = new SimpleDateFormat(dateFormat);
            return serverFormat.format(d);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Open date & time picker
     *
     * @param context   - Context
     * @param mTextView - Text View to set the date & time
     */
    public static void openDateTimePicker(final Context context, final TextView mTextView) {

        int mYear, mMonth, mDay, mHour, mMinute;

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        c.set(year, monthOfYear, dayOfMonth);
                        new TimePickerDialog(context, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
                            @SuppressLint("SimpleDateFormat")
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                c.set(Calendar.MINUTE, minute);
                                mTextView.setText(new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss)).
                                        format(c.getTime()));
                            }
                        }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), false).show();

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        datePickerDialog.show();
    }

    /**
     * Open date & time picker
     *
     * @param context   - Context
     * @param mTextView - Text View to set the date & time
     */
    public static void openTimePicker(final Context context, final TextView mTextView) {

        int mYear, mMonth, mDay, mHour, mMinute;

        // Get Current Time
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(context,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        mTextView.setText(new SimpleDateFormat(context.getResources().getString(R.string.date_format_hh_mm_a)).
                                format(c.getTime())
                                .replace(context.getResources().getString(R.string.hour_format_small_am), context.getResources().getString(R.string.hour_format_am))
                                .replace(context.getResources().getString(R.string.hour_format_small_pm), context.getResources().getString(R.string.hour_format_pm)));
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    /**
     * Open the date picker
     *
     * @param context   - Context
     * @param mTextView - Text View to set the date
     */
    public static void openDatePicker(final Context context, final TextView mTextView) {

        int mYear, mMonth, mDay, mHour, mMinute;

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        c.set(year, monthOfYear, dayOfMonth);
                        mTextView.setText(new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy)).
                                format(c.getTime()));

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        datePickerDialog.show();
    }


    /**
     * Open the Past date picker
     *
     * @param context   - Context
     * @param mTextView - Text View to set the date
     */
    public static void openPastDatePicker(final Context context, final EditText mTextView) {

        int mYear, mMonth, mDay, mHour, mMinute;

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        c.set(year, monthOfYear, dayOfMonth);
                        mTextView.setText(new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy)).
                                format(c.getTime()));

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        datePickerDialog.show();
    }

    /**
     * Open the Past date picker
     *
     * @param context   - Context
     * @param mTextView - Text View to set the date
     */
    public static void openPastDateTimePicker(final Context context, final EditText mTextView) {
        final Calendar calendar;
        try {
            calendar = Calendar.getInstance();
            DatePickerDialog datePickerDialog = new DatePickerDialog(context, R.style.DialogTheme, new DatePickerDialog.OnDateSetListener() {

                @SuppressLint("SimpleDateFormat")
                @Override
                public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {

                    calendar.set(year, monthOfYear, dayOfMonth);
                    new TimePickerDialog(context, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
                        @SuppressLint("SimpleDateFormat")
                        @Override
                        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                            calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            calendar.set(Calendar.MINUTE, minute);
                            mTextView.setText(new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss)).
                                    format(calendar.getTime()));
                        }
                    }, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), false).show();

                }
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DATE));
            datePickerDialog.getDatePicker().setMaxDate(calendar.getTimeInMillis());
            datePickerDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Open the Past date picker
     *
     * @param context - Context
     * @param -       Text View to set the date
     */
    public static void openEditAllDateTimePicker(final Context context, final EditText mTextView) {

        int mYear, mMonth, mDay, mHour, mMinute;

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        c.set(year, monthOfYear, dayOfMonth);
                        new TimePickerDialog(context, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
                            @SuppressLint("SimpleDateFormat")
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                c.set(Calendar.MINUTE, minute);
                                mTextView.setText(new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss)).
                                        format(c.getTime()));
                            }
                        }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), false).show();

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

    }

    /**
     * Open date picker for all dates like past, present & future dates are visible to select
     *
     * @param context   - Context
     * @param mTextView - Text View to set the date
     */
    public static void openDatePickerForAllDates(final Context context, final TextView mTextView) {

        int mYear, mMonth, mDay, mHour, mMinute;

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        c.set(year, monthOfYear, dayOfMonth);
                        mTextView.setText(new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy)).
                                format(c.getTime()));

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public static void openAllDateTimePicker(final Context context, final TextView mTextView) {

        int mYear, mMonth, mDay, mHour, mMinute;

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(context,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        c.set(year, monthOfYear, dayOfMonth);
                        new TimePickerDialog(context, R.style.DialogTheme, new TimePickerDialog.OnTimeSetListener() {
                            @SuppressLint("SimpleDateFormat")
                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                                c.set(Calendar.HOUR_OF_DAY, hourOfDay);
                                c.set(Calendar.MINUTE, minute);
                                mTextView.setText(new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss)).
                                        format(c.getTime()));
                            }
                        }, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), false).show();

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    /**
     * This method should convert the date and time from the server date
     *
     * @param context - Context
     * @param Date    - String date
     * @return - Return date in string format
     */
    public static String convertDateTimeToServer(Context context, String Date) {
        String strDate = Date;
        try {
            @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss));
            java.util.Date newDate = format.parse(Date);
            strDate = format.format(newDate);

            @SuppressLint("SimpleDateFormat") SimpleDateFormat formatDate = new SimpleDateFormat(context.getResources().getString(R.string.date_format_yyyy_mm_dd_hh_mm_ss));
            strDate = formatDate.format(newDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strDate;
    }

    /**
     * This method should convert the date and time from the server date
     *
     * @param context - Context
     * @param Date    - String date
     * @return - Return date in string format
     */
    public static String convertDateUsingDateFormat(Context context, String Date, String mServerDateFormat, String mOutputDateFormat) {
        String strDate = Date;
        try {
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat format = new SimpleDateFormat(mServerDateFormat);
            java.util.Date newDate = format.parse(Date);
            strDate = format.format(newDate);

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat formatDate = new SimpleDateFormat(mOutputDateFormat);
            strDate = formatDate.format(newDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strDate;
    }

    /**
     * This method should convert the date from the server date in the given format
     *
     * @param context - Context
     * @param Date    - String date
     * @return - Return date in string format
     */
    public static String convertDateToServer(Context context, String Date) {
        String strDate = Date;
        try {

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat format = new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy));
            java.util.Date newDate = format.parse(Date);
            strDate = format.format(newDate);

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat formatDate = new SimpleDateFormat(context.getResources().getString(R.string.date_format_yyyy_mm_dd));
            strDate = formatDate.format(newDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strDate;
    }

    @SuppressLint("SimpleDateFormat")
    public static String convertTime(Context context, String mTime) {
        String mConvertedTime = mTime;
        try {
            // ToDo: Split time
            String[] time = mConvertedTime.split("-");
            String mSplitTime = time[0];

            // ToDO: Convert split time to 24 hours and return the string
            final SimpleDateFormat sdf = new SimpleDateFormat(context.getResources().getString(R.string.date_format_hh_mm_aa));
            final Date dateObj = sdf.parse(mSplitTime);
            mConvertedTime = new SimpleDateFormat(context.getResources().getString(R.string.date_format_hh_mm_capital)).format(dateObj);
        } catch (final Exception e) {
            e.printStackTrace();
        }
        return mConvertedTime;
    }

    /**
     * Sets the current date & time
     *
     * @param context - Context
     * @return - Return date in date format
     */
    public static String setCurrentDateTime(Context context) {
        Date date = Calendar.getInstance().getTime();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat df = new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss));
        return df.format(date);
    }

    /**
     * Sets the current date & time
     *
     * @param context - Context
     * @return - Return date in date format
     */
    public static String setCheckCurrentDateTime(Context context) {
        Date date = Calendar.getInstance().getTime();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat df = new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy_kk_mm_ss_hyphen));
        return df.format(date);
    }


    /**
     * Set Convert String Date one format to another Format
     *
     * @param mActivity          -Activity
     * @param date               -String Date
     * @param input_date         - Input Date Format
     * @param output_date_format - Output Date Format
     * @return -Return String
     */
    public static String setConvertDateFormat(Activity mActivity, String date, String input_date, String output_date_format) {
        String strDate = date;
        try {

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat format = new SimpleDateFormat(input_date);
            Date newDate = format.parse(date);
            strDate = format.format(newDate);

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat formatDate = new SimpleDateFormat(output_date_format);
            strDate = formatDate.format(newDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return strDate;
    }

    /**
     * Sets the current date
     *
     * @param context - Context
     * @return - Return date in date format
     */
    public static String setCurrentDate(Context context) {
        Date date = Calendar.getInstance().getTime();
        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat df = new SimpleDateFormat(context.getResources().getString(R.string.date_format_dd_mm_yyyy));
        return df.format(date);
    }

    /**
     * Sets the toast message
     *
     * @param context - Context
     * @param message - Message for showing the toast
     */
    public static void setCustomToast(Context context, String message) {
        final Toast toast = Toast.makeText(context, message, Toast.LENGTH_SHORT);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, 5000);
    }


    /**
     * This method should check the string is null or empty
     *
     * @param context - Context
     * @param mValue  - Value
     * @return - Return value if string is not null otherwise return Na is string gets null
     */
    public static String isEmptyString(Context context, String mValue) {
        if (mValue != null && mValue.length() == 0) {
            mValue = context.getResources().getString(R.string.no_value_available);
        }
        return mValue;
    }

    /**
     * This method should check the string is null or empty
     *
     * @param context - Context
     * @return - Return value if string is not null otherwise return Na is string gets null
     */
    public static int getPageSizeForPagination(Context context) {
        return Integer.parseInt(GetJsonData.getConfigurationData(context, WebFields.CONFIGURATION.RESPONSE_PAGE_SIZE));
    }

    public static Runnable setAnimation(Activity mActivity, View mView, ViewGroup mGroupRevealAnimation) {
        Runnable revealAnimationRunnable = new Runnable() {
            @Override
            public void run() {
                int cx = mView.getRight();
                int cy = mView.getTop();

                int finalRadius = Math.max(mView.getWidth(), mView.getHeight());
                Animator animator = ViewAnimationUtils.createCircularReveal(mView, cx, cy, 0, finalRadius);
                mGroupRevealAnimation.setBackgroundColor(Common.setThemeColor(mActivity));
                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mGroupRevealAnimation.setBackground(mActivity.getResources().getDrawable(R.drawable.app_background));
                    }
                });
                animator.start();

            }
        };
        return revealAnimationRunnable;
    }

    /**
     * @param context  - context
     * @param body     - Body
     * @param FileName - Patient Prescription File Name
     * @return - Return boolean value true if file downloaded otherwise return false
     */
    public static boolean saveFileToStorage(Context context, String FolderName, ResponseBody body, String FileName) {
        try {
            File mFile = new File(Environment.getExternalStorageDirectory() + "/"
                    + getFileSdcardPath(context, FolderName));
            Common.insertLog("File Path::::> " + mFile);
            if (!mFile.exists()) {
                mFile.mkdirs();
            }

            File futureStudioIconFile = new File(mFile.getPath() + File.separator + FileName);
            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];
                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;
                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);
                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);
                    fileSizeDownloaded += read;
                    Common.insertLog("file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();
                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    public static File checkIsFileDownloaded(Context context, String mFolderName, String mFileName) {
        File mFile = new File(Environment.getExternalStorageDirectory() + "/"
                + Common.getFileSdcardPath(context, mFolderName));

        return new File(mFile, mFileName);
    }

    /**
     * Get Age From Birth Date
     *
     * @param dobString
     * @return
     */
    public static String getAge(String dobString) {

        String[] arr = dobString.split(AppConstants.STR_FORWARD_SLASH);
        int day = Integer.parseInt(arr[0]);
        int month = Integer.parseInt(arr[1]);
        int year = Integer.parseInt(arr[2]);


        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();

        return ageS;
    }

    public static void hideSoftKeyboard(Activity mActivity) {
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(mActivity.INPUT_METHOD_SERVICE);
        if (mActivity.getCurrentFocus() != null)
            imm.hideSoftInputFromWindow(mActivity.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /*
     * This method is fetching the absolute path of the image file
     * if you want to upload other kind of files like .pdf, .docx
     * you need to make changes on this method only
     * Rest part will be the same
     * */
    public static String getRealPathFromURI(Uri contentUri, Activity mActivity) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = mActivity.managedQuery(contentUri, proj, null, null, null);

        if (cursor == null)
            return null;

        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    // to scale the bitmap
    public static Bitmap decodeFile(File f, int WIDTH, int HIGHT) {
        try {
            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f), null, o);

            // The new size we want to scale to
            final int REQUIRED_WIDTH = WIDTH;
            final int REQUIRED_HIGHT = HIGHT;
            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= REQUIRED_WIDTH
                    && o.outHeight / scale / 2 >= REQUIRED_HIGHT)
                scale *= 2;

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {

        }
        return null;
    }

    public static void showLoadingDialog(Context context, String message) {
        mProgressDialog = new ProgressDialog(context);
        mProgressDialog.setMessage(message);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
    }

    public static void hideDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            mProgressDialog.dismiss();
    }

    public static Date addDay(Date date, int i) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_YEAR, i);
        return cal.getTime();
    }


    /**
     * TODO:Get Date Time Difference
     *
     * @param startDate -Current Date
     * @param endDate   -Check in Date.
     */
    public static String printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = startDate.getTime() - endDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        if (elapsedSeconds == 60) {
            elapsedMinutes = elapsedMinutes + 1;
        }

        String date = elapsedHours + "H ," + elapsedMinutes + "M," +
                elapsedSeconds + "S";
        return date;
    }

    public static InputFilter[] getFilter()
    {
        InputFilter EMOJI_FILTER = new InputFilter() {

            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int index = start; index < end; index++) {

                    int type = Character.getType(source.charAt(index));

                    if (type == Character.SURROGATE
                            || type==Character.OTHER_SYMBOL) {
                        return "";
                    }
                }
                return null;
            }
        };
        return new InputFilter[]{EMOJI_FILTER};
    }

    public static InputFilter[] setFilter() {

        InputFilter filter = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; ++i) {
                    if (!Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZ  abcdefghijklmnopqrstuvwxyz1234567890]*").matcher(String.valueOf(source.charAt(i))).matches()) {
                        return "";
                    }
                }

                return null;
            }
        };
        return new InputFilter[]{filter};
    }

}
