package com.trackopd.utils;

public class StringUtils {

    /**
     * Checks if string is empty or not
     *
     * @param value string to check
     * @return true if empty or null, false otherwise
     */
    public static boolean isEmpty(String value) {
        try {
            value = value.trim();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value == null || value.length() == 0;
    }

    /**
     * Checks if string is valid and having at least minimum number of characters or not
     *
     * @param value string to check
     * @param min   minimum length of validity
     * @return true if valid, false otherwise
     */
    public static boolean isValid(String value, int min) {
        try {
            value = value.trim();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value != null && value.length() >= min;
    }

    /**
     * Checks if string is valid or not
     *
     * @param value string to check
     * @return true if valid, false otherwise
     */
    public static boolean isValid(String value) {
        try {
            value = value.trim();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value != null && value.length() > 0;
    }


    public static boolean equals(String str1, String str2) {
        if (str1 == null && str2 == null) {
            return true;
        } else if (str1 != null && str2 != null) {
            return str1.equals(str2);
        }
        return false;
    }

    /***
     * Check if the Email is valid or not
     *
     * @param email - Email
     * @return - Return pattern matches
     */
    /*public static boolean isEmailValid(String email) {
        String regExpn = "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

//        CharSequence inputStr = email;



        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);

        if (matcher.matches()) {
            return true;
        } else {
            return false;
        }
    }*/

    /**
     * Check if the Email is valid or not
     *
     * @param email
     * @return Return pattern matches Valid Email Address
     */
    public static boolean isEmailValid(String email) {

        boolean result = false;
        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

        if (email.matches(emailPattern)) {
            result = true;
        }
        return result;
    }
}
