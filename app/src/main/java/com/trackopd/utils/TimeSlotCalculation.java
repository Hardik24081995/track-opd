package com.trackopd.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;

import com.trackopd.R;
import com.trackopd.model.TimeSlotModel;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class TimeSlotCalculation {

    private ArrayList<TimeSlotModel> slotArrayList;

    public TimeSlotCalculation() {
        slotArrayList = new ArrayList<>();
    }

    /**
     * Calculate time slot on bases of time slot duration
     * @param mActivity - Activity
     * @param OpeningTime - Opening Time String
     * @param ClosingTime - Closing Time String
     * @param SelectedDate - Selected Date String
     * @param CurrentDate - Current Date String
     * @return - Return Time Slot Array
     */
    public ArrayList<TimeSlotModel> calculate(Activity mActivity, String OpeningTime, String ClosingTime, Date SelectedDate, Date CurrentDate){
        try {
            Calendar endTimeCalender = Calendar.getInstance();
            Date endDate = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_hh_mm_a), Locale.US).parse(ClosingTime);
            endTimeCalender.setTime(endDate);

            Calendar slotTimingCalender = Calendar.getInstance();
            slotTimingCalender.setTime(new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_hh_mm_a),Locale.US).parse(OpeningTime));
            Date closingTime = endTimeCalender.getTime();

            // Current Date Time
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat df = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_hh_mm_a));
            String formattedCDate = df.format(Calendar.getInstance().getTime());
            @SuppressLint("SimpleDateFormat")
            Date cDate = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_hh_mm_a)).parse(formattedCDate);

            do {
                TimeSlotModel timeSlot = new TimeSlotModel();

                // Starting Slot
                String startingSlot = getFormattedTime(mActivity, slotTimingCalender);
                timeSlot.setStartTime(getFormattedTimeSlot(mActivity, slotTimingCalender));

                // Make Slots of 30 minutes
                String mTimeSlotDurationInMinutes = GetJsonData.getConfigurationData(mActivity, WebFields.CONFIGURATION.RESPONSE_TIME_SLOT_DURATION);
                slotTimingCalender.add(Calendar.MINUTE, Integer.parseInt(mTimeSlotDurationInMinutes));

                // Ending Slot
                String EndingSlot = getFormattedTime(mActivity, slotTimingCalender);
                timeSlot.setEndTime(getFormattedTimeSlot(mActivity, slotTimingCalender));

                timeSlot.setTimeSlot(startingSlot + AppConstants.STR_SPACE_HYPHEN +EndingSlot);

                // If the selected date is current then check time and show the slot after the current time
                if(SelectedDate.equals(CurrentDate) && slotTimingCalender.getTime().before(cDate)){
                } else {
                    slotArrayList.add(timeSlot);
                }
            } while (closingTime.after(slotTimingCalender.getTime()));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return slotArrayList;
    }

    /**
     * Get formatted time
     * @param mActivity - Activity
     * @param calender - Calender
     * @return
     */
    public static String getFormattedTime(Activity mActivity, Calendar calender){
        int am_pm;
        String AM_orPM ;

        am_pm = calender.get(Calendar.AM_PM);
        AM_orPM  = (am_pm == Calendar.AM) ? "AM" : "PM";

        android.text.format.DateFormat df = new android.text.format.DateFormat();
        return df.format(mActivity.getResources().getString(R.string.date_format_hh_mm), calender.getTime()).toString() + AppConstants.STR_EMPTY_SPACE+AM_orPM ;
    }

    /**
     * Get formatted time slot
     * @param mActivity - Activity
     * @param calender - Calender
     * @return
     */
    public static String getFormattedTimeSlot(Activity mActivity, Calendar calender){
        int am_pm;
        String AM_orPM ;

        am_pm = calender.get(Calendar.AM_PM);
        AM_orPM  = (am_pm == Calendar.AM) ? "AM" : "PM";

        android.text.format.DateFormat df = new android.text.format.DateFormat();
        return df.format(mActivity.getResources().getString(R.string.date_format_hh_mm_a), calender.getTime()).toString();
    }


    /**
     * Calculate time slot on bases of time slot duration
     * @param mActivity - Activity
     * @param OpeningTime - Opening Time String
     * @param ClosingTime - Closing Time String
     * @param SelectedDate - Selected Date String
     * @return - Return Time Slot Array
     */
    public ArrayList<TimeSlotModel> calculateTime(Activity mActivity, String OpeningTime, String ClosingTime, Date SelectedDate){
        try {
            Calendar endTimeCalender = Calendar.getInstance();
            Date endDate = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_hh_mm_a), Locale.US).parse(ClosingTime);
            endTimeCalender.setTime(endDate);

            Calendar slotTimingCalender = Calendar.getInstance();
            slotTimingCalender.setTime(new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_hh_mm_a),Locale.US).parse(OpeningTime));
            Date closingTime = endTimeCalender.getTime();

            // Current Date Time
            @SuppressLint("SimpleDateFormat") SimpleDateFormat df = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_hh_mm_a));
            String formattedCDate = df.format(Calendar.getInstance().getTime());
            @SuppressLint("SimpleDateFormat") Date cDate = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_hh_mm_a)).parse(formattedCDate);

            do {
                TimeSlotModel timeSlot = new TimeSlotModel();

                // Starting Slot
                String startingSlot = getFormattedTime(mActivity, slotTimingCalender);
                timeSlot.setStartTime(getFormattedTimeSlot(mActivity, slotTimingCalender));

                // Make Slots of 30 minutes
                String mTimeSlotDurationInMinutes = GetJsonData.getConfigurationData(mActivity, WebFields.CONFIGURATION.RESPONSE_TIME_SLOT_DURATION);
                slotTimingCalender.add(Calendar.MINUTE, Integer.parseInt(mTimeSlotDurationInMinutes));

                // Ending Slot
                String EndingSlot = getFormattedTime(mActivity, slotTimingCalender);
                timeSlot.setEndTime(getFormattedTimeSlot(mActivity, slotTimingCalender));

                timeSlot.setTimeSlot(startingSlot + AppConstants.STR_SPACE_HYPHEN +EndingSlot);


                slotArrayList.add(timeSlot);

            } while (closingTime.after(slotTimingCalender.getTime()));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return slotArrayList;
    }
}
