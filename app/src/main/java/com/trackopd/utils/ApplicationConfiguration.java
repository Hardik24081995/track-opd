package com.trackopd.utils;

import android.content.Context;
import android.provider.Settings;

import java.text.SimpleDateFormat;

/**
 * Application file is used for all Configuration that are used in the Application
 */
public class ApplicationConfiguration {

    private Context mContext;

    public static int MAX_PASSWORD_LENGTH = 20;

    public ApplicationConfiguration(Context Ctx) {
        mContext = Ctx;
    }
}
