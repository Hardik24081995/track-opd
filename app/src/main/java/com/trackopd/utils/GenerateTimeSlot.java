package com.trackopd.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.res.ColorStateList;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nex3z.flowlayout.FlowLayout;
import com.trackopd.R;
import com.trackopd.model.ReceptionistAppointmentModel;
import com.trackopd.model.ReceptionistTimeSlotAppointmentModel;
import com.trackopd.model.TimeSlotModel;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GenerateTimeSlot {

    private  ArrayList<TimeSlotModel> mArrTimeSlotModel;
    private  TimeSlotModel timeSlotModel;
    private  Button mButtonPrevious = null;
    private String mTimeSlot, mName, mPatientCode, mMobileNo, mAppointmentNo, mAppointmentDate,
            mDoctorId;
    private  ArrayList<ReceptionistTimeSlotAppointmentModel.Data> mArrAppointment;
    private Activity mActivity;
//    private ArrayList<ReceptionistAppointmentModel> mArrTimeSlotsAppointment;

    /**
     * Open time slot dialog
     *
     * @param mActivity - Activity
     * @param mTextView - Text View
     */
    /*public static void openTimeSlotDialog(Activity mActivity, TextView mTextView) {
        Common.insertLog("SLOT 1:::> " + mTextView.getText().toString());
        try {

            mArrAppointment=new ArrayList<>();
            mArrTimeSlotsAppointment=new ArrayList<>();

            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.custom_dialog_time_slot);

            // Button
            Button mButtonOk = dialog.findViewById(R.id.button_dialog_time_slot_ok);

            // ToDo: Generate Time Slot
            generateTimeSlot(mActivity, dialog, mTextView);

            // ToDo: Button Ok Click Listener
            mButtonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /**
     * This method should generate time slot from morning, noon to evening
     *
     * @param mActivity - Activity
     * @param dialog    - Dialog View
     * @param mTextView - Text View
     */
    /*public static void generateTimeSlot(final Activity mActivity, Dialog dialog, final TextView mTextView) {
        Common.insertLog("SLOT 2:::> " + mTextView.getText().toString());

        // Local time slot variables
        String mOpeningTime = mActivity.getResources().getString(R.string.time_slot_opening_time);
        String mClosingTime = mActivity.getResources().getString(R.string.time_slot_closing_time);
        String mMorningEndTime = mActivity.getResources().getString(R.string.time_slot_morning_end_time);
        String mNoonEndTime = mActivity.getResources().getString(R.string.time_slot_noon_end_time);
        String mTodayDate;
        Date mDateSelected;
        Date mDateCurrent;

        try {
            // Sets up the default views
            setDefault(dialog);

            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat format = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));
            mTodayDate = Common.setCurrentDate(mActivity);
            mDateSelected = format.parse(mTodayDate);
            mDateCurrent = format.parse(mTodayDate);

            // Calculate time slots based on the real time
            TimeSlotCalculation timeSlotCalculation = new TimeSlotCalculation();
            mArrTimeSlotModel = timeSlotCalculation.calculate(mActivity, mOpeningTime, mClosingTime, mDateSelected, mDateCurrent);

            // Condition calls when no slots available
            if (mArrTimeSlotModel == null || mArrTimeSlotModel.size() <= 0) {
                // ToDo: Linear Layout - No Slots Available
                LinearLayout mLinearNoData = dialog.findViewById(R.id.linear_dialog_time_slot_no_data);
                setViewVisibility(mLinearNoData, true);
            }

            // Condition calls when slots are available
            for (int i = 0; i < mArrTimeSlotModel.size(); i++) {
                View child = mActivity.getLayoutInflater().inflate(R.layout.row_time_slot_item, null);
                Button mButtonTimeSlot = (Button) child;

                mTimeSlot = mArrTimeSlotModel.get(i).getTimeSlot();
                mButtonTimeSlot.setText(mTimeSlot);
                mButtonTimeSlot.setTag(i);

                if (i == 0 && mTextView.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                    setTimeSlotButtonBackground(mActivity, mButtonTimeSlot, mTextView);
                }

                try {
                    Date MorningClosingDate = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_hh_mm_a), Locale.US).parse(mMorningEndTime);
                    Date NoonClosingDate = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_hh_mm_a), Locale.US).parse(mNoonEndTime);
                    @SuppressLint("SimpleDateFormat") Date ClosingDate = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_hh_mm_a)).parse(mArrTimeSlotModel.get(i).getStartTime());

                    if (ClosingDate.before(MorningClosingDate)) {
                        // ToDo: Flow Layout - Morning
                        FlowLayout mFlowMorning = dialog.findViewById(R.id.flow_layout_dialog_time_slot_morning);
                        mFlowMorning.addView(child);

                        // ToDo: Linear Layout - Morning
                        LinearLayout mLinearMorning = dialog.findViewById(R.id.linear_dialog_time_slot_morning);
                        setViewVisibility(mLinearMorning, true);
                    } else if (NoonClosingDate.after(ClosingDate)) {
                        // ToDo: Flow Layout - Noon
                        FlowLayout mFlowNoon = dialog.findViewById(R.id.flow_layout_dialog_time_slot_noon);
                        mFlowNoon.addView(child);

                        // ToDo: Linear Layout - Noon
                        LinearLayout mLinearNoon = dialog.findViewById(R.id.linear_dialog_time_slot_noon);
                        setViewVisibility(mLinearNoon, true);
                    } else {
                        // ToDo: Flow Layout - Evening
                        FlowLayout mFlowEvening = dialog.findViewById(R.id.flow_layout_dialog_time_slot_evening);
                        mFlowEvening.addView(child);

                        // ToDo: Linear Layout - Evening
                        LinearLayout mLinearEvening = dialog.findViewById(R.id.linear_dialog_time_slot_evening);
                        setViewVisibility(mLinearEvening, true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // When user comes up for the first time, it will set text based on the time slot
                // selection otherwise sets the selected time slot if already selected.
                if (mTextView.getText().toString().equalsIgnoreCase(mTimeSlot)) {
                    mButtonPrevious = mButtonTimeSlot;
                    @SuppressLint("ResourceType") ColorStateList colorStateList = ContextCompat.getColorStateList(mActivity,
                            R.drawable.time_slot_custom_button_press_text_bg);
                    mButtonTimeSlot.setTextColor(colorStateList);
                    mButtonTimeSlot.setBackgroundResource(R.drawable.time_slot_custom_button_press_bg);
                }

                // Time slot button click listener
                mButtonTimeSlot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Button mButtonTimeSlot1 = (Button) v;
                        setTimeSlotButtonBackground(mActivity, mButtonTimeSlot1, mTextView);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /**
     * Sets up the default views for time slot dialog
     *
     * @param dialog - Dialog View
     */
    public  void setDefault(Dialog dialog) {
        try {
            mButtonPrevious = null;
            timeSlotModel = null;

            // ToDo: Linear Layout - Morning
            LinearLayout mLinearMorning = dialog.findViewById(R.id.linear_dialog_time_slot_morning);
            setViewVisibility(mLinearMorning, false);

            // ToDo: Linear Layout - Noon
            LinearLayout mLinearNoon = dialog.findViewById(R.id.linear_dialog_time_slot_noon);
            setViewVisibility(mLinearNoon, false);

            // ToDo: Linear Layout - Evening
            LinearLayout mLinearEvening = dialog.findViewById(R.id.linear_dialog_time_slot_evening);
            setViewVisibility(mLinearEvening, false);

            // ToDo: Linear Layout - No Slots Available
            LinearLayout mLinearNoData = dialog.findViewById(R.id.linear_dialog_time_slot_no_data);
            setViewVisibility(mLinearNoData, false);

            // ToDo: Flow Layout - Morning
            FlowLayout mFlowMorning = dialog.findViewById(R.id.flow_layout_dialog_time_slot_morning);
            mFlowMorning.removeAllViews();

            // ToDo: Flow Layout - Noon
            FlowLayout mFlowNoon = dialog.findViewById(R.id.flow_layout_dialog_time_slot_noon);
            mFlowNoon.removeAllViews();

            // ToDo: Flow Layout - Evening
            FlowLayout mFlowEvening = dialog.findViewById(R.id.flow_layout_dialog_time_slot_evening);
            mFlowEvening.removeAllViews();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should set the visibility as per the boolean value
     *
     * @param mLinearLayout - Linear Layout
     * @param mIsVisible    - Checks the visibility using this boolean variable
     */
    private static void setViewVisibility(LinearLayout mLinearLayout, boolean mIsVisible) {
        if (mIsVisible) {
            mLinearLayout.setVisibility(View.VISIBLE);
        } else {
            mLinearLayout.setVisibility(View.GONE);
        }
    }

    /**
     * Sets the time slot background as per the theme applied
     *
     * @param mActivity       - Activity
     * @param mButtonTimeSlot - Button Time Slot View
     * @param mTextView       - Text View
     */
    private  void setTimeSlotButtonBackground(Activity mActivity, Button mButtonTimeSlot, TextView mTextView) {
        // ToDo: Sets the time slot button selected background color
        @SuppressLint("ResourceType")
        ColorStateList colorStateList = ContextCompat.getColorStateList(mActivity,
                R.drawable.time_slot_custom_button_press_text_bg);
        mButtonTimeSlot.setTextColor(colorStateList);
        mButtonTimeSlot.setBackgroundResource(R.drawable.time_slot_custom_button_press_bg);

        // ToDo: Sets the time slot button default background color
        if (mButtonPrevious != null) {
            @SuppressLint("ResourceType") ColorStateList previousColorStateList = ContextCompat.getColorStateList(mActivity,
                    R.drawable.time_slot_custom_button_text_bg);
            mButtonPrevious.setTextColor(previousColorStateList);
            mButtonPrevious.setBackgroundResource(R.drawable.time_slot_custom_button_bg);
        }

        // ToDo: Bind the time slots to the model
        if (mButtonPrevious != null && mButtonPrevious == mButtonTimeSlot) {
            mButtonPrevious = null;
            timeSlotModel = null;
        } else {
            mButtonPrevious = mButtonTimeSlot;
            timeSlotModel = mArrTimeSlotModel.get((Integer) mButtonTimeSlot.getTag());
        }

        // ToDo: Sets the selected time slot to the text view
        if (timeSlotModel != null) {
            mTimeSlot = (timeSlotModel.getStartTime() + AppConstants.STR_SPACE_HYPHEN + timeSlotModel.getEndTime())
                    .replace(mActivity.getResources().getString(R.string.hour_format_small_pm), mActivity.getResources().getString(R.string.hour_format_pm))
                    .replace(mActivity.getResources().getString(R.string.hour_format_small_am), mActivity.getResources().getString(R.string.hour_format_am));
        } else {
            mTimeSlot = AppConstants.STR_EMPTY_STRING;
        }
        mTextView.setText(mTimeSlot);
    }

    /**
     * this Method set Selected Date
     *
     * @param mActivity - Activity
     * @param mEditTime - Text View set Text
     * @param mEditDate - Selected Date
     */
    /*public static void openTimeSlotDialog(Activity mActivity, EditText mEditTime, EditText mEditDate) {

        Common.insertLog("SLOT 1:::> " + mEditTime.getText().toString());
        try {


            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.custom_dialog_time_slot);

            // Button
            Button mButtonOk = dialog.findViewById(R.id.button_dialog_time_slot_ok);


            // ToDo: Generate Time Slot
            selectedGenerateTimeSlot(mActivity, dialog, mEditTime, mEditDate);

            // ToDo: Button Ok Click Listener
            mButtonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /**
     * This method should generate time slot from morning, noon to evening
     *
     * @param mActivity - Activity
     * @param dialog    - Dialog View
     * @param mTextView - Text View
     */
    @SuppressLint("SetTextI18n")
    public void selectedGenerateTimeSlot(final Activity mActivity, Dialog dialog,
                                                final TextView mTextView, EditText mTextDate) {
        Common.insertLog("SLOT 2:::> " + mTextView.getText().toString());

        // Local time slot variables
        String mOpeningTime = mActivity.getResources().getString(R.string.time_slot_opening_time);
        String mClosingTime = mActivity.getResources().getString(R.string.time_slot_closing_time);
        String mMorningEndTime = mActivity.getResources().getString(R.string.time_slot_morning_end_time);
        String mNoonEndTime = mActivity.getResources().getString(R.string.time_slot_noon_end_time);
        String mTodayDate, mSelectedDate;
        Date mDateSelected;
        Date mDateCurrent;

        try {
            // Sets up the default views
            setDefault(dialog);

            mSelectedDate = mTextDate.getText().toString();
            @SuppressLint("SimpleDateFormat")
            SimpleDateFormat format =
                    new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));
            mTodayDate = Common.setCurrentDate(mActivity);
            mDateSelected = format.parse(mSelectedDate);
            mDateCurrent = format.parse(mTodayDate);

            // Calculate time slots based on the real time
            TimeSlotCalculation timeSlotCalculation = new TimeSlotCalculation();
            mArrTimeSlotModel = timeSlotCalculation.calculate(mActivity, mOpeningTime, mClosingTime, mDateSelected, mDateCurrent);

            // Condition calls when no slots available
            if (mArrTimeSlotModel == null || mArrTimeSlotModel.size() <= 0) {
                // ToDo: Linear Layout - No Slots Available
                LinearLayout mLinearNoData = dialog.findViewById(R.id.linear_dialog_time_slot_no_data);
                setViewVisibility(mLinearNoData, true);
            }

            // Condition calls when slots are available
            for (int i = 0; i < mArrTimeSlotModel.size(); i++) {
                View child = mActivity.getLayoutInflater().inflate(R.layout.row_time_slot_item, null);
                Button mButtonTimeSlot = child.findViewById(R.id.button_row_time_slots_item_time);
                TextView mTextCount = child.findViewById(R.id.text_row_time_slots_item_count);

                String start_time = mArrTimeSlotModel.get(i).getStartTime();
                String convert_statTime = Common.setConvertDateFormat(mActivity
                        , start_time, mActivity.getResources().getString(R.string.date_format_hh_mm_aa),
                        "HH:mm:ss");

                char first = convert_statTime.charAt(0);

                if (first=='0'){
                    convert_statTime=convert_statTime.substring(1);
                }


                for (int k = 0; k < mArrAppointment.size(); k++)
                {

                    String time=mArrAppointment.get(k).getTimeSlot().trim();
                    if(convert_statTime.equalsIgnoreCase(time))
                    {
                        int book_count = mArrAppointment.get(k).getAppointment().size();
                        if ( book_count > 0)
                        {
                            mTextCount.setText("(" +  book_count+ ")");
                            continue;
                        }else {
                            mTextCount.setText("(" + "0" + ")");
                        }
                    }else {
//                        mTextCount.setText("(" + "0" + ")");
                    }
                }


                    /*if (mArrAppointment.get(k).getTimeSlot().trim()
                            .equals(convert_statTime))
                    {
                     ArrayList<ReceptionistAppointmentModel>   mArrTimeSlotsAppointment= (ArrayList<ReceptionistAppointmentModel>) mArrAppointment.get(k).getAppointment();

                        if (mArrTimeSlotsAppointment.size()>0){
                            int book_count = mArrTimeSlotsAppointment.size();
                            mTextCount.setText("(" + book_count + ")");
                        }else {
                            mTextCount.setText("(" + "0" + ")");
                        }
                    }else {
                        mTextCount.setText("(" + "0" + ")");
                    }*/

//                }

                mTimeSlot = mArrTimeSlotModel.get(i).getTimeSlot();

                mButtonTimeSlot.setText(mTimeSlot);
                mButtonTimeSlot.setTag(i);


                if (i == 0 && mTextView.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                    setTimeSlotButtonBackground(mActivity, mButtonTimeSlot, mTextView);
                }

                try {
                    Date MorningClosingDate = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_hh_mm_a), Locale.US).parse(mMorningEndTime);
                    Date NoonClosingDate = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_hh_mm_a), Locale.US).parse(mNoonEndTime);
                    @SuppressLint("SimpleDateFormat") Date ClosingDate = new SimpleDateFormat(mActivity.getResources().getString(R.string.date_format_hh_mm_a)).parse(mArrTimeSlotModel.get(i).getStartTime());

                    if (ClosingDate.before(MorningClosingDate)) {
                        // ToDo: Flow Layout - Morning
                        FlowLayout mFlowMorning = dialog.findViewById(R.id.flow_layout_dialog_time_slot_morning);
                        mFlowMorning.addView(child);

                        // ToDo: Linear Layout - Morning
                        LinearLayout mLinearMorning = dialog.findViewById(R.id.linear_dialog_time_slot_morning);
                        setViewVisibility(mLinearMorning, true);
                    } else if (NoonClosingDate.after(ClosingDate)) {
                        // ToDo: Flow Layout - Noon
                        FlowLayout mFlowNoon = dialog.findViewById(R.id.flow_layout_dialog_time_slot_noon);
                        mFlowNoon.addView(child);

                        // ToDo: Linear Layout - Noon
                        LinearLayout mLinearNoon = dialog.findViewById(R.id.linear_dialog_time_slot_noon);
                        setViewVisibility(mLinearNoon, true);
                    } else {
                        // ToDo: Flow Layout - Evening
                        FlowLayout mFlowEvening = dialog.findViewById(R.id.flow_layout_dialog_time_slot_evening);
                        mFlowEvening.addView(child);

                        // ToDo: Linear Layout - Evening
                        LinearLayout mLinearEvening = dialog.findViewById(R.id.linear_dialog_time_slot_evening);
                        setViewVisibility(mLinearEvening, true);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // When user comes up for the first time, it will set text based on the time slot
                // selection otherwise sets the selected time slot if already selected.
                if (mTextView.getText().toString().equalsIgnoreCase(mTimeSlot)) {
                    mButtonPrevious = mButtonTimeSlot;
                    @SuppressLint("ResourceType")
                    ColorStateList colorStateList = ContextCompat.getColorStateList(mActivity,
                            R.drawable.time_slot_custom_button_press_text_bg);
                    mButtonTimeSlot.setTextColor(colorStateList);
                    mButtonTimeSlot.setBackgroundResource(R.drawable.time_slot_custom_button_press_bg);
                }

                // Time slot button click listener
                mButtonTimeSlot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Button mButtonTimeSlot1 = (Button) v;
                        setTimeSlotButtonBackground(mActivity, mButtonTimeSlot1, mTextView);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the appointment listing for receptionist
     *
     * @param mActivity - Activity
     * @param dialog    - Dialog
     * @param mEditTime - Edit Text Time
     * @param mEditDate - Edit Text Date
     */
    private  void callAppointmentListAPI(Activity mActivity, Dialog dialog, EditText mEditTime,
                                               EditText mEditDate) {
        try {
            if (mName == null) {
                mName = AppConstants.STR_EMPTY_STRING;
            }

            if (mPatientCode == null) {
                mPatientCode = AppConstants.STR_EMPTY_STRING;
            }

            if (mMobileNo == null) {
                mMobileNo = AppConstants.STR_EMPTY_STRING;
            }

            if (mAppointmentNo == null) {
                mAppointmentNo = AppConstants.STR_EMPTY_STRING;
            }


            if (mDoctorId == null) {
                mDoctorId = "-1";
            }

            int currentPageIndex = 1;

            String mAppointmentDate = mEditDate.getText().toString();
            String convert_appointmentDate = Common.convertDateUsingDateFormat(mActivity, mAppointmentDate,
                    mActivity.getString(R.string.date_format_dd_mm_yyyy),
                    mActivity.getString(R.string.date_format_yyyy_mm_dd));

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            String mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);
            String mMRDNo = AppConstants.STR_EMPTY_STRING;
            String mAppointmentStatus = mActivity.getResources().getString(R.string.status_pending);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setReceptionistTimeAppointmentListJson(mActivity, currentPageIndex, mUserType, mName, mPatientCode, mMobileNo, mMRDNo, mAppointmentNo,
                            convert_appointmentDate,
                            Integer.parseInt(mDoctorId), mActivity.getResources().getString(R.string.tab_all),
                            mAppointmentStatus, String.valueOf(-1), hospital_database));

            Call<ReceptionistTimeSlotAppointmentModel> call = RetrofitClient.createService(ApiInterface.class).getReceptionistTimeAppointmentList(requestBody);

            call.enqueue(new Callback<ReceptionistTimeSlotAppointmentModel>() {
                @Override
                public void onResponse(Call<ReceptionistTimeSlotAppointmentModel> call, Response<ReceptionistTimeSlotAppointmentModel> response) {

                    Common.insertLog(response.body().toString());
                    if (response.isSuccessful() &&
                            response.body().getError() == 200) {

                        if (response.body().getData() != null && response.body().getData().size() > 0) {
                            mArrAppointment.addAll(response.body().getData());

                            selectedGenerateTimeSlot(mActivity, dialog, mEditTime, mEditDate);
                        }
                    } else {
                        Common.setCustomToast(mActivity, response.body().getMessage());
                        selectedGenerateTimeSlot(mActivity, dialog, mEditTime, mEditDate);
                    }
                }

                @Override
                public void onFailure(Call<ReceptionistTimeSlotAppointmentModel> call, Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                    selectedGenerateTimeSlot(mActivity, dialog, mEditTime, mEditDate);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            selectedGenerateTimeSlot(mActivity, dialog, mEditTime, mEditDate);
        }
    }

    public void openTimeSlotDialog(Activity mActivity, EditText mEditTime, EditText mEditDate,
                                          String doctor_id) {

        Common.insertLog("SLOT 1:::> " + mEditTime.getText().toString());
        try {

            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(false);
            dialog.setContentView(R.layout.custom_dialog_time_slot);

            // Button
            Button mButtonOk = dialog.findViewById(R.id.button_dialog_time_slot_ok);

            mArrAppointment = new ArrayList<>();

            mDoctorId = doctor_id;

            //ToDo: Call Appointment
            callAppointmentListAPI(mActivity, dialog, mEditTime, mEditDate);

            // ToDo: Button Ok Click Listener
            mButtonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
