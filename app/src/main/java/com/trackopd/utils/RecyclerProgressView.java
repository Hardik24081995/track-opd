package com.trackopd.utils;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.github.florent37.fiftyshadesof.FiftyShadesOf;

public class RecyclerProgressView {

    private static FiftyShadesOf fiftyShadesOf;

    /***
     * Show Progress view
     * @param ProgressLayout - Progress Layout View
     * @param DisplayView - The view that needed to be display as progress view
     * @param context - Context
     */
    public static void startProgress(LinearLayout ProgressLayout, int DisplayView, Context context) {
        try {
            Resources r = context.getResources();
            int px = (int) TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    10,
                    r.getDisplayMetrics()
            );

            for (int i = 0; i < 3; i++) {
                RelativeLayout rl1 = new RelativeLayout(context);
                if (Build.VERSION.SDK_INT >= 19) {
                    LinearLayout.LayoutParams rlp = new LinearLayout.LayoutParams(
                            new LinearLayout.LayoutParams(
                                    LinearLayout.LayoutParams.MATCH_PARENT,
                                    LinearLayout.LayoutParams.WRAP_CONTENT));
                    rlp.setMargins(0, 0, 0, px);
                    rl1.setLayoutParams(rlp);
                } else {
                    RelativeLayout.LayoutParams rlp = new RelativeLayout.LayoutParams(
                            RelativeLayout.LayoutParams.MATCH_PARENT,
                            RelativeLayout.LayoutParams.WRAP_CONTENT);
                    rlp.setMargins(0, 0, 0, px);
                    rl1.setLayoutParams(rlp);
                }

                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View myView = inflater.inflate(DisplayView, null);
                rl1.addView(myView);
                ProgressLayout.addView(rl1);
            }
            fiftyShadesOf = FiftyShadesOf.with(context)
                    //.on(R.id.layout1,R.id.layout1,R.id.layout2)
                    .on(ProgressLayout)
                    // .on(vg)
                    .start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Stops the progress
     */
    public static void stopProgress() {
        try {
            fiftyShadesOf.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
