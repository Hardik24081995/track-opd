package com.trackopd.utils;

import android.Manifest;

import java.util.ArrayList;

public class AppPermissions {


    public static ArrayList<String> cameraRequiredPermission(){

        ArrayList<String> permissionsList = new ArrayList<>();

        permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissionsList.add(Manifest.permission.CAMERA);

        return permissionsList;
    }

    public static ArrayList<String> locationRequiredPermission(){

        ArrayList<String> permissionsList = new ArrayList<>();

        permissionsList.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        permissionsList.add(Manifest.permission.ACCESS_FINE_LOCATION);

        return permissionsList;
    }


    public static ArrayList<String> ReadWriteExternalStorageRequiredPermission(){

        ArrayList<String> permissionsList = new ArrayList<>();

        permissionsList.add(Manifest.permission.CAMERA);
        permissionsList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        permissionsList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);

        return permissionsList;
    }
}
