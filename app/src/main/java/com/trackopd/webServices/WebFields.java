package com.trackopd.webServices;

public class WebFields {

    public static final String API_SUB_URL = "api/service";
    public static final String IMAGE_BASE_FOLDER = "assets/uploads/";
    public static final String PDF_URL = IMAGE_BASE_FOLDER + "pdf/";
    public static final String DOC_URL = IMAGE_BASE_FOLDER + "report/";
    public static final String IMAGE_URL = IMAGE_BASE_FOLDER + "patient/";
    public static final String IMAGE_THUMBNAIL_URL = IMAGE_BASE_FOLDER + "patient/";
    // Common Params
    public final static String METHOD = "method";
    public final static String BODY = "body";
    public final static String MESSAGE = "message";
    public final static String ERROR = "error";
    public final static String ROW_COUNT = "rowCount";
    public final static String DATA = "data";

    // Local URl http://192.168.1.7/TrackOPD_Hospital_Web/trunk/api/service
//    public static String API_BASE_URL = "http://societyfy.in/Track_OPD/";
    public static String API_BASE_URL = "http://trackopd.com/Track_OPD/";
    public static final String API_ABOUT_US = API_BASE_URL + API_SUB_URL + "/getPage?PageName=AboutUs";
    public static final String API_TERMS_AND_CONDITIONS = API_BASE_URL + API_SUB_URL + "/getPage?PageName=TermsAndCondition";
        public static final String API_PRIVACY_POLICY = API_BASE_URL + API_SUB_URL + "/getPage?PageName=PrivacyPolicy";

    // Pass Code API Params
    public final static class PASS_CODE {
        public final static String MODE = "checkPassCode";

        public final static String REQUEST_PASS_CODE = "PassCode";

        public final static String RESPONSE_HOSPITAL_CODE = "HospitalCode";
    }

    // Configuration API Params
    public final static class CONFIGURATION {
        public final static String MODE = "getConfig";

        public final static String REQUEST_DATABASE_NAME = "DatabaseName";

        public final static String RESPONSE_TIME_SLOT_DURATION = "TimeSlotDuration";
        public final static String RESPONSE_PAGE_SIZE = "PageSize";
        public final static String RESPONSE_APPOINTMENT_CONFIGURATION = "AppointmentConfiguration";
    }

    // Login API Params
    public final static class LOGIN {
        public final static String MODE = "checkLogin";

        public final static String REQUEST_EMAIL_ID = "EmailID";
        public final static String REQUEST_PASSWORD = "Password";
        public final static String REQUEST_NOTIFICATION_TOKEN = "NotificationToken";
        public final static String REQUEST_DEVICE_UID = "DeviceUID";
        public final static String REQUEST_DEVICE_NAME = "DeviceName";
        public final static String REQUEST_OS_VERSION = "OSVersion";
        public final static String REQUEST_DEVICE_TYPE = "DeviceType";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";

        public final static String RESPONSE_USER_ID = "UserID";
        public final static String RESPONSE_ROLE_ID = "RoleID";
        public final static String RESPONSE_EMAIL_ID = "EmailID";
        public final static String RESPONSE_MOBILE_NO = "MobileNo";
        public final static String RESPONSE_PASSWORD = "Password";
        public final static String RESPONSE_USER_TYPE = "UserType";
        public final static String RESPONSE_FIRST_NAME = "FirstName";
        public final static String RESPONSE_LAST_NAME = "LastName";

    }

    // Forgot Password API Params
    public final static class FORGOT_PASSWORD {
        public final static String MODE = "forgotPassword";

        public final static String REQUEST_EMAIL_ID = "EmailID";
    }

    // Change Password API Params
    public final static class CHANGE_PASSWORD {
        public final static String MODE = "forgotPassword";

        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_OLD_PASSWORD = "OldPassword";
        public final static String REQUEST_PASSWORD = "Password";
    }

    // Receptionist - Appointment List API Params
    public final static class RECEPTIONIST_APPOINTMENT_LIST {
        public final static String MODE_RECEPTIONIST = "getAppointmentList";
//        public final static String MODE_COUNCILLOR = "C_getAppointmentList";
//        public final static String MODE_PATIENT = "P_getAppointmentList";

        public final static String REQUEST_CURRENT_PAGE = "CurrentPage";
        public final static String REQUEST_NAME = "Name";
        public final static String REQUEST_PATIENT_CODE = "PatientCode";
        public final static String REQUEST_MOBILE = "MobileNo";
        public final static String REQUEST_MRD_NO = "MRDNo";
        public final static String REQUEST_APPOINTMENT_NO = "AppointmentNo";
        public final static String REQUEST_APPOINTMENT_DATE = "AppointmentDate";
        public final static String REQUEST_DOCTOR_ID = "DoctorID";
        public final static String REQUEST_TYPE = "Type";
        public final static String REQUEST_PATIENT_ID = "PatientID";
        public final static String REQUEST_APPOINTMENT_STATUS = "AppointmentStatus";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Receptionist - Appointment List API Params
    public final static class RECEPTIONIST_TIME_APPOINTMENT_LIST {
        public final static String MODE_RECEPTIONIST = "getAppointmentAssignment";
//        public final static String MODE_COUNCILLOR = "C_getAppointmentList";
//        public final static String MODE_PATIENT = "P_getAppointmentList";

        public final static String REQUEST_CURRENT_PAGE = "CurrentPage";
        public final static String REQUEST_NAME = "Name";
        public final static String REQUEST_PATIENT_CODE = "PatientCode";
        public final static String REQUEST_MOBILE = "MobileNo";
        public final static String REQUEST_MRD_NO = "MRDNo";
        public final static String REQUEST_APPOINTMENT_NO = "AppointmentNo";
        public final static String REQUEST_APPOINTMENT_DATE = "AppointmentDate";
        public final static String REQUEST_DOCTOR_ID = "DoctorID";
        public final static String REQUEST_TYPE = "Type";
        public final static String REQUEST_PATIENT_ID = "PatientID";
        public final static String REQUEST_APPOINTMENT_STATUS = "AppointmentStatus";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Receptionist - Patient List API Params
    public final static class RECEPTIONIST_PATIENT_LIST {
        public final static String MODE_RECEPTIONIST = "getPatientList";
//        public final static String MODE_OPTOMETRIST = "O_getPatientList";
//        public final static String MODE_DOCTOR = "D_getPatientList";
//        public final static String MODE_COUNCILLOR = "C_getPatientList";

        public final static String REQUEST_CURRENT_PAGE = "CurrentPage";
        public final static String REQUEST_NAME = "Name";
        public final static String REQUEST_PATIENT_CODE = "PatientCode";
        public final static String REQUEST_MOBILE = "MobileNo";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";

    }

    // Receptionist - Follow up List API Params
    public final static class RECEPTIONIST_FOLLOW_UP_LIST {
        public final static String MODE_RECEPTIONIST = "getFollowUpList";
//        public final static String MODE_COUNCILLOR = "C_getFollowUpList";

        public final static String REQUEST_CURRENT_PAGE = "CurrentPage";
        public final static String REQUEST_NAME = "Name";
        public final static String REQUEST_PATIENT_CODE = "PatientCode";
        public final static String REQUEST_MOBILE = "MobileNo";
        public final static String REQUEST_FOLLOW_UP_DATE = "FollowupDate";
        public final static String REQUEST_DOCTOR_ID = "DoctorID";
        public final static String REQUEST_COUNCILLOR_ID = "CouncillorID";
        public final static String REQUEST_FOLLOW_DATE = "FollowupDate";
        public final static String REQUEST_TYPE = "Type";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";

    }

    // Receptionist - Payment List Params
    public final static class RECEPTIONIST_PAYMENT_LIST {
        public final static String MODE = "getPaymentList";

        public final static String REQUEST_CURRENT_PAGE = "CurrentPage";
        public final static String REQUEST_NAME = "Name";
        public final static String REQUEST_PATIENT_CODE = "PatientCode";
        public final static String REQUEST_MOBILE_NO = "MobileNo";
        public final static String REQUEST_APPOINTMENT_NO = "AppointmentNo";
        public final static String REQUEST_APPOINTMENT_DATE = "PaymentDate";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Receptionist - Add Patient API Params
    public final static class ADD_PATIENT {
        public final static String MODE = "addPatient";
        public final static String MODE_EDIT = "editPatient";

        public final static String REQUEST_FIRST_NAME = "FirstName";
        public final static String REQUEST_LAST_NAME = "LastName";
        public final static String REQUEST_EMAIL_ID = "EmailID";
        public final static String REQUEST_MOBILE_NO = "MobileNo";
        public final static String REQUEST_COUNTRY_ID = "CountryID";
        public final static String REQUEST_STATE_ID = "StateID";
        public final static String REQUEST_CITY_ID = "CityID";
        public final static String REQUEST_AREA_ID = "AreaID";
        public final static String REQUEST_GENDER = "Gender";
        public final static String REQUEST_AGE = "Age";
        public final static String REQUEST_AREA_NAME = "AreaName";
        public final static String REQUEST_CITY_NAME = "CityName";
        public final static String REQUEST_AREA_ADDRESS = "Address";
        public final static String REQUEST_PINCODE = "PinCode";
        public final static String REQUEST_OCC = "Occ";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
        public final static String REQUEST_PREFIX = "Prefix";
        public final static String REQUEST_MIDDLE_NAME = "MiddleName";
        public final static String REQUEST_PATIENT_USER_ID = "PatientUserID";
    }

    // Receptionist - Add Follow Up API Params
    public final static class ADD_FOLLOW_UP {
        public final static String MODE = "addFollowUp";

        public final static String REQUEST_PATIENT_ID = "PatientID";
        public final static String REQUEST_DOCTOR_ID = "DoctorID";
        public final static String REQUEST_COUNCILLOR_ID = "CouncillorID";
        public final static String REQUEST_FOLLOW_UPDATE = "FollowupDate";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_NOTES = "Notes";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Receptionist - Add Appointment API Params
    public final static class ADD_APPOINTMENT {
        public final static String MODE = "addAppointment";

        public final static String REQUEST_PATIENT_ID = "PatientID";
        public final static String REQUEST_DOCTOR_ID = "DoctorID";
        public final static String REQUEST_APPOINTMENT_DATE = "AppointmentDate";
        public final static String REQUEST_APPOINTMENT_TIME = "AppointmentTime";
        public final static String REQUEST_AMOUNT = "Amount";
        public final static String REQUEST_APPOINTMENT_TYPE = "AppointmentType";
        public final static String REQUEST_PAYMENT_STATUS = "PaymentStatus";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_APPOINTMENT_LOCATION = "AppointmentLocation";
        public final static String REQUEST_LOCATION = "Location";
        public final static String REQUEST_COMMENT = "Comment";
        public final static String REQUEST_FOLLOW_DATE = "FollowupDate";
        public final static String REQUEST_CHECK_UP_TYPE = "CheckUpType";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Receptionist - Add Payment API Params
    public final static class ADD_PAYMENT {
        public final static String MODE = "addPayment";

        public final static String REQUEST_PATIENT_ID = "PatientID";
        public final static String REQUEST_AMOUNT = "Amount";
        public final static String REQUEST_PAYMENT_DATE = "PaymentDate";
        public final static String REQUEST_PAYMENT_STATUS = "PaymentStatus";
        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Receptionist - Patient Details Process List API Params
    public final static class GET_PATIENT_PROCESS_LIST {
        public final static String MODE = "getPatientProcessList";

        public final static String REQUEST_CURRENT_PAGE = "CurrentPage";
        public final static String REQUEST_PATIENT_ID = "PatientID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Receptionist - Appointment Details Process List API Params
    public final static class GET_APPOINTMENT_PROCESS_LIST {
        public final static String MODE = "getAppointmentProcessList";

        public final static String REQUEST_CURRENT_PAGE = "CurrentPage";
        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Receptionist - Check Appointment Validity API Params
    public final static class CHECK_APPOINTMENT_VALIDITY {
        public final static String MODE = "getAppointmentAvailability";

        public final static String REQUEST_DATE = "Date";
        public final static String REQUEST_TIME_SLOT = "TimeSlot";
        public final static String REQUEST_DOCTOR_ID = "DoctorID";

        public final static String REQUEST_DATABASE_NAME = "DatabaseName";

    }

    // Receptionist - Add Check In API Params
    public final static class ADD_CHECK_IN {
        public final static String MODE = "addChechIN";

        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Councillor - Inquiry Form List API Params
    public final static class INQUIRY_FORM_LIST {
        public final static String MODE = "getCouncillorFormList";

        public final static String REQUEST_CURRENT_PAGE = "CurrentPage";
        public final static String REQUEST_MOBILE_NO = "MobileNo";
        public final static String REQUEST_NAME = "Name";
        public final static String REQUEST_COUNCILLOR_ID = "CouncillorID";
        public final static String REQUEST_COUNCILLOR_DATE = "CouncillorDate";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Patient - Appointment List API Params
    public final static class PATIENT_APPOINTMENT_LIST {
        public final static String MODE = "P_getAppointmentList";

        public final static String REQUEST_CURRENT_PAGE = "CurrentPage";
        public final static String REQUEST_APPOINTMENT_NO = "AppointmentNo";
        public final static String REQUEST_APPOINTMENT_DATE = "AppointmentDate";
        public final static String REQUEST_DOCTOR_ID = "DoctorID";
        public final static String REQUEST_PATIENT_ID = "PatientID";
        public final static String REQUEST_TYPE = "Type";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Councillor - Package List API Params
    public final static class GET_PACKAGE_LIST {
        public final static String MODE = "getPackageList";

        public final static String REQUEST_CURRENT_PAGE = "CurrentPage";
        public final static String REQUEST_TITLE = "Title";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Patient - Prescriptions List API Params
    public final static class PATIENT_PRESCRIPTIONS_LIST {
        public final static String MODE = "P_getMedicationList";

        public final static String REQUEST_CURRENT_PAGE = "CurrentPage";
        public final static String REQUEST_PATIENT_ID = "PatientID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";

        public final static String RESPONSE_JSON_ARR_MEDICATION = "medication";
    }

    // Doctor Spinner API Params
    public final static class GET_DOCTOR_LIST {
        public final static String MODE = "getDoctor";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Councillor - Get Package Item List API Params
    public final static class GET_PACKAGE_ITEM_LIST {
        public final static String MODE = "getBillingServices";

        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Councillor Spinner API Params
    public final static class GET_COUNCILLOR_LIST {
        public final static String MODE = "getCouncillor";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Country Spinner API Params
    public final static class GET_COUNTRY_LIST {
        public final static String MODE = "getCountry";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // State Spinner API Params
    public final static class GET_STATES {
        public final static String MODE = "getStates";

        public final static String REQUEST_COUNTRY_ID = "CountryID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";

    }

    // City Spinner API Params
    public final static class GET_CITIES {
        public final static String MODE = "getCities";

        public final static String REQUEST_STATE_ID = "StateID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Area Spinner API Params
    public final static class GET_AREA_LIST {
        public final static String MODE = "getArea";

        public final static String REQUEST_CITY_ID = "CityID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Get Medication API Params
    public final static class GET_MEDICATION {
        public final static String MODE = "getMedication";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Get Dosage API Params
    public final static class GET_DOSAGE {
        public final static String MODE = "getDosage";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Get UOM API Params
    public final static class GET_UOM {
        public final static String MODE = "getUOM";

        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Get Special Instructions API Params
    public final static class GET_SPECIAL_INSTRUCTIONS {
        public final static String MODE = "getSpecialInstruction";

        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // optometrist - Preliminary Examination Details API Params
    public final static class PRELIMINARY_EXAMINATION_DETAILS {
        public final static String MODE = "preliminaryExaminationData";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Optometrist - Preliminary Examination List API Params
    public final static class PRELIMINARY_EXAMINATION_LIST {
        public final static String MODE = "getPreliminaryExamination";

        public final static String REQUEST_CURRENT_PAGE = "CurrentPage";
        public final static String REQUEST_PATIENT_NAME = "PatientName";
        public final static String REQUEST_MOBILE_NO = "MobileNo";
        public final static String REQUEST_TREATMENT_DATE = "TreatmentDate";
        public final static String REQUEST_DOCTOR_ID = "DoctorID";
        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
        public final static String REQUEST_PATIENT_ID = "PatientID";

    }

    // Optometrist - Preliminary Examination View History List API Params
    public final static class VIEW_HISTORY_LIST {
        public final static String MODE = "getPreliminaryHistory";

        public final static String REQUEST_PATIENT_ID = "PatientID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Optometrist - Add Preliminary Examination API Params
    public final static class ADD_PRELIMINARY_EXAMINATION {
        public final static String MODE_ADD = "addPreliminaryExamination";
        public final static String MODE_EDIT = "editPreliminaryExamination";

        public final static String REQUEST_PRELIMINARY_EXAMINATION_ID = "PreliminaryExaminationID";
        public final static String REQUEST_PRELIMINARY_ADVICE_ID = "PreliminaryAdviceID";
        public final static String REQUEST_L_PRELIMINARY_EYE_ID = "LPreliminaryEyeID";
        public final static String REQUEST_R_PRELIMINARY_EYE_ID = "RPreliminaryEyeID";
        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_PATIENT_ID = "PatientID";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_DOCTOR_ID = "DoctorID";
        public final static String REQUEST_OCULAR_HISTORY_ID = "OcularHistoryID";
        public final static String REQUEST_PRIMARY_COMPLAINS = "PrimaryComplains";
        public final static String REQUEST_FAMILY_OCULAR_HISTORY = "FamilyOcularHistory";
        public final static String REQUEST_SYSTEMIC_HISTORY = "SystemicHistory";
        public final static String REQUEST_IPD = "IPD";
        public final static String REQUEST_TREATMENT_DATE = "TreatmentDate";
        public final static String REQUEST_ANATOMICAL_LOCATION_ID = "AnatomicalLocationID";
        public final static String REQUEST_DISEASE_ID = "DiseaseID";
        public final static String REQUEST_EYE_ID = "EyeID";
        public final static String REQUEST_EYE_LENS_ID = "EyeLenseID";
        public final static String REQUEST_REMARK = "Remark";
        public final static String REQUEST_TYPE_ID = "TypeID";
        public final static String REQUEST_PREVIOUS_GLASS = "PreviousGlass";
        public final static String REQUEST_DIAGNOSIS = "DiagnosisModel";
        public final static String REQUEST_DIALATED_AR = "DialatedAR";
        public final static String REQUEST_L_D_SPH = "LDSPH";
        public final static String REQUEST_L_D_CYL = "LDCyl";
        public final static String REQUEST_L_D_AXIS = "LDAxis";
        public final static String REQUEST_L_D_VA = "LDVA";
        public final static String REQUEST_L_N_SPH = "LNSPH";
        public final static String REQUEST_L_N_CYL = "LNCyl";
        public final static String REQUEST_L_N_AXIS = "LNAxis";
        public final static String REQUEST_L_N_VA = "LNVA";
        public final static String REQUEST_L_NCT = "LNCT";
        public final static String REQUEST_L_AT = "LAT";
        public final static String REQUEST_L_PACHYMETRY = "LPachymetry";
        public final static String REQUEST_L_COLOR_VISION = "LColorVision";
        public final static String REQUEST_L_SYRINGING = "LSyringing";
        public final static String REQUEST_L_SCHIRMER = "LSchirmer";
        public final static String REQUEST_L_OCULAR_MOTILITY = "LOcularMotiloty";
        public final static String REQUEST_L_PUPIL = "LPupil";
        public final static String REQUEST_L_ADNEX = "LAdnex";
        public final static String REQUEST_L_ANTERIOR_SEGMENT = "LAnteriorSegment";
        public final static String REQUEST_L_LENS = "LLens";
        public final static String REQUEST_L_FUNDUS = "LFundus";
        public final static String REQUEST_L_GONIOSCOPY = "LGonioscopy";
        public final static String REQUEST_R_D_SPH = "RDSPH";
        public final static String REQUEST_R_D_CYL = "RDCyl";
        public final static String REQUEST_R_D_AXIS = "RDAxis";
        public final static String REQUEST_R_D_VA = "RDVA";
        public final static String REQUEST_R_N_SPH = "RNSPH";
        public final static String REQUEST_R_N_CYL = "RNCyl";
        public final static String REQUEST_R_N_AXIS = "RNAxis";
        public final static String REQUEST_R_N_VA = "RNVA";
        public final static String REQUEST_R_NCT = "RNCT";
        public final static String REQUEST_R_AT = "RAT";
        public final static String REQUEST_R_PACHYMETRY = "RPachymetry";
        public final static String REQUEST_R_COLOR_VISION = "RColorVision";
        public final static String REQUEST_R_SYRINGING = "RSyringing";
        public final static String REQUEST_R_SCHIRMER = "RSchirmer";
        public final static String REQUEST_R_OCULAR_MOTILITY = "ROcularMotiloty";
        public final static String REQUEST_R_PUPIL = "RPupil";
        public final static String REQUEST_R_ADNEX = "RAdnex";
        public final static String REQUEST_R_ANTERIOR_SEGMENT = "RAnteriorSegment";
        public final static String REQUEST_R_LENS = "RLens";
        public final static String REQUEST_R_FUNDUS = "RFundus";
        public final static String REQUEST_R_GONIOSCOPY = "RGonioscopy";

        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Optometrist - Add History & Complaints API Params
    public final static class ADD_HISTORY_COMPLAINTS {
        public final static String MODE_PC = "addPrimaryComplains";
        public final static String MODE_SH = "addSystemicHistory";
        public final static String MODE_OH = "addOcularHistory";
        public final static String MODE_FH = "addFamilyOcularHistory";

        public final static String REQUEST_PRELIMINARY_EXAMINATION_ID = "PreliminaryExaminationID";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
        public final static String REQUEST_PRIMARY_COMPLAINS = "PrimaryComplains";
        public final static String REQUEST_SYSTEMIC_HISTORY = "SystemicHistory";
        public final static String REQUEST_OCULAR_HISTORY = "OcularHistory";
        public final static String REQUEST_FAMILY_OCULAR_HISTORY = "FamilyOcularHistory";
    }

    // Optometrist - Add Pre Examination API Params
    public final static class ADD_PRE_EXAMINATION {
        public final static String MODE = "addPreExamination";

        public final static String REQUEST_PRELIMINARY_EXAMINATION_ID = "PreliminaryExaminationID";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
        public final static String REQUEST_RNCT = "RNCT";
        public final static String REQUEST_LNCT = "LNCT";
        public final static String REQUEST_RAT = "RAT";
        public final static String REQUEST_LAT = "LAT";
        public final static String REQUEST_R_PACHYMETRY = "RPachymetry";
        public final static String REQUEST_L_PACHYMETRY = "LPachymetry";
        public final static String REQUEST_R_SCHIRMER = "RSchirmer";
        public final static String REQUEST_L_SCHIRMER = "LSchirmer";

    }

    // Councillor - Add Inquiry Form API Params
    public final static class ADD_INQUIRY_FORM {
        public final static String MODE = "addCouncillorForm";

        public final static String REQUEST_COUNCILLOR_FOR = "CouncillorFor";
        public final static String REQUEST_RECOMMENDED_BY = "RecommandBy";

        public final static String REQUEST_PATIENT_ID = "PatientID";
        public final static String REQUEST_COUNCILLOR_ID = "CouncillorID";
        public final static String REQUEST_COUNCILLOR_DATE = "CouncillorDate";
        public final static String REQUEST_EYE_ID = "EyeID";
        public final static String REQUEST_SURGERY_TYPE_ID = "SurgeryTypeID";
        public final static String REQUEST_DOCTOR_ID = "DoctorID";
        public final static String REQUEST_DISEASE_EXPLAINED = "DiseaseExplained";
        public final static String REQUEST_SURGERY_EXPLAINED = "SurgeryExplained";
        public final static String REQUEST_OPTION_SUGGESTED_ID = "OpetionSuggestedID";
        public final static String REQUEST_PATIENT_PREFERRED_OPTION_ID = "PatientPreferredOptionID";
        public final static String REQUEST_MEDICLAIM = "Mediclaim";
        public final static String REQUEST_ADMISSION_DATE = "AdmissionDate";
        public final static String REQUEST_ADMISSION_TIME = "AdmissionTime";
        public final static String REQUEST_REMARKS = "Remarks";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
        public final static String REQUEST_PRELIMINARY_EXAMINATION_ID = "PreliminaryExaminationID";
        public final static String REQUEST_SURGERYTYPE2 = "SurgeryType2";
    }

    // Councillor - Add Package API Params
    public final static class ADD_PACKAGE {
        public final static String MODE = "addPackage";

        public final static String REQUEST_ARR_ITEM = "item";

        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_TITLE = "Title";
        public final static String REQUEST_AMOUNT = "Amount";
        public final static String REQUEST_DISCOUNT_AMOUNT = "DiscountAmount";
        public final static String REQUEST_PACKAGE_DATE = "PackageDate";
        public final static String REQUEST_ARR_BILLING_ID = "BillingID";
        public final static String REQUEST_ARR_AMOUNT = "Amount";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // next Status Spinner API Params
    public class GET_STATUS_COMBO_BOX {
        public final static String MODE = "getStatusComboBox";
        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Get User By User Type Spinner API Params
    public class GET_USER_BY_USER_TYPE {
        public final static String MODE = "getUserbyUserType";
        public final static String RESPONSE_USER_TYPE = "UserType";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Do Change Status API Params
    public class CHANGE_STATUS {
        public final static String MODE = "changeStatus";

        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_STATUS_ID = "StatusID";
        public final static String REQUEST_ACTION_USER_ID = "ActionUserID";
        public final static String REQUEST_TIME_DEFERENCE = "TimeDifferance";
        public final static String REQUEST_PATIENT_ID = "PatientID";
        public final static String REQUEST_USER_ID = "UserID";

        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Status Tab API Params
    public class GET_STATUS_TAB {
        public static final String MODE = "getStatusTab";
        public static final String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Status Tab Listing API Params
    public class GET_STATUS_TAB_LIST {
        public static final String MODE = "getStatusTabList";

        public final static String REQUEST_CURRENT_PAGE = "CurrentPage";
        public final static String REQUEST_NAME = "Name";
        public final static String REQUEST_PATIENT_CODE = "PatientCode";
        public final static String RESPONSE_MOBILE_NO = "MobileNo";
        public final static String REQUEST_APPOINTMENT_NO = "AppointmentNo";
        public final static String REQUEST_APPOINTMENT_DATE = "AppointmentDate";
        public final static String REQUEST_STATUS_ID = "StatusID";
        public final static String REQUEST_USER_ID = "UserID";

        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Add Upload Pics API Params
    public class ADD_UPLOAD_PICS {
        public static final String MODE = "addUploadPics";

        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_ACCESS_TYPE = "AccessType";
        public final static String REQUEST_IMAGE_DATA = "Image";
        public static final String METHOD = "method";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Appointment Reason API Params
    public class GET_REASON {
        public static final String MODE = "getReason";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Appointment Cancel API Params
    public class APPOINTMENT_REJECT {
        public static final String MODE = "appointmentReject";

        public final static String REQUEST_REASON_ID = "ReasonID";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_TIME_DEFERENCE = "TimeDifferance";
        public final static String REQUEST_PATIENT_ID = "PatientID";

        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Appointment Reschedule API Params
    public class APPOINTMENT_RESCHEDULE {
        public static final String MODE = "appointmentReschedule";

        public final static String REQUEST_APPOINTMENT_DATE = "AppointmentDate";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_TIME_DIFFERENCE = "TimeDifferance";
        public final static String REQUEST_PATIENT_ID = "PatientID";
        public final static String REQUEST_APPOINTMENT_TIME = "AppointmentTime";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
        public final static String REQUEST_CHECK_UP_TYPE = "CheckUpType";
    }

    // Follow Up Reschedule API Params
    public class FOLLOW_UP_RESCHEDULE {
        public static final String MODE = "followupReschedule";

        public final static String REQUEST_FOLLOW_UP_DATE = "FollowupDate";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_PATIENT_ID = "PatientID";
        public final static String REQUEST_FOLLOW_UP_ID = "FollowupID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";

    }

    public class ADD_MEDICATION {
        public static final String MODE = "addMedication";
        public final static String REQUEST_MEDICATION_ID = "MedicationID";

        public final static String REQUEST_ITEM = "Item";
        public final static String REQUEST_DOSAGE_ID = "DosageID";
        public final static String REQUEST_START_DATE = "StartDate";
        public final static String REQUEST_END_DATE = "EndDate";
        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_DESCRIPTION = "Description";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
        public final static String REQUEST_TOTAL_TIMES = "TotalTimes";
        public final static String REQUEST_TYPE = "Type";
        public final static String REQUEST_TOTAL_DURATION = "TotalDuration";
        public final static String REQUEST_TOTAL_QTY = "TotalQty";
        public final static String REQUEST_UOM_ID = "UOMID";
        public final static String REQUEST_SPECIAL_INSTRUCTION = "SpecialInstruction";

        public final static String REQUEST_PRELIMINARY_EXAMINATION_ID = "PreliminaryExaminationID";

    }

    public class GET_DOCUMENT_LIST {
        public static final String MODE = "getDocument";
        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    public class GET_DOCUMENT_BY_PATIENT_ID {
        public static final String MODE = "getDocumentByPatientID";
        public final static String REQUEST_PATIENT_ID = "PatientID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    public class CHECK_DOCTOR_AVAILABILTY {
        public static final String MODE = "checkDoctorAvailabilty";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    public class GET_PATIENT_HOME {
        public static final String MODE = "getPatientHome";
        public static final String REQUEST_PATIENT_ID = "PatientID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    public class GET_RECEPTIONIST_HOME {
        public static final String MODE = "getReceptionistHome";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    public class GET_DISEASE_LIST {
        public static final String MODE = "getDisease";
        public final static String REQUEST_ANATOMICAL_LOCATION_ID = "AnatomicalLocationID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    public class GET_COUNCILLOR_HOME {
        public static final String MODE = "getCouncillorHome";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    public class GET_DOCTOR_AND_OPTO_HOME {
        public static final String MODE = "getDoctorAndOptoHome";
        public static final String DATE = "Date";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    public class EDIT_PAYMENT {
        public static final String MODE = "editPayment";

        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_PAYMENT_STATUS = "PaymentStatus";
        public final static String REQUEST_APPOINTMENT_DATE = "PaymentDate";
        public final static String REQUEST_AMOUNT = "Amount";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_PATIENT_ID = "PatientID";
        public static final String REQUEST_PAYMENT_HISTORY_ID = "PaymentHistoryID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    public class PAYMENT_SERVICES_PACKAGE {
        public static final String MODE = "getPaymentServicesPackage";
        public static final String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    public class ADD_PAYMENT_LIST {

        public static final String MODE = "addAppointmentService";

        public static final String REQUEST_DATABASE_NAME = "DatabaseName";

        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_PAYMENT_STATUS = "PaymentStatus";
        public final static String REQUEST_PAYMENT_DATE = "PaymentDate";
        public final static String REQUEST_AMOUNT = "Amount";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_PATIENT_ID = "PatientID";
        public static final String REQUEST_PAYMENT_HISTORY_ID = "PaymentHistoryID";
        public static final String REQUEST_PAYMENT_NOTES = "Notes";

        public static final String REQUEST_SUB_TOTAL = "SubTotal";
        public static final String REQUEST_TOTAL = "Total";
        public static final String REQUEST_RECEIVED_AMOUNT = "ReceivedAmount";
        public static final String REQUEST_OUT_STANDING_AMOUNT = "OutStandingAmount";

        public static final String REQUEST_PAYMENT_TYPE = "PaymentType";
        public static final String REQUEST_CHEQUE_NO = "Cheque_no";
        public static final String REQUEST_IFSC_CODE = "IfscCode";
        public static final String REQUEST_UTR_NO = "Utr";
        public static final String REQUEST_BANK_NAME = "BankName";
        public static final String REQUEST_BRANCH = "Branch";
        public static final String REQUEST_ACCOUNT_NO = "AccountNo";

        public static final String REQUEST_ITEM = "Item";

        public static final String REQUEST_ITEM_Type = "Type";
        public static final String REQUEST_ITEM_ID = "ID";
        public static final String REQUEST_ITEM_PARENT_ID = "ParentId";
        public static final String REQUEST_ITEM_TITLE = "Title";
        public static final String REQUEST_ITEM_AMOUNT = "Discount_amount";
        public static final String REQUEST_ITEM_ORIGINAL_AMOUNT = "Amount";

        public static final String REQUEST_CASH_RS = "CashRs";
        public static final String REQUEST_CHEQUE_RS = "ChequeRs";
        public static final String REQUEST_NEFT_RS = "NEFTRs";
        public static final String REQUEST_EWALLET_RS = "EWalletRs";

    }

    public class CHECK_IN_LIST {
        public static final String MODE = "getCheckINList";

        public static final String REQUEST_DATABASE_NAME = "DatabaseName";
        public final static String REQUEST_CURRENT_PAGE = "CurrentPage";
        public final static String REQUEST_NAME = "Name";
        public final static String REQUEST_MOBILE_NO = "MobileNo";
        public final static String REQUEST_CHECK_IN_DATE = "CheckINDate";
        public final static String REQUEST_CHECK_IN_TYPE = "CheckUpType";
    }

    public class ADD_MEDICLAIM {
        public static final String MODE = "addMediclaim";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";

        public static final String REQUEST_COMPANY_NAME = "Company";
        public static final String REQUEST_TPA = "TPA";
        public static final String REQUEST_POLICY_NO = "PolicyNo";
        public static final String REQUEST_EXPIRY_DATE = "ExpiryDate";
        public static final String REQUEST_SETTLEMENT_RECEIVED = "SettlementReceived";
        public static final String REQUEST_SETTLEMENT_AMOUNT = "SettlementAmount";
        public static final String REQUEST_PAYMENT_HISTORY_ID = "PaymentHistoryID";
        public final static String REQUEST_PRELIMINARY_EXAMINATION_ID = "PreliminaryExaminationID";
    }

    public class ADD_SURGERY_NOTE {
        public static final String MODE = "addSurgeryNotes";

        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
        public final static String REQUEST_START_TIME = "StartTime";
        public final static String REQUEST_END_TIME = "EndTime";
        public final static String REQUEST_SURGERY_DATE = "SurgeryDate";
        public final static String REQUEST_SURGERY_TYPE_ID = "SurgeryTypeID";
        public final static String REQUEST_RACD = "RACD";
        public final static String REQUEST_LACD = "LACD";
        public final static String REQUEST_RAL = "RAL";
        public final static String REQUEST_LAL = "LAL";
        public final static String REQUEST_RINCISION_SIZE = "RIncisionSize";
        public final static String REQUEST_LINCISION_SIZE = "LIncisionSize";
        public final static String REQUEST_RINCISION_TYPE = "RIncisionType";
        public final static String REQUEST_LINCISION_TYPE = "LIncisionType";
        public final static String REQUEST_RSURGERY_NOTES = "RSurgeryNotes";
        public final static String REQUEST_LSURGERY_NOTES = "LSurgeryNotes";
        public final static String REQUEST_RVISCOELASTICS = "RViscoelastics";
        public final static String REQUEST_LVISCOELASTICS = "LViscoelastics";
        public final static String REQUEST_RREMARKS = "RRemarks";
        public final static String REQUEST_LREMARKS = "LRemarks";

        public final static String REQUEST_REMARKS = "Remarks";
        public final static String REQUEST_ACD = "ACD";
        public final static String REQUEST_AL = "AL";
        public final static String REQUEST_INCISION_SIZE = "IncisionSize";
        public final static String REQUEST_INCISION_TYPE = "IncisionType";
        public final static String REQUEST_SURGERY_NOTES = "SurgeryNotes";
        public final static String REQUEST_VISCOELASTICS = "Viscoelastics";

        public final static String REQUEST_EYE_ID = "EyeID";
        public final static String REQUEST_PRELIMINARY_EXAMINATION_ID = "PreliminaryExaminationID";
        public final static String REQUEST_SURGERYTYPE2 = "SurgeryType2";

        public final static String REQUEST_START_DATE = "StartDate";
        public final static String REQUEST_END_DATE = "EndDate";
    }

    public class SURGERY_LIST {
        public static final String MODE = "getSurgeryList";
        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_SURGERY_DATE = "SurgeryDate";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
        public final static String REQUEST_CURRENT_PAGE = "CurrentPage";
        public final static String REQUEST_NAME = "Name";
        public final static String REQUEST_PATIENT_CODE = "PatientCode";
        public final static String REQUEST_MOBILE_NO = "MobileNo";
    }

    public class ADD_ANAESTHESIA_SURGERY {
        public static final String MODE = "addAnaesthesia";

        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_PULSE = "Pulse";
        public final static String REQUEST_BP = "BP";
        public final static String REQUEST_SPO2 = "SPO2";
        public final static String REQUEST_RBS = "RBS";
        public final static String REQUEST_EYE_ID = "EyeID";
        public final static String REQUEST_ANAESTHESIATYPE_ID = "AnaesthesiaTypeID";
        public final static String REQUEST_ANAESTHESIA_MEDICINE = "AnaesthesiaMedicine";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
        public static final String REQUEST_PAYMENT_HISTORY_ID = "PaymentHistoryID";
        public static final String REQUEST_DIASTOLICBP = "DiastolicBP";

        public final static String REQUEST_PRELIMINARY_EXAMINATION_ID = "PreliminaryExaminationID";
    }

    public class ADD_IMPLANT_SURGERY {

        public static final String MODE = "addImplant";

        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
        public final static String REQUEST_IMPLANT_BRAND = "ImplantBrand";
        public final static String REQUEST_IMPLANT_NAME = "ImplantName";
        public final static String REQUEST_A_CONSTANT = "A_Constant";
        public final static String REQUEST_IMPLANT_POWER = "ImplantPower";
        public final static String REQUEST_IMPLANT_PLACEMENT = "ImplantPlacement";
        public final static String REQUEST_IMPLANT_EXPIRY = "ImplantExpiry";
        public final static String REQUEST_IMPLANT_SL_NO = "ImplantSlNo";
        public final static String REQUEST_NOTES = "Notes";

        public final static String REQUEST_PRELIMINARY_EXAMINATION_ID = "PreliminaryExaminationID";
    }

    public class ADD_PRELIMINARY_VISION {
        public static final String MODE = "addPreliminaryExamination";

        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_PATIENT_ID = "PatientID";
        public final static String REQUEST_DOCTOR_ID = "DoctorID";
        public final static String REQUEST_TREATMENT_DATE = "TreatmentDate";

        public final static String REQUEST_DATABASE_NAME = "DatabaseName";

        public final static String REQUEST_UCAVDREOD = "UCAVDREOD";
        public final static String REQUEST_UCAVNREOD = "UCAVNREOD";
        public final static String REQUEST_UCAVDLEOS = "UCAVDLEOS";
        public final static String REQUEST_UCAVNLEOS = "UCAVNLEOS";
        public final static String REQUEST_UCAV_REMARKS = "UCAVRemarks";
        public final static String REQUEST_BCVAUNDILATEDDREOD = "BCVAUndilatedDREOD";
        public final static String REQUEST_BCVAUNDILATEDNREOD = "BCVAUndilatedNREOD";
        public final static String REQUEST_BCVAUNDILATEDDLEOS = "BCVAUndilatedDLEOS";
        public final static String REQUEST_BCVAUNDILATEDNLEOS = "BCVAUndilatedNLEOS";


        public final static String REQUEST_BCVADILATEDDREOD = "BCVADilatedDREOD";
        public final static String REQUEST_BCVADILATEDNREOD = "BCVADilatedNREOD";
        public final static String REQUEST_BCVADILATEDDLEOS = "BCVADilatedDLEOS";
        public final static String REQUEST_BCVADILATEDNLEOS = "BCVADilatedNLEOS";
        public final static String REQUEST_BCVADILATEDREMARKS = "BCVADilatedRemarks";
        public final static String REQUEST_REFUNDILATEDRDSPH = "RefUndilatedRDSph";
        public final static String REQUEST_REFUNDILATEDLDSPH = "RefUndilatedLDSph";
        public final static String REQUEST_REFUNDILATEDRNSPH = "RefUndilatedRNSph";
        public final static String REQUEST_REFUNDILATEDLNSPH = "RefUndilatedLNSph";
        public final static String REQUEST_REFUNDILATEDRDCYL = "RefUndilatedRDCyl";
        public final static String REQUEST_REFUNDILATEDLDCYL = "RefUndilatedLDCyl";
        public final static String REQUEST_REFUNDILATEDRNCYL = "RefUndilatedRNCyl";
        public final static String REQUEST_REFUNDILATEDLNCYL = "RefUndilatedLNCyl";
        public final static String REQUEST_REFUNDILATEDRDAXIS = "RefUndilatedRDAxis";
        public final static String REQUEST_REFUNDILATEDLDAXIS = "RefUndilatedLDAxis";
        public final static String REQUEST_REFUNDILATEDRNAXIS = "RefUndilatedRNAxis";
        public final static String REQUEST_REFUNDILATEDLNAXIS = "RefUndilatedLNAxis";
        public final static String REQUEST_REFUNDILATEDRDVA = "RefUndilatedRDVA";
        public final static String REQUEST_REFUNDILATEDLDVA = "RefUndilatedLDVA";
        public final static String REQUEST_REFUNDILATEDRNVA = "RefUndilatedRNVA";
        public final static String REQUEST_REFUNDILATEDLNVA = "RefUndilatedLNVA";
        public final static String REQUEST_REFDILATEDRDSPH = "RefDilatedRDSph";
        public final static String REQUEST_REFDILATEDLDSPH = "RefDilatedLDSph";
        public final static String REQUEST_REFDILATEDRNSPH = "RefDilatedRNSph";
        public final static String REQUEST_REFDILATEDLNSPH = "RefDilatedLNSph";
        public final static String REQUEST_REFDILATEDRDCYL = "RefDilatedRDCyl";
        public final static String REQUEST_REFDILATEDLDCYL = "RefDilatedLDCyl";
        public final static String REQUEST_REFDILATEDRNCYL = "RefDilatedRNCyl";
        public final static String REQUEST_REFDILATEDLNCYL = "RefDilatedLNCyl";
        public final static String REQUEST_REFDILATEDRDAXIS = "RefDilatedRDAxis";
        public final static String REQUEST_REFDILATEDLDAXIS = "RefDilatedLDAxis";
        public final static String REQUEST_REFDILATEDRNAXIS = "RefDilatedRNAxis";
        public final static String REQUEST_REFDILATEDLNAXIS = "RefDilatedLNAxis";
        public final static String REQUEST_REFDILATEDRDVA = "RefDilatedRDVA";
        public final static String REQUEST_REFDILATEDLDVA = "RefDilatedLDVA";
        public final static String REQUEST_REFDILATEDRNVA = "RefDilatedRNVA";
        public final static String REQUEST_REFDILATEDLNVA = "RefDilatedLNVA";
        public final static String REQUEST_REFFINALRDSPH = "RefFinalRDSph";
        public final static String REQUEST_REFFINALLDSPH = "RefFinalLDSph";
        public final static String REQUEST_REFFINALRNSPH = "RefFinalRNSph";
        public final static String REQUEST_REFFINALLNSPH = "RefFinalLNSph";
        public final static String REQUEST_REFFINALRDCYL = "RefFinalRDCyl";
        public final static String REQUEST_REFFINALLDCYL = "RefFinalLDCyl";
        public final static String REQUEST_REFFINALRNCYL = "RefFinalRNCyl";
        public final static String REQUEST_REFFINALLNCYL = "RefFinalLNCyl";
        public final static String REQUEST_REFFINALRDAXIS = "RefFinalRDAxis";
        public final static String REQUEST_REFFINALLDAXIS = "RefFinalLDAxis";
        public final static String REQUEST_REFFINALRNAXIS = "RefFinalRNAxis";
        public final static String REQUEST_REFFINALLNAXIS = "RefFinalLNAxis";
        public final static String REQUEST_REFFINALRDVA = "RefFinalRDVA";
        public final static String REQUEST_REFFINALLDVA = "RefFinalLDVA";
        public final static String REQUEST_REFFINALRNVA = "RefFinalRNVA";
        public final static String REQUEST_REFFINALLNVA = "RefFinalLNVA";
        public final static String REQUEST_REFFINALIPD = "RefFinalIPD";
        public final static String REQUEST_K1RPOWER = "K1RPower";
        public final static String REQUEST_K1RAXIS = "K1RAxis";
        public final static String REQUEST_K1LPOWER = "K1LPower";
        public final static String REQUEST_K1LAXIS = "K1LAxis";
        public final static String REQUEST_K2RPOWER = "K2RPower";
        public final static String REQUEST_K2RAXIS = "K2RAxis";
        public final static String REQUEST_K2LPOWER = "K2LPower";
        public final static String REQUEST_K2LAXIS = "K2LAxis";
        public static final String REQUEST_PRELIMINARYEXAMINATION_ID = "PreliminaryExaminationID";
    }

    public class GET_ANATOMICAL_LOCATION {
        public static final String MODE = "getAnatomicalLocation";
        public final static String REQUEST_CATEGORY_ID = "CategoryID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    public class ADD_DIAGNOSIS {
        public static final String MODE = "addDiagnosis";
        public final static String REQUEST_PRELIMINARY_EXAMINATION_ID = "PreliminaryExaminationID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
        public final static String RESPONSE_USER_ID = "UserID";
        public final static String REQUEST_ITEM = "Item";
        public final static String REQUEST_DATA = "body";
    }

    public class ADD_PRELIMINARY_BASIC_DETAILS {
        public static final String MODE = "addPreliminaryBasicDetails";

        public final static String REQUEST_TREATMENT_DATE = "TreatmentDate";
        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_PATIENT_ID = "PatientID";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_DOCTOR_ID = "DoctorID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
    }

    // Add Upload Pics API Params
    public class UPLOAD_DOODLE_PIC {
        public static final String MODE = "addDoodlingImage";

        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
        public final static String REQUEST_TYPE = "Type";
        public static final String METHOD = "method";
        public static final String REQUEST_IMAGE = "Image";
        public final static String REQUEST_USER_ID = "UserID";
    }

    // Councillor - Get Doodling Images API Params
    public class GET_DOODLE_IMAGES {
        public static final String MODE = "getDoodlingImage";

        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";

        public final static String RESPONSE_RIGHT_ADNEX = "RightAdnex";
    }

    // Councillor - Delete Doodle Image API Params
    public class DELETE_DOODLE_IMAGE {
        public static final String MODE = "deleteDoodlingImage";

        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_IMAGE_NAME = "ImageName";
        public final static String REQUEST_TYPE = "Type";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
        public final static String REQUEST_USER_ID = "UserID";
    }

    // Councillor - Delete Doodle Image API Params
    public class CLOSE_FILE {
        public static final String MODE = "closeFile";

        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";

    }

    public class ADD_INVESTIGATION {
        public static final String LABORATORY_TEST = "LaboratoryTestID";
        public static final String OCULAR_INVESTIGATION_ID = "OcularInvestigationID";
        public final static String REQUEST_USER_ID = "UserID";
        public static final String REQUEST_EYE_ID = "EyeID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
        public final static String REQUEST_PRELIMINARY_EXAMINATION_ID = "PreliminaryExaminationID";
        public static final String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public static final String MODE = "addInvestigation";
        public final static String REQUEST_IMAGE_DATA_1 = "Image1";
        public final static String REQUEST_IMAGE_DATA_2 = "Image2";
        public final static String REQUEST_IMAGE_DATA_3 = "Image3";
        public final static String REQUEST_IMAGE_DATA_4 = "Image4";
        public static final String REQUEST_PART= "image";

    }

    public class HISTORY_PRELIMINARY_EXAMINATION {
        public static final String MODE = "getHistoryPreliminaryExamination";

        public final static String REQUEST_TREATMENT_DATE = "TreatmentDate";
        public final static String REQUEST_CURRENT_PAGE = "CurrentPage";
        public final static String REQUEST_PATIENT_ID = "PatientID";
        public final static String REQUEST_MOBILE_NO = "MobileNo";
        public final static String REQUEST_USER_ID = "UserID";
        public final static String REQUEST_DOCTOR_ID = "DoctorID";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
        public final static String REQUEST_TYPE = "Type";
    }

    public class ADD_SURGERY_SUGGESTED {
        public final static String REQUEST_PRELIMINARY_EXAMINATION_ID = "PreliminaryExaminationID";

        public static final String MODE = "addSurgerySuggested";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";
        public static final String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public static final String REQUEST_CATEGORY_ID = "CategoryID";
        public static final String REQUEST_SURGERY_ID = "SurgeryTypeID";
        public static final String REQUEST_USER_ID = "UserID";
        public static final String REQUEST_REMARKS = "Remarks";
        public static final String REQUEST_DURATION = "Duration";
        public static final String REQUEST_EYE_ID = "EyeID";
        public static final String REQUEST_SURGERYTYPE2 = "SurgeryType2";

    }

    public class PRINT_RECEIPT {
        public final static String REQUEST_PRELIMINARY_EXAMINATION_ID = "PreliminaryExaminationID";
        public final static String REQUEST_APPOINTMENT_ID = "AppointmentID";
        public final static String REQUEST_HISTORY_AND_COPLAINTS = "HistoryAndCoplaints";
        public final static String REQUEST_VISION_UCVA = "VisionUCVA";
        public final static String REQUEST_VISION_FINAL = "VisionFinal";
        public final static String REQUEST_VISION_BCVA_DILATED = "VisionBCVADilated";
        public final static String REQUEST_VISION_BCVA_UNDILATED = "VisionBCVAUndilated";
        public final static String REQUEST_PRIMARY_EXAMINATION = "PrimaryExamination";
        public final static String REQUEST_DIAGNOSIS = "Diagnosis";
        public final static String REQUEST_INVESTIGATION_SUGGESTED = "InvestigationSuggested";
        public final static String REQUEST_TREATMENT_SUGGESTED = "TreatmentSuggested";
        public final static String REQUEST_COUNSELING_DETAILS = "CounselingDetails";
        public final static String REQUEST_SURGERY_DETAILS = "SurgeryDetails";
        public final static String REQUEST_PRESCRIPTION = "Prescription";
        public final static String REQUEST_PAYMENT = "Payment";
        public final static String REQUEST_LANGUAGE = "Language";
        public final static String REQUEST_DISCHARGE = "Discharge";
        public final static String REQUEST_DATABASE_NAME = "DatabaseName";

        public static final String MODE = "PrintReceipt";
    }

    // Councillor - Delete Doodle Image API Params
    public class ADD_TEMPLETE {
        public static final String MODE = "getTemplate";

        public final static String REQUEST_DATABASE_NAME = "DatabaseName";

    }
}