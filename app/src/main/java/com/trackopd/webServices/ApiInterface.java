package com.trackopd.webServices;

import com.trackopd.model.AddAppointmentModel;
import com.trackopd.model.AddBillingPaymentModel;
import com.trackopd.model.AddChangeStatusModel;
import com.trackopd.model.AddDoodleImageModel;
import com.trackopd.model.AddFollowUpModel;
import com.trackopd.model.AddHistoryComplainsModel;
import com.trackopd.model.AddMedicationModel;
import com.trackopd.model.AddMediclaimModel;
import com.trackopd.model.AddPackageItemModel;
import com.trackopd.model.AddPatientModel;
import com.trackopd.model.AddPaymentModel;
import com.trackopd.model.AddPreliminaryExaminationModel;
import com.trackopd.model.AddUploadPicModel;
import com.trackopd.model.AnatomicalLocationModel;
import com.trackopd.model.AppointmentReasonsModel;
import com.trackopd.model.AppointmentValidityModel;
import com.trackopd.model.AreaModel;
import com.trackopd.model.ChangePasswordModel;
import com.trackopd.model.ChangeStatusModel;
import com.trackopd.model.CheckInListModel;
import com.trackopd.model.CityModel;
import com.trackopd.model.CloseFileModel;
import com.trackopd.model.ConfigurationModel;
import com.trackopd.model.CouncillorInquiryFormsModel;
import com.trackopd.model.CouncillorModel;
import com.trackopd.model.CouncillorPackageModel;
import com.trackopd.model.CountryModel;
import com.trackopd.model.DiseaseModel;
import com.trackopd.model.DoctorHomeListModel;
import com.trackopd.model.DoctorModel;
import com.trackopd.model.DoodleImageModel;
import com.trackopd.model.DosageModel;
import com.trackopd.model.ForgotPasswordModel;
import com.trackopd.model.GetPatientHomeModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.LoginModel;
import com.trackopd.model.MedicationModel;
import com.trackopd.model.PassCodeModel;
import com.trackopd.model.PatientAppointmentModel;
import com.trackopd.model.PatientPrescriptionModel;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.model.PreliminaryExaminationModel;
import com.trackopd.model.PreliminaryExaminationViewHistoryModel;
import com.trackopd.model.PrintReceiptModel;
import com.trackopd.model.ReceptionistAppointmentDetailProcessModel;
import com.trackopd.model.ReceptionistAppointmentModel;
import com.trackopd.model.ReceptionistFollowUpModel;
import com.trackopd.model.ReceptionistPatientDetailDocumentModel;
import com.trackopd.model.ReceptionistPatientDetailProcessModel;
import com.trackopd.model.ReceptionistPatientModel;
import com.trackopd.model.ReceptionistPaymentModel;
import com.trackopd.model.ReceptionistTimeSlotAppointmentModel;
import com.trackopd.model.RescheduleModel;
import com.trackopd.model.ServicesPackagesModel;
import com.trackopd.model.SpecialInstructionsModel;
import com.trackopd.model.StateModel;
import com.trackopd.model.StatusTabListModel;
import com.trackopd.model.StatusTabModel;
import com.trackopd.model.SurgeryListModel;
import com.trackopd.model.TempleteModel;
import com.trackopd.model.UOMModel;
import com.trackopd.model.UploadDocumentModel;
import com.trackopd.model.UserByUserTypeModel;
import com.trackopd.model.checkDoctorAvailabiltyModel;
import com.trackopd.model.getCouncillorHomeModel;
import com.trackopd.model.getReceptionistModel;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Url;

public interface ApiInterface {

    // ToDo: Common APIs for All Modules
    // Pass Code API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<PassCodeModel> passCodeAPI(@Body RequestBody body);

    // Configuration API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ConfigurationModel> configurationAPI(@Body RequestBody body);

    // Login API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<LoginModel> loginAPI(@Body RequestBody body);

    // Forgot Password API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ForgotPasswordModel> forgotPasswordAPI(@Body RequestBody body);

    // Change Password API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ChangePasswordModel> changePasswordAPI(@Body RequestBody body);

    // Doctor Spinner API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<DoctorModel> getDoctorList(@Body RequestBody body);

    // Councillor Spinner API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<CouncillorModel> getCouncillorList(@Body RequestBody body);

    // Country Spinner API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<CountryModel> getCountryList(@Body RequestBody body);

    // State Spinner API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<StateModel> getStateList(@Body RequestBody body);

    // City Spinner API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<CityModel> getCityList(@Body RequestBody body);

    // Area Spinner API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AreaModel> getAreaList(@Body RequestBody body);

    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AppointmentReasonsModel> getReason(@Body RequestBody requestBody);

    //--------------------------------------------------------------------//
    //--------------------------------------------------------------------//
    //--------------------------------------------------------------------//
    //--------------------------------------------------------------------//
    // Get Medication List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<MedicationModel> getMedicationList(@Body RequestBody body);

    // Get Dosage List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<DosageModel> getDosage(@Body RequestBody body);

    // Get UOM List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<UOMModel> getUOMList(@Body RequestBody body);

    // Get Special Instructions API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<SpecialInstructionsModel> getSpecialInstructionsList(@Body RequestBody body);

    // get Status Tabs List
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<StatusTabListModel> getStatusTabList(@Body RequestBody requestBody);

    // Get Status ComboBox API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ChangeStatusModel> getStatusComboBox(@Body RequestBody requestBody);

    // Get User To User Type ComboBox API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<UserByUserTypeModel> getUserByUserType(@Body RequestBody requestBody);

    // Add Change Status Patient API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddChangeStatusModel> doChangeStatus(@Body RequestBody requestBody);

    // Get Change Status Tabs API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<StatusTabModel> getStatusTab(@Body RequestBody requestBody);

    @Multipart
    @POST(WebFields.API_SUB_URL)
//    Call<UploadDocumentModel> uploadDocument(@Body RequestBody requestBody);
    Call<UploadDocumentModel> uploadDocument(@Part MultipartBody.Part file, @PartMap() Map<String, String> partMap);
    //------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------//
    //------------------------------------------------------------------------------------------------//


    // ToDo: Receptionist APIs
    // Receptionist - Appointment List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ReceptionistAppointmentModel> getReceptionistAppointmentList(@Body RequestBody body);

    // Receptionist - Patient List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ReceptionistPatientModel> getReceptionistPatientList(@Body RequestBody body);

    // Receptionist - Follow Up List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ReceptionistFollowUpModel> getReceptionistFollowUpList(@Body RequestBody body);

    // Receptionist - Payment List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ReceptionistPaymentModel> getReceptionistPaymentList(@Body RequestBody body);

    // Receptionist - Add Patient API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddPatientModel> addReceptionistPatient(@Body RequestBody body);

    // Receptionist - Add Follow Up API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddFollowUpModel> addReceptionistFollowUp(@Body RequestBody body);

    // Receptionist - Add Appointment API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddAppointmentModel> addReceptionistAppointment(@Body RequestBody body);

    // Receptionist - Add Payment API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddPaymentModel> addPayment(@Body RequestBody body);

    // Receptionist - Patient Details Process List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ReceptionistPatientDetailProcessModel> getPatientProcessList(@Body RequestBody requestBody);

    // Receptionist - Appointment Details Process List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ReceptionistAppointmentDetailProcessModel> getAppointmentProcessList(@Body RequestBody requestBody);

    // Receptionist - Appointment Cancel API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddAppointmentModel> setAppointmentReject(@Body RequestBody requestBody);

    // Receptionist - Appointment Reschedule API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<RescheduleModel> setAppointmentReschedule(@Body RequestBody body);

    // Receptionist - Follow Up Reschedule API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<RescheduleModel> setFollowUpReschedule(@Body RequestBody body);

    // Receptionist - Get Document API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ReceptionistPatientDetailDocumentModel> getReceptionistDocumentList(@Body RequestBody requestBody);

    // Receptionist - Check Appointment Validity API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AppointmentValidityModel> checkAppointmentValidity(@Body RequestBody body);

    // Receptionist - Check In API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddPatientModel> addCheckIn(@Body RequestBody body);


    // ToDo: Patient APIs
    // Patient - Appointment List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<PatientAppointmentModel> getPatientAppointmentList(@Body RequestBody body);

    // Patient - Prescriptions List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<PatientPrescriptionModel> getPatientPrescriptionsList(@Body RequestBody body);

    // Patient - Prescriptions List API Interface
    @GET
    Call<ResponseBody> downloadFile(@Url String fileUrl);


    // ToDo: Councillor APIs
    // Councillor - Package List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<CouncillorPackageModel> getCouncillorPackageList(@Body RequestBody body);

    // Councillor  Home API
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<getCouncillorHomeModel> getCouncillorHomeListApi(@Body RequestBody requestBody);

    // Councillor  Home API
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<DoctorHomeListModel> getDoctorHomeListApi(@Body RequestBody requestBody);

    // Councillor - Add Package API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddPatientModel> addPackage(@Body RequestBody body);

    // Councillor - Add Inquiry Form API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddPatientModel> addInquiryForm(@Body RequestBody body);

    // Councillor - Inquiry Forms List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<CouncillorInquiryFormsModel> getCouncillorInquiryFormsList(@Body RequestBody body);

    // Councillor - Package Item List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddPackageItemModel> getPackageItemList(@Body RequestBody body);


    // TODO: Doctor API
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddMedicationModel> addMedication(@Body RequestBody requestBody);

    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<checkDoctorAvailabiltyModel> getcheckDoctorAvailabiltyListAPI(@Body RequestBody requestBody);


    // TODO: Optometrist API
    // Optometrist - Preliminary Examination Details API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<PreliminaryExaminationDetailsModel> getPreliminaryExaminationDetails(@Body RequestBody body);

    // Optometrist - Disease API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<DiseaseModel> getDiseaseList(@Body RequestBody body);

    // Optometrist - Preliminary Examination List API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<PreliminaryExaminationModel> getPreliminaryExaminationList(@Body RequestBody body);

    // Optometrist - Add Preliminary Examination API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddPreliminaryExaminationModel> addPreliminaryExamination(@Body RequestBody body);

    // Optometrist - Preliminary Examination View History API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<PreliminaryExaminationViewHistoryModel> getPreliminaryExaminationViewHistoryList(@Body RequestBody body);

    // Optometrist - Pre Examination API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddPreliminaryExaminationModel> addPreExamination(@Body RequestBody body);


    // Add Menual Expense Service
    @Multipart
    @POST(WebFields.API_SUB_URL)
    Call<AddUploadPicModel> addUploadPics(@Part(WebFields.ADD_UPLOAD_PICS.METHOD) RequestBody method,
                                          @Part(WebFields.ADD_UPLOAD_PICS.REQUEST_ACCESS_TYPE) RequestBody accessType,
                                          @Part(WebFields.ADD_UPLOAD_PICS.REQUEST_USER_ID) RequestBody userId,
                                          @Part(WebFields.ADD_UPLOAD_PICS.REQUEST_DATABASE_NAME) RequestBody mDatabaseName,
                                          @Part MultipartBody.Part file);

    // Patient Home API
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<GetPatientHomeModel> getPatientHomeList(@Body RequestBody body);

    // Receptionist Home API
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<getReceptionistModel> getReceptionistListApi(@Body RequestBody requestBody);

    // Edit Payment
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddPaymentModel> EditPayment(@Body RequestBody requestBody);

    // Edit Payment
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ServicesPackagesModel> getPaymentServicesPackageList(@Body RequestBody body);

    // Billing  Payment
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddBillingPaymentModel> addBillingsPayment(@Body RequestBody requestBody);

    // Check In List
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<CheckInListModel> getCheckInList(@Body RequestBody requestBody);

    // Receptionist Date wise Appointment List
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<ReceptionistTimeSlotAppointmentModel> getReceptionistTimeAppointmentList(@Body RequestBody requestBody);

    // Surgery Add Mediclaim Detail
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddMediclaimModel> AddMediclaim(@Body RequestBody requestBody);

    // Surgery Add Mediclaim Detail
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddMediclaimModel> AddSurgeryNote(@Body RequestBody requestBody);

    // Surgery List API
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<SurgeryListModel> getSurgeryList(@Body RequestBody requestBody);

    // Add Anaesthesia Surgery API
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddMediclaimModel> AddAnaesthesiaSurgery(@Body RequestBody requestBody);

    // Add Implant Surgery API
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddMediclaimModel> AddImplantSurgery(@Body RequestBody requestBody);

    // Add Implant Surgery API
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddPreliminaryExaminationModel> AddPreliminaeyExamination(@Body RequestBody requestBody);

    // Add Implant Surgery API
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AnatomicalLocationModel> getAnatomicalLocation(@Body RequestBody body);

    // Add Preliminary Basic Model  Surgery API
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddAppointmentModel> setAddPreliminaryBasicDetails(@Body RequestBody requestBody);

    // Get Doodle Images API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<DoodleImageModel> getDoodleImages(@Body RequestBody body);

    // Delete Doodle Image API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddDoodleImageModel> deleteDoodleImage(@Body RequestBody body);


    // Delete Doodle Image API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<TempleteModel> getTempleteAPI(@Body RequestBody body);

    // Upload Doodle Image API Interface
    @Multipart
    @POST(WebFields.API_SUB_URL)
    Call<AddDoodleImageModel> uploadDoodleImage(@Part(WebFields.UPLOAD_DOODLE_PIC.METHOD) RequestBody method,
                                                @Part(WebFields.UPLOAD_DOODLE_PIC.REQUEST_APPOINTMENT_ID) RequestBody mAppointmentId,
                                                @Part(WebFields.UPLOAD_DOODLE_PIC.REQUEST_DATABASE_NAME) RequestBody mDatabaseName,
                                                @Part(WebFields.UPLOAD_DOODLE_PIC.REQUEST_TYPE) RequestBody mType,
                                                @Part(WebFields.UPLOAD_DOODLE_PIC.REQUEST_USER_ID) RequestBody mUserId,
                                                @Part MultipartBody.Part file);

    // Print Receipt  API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<PrintReceiptModel> PrintReceipt(@Body RequestBody body);


    // Upload Investigation Image API Interface
    @Multipart
    @POST(WebFields.API_SUB_URL)
    Call<AddDoodleImageModel> AddInvestigationWithImage(@Part(WebFields.METHOD) RequestBody method,
                                                        @Part(WebFields.ADD_INVESTIGATION.REQUEST_USER_ID) RequestBody mUserID,
                                                        @Part(WebFields.ADD_INVESTIGATION.REQUEST_PRELIMINARY_EXAMINATION_ID) RequestBody mPreliminaryExaminationID,
                                                        @Part(WebFields.ADD_INVESTIGATION.OCULAR_INVESTIGATION_ID) RequestBody mInvestigationID,
                                                        @Part(WebFields.ADD_INVESTIGATION.LABORATORY_TEST) RequestBody laborateryTest,
                                                        @Part(WebFields.ADD_INVESTIGATION.REQUEST_EYE_ID) RequestBody mEyeID,
                                                        @Part(WebFields.ADD_INVESTIGATION.REQUEST_DATABASE_NAME) RequestBody mDatabase,
                                                        @Part(WebFields.ADD_INVESTIGATION.REQUEST_APPOINTMENT_ID) RequestBody mAppointmentID,
                                                        @Part MultipartBody.Part file);

    // Print Receipt  API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<HistoryPreliminaryExaminationModel> getPreliminaryExaminationHistory(@Body RequestBody body);

    // Print Receipt  API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<AddHistoryComplainsModel> AddHistoryComplains(@Body RequestBody body);

    // Close Files  API Interface
    @Headers({"Content-Type: application/json"})
    @POST(WebFields.API_SUB_URL)
    Call<CloseFileModel> closeFileAPI(@Body RequestBody body);
}