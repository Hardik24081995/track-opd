package com.trackopd.webServices;

import android.content.Context;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.SessionManager;
import org.json.JSONException;
import org.json.JSONObject;

public class GetJsonData {

    /**
     *
     * @param context - Context
     * @param value   - Value get Value
     * @return        - Return String
     */
    public static String getHospitalData(Context context,String value){
        String mField = AppConstants.STR_EMPTY_STRING;
        try {
            SessionManager mSessionManager = new SessionManager(context);
            String mData = mSessionManager.getPreferences(mSessionManager.KEY_HOSPITAL_DATA, "");
            JSONObject jObject = new JSONObject(mData);
            mField = jObject.getString(value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mField;
    }

    /**
     * This method should get the permissions from the json response.
     *
     * @param context - Context
     * @param value   - Value
     * @return - Return mField
     */
    public static String getLoginData(Context context, String value) {
        String mField = AppConstants.STR_EMPTY_STRING;
        try {
            SessionManager mSessionManager = new SessionManager(context);
            String mData = mSessionManager.getPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");
            JSONObject jObject = new JSONObject(mData);
            mField = jObject.getString(value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mField;
    }


    /**
     * This method should get the configurations from the json response.
     *
     * @param context - Context
     * @param value   - Value
     * @return - Return mField
     */
    public static String getConfigurationData(Context context, String value) {
        String mField = AppConstants.STR_EMPTY_STRING;
        try {
            SessionManager mSessionManager = new SessionManager(context);
            String mData = mSessionManager.getPreferences(mSessionManager.KEY_CONFIGURATION_DATA, "");
            JSONObject jObject = new JSONObject(mData);
            mField = jObject.getString(value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mField;
    }

    /**
     * This method should get the User Type from Login
     * @param context - Context
     * @return - Return User Type
     */
    public static String getUserType(Context context) {
        String mUserType = AppConstants.STR_EMPTY_STRING;
        try {
            mUserType = GetJsonData.getLoginData(context, WebFields.LOGIN.RESPONSE_USER_TYPE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mUserType;
    }
}
