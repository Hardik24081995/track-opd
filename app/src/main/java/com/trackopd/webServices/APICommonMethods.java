package com.trackopd.webServices;

import android.content.Context;

import com.trackopd.R;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;

public class APICommonMethods {

    /**
     * Sets up the media type for api call
     *
     * @return - Return Media Type
     */
    public static MediaType getMediaType() {
        return MediaType.parse("application/octet-stream");
    }

    /**
     * Sets the child json for Pass Code API
     *
     * @param mPassCode - Pass Code
     * @return - Return Result
     */
    public static String setPassCodeJson(String mPassCode) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.PASS_CODE.REQUEST_PASS_CODE, mPassCode);

            result = setParentJsonData(WebFields.PASS_CODE.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Configuration API
     *
     * @param hospital_databse - Hospital Database
     */
    public static String setConfigurationJson(String hospital_databse) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.CONFIGURATION.REQUEST_DATABASE_NAME, hospital_databse);

            result = setParentJsonData(WebFields.CONFIGURATION.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Login API
     *
     * @param mEmailId           - Email Id
     * @param mPassword          - Password
     * @param mNotificationToken - Notification Token
     * @param mDeviceUID         - Device UID
     * @param mDeviceName        - Device Name
     * @param mOSVersion         - OS Version
     * @param mDeviceType        - Device Type
     * @param hospital_database  - Hospital Name
     * @return - Return Result
     */
    public static String setLoginJson(String mEmailId, String mPassword, String mNotificationToken, String mDeviceUID, String mDeviceName, String mOSVersion, String mDeviceType, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.LOGIN.REQUEST_EMAIL_ID, mEmailId);
            jsonBody.put(WebFields.LOGIN.REQUEST_PASSWORD, mPassword);
            jsonBody.put(WebFields.LOGIN.REQUEST_NOTIFICATION_TOKEN, mNotificationToken);
            jsonBody.put(WebFields.LOGIN.REQUEST_DEVICE_UID, mDeviceUID);
            jsonBody.put(WebFields.LOGIN.REQUEST_DEVICE_NAME, mDeviceName);
            jsonBody.put(WebFields.LOGIN.REQUEST_OS_VERSION, mOSVersion);
            jsonBody.put(WebFields.LOGIN.REQUEST_DEVICE_TYPE, mDeviceType);
            jsonBody.put(WebFields.LOGIN.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.LOGIN.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Forgot Password API
     *
     * @param mEmailId - Email Id
     * @return - Return Result
     */
    public static String setForgotPasswordJson(String mEmailId) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.FORGOT_PASSWORD.REQUEST_EMAIL_ID, mEmailId);

            result = setParentJsonData(WebFields.FORGOT_PASSWORD.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Change Password API
     *
     * @param mUserId      - Email Id
     * @param mOldPassword - Old Password
     * @param mPassword    - New Password
     * @return - Return Result
     */
    public static String setChangePasswordJson(String mUserId, String mOldPassword, String mPassword) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.CHANGE_PASSWORD.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.CHANGE_PASSWORD.REQUEST_OLD_PASSWORD, mOldPassword);
            jsonBody.put(WebFields.CHANGE_PASSWORD.REQUEST_PASSWORD, mPassword);

            result = setParentJsonData(WebFields.CHANGE_PASSWORD.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Receptionist Appointment List API
     *
     * @param currentPage       - Current Page
     * @param mAddAppointment   - Flag for Appointment (1-> Receptionist, 2-> Councillor, 3-> Patient)
     * @param mName             - Patient Name
     * @param mPatientCode      - Patient Code
     * @param mMobileNo         - Mobile No
     * @param mMRDNo            - MRD No
     * @param mAppointmentNo    - Appointment No
     * @param mAppointmentDate  - Appointment Date
     * @param mDoctorId         - Doctor Id
     * @param mType             - All Appointment
     * @param appointmentType   - Appointment Type
     * @param hospital_database - Hospital Database
     * @return - Return Result
     */
    public static String setReceptionistAppointmentListJson(Context context, int currentPage, String mAddAppointment, String mName, String mPatientCode, String mMobileNo, String mMRDNo, String mAppointmentNo, String mAppointmentDate, int mDoctorId, String mType, String appointmentType, String mPatientId,
                                                            String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.RECEPTIONIST_APPOINTMENT_LIST.REQUEST_CURRENT_PAGE, String.valueOf(currentPage));

            jsonBody.put(WebFields.RECEPTIONIST_APPOINTMENT_LIST.REQUEST_NAME, mName);
            jsonBody.put(WebFields.RECEPTIONIST_APPOINTMENT_LIST.REQUEST_PATIENT_CODE, mPatientCode);
            jsonBody.put(WebFields.RECEPTIONIST_APPOINTMENT_LIST.REQUEST_MOBILE, mMobileNo);
//            jsonBody.put(WebFields.RECEPTIONIST_APPOINTMENT_LIST.REQUEST_MRD_NO, mMRDNo);
            jsonBody.put(WebFields.RECEPTIONIST_APPOINTMENT_LIST.REQUEST_APPOINTMENT_NO, mAppointmentNo);
//            jsonBody.put(WebFields.RECEPTIONIST_APPOINTMENT_LIST.REQUEST_APPOINTMENT_DATE, mAppointmentDate);
//           jsonBody.put(WebFields.RECEPTIONIST_APPOINTMENT_LIST.REQUEST_DOCTOR_ID, mDoctorId);
//            jsonBody.put(WebFields.RECEPTIONIST_APPOINTMENT_LIST.REQUEST_TYPE, mType);
//            jsonBody.put(WebFields.RECEPTIONIST_APPOINTMENT_LIST.REQUEST_APPOINTMENT_STATUS, appointmentType);

            if (mAddAppointment.equalsIgnoreCase(context.getString(R.string.user_type_patient))) {
                jsonBody.put(WebFields.RECEPTIONIST_APPOINTMENT_LIST.REQUEST_PATIENT_ID, GetJsonData.getLoginData(context, WebFields.LOGIN.RESPONSE_USER_ID));
            } else if (mAddAppointment.equalsIgnoreCase(context.getString(R.string.user_type_optometrist))) {
                jsonBody.put(WebFields.RECEPTIONIST_APPOINTMENT_LIST.REQUEST_PATIENT_ID, mPatientId);
            } else if (mAddAppointment.equalsIgnoreCase(context.getString(R.string.user_type_doctor))) {
                jsonBody.put(WebFields.RECEPTIONIST_APPOINTMENT_LIST.REQUEST_PATIENT_ID, mPatientId);
            } else if (mAddAppointment.equalsIgnoreCase(context.getString(R.string.user_type_councillor))) {
                jsonBody.put(WebFields.RECEPTIONIST_APPOINTMENT_LIST.REQUEST_PATIENT_ID, mPatientId);
            } else {
                jsonBody.put(WebFields.RECEPTIONIST_APPOINTMENT_LIST.REQUEST_PATIENT_ID, AppConstants.DEFAULT_VALUE);
            }

            //set Hospital Database
            jsonBody.put(WebFields.RECEPTIONIST_APPOINTMENT_LIST.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.RECEPTIONIST_APPOINTMENT_LIST.MODE_RECEPTIONIST, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * Sets the child json for Receptionist Appointment List API
     *
     * @param currentPage       - Current Page
     * @param mAddAppointment   - Flag for Appointment (1-> Receptionist, 2-> Councillor, 3-> Patient)
     * @param mName             - Patient Name
     * @param mPatientCode      - Patient Code
     * @param mMobileNo         - Mobile No
     * @param mMRDNo            - MRD No
     * @param mAppointmentNo    - Appointment No
     * @param mAppointmentDate  - Appointment Date
     * @param mDoctorId         - Doctor Id
     * @param mType             - All Appointment
     * @param appointmentType   - Appointment Type
     * @param hospital_database - Hospital Database
     * @return - Return Result
     */
    public static String setReceptionistTimeAppointmentListJson(Context context, int currentPage, String mAddAppointment, String mName, String mPatientCode, String mMobileNo, String mMRDNo, String mAppointmentNo, String mAppointmentDate, int mDoctorId, String mType, String appointmentType, String mPatientId,
                                                                String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.RECEPTIONIST_TIME_APPOINTMENT_LIST.REQUEST_CURRENT_PAGE, String.valueOf(currentPage));

            jsonBody.put(WebFields.RECEPTIONIST_TIME_APPOINTMENT_LIST.REQUEST_NAME, mName);
            jsonBody.put(WebFields.RECEPTIONIST_TIME_APPOINTMENT_LIST.REQUEST_PATIENT_CODE, mPatientCode);
            jsonBody.put(WebFields.RECEPTIONIST_TIME_APPOINTMENT_LIST.REQUEST_MOBILE, mMobileNo);
            jsonBody.put(WebFields.RECEPTIONIST_TIME_APPOINTMENT_LIST.REQUEST_MRD_NO, mMRDNo);
            jsonBody.put(WebFields.RECEPTIONIST_TIME_APPOINTMENT_LIST.REQUEST_APPOINTMENT_NO, mAppointmentNo);
            jsonBody.put(WebFields.RECEPTIONIST_TIME_APPOINTMENT_LIST.REQUEST_APPOINTMENT_DATE, mAppointmentDate);
            jsonBody.put(WebFields.RECEPTIONIST_TIME_APPOINTMENT_LIST.REQUEST_DOCTOR_ID, mDoctorId);
            jsonBody.put(WebFields.RECEPTIONIST_TIME_APPOINTMENT_LIST.REQUEST_TYPE, mType);
            jsonBody.put(WebFields.RECEPTIONIST_TIME_APPOINTMENT_LIST.REQUEST_APPOINTMENT_STATUS, appointmentType);

            if (mAddAppointment.equalsIgnoreCase(context.getString(R.string.user_type_patient))) {
                jsonBody.put(WebFields.RECEPTIONIST_TIME_APPOINTMENT_LIST.REQUEST_PATIENT_ID, GetJsonData.getLoginData(context, WebFields.LOGIN.RESPONSE_USER_ID));
            } else if (mAddAppointment.equalsIgnoreCase(context.getString(R.string.user_type_optometrist))) {
                jsonBody.put(WebFields.RECEPTIONIST_TIME_APPOINTMENT_LIST.REQUEST_PATIENT_ID, mPatientId);
            } else if (mAddAppointment.equalsIgnoreCase(context.getString(R.string.user_type_doctor))) {
                jsonBody.put(WebFields.RECEPTIONIST_TIME_APPOINTMENT_LIST.REQUEST_PATIENT_ID, mPatientId);
            } else if (mAddAppointment.equalsIgnoreCase(context.getString(R.string.user_type_councillor))) {
                jsonBody.put(WebFields.RECEPTIONIST_TIME_APPOINTMENT_LIST.REQUEST_PATIENT_ID, mPatientId);
            } else {
                jsonBody.put(WebFields.RECEPTIONIST_TIME_APPOINTMENT_LIST.REQUEST_PATIENT_ID, AppConstants.DEFAULT_VALUE);
            }

            //set Hospital Database
            jsonBody.put(WebFields.RECEPTIONIST_TIME_APPOINTMENT_LIST.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.RECEPTIONIST_TIME_APPOINTMENT_LIST.MODE_RECEPTIONIST, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Receptionist Patient List API
     *
     * @param currentPage       - Current Page
     * @param mName             - Patient Name
     * @param mPatientCode      - Patient Code
     * @param mMobileNo         - Mobile No
     * @param hospital_database - Hospital database
     * @return - Return Result
     */
    public static String setReceptionistPatientListJson(int currentPage, String mName, String mPatientCode, String mMobileNo, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.RECEPTIONIST_PATIENT_LIST.REQUEST_CURRENT_PAGE, String.valueOf(currentPage));
            jsonBody.put(WebFields.RECEPTIONIST_PATIENT_LIST.REQUEST_NAME, mName);
            jsonBody.put(WebFields.RECEPTIONIST_PATIENT_LIST.REQUEST_PATIENT_CODE, mPatientCode);
            jsonBody.put(WebFields.RECEPTIONIST_PATIENT_LIST.REQUEST_MOBILE, mMobileNo);

            jsonBody.put(WebFields.RECEPTIONIST_PATIENT_LIST.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.RECEPTIONIST_PATIENT_LIST.MODE_RECEPTIONIST, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Receptionist Follow Up List API
     *
     * @param currentPage       - Current Page
     * @param mPatientName      - Patient Name
     * @param mPatientCode      - Patient Code
     * @param mMobileNo         - Mobile No
     * @param mFollowUpDate     - Follow Up Date
     * @param mDoctorId         - Doctor Id
     * @param mCouncillorID     - Councillor Id
     * @param mType             - Type
     * @param hospital_database - Hospital Database
     * @return - Return Result
     */
    public static String setReceptionistFollowUpListJson(int currentPage, String mPatientName, String mPatientCode, String mMobileNo, String mFollowUpDate, int mDoctorId, int mCouncillorID, String mType, String hospital_database) {

        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.RECEPTIONIST_FOLLOW_UP_LIST.REQUEST_CURRENT_PAGE, String.valueOf(currentPage));
            jsonBody.put(WebFields.RECEPTIONIST_FOLLOW_UP_LIST.REQUEST_NAME, mPatientName);
            jsonBody.put(WebFields.RECEPTIONIST_FOLLOW_UP_LIST.REQUEST_PATIENT_CODE, mPatientCode);
            jsonBody.put(WebFields.RECEPTIONIST_FOLLOW_UP_LIST.REQUEST_MOBILE, mMobileNo);
            jsonBody.put(WebFields.RECEPTIONIST_FOLLOW_UP_LIST.REQUEST_FOLLOW_UP_DATE, mFollowUpDate);
            jsonBody.put(WebFields.RECEPTIONIST_FOLLOW_UP_LIST.REQUEST_DOCTOR_ID, mDoctorId);
            jsonBody.put(WebFields.RECEPTIONIST_FOLLOW_UP_LIST.REQUEST_COUNCILLOR_ID, mCouncillorID);
            jsonBody.put(WebFields.RECEPTIONIST_FOLLOW_UP_LIST.REQUEST_TYPE, mType);
            jsonBody.put(WebFields.RECEPTIONIST_FOLLOW_UP_LIST.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.RECEPTIONIST_FOLLOW_UP_LIST.MODE_RECEPTIONIST, jsonBody);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Receptionist Payment List API
     *
     * @param currentPage       - Current Page
     * @param mName             - Patient Name
     * @param mPatientCode      - Patient Code
     * @param mMobileNo         - Mobile No
     * @param mAppointmentNo    - Appointment No
     * @param mAppointmentDate  - Appointment Date
     * @param hospital_database - Hospital Database
     * @return - Return Result
     */
    public static String setReceptionistPaymentListJson(int currentPage, String mName, String mPatientCode, String mMobileNo, String mAppointmentNo, String mAppointmentDate, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.RECEPTIONIST_PAYMENT_LIST.REQUEST_CURRENT_PAGE, String.valueOf(currentPage));
            jsonBody.put(WebFields.RECEPTIONIST_PAYMENT_LIST.REQUEST_NAME, mName);
            jsonBody.put(WebFields.RECEPTIONIST_PAYMENT_LIST.REQUEST_PATIENT_CODE, mPatientCode);
            jsonBody.put(WebFields.RECEPTIONIST_PAYMENT_LIST.REQUEST_MOBILE_NO, mMobileNo);
            jsonBody.put(WebFields.RECEPTIONIST_PAYMENT_LIST.REQUEST_APPOINTMENT_NO, mAppointmentNo);
            jsonBody.put(WebFields.RECEPTIONIST_PAYMENT_LIST.REQUEST_APPOINTMENT_DATE, mAppointmentDate);
            jsonBody.put(WebFields.RECEPTIONIST_PAYMENT_LIST.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.RECEPTIONIST_PAYMENT_LIST.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Receptionist Add Patient API
     *
     * @param mFirstName       - First Name
     * @param mLastName        - Last Name
     * @param mEmail           - Email Id
     * @param mMobile          - Mobile
     * @param mCountryId       - Country Id
     * @param mStateId         - State Id
     * @param mCityId          - City Id
     * @param mAreaId          - Area Id
     * @param mGender          - Gender
     * @param age              - Age
     * @param mCityName        - City Name
     * @param mAreaName        - Area Name
     * @param mUserId          - User Id
     * @param mAddress         - Address
     * @param mPinCode         - Pin Code
     * @param mOccupation      - Occupation
     * @param namePrefix       - Name Prefix
     * @param MiddleName       - Middle Name
     * @param hospitalDatabase - Database
     * @return - Return Result
     */
    public static String setReceptionistAddPatientJson(String mFirstName, String mLastName, String mEmail,
                                                       String mMobile, int mCountryId, int mStateId,
                                                       int mCityId, int mAreaId, String mGender, String age,
                                                       String mCityName, String mAreaName, String mUserId,
                                                       String mAddress, String mPinCode, String mOccupation,
                                                       String namePrefix, String MiddleName, String hospitalDatabase) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_FIRST_NAME, mFirstName);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_LAST_NAME, mLastName);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_EMAIL_ID, mEmail);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_MOBILE_NO, mMobile);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_COUNTRY_ID, mCountryId);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_STATE_ID, mStateId);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_CITY_ID, mCityId);
//            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_AREA_ID, mAreaId);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_GENDER, mGender);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_AGE, age);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_CITY_NAME, mCityName);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_AREA_NAME, mAreaName);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_AREA_ADDRESS, mAddress);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_PINCODE, mPinCode);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_OCC, mOccupation);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_PREFIX, namePrefix);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_MIDDLE_NAME, MiddleName);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_DATABASE_NAME, hospitalDatabase);

            result = setParentJsonData(WebFields.ADD_PATIENT.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * Sets the child json for Receptionist Add Patient API
     *
     * @param mFirstName       - First Name
     * @param mLastName        - Last Name
     * @param mEmail           - Email Id
     * @param mMobile          - Mobile
     * @param mCountryId       - Country Id
     * @param mStateId         - State Id
     * @param mCityId          - City Id
     * @param mAreaId          - Area Id
     * @param mGender          - Gender
     * @param age              - Age
     * @param mCityName        - City Name
     * @param mAreaName        - Area Name
     * @param mUserId          - User Id
     * @param mAddress         - Address
     * @param mPinCode         - Pin Code
     * @param mOccupation      - Occupation
     * @param namePrefix       - Name Prefix
     * @param MiddleName       - Middle Name
     * @param hospitalDatabase - Database
     * @return - Return Result
     */
    public static String setEditPatientDetailJson(String mFirstName, String mLastName, String mEmail,
                                                  String mMobile, int mCountryId, int mStateId,
                                                  int mCityId, int mAreaId, String mGender, String age,
                                                  String mCityName, String mAreaName, String mUserId,
                                                  String mAddress, String mPinCode, String mOccupation,
                                                  String namePrefix, String MiddleName, String patientID, String hospitalDatabase) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_FIRST_NAME, mFirstName);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_LAST_NAME, mLastName);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_EMAIL_ID, mEmail);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_MOBILE_NO, mMobile);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_COUNTRY_ID, mCountryId);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_STATE_ID, mStateId);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_CITY_ID, mCityId);
//            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_AREA_ID, mAreaId);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_GENDER, mGender);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_AGE, age);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_CITY_NAME, mCityName);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_AREA_NAME, mAreaName);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_AREA_ADDRESS, mAddress);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_PINCODE, mPinCode);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_OCC, mOccupation);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_PREFIX, namePrefix);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_MIDDLE_NAME, MiddleName);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_DATABASE_NAME, hospitalDatabase);
            jsonBody.put(WebFields.ADD_PATIENT.REQUEST_PATIENT_USER_ID, patientID);

            result = setParentJsonData(WebFields.ADD_PATIENT.MODE_EDIT, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String setAddCouncillorFormJson(String mPatientId, String mCouncillorId, String councillor_date, String mEyeID, String mSurgeryTypeID,
                                                  String doctorId, String mDiseaseExplained, String mSurgeryExplained, String opetionSuggestedID, String patientPreferredOptionID,
                                                  String mMediclaim, String admission_date, String admission_time, String mRemarks, String mUserId, String mDatabaseName, String preliminaryExminationId) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_PATIENT_ID, mPatientId);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_COUNCILLOR_ID, mCouncillorId);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_COUNCILLOR_DATE, councillor_date);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_EYE_ID, mEyeID);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_SURGERY_TYPE_ID, mSurgeryTypeID);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_DOCTOR_ID, doctorId);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_DISEASE_EXPLAINED, mDiseaseExplained);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_SURGERY_EXPLAINED, mSurgeryExplained);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_PATIENT_PREFERRED_OPTION_ID, patientPreferredOptionID);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_OPTION_SUGGESTED_ID, opetionSuggestedID);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_MEDICLAIM, mMediclaim);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_ADMISSION_DATE, admission_date);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_ADMISSION_TIME, admission_time);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_REMARKS, mRemarks);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_DATABASE_NAME, mDatabaseName);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_PRELIMINARY_EXAMINATION_ID, preliminaryExminationId);

            result = setParentJsonData(WebFields.ADD_INQUIRY_FORM.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String setAddCouncillorFormJson(String mPatientId, String mUserId, String councillor_date,
                                                  String mEyeId, String surgery, String twoSurgery, String mDoctorId, String mDiseaseExplained,
                                                  String mSurgeryExplained, String mOptionSuggested, String mOptionOpted, String mMediclaim,
                                                  String admission_date, String admission_time, String mRemarks, String mUserId1,
                                                  String mDatabaseName, String preliminaryExminationId) {

        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_PATIENT_ID, mPatientId);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_COUNCILLOR_ID, mUserId);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_COUNCILLOR_DATE, councillor_date);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_EYE_ID, mEyeId);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_SURGERY_TYPE_ID, surgery);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_SURGERYTYPE2, twoSurgery);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_DOCTOR_ID, mDoctorId);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_DISEASE_EXPLAINED, mDiseaseExplained);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_SURGERY_EXPLAINED, mSurgeryExplained);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_PATIENT_PREFERRED_OPTION_ID, mOptionSuggested);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_OPTION_SUGGESTED_ID, mOptionOpted);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_MEDICLAIM, mMediclaim);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_ADMISSION_DATE, admission_date);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_ADMISSION_TIME, admission_time);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_REMARKS, mRemarks);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_USER_ID, mUserId1);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_DATABASE_NAME, mDatabaseName);
            jsonBody.put(WebFields.ADD_INQUIRY_FORM.REQUEST_PRELIMINARY_EXAMINATION_ID, preliminaryExminationId);

            result = setParentJsonData(WebFields.ADD_INQUIRY_FORM.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Councillor Inquiry Form List API
     *
     * @param currentPage      - Current Page
     * @param mPatientMobileNo - Patient Mobile no
     * @param mPatientName     - Patient Name
     * @param mCouncillorID    - Councillor Id
     * @param mCouncillorDate  - Councillor Date
     * @param mDatabaseName    - Database Name
     * @return - Return Result
     */
    public static String setCouncillorInquiryFormListJson(int currentPage, String mPatientMobileNo, String mPatientName, int mCouncillorID, String mCouncillorDate, String mDatabaseName) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.INQUIRY_FORM_LIST.REQUEST_CURRENT_PAGE, String.valueOf(currentPage));
            jsonBody.put(WebFields.INQUIRY_FORM_LIST.REQUEST_MOBILE_NO, mPatientMobileNo);
            jsonBody.put(WebFields.INQUIRY_FORM_LIST.REQUEST_NAME, mPatientName);
            jsonBody.put(WebFields.INQUIRY_FORM_LIST.REQUEST_COUNCILLOR_ID, String.valueOf(mCouncillorID));
            jsonBody.put(WebFields.INQUIRY_FORM_LIST.REQUEST_COUNCILLOR_DATE, mCouncillorDate);
            jsonBody.put(WebFields.INQUIRY_FORM_LIST.REQUEST_DATABASE_NAME, mDatabaseName);

            result = setParentJsonData(WebFields.INQUIRY_FORM_LIST.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Receptionist Add Follow Up API
     *
     * @param mPatientId        - Patient Id
     * @param mDoctorId         - Doctor Id
     * @param mCouncillorId     - Councillor Id
     * @param mFollowDate       - Follow Date
     * @param mUserId           - User Id
     * @param mNotes            - Notes
     * @param hospital_database - Hospital Database
     * @return - Return Result
     */
    public static String setReceptionistAddFollowUpJson(String mPatientId, int mDoctorId, int mCouncillorId, String mFollowDate, String mUserId, String mNotes, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();

            jsonBody.put(WebFields.ADD_FOLLOW_UP.REQUEST_PATIENT_ID, mPatientId);
            jsonBody.put(WebFields.ADD_FOLLOW_UP.REQUEST_DOCTOR_ID, mDoctorId);
            jsonBody.put(WebFields.ADD_FOLLOW_UP.REQUEST_COUNCILLOR_ID, mCouncillorId);
            jsonBody.put(WebFields.ADD_FOLLOW_UP.REQUEST_FOLLOW_UPDATE, mFollowDate);
            jsonBody.put(WebFields.ADD_FOLLOW_UP.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_FOLLOW_UP.REQUEST_NOTES, mNotes);
            jsonBody.put(WebFields.ADD_FOLLOW_UP.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.ADD_FOLLOW_UP.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Receptionist Add Appointment API
     *
     * @param mAppointmentDate   - Appointment Date
     * @param mAppointmentTime   - Appointment Time
     * @param mPatientId         - Patient Id
     * @param mDoctorId          - Doctor Id
     * @param mAmount            - Amount
     * @param mAppointmentType   - Appointment Type
     * @param mPaymentType
     * @param mComment           - Comment
     * @param mUserId            - User Id
     * @param mSelectedLocations - Selected Locations
     * @param mLocations         -Locations
     * @param followDate         - Follow Date
     * @param mSelectTreatment   - Treatment
     * @param hospital_database  - Hospital Database
     * @return - Return Result
     */
    public static String setReceptionistAddAppointmentJson(String mPatientId, int mDoctorId, String mAppointmentDate,
                                                           String mAppointmentTime, String mAmount,
                                                           String mAppointmentType, String mPaymentType,
                                                           String mComment, String mUserId, String mSelectedLocations, String mLocations, String followDate, String mSelectTreatment, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_APPOINTMENT.REQUEST_PATIENT_ID, mPatientId);
            jsonBody.put(WebFields.ADD_APPOINTMENT.REQUEST_DOCTOR_ID, mDoctorId);
            jsonBody.put(WebFields.ADD_APPOINTMENT.REQUEST_APPOINTMENT_DATE, mAppointmentDate);
            jsonBody.put(WebFields.ADD_APPOINTMENT.REQUEST_APPOINTMENT_TIME, mAppointmentTime);
            jsonBody.put(WebFields.ADD_APPOINTMENT.REQUEST_AMOUNT, mAmount);
            jsonBody.put(WebFields.ADD_APPOINTMENT.REQUEST_APPOINTMENT_TYPE, mAppointmentType);
            jsonBody.put(WebFields.ADD_APPOINTMENT.REQUEST_PAYMENT_STATUS, mPaymentType);
            jsonBody.put(WebFields.ADD_APPOINTMENT.REQUEST_COMMENT, mComment);
            jsonBody.put(WebFields.ADD_APPOINTMENT.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_APPOINTMENT.REQUEST_APPOINTMENT_LOCATION, mSelectedLocations);
            jsonBody.put(WebFields.ADD_APPOINTMENT.REQUEST_LOCATION, mLocations);
            jsonBody.put(WebFields.ADD_APPOINTMENT.REQUEST_FOLLOW_DATE, followDate);
            jsonBody.put(WebFields.ADD_APPOINTMENT.REQUEST_CHECK_UP_TYPE, mSelectTreatment);
            jsonBody.put(WebFields.ADD_APPOINTMENT.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.ADD_APPOINTMENT.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Receptionist Add Payment API
     *
     * @param mPatientId        - Patient Id
     * @param mAmount           - Amount
     * @param mPaymentDate      - Payment Date
     * @param mStatus           - Status
     * @param mAppointmentId    - Appointment Id
     * @param mUserId           - User Id
     * @param hospital_database - Hospital Database
     * @return - Return Result
     */
    public static String setReceptionistAddPaymentJson(String mPatientId, String mAmount, String mPaymentDate, String mStatus, String mAppointmentId, String mUserId, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_PAYMENT.REQUEST_PATIENT_ID, mPatientId);
            jsonBody.put(WebFields.ADD_PAYMENT.REQUEST_AMOUNT, mAmount);
            jsonBody.put(WebFields.ADD_PAYMENT.REQUEST_PAYMENT_DATE, mPaymentDate);
            jsonBody.put(WebFields.ADD_PAYMENT.REQUEST_PAYMENT_STATUS, mStatus);
            jsonBody.put(WebFields.ADD_PAYMENT.REQUEST_APPOINTMENT_ID, mAppointmentId);
            jsonBody.put(WebFields.ADD_PAYMENT.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_PAYMENT.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.ADD_PAYMENT.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Receptionist Check Appointment Validity for Add Appointment Json API
     *
     * @param mAppointmentDate - mAppointmentDate
     * @param mTimeSlot        - mTimeSlot
     * @param mDoctorId        - mDoctorId
     * @param mDatabaseName    - mDatabaseName
     * @return - Return Result
     */
    public static String checkAppointmentValidityJson(String mAppointmentDate, String mTimeSlot, int mDoctorId, String mDatabaseName) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.CHECK_APPOINTMENT_VALIDITY.REQUEST_DATE, mAppointmentDate);
            jsonBody.put(WebFields.CHECK_APPOINTMENT_VALIDITY.REQUEST_TIME_SLOT, mTimeSlot);
            jsonBody.put(WebFields.CHECK_APPOINTMENT_VALIDITY.REQUEST_DOCTOR_ID, mDoctorId);
            jsonBody.put(WebFields.CHECK_APPOINTMENT_VALIDITY.REQUEST_DATABASE_NAME, mDatabaseName);

            result = setParentJsonData(WebFields.CHECK_APPOINTMENT_VALIDITY.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Patient Appointment List API
     *
     * @param currentPage       - Current Page
     * @param mAppointmentNo    - Appointment No
     * @param mAppointmentDate  - Appointment Date
     * @param mDoctorId         - Doctor Id
     * @param mPatientId        - Patient Id
     * @param mType             - Type
     * @param hospital_database - Hospital Database
     * @return - Return Result
     */
    public static String setPatientAppointmentListJson(int currentPage, String mAppointmentNo, String mAppointmentDate, int mDoctorId, String mPatientId, String mType, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.PATIENT_APPOINTMENT_LIST.REQUEST_CURRENT_PAGE, String.valueOf(currentPage));
            jsonBody.put(WebFields.PATIENT_APPOINTMENT_LIST.REQUEST_APPOINTMENT_NO, mAppointmentNo);
            jsonBody.put(WebFields.PATIENT_APPOINTMENT_LIST.REQUEST_APPOINTMENT_DATE, mAppointmentDate);
            jsonBody.put(WebFields.PATIENT_APPOINTMENT_LIST.REQUEST_DOCTOR_ID, mDoctorId);
            jsonBody.put(WebFields.PATIENT_APPOINTMENT_LIST.REQUEST_PATIENT_ID, mPatientId);
            jsonBody.put(WebFields.PATIENT_APPOINTMENT_LIST.REQUEST_TYPE, mType);
            jsonBody.put(WebFields.PATIENT_APPOINTMENT_LIST.REQUEST_DATABASE_NAME, hospital_database);


            result = setParentJsonData(WebFields.PATIENT_APPOINTMENT_LIST.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Doctor List API
     *
     * @param hospital_database - Hospital Database
     */
    public static String setDoctorListJson(String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_DOCTOR_LIST.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_DOCTOR_LIST.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Package Item List API
     *
     * @param mDatabaseName - Hospital Database
     */
    public static String setPackageItemListJson(String mDatabaseName) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_PACKAGE_ITEM_LIST.REQUEST_DATABASE_NAME, mDatabaseName);

            result = setParentJsonData(WebFields.GET_PACKAGE_ITEM_LIST.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Preliminary Examination Details API
     *
     * @param hospital_database - Hospital Database
     */
    public static String setPreliminaryExaminationDetailsJson(String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.PRELIMINARY_EXAMINATION_DETAILS.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.PRELIMINARY_EXAMINATION_DETAILS.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Disease List API
     *
     * @param mAnatomicalLocationId - AnatomicalLocation Id
     * @param hospital_database     - Database Name
     * @return - Return Result
     */
    public static String setDiseaseListJson(int mAnatomicalLocationId, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_DISEASE_LIST.REQUEST_ANATOMICAL_LOCATION_ID, String.valueOf(mAnatomicalLocationId));
            jsonBody.put(WebFields.GET_DISEASE_LIST.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_DISEASE_LIST.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Councillor List API
     *
     * @param hospital_database - database Name
     */
    public static String setCouncillorListJson(String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_COUNCILLOR_LIST.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_COUNCILLOR_LIST.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Country List API
     *
     * @param hospital_database
     */
    public static String setCountryListJson(String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_COUNTRY_LIST.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_COUNTRY_LIST.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for State List API
     *
     * @param mCountryId        - Country Id
     * @param hospital_database - Hospital Database
     * @return - Return Result
     */
    public static String setStateListJson(String mCountryId, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_STATES.REQUEST_COUNTRY_ID, mCountryId);
            jsonBody.put(WebFields.GET_STATES.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_STATES.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for City List API
     *
     * @param mStateId          - State Id
     * @param hospital_database - Hospital Database
     * @return - Return Result
     */
    public static String setCityListJson(String mStateId, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_CITIES.REQUEST_STATE_ID, mStateId);
            jsonBody.put(WebFields.GET_CITIES.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_CITIES.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Area List API
     *
     * @param mCityId           - City Id
     * @param hospital_database
     * @return - Return Result
     */
    public static String setAreaListJson(String mCityId, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_AREA_LIST.REQUEST_CITY_ID, mCityId);
            jsonBody.put(WebFields.GET_AREA_LIST.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_AREA_LIST.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Package List API
     *
     * @param currentPage        - Current Page
     * @param mTitle             - Title
     * @param hospital_database- Hospital Database
     * @return - Return Result
     */
    public static String setCouncillorPackageListJson(int currentPage, String mTitle, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_PACKAGE_LIST.REQUEST_CURRENT_PAGE, currentPage);
            jsonBody.put(WebFields.GET_PACKAGE_LIST.REQUEST_TITLE, mTitle);
            jsonBody.put(WebFields.GET_PACKAGE_LIST.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_PACKAGE_LIST.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Patient Prescriptions List API
     *
     * @param currentPage       - Current Page
     * @param mPatientId        - Patient Id
     * @param hospital_database -Hospital Datbase
     * @return - Return Result
     */
    public static String setPatientPrescriptionsListJson(int currentPage, String mPatientId, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.PATIENT_PRESCRIPTIONS_LIST.REQUEST_CURRENT_PAGE, currentPage);
            jsonBody.put(WebFields.PATIENT_PRESCRIPTIONS_LIST.REQUEST_PATIENT_ID, mPatientId);
            jsonBody.put(WebFields.PATIENT_PRESCRIPTIONS_LIST.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.PATIENT_PRESCRIPTIONS_LIST.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Receptionist Patient Process List API
     *
     * @param currentPage       - Current Page
     * @param mPatientId        - Patient Id
     * @param hospital_database - Hospital Database
     * @return - Return Result
     */
    public static String setReceptionistProcessList(int currentPage, String mPatientId, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_PATIENT_PROCESS_LIST.REQUEST_CURRENT_PAGE, currentPage);
            jsonBody.put(WebFields.GET_PATIENT_PROCESS_LIST.REQUEST_PATIENT_ID, mPatientId);
            jsonBody.put(WebFields.GET_PATIENT_PROCESS_LIST.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_PATIENT_PROCESS_LIST.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Receptionist Appointment Process List API
     *
     * @param currentPage    - Current Page
     * @param mAppointmentId - Appointment Id
     * @param mDatabaseName  - Hospital Database
     * @return - Return Result
     */
    public static String setReceptionistAppointmentProcessList(int currentPage, String mAppointmentId, String mDatabaseName) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_APPOINTMENT_PROCESS_LIST.REQUEST_CURRENT_PAGE, currentPage);
            jsonBody.put(WebFields.GET_APPOINTMENT_PROCESS_LIST.REQUEST_APPOINTMENT_ID, mAppointmentId);
            jsonBody.put(WebFields.GET_APPOINTMENT_PROCESS_LIST.REQUEST_DATABASE_NAME, mDatabaseName);

            result = setParentJsonData(WebFields.GET_APPOINTMENT_PROCESS_LIST.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Add Check In API
     *
     * @param mAppointmentId - Appointment Id
     * @param mUserId        - User Id
     * @param mDatabaseName  - Database Name
     * @return - Return Result
     */
    public static String setAddCheckInJson(String mAppointmentId, String mUserId, String mDatabaseName) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_CHECK_IN.REQUEST_APPOINTMENT_ID, mAppointmentId);
            jsonBody.put(WebFields.ADD_CHECK_IN.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_CHECK_IN.REQUEST_DATABASE_NAME, mDatabaseName);

            result = setParentJsonData(WebFields.ADD_CHECK_IN.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Medication List API
     *
     * @param hospital_database -Hospital Database
     */
    public static String setMedicationListJson(String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_MEDICATION.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_MEDICATION.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Dosage List API
     *
     * @param hospital_database - Hospital Database
     */
    public static String setDosageListJson(String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_DOSAGE.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_DOSAGE.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Medication List API
     *
     * @param mDatabaseName - Hospital Database
     */
    public static String setUOMListJson(String mDatabaseName) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_UOM.REQUEST_DATABASE_NAME, mDatabaseName);

            result = setParentJsonData(WebFields.GET_UOM.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Special Instructions List API
     *
     * @param mDatabaseName - Hospital Database
     */
    public static String setSpecialInstructionsListJson(String mDatabaseName) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_SPECIAL_INSTRUCTIONS.REQUEST_DATABASE_NAME, mDatabaseName);

            result = setParentJsonData(WebFields.GET_SPECIAL_INSTRUCTIONS.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Preliminary Examination List API
     *
     * @param currentPage       - Current Page
     * @param mName             - Patient Name
     * @param mMobileNo         - Mobile No
     * @param mTreatmentDate    - Treatment Date
     * @param mDoctorId         - Doctor Id
     * @param mAppointmentId    - Appointment Id
     * @param hospital_database - Hospital Database
     * @return - Return Result
     */
    public static String setPreliminaryExaminationListJson(int currentPage, String mName, String mMobileNo, String mTreatmentDate, int mDoctorId, int mAppointmentId, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.PRELIMINARY_EXAMINATION_LIST.REQUEST_CURRENT_PAGE, currentPage);
            jsonBody.put(WebFields.PRELIMINARY_EXAMINATION_LIST.REQUEST_PATIENT_NAME, mName);
            jsonBody.put(WebFields.PRELIMINARY_EXAMINATION_LIST.REQUEST_MOBILE_NO, mMobileNo);
            jsonBody.put(WebFields.PRELIMINARY_EXAMINATION_LIST.REQUEST_TREATMENT_DATE, mTreatmentDate);
            jsonBody.put(WebFields.PRELIMINARY_EXAMINATION_LIST.REQUEST_DOCTOR_ID, mDoctorId);
            jsonBody.put(WebFields.PRELIMINARY_EXAMINATION_LIST.REQUEST_APPOINTMENT_ID, mAppointmentId);
            jsonBody.put(WebFields.PRELIMINARY_EXAMINATION_LIST.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.PRELIMINARY_EXAMINATION_LIST.MODE, jsonBody);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Optometrist Add Preliminary Examination API
     *
     * @param mValue                    - Value (Add or Edit)
     * @param mPreliminaryExaminationId - Preliminary Examination Id
     * @param mPreliminaryAdviceId      - Preliminary Advice Id
     * @param mPreliminaryLeftEyeId     - Preliminary Left Eye Id
     * @param mPreliminaryRightEyeId    - Preliminary Right Eye Id
     * @param mPatientId                - Patient Id
     * @param mUserId                   - User Id
     * @param mDoctorId                 - Doctor Id
     * @param mOcularHistoryId          - Ocular History Id
     * @param mComplaints               - Complaints
     * @param mFamilyOcularHistory      - Family Ocular History
     * @param mSystemicHistory          - Systemic History
     * @param mIPD                      - IPD
     * @param mTreatmentDate            - Treatment Date
     * @param mAnatomicalLocationId     - Anatomical Location Id
     * @param mDiseaseId                - Disease Id
     * @param mEyeId                    - EyeId
     * @param mEyeLensId                - EyeLensId
     * @param mRemarks                  - Remarks
     * @param mTypeId                   - TypeId
     * @param mPreviousGlass            - Previous Glass
     * @param mDiagnosis                - DiagnosisModel
     * @param mDialatedAR               - Dialated AR
     * @param mLeftEyeDistSPH           - Left Eye Dist SPH
     * @param mLeftEyeDistCYL           - Left Eye Dist CYL
     * @param mLeftEyeDistAxis          - Left Eye Dist Axis
     * @param mLeftEyeDistVA            - Left Eye Dist VA
     * @param mLeftEyeNearSPH           - Left Eye Near SPH
     * @param mLeftEyeNearCYL           - Left Eye Near CYL
     * @param mLeftEyeNearAxis          - Left Eye Near Axis
     * @param mLeftEyeNearVA            - Left Eye Near VA
     * @param mLeftEyeNCT               - Left Eye NCT
     * @param mLeftEyeAT                - Left Eye AT
     * @param mLeftEyePachymetry        - Left Eye Pachymetry
     * @param mLeftEyeColorVision       - Left Eye Color Vision
     * @param mLeftEyeSyringing         - Left Eye Syringing
     * @param mLeftEyeSchirmer          - Left Eye Schirmer
     * @param mLeftEyeOcularMotility    - Left Eye Ocular Motility
     * @param mLeftEyePupil             - Left Eye Pupil
     * @param mLeftEyeAdnex             - Left Eye Adnex
     * @param mLeftEyeAnteriorSegment   - Left Eye Anterior Segment
     * @param mLeftEyeLens              - Left Eye Lens
     * @param mLeftEyeFundus            - Left Eye Fundus
     * @param mLeftEyeGonioscopy        - Left Eye Gonioscopy
     * @param mRightEyeDistSPH          - Right Eye Dist SPH
     * @param mRightEyeDistCYL          - Right Eye Dist CYL
     * @param mRightEyeDistAxis         - Right Eye Dist Axis
     * @param mRightEyeDistVA           - Right Eye Dist VA
     * @param mRightEyeNearSPH          - Right Eye Near SPH
     * @param mRightEyeNearCYL          - Right Eye Near CYL
     * @param mRightEyeNearAxis         - Right Eye Near Axis
     * @param mRightEyeNearVA           - Right Eye Near VA
     * @param mRightEyeNCT              - Right Eye NCT
     * @param mRightEyeAT               - Right Eye AT
     * @param mRightEyePachymetry       - Right Eye Pachymetry
     * @param mRightEyeColorVision      - Right Eye Color Vision
     * @param mRightEyeSyringing        - Right Eye Syringing
     * @param mRightEyeSchirmer         - Right Eye Schirmer
     * @param mRightEyeOcularMotility   - Right Eye Ocular Motility
     * @param mRightEyePupil            - Right Eye Pupil
     * @param mRightEyeAdnex            - Right Eye Adnex
     * @param mRightEyeAnteriorSegment  - Right Eye Anterior Segment
     * @param mRightEyeLens             - Right Eye Lens
     * @param mRightEyeFundus           - Right Eye Funds
     * @param mRightEyeGonioscopy       - Right Eye Gonioscopy
     * @param hospital_database         - Data base Name
     * @return - Return Result
     */
    public static String setOptometristAddPreliminaryExaminationJson(
            String mValue, String mPreliminaryExaminationId, String mPreliminaryAdviceId, String mPreliminaryLeftEyeId,
            String mPreliminaryRightEyeId, int mAppointmentId, String mPatientId, String mUserId, int mDoctorId, int mOcularHistoryId,
            String mComplaints, String mFamilyOcularHistory, String mSystemicHistory, String mIPD, String mTreatmentDate, int mAnatomicalLocationId,
            int mDiseaseId, int mEyeId, int mEyeLensId, String mRemarks, int mTypeId, String mPreviousGlass, String mDiagnosis,
            String mDialatedAR, String mLeftEyeDistSPH, String mLeftEyeDistCYL, String mLeftEyeDistAxis, String mLeftEyeDistVA,
            String mLeftEyeNearSPH, String mLeftEyeNearCYL, String mLeftEyeNearAxis, String mLeftEyeNearVA, String mLeftEyeNCT,
            String mLeftEyeAT, String mLeftEyePachymetry, String mLeftEyeColorVision, String mLeftEyeSyringing, String mLeftEyeSchirmer,
            String mLeftEyeOcularMotility, String mLeftEyePupil, String mLeftEyeAdnex, String mLeftEyeAnteriorSegment, String mLeftEyeLens,
            String mLeftEyeFundus, String mLeftEyeGonioscopy, String mRightEyeDistSPH, String mRightEyeDistCYL, String mRightEyeDistAxis, String mRightEyeDistVA,
            String mRightEyeNearSPH, String mRightEyeNearCYL, String mRightEyeNearAxis, String mRightEyeNearVA, String mRightEyeNCT,
            String mRightEyeAT, String mRightEyePachymetry, String mRightEyeColorVision, String mRightEyeSyringing, String mRightEyeSchirmer,
            String mRightEyeOcularMotility, String mRightEyePupil, String mRightEyeAdnex, String mRightEyeAnteriorSegment,
            String mRightEyeLens, String mRightEyeFundus, String mRightEyeGonioscopy, String hospital_database) {

        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();

            if (mValue.equalsIgnoreCase(AppConstants.STR_EDIT)) {
                jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_PRELIMINARY_EXAMINATION_ID, String.valueOf(mPreliminaryExaminationId));
                jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_PRELIMINARY_ADVICE_ID, String.valueOf(mPreliminaryAdviceId));
                jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_PRELIMINARY_EYE_ID, String.valueOf(mPreliminaryLeftEyeId));
                jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_PRELIMINARY_EYE_ID, String.valueOf(mPreliminaryRightEyeId));
            }

            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_APPOINTMENT_ID, String.valueOf(mAppointmentId));
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_PATIENT_ID, mPatientId);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_DOCTOR_ID, String.valueOf(mDoctorId));
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_OCULAR_HISTORY_ID, String.valueOf(mOcularHistoryId));
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_PRIMARY_COMPLAINS, mComplaints);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_FAMILY_OCULAR_HISTORY, mFamilyOcularHistory);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_IPD, mIPD);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_SYSTEMIC_HISTORY, mSystemicHistory);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_TREATMENT_DATE, mTreatmentDate);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_ANATOMICAL_LOCATION_ID, String.valueOf(mAnatomicalLocationId));
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_DISEASE_ID, String.valueOf(mDiseaseId));
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_EYE_ID, String.valueOf(mEyeId));
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_EYE_LENS_ID, String.valueOf(mEyeLensId));
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_REMARK, mRemarks);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_TYPE_ID, String.valueOf(mTypeId));
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_PREVIOUS_GLASS, mPreviousGlass);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_DIAGNOSIS, mDiagnosis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_DIALATED_AR, mDialatedAR);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_D_SPH, mLeftEyeDistSPH);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_D_CYL, mLeftEyeDistCYL);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_D_AXIS, mLeftEyeDistAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_D_VA, mLeftEyeDistVA);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_N_SPH, mLeftEyeNearSPH);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_N_CYL, mLeftEyeNearCYL);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_N_AXIS, mLeftEyeNearAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_N_VA, mLeftEyeNearVA);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_NCT, mLeftEyeNCT);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_AT, mLeftEyeAT);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_PACHYMETRY, mLeftEyePachymetry);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_COLOR_VISION, mLeftEyeColorVision);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_SYRINGING, mLeftEyeSyringing);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_SCHIRMER, mLeftEyeSchirmer);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_OCULAR_MOTILITY, mLeftEyeOcularMotility);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_PUPIL, mLeftEyePupil);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_ADNEX, mLeftEyeAdnex);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_ANTERIOR_SEGMENT, mLeftEyeAnteriorSegment);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_LENS, mLeftEyeLens);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_FUNDUS, mLeftEyeFundus);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_L_GONIOSCOPY, mLeftEyeGonioscopy);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_D_SPH, mRightEyeDistSPH);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_D_CYL, mRightEyeDistCYL);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_D_AXIS, mRightEyeDistAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_D_VA, mRightEyeDistVA);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_N_SPH, mRightEyeNearSPH);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_N_CYL, mRightEyeNearCYL);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_N_AXIS, mRightEyeNearAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_N_VA, mRightEyeNearVA);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_NCT, mRightEyeNCT);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_AT, mRightEyeAT);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_PACHYMETRY, mRightEyePachymetry);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_COLOR_VISION, mRightEyeColorVision);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_SYRINGING, mRightEyeSyringing);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_SCHIRMER, mRightEyeSchirmer);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_OCULAR_MOTILITY, mRightEyeOcularMotility);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_PUPIL, mRightEyePupil);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_ADNEX, mRightEyeAdnex);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_ANTERIOR_SEGMENT, mRightEyeAnteriorSegment);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_LENS, mRightEyeLens);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_FUNDUS, mRightEyeFundus);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_R_GONIOSCOPY, mRightEyeGonioscopy);
            jsonBody.put(WebFields.ADD_PRELIMINARY_EXAMINATION.REQUEST_DATABASE_NAME, hospital_database);


            if (mValue.equalsIgnoreCase(AppConstants.STR_EDIT)) {
                result = setParentJsonData(WebFields.ADD_PRELIMINARY_EXAMINATION.MODE_EDIT, jsonBody);
            } else {
                result = setParentJsonData(WebFields.ADD_PRELIMINARY_EXAMINATION.MODE_ADD, jsonBody);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Preliminary Examination Details API
     *
     * @param context                   - Context
     * @param mType                     - Type
     * @param mPreliminaryExaminationId - Preliminary Examination Id
     * @param mUserId                   - User Id
     * @param mDatabaseName             - Database Name
     * @param mSelectedChipValue        - Selected Chip Value
     * @return - Return Result
     */
    public static String setHistoryAndComplaintsJson(Context context, String mType, String mPreliminaryExaminationId, String mUserId, String mDatabaseName, String mSelectedChipValue) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_HISTORY_COMPLAINTS.REQUEST_PRELIMINARY_EXAMINATION_ID, mPreliminaryExaminationId);
            jsonBody.put(WebFields.ADD_HISTORY_COMPLAINTS.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_HISTORY_COMPLAINTS.REQUEST_DATABASE_NAME, mDatabaseName);

            if (mType.equalsIgnoreCase(context.getResources().getString(R.string.text_primary_complaints))) {
                jsonBody.put(WebFields.ADD_HISTORY_COMPLAINTS.REQUEST_PRIMARY_COMPLAINS, mSelectedChipValue);
                result = setParentJsonData(WebFields.ADD_HISTORY_COMPLAINTS.MODE_PC, jsonBody);
            } else if (mType.equalsIgnoreCase(context.getResources().getString(R.string.text_systemic_history))) {
                jsonBody.put(WebFields.ADD_HISTORY_COMPLAINTS.REQUEST_SYSTEMIC_HISTORY, mSelectedChipValue);
                result = setParentJsonData(WebFields.ADD_HISTORY_COMPLAINTS.MODE_SH, jsonBody);
            } else if (mType.equalsIgnoreCase(context.getResources().getString(R.string.edit_ocular_history))) {
                jsonBody.put(WebFields.ADD_HISTORY_COMPLAINTS.REQUEST_OCULAR_HISTORY, mSelectedChipValue);
                result = setParentJsonData(WebFields.ADD_HISTORY_COMPLAINTS.MODE_OH, jsonBody);
            } else {
                jsonBody.put(WebFields.ADD_HISTORY_COMPLAINTS.REQUEST_FAMILY_OCULAR_HISTORY, mSelectedChipValue);
                result = setParentJsonData(WebFields.ADD_HISTORY_COMPLAINTS.MODE_FH, jsonBody);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Preliminary Examination View History List API
     *
     * @param mPatientId    - Patient Id
     * @param mDatabaseName - Database Name
     * @return - Return Result
     */
    public static String setPreliminaryExaminationViewHistoryListJson(String mPatientId, String mDatabaseName) {
        String result = AppConstants.STR_EMPTY_STRING;

        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.VIEW_HISTORY_LIST.REQUEST_PATIENT_ID, mPatientId);
            jsonBody.put(WebFields.VIEW_HISTORY_LIST.REQUEST_DATABASE_NAME, mDatabaseName);

            result = setParentJsonData(WebFields.VIEW_HISTORY_LIST.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Pre Examination API
     *
     * @param mPreliminaryExaminationId - Preliminary Examination Id
     * @param mUserId                   - User Id
     * @param mDatabaseName             - Database Name
     * @param mRightEyeNCT              - mRightEyeNCT
     * @param mRightEyeAT               - mRightEyeAT
     * @param mRightEyePachymetry       - mRightEyePachymetry
     * @param mRightEyeSchirmer         - mRightEyeSchirmer
     * @param mLeftEyeNCT               - mLeftEyeNCT
     * @param mLeftEyeAT                - mLeftEyeAT
     * @param mLeftEyePachymetry        - mLeftEyePachymetry
     * @param mLeftEyeSchirmer          - mLeftEyeSchirmer
     * @return - Return Result
     */
    public static String setAddPreExaminationJson(String mPreliminaryExaminationId, String mUserId, String mDatabaseName,
                                                  String mRightEyeNCT, String mRightEyeAT, String mRightEyePachymetry,
                                                  String mRightEyeSchirmer, String mLeftEyeNCT, String mLeftEyeAT,
                                                  String mLeftEyePachymetry, String mLeftEyeSchirmer) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_PRE_EXAMINATION.REQUEST_PRELIMINARY_EXAMINATION_ID, mPreliminaryExaminationId);
            jsonBody.put(WebFields.ADD_PRE_EXAMINATION.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_PRE_EXAMINATION.REQUEST_DATABASE_NAME, mDatabaseName);
            jsonBody.put(WebFields.ADD_PRE_EXAMINATION.REQUEST_RNCT, mRightEyeNCT);
            jsonBody.put(WebFields.ADD_PRE_EXAMINATION.REQUEST_RAT, mRightEyeAT);
            jsonBody.put(WebFields.ADD_PRE_EXAMINATION.REQUEST_R_PACHYMETRY, mRightEyePachymetry);
            jsonBody.put(WebFields.ADD_PRE_EXAMINATION.REQUEST_R_SCHIRMER, mRightEyeSchirmer);
            jsonBody.put(WebFields.ADD_PRE_EXAMINATION.REQUEST_LNCT, mLeftEyeNCT);
            jsonBody.put(WebFields.ADD_PRE_EXAMINATION.REQUEST_LAT, mLeftEyeAT);
            jsonBody.put(WebFields.ADD_PRE_EXAMINATION.REQUEST_L_PACHYMETRY, mLeftEyePachymetry);
            jsonBody.put(WebFields.ADD_PRE_EXAMINATION.REQUEST_L_SCHIRMER, mLeftEyeSchirmer);
            jsonBody.put(WebFields.ADD_PRE_EXAMINATION.REQUEST_L_SCHIRMER, mLeftEyeSchirmer);

            result = setParentJsonData(WebFields.ADD_PRE_EXAMINATION.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Status List API for Change Status
     *
     * @param mAppointmentId    - Appointment Id
     * @param hospital_database - Hospital Database
     * @return - Return Result
     */
    public static String setStatusListJson(String mAppointmentId, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_STATUS_COMBO_BOX.REQUEST_APPOINTMENT_ID, mAppointmentId);
            jsonBody.put(WebFields.GET_STATUS_COMBO_BOX.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_STATUS_COMBO_BOX.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for User By User Type List API
     *
     * @param mUserType         - User Type
     * @param hospital_database -Database Name
     * @return - Return Result
     */
    public static String setUserByUserTypeJson(String mUserType, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_USER_BY_USER_TYPE.RESPONSE_USER_TYPE, mUserType);
            jsonBody.put(WebFields.GET_USER_BY_USER_TYPE.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_USER_BY_USER_TYPE.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Change Status API
     *
     * @param mAppointmentId    -Appointment Id
     * @param mNextStatusId     -Next Status Id
     * @param mActionUserId     - Action User Id
     * @param mTimeDifference   - Time Difference
     * @param mPatientId        - Patient Id
     * @param mUserId           - User Id
     * @param hospital_database -hospital Database
     * @return - Return Result
     */
    public static String setChangeStatusJson(String mAppointmentId, String mNextStatusId, String mActionUserId, String mTimeDifference, String mPatientId, String mUserId, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.CHANGE_STATUS.REQUEST_APPOINTMENT_ID, mAppointmentId);
            jsonBody.put(WebFields.CHANGE_STATUS.REQUEST_STATUS_ID, mNextStatusId);
            jsonBody.put(WebFields.CHANGE_STATUS.REQUEST_ACTION_USER_ID, mActionUserId);
            jsonBody.put(WebFields.CHANGE_STATUS.REQUEST_TIME_DEFERENCE, mTimeDifference);
            jsonBody.put(WebFields.CHANGE_STATUS.REQUEST_PATIENT_ID, mPatientId);
            jsonBody.put(WebFields.CHANGE_STATUS.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.CHANGE_STATUS.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.CHANGE_STATUS.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param mUserId           - User Id
     * @param mTitle            - Title
     * @param mAmount           - Amount
     * @param mDiscountAmount   - Discount Amount
     * @param mPackageDate      - Package Date
     * @param jsonArray         - Json Array for binding package item
     * @param hospital_database - Database Name
     * @return - Return Result
     */
    public static String setCouncillorAddPackageJson(String mUserId, String mTitle, float mAmount, String mDiscountAmount,
                                                     String mPackageDate, JSONArray jsonArray, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_PACKAGE.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_PACKAGE.REQUEST_TITLE, mTitle);
            jsonBody.put(WebFields.ADD_PACKAGE.REQUEST_AMOUNT, String.valueOf(mAmount));
            jsonBody.put(WebFields.ADD_PACKAGE.REQUEST_DISCOUNT_AMOUNT, mDiscountAmount);
            jsonBody.put(WebFields.ADD_PACKAGE.REQUEST_PACKAGE_DATE, mPackageDate);
            jsonBody.put(WebFields.ADD_PACKAGE.REQUEST_ARR_ITEM, jsonArray);
            jsonBody.put(WebFields.ADD_PACKAGE.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.ADD_PACKAGE.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Status Tab List API
     *
     * @param mUserId           - User Id
     * @param hospital_database -Database Name
     * @return - Return Result
     */
    public static String setStatusTabListJson(String mUserId, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_STATUS_TAB.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.GET_STATUS_TAB.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_STATUS_TAB.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Get Status Tabs Wise List API
     *
     * @param currentPage       - Current Page
     * @param mName             - Name
     * @param mPatientCode      - Patient Code
     * @param mMobileNo         - Mobile No
     * @param mAppointmentNo    - Appointment No
     * @param mAppointmentDate  - Appointment Date
     * @param mStatusId         - Status Id
     * @param hospital_database - Hospital Database
     * @return - Return Result
     */
    public static String getStatusTabList(int currentPage, String mName, String mPatientCode, String mMobileNo, String mAppointmentNo, String mAppointmentDate, String mStatusId, String mUserId, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_STATUS_TAB_LIST.REQUEST_CURRENT_PAGE, currentPage);
            jsonBody.put(WebFields.GET_STATUS_TAB_LIST.REQUEST_NAME, mName);
            jsonBody.put(WebFields.GET_STATUS_TAB_LIST.REQUEST_PATIENT_CODE, mPatientCode);
            jsonBody.put(WebFields.GET_STATUS_TAB_LIST.RESPONSE_MOBILE_NO, mMobileNo);
            jsonBody.put(WebFields.GET_STATUS_TAB_LIST.REQUEST_APPOINTMENT_NO, mAppointmentNo);
            jsonBody.put(WebFields.GET_STATUS_TAB_LIST.REQUEST_APPOINTMENT_DATE, mAppointmentDate);
            jsonBody.put(WebFields.GET_STATUS_TAB_LIST.REQUEST_STATUS_ID, mStatusId);
            jsonBody.put(WebFields.GET_STATUS_TAB_LIST.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.GET_STATUS_TAB_LIST.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_STATUS_TAB_LIST.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Add Patient Document Upload
     *
     * @param mAccessType - Access Type
     * @param mPatientId  - Patient Id
     * @param mFilePath   - File Path
     * @return - Return Result
     */
    public static String setDocumentUploadJson(String mAccessType, String mPatientId, File mFilePath) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_UPLOAD_PICS.REQUEST_ACCESS_TYPE, mAccessType);
            jsonBody.put(WebFields.ADD_UPLOAD_PICS.REQUEST_USER_ID, mPatientId);
            jsonBody.put(WebFields.ADD_UPLOAD_PICS.REQUEST_IMAGE_DATA, mFilePath);

            result = setParentJsonData(WebFields.ADD_UPLOAD_PICS.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Appointment Reject
     *
     * @param mSelReasonId      - Select Reason Id
     * @param mUserId           - User Id
     * @param mAppointmentId    - Appointment Id
     * @param mTimeDifference   - Time Difference
     * @param mPatientId        - Patient Id
     * @param hospital_database - Hospital Database
     * @return - Return Result
     */
    public static String setAppointmentReject(String mSelReasonId, String mUserId, String mAppointmentId, String mTimeDifference, String mPatientId, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.APPOINTMENT_REJECT.REQUEST_REASON_ID, mSelReasonId);
            jsonBody.put(WebFields.APPOINTMENT_REJECT.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.APPOINTMENT_REJECT.REQUEST_APPOINTMENT_ID, mAppointmentId);
            jsonBody.put(WebFields.APPOINTMENT_REJECT.REQUEST_TIME_DEFERENCE, mTimeDifference);
            jsonBody.put(WebFields.APPOINTMENT_REJECT.REQUEST_PATIENT_ID, mPatientId);
            jsonBody.put(WebFields.APPOINTMENT_REJECT.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.APPOINTMENT_REJECT.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Appointment Reschedule
     *
     * @param mAppointmentDate  - Appointment Date
     * @param mUserId           - User Id
     * @param mAppointmentId    - Appointment Id
     * @param mPatientId        - Patient Id
     * @param mAppointmentTime  - Appointment Time
     * @param hospital_database -Hospital Database
     * @param type              -Type
     * @return - Return Result
     */
    public static String setAppointmentRescheduleJson(String mAppointmentDate, String mUserId, String mAppointmentId, String mPatientId, String mAppointmentTime, String hospital_database, String type) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.APPOINTMENT_RESCHEDULE.REQUEST_APPOINTMENT_DATE, mAppointmentDate);
            jsonBody.put(WebFields.APPOINTMENT_RESCHEDULE.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.APPOINTMENT_RESCHEDULE.REQUEST_APPOINTMENT_ID, mAppointmentId);
            jsonBody.put(WebFields.APPOINTMENT_RESCHEDULE.REQUEST_TIME_DIFFERENCE, "10");
            jsonBody.put(WebFields.APPOINTMENT_RESCHEDULE.REQUEST_PATIENT_ID, mPatientId);
            jsonBody.put(WebFields.APPOINTMENT_RESCHEDULE.REQUEST_APPOINTMENT_TIME, mAppointmentTime);
            jsonBody.put(WebFields.APPOINTMENT_RESCHEDULE.REQUEST_DATABASE_NAME, hospital_database);
            jsonBody.put(WebFields.APPOINTMENT_RESCHEDULE.REQUEST_CHECK_UP_TYPE, type);

            result = setParentJsonData(WebFields.APPOINTMENT_RESCHEDULE.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Follow Up Reschedule
     *
     * @param mFollowUpDate     - Follow Up Date
     * @param mUserId           - User Id
     * @param mPatientId        - Patient Id
     * @param mFollowUpId       - Follow Up Id
     * @param hospital_database
     * @return - Return Result
     */
    public static String setFollowUpRescheduleJson(String mFollowUpDate, String mUserId, String mPatientId, String mFollowUpId, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.FOLLOW_UP_RESCHEDULE.REQUEST_FOLLOW_UP_DATE, mFollowUpDate);
            jsonBody.put(WebFields.FOLLOW_UP_RESCHEDULE.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.FOLLOW_UP_RESCHEDULE.REQUEST_PATIENT_ID, mPatientId);
            jsonBody.put(WebFields.FOLLOW_UP_RESCHEDULE.REQUEST_FOLLOW_UP_ID, mFollowUpId);
            jsonBody.put(WebFields.FOLLOW_UP_RESCHEDULE.REQUEST_DATABASE_NAME, hospital_database);


            result = setParentJsonData(WebFields.FOLLOW_UP_RESCHEDULE.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Appointment Reason
     *
     * @param hospital_database Hospital Daabase
     */
    public static String setAppointmentReason(String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_REASON.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_REASON.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * Sets the child json for Add Medication Reason
     *
     * @param item                    -Item
     * @param hospital_database       -Hospital Database
     * @param preliminary_examination
     * @return
     */
    public static String addMedication(JSONArray item, String hospital_database, String preliminary_examination) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_MEDICATION.REQUEST_ITEM, item);
            jsonBody.put(WebFields.ADD_MEDICATION.REQUEST_DATABASE_NAME, hospital_database);
            jsonBody.put(WebFields.ADD_MEDICATION.REQUEST_PRELIMINARY_EXAMINATION_ID, preliminary_examination);

            result = setParentJsonData(WebFields.ADD_MEDICATION.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * Sets the child json for Receptionist Document List Json
     *
     * @param mAppointmentID    - Appointment ID
     * @param hospital_database - Hospital Database
     * @return
     */
    public static String setReceptionistDocumentListJson(String mAppointmentID, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_DOCUMENT_LIST.REQUEST_APPOINTMENT_ID, mAppointmentID);
            jsonBody.put(WebFields.GET_DOCUMENT_LIST.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_DOCUMENT_LIST.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Receptionist Document Patient List Json
     *
     * @param patientId
     * @param hospital_database
     * @return
     */
    public static String setReceptionistPatinetWiseDocumentListJson(String patientId, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_DOCUMENT_BY_PATIENT_ID.REQUEST_PATIENT_ID, patientId);
            jsonBody.put(WebFields.GET_DOCUMENT_BY_PATIENT_ID.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_DOCUMENT_BY_PATIENT_ID.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Patient Home List Json
     *
     * @param mPatientId        Patient Id
     * @param hospital_database - Database Name
     * @return return result
     */
    public static String setPatientHomeJson(String mPatientId, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_PATIENT_HOME.REQUEST_PATIENT_ID, mPatientId);
            jsonBody.put(WebFields.GET_PATIENT_HOME.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_PATIENT_HOME.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Receptionist Home List Json
     *
     * @param hospital_database - Hospital Database
     * @return
     */
    public static String setReceptionistListJson(String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_RECEPTIONIST_HOME.REQUEST_DATABASE_NAME, hospital_database);
            result = setParentJsonData(WebFields.GET_RECEPTIONIST_HOME.MODE, jsonBody);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Councillor Home List Json
     *
     * @param hospital_database -Database Name
     * @return return String
     */
    public static String setCouncillorHomeListJson(String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_COUNCILLOR_HOME.REQUEST_DATABASE_NAME, hospital_database);
            result = setParentJsonData(WebFields.GET_COUNCILLOR_HOME.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Doctor Home List Json
     *
     * @param mAppointmentDate  - Appointment Date
     * @param hospital_database - Database Name
     * @return return String
     */
    public static String setDoctorHomeListJson(String mAppointmentDate, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_DOCTOR_AND_OPTO_HOME.DATE, mAppointmentDate);
            jsonBody.put(WebFields.GET_DOCTOR_AND_OPTO_HOME.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_DOCTOR_AND_OPTO_HOME.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param hospital_database - Hospital Database
     * @return
     */
    public static String setDoctorAvailabiltyListJson(String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.CHECK_DOCTOR_AVAILABILTY.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.CHECK_DOCTOR_AVAILABILTY.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the child json for Receptionist Edit Payment List Json
     *
     * @param mAppointmentId    - Appointment ID
     * @param mStatus           - Status
     * @param mPaymentDateTime  - Payment Date Time
     * @param mAmount           - Amount
     * @param mUserId           - User Id
     * @param mPatientId        - Patient Id
     * @param mPaymentHistoryId - Patient History Id
     * @param hospital_database - Database Name
     * @return -Return String
     */
    public static String setReceptionistEditPaymentJson(String mAppointmentId, String mStatus, String mPaymentDateTime, String mAmount, String mUserId, String mPatientId, String mPaymentHistoryId, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.EDIT_PAYMENT.REQUEST_APPOINTMENT_ID, mAppointmentId);
            jsonBody.put(WebFields.EDIT_PAYMENT.REQUEST_PAYMENT_STATUS, mStatus);
            jsonBody.put(WebFields.EDIT_PAYMENT.REQUEST_APPOINTMENT_DATE, mPaymentDateTime);
            jsonBody.put(WebFields.EDIT_PAYMENT.REQUEST_AMOUNT, mAmount);
            jsonBody.put(WebFields.EDIT_PAYMENT.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.EDIT_PAYMENT.REQUEST_PATIENT_ID, mPatientId);
            jsonBody.put(WebFields.EDIT_PAYMENT.REQUEST_PAYMENT_HISTORY_ID, mPaymentHistoryId);
            jsonBody.put(WebFields.EDIT_PAYMENT.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.EDIT_PAYMENT.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;

    }

    /**
     * Sets the child json for Payment Services Package List Json
     *
     * @param hospital_database
     * @return
     */
    public static String setPaymentServicesPackageListJson(String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.PAYMENT_SERVICES_PACKAGE.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.PAYMENT_SERVICES_PACKAGE.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * set Json Add Payment
     *
     * @param patient_id        Patient ID
     * @param paymentDate       -Payment Date
     * @param appointmentID     - Appointment ID
     * @param mStatus           -Status
     * @param notes             -Notes
     * @param mUserID           -User ID
     * @param subTotal          -sub Total
     * @param total             -Total
     * @param received          -Received
     * @param outstanding       - Outstanding
     * @param arrayItems        -Item
     * @param strPaymetType     -PaymentType
     * @param chequeNo          -Cheque
     * @param ifsc_code         -IFSC Code
     * @param utr_no            -Utr No
     * @param bankName          -Bank Name
     * @param branch            -Branch
     * @param account_no        -Account No
     * @param hospital_database - Hospital Name
     * @return -String
     */
    public static String setAddPaymentJson(String patient_id, String paymentDate, String appointmentID, String mStatus,
                                           String notes, String mUserID, String subTotal, String total, String received, String outstanding,
                                           JSONArray arrayItems, String strPaymetType, String chequeNo, String ifsc_code, String utr_no, String bankName, String branch, String account_no, String hospital_database) {

        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_PATIENT_ID, patient_id);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_PAYMENT_DATE, paymentDate);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_APPOINTMENT_ID, appointmentID);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_PAYMENT_STATUS, mStatus);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_PAYMENT_NOTES, notes);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_USER_ID, mUserID);

            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_SUB_TOTAL, subTotal);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_TOTAL, total);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_RECEIVED_AMOUNT, received);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_OUT_STANDING_AMOUNT, outstanding);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_PAYMENT_TYPE, strPaymetType);

            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_CHEQUE_NO, chequeNo);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_IFSC_CODE, ifsc_code);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_UTR_NO, utr_no);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_BANK_NAME, bankName);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_BRANCH, branch);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_ACCOUNT_NO, account_no);

            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_ITEM, arrayItems);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.ADD_PAYMENT_LIST.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * Set Add Paymeny on Check Up
     *
     * @param strPatientId      -Patient ID
     * @param paymentDate       - Payment Date
     * @param appointmentID     - appointmentID
     * @param mStatus           - Status
     * @param notes             - Notes
     * @param mUserID           - User id
     * @param subTotal          - Sub Total
     * @param total             -Total
     * @param Received          - Received
     * @param oustanding        - OutStanding
     * @param arrayItems        -Items
     * @param strPaymetType     - Payment Type
     * @param chequeNo          - CHEQUE No
     * @param ifsc_code         -IFSC COde
     * @param utr_no            - UTR No
     * @param bankName          - Bank Name
     * @param branch            -Branch
     * @param account_no        - Account Number
     * @param hospital_database -Database
     * @param cashRs            -Cash RS
     * @param chequeRs          - Check Money
     * @param neft              -NEFT
     * @param eWallet           - EWallet
     * @return
     */
    public static String setAddPaymentCheckUpJson(String strPatientId, String paymentDate,
                                                  String appointmentID, String mStatus, String notes, String mUserID, String subTotal,
                                                  String total, String Received, String oustanding, JSONArray arrayItems,
                                                  String strPaymetType, String chequeNo, String ifsc_code, String utr_no, String bankName,
                                                  String branch, String account_no, String hospital_database,
                                                  String cashRs, String chequeRs, String neft, String eWallet) {

        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_PATIENT_ID, strPatientId);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_PAYMENT_DATE, paymentDate);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_APPOINTMENT_ID, appointmentID);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_PAYMENT_STATUS, mStatus);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_PAYMENT_NOTES, notes);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_USER_ID, mUserID);

            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_SUB_TOTAL, subTotal);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_TOTAL, total);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_RECEIVED_AMOUNT, Received);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_OUT_STANDING_AMOUNT, oustanding);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_PAYMENT_TYPE, strPaymetType);

            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_CHEQUE_NO, chequeNo);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_IFSC_CODE, ifsc_code);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_UTR_NO, utr_no);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_BANK_NAME, bankName);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_BRANCH, branch);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_ACCOUNT_NO, account_no);

            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_ITEM, arrayItems);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_DATABASE_NAME, hospital_database);

            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_CASH_RS, cashRs);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_CHEQUE_RS, chequeRs);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_NEFT_RS, neft);
            jsonBody.put(WebFields.ADD_PAYMENT_LIST.REQUEST_EWALLET_RS, eWallet);

            result = setParentJsonData(WebFields.ADD_PAYMENT_LIST.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Set Child Json for Check In Parameter
     *
     * @param currentPageIndex  -Current Page Index
     * @param name              -Name
     * @param phone             -Phone
     * @param mCheckInDate      -Check Date
     * @param hospital_database -Hospital Database
     * @param type
     * @return -return String
     */
    public static String setCheckInListJson(int currentPageIndex, String name, String phone, String mCheckInDate, String hospital_database, String type) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.CHECK_IN_LIST.REQUEST_CURRENT_PAGE, currentPageIndex);
            jsonBody.put(WebFields.CHECK_IN_LIST.REQUEST_NAME, name);
            jsonBody.put(WebFields.CHECK_IN_LIST.REQUEST_MOBILE_NO, phone);
            jsonBody.put(WebFields.CHECK_IN_LIST.REQUEST_CHECK_IN_DATE, mCheckInDate);
            jsonBody.put(WebFields.CHECK_IN_LIST.REQUEST_CHECK_IN_TYPE, type);
            jsonBody.put(WebFields.CHECK_IN_LIST.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.CHECK_IN_LIST.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param mUserId                   - User ID
     * @param mDatabaseName             -Database Name
     * @param mAppointmentID            -Appointment ID
     * @param strCompany                - Company Name
     * @param strTPA                    - TPA
     * @param strPolicyNo               -Policy No
     * @param strExpireDate             - Expire Date
     * @param strSettlementReceived     - Settlement Received
     * @param strSettlementAmount       - Setllement Amount
     * @param mPreliminaryExaminationID
     * @return -Return String
     */
    public static String setAddMediclaimJson(String mUserId, String mDatabaseName,
                                             String mAppointmentID, String strCompany, String strTPA,
                                             String strPolicyNo, String strExpireDate, String strSettlementReceived,
                                             String strSettlementAmount, String mPreliminaryExaminationID) {

        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_MEDICLAIM.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_MEDICLAIM.REQUEST_DATABASE_NAME, mDatabaseName);
            jsonBody.put(WebFields.ADD_MEDICLAIM.REQUEST_APPOINTMENT_ID, mAppointmentID);
            jsonBody.put(WebFields.ADD_MEDICLAIM.REQUEST_COMPANY_NAME, strCompany);
            jsonBody.put(WebFields.ADD_MEDICLAIM.REQUEST_TPA, strTPA);
            jsonBody.put(WebFields.ADD_MEDICLAIM.REQUEST_POLICY_NO, strPolicyNo);
            jsonBody.put(WebFields.ADD_MEDICLAIM.REQUEST_EXPIRY_DATE, strExpireDate);
            jsonBody.put(WebFields.ADD_MEDICLAIM.REQUEST_SETTLEMENT_RECEIVED, strSettlementReceived);
            jsonBody.put(WebFields.ADD_MEDICLAIM.REQUEST_SETTLEMENT_AMOUNT, strSettlementAmount);
            jsonBody.put(WebFields.ADD_MEDICLAIM.REQUEST_PRELIMINARY_EXAMINATION_ID, mPreliminaryExaminationID);


            result = setParentJsonData(WebFields.ADD_MEDICLAIM.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Set Sub Json for Surgery Notes Add
     *
     * @param mAppointmentID      - Appointment ID
     * @param mUserId             - User ID
     * @param mDatabaseName       - Database Name
     * @param date                - Surgery Date
     * @param start_time          - Surgery Start Time
     * @param end_time            - Surgery End Time
     * @param mStrSurgeryID       - Surgery Type
     * @param right_acd           - Right Eye ACD
     * @param left_act            - Left Eye ACD
     * @param right_al            - Right Eye AL
     * @param left_al             - Left Eye AL
     * @param right_incision_size - Right Incision Size
     * @param left_incision_size  - Left Incision Size
     * @param right_incision_type - Right Incision Type
     * @param left_incision_type  - Left Incision Type
     * @param right_surgery_notes - Right Surgery Notes
     * @param left_surgery_notes  - Left Surgery Notes
     * @param right_viscoelastics - Right Viscoelastics
     * @param left_viscoelastics  - Left Viscoelastics
     * @param right_remarks       - Right Remark
     * @param left_remarks        - Left Remark
     * @return - Return String
     */
    public static String setAddSurgeryNoteJson(String mAppointmentID, String mUserId, String mDatabaseName, String date, String start_time, String end_time, String mStrSurgeryID,
                                               String right_acd, String left_act, String right_al,
                                               String left_al, String right_incision_size, String left_incision_size,
                                               String right_incision_type, String left_incision_type, String right_surgery_notes,
                                               String left_surgery_notes, String right_viscoelastics,
                                               String left_viscoelastics, String right_remarks, String left_remarks) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_DATABASE_NAME, mDatabaseName);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_APPOINTMENT_ID, mAppointmentID);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_SURGERY_DATE, date);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_START_TIME, start_time);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_END_TIME, end_time);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_SURGERY_TYPE_ID, mStrSurgeryID);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_RACD, right_acd);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_LACD, left_act);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_RAL, right_al);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_LAL, left_al);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_RINCISION_SIZE, right_incision_size);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_LINCISION_SIZE, left_incision_size);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_RINCISION_TYPE, right_incision_type);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_LINCISION_TYPE, left_incision_type);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_RSURGERY_NOTES, right_surgery_notes);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_LSURGERY_NOTES, left_surgery_notes);

            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_RVISCOELASTICS, right_viscoelastics);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_LVISCOELASTICS, left_viscoelastics);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_RREMARKS, right_remarks);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_LREMARKS, left_remarks);

            result = setParentJsonData(WebFields.ADD_SURGERY_NOTE.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Set Sub Json for Preliminary Surgery Notes Add
     *
     * @param mUserId                   - User ID
     * @param hospital_database         - Hospital Data base
     * @param mAppointmentID            - Appointment ID;
     * @param surgery_date              - Surgery Date
     * @param convert_starTime          - Start Time
     * @param convert_endTime           - End Time
     * @param surgeryTypeID             - Surgery Type ID
     * @param ACD                       - ACD
     * @param AL                        - AL
     * @param incisionSize              - Incision Size
     * @param incisionType              - Incision Type
     * @param surgeryNote               - Surgery Note
     * @param viscoelastics             - Viscoelastics
     * @param remarks                   - Remarks
     * @param eyeID                     - Eye ID
     * @param mPreliminaryExaminationID - Preliminay Examination ID
     * @param surgeryTypeIDTwo          - Second Surgery
     * @param convert_starDate          -Start Date
     * @param convert_endDate           - End Date
     * @return - Return String
     */
    public static String setAddPreliminarySurgeryNoteJson(String mUserId, String hospital_database, String mAppointmentID, String surgery_date, String convert_starTime, String convert_endTime, String surgeryTypeID, String ACD, String AL, String incisionSize, String incisionType, String surgeryNote,
                                                          String viscoelastics, String remarks, String eyeID, String mPreliminaryExaminationID, String surgeryTypeIDTwo, String convert_starDate, String convert_endDate) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_DATABASE_NAME, hospital_database);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_APPOINTMENT_ID, mAppointmentID);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_SURGERY_DATE, surgery_date);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_START_TIME, convert_starTime);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_END_TIME, convert_endTime);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_SURGERY_TYPE_ID, surgeryTypeID);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_ACD, ACD);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_AL, AL);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_INCISION_SIZE, incisionSize);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_INCISION_TYPE, incisionType);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_SURGERY_NOTES, surgeryNote);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_VISCOELASTICS, viscoelastics);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_REMARKS, remarks);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_EYE_ID, eyeID);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_PRELIMINARY_EXAMINATION_ID, mPreliminaryExaminationID);

            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_START_DATE, convert_starDate);
            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_END_DATE, convert_endDate);

            jsonBody.put(WebFields.ADD_SURGERY_NOTE.REQUEST_SURGERYTYPE2, surgeryTypeIDTwo);

            result = setParentJsonData(WebFields.ADD_SURGERY_NOTE.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Set JSON  For Get Surgery List
     *
     * @param mAppointmentID    - Appointment ID
     * @param mSurgerytDate     - Surgery Date
     * @param hospital_database - Hospital Database
     * @param currentPageIndex  - Current Page
     * @param mName             -Patient Name
     * @param mPatientCode      - Patient Code
     * @param mMobileNo         -Patient Mobile
     * @return - Return String
     */
    public static String setSurgeryListJson(String mAppointmentID, String mSurgerytDate, String hospital_database, int currentPageIndex, String mName, String mPatientCode, String mMobileNo) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.SURGERY_LIST.REQUEST_APPOINTMENT_ID, mAppointmentID);
            jsonBody.put(WebFields.SURGERY_LIST.REQUEST_SURGERY_DATE, mSurgerytDate);
            jsonBody.put(WebFields.SURGERY_LIST.REQUEST_DATABASE_NAME, hospital_database);
            jsonBody.put(WebFields.SURGERY_LIST.REQUEST_CURRENT_PAGE, currentPageIndex);
            jsonBody.put(WebFields.SURGERY_LIST.REQUEST_NAME, mName);
            jsonBody.put(WebFields.SURGERY_LIST.REQUEST_PATIENT_CODE, mPatientCode);
            jsonBody.put(WebFields.SURGERY_LIST.REQUEST_MOBILE_NO, mMobileNo);

            result = setParentJsonData(WebFields.SURGERY_LIST.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param mUserId                   - User ID
     * @param mDatabaseName             -Database Name
     * @param appointmentID             - Appointment ID
     * @param pulse                     -Pulse
     * @param bp                        -BP
     * @param spo2                      -SPO2
     * @param rbs                       -RBS
     * @param eye_id                    -Eye Id
     * @param anaesthesia_id            -Anaesthesia ID
     * @param anaesthesia_medicine      - Anaesthesia Medicine
     * @param mPreliminaryExaminationID
     * @return - Return String
     */
    public static String setAddAnaesthesiaSurgeryJson(String mUserId, String mDatabaseName, String appointmentID,
                                                      String pulse, String bp, String spo2, String rbs, String eye_id, String anaesthesia_id,
                                                      String anaesthesia_medicine, String mPreliminaryExaminationID) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_DATABASE_NAME, mDatabaseName);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_APPOINTMENT_ID, appointmentID);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_PULSE, pulse);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_BP, bp);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_SPO2, spo2);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_RBS, rbs);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_EYE_ID, eye_id);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_ANAESTHESIATYPE_ID, anaesthesia_id);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_ANAESTHESIA_MEDICINE, anaesthesia_medicine);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_PRELIMINARY_EXAMINATION_ID, mPreliminaryExaminationID);

            result = setParentJsonData(WebFields.ADD_ANAESTHESIA_SURGERY.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String setAddAnaesthesiaSurgeryJson(String mUserId, String hospital_database,
                                                      String appointmentID, String pulse, String s1, String spo2, String rbs, String eye_id, String anaesthesia_id,
                                                      String anaesthesia_medicine, String mPreliminaryExaminationID, String systolic, String diastolic) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_DATABASE_NAME, hospital_database);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_APPOINTMENT_ID, appointmentID);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_PULSE, pulse);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_BP, systolic);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_SPO2, spo2);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_DIASTOLICBP, diastolic);

            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_RBS, rbs);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_EYE_ID, eye_id);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_ANAESTHESIATYPE_ID, anaesthesia_id);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_ANAESTHESIA_MEDICINE, anaesthesia_medicine);
            jsonBody.put(WebFields.ADD_ANAESTHESIA_SURGERY.REQUEST_PRELIMINARY_EXAMINATION_ID, mPreliminaryExaminationID);

            result = setParentJsonData(WebFields.ADD_ANAESTHESIA_SURGERY.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }


    /**
     * @param mUserId           - User ID
     * @param mDatabaseName     - Database Name
     * @param appointmentID     - Appointment Id
     * @param implantBrand      - Implant Brand
     * @param implantName       - Implant Name
     * @param a_constant        - A Constant
     * @param implant_power     - Implant Power
     * @param implant_placement - Implant Placement
     * @param implant_expirty   - Implant Expirty
     * @param implant_slNo      - Implant Sl No
     * @param implant_notes     - Implant Notes
     * @return Return String
     */
    public static String setAddImplantSurgeryJson(String mUserId, String mDatabaseName, String appointmentID, String implantBrand,
                                                  String implantName, String a_constant, String implant_power, String implant_placement, String implant_expirty, String implant_slNo, String implant_notes) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_APPOINTMENT_ID, appointmentID);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_DATABASE_NAME, mDatabaseName);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_IMPLANT_BRAND, implantBrand);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_IMPLANT_NAME, implantName);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_A_CONSTANT, a_constant);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_IMPLANT_POWER, implant_power);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_IMPLANT_PLACEMENT, implant_placement);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_IMPLANT_EXPIRY, implant_expirty);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_IMPLANT_SL_NO, implant_slNo);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_NOTES, implant_notes);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_PRELIMINARY_EXAMINATION_ID, implant_notes);

            result = setParentJsonData(WebFields.ADD_IMPLANT_SURGERY.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * @param categoryID
     * @param hospital_database
     * @return
     */
    public static String getAnatomicalLocation(String categoryID, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_ANATOMICAL_LOCATION.REQUEST_CATEGORY_ID, categoryID);
            jsonBody.put(WebFields.GET_ANATOMICAL_LOCATION.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.GET_ANATOMICAL_LOCATION.MODE, jsonBody);
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
        return result;
    }

    /**
     * Set Json for Preliminary Examination Add Vision
     *
     * @param mUserId                 - User ID
     * @param mDatabaseName           - Database Name
     * @param mAppointmentId          - Appointment ID
     * @param patientID               - Parent ID
     * @param doctorID                - Doctor ID
     * @param strTreatmentDate        - Treatment Date
     * @param ucva_re_distance
     * @param ucavnreod
     * @param ucavdleos
     * @param ucavnleos
     * @param ucavRemarks
     * @param bcvaUndilatedDREOD
     * @param bcvaUndilatedNREOD
     * @param bcvaUndilatedDLEOS
     * @param bcvaUndilatedNLEOS
     * @param bcvaDilatedDREOD
     * @param bcvaDilatedNREOD
     * @param bcvaDilatedDLEOS
     * @param bcvaDilatedNLEOS
     * @param bcvaDilatedRemarks
     * @param refUndilatedRDSph
     * @param refUndilatedLDSph
     * @param refUndilatedRNSph
     * @param refUndilatedLNSph
     * @param refUndilatedRDCyl
     * @param refUndilatedLDCyl
     * @param refUndilatedRNCyl
     * @param refUndilatedLNCyl
     * @param refUndilatedRDAxis
     * @param refUndilatedLDAxis
     * @param refUndilatedRNAxis
     * @param refUndilatedLNAxis
     * @param refUndilatedRDVA
     * @param refUndilatedLDVA
     * @param refUndilatedRNVA
     * @param refUndilatedLNVA
     * @param refDilatedRDSph
     * @param refDilatedLDSph
     * @param refDilatedRNSph
     * @param refDilatedLNSph
     * @param refDilatedRDCyl
     * @param refDilatedLDCyl
     * @param refDilatedRNCyl
     * @param refDilatedLNCyl
     * @param refDilatedRDAxis
     * @param refDilatedLDAxis
     * @param refDilatedRNAxis
     * @param refDilatedLNAxis
     * @param refDilatedRDVA
     * @param refDilatedLDVA
     * @param refDilatedRNVA
     * @param refDilatedLNVA
     * @param refFinalRDSph
     * @param refFinalLDSph
     * @param refFinalRNSph
     * @param refFinalLNSph
     * @param refFinalRDCyl
     * @param refFinalLDCyl
     * @param refFinalRNCyl
     * @param refFinalLNCyl
     * @param refFinalRDAxis
     * @param refFinalLDAxis
     * @param refFinalRNAxis
     * @param refFinalLNAxis
     * @param refFinalRDVA
     * @param refFinalLDVA
     * @param refFinalRNVA
     * @param refFinalLNVA
     * @param refFinalIPD
     * @param k1RPower
     * @param k1RAxis
     * @param k1LPower
     * @param k1LAxis
     * @param k2RPower                - K2 Right POWER
     * @param k2RAxis                 - K2 Right AXIS
     * @param k2LPower                - K2 Power
     * @param k2LAxis                 - K2 Axis
     * @param preliminaryExminationId -Preliminary Examination ID
     * @return - Return String
     */
    public static String setAddPreliminaryVisionJson(String mUserId, String mDatabaseName, String mAppointmentId,
                                                     String patientID, String doctorID, String strTreatmentDate, String ucva_re_distance, String ucavnreod, String ucavdleos, String ucavnleos, String ucavRemarks, String bcvaUndilatedDREOD, String bcvaUndilatedNREOD,
                                                     String bcvaUndilatedDLEOS, String bcvaUndilatedNLEOS, String bcvaDilatedDREOD, String bcvaDilatedNREOD, String bcvaDilatedDLEOS, String bcvaDilatedNLEOS, String bcvaDilatedRemarks, String refUndilatedRDSph,
                                                     String refUndilatedLDSph, String refUndilatedRNSph, String refUndilatedLNSph, String refUndilatedRDCyl, String refUndilatedLDCyl, String refUndilatedRNCyl, String refUndilatedLNCyl, String refUndilatedRDAxis, String refUndilatedLDAxis,
                                                     String refUndilatedRNAxis, String refUndilatedLNAxis, String refUndilatedRDVA, String refUndilatedLDVA, String refUndilatedRNVA, String refUndilatedLNVA, String refDilatedRDSph, String refDilatedLDSph, String refDilatedRNSph, String refDilatedLNSph,
                                                     String refDilatedRDCyl, String refDilatedLDCyl, String refDilatedRNCyl, String refDilatedLNCyl, String refDilatedRDAxis, String refDilatedLDAxis, String refDilatedRNAxis, String refDilatedLNAxis, String refDilatedRDVA, String refDilatedLDVA,
                                                     String refDilatedRNVA, String refDilatedLNVA, String refFinalRDSph, String refFinalLDSph, String refFinalRNSph, String refFinalLNSph, String refFinalRDCyl, String refFinalLDCyl, String refFinalRNCyl, String refFinalLNCyl, String refFinalRDAxis,
                                                     String refFinalLDAxis, String refFinalRNAxis, String refFinalLNAxis, String refFinalRDVA, String refFinalLDVA, String refFinalRNVA, String refFinalLNVA, String refFinalIPD, String k1RPower, String k1RAxis, String k1LPower, String k1LAxis, String k2RPower, String k2RAxis, String k2LPower, String k2LAxis, String preliminaryExminationId) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_DATABASE_NAME, mDatabaseName);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_APPOINTMENT_ID, mAppointmentId);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_PATIENT_ID, patientID);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_DOCTOR_ID, doctorID);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_TREATMENT_DATE, strTreatmentDate);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_UCAVDREOD, ucva_re_distance);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_UCAVNREOD, ucavnreod);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_UCAVDLEOS, ucavdleos);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_UCAV_REMARKS, ucavRemarks);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_UCAVNLEOS, ucavnleos);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_BCVAUNDILATEDDREOD, bcvaUndilatedDREOD);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_BCVAUNDILATEDNREOD, bcvaUndilatedNREOD);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_BCVAUNDILATEDDLEOS, bcvaUndilatedDLEOS);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_BCVAUNDILATEDNLEOS, bcvaUndilatedNLEOS);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_BCVADILATEDDREOD, bcvaDilatedDREOD);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_BCVADILATEDNREOD, bcvaDilatedNREOD);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_BCVADILATEDDLEOS, bcvaDilatedDLEOS);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_BCVADILATEDNLEOS, bcvaDilatedNLEOS);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_BCVADILATEDREMARKS, bcvaDilatedRemarks);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDRDSPH, refUndilatedRDSph);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDLDSPH, refUndilatedLDSph);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDRNSPH, refUndilatedRNSph);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDLNSPH, refUndilatedLNSph);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDRDCYL, refUndilatedRDCyl);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDLDCYL, refUndilatedLDCyl);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDRNCYL, refUndilatedRNCyl);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDLNCYL, refUndilatedLNCyl);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDRDAXIS, refUndilatedRDAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDLDAXIS, refUndilatedLDAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDRNAXIS, refUndilatedRNAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDLNAXIS, refUndilatedLNAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDRDVA, refUndilatedRDVA);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDLDVA, refUndilatedLDVA);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDRNVA, refUndilatedRNVA);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFUNDILATEDLNVA, refUndilatedLNVA);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDRDSPH, refDilatedRDSph);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDLDSPH, refDilatedLDSph);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDRNSPH, refDilatedRNSph);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDLNSPH, refDilatedLNSph);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDRDCYL, refDilatedRDCyl);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDLDCYL, refDilatedLDCyl);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDRNCYL, refDilatedRNCyl);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDLNCYL, refDilatedLNCyl);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDRDAXIS, refDilatedRDAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDLDAXIS, refDilatedLDAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDRNAXIS, refDilatedRNAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDLNAXIS, refDilatedLNAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDRDVA, refDilatedRDVA);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDLDVA, refDilatedLDVA);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDRNVA, refDilatedRNVA);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFDILATEDLNVA, refDilatedLNVA);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALRDSPH, refFinalRDSph);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALLDSPH, refFinalLDSph);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALRNSPH, refFinalRNSph);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALLNSPH, refFinalLNSph);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALRDCYL, refFinalRDCyl);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALLDCYL, refFinalLDCyl);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALRNCYL, refFinalRNCyl);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALLNCYL, refFinalLNCyl);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALRDAXIS, refFinalRDAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALLDAXIS, refFinalLDAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALRNAXIS, refFinalRNAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALLNAXIS, refFinalLNAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALRDVA, refFinalRDVA);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALLDVA, refFinalLDVA);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALRNVA, refFinalRNVA);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALLNVA, refFinalLNVA);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_REFFINALIPD, refFinalIPD);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_K1RPOWER, k1RPower);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_K1RAXIS, k1RAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_K1LPOWER, k1LPower);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_K1LAXIS, k1LAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_K2RPOWER, k2RPower);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_K2RAXIS, k2RAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_K2LPOWER, k2LPower);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_K2LAXIS, k2LAxis);
            jsonBody.put(WebFields.ADD_PRELIMINARY_VISION.REQUEST_PRELIMINARYEXAMINATION_ID, preliminaryExminationId);

            result = setParentJsonData(WebFields.ADD_PRELIMINARY_VISION.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Set Json for Preliminary Examination Add Vision
     *
     * @param preliminaryExaminationId - Preliminary Examination
     * @param mUserId                  - User id
     * @param jsonArray
     * @param hospital_database        - Database
     * @return - return String
     */
    public static String getAddPreliminaryDiagnosis(String preliminaryExaminationId, String mUserId,
                                                    JSONArray jsonArray, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_DIAGNOSIS.REQUEST_PRELIMINARY_EXAMINATION_ID, preliminaryExaminationId);
            jsonBody.put(WebFields.ADD_DIAGNOSIS.RESPONSE_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_DIAGNOSIS.REQUEST_ITEM, jsonArray);
            jsonBody.put(WebFields.ADD_DIAGNOSIS.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.ADD_DIAGNOSIS.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * get histroy of Preliminary Examination
     *
     * @param treatmentDate     - Treatment Date
     * @param currentPage       -Current Page
     * @param mPatientId        - Patient Id
     * @param mPatientMobile    - Mobile No
     * @param mDoctorId         - Doctor Id
     * @param hospital_database - Hospital Data base
     * @return - return String
     */
    public static String setHistoryPreliminaryExaminationListJson(String treatmentDate, int currentPage, String mPatientId,
                                                                  String mPatientMobile, String mDoctorId, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.PRELIMINARY_EXAMINATION_LIST.REQUEST_TREATMENT_DATE, treatmentDate);
            jsonBody.put(WebFields.PRELIMINARY_EXAMINATION_LIST.REQUEST_CURRENT_PAGE, currentPage);
            jsonBody.put(WebFields.PRELIMINARY_EXAMINATION_LIST.REQUEST_PATIENT_ID, mPatientId);
            jsonBody.put(WebFields.PRELIMINARY_EXAMINATION_LIST.REQUEST_MOBILE_NO, mPatientMobile);
            jsonBody.put(WebFields.PRELIMINARY_EXAMINATION_LIST.REQUEST_DOCTOR_ID, mDoctorId);
            jsonBody.put(WebFields.PRELIMINARY_EXAMINATION_LIST.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.PRELIMINARY_EXAMINATION_LIST.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Set Add Basic of Preliminary Basic Details
     *
     * @param mTreatmentDate    -TreatmentDate
     * @param mAppointmentId    - Appointment ID
     * @param mPatientId        - Patient ID
     * @param mUserId           - User ID
     * @param mDoctorId         - Doctor ID
     * @param hospital_database - Database
     * @return String
     */
    public static String setAddPreliminaryBasicDetailsJson(String mTreatmentDate, String mAppointmentId, String mPatientId, String mUserId, String mDoctorId, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_PRELIMINARY_BASIC_DETAILS.REQUEST_TREATMENT_DATE, mTreatmentDate);
            jsonBody.put(WebFields.ADD_PRELIMINARY_BASIC_DETAILS.REQUEST_APPOINTMENT_ID, mAppointmentId);
            jsonBody.put(WebFields.ADD_PRELIMINARY_BASIC_DETAILS.REQUEST_PATIENT_ID, mPatientId);
            jsonBody.put(WebFields.ADD_PRELIMINARY_BASIC_DETAILS.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_PRELIMINARY_BASIC_DETAILS.REQUEST_DOCTOR_ID, mDoctorId);
            jsonBody.put(WebFields.ADD_PRELIMINARY_BASIC_DETAILS.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.ADD_PRELIMINARY_BASIC_DETAILS.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Set Add Basic of Preliminary Basic Details
     *
     * @param mAppointmentId - Appointment Id
     * @param mDatabaseName  - Database Name
     * @return String
     */
    public static String setDoodlingImagesListJson(int mAppointmentId, String mDatabaseName) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.GET_DOODLE_IMAGES.REQUEST_APPOINTMENT_ID, mAppointmentId);
            jsonBody.put(WebFields.GET_DOODLE_IMAGES.REQUEST_DATABASE_NAME, mDatabaseName);

            result = setParentJsonData(WebFields.GET_DOODLE_IMAGES.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Set Add Basic of Preliminary Basic Details
     *
     * @param mAppointmentId - Appointment Id
     * @param mOldImageName  - Old Image Name
     * @param mType          - Doodle Type
     * @param mDatabaseName  - Database Name
     * @param mUserId        - User Id
     * @return String
     */
    public static String setDeleteDoodleImageJson(int mAppointmentId, String mOldImageName, String mType, String mDatabaseName, String mUserId) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.DELETE_DOODLE_IMAGE.REQUEST_APPOINTMENT_ID, mAppointmentId);
            jsonBody.put(WebFields.DELETE_DOODLE_IMAGE.REQUEST_IMAGE_NAME, mOldImageName);
            jsonBody.put(WebFields.DELETE_DOODLE_IMAGE.REQUEST_TYPE, mType);
            jsonBody.put(WebFields.DELETE_DOODLE_IMAGE.REQUEST_DATABASE_NAME, mDatabaseName);
            jsonBody.put(WebFields.DELETE_DOODLE_IMAGE.REQUEST_USER_ID, mUserId);

            result = setParentJsonData(WebFields.DELETE_DOODLE_IMAGE.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Get Preliminary Examination Hisytory
     *
     * @param mTreatmentDate    - TreatmentDate
     * @param mCurrentPage      -Current Page
     * @param mPatientId        - Patient ID
     * @param mobile            -Mobile No
     * @param mDoctorID         - Doctor ID
     * @param type              -Type
     * @param hospital_database - Hospital Database
     * @return - Return String
     */
    public static String setHistoryPreliminaryExaminationJson(String mTreatmentDate, int mCurrentPage, String mPatientId, String mobile, int mDoctorID, String type, String hospital_database) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.HISTORY_PRELIMINARY_EXAMINATION.REQUEST_TREATMENT_DATE, mTreatmentDate);
            jsonBody.put(WebFields.HISTORY_PRELIMINARY_EXAMINATION.REQUEST_CURRENT_PAGE, String.valueOf(mCurrentPage));
            jsonBody.put(WebFields.HISTORY_PRELIMINARY_EXAMINATION.REQUEST_PATIENT_ID, mPatientId);
            jsonBody.put(WebFields.HISTORY_PRELIMINARY_EXAMINATION.REQUEST_MOBILE_NO, mobile);
            jsonBody.put(WebFields.HISTORY_PRELIMINARY_EXAMINATION.REQUEST_DOCTOR_ID, mDoctorID);
            jsonBody.put(WebFields.HISTORY_PRELIMINARY_EXAMINATION.REQUEST_TYPE, type);
            jsonBody.put(WebFields.HISTORY_PRELIMINARY_EXAMINATION.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.HISTORY_PRELIMINARY_EXAMINATION.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * this is JSon bind for Preliminary Surgery Suggested
     *
     * @param mAppointmentId    - Appointment Id
     * @param categoryID        - Category ID
     * @param surgeryTypeID     - Surgery Id
     * @param eyeID             - Eye ID
     * @param duration          - Duration
     * @param remarks           - Remarks
     * @param mUserId           - UserID;
     * @param hospital_database - Database
     * @param prelimianaryID    -Preliminary Examination ID
     * @param surgeryTwo        - second Surgery
     * @return - String
     */
    public static String addSurgerySuggested(int mAppointmentId, int categoryID,
                                             String surgeryTypeID, String eyeID,
                                             String duration, String remarks, String mUserId, String hospital_database,
                                             String prelimianaryID, String surgeryTwo) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_SURGERY_SUGGESTED.REQUEST_APPOINTMENT_ID, String.valueOf(mAppointmentId));

            jsonBody.put(WebFields.ADD_SURGERY_SUGGESTED.REQUEST_PRELIMINARY_EXAMINATION_ID, prelimianaryID);
            jsonBody.put(WebFields.ADD_SURGERY_SUGGESTED.REQUEST_CATEGORY_ID, categoryID);
            jsonBody.put(WebFields.ADD_SURGERY_SUGGESTED.REQUEST_SURGERY_ID, surgeryTypeID);
            jsonBody.put(WebFields.ADD_SURGERY_SUGGESTED.REQUEST_SURGERYTYPE2, surgeryTwo);
            jsonBody.put(WebFields.ADD_SURGERY_SUGGESTED.REQUEST_EYE_ID, eyeID);
            jsonBody.put(WebFields.ADD_SURGERY_SUGGESTED.REQUEST_DURATION, duration);
            jsonBody.put(WebFields.ADD_SURGERY_SUGGESTED.REQUEST_REMARKS, remarks);
            jsonBody.put(WebFields.ADD_SURGERY_SUGGESTED.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_SURGERY_SUGGESTED.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.ADD_SURGERY_SUGGESTED.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * set Json for Print Receipt
     *
     * @param preliminaryExaminationId - Preliminary Examination ID
     * @param mAppointmentID           - Appointment id
     * @param mHistoryAndComplaints    - History and Complains
     * @param mVisionUCVA              - Vision UCVA
     * @param mVisionBCVAUndilated     - Vision BCVA Undialed
     * @param mVisionFinal             - Vision Final
     * @param mVisionBCVADilated       - Vision BCVA Dialed
     * @param mPrimaryExamination      - Primary Examination
     * @param mDiagnosis               - DiagnosisModel
     * @param mInvestigationSuggested  -Investigation Suggested
     * @param mTreatmentSuggested      - Treatment Suggested
     * @param mCounselingDetails       - Counseling Detail
     * @param mSurgeryDetails          - Surgery Detail
     * @param mPrescription            - Prescription
     * @param mPayment                 - Payement
     * @param hospital_database        - Hospital Database
     * @return - Return String
     */
    public static String setPrintReceiptJson(String preliminaryExaminationId, String mAppointmentID,
                                             boolean mHistoryAndComplaints, boolean mVisionUCVA, boolean mVisionBCVAUndilated,
                                             boolean mVisionFinal, boolean mVisionBCVADilated, boolean mPrimaryExamination,
                                             boolean mDiagnosis, boolean mInvestigationSuggested, boolean mTreatmentSuggested,
                                             boolean mCounselingDetails, boolean mSurgeryDetails, boolean mPrescription, String mLanguage, Boolean mDischarge,
                                             boolean mPayment, String hospital_database) {

        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.PRINT_RECEIPT.REQUEST_PRELIMINARY_EXAMINATION_ID, preliminaryExaminationId);
            jsonBody.put(WebFields.PRINT_RECEIPT.REQUEST_APPOINTMENT_ID, mAppointmentID);
            jsonBody.put(WebFields.PRINT_RECEIPT.REQUEST_HISTORY_AND_COPLAINTS, mHistoryAndComplaints);
            jsonBody.put(WebFields.PRINT_RECEIPT.REQUEST_VISION_UCVA, mVisionUCVA);
            jsonBody.put(WebFields.PRINT_RECEIPT.REQUEST_VISION_BCVA_DILATED, mVisionBCVADilated);
            jsonBody.put(WebFields.PRINT_RECEIPT.REQUEST_VISION_BCVA_UNDILATED, mVisionBCVAUndilated);
            jsonBody.put(WebFields.PRINT_RECEIPT.REQUEST_PRIMARY_EXAMINATION, mPrimaryExamination);
            jsonBody.put(WebFields.PRINT_RECEIPT.REQUEST_DIAGNOSIS, mDiagnosis);
            jsonBody.put(WebFields.PRINT_RECEIPT.REQUEST_INVESTIGATION_SUGGESTED, mInvestigationSuggested);
            jsonBody.put(WebFields.PRINT_RECEIPT.REQUEST_TREATMENT_SUGGESTED, mTreatmentSuggested);
            jsonBody.put(WebFields.PRINT_RECEIPT.REQUEST_COUNSELING_DETAILS, mCounselingDetails);
            jsonBody.put(WebFields.PRINT_RECEIPT.REQUEST_SURGERY_DETAILS, mSurgeryDetails);
            jsonBody.put(WebFields.PRINT_RECEIPT.REQUEST_PRESCRIPTION, mPrescription);
            jsonBody.put(WebFields.PRINT_RECEIPT.REQUEST_PAYMENT, mPayment);
            jsonBody.put(WebFields.PRINT_RECEIPT.REQUEST_LANGUAGE, mLanguage);
            jsonBody.put(WebFields.PRINT_RECEIPT.REQUEST_DISCHARGE, mDischarge);
            jsonBody.put(WebFields.PRINT_RECEIPT.REQUEST_VISION_FINAL, mVisionFinal);

            jsonBody.put(WebFields.PRINT_RECEIPT.REQUEST_DATABASE_NAME, hospital_database);

            result = setParentJsonData(WebFields.PRINT_RECEIPT.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Sets the parent json for all the APIs and integrated in each of the child method.
     *
     * @param methodName - Method Name
     * @param jsonBody   - Json Body
     * @return - Json Object
     */
    private static String setParentJsonData(String methodName, JSONObject jsonBody) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(WebFields.METHOD, methodName);
            jsonObject.put(WebFields.BODY, jsonBody);

            Common.insertLog("Request::::> " + jsonObject.toString());

            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    public static String setAddImplantSurgeryJson(String mUserId,
                                                  String mDatabaseName, String appointmentID, String implantBrand, String implantName,
                                                  String a_constant, String implant_power, String implant_placement, String implant_expirty,
                                                  String implant_slNo, String implant_notes, String mPreliminaryExaminationID) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_USER_ID, mUserId);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_APPOINTMENT_ID, appointmentID);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_DATABASE_NAME, mDatabaseName);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_IMPLANT_BRAND, implantBrand);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_IMPLANT_NAME, implantName);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_A_CONSTANT, a_constant);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_IMPLANT_POWER, implant_power);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_IMPLANT_PLACEMENT, implant_placement);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_IMPLANT_EXPIRY, implant_expirty);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_IMPLANT_SL_NO, implant_slNo);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_NOTES, implant_notes);
            jsonBody.put(WebFields.ADD_IMPLANT_SURGERY.REQUEST_PRELIMINARY_EXAMINATION_ID, mPreliminaryExaminationID);

            result = setParentJsonData(WebFields.ADD_IMPLANT_SURGERY.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String closeFileJson(int mAppointmentId, String mDatabaseName) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.CLOSE_FILE.REQUEST_APPOINTMENT_ID, mAppointmentId);
            jsonBody.put(WebFields.CLOSE_FILE.REQUEST_DATABASE_NAME, mDatabaseName);

            result = setParentJsonData(WebFields.CLOSE_FILE.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static String getTempleteJson(String mDatabaseName) {
        String result = AppConstants.STR_EMPTY_STRING;
        try {
            JSONObject jsonBody = new JSONObject();
            jsonBody.put(WebFields.ADD_TEMPLETE.REQUEST_DATABASE_NAME, mDatabaseName);

            result = setParentJsonData(WebFields.ADD_TEMPLETE.MODE, jsonBody);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

}