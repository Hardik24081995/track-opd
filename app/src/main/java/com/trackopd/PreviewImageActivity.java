package com.trackopd;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

public class PreviewImageActivity extends Activity {

    private ImageView mImagePreview;
    private String mDoodleUrl,mDoodleType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_image);

        getBundle();
        getIds();
        setData();
    }

    /**
     * Id declarations
     */
    private void getBundle() {
        try {
            Bundle extras = getIntent().getExtras();

            mDoodleUrl = extras.getString(AppConstants.BUNDLE_DOODLE_URL);
            mDoodleType = extras.getString(AppConstants.BUNDLE_DEFUALT_IMAGE);

            Common.insertLog("Preview URL:::> " + mDoodleUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Image Views
            mImagePreview = findViewById(R.id.image_preview_doodle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    private void setData() {
        try {
            setDefualtImage();
            if (!mDoodleUrl.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){

                Glide.with(PreviewImageActivity.this)
                        .load(mDoodleUrl)
                        .apply(new RequestOptions().error(R.drawable.no_image_display_pic).
                                placeholder(R.drawable.no_image_display_pic))
                        .into(mImagePreview);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        setResult(200, returnIntent);
        finish();
    }

    public void setDefualtImage(){
        if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_right_eye_adnex))) {
            mImagePreview.setImageResource(R.drawable.adnex_icon);
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_left_eye_adnex))) {
            mImagePreview.setImageResource(R.drawable.adnex_icon);
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_right_eye_anterior_segment))) {
            mImagePreview.setImageResource(R.drawable.anterior_segment_icon);
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_left_eye_anterior_segment))) {
            mImagePreview.setImageResource(R.drawable.anterior_segment_icon);

        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_right_eye_lens))) {
            mImagePreview.setImageResource(R.drawable.lens_icon);
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_left_eye_lens))) {
            mImagePreview.setImageResource(R.drawable.lens_icon);
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_right_eye_fundus))) {
            mImagePreview.setImageResource(R.drawable.left_fundus_icon);
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_left_eye_fundus))) {
            mImagePreview.setImageResource(R.drawable.left_fundus_icon);
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_right_eye_gonioscopy))) {
            mImagePreview.setImageResource(R.drawable.gonioscopy_icon);
        } else if (mDoodleType.equalsIgnoreCase(getResources().getString(R.string.text_left_eye_gonioscopy))) {
            mImagePreview.setImageResource(R.drawable.gonioscopy_icon);
        }
    }
}
