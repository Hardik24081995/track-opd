package com.trackopd.Interface;

import android.app.Activity;

import com.trackopd.model.HistoryPreliminaryExaminationModel;

public interface OnSelectedHistory {

    void onSelectHistoryData(Activity mActivity, HistoryPreliminaryExaminationModel.Data checkInData);
}
