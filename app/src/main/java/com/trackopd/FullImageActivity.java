package com.trackopd;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.trackopd.adapter.ReceptionistPatientDetailDocumentAdapter;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.WebFields;

public class FullImageActivity extends AppCompatActivity {

    private ImageView mTouchImage,mImageClose;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_image);

        getIds();
        setData();
        setListener();
    }
    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Image Views
            mTouchImage = findViewById(R.id.touch_image_view);
            mImageClose=findViewById(R.id.image_fragment_full_image_close);

            mImageClose.setColorFilter(getResources().getColor(R.color.colorWhite));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the data
     */
    private void setData() {
        try {
            if (!ReceptionistPatientDetailDocumentAdapter.patientDetailDocumentModel.getReport().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                String document_url= WebFields.API_BASE_URL+WebFields.DOC_URL+ReceptionistPatientDetailDocumentAdapter.patientDetailDocumentModel.getReport();
                Glide.with(FullImageActivity.this)
                        .load(document_url)
                        .apply(new RequestOptions().error(R.drawable.ic_documents_img).placeholder(R.drawable.ic_documents_img))
                        .into(mTouchImage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *  Set Listener
     *
     */
    private void setListener() {
       try{
            mImageClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
       }catch (Exception e){
           Common.insertLog(e.getMessage());
       }
    }
}
