package com.trackopd;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.trackopd.utils.AppConstants;

public class SplashActivity extends Activity {

    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MyApplication.onActivityCreateSetTheme(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);

        setHandler();
    }

    /**
     * Handler declaration
     */
    private void setHandler() {
        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            public void run() {
                callToPassCodeActivity();
            }
        }, AppConstants.INT_SPLASH_TIMEOUT);
    }

    @Override
    protected void onDestroy() {
        try {
            if (mHandler != null) {
                mHandler.removeCallbacks(null);
                mHandler.removeCallbacksAndMessages(null);
                mHandler.removeCallbacksAndMessages(null);
                mHandler = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    /**
     * This method redirects you to set the pass-code first and than call the dashboard activity
     */
    private void callToPassCodeActivity() {
        Intent intent = new Intent(SplashActivity.this, PassCodeActivity.class);
        startActivity(intent);
        finish();
    }
}
