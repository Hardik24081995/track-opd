package com.trackopd;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.trackopd.model.ConfigurationModel;
import com.trackopd.model.LoginModel;
import com.trackopd.utils.AppCallbackListener;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.AppUtil;
import com.trackopd.utils.Common;
import com.trackopd.utils.DeviceUtils;
import com.trackopd.utils.SessionManager;
import com.trackopd.utils.StringUtils;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity implements AppCallbackListener.CallBackListener {

    private RelativeLayout mRelativeMainView;
    private EditText mEditEmail, mEditPassword;
    private TextView mTextForgotPassword;
    private Button mButtonSubmit;
    private AppUtil mAppUtils;
    private Intent intent;
    private String mUserType, mEmail, mPassword, mNotificationToken, mDeviceUID, mDeviceName, mOSVersion, mDeviceType;
    private SessionManager mSessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        mAppUtils = new AppUtil(this);
        mSessionManager = new SessionManager(this);

        getIds();
        setRegListeners();
        setData();
        callConfigurationAPI();
    }

    /**
     * Id declarations
     */
    private void getIds() {
        try {
            // Relative Layouts
            mRelativeMainView = findViewById(R.id.relative_login_main_view);

            // Edit Texts
            mEditEmail = findViewById(R.id.edit_login_email);
            mEditPassword = findViewById(R.id.edit_login_password);

            // Buttons
            mButtonSubmit = findViewById(R.id.button_login_submit);

            // Text Views
            mTextForgotPassword = findViewById(R.id.text_login_forgot_password);

            // Set Request Focus
            mEditEmail.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listener declarations
     */
    private void setRegListeners() {
        try {
            mButtonSubmit.setOnClickListener(clickListener);
            mTextForgotPassword.setOnClickListener(clickListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the dummy data for login
     */
    @SuppressLint("SetTextI18n")
    private void setData() {
        try {
            mEditEmail.setText("receptionist@gmail.com");
            mEditPassword.setText("123456");

            mDeviceName = DeviceUtils.getDeviceName();
            mDeviceUID = DeviceUtils.getDeviceId(getApplicationContext());
            mOSVersion = DeviceUtils.getDeviceOSNumber();
            mDeviceType = DeviceUtils.DEVICE_TYPE;
//            mNotificationToken = FirebaseInstanceId.getInstance().getToken();
            mNotificationToken = "345gf6gy54y4y456345";
            Common.insertLog("Refreshed Token:::> " + mNotificationToken);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Click listeners
     */
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_login_submit:
                    doLogin();
                    break;

                case R.id.text_login_forgot_password:
                    callForgotPasswordActivity();
                    break;
            }
        }
    };

    /**
     * This method checks the function first then call the API
     */
    private void doLogin() {
        hideSoftKeyboard();
        if (checkValidation()) {
            if (!mAppUtils.getConnectionState()) {
                mAppUtils.displayNoInternetSnackBar(mRelativeMainView, new AppCallbackListener(this));
            } else {
                showDialog(LoginActivity.this, false);
                callLoginAPI();
            }
        }
    }

    /**
     * This method redirects you to the forgot password activity
     */
    private void callForgotPasswordActivity() {
        try {
            Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation() {
        boolean status = true;

        mEmail = mEditEmail.getText().toString().trim();
        mPassword = mEditPassword.getText().toString().trim();

        mEditEmail.setError(null);
        mEditPassword.setError(null);

        if (TextUtils.isEmpty(mEmail)) {
            status = false;
            mEditEmail.setError(getResources().getString(R.string.error_field_required));
        }

        if (TextUtils.isEmpty(mPassword)) {
            mEditPassword.setError(getResources().getString(R.string.error_field_required));
            status = false;
        }

        if (status) {
            if (!StringUtils.isEmailValid(mEmail)) {
                status = false;
                mEditEmail.setError(getResources().getString(R.string.error_invalid_email));
            }

            if (mPassword.length() < getResources().getInteger(R.integer.min_length_password)) {
                status = false;
                mEditPassword.setError(getResources().getString(R.string.error_invalid_password));
            }
        }
        return status;
    }

    @Override
    public void onAppCallback(int Code) {
        doLogin();
    }

    /**
     * This method should call the Login API
     */
    private void callLoginAPI() {
        try {

            String hospital_database=GetJsonData.getHospitalData(LoginActivity.this,mSessionManager.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.setLoginJson(mEmail, mPassword, mNotificationToken, mDeviceUID, mDeviceName, mOSVersion, mDeviceType,hospital_database));

            Call<LoginModel> call = RetrofitClient.createService(ApiInterface.class).loginAPI(body);
            call.enqueue(new Callback<LoginModel>() {
                @Override
                public void onResponse(@NonNull Call<LoginModel> call, @NonNull Response<LoginModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        int mError = Integer.parseInt(jsonObject.getString(WebFields.ERROR));
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && mError == 200) {
                            hideDialog();
                           // Common.setCustomToast(LoginActivity.this, mMessage);
                            saveDataToSharedPreferences(jsonObject);
                        } else {
                            hideDialog();
                            Common.setCustomToast(LoginActivity.this, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<LoginModel> call, @NonNull Throwable t) {
                    hideDialog();
                    Common.insertLog("Failure:::> " + t.getMessage());
                    Common.setCustomToast(LoginActivity.this, t.getMessage());

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method store the data to the shared preferences and call the home activity by the user type
     */
    private void saveDataToSharedPreferences(JSONObject jsonObject) {
        try {
            if (jsonObject.has(WebFields.DATA)) {
                String mUserData = jsonObject.getString(WebFields.DATA);
                mSessionManager.setPreferences(mSessionManager.KEY_LOGIN_USER_DATA, mUserData);

                mUserType = GetJsonData.getLoginData(LoginActivity.this, WebFields.LOGIN.RESPONSE_USER_TYPE);
                Common.insertLog("User type:::> " + mUserType);

                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (mUserType.equalsIgnoreCase(getResources().getString(R.string.user_type_receptionist))) {
                            intent = new Intent(LoginActivity.this,
                                    ReceptionistHomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        } else if (mUserType.equalsIgnoreCase(getResources().getString(R.string.user_type_optometrist))) {
                            intent = new Intent(LoginActivity.this,
                                    OptometristHomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        } else if (mUserType.equalsIgnoreCase(getResources().getString(R.string.user_type_doctor))) {
                            intent = new Intent(LoginActivity.this,
                                    DoctorHomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        } else if (mUserType.equalsIgnoreCase(getResources().getString(R.string.user_type_patient))) {
                            intent = new Intent(LoginActivity.this,
                                    PatientHomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        } else if (mUserType.equalsIgnoreCase(getResources().getString(R.string.user_type_councillor))) {
                            intent = new Intent(LoginActivity.this,
                                    CouncillorHomeActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        }

                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent .addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        intent .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        finish();
                    }
                }, 1000);
            } else {
                mSessionManager.setPreferences(mSessionManager.KEY_LOGIN_USER_DATA, "");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the Configuration API
     */
    private void callConfigurationAPI() {
        try {


            String hospital_database=GetJsonData.getHospitalData(LoginActivity.this,mSessionManager.KEY_HOSPITAL_DATABASE_NAME);


            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(), APICommonMethods.setConfigurationJson(hospital_database));


            Call<ConfigurationModel> call = RetrofitClient.createService(ApiInterface.class).configurationAPI(body);
            call.enqueue(new Callback<ConfigurationModel>() {
                @Override
                public void onResponse(@NonNull Call<ConfigurationModel> call, @NonNull Response<ConfigurationModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            hideDialog();
                           // Common.setCustomToast(LoginActivity.this, mMessage);

                            if (jsonObject.has(WebFields.DATA)) {
                                String mConfigurationData = jsonObject.getString(WebFields.DATA);
                                mSessionManager.setPreferences(mSessionManager.KEY_CONFIGURATION_DATA, mConfigurationData);
                            }
                        } else {
                            hideDialog();
                            Common.setCustomToast(LoginActivity.this, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ConfigurationModel> call, @NonNull Throwable t) {
                    hideDialog();
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
