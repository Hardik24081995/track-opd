package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.trackopd.R;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.fragments.AddNewPreliminaryExaminationFragment;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.PrintReceiptModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientHistoryVisionAdapter extends RecyclerView.Adapter {
    private static final int unselectedItem = -1;
    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    ArrayList<HistoryPreliminaryExaminationModel.Data> mArrPreliminaryExamination;
    private Activity mActivity;
    private int selectedItem = unselectedItem;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private String mUserId, mUserType;


    public PatientHistoryVisionAdapter(Activity mActivity, RecyclerView recyclerView,
                                       ArrayList<HistoryPreliminaryExaminationModel.Data> mArrHistory) {
        this.mActivity = mActivity;
        this.mRecyclerView = recyclerView;
        this.mArrPreliminaryExamination = mArrHistory;
        mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
        LayoutInflater mLayoutInflater = LayoutInflater.from(mActivity);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        setOnScrollListener();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_history_preliminary_vision_item, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        if (holder instanceof CustomViewHolder) {
            final HistoryPreliminaryExaminationModel.Data checkInData = mArrPreliminaryExamination.get(position);

            ((CustomViewHolder) holder).mTextDateTime.setText(checkInData.getTreatmentDate());

            HistoryPreliminaryExaminationModel.Vision vision = checkInData.getVision().get(0);
            /// UCVA
            ((CustomViewHolder) holder).TextViewUCVARightEyeDistance.setText(vision.getUCVADR());
            ((CustomViewHolder) holder).TextViewUCVALeftEyeDistance.setText(vision.getUCVADL());
            ((CustomViewHolder) holder).mTextViewUCVALeftEyeNear.setText(vision.getUCVANL());
            ((CustomViewHolder) holder).mTextViewUCVARightEyeNear.setText(vision.getUCVANR());

            ((CustomViewHolder) holder).mTextViewUCVARemarks.setText(vision.getUCVARemarks());

            // BCVA
            ((CustomViewHolder) holder).mTextViewBCVAUnDilatedRightDistance.setText(vision.getBCVAUDR());
            ((CustomViewHolder) holder).mTextViewBCVADilatedRightDistance.setText(vision.getBCVADDR());
            ((CustomViewHolder) holder).mTextViewBCVAUnDilatedRightNear.setText(vision.getBCVAUNR());
            ((CustomViewHolder) holder).mTextViewBCVADilatedRightNear.setText(vision.getBCVADNR());

            ((CustomViewHolder) holder).mTextViewBCVAUnDilatedLeftDistance.setText(vision.getBCVAUDL());
            ((CustomViewHolder) holder).mTextViewBCVADilatedLeftDistance.setText(vision.getBCVADDL());
            ((CustomViewHolder) holder).mTextViewBCVAUnDilatedLeftNear.setText(vision.getBCVAUNL());
            ((CustomViewHolder) holder).mTextViewBCVADilatedLeftNear.setText(vision.getBCVADNL());

            ((CustomViewHolder) holder).mTextViewBCVARemarks.setText(vision.getBCVARemarks());

            // Refraction  Undilated Right Eye Distance
            ((CustomViewHolder) holder).mEditRefractionUnDilatedRightDistanceSph.setText(vision.getRefUDRSph());
            ((CustomViewHolder) holder).mEditRefractionUnDilatedRightDistanceCyl.setText(vision.getRefUDRCyl());
            ((CustomViewHolder) holder).mEditRefractionUnDilatedRightAxis.setText(vision.getRefUDRAxis());
            ((CustomViewHolder) holder).mEditRefractionUnDilatedRightVA.setText(vision.getRefUDRVA());

            // Refraction  Undilated Right Eye Near
            ((CustomViewHolder) holder).mEditRefractionUnDilatedRightNearSph.setText(vision.getRefUNRSph());
            ((CustomViewHolder) holder).mEditRefractionUnDilatedRightNearCyl.setText(vision.getRefUNRCyl());
            ((CustomViewHolder) holder).mEditRefractionUnDilatedRightNearAxis.setText(vision.getRefUNRAxis());
            ((CustomViewHolder) holder).mEditRefractionUnDilatedRightNearVA.setText(vision.getRefUNRVA());

            // Refraction  Undilated Left Eye Distance
            ((CustomViewHolder) holder).mEditRefractionUnDilatedLeftDistanceSph.setText(vision.getRefUDLSph());
            ((CustomViewHolder) holder).mEditRefractionUnDilatedLeftDistanceCyl.setText(vision.getRefUDLCyl());
            ((CustomViewHolder) holder).mEditRefractionUnDilatedLeftAxis.setText(vision.getRefUDLAxis());
            ((CustomViewHolder) holder).mEditRefractionUnDilatedLeftVA.setText(vision.getRefUDLVA());

            // Refraction  Undilated Left Eye Near
            ((CustomViewHolder) holder).mEditRefractionUnDilatedLeftNearSph.setText(vision.getRefUNLSph());
            ((CustomViewHolder) holder).mEditRefractionUnDilatedLeftNearCyl.setText(vision.getRefUNLCyl());
            ((CustomViewHolder) holder).mEditRefractionUnDilatedLeftNearAxis.setText(vision.getRefUNLAxis());
            ((CustomViewHolder) holder).mEditRefractionUnDilatedLeftNearVA.setText(vision.getRefUNLVA());


            // Refraction  dilated Right Eye Distance
            ((CustomViewHolder) holder).mEditRefractionDilatedRightDistanceSph.setText(vision.getRefDDRSph());
            ((CustomViewHolder) holder).mEditRefractionDilatedRightDistanceCyl.setText(vision.getRefDDRCyl());
            ((CustomViewHolder) holder).mEditRefractionDilatedRightAxis.setText(vision.getRefDDRAxis());
            ((CustomViewHolder) holder).mEditRefractionDilatedRightVA.setText(vision.getRefDDRVA());

            // Refraction  dilated Right Eye Near
            ((CustomViewHolder) holder).mEditRefractionDilatedRightNearSph.setText(vision.getRefDNRSph());
            ((CustomViewHolder) holder).mEditRefractionDilatedRightNearCyl.setText(vision.getRefDNRCyl());
            ((CustomViewHolder) holder).mEditRefractionDilatedRightNearAxis.setText(vision.getRefDNRAxis());
            ((CustomViewHolder) holder).mEditRefractionDilatedRightNearVA.setText(vision.getRefDNRVA());

            // Refraction  Dilated Left Eye Distance
            ((CustomViewHolder) holder).mEditRefractionDilatedLeftDistanceSph.setText(vision.getRefDDLSph());
            ((CustomViewHolder) holder).mEditRefractionDilatedLeftDistanceCyl.setText(vision.getRefDDLCyl());
            ((CustomViewHolder) holder).mEditRefractionDilatedLeftAxis.setText(vision.getRefDDLAxis());
            ((CustomViewHolder) holder).mEditRefractionDilatedLeftVA.setText(vision.getRefDDLVA());
//
            // Refraction  Dilated Right Eye Near
            ((CustomViewHolder) holder).mEditRefractionDilatedLeftNearSph.setText(vision.getRefDNLSph());
            ((CustomViewHolder) holder).mEditRefractionDilatedLeftNearCyl.setText(vision.getRefDNLCyl());
            ((CustomViewHolder) holder).mEditRefractionDilatedLeftNearAxis.setText(vision.getRefDNLAxis());
            ((CustomViewHolder) holder).mEditRefractionDilatedLeftNearVA.setText(vision.getRefDNLVA());

            // Refraction Final Right Eye Distance
            ((CustomViewHolder) holder).mEditRefractionFinalRightDistanceSph.setText(vision.getRefFDRSph());
            ((CustomViewHolder) holder).mEditRefractionFinalRightDistanceCyl.setText(vision.getRefFDRCyl());
            ((CustomViewHolder) holder).mEditRefractionFinalRightAxis.setText(vision.getRefFDRAxis());
            ((CustomViewHolder) holder).mEditRefractionFinalRightVA.setText(vision.getRefFDRVA());

            // Refraction  Final Right Eye Near
            ((CustomViewHolder) holder).mEditRefractionFinalRightNearSph.setText(vision.getRefFNRSph());
            ((CustomViewHolder) holder).mEditRefractionFinalRightNearCyl.setText(vision.getRefFNRCyl());
            ((CustomViewHolder) holder).mEditRefractionFinalRightNearAxis.setText(vision.getRefFNRAxis());
            ((CustomViewHolder) holder).mEditRefractionFinalRightNearVA.setText(vision.getRefFNRVA());

            // Refraction  Final Left Eye Distance
            ((CustomViewHolder) holder).mEditRefractionFinalLeftDistanceSph.setText(vision.getRefFDLSph());
            ((CustomViewHolder) holder).mEditRefractionFinalLeftDistanceCyl.setText(vision.getRefFDLCyl());
            ((CustomViewHolder) holder).mEditRefractionFinalLeftAxis.setText(vision.getRefFDLAxis());
            ((CustomViewHolder) holder).mEditRefractionFinalLeftVA.setText(vision.getRefFDLVA());

            // Refraction  Final Right Eye Near
            ((CustomViewHolder) holder).mEditRefractionFinalLeftNearSph.setText(vision.getRefFNLSph());
            ((CustomViewHolder) holder).mEditRefractionFinalLeftNearCyl.setText(vision.getRefFNLCyl());
            ((CustomViewHolder) holder).mEditRefractionFinalLeftNearAxis.setText(vision.getRefFNLAxis());
            ((CustomViewHolder) holder).mEditRefractionFinalLeftNearVA.setText(vision.getRefFNLVA());

            ((CustomViewHolder) holder).mTextRefrectionIPD.setText(vision.getIPD());

            ((CustomViewHolder) holder).mEditKeratometryKOneRightPower.setText(vision.getK1RPower());
            ((CustomViewHolder) holder).mEditKeratometryKOneRightAxis.setText(vision.getK1RAxis());
            ((CustomViewHolder) holder).mEditKeratometryKOneLeftPower.setText(vision.getK1LPower());
            ((CustomViewHolder) holder).mEditKeratometryKOneLeftAxis.setText(vision.getK1LAxis());
            ((CustomViewHolder) holder).mEditKeratometryKTwoRightPower.setText(vision.getK2RPower());
            ((CustomViewHolder) holder).mEditKeratometryKTwoRightAxis.setText(vision.getK2RAxis());
            ((CustomViewHolder) holder).mEditKeratometryKTwoLeftPower.setText(vision.getK2LPower());
            ((CustomViewHolder) holder).mEditKeratometryKTwoLeftAxis.setText(vision.getK2LAxis());

            //Set On Click Listener
            ((CustomViewHolder) holder).mImagePrint.setOnClickListener(v ->
                    callToPrintFinalVision(checkInData.getAppointmentID()));


            //Set Text Color
            ((CustomViewHolder) holder).mImageDateTime.setColorFilter(Common.setThemeColor(mActivity));
            ((CustomViewHolder) holder).mImageEdit.setColorFilter(Common.setThemeColor(mActivity));

            //Expandable Layout
            ((CustomViewHolder) holder).mImageExpandableArrowBCVA.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((CustomViewHolder) holder).mExpandableLayoutBCVA.isExpanded()) {
                        ((CustomViewHolder) holder).mImageExpandableArrowBCVA.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
                        ((CustomViewHolder) holder).mExpandableLayoutBCVA.collapse();
                    } else {
                        ((CustomViewHolder) holder).mImageExpandableArrowBCVA.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        ((CustomViewHolder) holder).mExpandableLayoutBCVA.expand();
                    }
                }
            });
            ((CustomViewHolder) holder).mImageExpandableArrowUCVA.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((CustomViewHolder) holder).mExpandableLayoutUCVA.isExpanded()) {
                        ((CustomViewHolder) holder).mImageExpandableArrowUCVA.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
                        ((CustomViewHolder) holder).mExpandableLayoutUCVA.collapse();
                    } else {
                        ((CustomViewHolder) holder).mImageExpandableArrowUCVA.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        ((CustomViewHolder) holder).mExpandableLayoutUCVA.expand();
                    }
                }
            });
            ((CustomViewHolder) holder).mImageExpandableArrowRefraction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((CustomViewHolder) holder).mExpandableLayoutRefraction.isExpanded()) {
                        ((CustomViewHolder) holder).mImageExpandableArrowRefraction.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
                        ((CustomViewHolder) holder).mExpandableLayoutRefraction.collapse();
                    } else {
                        ((CustomViewHolder) holder).mImageExpandableArrowRefraction.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        ((CustomViewHolder) holder).mExpandableLayoutRefraction.expand();
                    }
                }
            });
            ((CustomViewHolder) holder).mImageExpandableArrowKeratometry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((CustomViewHolder) holder).mExpandableLayoutKeratometry.isExpanded()) {
                        ((CustomViewHolder) holder).mImageExpandableArrowKeratometry.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
                        ((CustomViewHolder) holder).mExpandableLayoutKeratometry.collapse();
                    } else {
                        ((CustomViewHolder) holder).mImageExpandableArrowKeratometry.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        ((CustomViewHolder) holder).mExpandableLayoutKeratometry.expand();
                    }
                }
            });

            ((CustomViewHolder) holder).mImageEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSelectedItem(mActivity, checkInData);
                }
            });

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }


    protected void onSelectedItem(Activity activity, HistoryPreliminaryExaminationModel.Data checkInData) {

    }

    @Override
    public int getItemCount() {
        return mArrPreliminaryExamination == null ? 0 : mArrPreliminaryExamination.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrPreliminaryExamination.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    private void callAlert(String appointmentID) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                mActivity);
        builder.setTitle("Language Options");
        builder.setMessage("Choose your desired language");
        builder.setNegativeButton("Hindi",
                (dialog, which) -> callToPrintFinalVision(appointmentID));
        builder.setPositiveButton("English",
                (dialog, which) -> callToPrintFinalVision(appointmentID));
        builder.show();
    }

    /**
     * @param appointmentID - Appointment ID
     */
    private void callToPrintFinalVision(String appointmentID) {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            boolean mHistoryAndComplaints = false, mVisionUCVA = false, mVisionBCVAUndilated = false, mVisionFinal = true,
                    mVisionBCVADilated = false, mPrimaryExamination = false, mDiagnosis = false, mInvestigationSuggested = false,
                    mTreatmentSuggested = false, mCounselingDetails = false, mSurgeryDetails = false, mPrescription = false,
                    mPayment = false;

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPrintReceiptJson(AddNewPreliminaryExaminationFragment.PreliminaryExminationId,
                            String.valueOf(appointmentID), mHistoryAndComplaints, mVisionUCVA, mVisionBCVAUndilated,
                            mVisionFinal, mVisionBCVADilated, mPrimaryExamination, mDiagnosis, mInvestigationSuggested,
                            mTreatmentSuggested, mCounselingDetails, mSurgeryDetails, mPrescription,"",false, mPayment, hospital_database));

            Call<PrintReceiptModel> call = RetrofitClient.createService(ApiInterface.class).PrintReceipt(body);
            call.enqueue(new Callback<PrintReceiptModel>() {
                @Override
                public void onResponse(@NonNull Call<PrintReceiptModel> call, @NonNull Response<PrintReceiptModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body() != null) {
                                String url = response.body().getData();
                                callToReceiptPrint(url);
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PrintReceiptModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Call To Receipt Print
     *
     * @param url - String Url Print
     */
    private void callToReceiptPrint(String url) {
        //  String pdfOpen = AppConstants.PDF_OPEN + url;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mActivity.startActivity(browserIntent);
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder implements ExpandableLayout.OnExpansionUpdateListener {


        TextView mTextDateTime;

        ImageView mImageDateTime, mImageExpandableArrowUCVA, mImageExpandableArrowBCVA, mImageExpandableArrowRefraction,
                mImageExpandableArrowKeratometry, mImageEdit, mImagePrint;
        LinearLayout mLinearExpandableArrowUCVA, mLinearExpandableArrowBCVA, mLinearExpandableArrowRefraction,
                mLinearExpandableArrowKeratometry;
        ExpandableLayout mExpandableLayoutUCVA, mExpandableLayoutBCVA, mExpandableLayoutRefraction,
                mExpandableLayoutKeratometry;

        EditText mEditRefractionUnDilatedRightDistanceSph, mEditRefractionUnDilatedRightDistanceCyl, mEditRefractionUnDilatedRightAxis,
                mEditRefractionUnDilatedRightNearSph, mEditRefractionUnDilatedRightNearCyl, mEditRefractionUnDilatedRightNearAxis,
                mEditRefractionUnDilatedLeftDistanceSph, mEditRefractionUnDilatedLeftDistanceCyl, mEditRefractionUnDilatedLeftAxis,
                mEditRefractionUnDilatedLeftNearSph, mEditRefractionUnDilatedLeftNearCyl, mEditRefractionUnDilatedLeftNearAxis,
                mEditRefractionDilatedRightDistanceSph, mEditRefractionDilatedRightDistanceCyl, mEditRefractionDilatedRightAxis,
                mEditRefractionDilatedRightNearSph, mEditRefractionDilatedRightNearCyl, mEditRefractionDilatedRightNearAxis,
                mEditRefractionDilatedLeftDistanceSph, mEditRefractionDilatedLeftDistanceCyl, mEditRefractionDilatedLeftAxis,
                mEditRefractionDilatedLeftNearSph, mEditRefractionDilatedLeftNearCyl, mEditRefractionDilatedLeftNearAxis,
                mEditRefractionFinalRightDistanceSph, mEditRefractionFinalRightDistanceCyl, mEditRefractionFinalRightAxis,
                mEditRefractionFinalRightNearSph, mEditRefractionFinalRightNearCyl, mEditRefractionFinalRightNearAxis,
                mEditRefractionFinalLeftDistanceSph, mEditRefractionFinalLeftDistanceCyl, mEditRefractionFinalLeftAxis,
                mEditRefractionFinalLeftNearSph, mEditRefractionFinalLeftNearCyl, mEditRefractionFinalLeftNearAxis, mEditUCVARemarks, mEditKeratometryKOneRightPower, mEditKeratometryKOneRightAxis, mEditKeratometryKOneLeftPower, mEditKeratometryKOneLeftAxis,
                mEditKeratometryKTwoRightPower, mEditKeratometryKTwoRightAxis, mEditKeratometryKTwoLeftPower, mEditKeratometryKTwoLeftAxis;

        TextView TextViewUCVARightEyeDistance, TextViewUCVALeftEyeDistance, mTextViewUCVARightEyeNear,
                mTextViewUCVALeftEyeNear, mTextViewUCVARemarks, mTextViewBCVAUnDilatedRightDistance, mTextViewBCVAUnDilatedLeftDistance, mTextViewBCVAUnDilatedRightNear, mTextViewBCVAUnDilatedLeftNear,
                mTextViewBCVADilatedRightDistance, mTextViewBCVADilatedLeftDistance, mTextViewBCVADilatedRightNear, mTextViewBCVADilatedLeftNear,
                mTextViewBCVARemarks, mEditRefractionUnDilatedRightVA, mTextRefrectionIPD,
                mEditRefractionUnDilatedLeftVA, mEditRefractionUnDilatedLeftNearVA, mEditRefractionDilatedRightVA, mEditRefractionDilatedRightNearVA, mEditRefractionDilatedLeftVA, mEditRefractionDilatedLeftNearVA, mEditRefractionFinalRightNearVA,
                mEditRefractionFinalLeftVA, mEditRefractionFinalLeftNearVA, mEditRefractionFinalRightVA, mEditRefractionUnDilatedRightNearVA;


        public CustomViewHolder(final View mView) {
            super(mView);

            // Image View
            this.mImageDateTime = mView.findViewById(R.id.image_row_history_preliminary_vision_date);
            this.mImageEdit = mView.findViewById(R.id.image_row_history_preliminary_vision_edit);
            //Text View
            this.mTextDateTime = mView.findViewById(R.id.text_row_history_preliminary_vision_date);

            // Image Views
            this.mImageExpandableArrowUCVA = mView.findViewById(R.id.image_add_vision_ucva_expandable_view);
            this.mImageExpandableArrowBCVA = mView.findViewById(R.id.image_add_vision_bcva_expandable_view);
            this.mImageExpandableArrowRefraction = mView.findViewById(R.id.image_add_vision_refraction_expandable_view);
            this.mImageExpandableArrowKeratometry = mView.findViewById(R.id.image_add_vision_keratometry_expandable_view);

            // Linear Layouts
            this.mLinearExpandableArrowUCVA = mView.findViewById(R.id.linear_add_vision_ucva_expandable_view);
            this.mLinearExpandableArrowBCVA = mView.findViewById(R.id.linear_add_vision_bcva_expandable_view);
            this.mLinearExpandableArrowRefraction = mView.findViewById(R.id.linear_add_vision_refraction_expandable_view);
            this.mLinearExpandableArrowKeratometry = mView.findViewById(R.id.linear_add_vision_keratometry_expandable_view);

            // Expandable Layouts
            this.mExpandableLayoutUCVA = mView.findViewById(R.id.expandable_layout_add_vision_ucva);
            this.mExpandableLayoutBCVA = mView.findViewById(R.id.expandable_layout_add_vision_bcva);
            this.mExpandableLayoutRefraction = mView.findViewById(R.id.expandable_layout_add_vision_refraction);
            this.mExpandableLayoutKeratometry = mView.findViewById(R.id.expandable_layout_add_vision_keratometry);

            // Text Views
            this.TextViewUCVARightEyeDistance = mView.findViewById(R.id.text_view_add_vision_ucva_right_eye_distance);
            this.TextViewUCVALeftEyeDistance = mView.findViewById(R.id.text_view_add_vision_ucva_left_eye_distance);
            this.mTextViewUCVARightEyeNear = mView.findViewById(R.id.text_view_add_vision_ucva_right_eye_near);
            this.mTextViewUCVALeftEyeNear = mView.findViewById(R.id.text_view_add_vision_ucva_left_eye_near);
            this.mTextViewUCVARemarks = mView.findViewById(R.id.text_view_add_vision_ucva_remarks);

            this.mTextViewBCVAUnDilatedRightDistance = mView.findViewById(R.id.text_view_vision_bcva_undilated_right_eye_distance);
            this.mTextViewBCVADilatedRightDistance = mView.findViewById(R.id.text_view_add_vision_bcva_dilated_right_eye_distance);
            this.mTextViewBCVAUnDilatedRightNear = mView.findViewById(R.id.text_view_add_vision_bcva_undilated_right_eye_near);
            this.mTextViewBCVADilatedRightNear = mView.findViewById(R.id.text_view_add_vision_bcva_dilated_right_eye_near);

            this.mTextViewBCVAUnDilatedLeftDistance = mView.findViewById(R.id.text_view_add_vision_bcva_undilated_left_eye_distance);
            this.mTextViewBCVADilatedLeftDistance = mView.findViewById(R.id.text_view_add_vision_bcva_dilated_left_eye_distance);
            this.mTextViewBCVAUnDilatedLeftNear = mView.findViewById(R.id.text_view_add_vision_bcva_undilated_left_eye_near);
            this.mTextViewBCVADilatedLeftNear = mView.findViewById(R.id.text_view_add_vision_bcva_dilated_left_eye_near);

            this.mTextViewBCVARemarks = mView.findViewById(R.id.text_view_add_vision_bcva_remarks);

            // Refraction  Undilated Right Eye Distance
            this.mEditRefractionUnDilatedRightDistanceSph = mView.findViewById(R.id.edit_add_vision_refraction_undilated_right_eye_distance_sph);
            this.mEditRefractionUnDilatedRightDistanceCyl = mView.findViewById(R.id.edit_add_vision_refraction_undilated_right_eye_distance_cyl);
            this.mEditRefractionUnDilatedRightAxis = mView.findViewById(R.id.edit_add_vision_refraction_undilated_right_eye_distance_axis);
            this.mEditRefractionUnDilatedRightVA = mView.findViewById(R.id.edit_add_vision_refraction_undilated_right_eye_distance_va);

            // Refraction  Undilated Right Eye Near
            this.mEditRefractionUnDilatedRightNearSph = mView.findViewById(R.id.edit_add_vision_refraction_undilated_right_eye_near_sph);
            this.mEditRefractionUnDilatedRightNearCyl = mView.findViewById(R.id.edit_add_vision_refraction_undilated_right_eye_near_cyl);
            this.mEditRefractionUnDilatedRightNearAxis = mView.findViewById(R.id.edit_add_vision_refraction_undilated_right_eye_near_axis);
            this.mEditRefractionUnDilatedRightNearVA = mView.findViewById(R.id.edit_add_vision_refraction_undilated_right_eye_near_va);

            // Refraction  Undilated Left Eye Distance
            this.mEditRefractionUnDilatedLeftDistanceSph = mView.findViewById(R.id.edit_add_vision_refraction_undilated_left_eye_distance_sph);
            this.mEditRefractionUnDilatedLeftDistanceCyl = mView.findViewById(R.id.edit_add_vision_refraction_undilated_left_eye_distance_cyl);
            this.mEditRefractionUnDilatedLeftAxis = mView.findViewById(R.id.edit_add_vision_refraction_undilated_left_eye_distance_axis);
            this.mEditRefractionUnDilatedLeftVA = mView.findViewById(R.id.edit_add_vision_refraction_undilated_left_eye_distance_va);

            // Refraction  Undilated Right Eye Near
            this.mEditRefractionUnDilatedLeftNearSph = mView.findViewById(R.id.edit_add_vision_refraction_undilated_left_eye_near_sph);
            this.mEditRefractionUnDilatedLeftNearCyl = mView.findViewById(R.id.edit_add_vision_refraction_undilated_left_eye_near_cyl);
            this.mEditRefractionUnDilatedLeftNearAxis = mView.findViewById(R.id.edit_add_vision_refraction_undilated_left_eye_near_axis);
            this.mEditRefractionUnDilatedLeftNearVA = mView.findViewById(R.id.edit_add_vision_refraction_undilated_left_eye_near_va);

            // Refraction  dilated Right Eye Distance
            this.mEditRefractionDilatedRightDistanceSph = mView.findViewById(R.id.edit_add_vision_refraction_dilated_right_eye_distance_sph);
            this.mEditRefractionDilatedRightDistanceCyl = mView.findViewById(R.id.edit_add_vision_refraction_dilated_right_eye_distance_cyl);
            this.mEditRefractionDilatedRightAxis = mView.findViewById(R.id.edit_add_vision_refraction_dilated_right_eye_distance_axis);
            this.mEditRefractionDilatedRightVA = mView.findViewById(R.id.edit_add_vision_refraction_dilated_right_eye_distance_va);

            // Refraction  dilated Right Eye Near
            this.mEditRefractionDilatedRightNearSph = mView.findViewById(R.id.edit_add_vision_refraction_dilated_right_eye_near_sph);
            this.mEditRefractionDilatedRightNearCyl = mView.findViewById(R.id.edit_add_vision_refraction_dilated_right_eye_near_cyl);
            this.mEditRefractionDilatedRightNearAxis = mView.findViewById(R.id.edit_add_vision_refraction_dilated_right_eye_near_axis);
            this.mEditRefractionDilatedRightNearVA = mView.findViewById(R.id.edit_add_vision_refraction_dilated_right_eye_near_va);

            // Refraction  Dilated Left Eye Distance
            this.mEditRefractionDilatedLeftDistanceSph = mView.findViewById(R.id.edit_add_vision_refraction_dilated_left_eye_distance_sph);
            this.mEditRefractionDilatedLeftDistanceCyl = mView.findViewById(R.id.edit_add_vision_refraction_dilated_left_eye_distance_cyl);
            this.mEditRefractionDilatedLeftAxis = mView.findViewById(R.id.edit_add_vision_refraction_dilated_left_eye_distance_axis);
            this.mEditRefractionDilatedLeftVA = mView.findViewById(R.id.edit_add_vision_refraction_dilated_left_eye_distance_va);

            // Refraction  Dilated Right Eye Near
            this.mEditRefractionDilatedLeftNearSph = mView.findViewById(R.id.edit_add_vision_refraction_dilated_left_eye_near_sph);
            this.mEditRefractionDilatedLeftNearCyl = mView.findViewById(R.id.edit_add_vision_refraction_dilated_left_eye_near_cyl);
            this.mEditRefractionDilatedLeftNearAxis = mView.findViewById(R.id.edit_add_vision_refraction_dilated_left_eye_near_axis);
            this.mEditRefractionDilatedLeftNearVA = mView.findViewById(R.id.edit_add_vision_refraction_dilated_left_eye_near_va);

            // Refraction Final Right Eye Distance
            this.mEditRefractionFinalRightDistanceSph = mView.findViewById(R.id.edit_add_vision_refraction_final_right_eye_distance_sph);
            this.mEditRefractionFinalRightDistanceCyl = mView.findViewById(R.id.edit_add_vision_refraction_final_right_eye_distance_cyl);
            this.mEditRefractionFinalRightAxis = mView.findViewById(R.id.edit_add_vision_refraction_final_right_eye_distance_axis);
            this.mEditRefractionFinalRightVA = mView.findViewById(R.id.edit_add_vision_refraction_final_right_eye_distance_va);

            // Refraction  Final Right Eye Near
            this.mEditRefractionFinalRightNearSph = mView.findViewById(R.id.edit_add_vision_refraction_final_right_eye_near_sph);
            this.mEditRefractionFinalRightNearCyl = mView.findViewById(R.id.edit_add_vision_refraction_final_right_eye_near_cyl);
            this.mEditRefractionFinalRightNearAxis = mView.findViewById(R.id.edit_add_vision_refraction_final_right_eye_near_axis);
            this.mEditRefractionFinalRightNearVA = mView.findViewById(R.id.edit_add_vision_refraction_final_right_eye_near_va);

            // Refraction  Final Left Eye Distance
            this.mEditRefractionFinalLeftDistanceSph = mView.findViewById(R.id.edit_add_vision_refraction_final_left_eye_distance_sph);
            this.mEditRefractionFinalLeftDistanceCyl = mView.findViewById(R.id.edit_add_vision_refraction_final_left_eye_distance_cyl);
            this.mEditRefractionFinalLeftAxis = mView.findViewById(R.id.edit_add_vision_refraction_final_left_eye_distance_axis);
            this.mEditRefractionFinalLeftVA = mView.findViewById(R.id.edit_add_vision_refraction_final_left_eye_distance_va);

            // Refraction  Final Right Eye Near
            this.mEditRefractionFinalLeftNearSph = mView.findViewById(R.id.edit_add_vision_refraction_final_left_eye_near_sph);
            this.mEditRefractionFinalLeftNearCyl = mView.findViewById(R.id.edit_add_vision_refraction_final_left_eye_near_cyl);
            this.mEditRefractionFinalLeftNearAxis = mView.findViewById(R.id.edit_add_vision_refraction_final_left_eye_near_axis);
            this.mEditRefractionFinalLeftNearVA = mView.findViewById(R.id.edit_add_vision_refraction_final_left_eye_near_va);

            //RefractionIPD
            this.mTextRefrectionIPD = mView.findViewById(R.id.text_row_history_vision_item_ipd);

            // Keratometry
            this.mEditKeratometryKOneRightPower = mView.findViewById(R.id.edit_add_vision_keratometry_kone_re_power);
            this.mEditKeratometryKOneRightAxis = mView.findViewById(R.id.edit_add_vision_keratometry_kone_re_axis);
            this.mEditKeratometryKOneLeftPower = mView.findViewById(R.id.edit_add_vision_keratometry_kone_le_power);
            this.mEditKeratometryKOneLeftAxis = mView.findViewById(R.id.edit_add_vision_keratometry_kone_le_axis);

            this.mEditKeratometryKTwoRightPower = mView.findViewById(R.id.edit_add_vision_keratometry_ktwo_re_power);
            this.mEditKeratometryKTwoRightAxis = mView.findViewById(R.id.edit_add_vision_keratometry_ktwo_re_axis);
            this.mEditKeratometryKTwoLeftPower = mView.findViewById(R.id.edit_add_vision_keratometry_ktwo_le_power);
            this.mEditKeratometryKTwoLeftAxis = mView.findViewById(R.id.edit_add_vision_keratometry_ktwo_le_axis);

            this.mImagePrint = mView.findViewById(R.id.image_vision_fianl_print);

         /*   this.mEditUCVARemarks=mView.findViewById(R.id.edit_text_add_vision_ucva_remarks);
            this.mTextViewBCVARemarks=mView.findViewById(R.id.edit_text_add_vision_bcva_remarks);*/
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Common.insertLog("ExpandableLayout State : " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                mRecyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }

        /**
         * Binds the expandable view
         */
        public void bind() {
            int position = getAdapterPosition();
            boolean isSelected = position == selectedItem;
        }
    }

}
