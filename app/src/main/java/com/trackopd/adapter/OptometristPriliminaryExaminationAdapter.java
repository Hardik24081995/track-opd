package com.trackopd.adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.fragments.AddMedicationFragment;
import com.trackopd.fragments.AddPreliminaryExaminationFragment;
import com.trackopd.fragments.MedicationDetailsFragment;
import com.trackopd.fragments.PreliminaryHistoryComplaintsFragment;
import com.trackopd.model.PreliminaryExaminationModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.io.File;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OptometristPriliminaryExaminationAdapter extends RecyclerView.Adapter {

    private Activity mActivity;
    private ArrayList<PreliminaryExaminationModel> mArrPreliminaryExamination;
    private static final int unselectedItem = -1;
    private int selectedItem = unselectedItem;
    private String mUserType;

    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;

    public OptometristPriliminaryExaminationAdapter(Activity mActivity, String mUserType, RecyclerView mRecyclerView, ArrayList<PreliminaryExaminationModel> mArrCouncillorPackage) {
        this.mActivity = mActivity;
        this.mUserType = mUserType;
        this.mRecyclerView = mRecyclerView;
        this.mArrPreliminaryExamination = mArrCouncillorPackage;
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;

        setOnScrollListener();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_optometrist_preliminary_examination_item, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (holder instanceof CustomViewHolder) {
            final PreliminaryExaminationModel preliminaryExaminationModel = mArrPreliminaryExamination.get(position);
            ((CustomViewHolder) holder).mLinearExpandableArrow.setTag(position);

            ((CustomViewHolder) holder).mRelativeImageBorder.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.user_image_border));

            ((CustomViewHolder) holder).mImageUserProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, preliminaryExaminationModel.getPatientFirstName(),
                    preliminaryExaminationModel.getPatientLastName()));

            String mPatientName = preliminaryExaminationModel.getPatientFirstName() + AppConstants.STR_EMPTY_SPACE + preliminaryExaminationModel.getPatientLastName();
            String mDoctorName = preliminaryExaminationModel.getDoctorFirstName() + AppConstants.STR_EMPTY_SPACE + preliminaryExaminationModel.getDoctorLastName();
            ((CustomViewHolder) holder).mTextName.setText(Common.isEmptyString(mActivity, mPatientName));
            ((CustomViewHolder) holder).mTextMobileNo.setText(Common.isEmptyString(mActivity, preliminaryExaminationModel.getPatientMobileNo()));
            ((CustomViewHolder) holder).mTextAppointmentNo.setText(Common.isEmptyString(mActivity, preliminaryExaminationModel.getTicketNumber()));
            ((CustomViewHolder) holder).mTextAppointmentDate.setText(Common.isEmptyString(mActivity, preliminaryExaminationModel.getAppointmentDate()));
            ((CustomViewHolder) holder).mTextPatientCode.setText(Common.isEmptyString(mActivity, preliminaryExaminationModel.getPatientCode()));
            ((CustomViewHolder) holder).mTextTreatmentDate.setText(Common.isEmptyString(mActivity, preliminaryExaminationModel.getTreatmentDate()));
            ((CustomViewHolder) holder).mTextDoctorName.setText(Common.isEmptyString(mActivity, mDoctorName));
           // ((CustomViewHolder) holder).mTextRemarks.setText(Common.isEmptyString(mActivity, preliminaryExaminationModel.getRemark()));

            if (preliminaryExaminationModel.getMedication().size() == 0) {
                ((CustomViewHolder) holder).mImageMedicationDetails.setVisibility(View.GONE);
            } else {
                ((CustomViewHolder) holder).mImageMedicationDetails.setVisibility(View.VISIBLE);
            }

            /*if(preliminaryExaminationModel.getDocument().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                ((CustomViewHolder) holder).mImageMedicationPDF.setVisibility(View.GONE);
                ((CustomViewHolder) holder).mImageMedicationDetails.setVisibility(View.GONE);

                if(mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist)) ||
                        mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                    ((CustomViewHolder) holder).mImageAddMedication.setVisibility(View.VISIBLE);
                    ((CustomViewHolder) holder).mImageAddMedication.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_add_icon));
                    ((CustomViewHolder) holder).mImageAddMedication.setColorFilter(Common.setThemeColor(mActivity));
                } else {
                    ((CustomViewHolder) holder).mImageAddMedication.setVisibility(View.GONE);
                }
            } else {
                ((CustomViewHolder) holder).mImageMedicationPDF.setVisibility(View.VISIBLE);
                ((CustomViewHolder) holder).mImageMedicationDetails.setVisibility(View.VISIBLE);
                ((CustomViewHolder) holder).mImageAddMedication.setVisibility(View.GONE);
            }*/

            // ToDo: Reschedule Click Listener
            ((CustomViewHolder) holder).mLinearFullView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callToAddPreliminaryExaminationFragment(preliminaryExaminationModel);
                }
            });

            // ToDo: Image PDF Download Click Listener
            ((CustomViewHolder) holder).mImageAddMedication.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    redirectedToAddMedications(preliminaryExaminationModel);
                }
            });

            // ToDo: Image PDF Download Click Listener
            ((CustomViewHolder) holder).mImageMedicationPDF.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*if (!preliminaryExaminationModel.getDocument().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                        ((CustomViewHolder) holder).mImageMedicationPDF.setVisibility(View.VISIBLE);
                        downloadDocument(preliminaryExaminationModel);
                    } else {
                        ((CustomViewHolder) holder).mImageMedicationPDF.setVisibility(View.GONE);
                    }*/
                }
            });

            // ToDo: Image Prescription Detail Click Listener
            ((CustomViewHolder) holder).mImageMedicationDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callToMedicationDetailsFragment(preliminaryExaminationModel);
                }
            });

            int pos = holder.getAdapterPosition();
            if (pos != selectedItem) {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                ((CustomViewHolder) holder).mExpandableLayout.collapse();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
            } else {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                ((CustomViewHolder) holder).mExpandableLayout.expand();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
            }

            // ToDo: Expandable Arrow Click Listener
            ((CustomViewHolder) holder).bind();
            ((CustomViewHolder) holder).mLinearExpandableArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                    ((CustomViewHolder) holder).mExpandableLayout.collapse();
                    ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));

                    Common.insertLog("Position:::> " + position);
                    Common.insertLog("selectedItem:::> " + selectedItem);

                    int pos = holder.getAdapterPosition();
                    if (pos == selectedItem) {
                        selectedItem = unselectedItem;
                        notifyItemChanged(holder.getAdapterPosition());
                    } else {
                        ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                        ((CustomViewHolder) holder).mExpandableLayout.expand();
                        ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        selectedItem = pos;
                        notifyDataSetChanged();
                    }
                }
            });
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    /**
     * This method should check the permissions first and then able to download the document
     *
     * @param preliminaryExaminationModel - Optometrist Preliminary Examination Model
     */
    public void downloadDocument(PreliminaryExaminationModel preliminaryExaminationModel) {
        try {
            if (checkPermission()) {
               /* File mDownloadedFile = Common.checkIsFileDownloaded(mActivity, mActivity.getResources().getString(R.string.text_medication), preliminaryExaminationModel.getDocument());
                if (!(mDownloadedFile.exists())) {
                    callToDownloadPrescriptionsAPI(preliminaryExaminationModel);
                } else {
                   // Common.openDocument(mActivity, mDownloadedFile, preliminaryExaminationModel.getDocument());
                }*/
            } else {
                requestPermission();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the API to download medications pdf file and store into your local database
     */
    private void callToDownloadPrescriptionsAPI(PreliminaryExaminationModel preliminaryExaminationModel) {
        try {
           /* String mUrl = WebFields.API_BASE_URL + WebFields.PDF_URL + preliminaryExaminationModel.getDocument();

            Call<ResponseBody> call = RetrofitClient.createService(ApiInterface.class).downloadFile(mUrl);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    try {
                        if (response.isSuccessful()) {
                            boolean isDownloaded = Common.saveFileToStorage(mActivity, mActivity.getResources().getString(R.string.text_medication), response.body(), preliminaryExaminationModel.getDocument());

                            // ToDo: Checks if document is downloaded or not, if downloaded then it will open the document
                            File mDownloadedFile = Common.checkIsFileDownloaded(mActivity, mActivity.getResources().getString(R.string.nav_menu_prescriptions), preliminaryExaminationModel.getDocument());
                            Common.openDocument(mActivity, mDownloadedFile, preliminaryExaminationModel.getDocument());
                            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.val_prescription_downloaded_successfully));
                            Common.insertLog("Medication downloaded::::> " + isDownloaded);

                            // ToDo: Bind the notification
                            *//*File mFile = new File(Environment.getExternalStorageDirectory() + "/"
                                    + Common.getFileSdcardPath(mActivity, mActivity.getResources().getString(R.string.nav_menu_prescriptions)));
                            if (!mFile.exists()) {
                                mFile.mkdirs();
                            }

                            File file = new File(mFile, patientPrescriptionModel.getDocument());
                            String mFilePath = file.toString();
                            Common.bindNotification(mActivity, mFilePath, file, patientPrescriptionModel.getDocument());*//*
                        } else {
                            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.error_server));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                }
            });*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should check the custom permissions
     */
    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(mActivity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * This method should request the custom permissions
     */
    private void requestPermission() {
        try {
            int PERMISSION_REQUEST_CODE = 1;
            ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method redirected you to the Add Medications Fragment
     */
    private void redirectedToAddMedications(PreliminaryExaminationModel preliminaryExaminationModel) {
        try {
            String mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

            Fragment fragment = new AddMedicationFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, preliminaryExaminationModel.getPatientFirstName());
            args.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, preliminaryExaminationModel.getPatientLastName());
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, preliminaryExaminationModel.getPatientMobileNo());
            args.putString(AppConstants.BUNDLE_PATIENT_ID, preliminaryExaminationModel.getPatientID());
            args.putSerializable(AppConstants.BUNDLE_PATIENT_ARRAY, preliminaryExaminationModel);
            if (preliminaryExaminationModel.getAppointmentID() != null) {
                args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, preliminaryExaminationModel.getAppointmentID());
            }
            fragment.setArguments(args);
            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.text_medication));
                fragmentManager =
                        ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.text_medication));
                fragmentManager =
                        ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.text_medication));
                fragmentManager =
                        ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.text_medication));
                fragmentManager =
                        ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_medication))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_medication))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Preliminary Examination Fragment
     *
     * @param preliminaryExaminationModel - Preliminary Examination Model
     */
    private void callToMedicationDetailsFragment(
            final PreliminaryExaminationModel preliminaryExaminationModel) {
        try {
            String mPatientName = preliminaryExaminationModel.getPatientFirstName() + AppConstants.STR_EMPTY_SPACE + preliminaryExaminationModel.getPatientLastName();
            String mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

            Fragment fragment = new MedicationDetailsFragment();
            Bundle args = new Bundle();
            args.putSerializable(AppConstants.BUNDLE_PATIENT_ARRAY, preliminaryExaminationModel);
            args.putSerializable(AppConstants.BUNDLE_LOGIN_TYPE, mUserType);
            fragment.setArguments(args);
            FragmentManager fragmentManager = null;

            if (mUserType.equals(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mPatientName);
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mPatientName);
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mPatientName);
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mPatientName);
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_prescription_details))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_prescription_details))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Receptionist Add New Appointment Fragment
     *
     * @param preliminaryExaminationModel - Optometrist Preliminary Examination Model
     */
    private void callToAddPreliminaryExaminationFragment(PreliminaryExaminationModel
                                                                 preliminaryExaminationModel) {
        try {
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_preliminary_examination));
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_preliminary_examination));
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_preliminary_examination));
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_preliminary_examination));
            }

           // Fragment fragment = new AddPreliminaryExaminationFragment();
            Fragment fragment =new AddPreliminaryExaminationFragment();

            Bundle args = new Bundle();
            args.putSerializable(AppConstants.BUNDLE_PATIENT_ARRAY, preliminaryExaminationModel);
            fragment.setArguments(args);
            FragmentManager fragmentManager = null;

            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_preliminary_examination))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_preliminary_examination))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mArrPreliminaryExamination == null ? 0 : mArrPreliminaryExamination.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrPreliminaryExamination.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder implements ExpandableLayout.OnExpansionUpdateListener {
        RelativeLayout mRelativeImageBorder;
        ImageView mImageUserProfilePic, mImageExpandableArrow, mImageAddMedication, mImageMedicationPDF, mImageMedicationDetails;
        LinearLayout mLinearExpandableArrow, mLinearFullView;
        ExpandableLayout mExpandableLayout;
        TextView mTextName, mTextMobileNo, mTextAppointmentNo, mTextAppointmentDate, mTextPatientCode, mTextTreatmentDate, mTextDoctorName, mTextRemarks;

        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Relative Layouts
            this.mRelativeImageBorder = itemView.findViewById(R.id.relative_row_optometrist_preliminary_examination_patient_profile_pic);

            // Image Views
            this.mImageExpandableArrow = itemView.findViewById(R.id.image_row_optometrist_preliminary_examination_expandable_view);
            this.mImageUserProfilePic = itemView.findViewById(R.id.image_row_optometrist_preliminary_examination_patient_profile_pic);
            this.mImageAddMedication = itemView.findViewById(R.id.image_row_optometrist_preliminary_examination_add_medication);
            this.mImageMedicationPDF = itemView.findViewById(R.id.image_row_optometrist_preliminary_examination_medication_pdf);
            this.mImageMedicationDetails = itemView.findViewById(R.id.image_row_optometrist_preliminary_examination_details);

            // Linear Layouts
            this.mLinearExpandableArrow = itemView.findViewById(R.id.linear_row_optometrist_preliminary_examination_expandable_view);
            this.mLinearFullView = itemView.findViewById(R.id.linear_row_optometrist_preliminary_examination_full_view);

            // Expandable Layout
            this.mExpandableLayout = itemView.findViewById(R.id.expandable_layout_row_optometrist_preliminary_examination);

            // Text Views
            this.mTextName = itemView.findViewById(R.id.text_row_optometrist_preliminary_examination_patient_name);
            this.mTextMobileNo = itemView.findViewById(R.id.text_row_optometrist_preliminary_examination_patient_mobile_no);
            this.mTextAppointmentNo = itemView.findViewById(R.id.text_row_optometrist_preliminary_examination_appointment_number);
            this.mTextAppointmentDate = itemView.findViewById(R.id.text_row_optometrist_preliminary_examination_appointment_date);
            this.mTextPatientCode = itemView.findViewById(R.id.text_row_optometrist_preliminary_examination_patient_code);
            this.mTextTreatmentDate = itemView.findViewById(R.id.text_row_optometrist_preliminary_examination_treatment_date);
            this.mTextDoctorName = itemView.findViewById(R.id.text_row_optometrist_preliminary_examination_doctor_name);
            this.mTextRemarks = itemView.findViewById(R.id.text_row_optometrist_preliminary_examination_remarks);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Common.insertLog("ExpandableLayout State : " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                mRecyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }

        /**
         * Binds the expandable view
         */
        public void bind() {
            int position = getAdapterPosition();
            boolean isSelected = position == selectedItem;

            mLinearExpandableArrow.setSelected(isSelected);
            mExpandableLayout.setExpanded(isSelected, false);
        }
    }
}
