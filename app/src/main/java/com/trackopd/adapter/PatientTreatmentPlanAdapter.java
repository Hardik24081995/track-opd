package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.model.PatientPrescriptionModel;
import com.trackopd.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

public class PatientTreatmentPlanAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity mActivity;
    private ArrayList<PatientPrescriptionModel.Medication> mArrPatientPrescriptionModel;

    public PatientTreatmentPlanAdapter(Activity mActivity, ArrayList<PatientPrescriptionModel.Medication> mArrPatientTreatment) {
        this.mActivity = mActivity;
        this.mArrPatientPrescriptionModel = mArrPatientTreatment;
        LayoutInflater mLayoutInflater = LayoutInflater.from(mActivity);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(mActivity).inflate(R.layout.row_patient_treatment_plan_items, viewGroup,
                false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final PatientPrescriptionModel.Medication patientTreatmentPlanModel = mArrPatientPrescriptionModel.get(position);

        ((CustomViewHolder) holder).mTextTreatmentName.setText(patientTreatmentPlanModel.getMedicationName());

        List<PatientPrescriptionModel.Dosage> medicine = patientTreatmentPlanModel.getDosage();
        if (((CustomViewHolder) holder).mLinearTreatmentPlan.getChildCount() > 0) {
            ((CustomViewHolder) holder).mLinearTreatmentPlan.removeAllViews();
        }

        for (int i = 0; i < medicine.size(); i++) {
            LayoutInflater layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            final View view = layoutInflater.inflate(R.layout.row_patient_treatment_plan_patient_medicine_item, ((CustomViewHolder) holder).mLinearTreatmentPlan, false);

            // Text Views
            TextView mTextDosageHeader = view.findViewById(R.id.text_row_patient_treatment_plan_medicine_dosage_header);
            TextView mTexDosage = view.findViewById(R.id.text_row_patient_treatment_plan_medicine_dosage);

            // Sets up the data
            mTextDosageHeader.setText(medicine.get(i).getDosage());
            mTexDosage.setText(medicine.get(i).getDescription());

            ((CustomViewHolder) holder).mLinearTreatmentPlan.addView(view);
        }
    }

    @Override
    public int getItemCount() {
        return mArrPatientPrescriptionModel.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView mTextTreatmentName, mTextTreatmentDate;
        LinearLayout mLinearTreatmentPlan;

        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);

            // Text Views
            this.mTextTreatmentName = itemView.findViewById(R.id.text_row_patient_treatment_plan_name);
            this.mTextTreatmentDate = itemView.findViewById(R.id.text_row_patient_treatment_plan_date);

            // Linear Layout
            this.mLinearTreatmentPlan = itemView.findViewById(R.id.linear_row_patient_treatment_plan);
        }
    }
}
