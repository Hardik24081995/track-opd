package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import java.util.ArrayList;

public class AddPreliminaryExaminationFamilyOcularHistoryAdapter extends RecyclerView.Adapter {

    private Activity mActivity;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.FamilyOcularHistory> mArrFamilyOcularHistory;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.FamilyOcularHistory> mArrSelectedFamilyOcularHistory;
    private ArrayList<String> mFamilyOcularHistory = new ArrayList<>();
    private String mSelectedValue, mUserType;
    private static SingleClickListener sClickListener;

    public AddPreliminaryExaminationFamilyOcularHistoryAdapter(Activity mActivity, String mUserType, String selectedValue, ArrayList<PreliminaryExaminationDetailsModel.Data.FamilyOcularHistory> mArrFamilyOcularHistory) {
        this.mActivity = mActivity;
        this.mUserType = mUserType;
        mSelectedValue = selectedValue;
        this.mArrFamilyOcularHistory = mArrFamilyOcularHistory;
        mArrSelectedFamilyOcularHistory = new ArrayList<>();
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
    }

    /**
     * Create an interface for the recycler view click events
     */
    public void setOnItemClickListener(SingleClickListener clickListener) {
        sClickListener = clickListener;
    }

    /**
     * Set Update
     * @param mArrFamilyOcularHistory  new ArrayList
     */
    public void update(ArrayList<PreliminaryExaminationDetailsModel.Data.FamilyOcularHistory> mArrFamilyOcularHistory) {
       this.mArrFamilyOcularHistory=mArrFamilyOcularHistory;
    }

    public interface SingleClickListener {
        void onFamilyOcularHistoryCheckBoxClickListener(String mFinalFamilyOcularHistory);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .row_add_preliminary_examination_anatomical_location_item, parent, false);
        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final PreliminaryExaminationDetailsModel.Data.FamilyOcularHistory familyOcularHistoryModel = mArrFamilyOcularHistory.get(position);

        ((CustomViewHolder) holder).mTextTitle.setSelected(true);
        ((CustomViewHolder) holder).mTextTitle.setText(Common.isEmptyString(mActivity, familyOcularHistoryModel.getFamilyOcularHistory()));

        if (!mSelectedValue.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            mFamilyOcularHistory = new ArrayList<>();
            String[] mSplitValue = mSelectedValue.split(",");
            for (int j = 0; j < mSplitValue.length; j++) {
                String mSelected = mSplitValue[j];
                mFamilyOcularHistory.add(mSelected);
                if (mSelected.equals(familyOcularHistoryModel.getFamilyOcularHistory())) {
                    ((CustomViewHolder) holder).mCheckBox.setChecked(true);
                }
            }
        }

        if(mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist)) || mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
            ((CustomViewHolder) holder).mCheckBox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    if (cb.isChecked()) {
                        mArrSelectedFamilyOcularHistory.clear();
                        mArrSelectedFamilyOcularHistory.add(familyOcularHistoryModel);
                        for (int j = 0; j < mArrSelectedFamilyOcularHistory.size(); j++) {
                            String mFamilyOcularHistoryName = mArrSelectedFamilyOcularHistory.get(j).getFamilyOcularHistory();
                            mFamilyOcularHistory.add(mFamilyOcularHistoryName);
                        }
                    } else {
                        mFamilyOcularHistory.remove(familyOcularHistoryModel.getFamilyOcularHistory());
                        mArrSelectedFamilyOcularHistory.remove(familyOcularHistoryModel);
                    }
                    String mFinalFamilyOcularHistory = mFamilyOcularHistory.toString().replaceAll("\\[", "").replaceAll("\\]", "").replace(", ", ",");
                    sClickListener.onFamilyOcularHistoryCheckBoxClickListener(mFinalFamilyOcularHistory);
                    Common.insertLog("Sel Family Ocular History:::> " + mFinalFamilyOcularHistory);
                }
            });
        } else {
            ((CustomViewHolder) holder).mCheckBox.setClickable(false);
        }
    }

    @Override
    public int getItemCount() {
        return mArrFamilyOcularHistory.size();
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder {

        CheckBox mCheckBox;
        TextView mTextTitle;

        public CustomViewHolder(final View itemView) {
            super(itemView);
            this.setIsRecyclable(false);

            // Check Box
            this.mCheckBox = itemView.findViewById(R.id.checkbox_row_add_preliminary_examination_anatomical_location);

            // Text Views
            this.mTextTitle = itemView.findViewById(R.id.text_row_add_preliminary_examination_anatomical_location);
        }
    }
}
