package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.ReceptionistAppointmentDetailProcessModel;
import com.trackopd.utils.AppConstants;

import java.util.ArrayList;

public class ReceptionistAppointmentDetailProcessAdapter extends RecyclerView.Adapter {

    private Activity mActivity;
    private ArrayList<ReceptionistAppointmentDetailProcessModel.Data> mAppointmentProcess;

    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;

    /**
     * Adapter contains the data to be displayed
     */
    public ReceptionistAppointmentDetailProcessAdapter(Activity mActivity, RecyclerView mRecyclerView, ArrayList<ReceptionistAppointmentDetailProcessModel.Data> mAppointmentProcess) {
        this.mActivity = mActivity;
        this.mRecyclerView = mRecyclerView;
        this.mAppointmentProcess = mAppointmentProcess;
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;

        setOnScrollListener();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_receptionist_patient_detail_process_item, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof CustomViewHolder) {
            final ReceptionistAppointmentDetailProcessModel.Data processModel = mAppointmentProcess.get(position);
            ((CustomViewHolder) holder).mTextName.setText(processModel.getFirstName() + AppConstants.STR_EMPTY_SPACE + processModel.getLastName());
            ((CustomViewHolder) holder).mTextDate.setText(processModel.getProcessDateTime());
            ((CustomViewHolder) holder).mTextDescription.setText(processModel.getDescription());
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return mAppointmentProcess == null ? 0 : mAppointmentProcess.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mAppointmentProcess.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView mTextName, mTextDate, mTextDescription;

        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Text Views
            this.mTextName = itemView.findViewById(R.id.text_row_receptionist_patient_detail_process_name);
            this.mTextDate = itemView.findViewById(R.id.text_row_receptionist_patient_detail_process_date_time);
            this.mTextDescription = itemView.findViewById(R.id.text_row_receptionist_patient_detail_process_description);
        }
    }
}
