package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import java.util.ArrayList;

public class AddPreliminaryExaminationSystemicHistoryAdapter extends RecyclerView.Adapter {

    private Activity mActivity;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.SystemicHistory> mArrSystemicHistory;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.SystemicHistory> mArrSelectedSystemicHistory;
    private ArrayList<String> mSystemicHistory = new ArrayList<>();
    private String mSelectedValue, mUserType;
    private static SingleClickListener sClickListener;

    public AddPreliminaryExaminationSystemicHistoryAdapter(Activity mActivity, String mUserType, String selectedValue, ArrayList<PreliminaryExaminationDetailsModel.Data.SystemicHistory> mArrSystemicHistory) {
        this.mActivity = mActivity;
        this.mUserType = mUserType;
        mSelectedValue = selectedValue;
        this.mArrSystemicHistory = mArrSystemicHistory;
        mArrSelectedSystemicHistory = new ArrayList<>();
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
    }

    /**
     * Create an interface for the recycler view click events
     */
    public void setOnItemClickListener(SingleClickListener clickListener) {
        sClickListener = clickListener;
    }

    /**
     * Update Systemic History List
     * @param mArrSystemicHistory -Array List
     */
    public void updateList(ArrayList<PreliminaryExaminationDetailsModel.Data.SystemicHistory> mArrSystemicHistory) {
       this.mArrSystemicHistory=mArrSystemicHistory;
       notifyDataSetChanged();
    }

    public interface SingleClickListener {
        void onSystemicHistoryCheckBoxClickListener(String mSystemicHistory);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .row_add_preliminary_examination_anatomical_location_item, parent, false);
        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final PreliminaryExaminationDetailsModel.Data.SystemicHistory systemicHistoryModel = mArrSystemicHistory.get(position);

        ((CustomViewHolder) holder).mCheckBox.setTag(position);

        ((CustomViewHolder) holder).mTextTitle.setSelected(true);
        ((CustomViewHolder) holder).mTextTitle.setText(Common.isEmptyString(mActivity, systemicHistoryModel.getSystemicHistory()));

        /*String[] mSplitValue = mSelectedValue.split(",");
        for (int j = 0; j < mSplitValue.length; j++) {
            if (mSplitValue[j].equals(systemicHistoryModel.getSystemicHistory())) {
                ((CustomViewHolder) holder).mCheckBox.setChecked(true);
            }
        }*/

        if (!mSelectedValue.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            mSystemicHistory = new ArrayList<>();
            String[] mSplitValue = mSelectedValue.split(",");
            for (int j = 0; j < mSplitValue.length; j++) {
                String mSelected = mSplitValue[j];
                mSystemicHistory.add(mSelected);
                if (mSelected.equals(systemicHistoryModel.getSystemicHistory())) {
                    ((CustomViewHolder) holder).mCheckBox.setChecked(true);
                }
            }
        }

        if(mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist)) || mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
            ((CustomViewHolder) holder).mCheckBox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    if (cb.isChecked()) {
                        mArrSelectedSystemicHistory.clear();
                        mArrSelectedSystemicHistory.add(systemicHistoryModel);
                        for (int j = 0; j < mArrSelectedSystemicHistory.size(); j++) {
                            String mSystemicHistoryName = mArrSelectedSystemicHistory.get(j).getSystemicHistory();
                            mSystemicHistory.add(mSystemicHistoryName);
                        }
                    } else {
                        mSystemicHistory.remove(systemicHistoryModel.getSystemicHistory());
                        mArrSelectedSystemicHistory.remove(systemicHistoryModel);
                    }
                    String mFinalSystemicHistory = mSystemicHistory.toString().replaceAll("\\[", "").replaceAll("\\]", "").replace(", ", ",");
                    sClickListener.onSystemicHistoryCheckBoxClickListener(mFinalSystemicHistory);
                    Common.insertLog("Sel Systemic History:::> " + mFinalSystemicHistory);
                }
            });
        } else {
            ((CustomViewHolder) holder).mCheckBox.setClickable(false);
        }
    }

    @Override
    public int getItemCount() {
        return mArrSystemicHistory.size();
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder {

        CheckBox mCheckBox;
        TextView mTextTitle;

        public CustomViewHolder(final View itemView) {
            super(itemView);
            this.setIsRecyclable(false);

            // Check Box
            this.mCheckBox = itemView.findViewById(R.id.checkbox_row_add_preliminary_examination_anatomical_location);

            // Text Views
            this.mTextTitle = itemView.findViewById(R.id.text_row_add_preliminary_examination_anatomical_location);
        }
    }
}
