package com.trackopd.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.trackopd.fragments.DiagnosisFragment;
import com.trackopd.fragments.ReceptionistPatientDetailPrelimimaryExaminationFragment;
import com.trackopd.utils.AppConstants;

import java.util.ArrayList;
import java.util.List;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private List<Fragment> mArrFragmentsList = new ArrayList<>();
    private List<String> mArrFragmentsTitleList = new ArrayList<>();

    public void addFragment(Fragment fragment, String title, Bundle mBundle) {
        fragment.setArguments(mBundle);
        mArrFragmentsList.add(fragment);
        mArrFragmentsTitleList.add(title);
    }

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return mArrFragmentsList.get(position);
    }

    @Override
    public int getCount() {
        return mArrFragmentsList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mArrFragmentsTitleList.get(position);
    }

    public void addFragment(Fragment fragment, String title) {
        mArrFragmentsList.add(fragment);
        mArrFragmentsTitleList.add(title);
    }

    public void addFragment(Fragment fragment,
                            String title, Bundle mBundle, String type)
    {

        mBundle.putString(AppConstants.STR_TITLE,type);
        fragment.setArguments(mBundle);
        mArrFragmentsList.add(fragment);
        mArrFragmentsTitleList.add(title);
    }
}