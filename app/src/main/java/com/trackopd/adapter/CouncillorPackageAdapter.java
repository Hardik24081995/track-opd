package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.CouncillorPackageModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;
import java.util.List;

public class CouncillorPackageAdapter extends RecyclerView.Adapter {

    private Activity mActivity;
    private ArrayList<CouncillorPackageModel> mArrCouncillorPackageModel;
    private static final int unselectedItem = -1;
    private int selectedItem = unselectedItem;

    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;



    public CouncillorPackageAdapter(Activity mActivity, RecyclerView mRecyclerView, ArrayList<CouncillorPackageModel> mArrCouncillorPackage) {
        this.mActivity = mActivity;
        this.mRecyclerView = mRecyclerView;
        this.mArrCouncillorPackageModel = mArrCouncillorPackage;
        LayoutInflater mLayoutInflater = LayoutInflater.from(mActivity);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;

        setOnScrollListener();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_councillor_package_item, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (holder instanceof CustomViewHolder) {
            final CouncillorPackageModel councillorPackageModel = mArrCouncillorPackageModel.get(position);
            ((CustomViewHolder) holder).mLinearExpandableArrow.setTag(position);

            ((CustomViewHolder) holder).mTextTitle.setText(Common.isEmptyString(mActivity, councillorPackageModel.getTitle()));
            ((CustomViewHolder) holder).mTextDate.setText(Common.isEmptyString(mActivity, councillorPackageModel.getPackageDate()));
            ((CustomViewHolder) holder).mTextAmount.setText(Common.isEmptyString(mActivity, councillorPackageModel.getAmount()));
            ((CustomViewHolder) holder).mTextDiscount.setText(Common.isEmptyString(mActivity, councillorPackageModel.getDiscountAmount()));


            List<CouncillorPackageModel.Item> mArrCouncillorPackageItems = councillorPackageModel.getItem();

            if (((CustomViewHolder) holder).mLinealLayoutItem.getChildCount() > 0) {
                ((CustomViewHolder) holder).mLinealLayoutItem.removeAllViews();
            }

            for (int l = 0; l < mArrCouncillorPackageItems.size(); l++) {
                LayoutInflater layoutInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                final View view = layoutInflater.inflate(R.layout.row_councillor_package_sub_item, ((CustomViewHolder) holder).mLinealLayoutItem, false);

                // Text Views
                TextView mTextSubTitle = view.findViewById(R.id.text_row_councillor_package_sub_title);
                TextView mTextAmount = view.findViewById(R.id.text_row_councillor_package_sub_amount);

                mTextSubTitle.setText(Common.isEmptyString(mActivity, mArrCouncillorPackageItems.get(l).getTitle()));
                mTextAmount.setText(Common.isEmptyString(mActivity, mArrCouncillorPackageItems.get(l).getAmount()));

                ((CustomViewHolder) holder).mLinealLayoutItem.addView(view);
            }

            int pos = holder.getAdapterPosition();
            if (pos != selectedItem) {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                ((CustomViewHolder) holder).mExpandableLayout.collapse();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
            } else {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                ((CustomViewHolder) holder).mExpandableLayout.expand();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
            }

            // ToDo: Expandable Arrow Click Listener
            ((CustomViewHolder) holder).bind();
            ((CustomViewHolder) holder).mLinearExpandableArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                    ((CustomViewHolder) holder).mExpandableLayout.collapse();
                    ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));

                    Common.insertLog("Position:::> " + position);
                    Common.insertLog("selectedItem:::> " + selectedItem);

                    int pos = holder.getAdapterPosition();
                    if (pos == selectedItem) {
                        selectedItem = unselectedItem;
                        notifyItemChanged(holder.getAdapterPosition());
                    } else {
                        ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                        ((CustomViewHolder) holder).mExpandableLayout.expand();
                        ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        selectedItem = pos;
                        notifyDataSetChanged();
                    }
                }
            });
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return mArrCouncillorPackageModel == null ? 0 : mArrCouncillorPackageModel.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrCouncillorPackageModel.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder implements ExpandableLayout.OnExpansionUpdateListener {

        ImageView mImageExpandableArrow;
        LinearLayout mLinealLayoutItem, mLinearExpandableArrow;
        ExpandableLayout mExpandableLayout;
        TextView mTextTitle, mTextDate, mTextAmount, mTextDiscount;

        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Image Views
            this.mImageExpandableArrow = itemView.findViewById(R.id.image_row_councillor_package_expandable_view);

            // Linear Layouts
            this.mLinealLayoutItem = itemView.findViewById(R.id.linear_row_councillor_package);
            this.mLinearExpandableArrow = itemView.findViewById(R.id.linear_row_councillor_package_expandable_view);

            // Expandable Layout
            this.mExpandableLayout = itemView.findViewById(R.id.expandable_layout_row_councillor_package);

            // Text Views
            mTextTitle = itemView.findViewById(R.id.text_row_councillor_package_title);
            mTextDate = itemView.findViewById(R.id.text_row_councillor_package_date);
            mTextAmount = itemView.findViewById(R.id.text_row_councillor_package_amount);
            mTextDiscount = itemView.findViewById(R.id.text_row_councillor_package_discount_amount);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Common.insertLog("ExpandableLayout State : " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                mRecyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }

        /**
         * Binds the expandable view
         */
        public void bind() {
            int position = getAdapterPosition();
            boolean isSelected = position == selectedItem;

            mLinearExpandableArrow.setSelected(isSelected);
            mExpandableLayout.setExpanded(isSelected, false);
        }
    }
}
