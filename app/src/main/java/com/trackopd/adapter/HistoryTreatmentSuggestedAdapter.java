package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.trackopd.R;
import com.trackopd.fragments.AddNewPreliminaryExaminationFragment;
import com.trackopd.fragments.CheckUpCounselingFragment;
import com.trackopd.fragments.SurgerySuggestedFragment;
import com.trackopd.fragments.TreatmentSuggestedFragment;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel.Data;
import com.trackopd.model.PrintReceiptModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryTreatmentSuggestedAdapter extends RecyclerView.Adapter {
    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    onHistoryDiagnosisListener onHistoryListener;
    private Activity mActivity;
    private RecyclerView mRecyclerView;
    private ArrayList<Data> mArrPreliminaryExamination;
    private String TYPE;

    public HistoryTreatmentSuggestedAdapter(Activity mActivity, RecyclerView recyclerView, ArrayList<Data> mArrHistory, TreatmentSuggestedFragment treatmentSuggestedFragment) {
        this.mActivity = mActivity;
        this.mRecyclerView = recyclerView;
        this.mArrPreliminaryExamination = mArrHistory;
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        onHistoryListener = treatmentSuggestedFragment;
        this.TYPE = "TreatmentSuggested";
    }

    public HistoryTreatmentSuggestedAdapter(Activity mActivity, RecyclerView recyclerView, ArrayList<Data> mArrHistory, SurgerySuggestedFragment surgerySuggestedFragment) {
        this.mActivity = mActivity;
        this.mRecyclerView = recyclerView;
        this.mArrPreliminaryExamination = mArrHistory;
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        onHistoryListener = surgerySuggestedFragment;
        this.TYPE = "SurgerySuggested";
    }

    public HistoryTreatmentSuggestedAdapter(Activity mActivity, RecyclerView recyclerView,
                                            ArrayList<Data> mArrHistory, CheckUpCounselingFragment checkUpCounselingFragment) {
        this.mActivity = mActivity;
        this.mRecyclerView = recyclerView;
        this.mArrPreliminaryExamination = mArrHistory;
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        onHistoryListener = checkUpCounselingFragment;
        this.TYPE = mActivity.getString(R.string.nav_menu_counselling);
    }



    public HistoryTreatmentSuggestedAdapter(Activity mActivity, RecyclerView recyclerView,
                                            ArrayList<Data> mArrHistory, String type) {
        this.mActivity = mActivity;
        this.mRecyclerView = recyclerView;
        this.mArrPreliminaryExamination = mArrHistory;
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        this.TYPE = type;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_history_preliminary_diagnosis_item, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        if (holder instanceof CustomViewHolder) {

            HistoryPreliminaryExaminationModel.Data checkInData = mArrPreliminaryExamination.get(position);

            if (TYPE.equals("SurgerySuggested")) {
                if (checkInData.getSurgerySuggested() != null &&
                        checkInData.getSurgerySuggested().size() > 0) {


                    ((CustomViewHolder) holder).mTextDate.setText(checkInData.getTreatmentDate());


                    ArrayList<HistoryPreliminaryExaminationModel.Medication>
                            mArrMedications = checkInData.getMedication();

                    HistoryPreliminaryExaminationModel.SurgerySuggested _surgerySuggested
                            = checkInData.getSurgerySuggested().get(0);

                    View view_container = LayoutInflater.from(mActivity.getBaseContext()).
                            inflate(R.layout.row_surgery_suggested,
                                    null, false);

                    LinearLayout mLinearContainer = view_container.findViewById(
                            R.id.linear_row_date_history_treatment_suggested_item_prescription);

                    //Set Surgery Data
                    TextView mTextCategory = view_container.findViewById(R.id.text_view_row_date_history_treatment_suggested_item_category);
                    TextView mTextSurgeryName = view_container.findViewById(R.id.text_view_row_date_history_treatment_suggested_item_surgery_name);
                    TextView mTextSurgeryTwo = view_container.findViewById(R.id.text_view_row_date_history_treatment_suggested_item_surgery_two);

                    TextView mTextSurgeryOption = view_container.findViewById(R.id.text_view_row_date_history_treatment_suggested_item_surgery_option);
                    TextView mTextEye = view_container.findViewById(R.id.text_view_row_date_history_treatment_suggested_item_eye);
                    TextView mTextRemarks = view_container.findViewById(R.id.text_view_row_date_history_treatment_suggested_item_remarks);

                    mTextCategory.setText(Common.isEmptyString(mActivity, _surgerySuggested.getCategory()));
                    mTextSurgeryName.setText(Common.isEmptyString(mActivity, _surgerySuggested.getSurgeryName()));
                    mTextSurgeryOption.setText(Common.isEmptyString(mActivity, _surgerySuggested.getDuration()));
                    mTextEye.setText(Common.isEmptyString(mActivity, _surgerySuggested.getEye()));
                    mTextRemarks.setText(Common.isEmptyString(mActivity, _surgerySuggested.getRemarks()));
                    mTextSurgeryTwo.setText(Common.isEmptyString(mActivity,_surgerySuggested.getSurgeryType2()));


                    ((CustomViewHolder) holder).mLinearContainer.addView(view_container);


                } else {
                    ((CustomViewHolder) holder).mLinearMain.setVisibility(View.GONE);
                }
            }

            if (TYPE.equals("TreatmentSuggested")) {
                if (checkInData.getSurgerySuggested() != null &&
                        checkInData.getSurgerySuggested().size() > 0) {


                    ((CustomViewHolder) holder).mTextDate.setText(checkInData.getTreatmentDate());


                    ArrayList<HistoryPreliminaryExaminationModel.Medication>
                            mArrMedications = checkInData.getMedication();

                    HistoryPreliminaryExaminationModel.SurgerySuggested _surgerySuggested
                            = checkInData.getSurgerySuggested().get(0);

                    View view_container = LayoutInflater.from(mActivity.getBaseContext()).
                            inflate(R.layout.row_date_history_treatment_suggested_tem,
                                    null, false);

                    LinearLayout mLinearContainer = view_container.findViewById(
                            R.id.linear_row_date_history_treatment_suggested_item_prescription);


                    AddPrescriptionData(mLinearContainer, mArrMedications);

                    ImageView mImagePrint = view_container.findViewById(R.id.print_treament_suggested_prescription_print);


                    if (mArrMedications.size() > 0) {
                        mImagePrint.setVisibility(View.VISIBLE);
                    } else {
                        mImagePrint.setVisibility(View.GONE);

                    }

                    //Set Surgery Data
                    TextView mTextCategory = view_container.findViewById(R.id.text_view_row_date_history_treatment_suggested_item_category);
                    TextView mTextSurgeryName = view_container.findViewById(R.id.text_view_row_date_history_treatment_suggested_item_surgery_name);
                    TextView mTextSurgeryTwo = view_container.findViewById(R.id.text_view_row_date_history_treatment_suggested_item_surgery_two);

                    TextView mTextSurgeryOption = view_container.findViewById(R.id.text_view_row_date_history_treatment_suggested_item_surgery_option);
                    TextView mTextEye = view_container.findViewById(R.id.text_view_row_date_history_treatment_suggested_item_eye);
                    TextView mTextRemarks = view_container.findViewById(R.id.text_view_row_date_history_treatment_suggested_item_remarks);

                    mTextCategory.setText(Common.isEmptyString(mActivity, _surgerySuggested.getCategory()));
                    mTextSurgeryName.setText(Common.isEmptyString(mActivity, _surgerySuggested.getSurgeryName()));
                    mTextSurgeryOption.setText(Common.isEmptyString(mActivity, _surgerySuggested.getDuration()));
                    mTextEye.setText(Common.isEmptyString(mActivity, _surgerySuggested.getEye()));
                    mTextRemarks.setText(Common.isEmptyString(mActivity, _surgerySuggested.getRemarks()));
                    mTextSurgeryTwo.setText(Common.isEmptyString(mActivity,_surgerySuggested.getSurgeryType2()));


                    ((CustomViewHolder) holder).mLinearContainer.addView(view_container);

                    //Print Prescription
                    mImagePrint.setOnClickListener(v ->
                        callAlert(checkInData.getAppointmentID()));


                } else {
                    ((CustomViewHolder) holder).mLinearMain.setVisibility(View.GONE);
                }
            }


            if (TYPE.equals(mActivity.getString(R.string.nav_menu_counselling))) {

                // Get Counselling Dialog Box
                if (checkInData.getCounselingDetails() != null &&
                        checkInData.getCounselingDetails().size() > 0) {

                    ((CustomViewHolder) holder).mTextDate.setText(checkInData.getTreatmentDate());


                    HistoryPreliminaryExaminationModel.CounselingDetail _councillor
                            = checkInData.getCounselingDetails().get(0);


                    View view_container = LayoutInflater.from(mActivity.getBaseContext()).
                            inflate(R.layout.row_history_preliminary_counseling_item,
                                    null, false);

                    TextView mTextEye = view_container.findViewById(R.id.text_row_history_counseling_item_eye);
                    TextView mTextSurgeryName = view_container.findViewById(R.id.text_row_history_counseling_item_surgery_type);
                    TextView mTextDoctor = view_container.findViewById(R.id.text_row_history_counseling_item_doctor);
                    TextView mTextCouncillor = view_container.findViewById(R.id.text_row_history_counseling_item_councillor);
                    TextView mTextDieseasExplained = view_container.findViewById(R.id.text_row_history_counseling_item_disease_explained);
                    TextView mTextSurgeryExplained = view_container.findViewById(R.id.text_row_history_counseling_item_surgery_suggested);
                    TextView mTextOption = view_container.findViewById(R.id.text_row_history_counseling_item_option_suggested);
                    TextView mTextOpted = view_container.findViewById(R.id.text_row_history_counseling_item_option_opted);
                    TextView mTextMediclaim = view_container.findViewById(R.id.text_row_history_counseling_item_mediclaim);
                    TextView mTextRemarks = view_container.findViewById(R.id.text_row_history_counseling_item_remaks);
                    TextView mTextAdmissionDate = view_container.findViewById(R.id.text_row_history_counseling_item_admission_date_time);
                    TextView mTextTwoSurgery = view_container.findViewById(R.id.text_row_history_counseling_item_surgery_two);

                    mTextEye.setText(_councillor.getCouncillorEye());
                    mTextSurgeryName.setText(_councillor.getCouncillorSurgeryName());
                    mTextTwoSurgery.setText(_councillor.getSurgeryType2());
                    mTextDoctor.setText(_councillor.getDoctorName());
                    mTextCouncillor.setText(_councillor.getCouncillorName());
                    mTextDieseasExplained.setText(_councillor.getDiseaseExplained());
                    mTextSurgeryExplained.setText(_councillor.getSurgeryExplained());
                    mTextOption.setText(_councillor.getPatientPreferredOption());
                    mTextOpted.setText(_councillor.getOpetionSuggested());
                    //swapped values
                    //getOpetionSuggested()
                    //getPatientPreferredOption()
                    mTextMediclaim.setText(_councillor.getMeclaim());
                    mTextRemarks.setText(_councillor.getCouncillorRemarks());
                    mTextAdmissionDate.setText(_councillor.getCouncillorDate() + AppConstants.STR_EMPTY_SPACE
                            + _councillor.getAdmissionTime());

                    ((CustomViewHolder) holder).mLinearContainer.addView(view_container);
                } else {
                    ((CustomViewHolder) holder).mLinearMain.setVisibility(View.GONE);
                }
            }

            ((CustomViewHolder) holder).mImageEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TYPE.equals(mActivity.getString(R.string.nav_menu_counselling))) {
                        onSelectedItem(mActivity, checkInData);
                    } else {
                        onSelectedTreatmentSuggested(mActivity, checkInData);
                    }
                }
            });

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    private void callAlert(String appointmentID) {

        AlertDialog.Builder builder = new AlertDialog.Builder(
                mActivity);
        builder.setTitle("Language Options");
        builder.setMessage("Choose your desired language");
        builder.setNegativeButton("Hindi",
                (dialog, which) -> {
                    String lang = "Hindi";
                    HistoryTreatmentSuggestedAdapter.this.callToPrintFinalVision(appointmentID,lang);

                });
        builder.setPositiveButton("Gujarati",
                (dialog, which) -> {
                    String lang = "Gujarati";
                    HistoryTreatmentSuggestedAdapter.this.callToPrintFinalVision(appointmentID,lang);
                });
        builder.show();
    }

    private void AddPrescriptionData(LinearLayout mLinearContainer, ArrayList<HistoryPreliminaryExaminationModel.Medication> mArrMedications) {

        //set Medication Data
        for (int a = 0; a < mArrMedications.size(); a++) {
            LayoutInflater layoutInflater = (LayoutInflater) mActivity.getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view_medication = layoutInflater.inflate(R.layout.row_prescription_medication_item, null, false);

            TextView mTextMedication = view_medication.findViewById(R.id.text_row_prescription_medication_item_medication);
            TextView mTextStatDate = view_medication.findViewById(R.id.text_row_prescription_medication_item_start_date);
            TextView mTextEndDate = view_medication.findViewById(R.id.text_row_prescription_medication_item_end_date);
            TextView mTextTime = view_medication.findViewById(R.id.text_row_prescription_medication_item_time);

            mTextMedication.setText(mArrMedications.get(a).getMedicationName());
            mTextStatDate.setText(mArrMedications.get(a).getStartDate());
            mTextEndDate.setText(mArrMedications.get(a).getEndDate());

            //String days=mArrMedications.get(a).

            String strTime = "";
            if (!mArrMedications.get(a).getTotalTimes().equalsIgnoreCase("")) {
                int medication = Integer.parseInt(mArrMedications.get(a).getTotalTimes());

                for (int m = 0; m < medication; m++) {

                    if (strTime.equalsIgnoreCase("")) {
                        strTime = "1";
                    } else {
                        strTime = strTime + "-1";
                    }
                }
            }

            mTextTime.setText(Common.isEmptyString(mActivity, strTime));

            mLinearContainer.addView(view_medication);
        }
    }

    /**
     * @param mActivity   - Activity
     * @param checkInData - Check In Data
     */
    protected void onSelectedTreatmentSuggested(Activity mActivity, HistoryPreliminaryExaminationModel.Data checkInData) {
    }


    protected void onSelectedItem(Activity mActivity, Data checkInData) {

    }

    @Override
    public int getItemCount() {
        return mArrPreliminaryExamination == null ? 0 : mArrPreliminaryExamination.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrPreliminaryExamination.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * @param appointmentID - Appointment ID
     */
    private void callToPrintFinalVision(String appointmentID,String lang) {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            boolean mHistoryAndComplaints = false, mVisionUCVA = false, mVisionBCVAUndilated = false, mVisionFinal = false,
                    mVisionBCVADilated = false, mPrimaryExamination = false, mDiagnosis = false, mInvestigationSuggested = false,
                    mTreatmentSuggested = false, mCounselingDetails = false, mSurgeryDetails = false, mPrescription = true,
                    mPayment = false;

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPrintReceiptJson(AddNewPreliminaryExaminationFragment.PreliminaryExminationId,
                            String.valueOf(appointmentID), mHistoryAndComplaints, mVisionUCVA, mVisionBCVAUndilated,
                            mVisionFinal, mVisionBCVADilated, mPrimaryExamination, mDiagnosis, mInvestigationSuggested,
                            mTreatmentSuggested, mCounselingDetails, mSurgeryDetails, mPrescription, lang,false,mPayment, hospital_database));

            Call<PrintReceiptModel> call = RetrofitClient.createService(ApiInterface.class).PrintReceipt(body);
            call.enqueue(new Callback<PrintReceiptModel>() {
                @Override
                public void onResponse(@NonNull Call<PrintReceiptModel> call, @NonNull Response<PrintReceiptModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body() != null) {
                                String url = response.body().getData();
                                callToReceiptPrint(url);
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PrintReceiptModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Call To Receipt Print
     *
     * @param url - String Url Print
     */
    private void callToReceiptPrint(String url) {
        /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);*/

        // String pdfOpen = AppConstants.PDF_OPEN + url;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mActivity.startActivity(browserIntent);
    }

    public interface onHistoryDiagnosisListener {
        void onSelectHistioyDiagnosis(Activity mActivity, Data checkInData);
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder implements ExpandableLayout.OnExpansionUpdateListener {

        ImageView mImageEdit, mImageDate, mImagePrint;
        TextView mTextDate;
        LinearLayout mLinearContainer, mLinearMain;


        public CustomViewHolder(final View mView) {
            super(mView);
            //Image View
            this.mImageDate = mView.findViewById(R.id.image_row_history_preliminary_diagnosis_date);
            this.mImageEdit = mView.findViewById(R.id.image_row_history_preliminary_diagnosis_edit);

            // Text View
            this.mTextDate = mView.findViewById(R.id.text_row_history_preliminary_diagnosis_date);
            //Linear Layout
            this.mLinearContainer = mView.findViewById(R.id.linear_row_history_preliminary_diagnosis_container);
            this.mLinearMain = mView.findViewById(R.id.linear_row_history_preliminary_diagnosis_item);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Common.insertLog("ExpandableLayout State : " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                mRecyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }
    }
}
