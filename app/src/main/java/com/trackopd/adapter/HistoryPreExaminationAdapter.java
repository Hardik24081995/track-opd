package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.trackopd.R;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.fragments.AddNewPreliminaryExaminationFragment;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.PrintReceiptModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HistoryPreExaminationAdapter extends RecyclerView.Adapter {
    private static final int unselectedItem = -1;
    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    ArrayList<HistoryPreliminaryExaminationModel.Data> mArrPreliminaryExamination;
    private Activity mActivity;
    private int selectedItem = unselectedItem;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading,showHistoryGraph;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private String mUserId, mUserType;



    public HistoryPreExaminationAdapter(Context context, RecyclerView recyclerView,
                                        ArrayList<HistoryPreliminaryExaminationModel.Data> mArrPreliminaryExamination,boolean status) {
        this.mActivity = (Activity) context;
        this.mRecyclerView = recyclerView;
        this.mArrPreliminaryExamination = mArrPreliminaryExamination;
        mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
        LayoutInflater mLayoutInflater = LayoutInflater.from(mActivity);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);
        showHistoryGraph=status;

        setOnScrollListener();
    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_history_pre_examination_item, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        if (holder instanceof CustomViewHolder) {
            final HistoryPreliminaryExaminationModel.Data checkInData = mArrPreliminaryExamination.get(position);
            if (checkInData.getPreExamination() != null &&
                    checkInData.getPreExamination().size() > 0) {
                ((CustomViewHolder) holder).mTextDate.setText(checkInData.getTreatmentDate());

                ((CustomViewHolder) holder).mEditLeftPachymerty.setText(checkInData.getPreExamination().get(0)
                        .getLPachymetry());
                ((CustomViewHolder) holder).mEditRightPachymerty.setText(checkInData.getPreExamination().get(0)
                        .getRPachymetry());
                ((CustomViewHolder) holder).mEditRightSchiremer.setText(checkInData.getPreExamination().get(0)
                        .getRSchirmer());
                ((CustomViewHolder) holder).mEditLeftSchiremer.setText(checkInData.getPreExamination().get(0)
                        .getLSchirmer());
                ((CustomViewHolder) holder).mEditRightAT.setText(checkInData.getPreExamination().get(0).getRAT());
                ((CustomViewHolder) holder).mEditLeftAT.setText(checkInData.getPreExamination().get(0).getLAT());

                if (showHistoryGraph){
                    ((CustomViewHolder) holder).mImageGraph.setVisibility(View.VISIBLE);
                }else {
                    ((CustomViewHolder) holder).mImageGraph.setVisibility(View.GONE);
                }

                ((CustomViewHolder) holder).mImageGraph.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onSelectedGraph(mActivity,checkInData.getPreExamination(),holder.getAdapterPosition());
                    }
                });

            }
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    protected void onSelectedGraph(Activity mActivity, ArrayList<HistoryPreliminaryExaminationModel.PreExamination> preExamination, int adapterPosition) {
    }

    protected void onSelectedItem(Activity mActivity, HistoryPreliminaryExaminationModel.Data data) {

    }




    /**
     * @param appointmentID - Appointment ID
     */
    private void callToPrintFinalVision(String appointmentID) {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            boolean mHistoryAndComplaints = false, mVisionUCVA = false, mVisionBCVAUndilated = false, mVisionFinal = true,
                    mVisionBCVADilated = false, mPrimaryExamination = false, mDiagnosis = false, mInvestigationSuggested = false,
                    mTreatmentSuggested = false, mCounselingDetails = false, mSurgeryDetails = false, mPrescription = false,
                    mPayment = false;

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPrintReceiptJson(AddNewPreliminaryExaminationFragment.PreliminaryExminationId,
                            String.valueOf(appointmentID), mHistoryAndComplaints, mVisionUCVA, mVisionBCVAUndilated,
                            mVisionFinal, mVisionBCVADilated, mPrimaryExamination, mDiagnosis, mInvestigationSuggested,
                            mTreatmentSuggested, mCounselingDetails, mSurgeryDetails, mPrescription,"" ,false,mPayment, hospital_database));

            Call<PrintReceiptModel> call = RetrofitClient.createService(ApiInterface.class).PrintReceipt(body);
            call.enqueue(new Callback<PrintReceiptModel>() {
                @Override
                public void onResponse(@NonNull Call<PrintReceiptModel> call, @NonNull Response<PrintReceiptModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body() != null) {
                                String url = response.body().getData();
                                callToReceiptPrint(url);
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PrintReceiptModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Call To Receipt Print
     *
     * @param url - String Url Print
     */
    private void callToReceiptPrint(String url) {
        // String pdfOpen = AppConstants.PDF_OPEN + url;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mActivity.startActivity(browserIntent);
    }


    @Override
    public int getItemCount() {
        return mArrPreliminaryExamination == null ? 0 : mArrPreliminaryExamination.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrPreliminaryExamination.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder /*implements ExpandableLayout.OnExpansionUpdateListener*/ {

        private TextView mTextDate;
        private EditText mEditRightPachymerty, mEditLeftPachymerty, mEditRightSchiremer, mEditLeftSchiremer;
        private EditText mEditRightAT, mEditLeftAT;
        private ImageView mImageGraph;

        public CustomViewHolder(final View mView) {
            super(mView);

            mTextDate = mView.findViewById(R.id.text_row_pre_examination_date);
            mEditRightPachymerty = mView.findViewById(R.id.edit_add_pre_examination_right_eye_pachymetry);
            mEditLeftPachymerty = mView.findViewById(R.id.edit_add_pre_examination_left_eye_pachymetry);
            mEditRightSchiremer = mView.findViewById(R.id.edit_add_pre_examination_right_eye_schirmer);
            mEditLeftSchiremer = mView.findViewById(R.id.edit_add_pre_examination_left_eye_schirmer);
            mEditRightAT = mView.findViewById(R.id.edit_add_pre_examination_right_eye_at);
            mEditLeftAT = mView.findViewById(R.id.edit_add_pre_examination_left_eye_at);
            mImageGraph=mView.findViewById(R.id.image_history_pre_examination_item_graph);
        }

        /**
         * Binds the expandable view
         */
        public void bind() {
            int position = getAdapterPosition();
            boolean isSelected = position == selectedItem;
        }
    }
}
