package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.fragments.AddServiceAndPackage;
import com.trackopd.fragments.CheckInFragment;
import com.trackopd.fragments.ReceptionistAppointmentDetailFragment;
import com.trackopd.fragments.ReceptionistPatientDetailDocumentFragment;
import com.trackopd.fragments.ReceptionistPatientDetailPrescriptionFragment;
import com.trackopd.model.AddAppointmentModel;
import com.trackopd.model.AddPatientModel;
import com.trackopd.model.AppointmentReasonsModel;
import com.trackopd.model.ReceptionistAppointmentModel;
import com.trackopd.model.ReceptionistTimeSlotAppointmentModel;
import com.trackopd.model.RescheduleModel;
import com.trackopd.model.TimeSlotModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.GenerateTimeSlot;
import com.trackopd.utils.KeyboardUtility;
import com.trackopd.utils.StringUtils;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceptionistAppointmentTimeLineAdapter extends RecyclerView.Adapter implements ReceptionistAppointmentAdapter.SingleClickListener {

    private Activity mActivity;
    private ArrayList<ReceptionistTimeSlotAppointmentModel.Data> mArrReceptionistAppointmentModel;
    private ArrayList<AppointmentReasonsModel> mArrAppointmentReasonsModel;
    //private static SingleClickListener sClickListener;
    private static final int unselectedItem = -1;
    private int selectedItem = unselectedItem;

    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private String mUserType;

    ArrayList<ReceptionistAppointmentModel> mArrAppointment;
    ReceptionistAppointmentAdapter mAdapter;

    public ReceptionistAppointmentTimeLineAdapter(Activity mActivity, RecyclerView mRecyclerView, ArrayList<ReceptionistTimeSlotAppointmentModel.Data> mArrTimeSlotAppointment, ArrayList<AppointmentReasonsModel> mArrAppointmentReasons) {
        this.mActivity = mActivity;
        this.mRecyclerView = mRecyclerView;
        this.mArrReceptionistAppointmentModel=mArrTimeSlotAppointment;
        this.mArrAppointmentReasonsModel = mArrAppointmentReasons;
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        setOnScrollListener();
    }


    @Override
    public void onItemSelected(String date) {
        Update(date);


    }

    /**
     * This Method Use Date
     * @param date Date
     */
    protected void Update(String date){

    }


    public void selectedItem() {
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_receptionist_appointment_time_line_item, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder,
                                 @SuppressLint("RecyclerView") final int position) {
        if (holder instanceof CustomViewHolder) {


            ReceptionistTimeSlotAppointmentModel.Data data=mArrReceptionistAppointmentModel.get(position);

            ((CustomViewHolder) holder).mTextTime.setText(data.getTimeSlot());


            mArrAppointment= (ArrayList<ReceptionistAppointmentModel>) data.getAppointment();

            Common.insertLog("Position"+"Appointment List......"+mArrAppointment.clone());

            if (mArrAppointment.size()>0){

                Common.insertLog("Time:"+data.getTimeSlot()+"Appointment"+ data);

                ((CustomViewHolder) holder).recyclerViewAppointment.setHasFixedSize(true);
                LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mActivity);
                ((CustomViewHolder) holder).recyclerViewAppointment.setLayoutManager(mLinearLayoutManager);

                mAdapter=new ReceptionistAppointmentAdapter(mActivity,
                        ((CustomViewHolder) holder).recyclerViewAppointment,mArrAppointment,mArrAppointmentReasonsModel);
                ((CustomViewHolder) holder).recyclerViewAppointment.setAdapter(mAdapter);

                mAdapter.setOnItemClickListener(this);
            }

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return mArrReceptionistAppointmentModel == null ? 0 : mArrReceptionistAppointmentModel.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrReceptionistAppointmentModel.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder {

        TextView mTextTime;
        RecyclerView  recyclerViewAppointment;
        public CustomViewHolder(final View itemView) {
            super(itemView);
            this.setIsRecyclable(false);

            this.mTextTime=itemView.findViewById(R.id.text_row_receptionist_appointment_line_process_time);
            this.recyclerViewAppointment=itemView.findViewById(R.id.recycler_view);

            this.recyclerViewAppointment.setHasFixedSize(true);
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mActivity);
            this.recyclerViewAppointment.setLayoutManager(mLinearLayoutManager);
        }
    }
}
