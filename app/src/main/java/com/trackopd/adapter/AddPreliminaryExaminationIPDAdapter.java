package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import java.util.ArrayList;

public class AddPreliminaryExaminationIPDAdapter extends RecyclerView.Adapter {

    private Activity mActivity;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.IPD> mArrIPD;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.IPD> mArrSelectedIPD;
    private ArrayList<String> mIPD = new ArrayList<>();
    private String mSelectedValue, mUserType;
    private static SingleClickListener sClickListener;

    public AddPreliminaryExaminationIPDAdapter(Activity mActivity, String mUserType, String selectedValue, ArrayList<PreliminaryExaminationDetailsModel.Data.IPD> mArrIPD) {
        this.mActivity = mActivity;
        this.mUserType = mUserType;
        mSelectedValue = selectedValue;
        this.mArrIPD = mArrIPD;
        mArrSelectedIPD = new ArrayList<>();
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
    }

    /**
     * Create an interface for the recycler view click events
     */
    public void setOnItemClickListener(SingleClickListener clickListener) {
        sClickListener = clickListener;
    }

    /**
     * Update Filter
     * @param mArrIPD
     */
    public void updateList(ArrayList<PreliminaryExaminationDetailsModel.Data.IPD> mArrIPD) {
        this.mArrIPD=mArrIPD;
        notifyDataSetChanged();
    }

    public interface SingleClickListener {
        void onIPDCheckBoxClickListener(String mIPD);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .row_add_preliminary_examination_anatomical_location_item, parent, false);
        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final PreliminaryExaminationDetailsModel.Data.IPD ipdModel = mArrIPD.get(position);

        ((CustomViewHolder) holder).mCheckBox.setTag(position);

        ((CustomViewHolder) holder).mTextTitle.setSelected(true);
        ((CustomViewHolder) holder).mTextTitle.setText(Common.isEmptyString(mActivity, ipdModel.getIPD()));

        /*String[] mSplitValue = mSelectedValue.split(",");
        for (int j = 0; j < mSplitValue.length; j++) {
            if (mSplitValue[j].equals(systemicHistoryModel.getIPD())) {
                ((CustomViewHolder) holder).mCheckBox.setChecked(true);
            }
        }*/

        if (!mSelectedValue.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            mIPD = new ArrayList<>();
            String[] mSplitValue = mSelectedValue.split(",");
            for (int j = 0; j < mSplitValue.length; j++) {
                String mSelected = mSplitValue[j];
                mIPD.add(mSelected);
                if (mSelected.equals(ipdModel.getIPD())) {
                    ((CustomViewHolder) holder).mCheckBox.setChecked(true);
                }
            }
        }

        if(mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist)) || mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
            ((CustomViewHolder) holder).mCheckBox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    if (cb.isChecked()) {
                        mArrSelectedIPD.clear();
                        mArrSelectedIPD.add(ipdModel);
                        for (int j = 0; j < mArrSelectedIPD.size(); j++) {
                            String mIPDName = mArrSelectedIPD.get(j).getIPD();
                            mIPD.add(mIPDName);
                        }
                    } else {
                        mIPD.remove(ipdModel.getIPD());
                        mArrSelectedIPD.remove(ipdModel);
                    }
                    String mFinalSystemicHistory = mIPD.toString().replaceAll("\\[", "").replaceAll("\\]", "").replace(", ", ",");
                    sClickListener.onIPDCheckBoxClickListener(mFinalSystemicHistory);
                    Common.insertLog("IPD:::> " + mFinalSystemicHistory);
                }
            });
        } else {
            ((CustomViewHolder) holder).mCheckBox.setClickable(false);
        }
    }

    @Override
    public int getItemCount() {
        return mArrIPD.size();
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder {

        CheckBox mCheckBox;
        TextView mTextTitle;

        public CustomViewHolder(final View itemView) {
            super(itemView);
            this.setIsRecyclable(false);

            // Check Box
            this.mCheckBox = itemView.findViewById(R.id.checkbox_row_add_preliminary_examination_anatomical_location);

            // Text Views
            this.mTextTitle = itemView.findViewById(R.id.text_row_add_preliminary_examination_anatomical_location);
        }
    }
}
