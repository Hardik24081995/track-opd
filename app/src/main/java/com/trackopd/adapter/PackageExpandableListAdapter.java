package com.trackopd.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.model.ServicesPackagesModel;

import java.util.ArrayList;

public class PackageExpandableListAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    private ArrayList<ServicesPackagesModel.Package> mPackageArrayList;
    private ExpandableListView mExpandableListView;
    private boolean isOffer = false;
    public static int ServiceAndPackageLimit = 5;


    public PackageExpandableListAdapter(Context context, ArrayList<ServicesPackagesModel.Package> ServiceArrayList
            , ExpandableListView expandServiceListView) {

        this.mContext = context;
        this.isOffer = isOffer;
        this.mExpandableListView = expandServiceListView;
        this.mPackageArrayList = ServiceArrayList;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return mPackageArrayList.get(groupPosition).getItem().size();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        String serviceTitle = mPackageArrayList.get(groupPosition).
                getItem().get(childPosition).getTitle();

        String servicePrice = mPackageArrayList.get(groupPosition).
                getItem().get(childPosition).getAmount();


        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_packages_list_child_item, null);
        }

        TextView txtTitle = (TextView) convertView.findViewById(R.id.text_row_package_list_child_titles);
        TextView txtPrice = (TextView) convertView.findViewById(R.id.text_row_package_list_child_prices);

        txtTitle.setText(serviceTitle);
        txtPrice.setText(servicePrice);
        txtTitle.setSelected(true);

        return convertView;
    }

    private void selectedServices(int groupPosition, int childPosition,
                                  CheckBox checkBox) {

        if (!mPackageArrayList.get(groupPosition).isSelected()) {
            mPackageArrayList.get(groupPosition).isSelected();

            checkBox.setChecked(true);
            addItem(mPackageArrayList.get(groupPosition), childPosition);
        } else {
            mPackageArrayList.get(groupPosition).isSelected();
            checkBox.setChecked(false);

            removeItem(mPackageArrayList.get(groupPosition), childPosition);
        }
    }


    protected void addItem(ServicesPackagesModel.Package packages, int position) {

    }

    protected void removeItem(ServicesPackagesModel.Package packages, int position) {

    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mPackageArrayList.get(groupPosition).getItem().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mPackageArrayList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.mPackageArrayList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        String headerTitle = mPackageArrayList.get(groupPosition).getTitle();
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_packages_list_group_item, null);
        }

        TextView txtTitle = (TextView) convertView
                .findViewById(R.id.text_row_package_list_group_Item_title);
        TextView textPrice=convertView.findViewById(R.id.text_row_package_list_group_Item_offer_price);
        CheckBox checkBox=convertView.findViewById(R.id.checkbox_row_package_list_group_Item);

        ImageView image_down = convertView.findViewById(R.id.image_row_package_list_group_Item_arrow);

        RelativeLayout relative_main=convertView.findViewById(R.id.relative_row_packages_list_group_item_main);

        if (isExpanded) {
            txtTitle.setCompoundDrawablePadding(10);
            image_down.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_up_arrow));
        } else {
            txtTitle.setCompoundDrawablePadding(10);
            image_down.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_down_arrow));
        }

        txtTitle.setText(headerTitle);
        txtTitle.setSelected(true);
        textPrice.setText(mPackageArrayList.get(groupPosition).getDiscountAmount());

        checkBox.setChecked(mPackageArrayList.get(groupPosition).isSelected());

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedServices(groupPosition, groupPosition, checkBox);
            }
        });

        relative_main.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (isExpanded){
                    mExpandableListView.collapseGroup(groupPosition);
                }else {
                    mExpandableListView.expandGroup(groupPosition);
                }
                return false;
            }
        });

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
