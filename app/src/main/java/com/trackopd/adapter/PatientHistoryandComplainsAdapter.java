package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel.Data;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public class PatientHistoryandComplainsAdapter extends RecyclerView.Adapter {
    private Activity mActivity;
    private static final int unselectedItem = -1;
    private int selectedItem = unselectedItem;

    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;

    ArrayList<HistoryPreliminaryExaminationModel.Data> mArrPreliminaryExamination;

    private String mUserId,mUserType;
    
    public PatientHistoryandComplainsAdapter(Activity mActivity, RecyclerView recyclerView,
                  ArrayList<HistoryPreliminaryExaminationModel.Data> mArrHistory) {
   
        this.mActivity=mActivity;
        mRecyclerView=recyclerView;
        mArrPreliminaryExamination=mArrHistory;
        
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_history_preliminary_examination_history_compplains_item, null, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder( RecyclerView.ViewHolder holder, int position) {
        
        if (holder instanceof CustomViewHolder) {

             HistoryPreliminaryExaminationModel.Data checkInData = mArrPreliminaryExamination.get(position);

            ((CustomViewHolder) holder).mTextDateTime.setText(checkInData.getTreatmentDate());

            int size_primary=0,size_systemic=0,size_ocular=0,size_famliy=0;

            if (!checkInData.getHistoryAndComplains().get(0).getPrimaryComplains().trim().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                String[] array=checkInData.getHistoryAndComplains().get(0).getPrimaryComplains().split(",");
                size_primary=array.length;
                for ( String _string:array){
                    View view_container=LayoutInflater.from(mActivity.getBaseContext()).inflate(R.layout.row_add_preliminary_examination_compliance_item,
                            null,false);

                    TextView mTextName=view_container.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
                    ImageView mImageDelete=view_container.findViewById(R.id.image_row_add_preliminary_examination_compliance_delete);
                    mImageDelete.setVisibility(View.GONE);
                    mTextName.setText(_string);
                    mTextName.setTextColor(Color.BLACK);

                    ((CustomViewHolder) holder).mLinearPrimary.addView(view_container);
                }
            }

            if (!checkInData.getHistoryAndComplains().get(0).getSystemicHistory().trim().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                String[] array=checkInData.getHistoryAndComplains().get(0).getSystemicHistory().split(",");
                size_systemic=array.length;
                for ( String _string:array){
                    View view_container=LayoutInflater.from(mActivity).inflate(R.layout.row_add_preliminary_examination_compliance_item,
                            null,false);

                    TextView mTextName=view_container.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
                    ImageView mImageDelete=view_container.findViewById(R.id.image_row_add_preliminary_examination_compliance_delete);
                    mImageDelete.setVisibility(View.GONE);
                    mTextName.setText(_string);
                    mTextName.setTextColor(Color.BLACK);
                    ((CustomViewHolder) holder).mLinearSystemic.addView(view_container);
                }
            }

            if (!checkInData.getHistoryAndComplains().get(0).getOcularHistory().trim().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                String[] array=checkInData.getHistoryAndComplains().get(0).getOcularHistory().split(",");
                size_ocular=array.length;
                for ( String _string:array){
                    View view_container=LayoutInflater.from(mActivity).inflate(R.layout.row_add_preliminary_examination_compliance_item,
                            null,false);

                    TextView mTextName=view_container.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
                    ImageView mImageDelete=view_container.findViewById(R.id.image_row_add_preliminary_examination_compliance_delete);
                    mImageDelete.setVisibility(View.GONE);
                    mTextName.setText(_string);
                    mTextName.setTextColor(Color.BLACK);

                    ((CustomViewHolder) holder).mLinearOcular.addView(view_container);
                }
            }
            if (!checkInData.getHistoryAndComplains().get(0).getFamilyOcularHistory().trim().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                String[] array=checkInData.getHistoryAndComplains().get(0).getFamilyOcularHistory().split(",");
                size_famliy=array.length;
                for ( String _string:array){
                    View view_container=LayoutInflater.from(mActivity).inflate(R.layout.row_add_preliminary_examination_compliance_item,
                            null,false);

                    TextView mTextName=view_container.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
                    ImageView mImageDelete=view_container.findViewById(R.id.image_row_add_preliminary_examination_compliance_delete);
                    mImageDelete.setVisibility(View.GONE);
                    mTextName.setText(_string);
                    mTextName.setTextColor(Color.BLACK);

                    ((CustomViewHolder) holder).mLinearFamily.addView(view_container);
                }
            }

            //Set TextCount
            ((CustomViewHolder) holder).mTextPrimaryCount.setText("("+size_primary+")");
            ((CustomViewHolder) holder).mTextSystemicCount.setText("("+size_systemic+")");
            ((CustomViewHolder) holder).mTextOcularCount.setText("("+size_ocular+")");
            ((CustomViewHolder) holder).mTextFamilyCount.setText("("+size_famliy+")");


            //Set Text Color
            ((CustomViewHolder) holder).mImageDateTime.setColorFilter(Common.setThemeColor(mActivity));
            ((CustomViewHolder) holder).mImageEdit.setColorFilter(Common.setThemeColor(mActivity));

            //Expandable Layout
            ((CustomViewHolder) holder).mImagePrimaryArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((CustomViewHolder) holder).mExpandablePrimary.isExpanded()){
                        ((CustomViewHolder) holder).mImagePrimaryArrow.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
                        ((CustomViewHolder) holder).mExpandablePrimary.collapse();
                    }else {
                        ((CustomViewHolder) holder).mImagePrimaryArrow.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        ((CustomViewHolder) holder).mExpandablePrimary.expand();
                    }
                }
            });
            ((CustomViewHolder) holder).mImageSystemicArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((CustomViewHolder) holder).mExpandableSystemic.isExpanded()){
                        ((CustomViewHolder) holder).mImageSystemicArrow.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
                        ((CustomViewHolder) holder).mExpandableSystemic.collapse();
                    }else {
                        ((CustomViewHolder) holder).mImageSystemicArrow.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        ((CustomViewHolder) holder).mExpandableSystemic.expand();
                    }
                }
            });
            ((CustomViewHolder) holder).mImageOcularArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((CustomViewHolder) holder).mExpandableOcular.isExpanded()){
                        ((CustomViewHolder) holder).mImageOcularArrow.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
                        ((CustomViewHolder) holder).mExpandableOcular.collapse();
                    }else {
                        ((CustomViewHolder) holder).mImageOcularArrow.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        ((CustomViewHolder) holder).mExpandableOcular.expand();
                    }
                }
            });
            ((CustomViewHolder) holder).mImageFamilyArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((CustomViewHolder) holder).mExpandableFamily.isExpanded()){
                        ((CustomViewHolder) holder).mImageFamilyArrow.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
                        ((CustomViewHolder) holder).mExpandableFamily.collapse();
                    }else {
                        ((CustomViewHolder) holder).mImageFamilyArrow.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        ((CustomViewHolder) holder).mExpandableFamily.expand();
                    }
                }
            });

            ((CustomViewHolder) holder).mImageEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSelectIten(checkInData);
                }
            });

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    // On Click get Edit
    protected void onSelectIten(HistoryPreliminaryExaminationModel.Data checkInData){

    }


    @Override
    public int getItemCount() {
        return mArrPreliminaryExamination == null ? 0 : mArrPreliminaryExamination.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrPreliminaryExamination.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder implements ExpandableLayout.OnExpansionUpdateListener {

        private ImageView mImageDateTime,mImagePrimaryArrow,mImageOcularArrow,mImageSystemicArrow,
                mImageFamilyArrow,mImageEdit;
        private TextView mTextDateTime, mTextPrimaryCount,mTextOcularCount,mTextSystemicCount,mTextFamilyCount;
        private ExpandableLayout mExpandablePrimary,mExpandableOcular,mExpandableSystemic,
                mExpandableFamily;
        private LinearLayout mLinearPrimary,mLinearOcular,mLinearSystemic,
                mLinearFamily;


        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Image View
            this.mImageDateTime=itemView.findViewById(R.id.image_row_history_preliminary_examination_history_complains_date);
            this.mImagePrimaryArrow=itemView.findViewById(R.id.image_row_history_preliminary_examination_history_complains_primary_count);
            this.mImageSystemicArrow=itemView.findViewById(R.id.image_row_history_preliminary_examination_history_complains_systemic_count);
            this.mImageOcularArrow=itemView.findViewById(R.id.image_row_history_preliminary_examination_ocular);
            this.mImageFamilyArrow=itemView.findViewById(R.id.image_row_history_preliminary_examination_family);
            this.mImageEdit=itemView.findViewById(R.id.image_row_history_preliminary_examination_history_complains_edit);

            //Text View
            this.mTextDateTime=itemView.findViewById(R.id.text_row_history_preliminary_examination_history_complains_date);
            this.mTextPrimaryCount=itemView.findViewById(R.id.text_row_history_preliminary_examination_history_complains_primary_count);
            this.mTextOcularCount=itemView.findViewById(R.id.text_row_history_preliminary_examination_ocular_count);
            this.mTextSystemicCount=itemView.findViewById(R.id.text_row_history_preliminary_examination_history_complains_systemic_count);
            this.mTextFamilyCount=itemView.findViewById(R.id.text_row_history_preliminary_examination_family_count);

            //Expandable Layout
            this.mExpandablePrimary=itemView.findViewById(R.id.expandable_layout_row_history_primary_complains);
            this.mExpandableOcular=itemView.findViewById(R.id.expndable_row_history_preliminary_complains_ocular);
            this.mExpandableSystemic=itemView.findViewById(R.id.expandable_layout_row_history_systemic_histroy);
            this.mExpandableFamily=itemView.findViewById(R.id.expandable_layout_row_history_family_histroy);

            //Expandable Layout
            this.mLinearPrimary=itemView.findViewById(R.id.linear_row_history_preliminary_complains_primary);
            this.mLinearOcular=itemView.findViewById(R.id.linear_row_history_preliminary_complains_ocular);
            this.mLinearSystemic=itemView.findViewById(R.id.linear_row_history_preliminary_complains_systemic);
            this.mLinearFamily=itemView.findViewById(R.id.linear_row_history_preliminary_complains_famliy);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Common.insertLog("ExpandableLayout State : " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                mRecyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }

        /**
         * Binds the expandable view
         */
        public void bind() {
            int position = getAdapterPosition();
            boolean isSelected = position == selectedItem;
        }
    }
}
