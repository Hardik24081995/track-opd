package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.model.AppointmentReasonsModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import java.util.ArrayList;

public class AppointmentReasonsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Activity mActivity;
    private LayoutInflater mLayoutInflater;
    private ArrayList<AppointmentReasonsModel> mArrAppointmentReasonsModel;
    private static SingleClickListener sClickListener;
    private String mReason, mReasonId;

    public AppointmentReasonsAdapter(Activity mActivity, ArrayList<AppointmentReasonsModel> mArrAppointmentReasonsModel, String mSelectedReason) {
        this.mActivity = mActivity;
        this.mArrAppointmentReasonsModel = mArrAppointmentReasonsModel;
        mReason = mSelectedReason;
        mLayoutInflater = LayoutInflater.from(mActivity);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
    }

    /**
     * Create an interface for the recycler view click events
     */
    public void setOnItemClickListener(SingleClickListener clickListener) {
        sClickListener = clickListener;
    }

    public interface SingleClickListener {
        void onItemClickListener(int position, View view, String mReason, String mReasonID);
    }

    public void selectedItem() {
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mLayoutInflater.inflate(R.layout.row_appointment_reason_item, viewGroup,
                false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final AppointmentReasonsModel appointmentReasonsModel = mArrAppointmentReasonsModel.get(position);

        ((CustomViewHolder) holder).mRadioReason.setTag(position);

        ((CustomViewHolder) holder).mTextReason.setText(Common.isEmptyString(mActivity, appointmentReasonsModel.getReasons()));
        ((CustomViewHolder) holder).mRadioReason.setChecked(appointmentReasonsModel.getSelected());

        if (appointmentReasonsModel.getReasons().equalsIgnoreCase(mReason)) {
            ((CustomViewHolder) holder).mRadioReason.setChecked(true);
        } else {
            ((CustomViewHolder) holder).mRadioReason.setChecked(false);
        }

        // ToDo: Radio Button Click Listener
        ((CustomViewHolder) holder).mRadioReason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mReason = appointmentReasonsModel.getReasons();
                mReasonId = appointmentReasonsModel.getReasonID();
                sClickListener.onItemClickListener(position, v, mReason, mReasonId);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrAppointmentReasonsModel.size();
    }

    /**
     * Class  RecyclerView in Custom View Holder declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView mTextReason;
        RadioButton mRadioReason;

        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Text Views
            this.mTextReason = itemView.findViewById(R.id.text_row_appointment_reason);

            // Radio Buttons
            this.mRadioReason = itemView.findViewById(R.id.radio_row_appointment_reason);
        }
    }
}
