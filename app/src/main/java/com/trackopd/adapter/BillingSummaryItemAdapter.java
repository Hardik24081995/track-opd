package com.trackopd.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.model.BillingListItemModel;
import com.trackopd.utils.AppConstants;

import java.util.ArrayList;

public class BillingSummaryItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private RecyclerView mRecyclerView;
    private ArrayList<BillingListItemModel> mArrayBillingItem;

    public BillingSummaryItemAdapter(Activity mActivity, RecyclerView mRecyclerView, ArrayList<BillingListItemModel> mArrayBillingItem) {
        this.mContext=mActivity;
        this.mRecyclerView=mRecyclerView;
        this.mArrayBillingItem=mArrayBillingItem;
        mLayoutInflater = LayoutInflater.from(mActivity);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mLayoutInflater.inflate(R.layout.row_billing_summary_list_item, viewGroup,
                false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        if (holder instanceof CustomViewHolder){
            BillingListItemModel data=mArrayBillingItem.get(i);
            ((CustomViewHolder) holder).mTextTitle.setText(data.getName());
            ((CustomViewHolder) holder).mEditAmount.setText(data.getRate());

            ((CustomViewHolder) holder).mImageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    removeBillingItem(data,holder.getAdapterPosition());
                }
            });


            ((CustomViewHolder) holder).mEditAmount.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!s.toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){

                        data.setRate(s.toString());
                        UpdateRate(data,holder.getAdapterPosition());
                    }else {
                        ((CustomViewHolder) holder).mEditAmount.
                                setError(mContext.getResources().getString(R.string.error_field_required));
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

        }
    }


    protected void UpdateRate(BillingListItemModel data, int i) {
    }

    protected void removeBillingItem(BillingListItemModel data, int i) {
    }


    @Override
    public int getItemCount() {
        return mArrayBillingItem.size();
    }


    private class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView mTextTitle,mTextPrice;
        ImageView mImageDelete;
        EditText mEditAmount;
        public CustomViewHolder(View view) {
            super(view);
            this.mTextTitle=view.findViewById(R.id.text_row_billing_summary_list_item_title);
           // this.mTextPrice=view.findViewById(R.id.text_row_billing_summary_list_item_rate);

            this.mEditAmount=view.findViewById(R.id.edit_row_billing_summary_list_item_amount);

            this.mImageDelete=view.findViewById(R.id.image_row_billing_summary_list_item_remove);
        }
    }
}

