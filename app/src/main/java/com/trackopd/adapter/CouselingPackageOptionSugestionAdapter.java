package com.trackopd.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.fragments.CheckUpCounselingFragment;
import com.trackopd.fragments.SurgerySuggestedFragment;
import com.trackopd.model.CouselingPackageSuggestionModel;

import java.util.ArrayList;

public class CouselingPackageOptionSugestionAdapter extends BaseAdapter {

    private Activity mActivity;
    private ArrayList<CouselingPackageSuggestionModel> mArrPackageSuggested;

    private customSpinnerListener listener;

    public CouselingPackageOptionSugestionAdapter(Activity mActivity,
                                                  ArrayList<CouselingPackageSuggestionModel> mArrPackageSuggested,
                                                  CheckUpCounselingFragment checkUpCounselingFragment) {
        this.mActivity = mActivity;
        this.mArrPackageSuggested = mArrPackageSuggested;
        listener = checkUpCounselingFragment;
    }

    public CouselingPackageOptionSugestionAdapter(Activity mActivity, ArrayList<CouselingPackageSuggestionModel> mArrPackageSuggested, SurgerySuggestedFragment surgerySuggestedFragment) {
        this.mActivity = mActivity;
        this.mArrPackageSuggested = mArrPackageSuggested;
        listener = surgerySuggestedFragment;
    }

    @Override
    public int getCount() {
        return mArrPackageSuggested.size();
    }

    @Override
    public Object getItem(int position) {
        return mArrPackageSuggested.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        LayoutInflater layoutInflator = LayoutInflater.from(mActivity);
        convertView = layoutInflator.inflate(R.layout.row_couseling_package_suggested, parent, false);
        holder = new ViewHolder();
        holder.mTextView = convertView
                .findViewById(R.id.text_row_couseling_package_suggested_name);
        holder.mCheckBox = convertView
                .findViewById(R.id.checkbox_row_couseling_package_suggested);

        holder.mLinearPackageSuggested = convertView
                .findViewById(R.id.linear_couseling_package_suggested);

        convertView.setTag(holder);

        if (position == 0) {
            holder.mCheckBox.setVisibility(View.GONE);
        } else {
            holder.mCheckBox.setVisibility(View.VISIBLE);
        }

        holder.mTextView.setText(mArrPackageSuggested.get(position).getOpetionSuggested());

        if (mArrPackageSuggested.get(position).getChecked() == true) {
            holder.mCheckBox.setChecked(true);
        } else {
            holder.mCheckBox.setChecked(false);
        }

        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //model.setChecked(true);
                    mArrPackageSuggested.get(position).setChecked(true);
                    listener.onItemUpdate();

                } else {
                    mArrPackageSuggested.get(position).setChecked(false);
                    listener.onItemUpdate();
                }
            }
        });
        return convertView;
    }


    public interface customSpinnerListener {
        void onItemUpdate();
    }

    private class ViewHolder {
        private LinearLayout mLinearPackageSuggested;
        private TextView mTextView;
        private CheckBox mCheckBox;
    }
}
