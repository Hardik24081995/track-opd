package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.model.NotificationModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import java.util.ArrayList;

public class NotificationAdapter extends RecyclerView.Adapter {

    private Activity mActivity;
    private ArrayList<NotificationModel> mArrNotificationModel;
    private LayoutInflater mLayoutInflater;

    /**
     * Adapter contains the data to be displayed
     */
    public NotificationAdapter(Activity mActivity, ArrayList<NotificationModel> mArrNotificationModel) {
        this.mActivity = mActivity;
        this.mArrNotificationModel = mArrNotificationModel;
        mLayoutInflater = LayoutInflater.from(mActivity);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.row_notification_item, parent,
                false);
        return new CustomViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        final NotificationModel notificationModel = mArrNotificationModel.get(position);

        ((CustomViewHolder) holder).mTextTitle.setText(Common.isEmptyString(mActivity, notificationModel.getTitle()));
        ((CustomViewHolder) holder).mTextDescription.setText(Common.isEmptyString(mActivity, notificationModel.getDescription()));
        ((CustomViewHolder) holder).mTextDate.setText(mActivity.getResources().getString(R.string.text_payment_on) + Common.isEmptyString(mActivity, notificationModel.getDate()));
    }

    @Override
    public int getItemCount() {
        return mArrNotificationModel.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView mTextTitle, mTextDescription, mTextDate;

        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Text Views
            this.mTextTitle = itemView.findViewById(R.id.text_row_notification_title);
            this.mTextDescription = itemView.findViewById(R.id.text_row_notification_description);
            this.mTextDate = itemView.findViewById(R.id.text_row_notification_date);
        }
    }
}
