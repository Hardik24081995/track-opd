package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.trackopd.R;
import com.trackopd.fragments.DiagnosisFragment;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel.Data;
import com.trackopd.model.PreliminaryExaminationModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import net.cachapa.expandablelayout.ExpandableLayout;
import java.util.ArrayList;

public class HistoryDiagnosisAdapter extends RecyclerView.Adapter {
    private Activity mActivity;

    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private RecyclerView mRecyclerView;
    private ArrayList<HistoryPreliminaryExaminationModel.Data> mArrPreliminaryExamination;
    onHistoryDiagnosisListener onHistoryListener;

    public HistoryDiagnosisAdapter(Activity context, RecyclerView recyclerView, ArrayList<Data> mArrHistoryDiagnosis, DiagnosisFragment diagnosisFragment) {
        this.mActivity = context;
        this.mRecyclerView = recyclerView;
        this.mArrPreliminaryExamination = mArrHistoryDiagnosis;
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
      //  onHistoryListener=diagnosisFragment ;
    }

    public HistoryDiagnosisAdapter(Activity mActivity, RecyclerView recyclerView, ArrayList<Data> mArrHistory) {
        this.mActivity = mActivity;
        this.mRecyclerView = recyclerView;
        this.mArrPreliminaryExamination = mArrHistory;
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_history_diagnosis_header_layout, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder,
                                 @SuppressLint("RecyclerView") final int position) {
        
        if (holder instanceof CustomViewHolder) {

            HistoryPreliminaryExaminationModel.Data checkInData =mArrPreliminaryExamination.get(position);

            if (checkInData.getDiagnosis()!=null &&
                    checkInData.getDiagnosis().size()>0){
                ((CustomViewHolder) holder).mTextDate.setText(checkInData.getTreatmentDate());

                for (int a=0;a<checkInData.getDiagnosis().size();a++)
                {
                    HistoryPreliminaryExaminationModel.Diagnosi _diagnosis=checkInData.getDiagnosis().get(a);

                    Common.insertLog("Diagnosis.."+_diagnosis);
                    View view_container = LayoutInflater.from(mActivity.getBaseContext()).
                            inflate(R.layout.row_date_history_diagnosis_tem,
                                    null, false);

                    TextView textCategory = view_container.findViewById(R.id.text_row_date_history_diagnosis_item_category);
                    TextView textAnatomical = view_container.findViewById(R.id.text_row_date_history_diagnosis_item_anatomical_location);
                    TextView textEye = view_container.findViewById(R.id.text_row_date_history_diagnosis_item_eye);
                    TextView textDisease = view_container.findViewById(R.id.text_row_date_history_diagnosis_item_disease);
                    TextView textCataractType = view_container.findViewById(R.id.text_row_date_history_diagnosis_item_cataract_type);
                    TextView textStage = view_container.findViewById(R.id.text_row_date_history_diagnosis_item_stage);
                    TextView textSideBrain = view_container.findViewById(R.id.text_row_date_history_diagnosis_item_side_brain);
                    TextView textPatternType = view_container.findViewById(R.id.text_row_date_history_diagnosis_item_pattern_type);
                    TextView textEyeLence = view_container.findViewById(R.id.text_row_date_history_diagnosis_item_eye_lence);

                    textCategory.setText(_diagnosis.getCategory());
                    textAnatomical.setText(_diagnosis.getAnatomicalLocation());
                    textEye.setText(_diagnosis.getEye());
                    textDisease.setText(_diagnosis.getDisease());
                    textCataractType.setText(_diagnosis.getCataracttype());
                    textStage.setText(_diagnosis.getStage());
                    textSideBrain.setText(_diagnosis.getSideofbrain());
                    textPatternType.setText(_diagnosis.getPatterntype());
                    textEyeLence.setText(_diagnosis.getEyeLense());

                    ((CustomViewHolder) holder).mLinearContainer.addView(view_container);
                }
            }else {
                ((CustomViewHolder) holder).mLinearMain.setVisibility(View.GONE);
            }

            ((CustomViewHolder) holder).mImageEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSelectedItem(mActivity,checkInData);

//                    if (onHistoryListener!=null) {
//                        //onHistoryListener.onSelectHistioyDiagnosis(mActivity, checkInData);
//                    }
                }
            });

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }


    protected void onSelectedItem(Activity mActivity, Data checkInData){

    }

    @Override
    public int getItemCount() {
        return mArrPreliminaryExamination == null ? 0 : mArrPreliminaryExamination.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrPreliminaryExamination.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }


    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder implements ExpandableLayout.OnExpansionUpdateListener {

        ImageView mImageEdit,mImageDate;
        TextView mTextDate;
        LinearLayout mLinearContainer,mLinearMain;

        public CustomViewHolder(final View mView) {
            super(mView);
            //Image View
            this.mImageDate=mView.findViewById(R.id.image_row_history_preliminary_diagnosis_date);
            this.mImageEdit=mView.findViewById(R.id.image_row_history_preliminary_diagnosis_edit);
            // Text View
            this.mTextDate=mView.findViewById(R.id.text_row_history_preliminary_diagnosis_date);
            //Linear Layout
            this.mLinearContainer=mView.findViewById(R.id.linear_row_history_preliminary_diagnosis_container);
            this.mLinearMain=mView.findViewById (R.id.linear_row_history_preliminary_diagnosis_item);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Common.insertLog("ExpandableLayout State : " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                mRecyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }
    }

    public interface onHistoryDiagnosisListener {
       void onSelectHistioyDiagnosis(Activity mActivity, Data checkInData);
    }
}
