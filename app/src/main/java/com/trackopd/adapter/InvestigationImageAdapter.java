package com.trackopd.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.trackopd.R;
import com.yalantis.ucrop.util.FileUtils;

import java.util.ArrayList;

public class InvestigationImageAdapter extends RecyclerView.Adapter<InvestigationImageAdapter.ViewHolder> {

    Context mContext;
    ArrayList<Uri> arrayList;

    public InvestigationImageAdapter(Context mContext, ArrayList<Uri> arrayList) {
        this.mContext = mContext;
        this.arrayList = arrayList;
    }

    @NonNull
    @Override
    public InvestigationImageAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_items,parent,false);

        return new InvestigationImageAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InvestigationImageAdapter.ViewHolder holder, int position) {

        holder.imagePath.setText(FileUtils.getPath(mContext, arrayList.get(position)));

        Glide.with(mContext)
                .load(arrayList.get(position))
                .into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView imagePath;
        ImageView imageView;

        public ViewHolder(@NonNull View convertView) {
            super(convertView);

            imageView = convertView.findViewById(R.id.imageView);
            imagePath = convertView.findViewById(R.id.imagePath);
        }
    }
}
