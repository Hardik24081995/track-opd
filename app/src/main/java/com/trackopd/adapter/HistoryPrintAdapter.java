package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel.Data;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public class HistoryPrintAdapter extends RecyclerView.Adapter {

    private static final int unselectedItem = -1;
    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private Activity mActivity;
    private int selectedItem = unselectedItem;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;

    private ArrayList<HistoryPreliminaryExaminationModel.Data>
            mArrPreliminaryExamination = new ArrayList<>();

    public HistoryPrintAdapter(Activity mActivity, RecyclerView recyclerView, ArrayList<Data> mArrHistory) {
        this.mActivity = mActivity;
        this.mRecyclerView = recyclerView;
        this.mArrPreliminaryExamination = mArrHistory;

    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_history_print_item, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position) {

        if (holder instanceof CustomViewHolder) {
            HistoryPreliminaryExaminationModel.Data data = mArrPreliminaryExamination.get(position);

            String convertAppointment = AppConstants.STR_EMPTY_STRING;
            String convertTreatment = AppConstants.STR_EMPTY_STRING;

            String appointmentDate = data.getAppointmentDate();

            if (!appointmentDate.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                convertAppointment = Common.convertDateUsingDateFormat(mActivity, appointmentDate,
                        mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_kk_mm_ss_hyphen),
                        mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));
            }

            ((CustomViewHolder) holder).mTextAppointmentDate.setText(Common.isEmptyString(mActivity, convertAppointment));
            ((CustomViewHolder) holder).mTextAppointmentNumber.setText(Common.isEmptyString(mActivity, data.getTicketNumber()));

            String convertTreatement = AppConstants.STR_EMPTY_STRING;
            String treatmentDate = data.getTreatmentDate();

            if (!treatmentDate.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                convertTreatement = Common.convertDateUsingDateFormat(mActivity, appointmentDate,
                        mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen),
                        mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));
            }

            ((CustomViewHolder) holder).mTextTreatmentDate.setText(Common.isEmptyString(mActivity, convertTreatement));

            String doctorName = data.getDoctorFirstName() + AppConstants.STR_EMPTY_SPACE + data.getDoctorLastName();
            ((CustomViewHolder) holder).mTextDoctorName.setText(Common.isEmptyString(mActivity, doctorName));


            //Todo:Check Items
            ((CustomViewHolder) holder).mButtonPrint.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean mHistoryAndComplains = ((CustomViewHolder) holder).mCheckboxHistoryAndComplaints.isChecked();
                    boolean VisionUCVA = ((CustomViewHolder) holder).mCheckboxVisionUCVA.isChecked();
                    boolean VisionFinal = ((CustomViewHolder) holder).mCheckboxVisionFinal.isChecked();
                    boolean VisionBCVAUndilated = ((CustomViewHolder) holder).mCheckboxVisionBCVAUndilated.isChecked();
                    boolean VisionBCVADilated = ((CustomViewHolder) holder).mCheckboxVisionBCVADilated.isChecked();
                    boolean mPrimaryExamination = ((CustomViewHolder) holder).mCheckboxPrimaryExamination.isChecked();

                    boolean Diagnosis = ((CustomViewHolder) holder).mCheckboxDiagnosis.isChecked();
                    boolean CounselingDetails = ((CustomViewHolder) holder).mCheckboxCounselingDetails.isChecked();
                    boolean InvestigationSuggested = ((CustomViewHolder) holder).mCheckboxInvestigationSuggested.isChecked();
                    boolean TreatmentSuggested = ((CustomViewHolder) holder).mCheckboxTreatmentSuggested.isChecked();
                    boolean SurgeryDetails = ((CustomViewHolder) holder).mCheckboxSurgeryDetails.isChecked();
                    boolean Prescription = ((CustomViewHolder) holder).mCheckboxPrescription.isChecked();
                    boolean Payment = ((CustomViewHolder) holder).mCheckboxPayment.isChecked();
                    boolean Discharge = ((CustomViewHolder) holder).mCheckboxDischarge.isChecked();
                    String lang = "";


                    if (!mHistoryAndComplains && !VisionUCVA && !VisionFinal && !VisionBCVAUndilated &&
                            !VisionBCVADilated && !mPrimaryExamination && !Diagnosis && !CounselingDetails &&
                            !InvestigationSuggested && !TreatmentSuggested && !SurgeryDetails
                            && !Prescription && !Payment && !Discharge) {

                        Common.setCustomToast(mActivity, mActivity.getString(R.string.error_select_print));

                    } else {

                        if (Prescription || Discharge) {
                            callAlert(mActivity, data, mHistoryAndComplains, VisionUCVA, VisionFinal, VisionBCVAUndilated,
                                    VisionBCVADilated, mPrimaryExamination, Diagnosis, CounselingDetails, InvestigationSuggested,
                                    TreatmentSuggested, SurgeryDetails, Prescription,Discharge, Payment);
                        } else {
                            onSelectedItem(mActivity, data, mHistoryAndComplains, VisionUCVA, VisionFinal, VisionBCVAUndilated,
                                    VisionBCVADilated, mPrimaryExamination, Diagnosis, CounselingDetails, InvestigationSuggested,
                                    TreatmentSuggested, SurgeryDetails, Prescription,lang,Discharge, Payment);
                        }


                    }
                }
            });

            ((CustomViewHolder) holder).mImageExpand.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((CustomViewHolder) holder).mExpandableLayout.isExpanded()) {
                        ((CustomViewHolder) holder).mExpandableLayout.collapse();

                        ((CustomViewHolder) holder).mImageExpand.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));

                    } else {
                        ((CustomViewHolder) holder).mExpandableLayout.expand();
                        ((CustomViewHolder) holder).mImageExpand.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                    }
                }
            });

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    private void callAlert(Activity activity, Data data, boolean mHistoryAndComplains, boolean visionUCVA, boolean visionFinal, boolean
            visionBCVAUndilated, boolean visionBCVADilated, boolean mPrimaryExamination, boolean diagnosis, boolean counselingDetails,
                           boolean investigationSuggested, boolean treatmentSuggested, boolean surgeryDetails, boolean prescription,boolean discharge, boolean payment) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                mActivity);
        builder.setTitle("Language Options");
        builder.setMessage("Choose your desired language");
        builder.setNegativeButton("Hindi",
                (dialog, which) -> {
                    String lang = "Hindi";
                    HistoryPrintAdapter.this.onSelectedItem(mActivity, data, mHistoryAndComplains, visionUCVA, visionFinal, visionBCVAUndilated,
                            visionBCVADilated, mPrimaryExamination, diagnosis, counselingDetails, investigationSuggested,
                            treatmentSuggested, surgeryDetails,prescription, lang,discharge, payment);
                });
        builder.setPositiveButton("Gujarati",
                (dialog, which) -> {
                    String lang = "Gujarati";
                    HistoryPrintAdapter.this.onSelectedItem(mActivity, data, mHistoryAndComplains, visionUCVA, visionFinal, visionBCVAUndilated,
                            visionBCVADilated, mPrimaryExamination, diagnosis, counselingDetails, investigationSuggested,
                            treatmentSuggested, surgeryDetails, prescription,lang,discharge, payment);
                });
        builder.show();
    }

    protected void onSelectedItem(Activity activity, Data data, boolean mHistoryAndComplains, boolean visionUCVA, boolean visionFinal, boolean visionBCVAUndilated, boolean visionBCVADilated, boolean mPrimaryExamination, boolean diagnosis, boolean counselingDetails, boolean investigationSuggested, boolean treatmentSuggested, boolean surgeryDetails, boolean prescription,String lang,boolean discharge, boolean payment) {
    }


    @Override
    public int getItemCount() {
        return mArrPreliminaryExamination == null ? 0 : mArrPreliminaryExamination.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrPreliminaryExamination.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView mTextAppointmentDate, mTextAppointmentNumber, mTextTreatmentDate,
                mTextDoctorName;
        ImageView mImagePrint, mImageExpand;
        LinearLayout mLinearMain;
        CheckBox mCheckboxHistoryAndComplaints, mCheckboxVisionUCVA, mCheckboxVisionFinal,
                mCheckboxVisionBCVAUndilated, mCheckboxVisionBCVADilated, mCheckboxPrimaryExamination,
                mCheckboxDiagnosis, mCheckboxInvestigationSuggested, mCheckboxTreatmentSuggested,
                mCheckboxCounselingDetails, mCheckboxSurgeryDetails, mCheckboxPrescription, mCheckboxPayment, mCheckboxDischarge;

        Button mButtonPrint;
        ExpandableLayout mExpandableLayout;

        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            mLinearMain = itemView.findViewById(R.id.linear_history_print_item);

            mTextAppointmentDate = itemView.findViewById(R.id.text_row_history_print_item_appointment_date);
            mTextAppointmentNumber = itemView.findViewById(R.id.text_row_history_print_item_appointment_number);
            mTextTreatmentDate = itemView.findViewById(R.id.text_row_history_print_item_treatment_date);
            mTextDoctorName = itemView.findViewById(R.id.text_row_history_print_item_doctor);

            //mImagePrint=itemView.findViewById(R.id.image_row_history_print_item_print);
            mImageExpand = itemView.findViewById(R.id.image_row_history_print_item_arrow);
            // Check Boxes
            mCheckboxHistoryAndComplaints = itemView.findViewById(R.id.check_box_row_history_print_item_history_and_complaint);
            mCheckboxVisionUCVA = itemView.findViewById(R.id.check_box_row_history_print_item_vision_ucva);
            mCheckboxVisionFinal = itemView.findViewById(R.id.check_box_row_history_print_item_vision_final);
            mCheckboxVisionBCVAUndilated = itemView.findViewById(R.id.check_box_row_history_print_item_vision_bcva_undilated);
            mCheckboxVisionBCVADilated = itemView.findViewById(R.id.check_box_row_history_print_item_vision_bcva_dilated);
            mCheckboxPrimaryExamination = itemView.findViewById(R.id.check_box_row_history_print_item_primary_examination);
            mCheckboxDiagnosis = itemView.findViewById(R.id.check_box_row_history_print_item_diagnosis);
            mCheckboxInvestigationSuggested = itemView.findViewById(R.id.check_box_row_history_print_item_investigation_suggested);
            mCheckboxTreatmentSuggested = itemView.findViewById(R.id.check_box_row_history_print_item_treatment_suggested);
            mCheckboxCounselingDetails = itemView.findViewById(R.id.check_box_row_history_print_item_counseling_details);
            mCheckboxSurgeryDetails = itemView.findViewById(R.id.check_box_row_history_print_item_surgery_details);
            mCheckboxPrescription = itemView.findViewById(R.id.check_box_row_history_print_item_prescription);
            mCheckboxPayment = itemView.findViewById(R.id.check_box_row_history_print_item_payment);
            mCheckboxDischarge = itemView.findViewById(R.id.check_box_row_history_print_item_discharge);

            mButtonPrint = itemView.findViewById(R.id.button_row_history_print_item_submit);

            mExpandableLayout = itemView.findViewById(R.id.expandable_row_history_print_item_layout);

        }
    }
}
