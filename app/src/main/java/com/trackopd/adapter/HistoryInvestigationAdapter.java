package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.trackopd.Interface.OnSelectedHistory;
import com.trackopd.R;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.fragments.InvestigationFragment;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel.Data;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.model.PreliminaryExaminationModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public abstract class HistoryInvestigationAdapter extends RecyclerView.Adapter {
    private Activity mActivity;
    private static final int unselectedItem = -1;
    private int selectedItem = unselectedItem;

    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;

    ArrayList<HistoryPreliminaryExaminationModel.Data> mArrPreliminaryExamination;


    public HistoryInvestigationAdapter(Activity mActivity, RecyclerView recyclerView,
                                       ArrayList<HistoryPreliminaryExaminationModel.Data> mArrHistory) {
        this.mActivity = mActivity;
        this.mRecyclerView = recyclerView;
        this.mArrPreliminaryExamination = mArrHistory;
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_history_preliminary_diagnosis_item, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder,
                                 @SuppressLint("RecyclerView") int position) {

        if (holder instanceof CustomViewHolder) {

            HistoryPreliminaryExaminationModel.Data data = mArrPreliminaryExamination.get(position);

            if (data.getInvestigation() != null &&
                    data.getInvestigation().size() > 0) {

                ((CustomViewHolder) holder).mTextDate.setText(data.getTreatmentDate());


                for (int a = 0; a < data.getInvestigation().size(); a++) {

                    HistoryPreliminaryExaminationModel.Investigation
                            investigation = data.getInvestigation().get(a);

                    View view_container = LayoutInflater.from(mActivity.getBaseContext())
                            .inflate(R.layout.row_history_preliminary_investigation_item,
                                    null, false);

                    TextView mTextEye = view_container.findViewById(R.id.text_row_history_investigation_item_eye);
                    TextView mTextOcularInvestigation =
                            view_container.findViewById(R.id.text_row_history_investigation_item_ocular_investiagtion);
                    TextView mTextLaborateyTest =
                            view_container.findViewById(R.id.text_row_history_investigation_item_laboratey_test);


                    ImageView mImageInvestigation =
                            view_container.findViewById(R.id.image_row_row_history_investigation_item_pic);

                    mTextEye.setText(Common.isEmptyString(mActivity, investigation.getInvestigationEye()));
                    mTextOcularInvestigation.setText(Common.isEmptyString(mActivity, investigation.getOcularinvestigation()));
                    mTextLaborateyTest.setText(Common.isEmptyString(mActivity, investigation.getLaboratoryTest()));

                    String image = data.getInvestigation().get(a).getImage();




                    if (!image.equalsIgnoreCase("")) {
                        setGlideImageView(mImageInvestigation, image, data.getAppointmentID());
                    }

                    //set Image
                    // setImageOnImageView(mImageInvestigation,data.getInvestigation().get(a).getImage(),data.getAppointmentID());

                    ((CustomViewHolder) holder).mLinearContainer.addView(view_container);
                }
            }

            ((CustomViewHolder) holder).mImageEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSelectedItem(mActivity, data);
                }
            });

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    /**
     * This should set the image view using glide
     */
    private void setGlideImageView(ImageView image, String imageName, String AppointmentID) {
        try {
            String mBaseURL = WebFields.API_BASE_URL;
            String mFolderName = WebFields.IMAGE_BASE_FOLDER;
            String mHospitalCode = GetJsonData.getHospitalData(mActivity, WebFields.PASS_CODE.RESPONSE_HOSPITAL_CODE);

            // http://societyfy.in/Track_OPD/assets/uploads/12356/34/Investigation/1568020896_34.jpg
            String url = mBaseURL + mFolderName + mHospitalCode + AppConstants.STR_FORWARD_SLASH + AppointmentID + AppConstants.STR_FORWARD_SLASH +
                    "Investigation" + AppConstants.STR_FORWARD_SLASH + imageName;

            Common.insertLog("Image url " + url);

            Glide.with(mActivity)
                    .load(url)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .placeholder(R.drawable.upload_icon)
                    .into(image);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected void onSelectedItem(Activity activity, HistoryPreliminaryExaminationModel.Data data) {

    }


    @Override
    public int getItemCount() {
        return mArrPreliminaryExamination == null ? 0 : mArrPreliminaryExamination.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrPreliminaryExamination.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder implements ExpandableLayout.OnExpansionUpdateListener {

        ImageView mImageEdit, mImageDate;
        TextView mTextDate;
        LinearLayout mLinearContainer, mLinearMain;


        public CustomViewHolder(final View mView) {
            super(mView);
            //Image View
            this.mImageDate = mView.findViewById(R.id.image_row_history_preliminary_diagnosis_date);
            this.mImageEdit = mView.findViewById(R.id.image_row_history_preliminary_diagnosis_edit);
            // Text View
            this.mTextDate = mView.findViewById(R.id.text_row_history_preliminary_diagnosis_date);
            //Linear Layout
            this.mLinearContainer = mView.findViewById(R.id.linear_row_history_preliminary_diagnosis_container);
            this.mLinearMain = mView.findViewById(R.id.linear_row_history_preliminary_diagnosis_item);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Common.insertLog("ExpandableLayout State : " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                mRecyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }

        /**
         * Binds the expandable view
         */
        public void bind() {
            int position = getAdapterPosition();
            boolean isSelected = position == selectedItem;
        }
    }

    public void setImageOnImageView(ImageView image, String imageName, String AppointmentID) {
        try {


            if (imageName.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                return;
            }

            String mBaseURL = WebFields.API_BASE_URL;
            String mFolderName = WebFields.IMAGE_BASE_FOLDER;
            String mHospitalCode = GetJsonData.getHospitalData(mActivity, WebFields.PASS_CODE.RESPONSE_HOSPITAL_CODE);

            // http://societyfy.in/Track_OPD/assets/uploads/12356/34/Investigation/1568020896_34.jpg
            String url = mBaseURL + mFolderName + mHospitalCode + AppConstants.STR_FORWARD_SLASH + AppointmentID + AppConstants.STR_FORWARD_SLASH +
                    "Investigation" + AppConstants.STR_FORWARD_SLASH + imageName;

            Common.insertLog("Image url " + url);

            Glide.with(mActivity)
                    .load(url)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .placeholder(R.drawable.upload_icon)
                    .into(image);

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }
}
