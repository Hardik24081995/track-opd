package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParser;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.fragments.CheckInFragment;
import com.trackopd.fragments.ReceptionistAppointmentDetailFragment;
import com.trackopd.fragments.ReceptionistPatientDetailDocumentFragment;
import com.trackopd.fragments.ReceptionistPatientDetailFragment;
import com.trackopd.fragments.ReceptionistPatientDetailPrescriptionFragment;
import com.trackopd.fragments.SurgeryListFragment;
import com.trackopd.model.AddAppointmentModel;
import com.trackopd.model.AddPatientModel;
import com.trackopd.model.AppointmentReasonsModel;
import com.trackopd.model.ReceptionistAppointmentModel;
import com.trackopd.model.ReceptionistPatientModel;
import com.trackopd.model.RescheduleModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.GenerateTimeSlot;
import com.trackopd.utils.KeyboardUtility;
import com.trackopd.utils.SessionManager;
import com.trackopd.utils.StringUtils;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReceptionistAppointmentAdapter extends RecyclerView.Adapter
        implements AppointmentReasonsAdapter.SingleClickListener {

    private static final int unselectedItem = -1;
    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    public SingleClickListener sClickListener;
    private Activity mActivity;
    private ArrayList<ReceptionistAppointmentModel> mArrReceptionistAppointmentModel;
    private ArrayList<AppointmentReasonsModel> mArrAppointmentReasonsModel;
    private String mSelReason, mSelReasonID, mAppointmentId, mUserId, mPatientId;
    private AppointmentReasonsAdapter mAdapterAppointmentReasons;
    private int selectedItem = unselectedItem;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private String mUserType;
    private Runnable updater;

    public ReceptionistAppointmentAdapter(Activity mActivity, RecyclerView mRecyclerView, ArrayList<ReceptionistAppointmentModel> mArrReceptionistAppointmentModel, ArrayList<AppointmentReasonsModel> mArrAppointmentReasons) {
        this.mActivity = mActivity;
        this.mRecyclerView = mRecyclerView;
        this.mArrReceptionistAppointmentModel = mArrReceptionistAppointmentModel;
        this.mArrAppointmentReasonsModel = mArrAppointmentReasons;
        mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);
        //setOnScrollListener();
    }

    /**
     * Create an interface for the recycler view click events
     */
    public void setOnItemClickListener(SingleClickListener clickListener) {
        sClickListener = clickListener;
    }

    @Override
    public void onItemClickListener(int position, View view, String mReason, String mReasonID) {
        mAdapterAppointmentReasons.selectedItem();
        mSelReason = mReason;
        mSelReasonID = mReasonID;
        Common.insertLog("Reason Appoint Adapter:::> " + mReason);
    }


    public void selectedItem() {
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_receptionist_appointment_item, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (holder instanceof CustomViewHolder) {

            final ReceptionistAppointmentModel receptionistAppointmentModel = mArrReceptionistAppointmentModel.get(position);
            ReceptionistPatientModel receptionistPatientModel = new ReceptionistPatientModel();

            ((CustomViewHolder) holder).mLinearReschedule.setTag(position);
            ((CustomViewHolder) holder).mLinearCancel.setTag(position);
            ((CustomViewHolder) holder).mLinearExpandableArrow.setTag(position);
            ((CustomViewHolder) holder).mLinearCheckIn.setTag(position);

            ((CustomViewHolder) holder).mRelativeImageBorder.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.user_image_border));


            if (receptionistAppointmentModel.getProfileImage() != null && !receptionistAppointmentModel.getProfileImage().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                //Set Thumbnail
                String thumbmail_url = WebFields.API_BASE_URL + WebFields.IMAGE_THUMBNAIL_URL + receptionistAppointmentModel.getProfileImage();
                Glide.with(mActivity)
                        .load(thumbmail_url)
                        .apply(new RequestOptions().error(R.drawable.ic_documents_img).placeholder(R.drawable.ic_documents_img))
                        .into(((CustomViewHolder) holder).mImageUserProfilePic);
                ((CustomViewHolder) holder).mImageUserProfilePic.setImageURI(Uri.parse(thumbmail_url));
            } else {
                ((CustomViewHolder) holder).mImageUserProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, receptionistAppointmentModel.getPatientFirstName(),
                        receptionistAppointmentModel.getPatientLastName()));
            }

            if (!receptionistAppointmentModel.getAppointmentStatus().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                ((CustomViewHolder) holder).mImageStatus.setVisibility(View.VISIBLE);
                if (receptionistAppointmentModel.getAppointmentStatus().equalsIgnoreCase(mActivity.getResources().getString(R.string.status_accept))) {
                    ((CustomViewHolder) holder).mImageStatus.setImageResource(R.drawable.ic_dot_green_icon);
                } else if (receptionistAppointmentModel.getAppointmentStatus().equalsIgnoreCase(mActivity.getResources().getString(R.string.status_reject))) {
                    ((CustomViewHolder) holder).mImageStatus.setImageResource(R.drawable.ic_dot_red_icon);
                } else if (receptionistAppointmentModel.getAppointmentStatus().equalsIgnoreCase(mActivity.getResources().getString(R.string.status_pending))) {
                    ((CustomViewHolder) holder).mImageStatus.setImageResource(R.drawable.ic_dot_orange_icon);
                } else if (receptionistAppointmentModel.getAppointmentStatus().equalsIgnoreCase(mActivity.getResources().getString(R.string.status_completed))) {
                    ((CustomViewHolder) holder).mImageStatus.setImageResource(R.drawable.ic_dot_blue_icon);
                }
            } else {
                ((CustomViewHolder) holder).mImageStatus.setVisibility(View.GONE);
            }

            if (receptionistAppointmentModel.getAppointmentType().equalsIgnoreCase(mActivity.getResources().getString(R.string.text_walk_in))) {
                ((CustomViewHolder) holder).mImageWalkInPatient.setVisibility(View.VISIBLE);
            } else {
                ((CustomViewHolder) holder).mImageWalkInPatient.setVisibility(View.GONE);
            }

            String mPatientName = receptionistAppointmentModel.getPatientFirstName() + AppConstants.STR_EMPTY_SPACE + receptionistAppointmentModel.getPatientLastName();
            String mDoctorName = receptionistAppointmentModel.getDoctorFirstName() + AppConstants.STR_EMPTY_SPACE + receptionistAppointmentModel.getDoctorLastName();
            // ((CustomViewHolder) holder).mTextMRDNo.setText(Common.isEmptyString(mActivity, receptionistAppointmentModel.getMRDNo()));
            ((CustomViewHolder) holder).mTextName.setText(Common.isEmptyString(mActivity, mPatientName));
            ((CustomViewHolder) holder).mTextMobileNo.setText(Common.isEmptyString(mActivity, receptionistAppointmentModel.getPatientMobileNo()));
            ((CustomViewHolder) holder).mTextAppointmentNo.setText(Common.isEmptyString(mActivity, receptionistAppointmentModel.getTicketNumber()));
            ((CustomViewHolder) holder).mTextAppointmentDate.setText(Common.isEmptyString(mActivity, receptionistAppointmentModel.getAppointmentDate()));
            ((CustomViewHolder) holder).mTextDoctorName.setText(Common.isEmptyString(mActivity, mDoctorName));
            ((CustomViewHolder) holder).mTextBookingType.setText(Common.isEmptyString(mActivity, receptionistAppointmentModel.getAppointmentType()));
            ((CustomViewHolder) holder).mTextPaymentType.setText(Common.isEmptyString(mActivity, receptionistAppointmentModel.getPaymentStatus()));
            ((CustomViewHolder) holder).mTextStatus.setText(Common.isEmptyString(mActivity, receptionistAppointmentModel.getCurrentStatus()));
            ((CustomViewHolder) holder).mTextSelLocations.setText(Common.isEmptyString(mActivity, receptionistAppointmentModel.getAppointmentLocation()));
            ((CustomViewHolder) holder).mTextPatientCode.setText(Common.isEmptyString(mActivity, receptionistAppointmentModel.getPatientCode()));

            if (!receptionistAppointmentModel.getAppointmentLocation().equalsIgnoreCase(mActivity.getResources().getString(R.string.text_direct))) {
                ((CustomViewHolder) holder).mLinearLocations.setVisibility(View.VISIBLE);
                ((CustomViewHolder) holder).mTextLocations.setText(Common.isEmptyString(mActivity, receptionistAppointmentModel.getLocation()));
            } else {
                ((CustomViewHolder) holder).mLinearLocations.setVisibility(View.GONE);
            }

            // Reason Check is Cancelled
            if (!StringUtils.isEmpty(receptionistAppointmentModel.getReason())) {
                mSelReason = receptionistAppointmentModel.getReason();
                ((CustomViewHolder) holder).mLinearReason.setVisibility(View.VISIBLE);
                ((CustomViewHolder) holder).mTextReason.setText(receptionistAppointmentModel.getReason());
            }

            if (!receptionistAppointmentModel.getReasonID().equalsIgnoreCase("0")) {
                mSelReason = receptionistAppointmentModel.getReason();
                ((CustomViewHolder) holder).mLinearReason.setVisibility(View.VISIBLE);
                ((CustomViewHolder) holder).mTextReason.setText(receptionistAppointmentModel.getReason());
            } else {
                mSelReason = "";
                ((CustomViewHolder) holder).mLinearReason.setVisibility(View.GONE);
            }

            // Appointment  Complete by Receptionist after show Document
            if (receptionistAppointmentModel.getCurrentStatus().equalsIgnoreCase(AppConstants.STATUS_COMPLETE_BY_RECEPTIONIST)) {
                ((CustomViewHolder) holder).mLinearDocument.setVisibility(View.VISIBLE);
            } else {
                ((CustomViewHolder) holder).mLinearDocument.setVisibility(View.GONE);
            }

            //TODO: Check In Date Set and Same Date Show Timer
            String checkInDate = receptionistAppointmentModel.getCheckINDate();

            if (!checkInDate.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {

                ((CustomViewHolder) holder).mLinearCheckIn.setVisibility(View.GONE);
                ((CustomViewHolder) holder).mTextCheckInDate.setVisibility(View.VISIBLE);
                //((CustomViewHolder) holder).mTextCheckInDate.setText(receptionistAppointmentModel.getCheckINDate());

                String currentDate = Common.setCurrentDate(mActivity);
                String convertCheckInDate = Common.convertDateUsingDateFormat(mActivity, checkInDate,
                        mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_kk_mm_ss_hyphen)
                        , mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen));

                String convertCurrentDate = Common.convertDateUsingDateFormat(mActivity, currentDate,
                        mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy)
                        , mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen));


                if (!convertCheckInDate.equals(convertCurrentDate)) {
                    ((CustomViewHolder) holder).mTextCheckInDate.setText("Check In:" + convertCheckInDate);
                } else {
                    ((CustomViewHolder) holder).mTextCheckInDate.setVisibility(View.GONE);
                }

                if (receptionistAppointmentModel.getCheckUpType().equals("Surgery")) {
                    ((CustomViewHolder) holder).mLinearSurgery.setVisibility(View.VISIBLE);
                    ((CustomViewHolder) holder).mButtonSurgery.setText(
                            mActivity.getResources().getString(R.string.text_checkup));

                } else {
                    ((CustomViewHolder) holder).mLinearSurgery.setVisibility(View.VISIBLE);
                    ((CustomViewHolder) holder).mButtonSurgery.setText(
                            mActivity.getResources().getString(R.string.text_surgery));
                }

                if (receptionistAppointmentModel.getCurrentStatus().trim().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                    String message = mPatientName + " has Check IN but Examination is not started";
                    ((CustomViewHolder) holder).mTextStatus.setText(message);
                }

            } else {
                ((CustomViewHolder) holder).mLinearCheckIn.setVisibility(View.VISIBLE);
                ((CustomViewHolder) holder).mTextCheckInDate.setVisibility(View.GONE);
                ((CustomViewHolder) holder).mLinearSurgery.setVisibility(View.GONE);

                String message = mPatientName + " Appointment is Pending status and not Check IN";

                if (receptionistAppointmentModel.getCurrentStatus().trim().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                    ((CustomViewHolder) holder).mTextStatus.setText(message);
                }
            }

            // ToDo: Check-In Click Listener
            ((CustomViewHolder) holder).mLinearCheckIn.setOnClickListener(view -> {

                String checkupType = receptionistAppointmentModel.getCheckUpType();
                callToAddCheckInAPI(receptionistAppointmentModel, checkupType);
            });

            ((CustomViewHolder) holder).button_checkin.setOnClickListener(v -> {

                String checkupType = receptionistAppointmentModel.getCheckUpType();
                callToAddCheckInAPI(receptionistAppointmentModel, checkupType);

            });

            // ToDo: Reschedule Click Listener
            ((CustomViewHolder) holder).mLinearReschedule.setOnClickListener(view -> openChangeAppointmentDialog(receptionistAppointmentModel));

            // ToDo: Reschedule Click Listener
            ((CustomViewHolder) holder).mButtonSurgery.setOnClickListener(view -> {
                // Convert to Appointment's Visa-versa
                openChangeAppointment(receptionistAppointmentModel);
            });

            // ToDo: Cancel Click Listener
            Common.insertLog("REason ID::::> " + receptionistAppointmentModel.getReasonID());
            if (receptionistAppointmentModel.getReasonID().equalsIgnoreCase("0") &&
                    receptionistAppointmentModel.getReason().equalsIgnoreCase("")) {
                ((CustomViewHolder) holder).mLinearCancel.setEnabled(true);
                ((CustomViewHolder) holder).mTextCancel.setTextColor(mActivity.getResources().getColor(R.color.colorGray));
                ((CustomViewHolder) holder).mLinearCancel.setOnClickListener(view -> openCancelDialog(holder, receptionistAppointmentModel));
            } else {
                ((CustomViewHolder) holder).mLinearCancel.setEnabled(false);
                ((CustomViewHolder) holder).mTextCancel.setTextColor(mActivity.getResources().getColor(R.color.button_disable));
            }

            int pos = holder.getAdapterPosition();
            if (pos != selectedItem) {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                ((CustomViewHolder) holder).mExpandableLayout.collapse();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
            } else {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                ((CustomViewHolder) holder).mExpandableLayout.expand();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
            }

            // ToDo: Expandable Arrow Click Listener
            ((CustomViewHolder) holder).bind();
            ((CustomViewHolder) holder).mLinearExpandableArrow.setOnClickListener(v -> {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                ((CustomViewHolder) holder).mExpandableLayout.collapse();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));

                Common.insertLog("Position:::> " + position);
                Common.insertLog("selectedItem:::> " + selectedItem);

                int pos1 = holder.getAdapterPosition();
                if (pos1 == selectedItem) {
                    selectedItem = unselectedItem;
                } else {
                    ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                    ((CustomViewHolder) holder).mExpandableLayout.expand();
                    ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                    selectedItem = pos1;
                }
                notifyDataSetChanged();
            });

            ((CustomViewHolder) holder).mImageReport.setOnClickListener(v -> redirectToDocumentList(receptionistAppointmentModel));

            ((CustomViewHolder) holder).mImagePrescription.setOnClickListener(v -> redirectToPrescriptionList(receptionistAppointmentModel));

            // ToDo: Appointment Detail Click Listener
            View.OnClickListener onClickListener = v ->
                    callPatientListAPI(receptionistAppointmentModel.getPatientCode());
            ((CustomViewHolder) holder).mTextName.setOnClickListener(onClickListener);

            ((CustomViewHolder) holder).mLinearBottom.setOnClickListener(onClickListener);

            ((CustomViewHolder) holder).mImageUserProfilePic.setOnClickListener(onClickListener);

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    /**
     * This method should call the patient listing for receptionist
     */
    private void callPatientListAPI(String patientCode) {
        try {
            int currentPageIndex = 1;

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setReceptionistPatientListJson(currentPageIndex, "", patientCode, "", hospital_database));

            Call<ReceptionistPatientModel> call = RetrofitClient.createService(ApiInterface.class).getReceptionistPatientList(requestBody);
            call.enqueue(new Callback<ReceptionistPatientModel>() {
                @Override
                public void onResponse(@NonNull Call<ReceptionistPatientModel> call, @NonNull Response<ReceptionistPatientModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            if (response.body() != null && response.body().getData() != null) {
                                ArrayList<ReceptionistPatientModel> patientModel = new ArrayList<>(Arrays
                                        .asList(new GsonBuilder().serializeNulls().create()
                                                .fromJson(new JsonParser().parse(jsonObject.getString(WebFields.DATA)).
                                                        getAsJsonArray(), ReceptionistPatientModel[].class)));
                                callToReceptionistPatientDetailFragment(patientModel);
                            }

                        } else {
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ReceptionistPatientModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Receptionist Patient Detail Fragment
     *
     * @param - Receptionist Patient Model
     */
    private void callToReceptionistPatientDetailFragment(ArrayList<ReceptionistPatientModel> patientModel) {
        try {

            String mFirstName = patientModel.get(0).getFirstName();
            String mLastName = patientModel.get(0).getLastName();
            String mMobile = patientModel.get(0).getMobileNo();
            String mPatientCode = patientModel.get(0).getPatientCode();
            String mPatientId = patientModel.get(0).getPatientID();
            String mUserID = patientModel.get(0).getUserID();

            String Name = mFirstName + AppConstants.STR_EMPTY_SPACE + mLastName;

            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(Name);
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(Name);
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(Name);
            }

            Bundle bundle = new Bundle();
            bundle.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, mFirstName);
            bundle.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, mLastName);
            bundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mMobile);
            bundle.putString(AppConstants.BUNDLE_PATIENT_CODE, mPatientCode);
            bundle.putString(AppConstants.BUNDLE_PATIENT_ID, mPatientId);
            bundle.putString(AppConstants.BUNDLE_ADD_PATIENT_USERID, mUserID);

            SessionManager manager = new SessionManager(mActivity);
            manager.setPreferences(manager.PID, "patient");
            manager.setPreferences(manager.PATIENT_USERID, mUserID);
            manager.setPreferences(manager.FIRSTNAME, mFirstName);
            manager.setPreferences(manager.LASTNAME, mLastName);
            manager.setPreferences(manager.MOBILE, mMobile);

            bundle.putSerializable(AppConstants.BUNDLE_ADD_PATIENT, patientModel.get(0));
            bundle.putString(AppConstants.BUNDLE_LOGIN_TYPE, mUserType);

            Fragment fragment = new ReceptionistPatientDetailFragment();
            fragment.setArguments(bundle);
            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_receptionist_patient_detail))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_receptionist_patient_detail))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTimeCountDown(ReceptionistAppointmentModel data, TextView mTextCheckInDate, int adapterPosition) {
        String currentDate = Common.setCheckCurrentDateTime(mActivity);
        final Handler timerHandler = new Handler();
        timerHandler.removeCallbacks(updater); //stop handler when activity not visible

        updater = new Runnable() {
            @Override
            public void run() {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat
                            (mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_kk_mm_ss_hyphen));
                    Date cDate = sdf.parse(currentDate);
                    Date checkingDate = sdf.parse(data.getCheckINDate());

                    String difference = AppConstants.STR_EMPTY_STRING;
                    Common.insertLog("After" + checkingDate);
                    difference = Common.printDifference(cDate, checkingDate);

                    data.setTimeRemaining(difference);

                    mTextCheckInDate.setText(difference);
                    notifyItemChanged(adapterPosition);
                } catch (Exception e) {
                    Common.insertLog(e.getMessage());
                }
            }
        };
        timerHandler.post(updater);
    }


    /**
     * This Method Call To Add Check In API
     */
    private void callToAddCheckInAPI(ReceptionistAppointmentModel receptionistAppointmentModel,
                                     String type) {
        try {
            String mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
            String mDatabaseName = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);
            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAddCheckInJson(receptionistAppointmentModel.getAppointmentID(),
                            mUserId, mDatabaseName));

            Call<AddPatientModel> call = RetrofitClient.createService(ApiInterface.class).addCheckIn(requestBody);
            call.enqueue(new Callback<AddPatientModel>() {
                @Override
                public void onResponse(@NonNull Call<AddPatientModel> call, @NonNull Response<AddPatientModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful()) {
                            Common.setCustomToast(mActivity, mMessage);
                            if (response.body() != null) {

                                if (type.equalsIgnoreCase("Surgery")) {
                                    redirectedToSurgeryList();
                                } else {
                                    redirectedToCheckInList();
                                }


                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddPatientModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Check In Fragment
     */
    private void redirectedToCheckInList() {
        try {
            Fragment fragment = new CheckInFragment();
            FragmentManager fragmentManager = null;

            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_patient))) {
                ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    mActivity.getResources().getString(R.string.nav_menu_check_up)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Check In Fragment
     */
    private void redirectedToSurgeryList() {
        try {
            Fragment fragment = new SurgeryListFragment();
            FragmentManager fragmentManager = null;

            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_patient))) {
                ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
                fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame, fragment,
                    mActivity.getResources().getString(R.string.nav_menu_surgery)).commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Receptionist Appointment Detail Fragment
     *
     * @param receptionistAppointmentModel - Receptionist Appointment Model
     */
    private void callToReceptionistAppointmentDetailFragment(ReceptionistAppointmentModel receptionistAppointmentModel) {
        try {
            String mFirstName = receptionistAppointmentModel.getPatientFirstName();
            String mLastName = receptionistAppointmentModel.getPatientLastName();
            String mMobile = receptionistAppointmentModel.getPatientMobileNo();
            String mPatientCode = receptionistAppointmentModel.getPatientCode();
            String mPatientId = receptionistAppointmentModel.getPatientID();
            String mAppointmentId = receptionistAppointmentModel.getAppointmentID();
            String mAppointmentNo = receptionistAppointmentModel.getTicketNumber();
            String mBookingType = receptionistAppointmentModel.getAppointmentType();
            String mPaymentStatus = receptionistAppointmentModel.getPaymentStatus();
            String mAppointmentDate = receptionistAppointmentModel.getAppointmentDate();
            String mDoctorName = receptionistAppointmentModel.getDoctorFirstName()
                    + AppConstants.STR_EMPTY_SPACE + receptionistAppointmentModel.getDoctorLastName();

            String Name = mFirstName + AppConstants.STR_EMPTY_SPACE + mLastName;

            Bundle bundle = new Bundle();
            bundle.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, mFirstName);
            bundle.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, mLastName);
            bundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mMobile);
            bundle.putString(AppConstants.BUNDLE_PATIENT_CODE, mPatientCode);
            bundle.putString(AppConstants.BUNDLE_PATIENT_ID, mPatientId);
            bundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_ID, mAppointmentId);
            bundle.putString(AppConstants.BUNDLE_LOGIN_TYPE, mUserType);
            bundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, mAppointmentNo);
            bundle.putString(AppConstants.BUNDLE_APPOINTMENT_TYPE, mBookingType);
            bundle.putString(AppConstants.BUNDLE_PAYMENT_EDIT_MODE, mPaymentStatus);
            bundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DATE, mAppointmentDate);
            bundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, mDoctorName);


            Fragment fragment = new ReceptionistAppointmentDetailFragment();
            fragment.setArguments(bundle);
            FragmentManager fragmentManager = null;

            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(Name);
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(Name);
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(Name);
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(Name);
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_patient))) {
                ((PatientHomeActivity) mActivity).setAppHeader(Name);
                fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_receptionist_patient_detail))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_receptionist_patient_detail))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Open dialog for appointment reschedule
     */
    public void openChangeAppointment(ReceptionistAppointmentModel receptionistAppointmentModel) {
        try {

            String mDate = Common.convertDateUsingDateFormat(mActivity, receptionistAppointmentModel.getAppointmentDate(),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss_hyphen),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));

            String mTime = Common.convertDateUsingDateFormat(mActivity, receptionistAppointmentModel.getAppointmentDate(),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss_hyphen),
                    mActivity.getResources().getString(R.string.date_format_hh_mm_a));


            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

            String type;

            if (receptionistAppointmentModel.getCheckUpType().equals("Surgery")) {
                type = "Check-up";
                builder.setMessage(R.string.dialog_confirmation_appointment_check_in);

            } else {
                type = "Surgery";
                builder.setMessage(R.string.dialog_confirmation_appointment_surgery);

            }

            // builder.setMessage(R.string.dialog_reschedule_appointment_message)
            builder.setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {

                    callToSurgeryAppointmentAPI(mDate, mTime, receptionistAppointmentModel, type);
                }
            })
                    .setNegativeButton(R.string.action_cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            dialog.dismiss();
                        }
                    });
            builder.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Open dialog for appointment reschedule
     */
    public void openChangeAppointmentDialog(ReceptionistAppointmentModel receptionistAppointmentModel) {
        try {
            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.custom_dialog_appointment_reschedule);

            // Edit Texts
            EditText mEditDate = dialog.findViewById(R.id.edit_dialog_appointment_reschedule_date);
            EditText mEditTime = dialog.findViewById(R.id.edit_dialog_appointment_reschedule_time);
            RadioGroup radioGroup = dialog.findViewById(R.id.radio_group_appointment_reschedule_type);
            RadioButton radioButtonCheckUp = dialog.findViewById(R.id.radio_button_appointment_reschedule_type_checkup);
            RadioButton radioButtonSurgery = dialog.findViewById(R.id.radio_button_appointment_reschedule_type_surgery);
            ImageView imageType = dialog.findViewById(R.id.image_appointment_reschedule_type);

            final String[] AppointmentType = {AppConstants.STR_EMPTY_STRING};

            // Button
            Button mButtonOk = dialog.findViewById(R.id.button_dialog_appointment_reschedule_ok);

            // Set current date
            mEditDate.setText(Common.setCurrentDate(mActivity));

            // ToDo: Edit Text Date Click Listener
            mEditDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Common.openDatePicker(mActivity, mEditDate);
                }
            });

            // ToDo: Edit Text Date Click Listener
            mEditTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String doctor_id = receptionistAppointmentModel.getDoctorID();

                    GenerateTimeSlot generateTimeSlot = new GenerateTimeSlot();
                    generateTimeSlot.openTimeSlotDialog(mActivity, mEditTime, mEditDate, doctor_id);
                }
            });
            AppointmentType[0] = "Check-up";

            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                    // This will get the radiobutton that has changed in its check state
                    RadioButton checkedRadioButton = group.findViewById(checkedId);

                    // This will get the radiobutton that has changed in its check state
                    if (checkedRadioButton.getId() == R.id.radio_button_appointment_reschedule_type_surgery) {
                        //some code
                        AppointmentType[0] = "Surgery";
                    } else if (checkedRadioButton.getId() == R.id.radio_button_appointment_reschedule_type_checkup) {
                        //some code
                        AppointmentType[0] = "Check-up";
                    }
                }
            });

            // ToDo: Button Ok Click Listener
            mButtonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    doRescheduleAppointment(dialog, mEditDate, mEditTime,
                            receptionistAppointmentModel, AppointmentType[0]);
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method checks the validation first then call the API
     */
    private void doRescheduleAppointment(Dialog dialog, EditText mEditDate,
                                         EditText mEditTime, ReceptionistAppointmentModel receptionistAppointmentModel,
                                         String type) {
        KeyboardUtility.HideKeyboard(mActivity, mEditDate);

        if (checkValidation(mEditTime)) {
            openConfirmationDialog(dialog, mEditDate.getText().toString(),
                    mEditTime.getText().toString(), receptionistAppointmentModel, type);
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation(EditText mEditTime) {
        boolean status = true;

        if (mEditTime.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.val_select_time));
            status = false;
        }
        return status;
    }

    /**
     * Open confirmation popup for appointment reschedule
     */
    private void openConfirmationDialog(Dialog dialog, String mDate, String mTime, ReceptionistAppointmentModel receptionistAppointmentModel, String type) {
        try {
            dialog.dismiss();
            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
            builder.setMessage(R.string.dialog_reschedule_appointment_message)
                    .setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            callToRescheduleAppointmentAPI(mDate, mTime, receptionistAppointmentModel, type);
                        }
                    })
                    .setNegativeButton(R.string.action_cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            dialog.dismiss();
                        }
                    });
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the reschedule API for Appointment
     *
     * @param mDate - Date
     * @param mTime - Time
     * @param type
     */
    private void callToRescheduleAppointmentAPI(String mDate, String mTime, ReceptionistAppointmentModel receptionistAppointmentModel, String type) {
        try {
            String mConvertedDate = Common.convertDateToServer(mActivity, mDate);
            String mConvertedTime = Common.convertTime(mActivity, mTime);

            String hospital_database = GetJsonData.getHospitalData(mActivity,
                    AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAppointmentRescheduleJson(mConvertedDate, mUserId,
                            receptionistAppointmentModel.getAppointmentID(),
                            receptionistAppointmentModel.getPatientID(), mConvertedTime, hospital_database, type));

            Call<RescheduleModel> call = RetrofitClient.createService(ApiInterface.class).setAppointmentReschedule(requestBody);
            call.enqueue(new Callback<RescheduleModel>() {
                @Override
                public void onResponse(@NonNull Call<RescheduleModel> call, @NonNull Response<RescheduleModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && response.body().getError() == 200) {
                            Common.setCustomToast(mActivity, mMessage);

                            sClickListener.onItemSelected(mConvertedDate);
                        } else {
                            Common.setCustomToast(mActivity, response.body().getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<RescheduleModel> call, @NonNull Throwable t) {
                    Common.insertLog(t.getMessage());
                    //Common.setCustomToast(mActivity, response.body().getMessage());
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * This method should call the reschedule API for Appointment
     *
     * @param mDate - Date
     * @param mTime - Time
     * @param type
     */
    private void callToSurgeryAppointmentAPI(String mDate, String mTime,
                                             ReceptionistAppointmentModel receptionistAppointmentModel,
                                             String type) {
        try {
            String mConvertedDate = Common.convertDateToServer(mActivity, mDate);
            String mConvertedTime = Common.convertTime(mActivity, mTime);

            String hospital_database = GetJsonData.getHospitalData(mActivity,
                    AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAppointmentRescheduleJson(mConvertedDate, mUserId,
                            receptionistAppointmentModel.getAppointmentID(),
                            receptionistAppointmentModel.getPatientID(), mConvertedTime, hospital_database, type));

            Call<RescheduleModel> call = RetrofitClient.createService(ApiInterface.class).setAppointmentReschedule(requestBody);
            call.enqueue(new Callback<RescheduleModel>() {
                @Override
                public void onResponse(@NonNull Call<RescheduleModel> call, @NonNull Response<RescheduleModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && response.body().getError() == 200) {

                            callToAddCheckInAPI(receptionistAppointmentModel, type);

                        } else {
                            Common.setCustomToast(mActivity, response.body().getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<RescheduleModel> call, @NonNull Throwable t) {
                    Common.insertLog(t.getMessage());
                    //Common.setCustomToast(mActivity, response.body().getMessage());
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Open dialog for appointment reasons
     *
     * @param holder                       - RecyclerView View Holder
     * @param receptionistAppointmentModel - Receptionist Appointment Model
     */
    public void openCancelDialog(final RecyclerView.ViewHolder holder, ReceptionistAppointmentModel receptionistAppointmentModel) {
        try {
            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.custom_dialog_appointment_reason);

            // Recycler View
            RecyclerView mRecyclerView = dialog.findViewById(R.id.recycler_view_dialog_appointment_reasons);

            // Text View
            TextView mTextNoData = dialog.findViewById(R.id.text_dialog_appointment_reasons_no_data);

            // Button
            Button mButtonOk = dialog.findViewById(R.id.button_dialog_appointment_reasons_ok);

            // ToDo: Sets up the data and bind it to the adapter
            setAdapterData(mRecyclerView, mTextNoData, ((CustomViewHolder) holder).mTextReason.getText().toString());

            // ToDo: Button Ok Click Listener
            mButtonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    /*sClickListener.onItemClickListener(v, mSelReason,mSelReasonID,
                            ((CustomViewHolder) holder).mLinearReason,
                            ((CustomViewHolder) holder).mTextReason);*/

                    callToAppointmentReject(v, receptionistAppointmentModel, ((CustomViewHolder) holder).mLinearReason,
                            ((CustomViewHolder) holder).mTextReason);
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Call To Redirect Open Document List
     *
     * @param receptionistAppointmentModel
     */
    private void redirectToDocumentList(ReceptionistAppointmentModel receptionistAppointmentModel) {
        try {
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, receptionistAppointmentModel.getAppointmentID());

            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_document));
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_document));
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_document));
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_document));
            }

            Fragment fragment = new ReceptionistPatientDetailDocumentFragment();
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                fragmentManager =
                        ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                fragmentManager =
                        ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                fragmentManager =
                        ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                fragmentManager =
                        ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_receptionist_patient_detail_document))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_receptionist_patient_detail_document))
                    .commit();
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Call To Redirect Open Prescription List
     *
     * @param receptionistAppointmentModel
     */
    private void redirectToPrescriptionList(ReceptionistAppointmentModel receptionistAppointmentModel) {
        //ReceptionistPatientDetailPrescriptionFragment

        try {
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_ID, receptionistAppointmentModel.getPatientID());

            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_prescription));
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_prescription));
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_prescription));
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_prescription));
            }

            Fragment fragment = new ReceptionistPatientDetailPrescriptionFragment();
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                fragmentManager =
                        ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                fragmentManager =
                        ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                fragmentManager =
                        ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                fragmentManager =
                        ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_receptionist_patient_detail_prescription))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_receptionist_patient_detail_prescription))
                    .commit();
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }


    /**
     * Call Appointment Cancel API Call
     *
     * @param mView                        - View
     * @param receptionistAppointmentModel - Receptionist Appointment Model
     * @param mLinearReason                - Linear Layout Reason
     * @param mTextReason                  - Text View Reason
     */
    private void callToAppointmentReject(View mView, ReceptionistAppointmentModel receptionistAppointmentModel, LinearLayout mLinearReason, TextView mTextReason) {
        try {
            mAppointmentId = receptionistAppointmentModel.getAppointmentID();
            String TimeDeference = "20";
            mPatientId = receptionistAppointmentModel.getPatientID();

            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            String date = receptionistAppointmentModel.getAppointmentDate();

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setAppointmentReject(mSelReasonID, mUserId, mAppointmentId, TimeDeference, mPatientId, hospital_database));

            Call<AddAppointmentModel> call = RetrofitClient.createService(ApiInterface.class).setAppointmentReject(requestBody);
            call.enqueue(new Callback<AddAppointmentModel>() {
                @Override
                public void onResponse(@NonNull Call<AddAppointmentModel> call, @NonNull Response<AddAppointmentModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    if (response.isSuccessful() && response.body().getError() == 200) {

                        String convert_dd_mm_yyyy = Common.convertDateUsingDateFormat(mActivity,
                                date,
                                mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_kk_mm_ss_hyphen),
                                mActivity.getResources().getString(R.string.date_format_yyyy_mm_dd));
                        sClickListener.onItemSelected(convert_dd_mm_yyyy);
                        //sClickListener.onItemSelected(date);
                    } else {
                        Common.setCustomToast(mActivity, response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<AddAppointmentModel> call, @NonNull Throwable t) {
                    Common.insertLog(t.getMessage());
                    //Common.setCustomToast(mActivity,response.body().getMessage());
                }
            });

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Sets up the data and bind it to the adapter
     */
    private void setAdapterData(RecyclerView mRecyclerView, TextView mTextNoData, String mSelectedReason) {
        try {
            mRecyclerView.setHasFixedSize(true);
            LinearLayoutManager mLinearLayoutManager = new LinearLayoutManager(mActivity);
            mRecyclerView.setLayoutManager(mLinearLayoutManager);

            if (mSelectedReason.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                mSelReason = mArrAppointmentReasonsModel.get(0).getReasons();
                mSelReasonID = mArrAppointmentReasonsModel.get(0).getReasonID();
            } else {
                mSelReason = mSelectedReason;
            }

            mAdapterAppointmentReasons = new AppointmentReasonsAdapter(mActivity, mArrAppointmentReasonsModel, mSelReason);
            mRecyclerView.setAdapter(mAdapterAppointmentReasons);
            mAdapterAppointmentReasons.setOnItemClickListener(this);

            setViewVisibility(mTextNoData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the view visibility as per the array size
     */
    private void setViewVisibility(TextView mTextNoData) {
        if (mArrReceptionistAppointmentModel.size() != 0) {
            mTextNoData.setVisibility(View.GONE);
            mRecyclerView.setVisibility(View.VISIBLE);
        } else {
            mTextNoData.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mArrReceptionistAppointmentModel == null ? 0 : mArrReceptionistAppointmentModel.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrReceptionistAppointmentModel.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    /**
     * Delcare Inteface On Item Selected
     */
    public interface SingleClickListener {
        void onItemSelected(String date);
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder implements ExpandableLayout.OnExpansionUpdateListener {
        RelativeLayout mRelativeImageBorder;
        LinearLayout mLinearReason, mLinearReschedule, mLinearCancel, mLinearCheckIn;
        Button button_checkin;
        ImageView mImageUserProfilePic, mImageStatus, mImageExpandableArrow,
                mImageWalkInPatient, mImageReport, mImagePrescription;
        LinearLayout mLinearExpandableArrow, mLinearDocument, mLinearLocations, mLinearBottom, mLinearSurgery;
        ExpandableLayout mExpandableLayout;
        TextView mTextMRDNo, mTextCheckInDate, mTextName, mTextMobileNo, mTextAppointmentNo, mTextAppointmentDate, mTextDoctorName,
                mTextBookingType, mTextAppointmentType, mTextReason, mTextPaymentType, mTextStatus, mTextSelLocations, mTextLocations,
                mTextPaymentAmount, mTextCancel, mTextPatientCode;


        Button mButtonSurgery;

        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Relative Layouts
            this.mRelativeImageBorder = itemView.findViewById(R.id.relative_row_receptionist_appointment_pic);

            // Linear Layouts
            this.mLinearReason = itemView.findViewById(R.id.linear_row_receptionist_appointment_reason);
            this.mLinearReschedule = itemView.findViewById(R.id.linear_row_receptionist_appointment_reschedule);
            this.mLinearCancel = itemView.findViewById(R.id.linear_row_receptionist_appointment_Cancel);
            this.mLinearCheckIn = itemView.findViewById(R.id.linear_row_receptionist_appointment_check_in);
            this.mLinearBottom = itemView.findViewById(R.id.layout_receptionist_appointment_reschedule);

            // Image Views
            this.mImageUserProfilePic = itemView.findViewById(R.id.image_row_receptionist_appointment_pic);
            this.mImageStatus = itemView.findViewById(R.id.image_row_receptionist_appointment_status);
            this.mImageExpandableArrow = itemView.findViewById(R.id.image_row_receptionist_appointment_expandable_view);
            this.mImageWalkInPatient = itemView.findViewById(R.id.image_row_receptionist_appointment_walk_in_patient);
            this.mImageReport = itemView.findViewById(R.id.image_row_receptionist_appointment_report);
            this.mImagePrescription = itemView.findViewById(R.id.image_row_receptionist_appointment_prescription);

            // Linear Layouts
            this.mLinearExpandableArrow = itemView.findViewById(R.id.linear_row_receptionist_appointment_expandable_view);
            this.mLinearDocument = itemView.findViewById(R.id.linear_row_receptionist_appointment_report);
            this.mLinearLocations = itemView.findViewById(R.id.linear_row_receptionist_appointment_locations);
            this.mLinearSurgery = itemView.findViewById(R.id.linear_row_receptionist_appointment_surgery);

            //Button
            this.button_checkin = itemView.findViewById(R.id.button_checkin);

            // Expandable Layout
            this.mExpandableLayout = itemView.findViewById(R.id.expandable_layout_row_receptionist_appointment);

            // Text Views
            //this.mTextMRDNo = itemView.findViewById(R.id.ribbon_view_row_receptionist_appointment_mrd_no);
            this.mTextCheckInDate = itemView.findViewById(R.id.text_row_receptionist_appointment_check_in_date);
            this.mTextName = itemView.findViewById(R.id.text_row_receptionist_appointment_name);
            this.mTextMobileNo = itemView.findViewById(R.id.text_row_receptionist_appointment_mobile_no);
            this.mTextAppointmentNo = itemView.findViewById(R.id.text_row_receptionist_appointment_number);
            this.mTextAppointmentDate = itemView.findViewById(R.id.text_row_receptionist_appointment_date);
            this.mTextDoctorName = itemView.findViewById(R.id.text_row_receptionist_appointment_doctor_name);
            this.mTextBookingType = itemView.findViewById(R.id.text_row_receptionist_appointment_booking_type);
            this.mTextAppointmentType = itemView.findViewById(R.id.text_row_receptionist_appointment_type);
            this.mTextReason = itemView.findViewById(R.id.text_row_receptionist_appointment_reason);
            this.mTextPaymentType = itemView.findViewById(R.id.text_row_receptionist_appointment_payment_type);
            this.mTextStatus = itemView.findViewById(R.id.text_row_receptionist_appointment_status);
            this.mTextSelLocations = itemView.findViewById(R.id.text_row_receptionist_appointment_sel_locations);
            this.mTextLocations = itemView.findViewById(R.id.text_row_receptionist_appointment_locations);
            this.mTextPaymentAmount = itemView.findViewById(R.id.text_row_receptionist_appointment_payment_amount);
            this.mTextCancel = itemView.findViewById(R.id.text_row_receptionist_appointment_Cancel);
            this.mTextPatientCode = itemView.findViewById(R.id.text_row_receptionist_patient_code);
            this.mButtonSurgery = itemView.findViewById(R.id.button_row_receptionist_appointment_surgery);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Common.insertLog("ExpandableLayout State : " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                mRecyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }

        /**
         * Binds the expandable view
         */
        public void bind() {
            int position = getAdapterPosition();
            boolean isSelected = position == selectedItem;

            mLinearExpandableArrow.setSelected(isSelected);
            mExpandableLayout.setExpanded(isSelected, false);
        }
    }
}
