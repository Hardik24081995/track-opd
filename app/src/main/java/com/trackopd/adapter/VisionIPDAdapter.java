package com.trackopd.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.trackopd.R;
import com.trackopd.model.PreliminaryExaminationDetailsModel;

import java.util.ArrayList;

public class VisionIPDAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity mActivity;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.IPD> mArrIPD;

    public VisionIPDAdapter(Activity mActivity, ArrayList<PreliminaryExaminationDetailsModel.Data.IPD> _mArrIPD) {
        this.mActivity=mActivity;
        this.mArrIPD=_mArrIPD;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view= LayoutInflater.from(mActivity.getBaseContext()).inflate(R.layout.row_vision_ipd,null,false);
        return new customHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i)
    {
        if (holder instanceof customHolder){
            PreliminaryExaminationDetailsModel.Data.IPD ipd=mArrIPD.get(i);
            ((customHolder) holder).mCheckBox.setText(ipd.getIPD());


            if (mArrIPD.get(holder.getAdapterPosition()).isSelected()){
                ((customHolder) holder).mCheckBox.setChecked(true);
            }else {
                ((customHolder) holder).mCheckBox.setChecked(false);
            }

            ((customHolder) holder).mCheckBox.setChecked(Boolean.parseBoolean(ipd.getStatus()));

            ((customHolder) holder).mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (buttonView.isChecked()){
                        //ipd.setSelected(true);
                        changeStatus(holder.getAdapterPosition(),true);
                    }else {
                        //ipd.setSelected(false);
                        changeStatus(holder.getAdapterPosition(),false);
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return mArrIPD.size();
    }

    protected void changeStatus(int adapterPosition, boolean b){

    }


    class customHolder extends RecyclerView.ViewHolder{

        private CheckBox mCheckBox;

        public customHolder(@NonNull View itemView) {
            super(itemView);
            this.mCheckBox=itemView.findViewById(R.id.check_box_row_vision_ipd);
        }
    }
}
