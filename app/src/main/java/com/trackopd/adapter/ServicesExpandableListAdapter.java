package com.trackopd.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.trackopd.R;
import com.trackopd.model.ServicesPackagesModel;

import java.util.ArrayList;

public class ServicesExpandableListAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    private ArrayList<ServicesPackagesModel.Service> mServicesArrayList;
    private ExpandableListView mExpandableListView;
    private boolean isOffer = false;
    public static int ServiceAndPackageLimit = 5;


    public ServicesExpandableListAdapter(Context context, ArrayList<ServicesPackagesModel.Service> ServiceArrayList
            , ExpandableListView expandServiceListView) {

        this.mContext = context;
        this.isOffer = isOffer;
        this.mExpandableListView = expandServiceListView;
        this.mServicesArrayList = ServiceArrayList;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return mServicesArrayList.get(groupPosition).getItem().size();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        String serviceTitle = mServicesArrayList.get(groupPosition).
                getItem().get(childPosition).getServicesName();

        String servicePrice=mServicesArrayList.get(groupPosition).getItem()
                .get(childPosition).getRate();

        boolean checked = mServicesArrayList.get(groupPosition).
                getItem().get(childPosition).isSelected();


        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_services_list_child_item, null);
        }

        LinearLayout llParent=convertView.findViewById(R.id.linear_row_service_list_child_item);
        TextView txtTitle = (TextView) convertView.findViewById(R.id.text_row_service_list_child_item_name);
        TextView txtPrice = (TextView) convertView.findViewById(R.id.text_row_service_list_child_item_price);


        final CheckBox checkBox = (CheckBox) convertView.findViewById(R.id.checkbox_row_service_list_child_item_check);

        txtTitle.setText(serviceTitle);
        txtPrice.setText(servicePrice);
        checkBox.setChecked(checked);
        txtTitle.setSelected(true);

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedServices(groupPosition, childPosition, checkBox);
            }
        });

        llParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedServices(groupPosition, childPosition, checkBox);

            }
        });

        return convertView;
    }

    private void selectedServices(int groupPosition, int childPosition,
                                  CheckBox checkBox) {

        if (!mServicesArrayList.get(groupPosition).getItem().get(childPosition)
                .isSelected()) {
            mServicesArrayList.get(groupPosition).getItem().get(childPosition)
                    .setSelected(true);

            checkBox.setChecked(true);
            addItem(mServicesArrayList.get(groupPosition), childPosition);
        } else {
            mServicesArrayList.get(groupPosition).getItem().get(childPosition)
                    .setSelected(false);
            checkBox.setChecked(false);

            removeItem(mServicesArrayList.get(groupPosition), childPosition);
        }
    }


    protected void addItem(ServicesPackagesModel.Service packages, int position) {

    }

    protected void removeItem(ServicesPackagesModel.Service packages, int position) {

    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mServicesArrayList.get(groupPosition).getItem().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mServicesArrayList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return mServicesArrayList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        String headerTitle = mServicesArrayList.get(groupPosition).getServicesName();
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.services_list_group_item, null);
        }

        TextView txtTitle = (TextView) convertView
                .findViewById(R.id.text_row_services_list_group_title);
        ImageView image_down = convertView.findViewById(R.id.image_row_services_list_group_arrow);

        if (isExpanded) {
            txtTitle.setCompoundDrawablePadding(10);
            image_down.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_up_arrow));
        } else {
            txtTitle.setCompoundDrawablePadding(10);
            image_down.setImageDrawable(mContext.getResources().getDrawable(R.drawable.ic_down_arrow));
        }

        txtTitle.setSelected(true);
        txtTitle.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
