package com.trackopd.adapter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.model.AnatomicalLocationModel;
import com.trackopd.utils.AppConstants;

import java.util.ArrayList;

public class AddPreliminaryExaminationAnatomicalLocationAdapter extends RecyclerView.Adapter {

    private Activity mActivity;
    private LayoutInflater mLayoutInflater;
    private ArrayList<AnatomicalLocationModel> mArrAnatomicalLocationModel;
    private String mSelLocation;

    public AddPreliminaryExaminationAnatomicalLocationAdapter(Activity mActivity, ArrayList<AnatomicalLocationModel> mArrAnatomicalLocationModel, String mSelectedLocation) {
        this.mActivity = mActivity;
        this.mArrAnatomicalLocationModel = mArrAnatomicalLocationModel;
        mSelLocation = mSelectedLocation;
        this.mLayoutInflater = LayoutInflater.from(mActivity);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = mLayoutInflater.inflate(R.layout.row_add_preliminary_examination_anatomical_location_item, parent,
                false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, final int position) {
        final AnatomicalLocationModel anatomicalLocationModel = mArrAnatomicalLocationModel.get(position);

        ((CustomViewHolder) viewHolder).mTextAnatomicalLocation.setText(anatomicalLocationModel.getName());

        ((CustomViewHolder) viewHolder).mCheckBoxAnatomicalLocation.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                anatomicalLocationModel.setStatus(isChecked);
            }
        });

        ((CustomViewHolder) viewHolder).mCheckBoxAnatomicalLocation.setChecked(anatomicalLocationModel.isStatus());

        String[] selected = mSelLocation.split(AppConstants.STR_COMMA_SEPARATOR);
        for (int j = 0; j < selected.length; j++) {
            if (selected[j].equals(anatomicalLocationModel.getName())) {
                ((CustomViewHolder) viewHolder).mCheckBoxAnatomicalLocation.setChecked(true);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mArrAnatomicalLocationModel.size();
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView mTextAnatomicalLocation;
        CheckBox mCheckBoxAnatomicalLocation;

        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);

            // Text View
            mTextAnatomicalLocation = itemView.findViewById(R.id.text_row_add_preliminary_examination_anatomical_location);

            // Check Box
            mCheckBoxAnatomicalLocation = itemView.findViewById(R.id.checkbox_row_add_preliminary_examination_anatomical_location);
        }
    }
}
