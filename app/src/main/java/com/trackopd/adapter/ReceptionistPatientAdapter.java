package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.fragments.AddAppointmentFragment;
import com.trackopd.fragments.AddFollowUpFragment;
import com.trackopd.fragments.AddInquiryFormFragment;
import com.trackopd.fragments.AddSurgeryPatientDetailFragment;
import com.trackopd.fragments.ReceptionistPatientDetailFragment;
import com.trackopd.model.ReceptionistPatientModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.SessionManager;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;
import com.yalantis.flipviewpager.adapter.BaseFlipAdapter;
import com.yalantis.flipviewpager.utils.FlipSettings;

import java.util.ArrayList;

public class ReceptionistPatientAdapter extends BaseFlipAdapter {

    private Activity mActivity;
    private String mIsFromSearch, mAddPatient, mUserType;
    private RecyclerView mRecyclerView;

    // Load More Listener Variables
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;

    public ReceptionistPatientAdapter(Activity mActivity, RecyclerView mRecyclerView, ArrayList<ReceptionistPatientModel> mArrReceptionistPatients, String mIsFromSearch, String mAddPatient, FlipSettings settings) {
        super(mArrReceptionistPatients, settings);
        this.mActivity = mActivity;
        this.mRecyclerView = mRecyclerView;
        this.mIsFromSearch = mIsFromSearch;
        this.mAddPatient = mAddPatient;
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        setOnScrollListener();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getPage(int position, View convertView, ViewGroup parent, Object patientModel, CloseListener closeListener) {
        final CustomViewHolder holder;

        if (convertView == null) {
            holder = new CustomViewHolder();

            // ToDo: For Main View
            holder.mMainView = mActivity.getLayoutInflater().inflate(R.layout.row_receptionist_patient_item, parent, false);

            // Image Views
            holder.mImageUserProfilePic = holder.mMainView.findViewById(R.id.image_row_receptionist_patient_pic);

            // Relative Layouts
            holder.mRelativeImageBorder = holder.mMainView.findViewById(R.id.relative_row_receptionist_patient_pic);

            // Linear Layouts
            holder.mLinearAppointment = holder.mMainView.findViewById(R.id.linear_row_receptionist_patient_appointment);
            holder.mLinearFollowUp = holder.mMainView.findViewById(R.id.linear_row_receptionist_patient_follow_up);
            holder.mLinearActionBottom = holder.mMainView.findViewById(R.id.linear_row_receptionist_patient_bottom_actions);

            // Text Views
            holder.mTextName = holder.mMainView.findViewById(R.id.text_row_receptionist_patient_name);
            holder.mTextMobileNo = holder.mMainView.findViewById(R.id.text_row_receptionist_patient_mobile_no);
            holder.mTextPatientCode = holder.mMainView.findViewById(R.id.text_row_receptionist_patient_code);
            holder.mTextLastVisitedDate = holder.mMainView.findViewById(R.id.text_row_receptionist_patient_last_visited_date);
            holder.mTextAppointment = holder.mMainView.findViewById(R.id.text_row_receptionist_patient_appointment);
            holder.mTextFollowUp = holder.mMainView.findViewById(R.id.text_row_receptionist_patient_follow_up);


            holder.mTextMobileNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:" + ((ReceptionistPatientModel) patientModel).getMobileNo()));
                    mActivity.startActivity(intent);

                }
            });
            // Flip View
            holder.mFlipView = holder.mMainView.findViewById(R.id.view_row_receptionist_patient_bottom_actions);

            // ToDo: For Flip View
            convertView = mActivity.getLayoutInflater().inflate(R.layout.row_receptionist_patient_flip_view, parent, false);

            // Image Views
            holder.mImageFlipView = convertView.findViewById(R.id.image_row_receptionist_patient_flip_view);

            // Text Views
            holder.mTextFlipName = convertView.findViewById(R.id.text_row_receptionist_patient_flip_name);
            holder.mTextFlipMobile = convertView.findViewById(R.id.text_row_receptionist_patient_flip_mobile);
            holder.mTextFlipCode = convertView.findViewById(R.id.text_row_receptionist_patient_flip_code);
            holder.mTextFlipAge = convertView.findViewById(R.id.text_row_receptionist_patient_flip_age);
            holder.mTextFlipGender = convertView.findViewById(R.id.text_row_receptionist_patient_flip_gender);

            convertView.setTag(holder);
        } else {
            holder = (CustomViewHolder) convertView.getTag();
        }

        switch (position) {
            case 1:
                FlipViewHolder(holder, position, (ReceptionistPatientModel) patientModel);
                holder.mMainView.setTag(holder);
                return holder.mMainView;

            default:

                if (((ReceptionistPatientModel) patientModel).getProfileImage() != null &&
                        !((ReceptionistPatientModel) patientModel).getProfileImage().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {

                    //Set full Image
                    String full_image_url = WebFields.API_BASE_URL + WebFields.IMAGE_URL + ((ReceptionistPatientModel) patientModel).getProfileImage();

                    Glide.with(mActivity)
                            .load(full_image_url)
                            .apply(new RequestOptions().error(R.drawable.ic_documents_img).placeholder(R.drawable.ic_documents_img))
                            .into(holder.mImageFlipView);
                }

                holder.mTextFlipName.setText(Common.isEmptyString(mActivity, ((ReceptionistPatientModel) patientModel).getFirstName() + AppConstants.STR_EMPTY_SPACE + ((ReceptionistPatientModel) patientModel).getLastName()));
                holder.mTextFlipMobile.setText(Common.isEmptyString(mActivity, ((ReceptionistPatientModel) patientModel).getMobileNo()));
                holder.mTextFlipCode.setText(Common.isEmptyString(mActivity, ((ReceptionistPatientModel) patientModel).getPatientCode()));

                holder.mTextFlipMobile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Intent.ACTION_DIAL);
                        intent.setData(Uri.parse("tel:" + ((ReceptionistPatientModel) patientModel).getMobileNo()));
                        mActivity.startActivity(intent);
                    }
                });

                if (((ReceptionistPatientModel) patientModel).getAgeYear().equalsIgnoreCase("0") ||
                        ((ReceptionistPatientModel) patientModel).getAgeYear().equalsIgnoreCase("1")) {
                    holder.mTextFlipAge.setText(Common.isEmptyString(mActivity, ((ReceptionistPatientModel) patientModel).getAgeYear()
                            + AppConstants.STR_EMPTY_SPACE + mActivity.getResources().getString(R.string.text_year))
                            + AppConstants.STR_COMMA_SEPARATOR + ((ReceptionistPatientModel) patientModel).getAgeMonth()
                            + AppConstants.STR_EMPTY_SPACE + mActivity.getResources().getString(R.string.text_months));
                } else {
                    holder.mTextFlipAge.setText(Common.isEmptyString(mActivity, ((ReceptionistPatientModel) patientModel).getAgeYear()
                            + AppConstants.STR_EMPTY_SPACE + mActivity.getResources().getString(R.string.text_years))
                            + AppConstants.STR_COMMA_SEPARATOR + ((ReceptionistPatientModel) patientModel).getAgeMonth()
                            + AppConstants.STR_EMPTY_SPACE + mActivity.getResources().getString(R.string.text_months));
                }

                holder.mTextFlipGender.setText(Common.isEmptyString(mActivity, ((ReceptionistPatientModel) patientModel).getGender()));
                break;
        }
        return convertView;
    }

    @Override
    public int getPagesCount() {
        return 3;
    }

    @SuppressLint("SetTextI18n")
    private void FlipViewHolder(CustomViewHolder holder, int pos, final ReceptionistPatientModel receptionistPatientModel) {
        if (receptionistPatientModel == null)
            return;

        holder.mTextName.setTag(pos);
        holder.mLinearAppointment.setTag(pos);
        holder.mLinearFollowUp.setTag(pos);

        holder.mRelativeImageBorder.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.user_image_border));

        // Set Image Profile Picture
        if (receptionistPatientModel.getProfileImage() != null &&
                !receptionistPatientModel.getProfileImage().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {

            //Set Thumbnail
            String thumbmail_url = WebFields.API_BASE_URL + WebFields.IMAGE_THUMBNAIL_URL + receptionistPatientModel.getProfileImage();
            Glide.with(mActivity)
                    .load(thumbmail_url)
                    .apply(new RequestOptions().error(R.drawable.ic_documents_img).placeholder(R.drawable.ic_documents_img))
                    .into(holder.mImageUserProfilePic);
        } else {
            holder.mImageUserProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, receptionistPatientModel.getFirstName(), receptionistPatientModel.getLastName()));
        }


        String mPatientName = receptionistPatientModel.getFirstName() + AppConstants.STR_EMPTY_SPACE + receptionistPatientModel.getLastName();
        holder.mTextName.setText(Common.isEmptyString(mActivity, mPatientName));
        holder.mTextMobileNo.setText(Common.isEmptyString(mActivity, receptionistPatientModel.getMobileNo()));
        holder.mTextPatientCode.setText(Common.isEmptyString(mActivity, receptionistPatientModel.getPatientCode()));
        holder.mTextLastVisitedDate.setText(mActivity.getResources().getString(R.string.text_visited_on) + AppConstants.STR_EMPTY_SPACE + Common.isEmptyString(mActivity, receptionistPatientModel.getLastVisitedDate()));

        // ToDo: Appointment Click Listener
        holder.mLinearAppointment.setOnClickListener(v -> {
            //openPopup(receptionistPatientModel);
            callToAddReceptionistAppointmentFragment(receptionistPatientModel);
        });

        // ToDo: Follow-up Click Listener
        holder.mLinearFollowUp.setOnClickListener(v -> callToAddFollowUpFragment(receptionistPatientModel));

        if (mIsFromSearch.equalsIgnoreCase(AppConstants.RECEPTIONIST_PATIENT) ||
                mIsFromSearch.equalsIgnoreCase(AppConstants.OPTOMETRIST_PATIENT)) {
            holder.mFlipView.setVisibility(View.GONE);
            holder.mLinearActionBottom.setVisibility(View.GONE);
        } else {
            holder.mFlipView.setVisibility(View.GONE);
            holder.mLinearActionBottom.setVisibility(View.GONE);
        }

        // ToDo: Full View Click Listener
        holder.mTextName.setOnClickListener(v -> {
            if (mIsFromSearch.equals(AppConstants.RECEPTIONIST_PATIENT)) {

                if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                    callToPatientPrescription(receptionistPatientModel);
                } else {
                    callToReceptionistPatientDetailFragment(receptionistPatientModel);
                }
            }
            if (mIsFromSearch.equalsIgnoreCase(AppConstants.BUNDLE_ADD_FOLLOW_UP)) {
                callToAddFollowUpFragment(receptionistPatientModel);
            }

            if (mIsFromSearch.equalsIgnoreCase(AppConstants.BUNDLE_ADD_APPOINTMENT)) {
                callToAddReceptionistAppointmentFragment(receptionistPatientModel);
            }

            if (mIsFromSearch.equalsIgnoreCase(AppConstants.OPTOMETRIST_PRELIMINARY_EXAMINATION)) {
                callToAddPreliminaryExaminationFragment(receptionistPatientModel);
            }

            if (mIsFromSearch.equalsIgnoreCase(AppConstants.OPTOMETRIST_PATIENT)) {
                callToPatientPrescription(receptionistPatientModel);
            }

            if (mIsFromSearch.equalsIgnoreCase(AppConstants.BUNDLE_ADD_COUNCILLOR)) {
                callToAddCouncillorInquiryFormFragment(receptionistPatientModel);
            }

            if (mIsFromSearch.equalsIgnoreCase(AppConstants.BUNDLE_ADD_SURGERY)) {
                callToAddSurgeryFragment(receptionistPatientModel);
            }
        });
    }

    /**
     * Sets up the Councillor Add Inquiry Form Fragment
     *
     * @param receptionistPatientsModel - Receptionist Patients Model
     */
    private void callToAddCouncillorInquiryFormFragment(ReceptionistPatientModel receptionistPatientsModel) {
        try {
            ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.header_counselling));
            Fragment fragment = new AddInquiryFormFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, receptionistPatientsModel.getFirstName());
            args.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, receptionistPatientsModel.getLastName());
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, receptionistPatientsModel.getMobileNo());
            args.putString(AppConstants.BUNDLE_PATIENT_ID, receptionistPatientsModel.getUserID());
            args.putString(AppConstants.BUNDLE_ADD_PATIENT, mAddPatient);
            args.putString(AppConstants.BUNDLE_PATIENT_PROFILE, receptionistPatientsModel.getProfileImage());

            fragment.setArguments(args);
            FragmentManager fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_councillor_inquiry_form))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_councillor_inquiry_form))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Add Surgery Form Fragment
     *
     * @param receptionistPatientModel - Receptionist Patients Model
     */
    private void callToAddSurgeryFragment(ReceptionistPatientModel receptionistPatientModel) {
        try {
            Bundle args = new Bundle();
            args.putSerializable(AppConstants.BUNDLE_ADD_PATIENT, receptionistPatientModel);
            args.putString(AppConstants.BUNDLE_IS_FROM_SEARCH, AppConstants.BUNDLE_ADD_SURGERY);


            Fragment fragment = new AddSurgeryPatientDetailFragment();
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
                fragmentManager =
                        ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
                fragmentManager =
                        ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
                fragmentManager =
                        ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
                fragmentManager =
                        ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_surgery))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_surgery))
                    .commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Optometrist Add New Preliminary Examination Fragment
     *
     * @param receptionistPatientModel - Receptionist Patients Model
     */
    private void callToAddPreliminaryExaminationFragment(ReceptionistPatientModel receptionistPatientModel) {
        try {

            Bundle args = new Bundle();
            args.putSerializable(AppConstants.BUNDLE_ADD_PATIENT, receptionistPatientModel);
            args.putString(AppConstants.BUNDLE_IS_FROM_SEARCH, AppConstants.OPTOMETRIST_PRELIMINARY_EXAMINATION);

//            Fragment fragment = new AddNewPreliminaryExaminationFragment();
//            fragment.setArguments(args);

            Fragment fragment = new AddSurgeryPatientDetailFragment();
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_preliminary_examination));
                fragmentManager =
                        ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_preliminary_examination));
                fragmentManager =
                        ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_preliminary_examination));
                fragmentManager =
                        ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_preliminary_examination));
                fragmentManager =
                        ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_preliminary_examination))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_preliminary_examination))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Optometrist Login Select Patient to  List Patient Prescription
     *
     * @param receptionistPatientModel - Receptionist Patient Model
     */
    private void callToPatientPrescription(ReceptionistPatientModel receptionistPatientModel) {
        try {
            ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.nav_menu_preliminary_examination));

           /* Bundle mBundle = new Bundle();
            mBundle.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, receptionistPatientModel.getFirstName());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, receptionistPatientModel.getLastName());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE,receptionistPatientModel.getMobileNo());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_PROFILE, receptionistPatientModel.getProfileImage());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_ID,receptionistPatientModel.getPatientID());

            Fragment fragment = new AddNewPreliminaryExaminationFragment();
            fragment.setArguments(mBundle);*/

            Bundle args = new Bundle();
            args.putSerializable(AppConstants.BUNDLE_ADD_PATIENT, receptionistPatientModel);
            args.putString(AppConstants.BUNDLE_IS_FROM_SEARCH, AppConstants.BUNDLE_ADD_PRELIMINARY_EXAM);


            Fragment fragment = new AddSurgeryPatientDetailFragment();
            fragment.setArguments(args);

            /* Fragment fragment = new PatientPrescriptionFragment();*/
          /*  args.putString(AppConstants.BUNDLE_PATIENT_ID, receptionistPatientModel.getUserID());
            fragment.setArguments(args);*/

            FragmentManager fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_preliminary_examination))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_preliminary_examination))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Receptionist Add Follow Up Fragment
     *
     * @param receptionistPatientsModel - Receptionist Patient Model
     */
    private void callToAddFollowUpFragment(ReceptionistPatientModel receptionistPatientsModel) {
        try {
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_follow_up));
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_follow_up));
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_follow_up));
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_follow_up));
            }

            Fragment fragment = new AddFollowUpFragment();
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, receptionistPatientsModel.getFirstName());
            args.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, receptionistPatientsModel.getLastName());
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, receptionistPatientsModel.getMobileNo());
            args.putString(AppConstants.BUNDLE_PATIENT_ID, receptionistPatientsModel.getUserID());
            args.putString(AppConstants.BUNDLE_ADD_PATIENT, mAddPatient);
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                fragmentManager =
                        ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                fragmentManager =
                        ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                fragmentManager =
                        ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                fragmentManager =
                        ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_receptionist_follow_up))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_receptionist_follow_up))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Receptionist Patient Detail Fragment
     *
     * @param receptionistPatientModel - Receptionist Patient Model
     */
    private void callToReceptionistPatientDetailFragment(ReceptionistPatientModel receptionistPatientModel) {
        try {

            String mFirstName = receptionistPatientModel.getFirstName();
            String mLastName = receptionistPatientModel.getLastName();
            String mMobile = receptionistPatientModel.getMobileNo();
            String mPatientCode = receptionistPatientModel.getPatientCode();
            String mPatientId = receptionistPatientModel.getPatientID();
            String mUserID = receptionistPatientModel.getUserID();

            String Name = mFirstName + AppConstants.STR_EMPTY_SPACE + mLastName;

            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(Name);
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(Name);
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(Name);
            }

            Bundle bundle = new Bundle();
            bundle.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, mFirstName);
            bundle.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, mLastName);
            bundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mMobile);
            bundle.putString(AppConstants.BUNDLE_PATIENT_CODE, mPatientCode);
            bundle.putString(AppConstants.BUNDLE_PATIENT_ID, mPatientId);
            bundle.putString(AppConstants.BUNDLE_ADD_PATIENT_USERID, mUserID);

            SessionManager manager = new SessionManager(mActivity);
            manager.setPreferences(manager.PID, "patient");
            manager.setPreferences(manager.PATIENT_USERID, mUserID);
            manager.setPreferences(manager.FIRSTNAME, mFirstName);
            manager.setPreferences(manager.LASTNAME, mLastName);
            manager.setPreferences(manager.MOBILE, mMobile);

            bundle.putSerializable(AppConstants.BUNDLE_ADD_PATIENT, receptionistPatientModel);
            bundle.putString(AppConstants.BUNDLE_LOGIN_TYPE, mUserType);

            Fragment fragment = new ReceptionistPatientDetailFragment();
            fragment.setArguments(bundle);
            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_receptionist_patient_detail))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_receptionist_patient_detail))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * `
     * Sets up the Receptionist Add New Appointment Fragment
     *
     * @param receptionistPatientsModel - Receptionist Patients Model
     */
    private void callToAddReceptionistAppointmentFragment(ReceptionistPatientModel receptionistPatientsModel) {
        try {
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, receptionistPatientsModel.getFirstName());
            args.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, receptionistPatientsModel.getLastName());
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, receptionistPatientsModel.getMobileNo());
            args.putString(AppConstants.BUNDLE_PATIENT_ID, receptionistPatientsModel.getPatientID());
            args.putString(AppConstants.BUNDLE_ADD_PATIENT_USERID, receptionistPatientsModel.getUserID());
            args.putString(AppConstants.BUNDLE_ADD_PATIENT, mAddPatient);
            args.putString(AppConstants.BUNDLE_PATIENT_PROFILE, receptionistPatientsModel.getProfileImage());

            SessionManager manager = new SessionManager(mActivity);
            manager.setPreferences(manager.PID, "patientAppointment");
            manager.setPreferences(manager.PATIENT_USERID, receptionistPatientsModel.getUserID());

            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            }

            Fragment fragment = new AddAppointmentFragment();
            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                fragmentManager =
                        ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                fragmentManager =
                        ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                fragmentManager =
                        ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                fragmentManager =
                        ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_receptionist_appointment))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_receptionist_appointment))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    class CustomViewHolder {
        RelativeLayout mRelativeImageBorder;
        ImageView mImageFlipView, mImageUserProfilePic;
        View mMainView, mFlipView;
        LinearLayout mLinearAppointment, mLinearFollowUp, mLinearActionBottom;
        TextView mTextFlipName, mTextFlipMobile, mTextFlipCode, mTextFlipAge, mTextFlipGender, mTextName, mTextMobileNo, mTextPatientCode, mTextLastVisitedDate, mTextAppointment, mTextFollowUp;
    }
}
