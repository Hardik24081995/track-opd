package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.fragments.AddPreliminaryExaminationFragment;
import com.trackopd.fragments.ChangeStatusFragment;
import com.trackopd.model.StatusTabListModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public class RoleWisePatientListAdapter extends RecyclerView.Adapter {

    private Activity mActivity;
    private ArrayList<StatusTabListModel> mArrRoleWisePatient;
    private static final int unselectedItem = -1;
    private int selectedItem = unselectedItem;

    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private String mUserType;

    public RoleWisePatientListAdapter(Activity mActivity, RecyclerView mRecyclerView, ArrayList<StatusTabListModel> mArrRoleWisePatient) {
        this.mActivity = mActivity;
        this.mRecyclerView = mRecyclerView;
        this.mArrRoleWisePatient = mArrRoleWisePatient;
        LayoutInflater mLayoutInflater = LayoutInflater.from(mActivity);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        setOnScrollListener();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout
                    .row_role_wise_patinet_list, viewGroup, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout
                    .loading_more_data_footer, viewGroup, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (holder instanceof CustomViewHolder) {
            final StatusTabListModel mRoleWisePatient = mArrRoleWisePatient.get(position);

            ((CustomViewHolder) holder).mLinearExpandableArrow.setTag(position);

            ((CustomViewHolder) holder).mRelativeImageBorder.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.user_image_border));

            ((CustomViewHolder) holder).mImageUserProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, mRoleWisePatient.getPatientFirstName(),
                    mRoleWisePatient.getPatientLastName()));

            String mPatientName = mRoleWisePatient.getPatientFirstName() + AppConstants.STR_EMPTY_SPACE + mRoleWisePatient.getPatientLastName();
            String mDoctorName = mRoleWisePatient.getAsigneeName();

            ((CustomViewHolder) holder).mTextName.setText(Common.isEmptyString(mActivity, mPatientName));
            ((CustomViewHolder) holder).mTextMobileNo.setText(Common.isEmptyString(mActivity, mRoleWisePatient.getPatientMobileNo()));
            ((CustomViewHolder) holder).mTextAppointmentNo.setText(Common.isEmptyString(mActivity, mRoleWisePatient.getTicketNumber()));
            ((CustomViewHolder) holder).mTextAppointmentDate.setText(Common.isEmptyString(mActivity, mRoleWisePatient.getAppointmentDate()));
            ((CustomViewHolder) holder).mTextDoctorName.setText(Common.isEmptyString(mActivity, mDoctorName));
            ((CustomViewHolder) holder).mTextBookingType.setText(Common.isEmptyString(mActivity, mRoleWisePatient.getCurrentStatus()));
            ((CustomViewHolder) holder).mTextPaymentType.setText(Common.isEmptyString(mActivity, mRoleWisePatient.getPaymentStatus()));

            int pos = holder.getAdapterPosition();
            if (pos != selectedItem) {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                ((CustomViewHolder) holder).mExpandableLayout.collapse();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
            } else {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                ((CustomViewHolder) holder).mExpandableLayout.expand();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
            }

            // ToDo: Expandable Arrow Click Listener
            ((CustomViewHolder) holder).bind();
            ((CustomViewHolder) holder).mLinearExpandableArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                    ((CustomViewHolder) holder).mExpandableLayout.collapse();
                    ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));

                    Common.insertLog("Position:::> " + position);
                    Common.insertLog("selectedItem:::> " + selectedItem);

                    int pos = holder.getAdapterPosition();
                    if (pos == selectedItem) {
                        selectedItem = unselectedItem;
                    } else {
                        ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                        ((CustomViewHolder) holder).mExpandableLayout.expand();
                        ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        selectedItem = pos;
                    }
                    notifyDataSetChanged();
                }
            });

            ((CustomViewHolder) holder).mLinearChangeStatus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callToChangeStatusOnPatient(mRoleWisePatient);
                }
            });

            ((CustomViewHolder) holder).mTextName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mRoleWisePatient.getCurrentStatus().equalsIgnoreCase(AppConstants.EXAMINATION_BY_OPTOMETRIST)
                         ||  mRoleWisePatient.getCurrentStatus().equalsIgnoreCase(AppConstants.EXAMINATION_BY_DOCTOR)){
                        callToPreliminary(mRoleWisePatient);
                    }

                }
            });

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }



    @Override
    public int getItemCount() {
        return mArrRoleWisePatient == null ? 0 : mArrRoleWisePatient.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrRoleWisePatient.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    /**
     * This Call To Change Status
     *
     * @param statusTabListModel - Status Tab List Model
     */
    private void callToChangeStatusOnPatient(StatusTabListModel statusTabListModel) {
        try {
            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_change_status));
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_change_status));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_change_status));
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_change_status));
            }

            Bundle bundle = new Bundle();
            bundle.putSerializable(AppConstants.BUNDLE_PATIENT_FIRST_NAME, statusTabListModel.getPatientFirstName());
            bundle.putSerializable(AppConstants.BUNDLE_PATIENT_LAST_NAME, statusTabListModel.getPatientLastName());
            bundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE, statusTabListModel.getPatientMobileNo());
            bundle.putString(AppConstants.BUNDLE_PATIENT_CODE, statusTabListModel.getPatientCode());
            bundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_STATUS, statusTabListModel.getCurrentStatus());
            bundle.putString(WebFields.CHANGE_STATUS.REQUEST_APPOINTMENT_ID,statusTabListModel.getAppointmentID());
            bundle.putString(WebFields.CHANGE_STATUS.REQUEST_PATIENT_ID,statusTabListModel.getPatientID());
            bundle.putString(WebFields.CHANGE_STATUS.REQUEST_ACTION_USER_ID,statusTabListModel.getAsigneeName());
            bundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO,statusTabListModel.getTicketNumber());

            Fragment fragment = new ChangeStatusFragment();
            fragment.setArguments(bundle);
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_change_status_role))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_change_status_role))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *  Call To Add Preliminary Examination only Doctor and Optometrist
     * @param mRoleWisePatient - Add Preliminary Examination
     *
     */
    private void callToPreliminary(StatusTabListModel mRoleWisePatient) {
        try {

            if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))
                || mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor)))
            {
                FragmentManager fragmentManager = null;
                if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                    fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
                    ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_change_status));
                }
                if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
                    fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
                    ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources().getString(R.string.nav_menu_change_status));
                }

                Bundle args = new Bundle();
                args.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, mRoleWisePatient.getPatientFirstName());
                args.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, mRoleWisePatient.getPatientLastName());
                args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mRoleWisePatient.getPatientMobileNo());
                args.putString(AppConstants.BUNDLE_PATIENT_ID, mRoleWisePatient.getPatientID());
                args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO,mRoleWisePatient.getAppointmentID());


                Fragment fragment = new AddPreliminaryExaminationFragment();
                fragment.setArguments(args);
                fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                        fragment, mActivity.getResources()
                                .getString(R.string.tag_change_status_role))
                        .addToBackStack(mActivity.getResources()
                                .getString(R.string.back_stack_change_status_role))
                        .commit();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder implements ExpandableLayout.OnExpansionUpdateListener {

        RelativeLayout mRelativeImageBorder;
        LinearLayout mLinearChangeStatus;
        ImageView mImageUserProfilePic, mImageStatus, mImageExpandableArrow;
        LinearLayout mLinearExpandableArrow;
        ExpandableLayout mExpandableLayout;
        TextView mTextName, mTextMobileNo, mTextAppointmentNo, mTextAppointmentDate, mTextDoctorName,
                mTextBookingType, mTextAppointmentType, mTextPaymentType, mTextChangeStatus;

        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Relative Layouts
            this.mRelativeImageBorder = itemView.findViewById(R.id.relative_row_role_wise_patient_pic);

            // Image Views
            this.mImageUserProfilePic = itemView.findViewById(R.id.image_row_role_wise_patient_pic);
            this.mImageStatus = itemView.findViewById(R.id.image_row_role_wise_patient_status);
            this.mImageExpandableArrow = itemView.findViewById(R.id.image_row_role_wise_patient_expandable_view);

            // Linear Layouts
            this.mLinearChangeStatus = itemView.findViewById(R.id.linear_row_role_wise_patient_change_status);
            this.mLinearExpandableArrow = itemView.findViewById(R.id.linear_row_role_wise_patient_expandable_view);

            // Expandable Layout
            this.mExpandableLayout = itemView.findViewById(R.id.expandable_layout_row_role_wise_patient);

            // Text Views
            this.mTextName = itemView.findViewById(R.id.text_row_role_wise_patient_name);
            this.mTextMobileNo = itemView.findViewById(R.id.text_row_role_wise_patient_mobile_no);
            this.mTextAppointmentNo = itemView.findViewById(R.id.text_row_role_wise_patient_number);
            this.mTextAppointmentDate = itemView.findViewById(R.id.text_row_role_wise_patient_date);
            this.mTextDoctorName = itemView.findViewById(R.id.text_row_role_wise_patient_doctor_name);
            this.mTextBookingType = itemView.findViewById(R.id.text_row_role_wise_patient_booking_type);
            this.mTextAppointmentType = itemView.findViewById(R.id.text_row_role_wise_patient_type);
            this.mTextPaymentType = itemView.findViewById(R.id.text_row_role_wise_patient_payment_type);
            this.mTextChangeStatus = itemView.findViewById(R.id.text_row_role_wise_patient_change_status);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Common.insertLog("ExpandableLayout State : " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                mRecyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }

        /**
         * Binds the expandable view
         */
        public void bind() {
            int position = getAdapterPosition();
            boolean isSelected = position == selectedItem;

            mLinearExpandableArrow.setSelected(isSelected);
            mExpandableLayout.setExpanded(isSelected, false);
        }
    }
}
