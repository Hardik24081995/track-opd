package com.trackopd.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.fragments.CheckUpCounselingFragment;
import com.trackopd.fragments.InvestigationFragment;
import com.trackopd.model.CouselingPackageSuggestionModel;

import java.util.ArrayList;
import java.util.List;

public class InvestigationSpinnerAdapter extends BaseAdapter{

    private Activity mActivity;
    private ArrayList<CouselingPackageSuggestionModel> mArrPackageSuggested;
    private String  mSelectedSuggestion;
    public   String mType;
    private  InvestigationSpinnerListner listener;
    private int position;
    private Spinner mySpinner;
    private LayoutInflater mInflater;



    public InvestigationSpinnerAdapter(Activity mActivity,
                 ArrayList<CouselingPackageSuggestionModel> mOcularInvestigation)
    {
        this.mActivity=mActivity;
        this.mArrPackageSuggested=mOcularInvestigation;
    }

    @Override
    public int getCount() {
        return mArrPackageSuggested.size();
    }

    @Override
    public Object getItem(int position) {
        return mArrPackageSuggested.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        LayoutInflater layoutInflator = LayoutInflater.from(mActivity);
        convertView = layoutInflator.inflate(R.layout.row_couseling_package_suggested, parent,false);
        holder = new ViewHolder();
        holder.mTextView = (TextView) convertView
                .findViewById(R.id.text_row_couseling_package_suggested_name);
        holder.mCheckBox = (CheckBox) convertView
                .findViewById(R.id.checkbox_row_couseling_package_suggested);

        holder.mLinearPackageSuggested=convertView
                .findViewById(R.id.linear_couseling_package_suggested);

        convertView.setTag(holder);

        if (position==0){
            holder.mCheckBox.setVisibility(View.GONE);
        }else {
            holder.mCheckBox.setVisibility(View.VISIBLE);
        }

        holder.mTextView.setText(mArrPackageSuggested.get(position).getOpetionSuggested());

        if (mArrPackageSuggested.get(position).getChecked()==true){
           holder.mCheckBox.setChecked(true);
        }else {
           holder.mCheckBox.setChecked(false);
        }

        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    //model.setChecked(true);
                    mArrPackageSuggested.get(position).setChecked(true);

                }else {
                    mArrPackageSuggested.get(position).setChecked(false);
                }


            }
        });
        return convertView;
    }

    protected void update(String selected)
    {

    }


    private class ViewHolder {
        private LinearLayout mLinearPackageSuggested;
        private TextView mTextView;
        private CheckBox mCheckBox;
    }

    public interface InvestigationSpinnerListner{
        void onItemUpdate(String type,int pos);
    }
}
