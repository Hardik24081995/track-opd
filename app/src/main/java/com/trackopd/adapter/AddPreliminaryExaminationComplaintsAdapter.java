package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import java.util.ArrayList;

public class AddPreliminaryExaminationComplaintsAdapter extends RecyclerView.Adapter {

    private Activity mActivity;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.PrimaryComplain> mArrComplaints;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.PrimaryComplain> mArrSelectedComplaints;
    private ArrayList<PreliminaryExaminationDetailsModel.Data.PrimaryComplain> mArrOriginalComplaints;
    private ArrayList<String> mComplaints = new ArrayList<>();
    private String mSelectedValue, mUserType;
    private static SingleClickListener sClickListener;

    public AddPreliminaryExaminationComplaintsAdapter(Activity mActivity, String mUserType, String selectedValue, ArrayList<PreliminaryExaminationDetailsModel.Data.PrimaryComplain> mArrComplaints) {
        this.mActivity = mActivity;
        this.mUserType = mUserType;
        mSelectedValue = selectedValue;
        this.mArrComplaints = mArrComplaints;
        this.mArrOriginalComplaints=mArrComplaints;
        mArrSelectedComplaints = new ArrayList<>();
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
    }

    public AddPreliminaryExaminationComplaintsAdapter(Activity mActivity, String mUserType, String mComplaints) {
    }

    /**
     * Create an interface for the recycler view click events
     */
    public void setOnItemClickListener(SingleClickListener clickListener) {
        sClickListener = clickListener;
    }

    /**
     * Update List Data
     * @param mArrComplaints
     */
    public void updateList(ArrayList<PreliminaryExaminationDetailsModel.Data.PrimaryComplain> mArrComplaints) {
        this.mArrComplaints = mArrComplaints;
        notifyDataSetChanged();
    }


    public interface SingleClickListener {
        void onComplaintsCheckBoxClickListener(String mComplaints);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .row_add_preliminary_examination_anatomical_location_item, parent, false);
        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
       final PreliminaryExaminationDetailsModel.Data.PrimaryComplain primaryComplainModel = mArrComplaints.get(position);

        ((CustomViewHolder) holder).mCheckBox.setTag(position);

        ((CustomViewHolder) holder).mTextTitle.setSelected(true);
        ((CustomViewHolder) holder).mTextTitle.setText(Common.isEmptyString(mActivity, primaryComplainModel.getPrimaryComplains()));


        if(!mSelectedValue.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            mComplaints = new ArrayList<>();
            String[] mSplitValue = mSelectedValue.split(",");
            for (int j = 0; j < mSplitValue.length; j++) {
                String mSelected = mSplitValue[j];
                mComplaints.add(mSelected);
                if (mSelected.equals(primaryComplainModel.getPrimaryComplains())) {
                    ((CustomViewHolder) holder).mCheckBox.setChecked(true);
                }
            }
        }

        if(mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist)) || mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {
            ((CustomViewHolder) holder).mCheckBox.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    CheckBox cb = (CheckBox) v;
                    if (cb.isChecked()) {
                        mArrSelectedComplaints.clear();
                        mArrSelectedComplaints.add(primaryComplainModel);
                        for (int j = 0; j < mArrSelectedComplaints.size(); j++) {
                            String mComplaintsName = mArrSelectedComplaints.get(j).getPrimaryComplains();
                            mComplaints.add(mComplaintsName);
                        }
                    } else {
                        mComplaints.remove(primaryComplainModel.getPrimaryComplains());
                        mArrSelectedComplaints.remove(primaryComplainModel);
                    }
                    String mFinalComplaints = mComplaints.toString().replaceAll("\\[", "").replaceAll("\\]", "").replace(", ", ",");
                    sClickListener.onComplaintsCheckBoxClickListener(mFinalComplaints);
                    Common.insertLog("Sel Complaints:::> " + mFinalComplaints);
                }
            });
        } else {
            ((CustomViewHolder) holder).mCheckBox.setClickable(false);
        }
    }


    @Override
    public int getItemCount() {
        return mArrComplaints.size();
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder {

        CheckBox mCheckBox;
        TextView mTextTitle;

        public CustomViewHolder(final View itemView) {
            super(itemView);
            this.setIsRecyclable(false);

            // Check Box
            this.mCheckBox = itemView.findViewById(R.id.checkbox_row_add_preliminary_examination_anatomical_location);

            // Text Views
            this.mTextTitle = itemView.findViewById(R.id.text_row_add_preliminary_examination_anatomical_location);
        }
    }
}
