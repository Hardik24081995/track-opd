package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.model.CouncillorConsultingPatientModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public class CouncillorConsultingPatientAdapter extends RecyclerView.Adapter {
    private Activity mActivity;
    private ArrayList<CouncillorConsultingPatientModel> mArrCouncillorConsultingPatientModel;
    private LayoutInflater mLayoutInflater;
    private RecyclerView mRecyclerView;
    private static final int unselectedItem = -1;
    private int selectedItem = unselectedItem;

    /**
     * Adapter contains the data to be displayed
     */
    public CouncillorConsultingPatientAdapter(Activity mActivity, RecyclerView mRecyclerView, ArrayList<CouncillorConsultingPatientModel> mArrCouncillorConsultingPatientModel) {
        this.mActivity = mActivity;
        this.mRecyclerView = mRecyclerView;
        this.mArrCouncillorConsultingPatientModel = mArrCouncillorConsultingPatientModel;
        mLayoutInflater = LayoutInflater.from(mActivity);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.row_councillor_consulting_patient_item, parent,
                false);
        return new CustomViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final CouncillorConsultingPatientModel councillorConsultingPatientModel = mArrCouncillorConsultingPatientModel.get(position);

        if (councillorConsultingPatientModel.getProfilePic().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            ((CustomViewHolder) holder).mImageUserProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, councillorConsultingPatientModel.getFirstName(),
                    councillorConsultingPatientModel.getLastName()));
        } else {
            // Set image
        }

        String mPatientName = councillorConsultingPatientModel.getFirstName() + AppConstants.STR_EMPTY_SPACE + councillorConsultingPatientModel.getLastName();
        ((CustomViewHolder) holder).mTextName.setText(Common.isEmptyString(mActivity, mPatientName));
        ((CustomViewHolder) holder).mTextMobileNo.setText(Common.isEmptyString(mActivity, councillorConsultingPatientModel.getMobileNo()));
        ((CustomViewHolder) holder).mTextPatientCode.setText(Common.isEmptyString(mActivity, councillorConsultingPatientModel.getPatientCode()));
        ((CustomViewHolder) holder).mTextLastVisitedDate.setText(mActivity.getResources().getString(R.string.text_visited_on) + Common.isEmptyString(mActivity, councillorConsultingPatientModel.getLastVisitedDate()));

        int pos = holder.getAdapterPosition();
        if (pos != selectedItem) {
            ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
            ((CustomViewHolder) holder).mExpandableLayout.collapse();
            ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
        } else {
            ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
            ((CustomViewHolder) holder).mExpandableLayout.expand();
            ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
        }

        // ToDo: Expandable Arrow Click Listener
        ((CustomViewHolder) holder).bind();
        ((CustomViewHolder) holder).mLinearExpandableArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                ((CustomViewHolder) holder).mExpandableLayout.collapse();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));

                Common.insertLog("Position:::> " + position);
                Common.insertLog("selectedItem:::> " + selectedItem);

                int pos = holder.getAdapterPosition();
                if (pos == selectedItem) {
                    selectedItem = unselectedItem;
                } else {
                    ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                    ((CustomViewHolder) holder).mExpandableLayout.expand();
                    ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                    selectedItem = pos;
                }
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrCouncillorConsultingPatientModel.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder implements ExpandableLayout.OnExpansionUpdateListener {
        ImageView mImageUserProfilePic, mImageExpandableArrow;
        LinearLayout mLinearExpandableArrow;
        ExpandableLayout mExpandableLayout;
        TextView mTextName, mTextMobileNo, mTextPatientCode, mTextLastVisitedDate, mTextAppointment, mTextFollowUp;

        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Image Views
            this.mImageUserProfilePic = itemView.findViewById(R.id.image_row_councillor_consulting_patient_pic);
            this.mImageExpandableArrow = itemView.findViewById(R.id.image_row_councillor_consulting_patient_expandable_view);

            // Linear Layouts
            this.mLinearExpandableArrow = itemView.findViewById(R.id.linear_row_councillor_consulting_patient_expandable_view);

            // Expandable Layout
            this.mExpandableLayout = itemView.findViewById(R.id.expandable_layout_row_councillor_consulting_patient);

            // Text Views
            this.mTextName = itemView.findViewById(R.id.text_row_councillor_consulting_patient_name);
            this.mTextMobileNo = itemView.findViewById(R.id.text_row_councillor_consulting_patient_mobile_no);
            this.mTextPatientCode = itemView.findViewById(R.id.text_row_councillor_consulting_patient_code);
            this.mTextLastVisitedDate = itemView.findViewById(R.id.text_row_councillor_consulting_patient_last_visited_date);
            this.mTextAppointment = itemView.findViewById(R.id.text_row_councillor_consulting_patient_appointment);
            this.mTextFollowUp = itemView.findViewById(R.id.text_row_councillor_consulting_patient_follow_up);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Common.insertLog("ExpandableLayout State : " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                mRecyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }

        /**
         * Binds the expandable view
         */
        public void bind() {
            int position = getAdapterPosition();
            boolean isSelected = position == selectedItem;

            mLinearExpandableArrow.setSelected(isSelected);
            mExpandableLayout.setExpanded(isSelected, false);
        }
    }
}
