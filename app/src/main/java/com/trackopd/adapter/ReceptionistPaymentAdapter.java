package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.fragments.AddFollowUpFragment;
import com.trackopd.fragments.AddPaymentFragment;
import com.trackopd.fragments.AddPaymentStepTwoFragment;
import com.trackopd.fragments.DiagnosisFragment;
import com.trackopd.fragments.ReceptionistAppointmentDetailFragment;
import com.trackopd.model.ReceptionistAppointmentModel;
import com.trackopd.model.ReceptionistPatientModel;
import com.trackopd.model.ReceptionistPaymentModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public class ReceptionistPaymentAdapter extends RecyclerView.Adapter {
    private Activity mActivity;
    private ArrayList<ReceptionistPaymentModel> mArrReceptionistPaymentModel;
    private static final int unselectedItem = -1;
    private int selectedItem = unselectedItem;

    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading,isClicked=false;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private  String mUserType;
    
    /**
     * Adapter contains the data to be displayed
     */
    public ReceptionistPaymentAdapter(Activity mActivity, RecyclerView mRecyclerView, ArrayList<ReceptionistPaymentModel> mArrReceptionistPayment, boolean status) {
        this.mActivity = mActivity;
        this.mRecyclerView = mRecyclerView;
        this.mArrReceptionistPaymentModel = mArrReceptionistPayment;
        LayoutInflater mLayoutInflater = LayoutInflater.from(mActivity);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);
        isClicked=status;
        setOnScrollListener();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_receptionist_payment_item, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (holder instanceof CustomViewHolder) {
            final ReceptionistPaymentModel receptionistPaymentModel = mArrReceptionistPaymentModel.get(position);

            ((CustomViewHolder) holder).mLinearExpandableArrow.setTag(position);

            // Set Image Profile Picture
            if (receptionistPaymentModel.getProfileImage()!=null &&
                    !receptionistPaymentModel.getProfileImage().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){

                //Set Thumbnail
                String thumbmail_url= WebFields.API_BASE_URL+WebFields.IMAGE_THUMBNAIL_URL+receptionistPaymentModel.getProfileImage();
                Glide.with(mActivity)
                        .load(thumbmail_url)
                        .apply(new RequestOptions().error(R.drawable.ic_documents_img).placeholder(R.drawable.ic_documents_img))
                        .into(((CustomViewHolder) holder).mImageUserProfilePic);
            }else {
                ((CustomViewHolder) holder).mImageUserProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, receptionistPaymentModel.getPatientFirstName(), receptionistPaymentModel.getPatientLastName()));
            }

            ((CustomViewHolder) holder).mRelativePicture.setBackground(mActivity.getResources().getDrawable(R.drawable.user_image_border));

            String mPatientName = receptionistPaymentModel.getPatientFirstName() + AppConstants.STR_EMPTY_SPACE + receptionistPaymentModel.getPatientLastName();
            ((CustomViewHolder) holder).mTextName.setText(Common.isEmptyString(mActivity, mPatientName));
            ((CustomViewHolder) holder).mTextMobileNo.setText(Common.isEmptyString(mActivity, receptionistPaymentModel.getPatientMobileNo()));
            ((CustomViewHolder) holder).mTextAmount.setText(Common.isEmptyString(mActivity, receptionistPaymentModel.getAmount()));
            ((CustomViewHolder) holder).mTextAppointmentDate.setText(Common.isEmptyString(mActivity, receptionistPaymentModel.getPaymentDate()));
            ((CustomViewHolder) holder).mTextAppointmentNo.setText(Common.isEmptyString(mActivity, receptionistPaymentModel.getPatientCode()));

            int pos = holder.getAdapterPosition();
            if (pos != selectedItem) {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                ((CustomViewHolder) holder).mExpandableLayout.collapse();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
            } else {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                ((CustomViewHolder) holder).mExpandableLayout.expand();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
            }

            // ToDo: Expandable Arrow Click Listener
            ((CustomViewHolder) holder).bind();
            ((CustomViewHolder) holder).mLinearExpandableArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                    ((CustomViewHolder) holder).mExpandableLayout.collapse();
                    ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));

                    Common.insertLog("Position:::> " + position);
                    Common.insertLog("selectedItem:::> " + selectedItem);
                    int pos = holder.getAdapterPosition();
                    if (pos == selectedItem) {
                        selectedItem = unselectedItem;
                        ((CustomViewHolder) holder).mExpandableLayout.collapse();

                    } else {
                        ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                        ((CustomViewHolder) holder).mExpandableLayout.expand();
                        ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        selectedItem = pos;
                    }
                    notifyDataSetChanged();
                }
            });
     
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_patient))){
                ((CustomViewHolder) holder).mImageEdit.setVisibility(View.GONE);
            }
            
            
            //Edit icon Click Listener
            ((CustomViewHolder) holder).mImageEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                       callToEditAddPaymentFragment(receptionistPaymentModel);
                }
            });

            ((CustomViewHolder) holder).mTextName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   if (isClicked){
                       callToReceptionistAppointmentDetailFragment(receptionistPaymentModel);
                   }
                }
            });
            
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    /**
     * Sets up the Receptionist Add Follow Up Fragment
     *
     * @param receptionistPaymentModel - Receptionist Payment Model
     */
    private void callToEditAddPaymentFragment(ReceptionistPaymentModel receptionistPaymentModel) {
        try {
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_payment));
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_payment));
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_payment));
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_payment));
            }

            Fragment fragment = new AddPaymentStepTwoFragment();
           // Fragment fragment = new AddPaymentFragment();

            //((AddPaymentFragment) fragment).setSecoundPage();

            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PAYMENT_EDIT_MODE,AppConstants.BUNDLE_EDIT_PAYMENT);
            args.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, receptionistPaymentModel.getPatientFirstName());
            args.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, receptionistPaymentModel.getPatientLastName());
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, receptionistPaymentModel.getPatientMobileNo());
            args.putString(AppConstants.BUNDLE_PATIENT_ID, receptionistPaymentModel.getPatientCode());
            args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, receptionistPaymentModel.getAppointmentID());
            args.putString(AppConstants.BUNDLE_PATIENT_PAYMENT_AMOUNT, receptionistPaymentModel.getAmount());
            args.putString(AppConstants.BUNDLE_PATIENT_PAYMENT_DATE, receptionistPaymentModel.getPaymentDate());
            args.putString(AppConstants.BUNDLE_PATIENT_PAYMENT_HISTORY_ID, receptionistPaymentModel.getPaymentHistoryID());
            args.putString(AppConstants.BUNDLE_PATIENT_PROFILE, receptionistPaymentModel.getProfileImage());

            fragment.setArguments(args);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                fragmentManager =
                        ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                fragmentManager =
                        ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                fragmentManager =
                        ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                fragmentManager =
                        ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_receptionist_payment))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_receptionist_payment))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
//        return mArrReceptionistPaymentModel.size();
        return mArrReceptionistPaymentModel == null ? 0 : mArrReceptionistPaymentModel.size();
    }

    @Override
    public int getItemViewType(int position) {
//        return position;
        return mArrReceptionistPaymentModel.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

   /* *
     * Sets up the Receptionist Appointment Detail Fragment
     *
     * @param receptionistAppointmentModel - Receptionist Appointment Model
     * @param receptionistPaymentModel*/

    private void callToReceptionistAppointmentDetailFragment(ReceptionistPaymentModel receptionistPaymentModel) {
        try {


            String mFirstName = receptionistPaymentModel.getPatientFirstName();
            String mLastName = receptionistPaymentModel.getPatientLastName();
            String mMobile = receptionistPaymentModel.getPatientMobileNo();
            String mPatientCode = receptionistPaymentModel.getPatientCode();
            String mPatientId = receptionistPaymentModel.getPatientID();
            String mAppointmentId = receptionistPaymentModel.getAppointmentID();
            String mAppointmentNo = receptionistPaymentModel.getTicketNumber();
            String mBookingType = receptionistPaymentModel.getPatientID();
            String mAppointmentDate = receptionistPaymentModel.getAppointmentDate();
            String mDoctorName = "";
            String mPaymentStatus = "";


            Bundle bundle = new Bundle();

            bundle.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, mFirstName);
            bundle.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, mLastName);
            bundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE, mMobile);
            bundle.putString(AppConstants.BUNDLE_PATIENT_CODE, mPatientCode);
            bundle.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_ID, -1);
            bundle.putString(AppConstants.BUNDLE_LOGIN_TYPE, mUserType);
            bundle.putString(AppConstants.BUNDLE_IS_FROM_SEARCH,"Payment");

            bundle.putString(AppConstants.BUNDLE_PATIENT_ID, mPatientId);

            bundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_ID, mAppointmentId);
            bundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, mAppointmentNo);
            bundle.putString(AppConstants.BUNDLE_APPOINTMENT_TYPE, mBookingType);
            bundle.putString(AppConstants.BUNDLE_PAYMENT_EDIT_MODE,mPaymentStatus);
            bundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DATE,mAppointmentDate);
            bundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR,mDoctorName);


            Fragment fragment = new ReceptionistAppointmentDetailFragment();
            fragment.setArguments(bundle);
            FragmentManager fragmentManager = null;

            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_patient))) {
                ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
                fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_receptionist_patient_detail))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_receptionist_patient_detail))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * Id declarations
     */
    public class CustomViewHolder extends ViewHolder implements ExpandableLayout.OnExpansionUpdateListener {
        RelativeLayout mRelativePicture;
        ImageView mImageUserProfilePic, mImageExpandableArrow,mImageEdit;
        LinearLayout mLinearExpandableArrow;
        ExpandableLayout mExpandableLayout;
        TextView mTextName, mTextMobileNo, mTextAmount, mTextAppointmentDate, mTextAppointmentNo;

        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Relative Layouts
            this.mRelativePicture = itemView.findViewById(R.id.relative_row_receptionist_payment_pic);

            // Image Views
            this.mImageUserProfilePic = itemView.findViewById(R.id.image_row_receptionist_payment_pic);
            this.mImageExpandableArrow = itemView.findViewById(R.id.image_row_receptionist_payment_expandable_view);
            this.mImageEdit = itemView.findViewById(R.id.image_row_receptionist_payment_edit_image);

            // Linear Layouts
            this.mLinearExpandableArrow = itemView.findViewById(R.id.linear_row_receptionist_payment_expandable_view);

            // Expandable Layout
            this.mExpandableLayout = itemView.findViewById(R.id.expandable_layout_row_receptionist_payment);

            // Text Views
            this.mTextName = itemView.findViewById(R.id.text_row_receptionist_payment_name);
            this.mTextMobileNo = itemView.findViewById(R.id.text_row_receptionist_payment_mobile_no);
            this.mTextAmount = itemView.findViewById(R.id.text_row_receptionist_payment_amount);
            this.mTextAppointmentDate = itemView.findViewById(R.id.text_row_receptionist_payment_appointment_date);
            this.mTextAppointmentNo = itemView.findViewById(R.id.text_row_receptionist_payment_appointment_number);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Common.insertLog("ExpandableLayout State : " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                mRecyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }

        /**
         * Binds the expandable view
         */
        public void bind() {
            int position = getAdapterPosition();
            boolean isSelected = position == selectedItem;

            mLinearExpandableArrow.setSelected(isSelected);
            mExpandableLayout.setExpanded(isSelected, false);
        }
    }
}
