package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import java.util.ArrayList;
import java.util.List;

public class PreliminaryExaminationComplaintsAdapter extends RecyclerView.Adapter {

    private Activity mActivity;
    private ArrayList<String> mList;
    private String mUserType;
    AddPreliminaryExaminationComplaintsAdapter.SingleClickListener sClickListener;

    public PreliminaryExaminationComplaintsAdapter(Activity mActivity, String mUserType, ArrayList<String> al) {
        this.mActivity = mActivity;
        this.mUserType = mUserType;
        mList=al;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .row_add_preliminary_examination_compliance_item, parent, false);
        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
       //final PreliminaryExaminationDetailsModel.Data.PrimaryComplain primaryComplainModel = mList.get(position);

        String title=mList.get(position);

        ((CustomViewHolder) holder).mCheckBox.setTag(position);

        ((CustomViewHolder) holder).mTextTitle.setSelected(true);
        ((CustomViewHolder) holder).mTextTitle.setText(Common.isEmptyString(mActivity,title));

         ((CustomViewHolder) holder).mImageDelete.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                   mList.remove(title);
                   RemoveItem(title);
                   notifyDataSetChanged();
             }
         });
    }

    protected void RemoveItem(String title){

    }

    /**
     * Create an interface for the recycler view click events
     */
    public void setOnItemClickListener(AddPreliminaryExaminationComplaintsAdapter.SingleClickListener clickListener) {
        sClickListener = clickListener;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }


    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder {

        CheckBox mCheckBox;
        TextView mTextTitle;
        ImageView mImageDelete;

        public CustomViewHolder(final View itemView) {
            super(itemView);
            this.setIsRecyclable(false);

            // Check Box
            this.mCheckBox = itemView.findViewById(R.id.checkbox_row_add_preliminary_examination_compliance);

            // Text Views
            this.mTextTitle = itemView.findViewById(R.id.text_row_add_preliminary_examination_compliance_compliance);
            // ImageView
            this.mImageDelete = itemView.findViewById(R.id.image_row_add_preliminary_examination_compliance_delete);
            this.mImageDelete.setColorFilter(Common.setThemeColor(mActivity));
            this.mCheckBox.setChecked(true);
        }
    }
}
