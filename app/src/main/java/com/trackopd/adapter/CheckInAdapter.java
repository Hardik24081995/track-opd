package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.fragments.AddNewPreliminaryExaminationFragment;
import com.trackopd.fragments.AddSurgeryFragment;
import com.trackopd.model.CheckInListModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.SessionManager;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class CheckInAdapter extends RecyclerView.Adapter {
    private static final int unselectedItem = -1;
    private static SingleClickListener sClickListener;
    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    Runnable updater;
    private Activity mActivity;
    private ArrayList<CheckInListModel.Data> mArrCheckInModel;
    private String mSelReason, mSelReasonID, mAppointmentId, mUserId, mPatientId;
    private AppointmentReasonsAdapter mAdapterAppointmentReasons;
    private int selectedItem = unselectedItem;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private String mUserType;

    public CheckInAdapter(Activity mActivity, RecyclerView mRecyclerView, ArrayList<CheckInListModel.Data> mArrCheckIn) {
        this.mActivity = mActivity;
        this.mRecyclerView = mRecyclerView;
        this.mArrCheckInModel = mArrCheckIn;
        mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
        LayoutInflater mLayoutInflater = LayoutInflater.from(mActivity);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);
        // setTimeCountDown();
        setOnScrollListener();
    }

    /**
     * Create an interface for the recycler view click events
     */
    public void setOnItemClickListener(SingleClickListener clickListener) {
        sClickListener = clickListener;
    }

    public void selectedItem() {
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_receptionist_check_in_item, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        if (holder instanceof CustomViewHolder) {

            final CheckInListModel.Data checkInData = mArrCheckInModel.get(position);

            ((CustomViewHolder) holder).mLinearExpandableArrow.setTag(position);

            ((CustomViewHolder) holder).mRelativeImageBorder.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.user_image_border));

            // Set Profile Pic
            if (checkInData.getProfileImage() != null &&
                    !checkInData.getProfileImage().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                //Set Thumbnail
                String thumbmail_url = WebFields.API_BASE_URL + WebFields.IMAGE_THUMBNAIL_URL + checkInData.getProfileImage();

                Glide.with(mActivity)
                        .load(thumbmail_url)
                        .apply(new RequestOptions().error(R.drawable.ic_documents_img).placeholder(R.drawable.ic_documents_img))
                        .into(((CustomViewHolder) holder).mImageUserProfilePic);
            } else {
                ((CustomViewHolder) holder).mImageUserProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, checkInData.getPatientFirstName(),
                        checkInData.getPatientLastName()));
            }


            if (!checkInData.getAppointmentStatus().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                ((CustomViewHolder) holder).mImageStatus.setVisibility(View.VISIBLE);
                if (checkInData.getAppointmentStatus().equalsIgnoreCase(mActivity.getResources().getString(R.string.status_accept))) {
                    ((CustomViewHolder) holder).mImageStatus.setImageResource(R.drawable.ic_dot_green_icon);
                } else if (checkInData.getAppointmentStatus().equalsIgnoreCase(mActivity.getResources().getString(R.string.status_reject))) {
                    ((CustomViewHolder) holder).mImageStatus.setImageResource(R.drawable.ic_dot_red_icon);
                } else if (checkInData.getAppointmentStatus().equalsIgnoreCase(mActivity.getResources().getString(R.string.status_pending))) {
                    ((CustomViewHolder) holder).mImageStatus.setImageResource(R.drawable.ic_dot_orange_icon);
                } else if (checkInData.getAppointmentStatus().equalsIgnoreCase(mActivity.getResources().getString(R.string.status_completed))) {
                    ((CustomViewHolder) holder).mImageStatus.setImageResource(R.drawable.ic_dot_blue_icon);
                }
            } else {
                ((CustomViewHolder) holder).mImageStatus.setVisibility(View.GONE);
            }

            if (checkInData.getAppointmentType().equalsIgnoreCase(mActivity.getResources().getString(R.string.text_walk_in))) {
                ((CustomViewHolder) holder).mImageWalkInPatient.setVisibility(View.VISIBLE);
            } else {
                ((CustomViewHolder) holder).mImageWalkInPatient.setVisibility(View.GONE);
            }

            String mPatientName = checkInData.getPatientFirstName() + AppConstants.STR_EMPTY_SPACE + checkInData.getPatientLastName();
            String mDoctorName = checkInData.getDoctorFirstName() + AppConstants.STR_EMPTY_SPACE + checkInData.getDoctorLastName();
            ((CustomViewHolder) holder).mTextMRDNo.setText(Common.isEmptyString(mActivity, checkInData.getMRDNo()));
            ((CustomViewHolder) holder).mTextName.setText(Common.isEmptyString(mActivity, mPatientName));
            ((CustomViewHolder) holder).mTextMobileNo.setText(Common.isEmptyString(mActivity, checkInData.getPatientMobileNo()));
            ((CustomViewHolder) holder).mTextAppointmentNo.setText(Common.isEmptyString(mActivity, checkInData.getTicketNumber()));
            ((CustomViewHolder) holder).mTextAppointmentDate.setText(Common.isEmptyString(mActivity, checkInData.getAppointmentDate()));
            ((CustomViewHolder) holder).mTextPatientCode.setText(Common.isEmptyString(mActivity, checkInData.getPatientCode()));
            ((CustomViewHolder) holder).mTextCurrentStatus.setText(Common.isEmptyString(mActivity, checkInData.getCurrentStatus()));


            //TODO: Check In Date Set and Same Date Show Timer
            String checkInDate = checkInData.getCheckINDate();

            if (!checkInDate.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {

                ((CustomViewHolder) holder).mTextCheckInDate.setVisibility(View.VISIBLE);
                ((CustomViewHolder) holder).mTextCheckInDate.setText(checkInDate);
            } else {
                ((CustomViewHolder) holder).mTextCheckInDate.setVisibility(View.GONE);
            }

            String currentDate = Common.setCurrentDate(mActivity);
            String convertCheckInDate = Common.convertDateUsingDateFormat(mActivity, checkInDate,
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_kk_mm_ss_hyphen)
                    , mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen));

            String convertCurrentDate = Common.convertDateUsingDateFormat(mActivity, currentDate,
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy)
                    , mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hyphen));


            //Update Timer Code
            /*if (convertCheckInDate.equalsIgnoreCase(convertCurrentDate)){
                setTimeCountDown(checkInData,((CustomViewHolder) holder).mTextWaitTime,holder.getAdapterPosition());
                ((CustomViewHolder) holder).mTextWaitTime.setText(checkInData.getTimeRemaining());
            }else{
                ((CustomViewHolder) holder).mTextWaitTime.setText(convertCheckInDate);
            }*/


            // ToDo: Expandable Arrow Click Listener
            ((CustomViewHolder) holder).bind();

            if (((CustomViewHolder) holder).mExpandableLayout.isExpanded()) {
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
            } else {
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
            }

            ((CustomViewHolder) holder).mLinearExpandableArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = holder.getAdapterPosition();
                    if (pos == selectedItem) {
                        selectedItem = unselectedItem;
                        ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
                        ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                        ((CustomViewHolder) holder).mExpandableLayout.collapse();
                    } else {
                        ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                        ((CustomViewHolder) holder).mExpandableLayout.expand();
                        ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        selectedItem = pos;
                    }
                    //notifyDataSetChanged();
                }
            });

            //check Appointment Type
            if (checkInData.getCheckUpType().equalsIgnoreCase(
                    mActivity.getResources().getString(R.string.nav_menu_surgery))) {
                ((CustomViewHolder) holder).mTextExaminationText.setText(
                        mActivity.getResources().getString(R.string.action_start_surgey));
            } else {
                ((CustomViewHolder) holder).mTextExaminationText.setText(
                        mActivity.getResources().getString(R.string.action_start_examination));
            }

            ((CustomViewHolder) holder).mLinearPreliminary.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkInData.getCheckUpType().equalsIgnoreCase(
                            mActivity.getResources().getString(R.string.nav_menu_surgery))) {
                        // callToAddSurgeryFragment(checkInData);
                        callToAddPreliminaryExaminationFragment(checkInData);
                    } else {
                        SessionManager manager = new SessionManager(mActivity);
                        manager.setPreferences("ClosingFlags" , checkInData.getCloseFile());
                        callToAddPreliminaryExaminationFragment(checkInData);
                    }

                }
            });

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    /**
     * This Method Check Surgery
     *
     * @param checkInData -Check In Data
     */
    private void callToAddSurgeryFragment(CheckInListModel.Data checkInData) {


        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, checkInData.getPatientFirstName());
        bundle.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, checkInData.getPatientLastName());
        bundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE, checkInData.getPatientMobileNo());
        bundle.putString(AppConstants.BUNDLE_PATIENT_CODE, checkInData.getPatientCode());
        bundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, AppConstants.STR_EMPTY_STRING);
        bundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_ID, checkInData.getAppointmentID());
        bundle.putString(AppConstants.BUNDLE_PATIENT_PROFILE, checkInData.getProfileImage());

        Fragment fragment = new AddSurgeryFragment();
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = null;

        if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
            ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.nav_menu_surgery));
            fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
        } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
            ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.nav_menu_surgery));
            fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
        } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
            ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.nav_menu_surgery));
            fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
        } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
            ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.nav_menu_surgery));
            fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
        } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_patient))) {
            ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.nav_menu_surgery));
            fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
        }

        fragmentManager.beginTransaction().replace(R.id.fragment_content_frame,
                fragment, mActivity.getResources()
                        .getString(R.string.tag_check_up_surgery))
                .addToBackStack(mActivity.getResources()
                        .getString(R.string.back_stack_check_up_surgery))
                .commit();
    }

    /**
     * Sets up the Optometrist Add New Preliminary Examination Fragment
     *
     * @param checkInData Receptionist Patients Model
     */
    private void callToAddPreliminaryExaminationFragment(CheckInListModel.Data checkInData) {
        try {
            Bundle mBundle = new Bundle();

            mBundle.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, checkInData.getPatientFirstName());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, checkInData.getPatientLastName());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE, checkInData.getPatientMobileNo());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_PROFILE, checkInData.getProfileImage());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_ID, checkInData.getPatientID());
            mBundle.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, Integer.parseInt(checkInData.getAppointmentID()));
            mBundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, checkInData.getDoctorID());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE, checkInData.getCheckINDate());
            mBundle.putString(AppConstants.BUNDLE_PRELIMINARY_EXAMINATION_ID, checkInData.getPreliminaryExaminationID());
            mBundle.putString(AppConstants.BUNDLE_CHECK_TYPE, checkInData.getCheckUpType());
            mBundle.putString(AppConstants.BUNDLE_ADD_PATIENT_PHOTO, checkInData.getProfileImage());

            Fragment fragment = new AddNewPreliminaryExaminationFragment();
            fragment.setArguments(mBundle);

            FragmentManager fragmentManager = null;
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager =
                        ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager =
                        ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager =
                        ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_check_up));
                fragmentManager =
                        ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_preliminary_examination))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_add_preliminary_examination))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //TODO: Set Timer Waiting Item
    private void setTimeCountDown(CheckInListModel.Data data, TextView mTextWaitTime, int adapterPosition) {

        String currentDate = Common.setCheckCurrentDateTime(mActivity);
        final Handler timerHandler = new Handler();
        timerHandler.removeCallbacks(updater); //stop handler when activity not visible

        updater = new Runnable() {
            @Override
            public void run() {
                try {
                    SimpleDateFormat sdf = new SimpleDateFormat
                            (mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_kk_mm_ss_hyphen));
                    Date cDate = sdf.parse(currentDate);
                    Date checkingDate = sdf.parse(data.getCheckINDate());

                    String difference = AppConstants.STR_EMPTY_STRING;
                    Common.insertLog("After" + checkingDate);
                    difference = Common.printDifference(cDate, checkingDate);

                    data.setTimeRemaining(difference);

                    mTextWaitTime.setText(difference);

                    notifyItemChanged(adapterPosition);

                } catch (Exception e) {
                    Common.insertLog(e.getMessage());
                }
            }
        };
        timerHandler.post(updater);
    }

    @Override
    public int getItemCount() {
        return mArrCheckInModel == null ? 0 : mArrCheckInModel.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrCheckInModel.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public interface SingleClickListener {
        void onItemClickListener();
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder implements ExpandableLayout.OnExpansionUpdateListener {

        RelativeLayout mRelativeImageBorder;
        ImageView mImageUserProfilePic, mImageStatus, mImageExpandableArrow, mImageWalkInPatient;
        LinearLayout mLinearExpandableArrow, mLinearPreliminary;
        ExpandableLayout mExpandableLayout;
        TextView mTextMRDNo, mTextCheckInDate, mTextName, mTextMobileNo, mTextAppointmentNo, mTextAppointmentDate,
                mTextPatientCode, mTextWaitTime, mTextExaminationText, mTextCurrentStatus;

        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Relative Layouts
            this.mRelativeImageBorder = itemView.findViewById(R.id.relative_row_receptionist_check_in_pic);

            // Image Views
            this.mImageUserProfilePic = itemView.findViewById(R.id.image_row_receptionist_check_in_pic);
            this.mImageStatus = itemView.findViewById(R.id.image_row_receptionist_check_in_status);
            this.mImageExpandableArrow = itemView.findViewById(R.id.image_row_receptionist_check_in_expandable_view);
            this.mImageWalkInPatient = itemView.findViewById(R.id.image_row_receptionist_check_in_walk_in_patient);
            // Linear Layouts
            this.mLinearExpandableArrow = itemView.findViewById(R.id.linear_row_receptionist_check_in_expandable_view);
            this.mLinearPreliminary = itemView.findViewById(R.id.linear_row_receptionist_check_in_add_preliminary);

            // Expandable Layout
            this.mExpandableLayout = itemView.findViewById(R.id.expandable_layout_row_receptionist_appointment);

            // Text Views
            this.mTextMRDNo = itemView.findViewById(R.id.ribbon_view_row_receptionist_check_in_mrd_no);
            this.mTextName = itemView.findViewById(R.id.text_row_receptionist_check_in_name);
            this.mTextMobileNo = itemView.findViewById(R.id.text_row_receptionist_check_in_mobile_no);
            this.mTextAppointmentNo = itemView.findViewById(R.id.text_row_receptionist_check_in_number);
            this.mTextAppointmentDate = itemView.findViewById(R.id.text_row_receptionist_check_in_appointment_date);
            this.mTextPatientCode = itemView.findViewById(R.id.text_row_receptionist_patient_code);
            this.mTextWaitTime = itemView.findViewById(R.id.text_view_row_receptionist_check_in_waiting_time);
            this.mTextCheckInDate = itemView.findViewById(R.id.text_row_receptionist_check_in_date);
            this.mTextExaminationText = itemView.findViewById(R.id.text_row_receptionist_check_in_examination);
            this.mTextCurrentStatus = itemView.findViewById(R.id.text_row_receptionist_check_in_currnet_status);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Common.insertLog("ExpandableLayout State : " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                mRecyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }

        /**
         * Binds the expandable view
         */
        public void bind() {
            int position = getAdapterPosition();
            boolean isSelected = position == selectedItem;

            mLinearExpandableArrow.setSelected(isSelected);
            mExpandableLayout.setExpanded(isSelected, false);
        }
    }
}
