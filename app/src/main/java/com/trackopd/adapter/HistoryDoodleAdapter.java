package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.trackopd.R;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel.Data;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public abstract class HistoryDoodleAdapter extends RecyclerView.Adapter {
    private static final int unselectedItem = -1;
    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    ArrayList<Data> mArrPreliminaryExamination;
    private Activity mActivity;
    private int selectedItem = unselectedItem;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;


    public HistoryDoodleAdapter(Activity mActivity, RecyclerView recyclerView,
                                ArrayList<Data> mArrHistory) {
        this.mActivity = mActivity;
        this.mRecyclerView = recyclerView;
        this.mArrPreliminaryExamination = mArrHistory;
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_history_preliminary_diagnosis_item, null, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, null, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder,
                                 @SuppressLint("RecyclerView") int position) {

        if (holder instanceof CustomViewHolder) {

            Data data = mArrPreliminaryExamination.get(position);

            ((CustomViewHolder) holder).mCardViewHolder.setVisibility(View.VISIBLE);
            ((CustomViewHolder) holder).mTextDate.setText(data.getTreatmentDate());

            if (data.getDoodle() != null &&
                    data.getDoodle().size() > 0) {


                for (int a = 0; a < data.getDoodle().size(); a++) {

                    HistoryPreliminaryExaminationModel.Doodle
                            investigation = data.getDoodle().get(a);

                    View view_container = LayoutInflater.from(mActivity.getBaseContext())
                            .inflate(R.layout.row_history_doodle_item,
                                    null, false);

                    ImageView mImageArrowAndex = view_container.
                            findViewById(R.id.image_row_history_doodle_andex_arrow);

                    ImageView mImageArrowAnteriorSegment = view_container.
                            findViewById(R.id.image_row_history_doodle_anterior_segment);

                    ImageView mImageArrowAnteriorLense = view_container.
                            findViewById(R.id.image_row_history_doodle_anterior_segment_icon);

                    ImageView mImageArrowFundus = view_container.
                            findViewById(R.id.image_row_history_history_doodle_fundus);

                    ImageView mImageArrowGoniscopy = view_container.
                            findViewById(R.id.image_row_history_doodle_gonioscopy);

                    //Lense
                    ImageView mImageRightAndex =
                            view_container.findViewById(R.id.image_doodle_right_eye_adnex);
                    ImageView mImageLeftAndex =
                            view_container.findViewById(R.id.image_doodle_left_eye_adnex);

                    ImageView mImageRightAnteriorSegment =
                            view_container.findViewById(R.id.image_doodle_right_eye_anterior_segment);
                    ImageView mImageLeftAnteriorSegment =
                            view_container.findViewById(R.id.image_doodle_left_eye_anterior_segment);

                    ImageView mImageRightLense =
                            view_container.findViewById(R.id.image_doodle_right_eye_lense);
                    ImageView mImageLeftLense =
                            view_container.findViewById(R.id.image_doodle_left_eye_lense);
                    ImageView mImageRightFundus =
                            view_container.findViewById(R.id.image_doodle_right_eye_fundus);
                    ImageView mImageLeftFundus =
                            view_container.findViewById(R.id.image_doodle_left_eye_fundus);

                    ImageView mImageRightGonioscopy =
                            view_container.findViewById(R.id.image_doodle_right_eye_gonioscopy);
                    ImageView mImageLeftGonioscopy =
                            view_container.findViewById(R.id.image_doodle_left_eye_gonioscopy);


                    ExpandableLayout mExpandble = view_container.findViewById(R.id.expandable_layout_row_history_doodle_andex);
                    ExpandableLayout mExpandbleSegment = view_container.findViewById(R.id.expandable_layout_row_history_doodle_anterior_segment);
                    ExpandableLayout mExpandblelense = view_container.findViewById(R.id.expandable_layout_row_history_doodle_lense);
                    ExpandableLayout mExpandbleFundus = view_container.findViewById(R.id.expandable_layout_row_history_doodle_fundus);
                    ExpandableLayout mExpandblegonioscopy = view_container.findViewById(R.id.expandable_layout_row_history_doodle_gonioscopy);


                    //Set Image Get

                    if (!data.getDoodle().get(a).getRightAdnex()
                            .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                        setGlideImageView(data.getDoodle().get(a).getRightAdnex(), mImageRightAndex,
                                data.getDoodle().get(a).getAppointmentID(), R.drawable.adnex_icon);
                    }

                    if (!data.getDoodle().get(a).getLeftAdnex()
                            .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                        setGlideImageView(data.getDoodle().get(a).getLeftAdnex(),
                                mImageLeftAndex,
                                data.getDoodle().get(a).getAppointmentID(), R.drawable.adnex_icon);
                    }

                    if (!data.getDoodle().get(a).getRightAnteriorSegment()
                            .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                        setGlideImageView(data.getDoodle().get(a).getRightAnteriorSegment(),
                                mImageRightAnteriorSegment,
                                data.getDoodle().get(a).getAppointmentID(), R.drawable.anterior_segment_icon);
                    }
                    if (!data.getDoodle().get(a).getLeftAnteriorSegment()
                            .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                        setGlideImageView(data.getDoodle().get(a).getLeftAnteriorSegment(),
                                mImageLeftAnteriorSegment,
                                data.getDoodle().get(a).getAppointmentID(), R.drawable.anterior_segment_icon);
                    }

                    if (!data.getDoodle().get(a).getRightLens()
                            .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                        setGlideImageView(data.getDoodle().get(a).getRightLens(),
                                mImageRightLense,
                                data.getDoodle().get(a).getAppointmentID(), R.drawable.lens_icon);
                    }
                    if (!data.getDoodle().get(a).getLeftLens()
                            .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                        setGlideImageView(data.getDoodle().get(a).getLeftLens(),
                                mImageLeftLense,
                                data.getDoodle().get(a).getAppointmentID(), R.drawable.lens_icon);
                    }
                    if (!data.getDoodle().get(a).getRightFundus()
                            .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                        setGlideImageView(data.getDoodle().get(a).getRightFundus(),
                                mImageRightFundus,
                                data.getDoodle().get(a).getAppointmentID(), R.drawable.right_fundus_icon);
                    }
                    if (!data.getDoodle().get(a).getLeftFundus()
                            .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                        setGlideImageView(data.getDoodle().get(a).getLeftFundus(),
                                mImageLeftFundus,
                                data.getDoodle().get(a).getAppointmentID(), R.drawable.left_fundus_icon);
                    }
                    if (!data.getDoodle().get(a).getRightGonioscopy()
                            .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                        setGlideImageView(data.getDoodle().get(a).getRightGonioscopy(),
                                mImageRightGonioscopy,
                                data.getDoodle().get(a).getAppointmentID(), R.drawable.gonioscopy_icon);
                    }
                    if (!data.getDoodle().get(a).getLeftGonioscopy()
                            .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                        setGlideImageView(data.getDoodle().get(a).getLeftGonioscopy(),
                                mImageLeftGonioscopy,
                                data.getDoodle().get(a).getAppointmentID(), R.drawable.gonioscopy_icon);
                    }


                    //TODO:Set On Click Listener
                    mImageArrowAndex.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mExpandble.isExpanded()) {
                                mExpandble.collapse();
                                mImageArrowAndex.setImageResource(R.drawable.ic_down_arrow);
                            } else {
                                mExpandble.expand();
                                mImageArrowAndex.setImageResource(R.drawable.ic_up_arrow);

                            }
                        }
                    });

                    mImageArrowAnteriorSegment.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mExpandbleSegment.isExpanded()) {
                                mExpandbleSegment.collapse();
                                mImageArrowAnteriorSegment.setImageResource(R.drawable.ic_down_arrow);

                            } else {
                                mExpandbleSegment.expand();
                                mImageArrowAnteriorSegment.setImageResource(R.drawable.ic_up_arrow);

                            }
                        }
                    });
                    mImageArrowAnteriorLense.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mExpandblelense.isExpanded()) {
                                mExpandblelense.collapse();
                                mImageArrowAnteriorLense.setImageResource(R.drawable.ic_down_arrow);

                            } else {
                                mExpandblelense.expand();
                                mImageArrowAnteriorLense.setImageResource(R.drawable.ic_up_arrow);

                            }
                        }
                    });

                    mImageArrowFundus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mExpandbleFundus.isExpanded()) {
                                mExpandbleFundus.collapse();
                                mImageArrowFundus.setImageResource(R.drawable.ic_down_arrow);

                            } else {
                                mExpandbleFundus.expand();
                                mImageArrowFundus.setImageResource(R.drawable.ic_up_arrow);

                            }
                        }
                    });
                    mImageArrowGoniscopy.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (mExpandblegonioscopy.isExpanded()) {
                                mExpandblegonioscopy.collapse();
                                mImageArrowGoniscopy.setImageResource(R.drawable.ic_down_arrow);

                            } else {
                                mExpandblegonioscopy.expand();
                                mImageArrowGoniscopy.setImageResource(R.drawable.ic_up_arrow);

                            }
                        }
                    });


                    ((CustomViewHolder) holder).mLinearContainer.addView(view_container);
                }
            } else {
                ((CustomViewHolder) holder).mCardViewHolder.setVisibility(View.GONE);
            }

            ((CustomViewHolder) holder).mImageEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSelectedItem(mActivity, data);
                }
            });

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }


    protected void onSelectedItem(Activity activity, Data data) {

    }


    @Override
    public int getItemCount() {
        return mArrPreliminaryExamination == null ? 0 : mArrPreliminaryExamination.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrPreliminaryExamination.get(position) == null ?
                VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setImageOnImageView(ImageView image, String imageName, String AppointmentID) {
        try {


            if (imageName.equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                return;
            }

            String mBaseURL = WebFields.API_BASE_URL;
            String mFolderName = WebFields.IMAGE_BASE_FOLDER;
            String mHospitalCode = GetJsonData.getHospitalData(mActivity, WebFields.PASS_CODE.RESPONSE_HOSPITAL_CODE);

            // http://societyfy.in/Track_OPD/assets/uploads/12356/34/Investigation/1568020896_34.jpg
            String url = mBaseURL + mFolderName + mHospitalCode + AppConstants.STR_FORWARD_SLASH + AppointmentID + AppConstants.STR_FORWARD_SLASH +
                    "Investigation" + AppConstants.STR_FORWARD_SLASH + imageName;

            Common.insertLog("Image url " + url);

            Glide.with(mActivity)
                    .load(url)
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .placeholder(R.drawable.upload_icon)
                    .into(image);

        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * This should set the image view using glide
     *
     * @param mType          - Type
     * @param mImageView     - Image View
     * @param mAppointmentId - mAppointmentId
     * @param drawable       - Drawable
     */
    private void setGlideImageView(String mType, ImageView mImageView,
                                   String mAppointmentId, int drawable) {
        try {
            String mBaseURL = WebFields.API_BASE_URL;
            String mFolderName = WebFields.IMAGE_BASE_FOLDER;
            String mHospitalCode = GetJsonData.getHospitalData(mActivity, WebFields.PASS_CODE.RESPONSE_HOSPITAL_CODE);
            String mDoodleUrl = mBaseURL + mFolderName + mHospitalCode + "/" + mAppointmentId + "/DoodleImage/" + mType;
            Common.insertLog("Doodle Url::> " + mDoodleUrl);
            Glide.with(mActivity)
                    .load(mDoodleUrl)
                    .apply(new RequestOptions().error(drawable).
                            placeholder(drawable))
                    .into(mImageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder implements ExpandableLayout.OnExpansionUpdateListener {

        ImageView mImageEdit, mImageDate;
        TextView mTextDate;
        LinearLayout mLinearContainer, mLinearMain;
        CardView mCardViewHolder;

        public CustomViewHolder(final View mView) {
            super(mView);
            //Image View
            this.mImageDate = mView.findViewById(R.id.image_row_history_preliminary_diagnosis_date);
            this.mImageEdit = mView.findViewById(R.id.image_row_history_preliminary_diagnosis_edit);
            // Text View
            this.mTextDate = mView.findViewById(R.id.text_row_history_preliminary_diagnosis_date);
            //Linear Layout
            this.mLinearContainer = mView.findViewById(R.id.linear_row_history_preliminary_diagnosis_container);
            this.mLinearMain = mView.findViewById(R.id.linear_row_history_preliminary_diagnosis_item);

            mCardViewHolder = mView.findViewById(R.id.card_view_row_history_preliminary_diagnosis);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Common.insertLog("ExpandableLayout State : " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                mRecyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }

        /**
         * Binds the expandable view
         */
        public void bind() {
            int position = getAdapterPosition();
            boolean isSelected = position == selectedItem;
        }
    }
}
