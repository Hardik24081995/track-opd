package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.google.gson.Gson;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.fragments.AddAppointmentFragment;
import com.trackopd.fragments.SearchReceptionistExistingAppointmentFragment;
import com.trackopd.model.ReceptionistFollowUpModel;
import com.trackopd.model.RescheduleModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.GenerateTimeSlot;
import com.trackopd.utils.KeyboardUtility;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import net.cachapa.expandablelayout.ExpandableLayout;

import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReceptionistFollowUpAdapter extends RecyclerView.Adapter {
    private Activity mActivity;
    private String mUserType, mUserId;
    private static SingleClickListener sClickListener;
    private ArrayList<ReceptionistFollowUpModel> mArrReceptionistFollowUpModel;
    private static final int unselectedItem = -1;
    private int selectedItem = unselectedItem;

    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int lastVisibleItem, totalItemCount, visibleThreshold = 2;

    /**
     * Adapter contains the data to be displayed
     */
    public ReceptionistFollowUpAdapter(Activity mActivity, RecyclerView mRecyclerView, ArrayList<ReceptionistFollowUpModel> mArrReceptionistFollowUpModel) {
        this.mActivity = mActivity;
        this.mRecyclerView = mRecyclerView;
        this.mArrReceptionistFollowUpModel = mArrReceptionistFollowUpModel;
        mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
        LayoutInflater mLayoutInflater = LayoutInflater.from(mActivity);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        setOnScrollListener();
    }

    /**
     * Create an interface for the recycler view click events
     */
    public void setOnItemClickListener(SingleClickListener clickListener) {
        sClickListener = clickListener;
    }

    public interface SingleClickListener {
        void onItemClickListener();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_receptionist_follow_up_item, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (holder instanceof CustomViewHolder) {
            final ReceptionistFollowUpModel receptionistFollowUpModel = mArrReceptionistFollowUpModel.get(position);

            ((CustomViewHolder) holder).mImageUserProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, receptionistFollowUpModel.getPatientFirstName(),
                    receptionistFollowUpModel.getPatientLastName()));

            ((CustomViewHolder) holder).mRelativePicture.setBackground(mActivity.getResources().getDrawable(R.drawable.user_image_border));

//            if(!receptionistFollowUpModel.getAppointmentStatus().equalsIgnoreCase("")) {
//                ((CustomViewHolder) holder).mImageStatus.setVisibility(View.VISIBLE);
//                if(receptionistFollowUpModel.getAppointmentStatus().equalsIgnoreCase(mActivity.getResources().getString(R.string.status_accept))) {
//                    ((CustomViewHolder) holder).mImageStatus.setImageResource(R.drawable.ic_dot_green_icon);
//                } else if(receptionistFollowUpModel.getAppointmentStatus().equalsIgnoreCase(mActivity.getResources().getString(R.string.status_reject))) {
//                    ((CustomViewHolder) holder).mImageStatus.setImageResource(R.drawable.ic_dot_red_icon);
//                } else if(receptionistFollowUpModel.getAppointmentStatus().equalsIgnoreCase(mActivity.getResources().getString(R.string.status_pending))) {
//                    ((CustomViewHolder) holder).mImageStatus.setImageResource(R.drawable.ic_dot_orange_icon);
//                }
//            } else {
//                ((CustomViewHolder) holder).mImageStatus.setVisibility(View.GONE);
//            }

            String mPatientName = receptionistFollowUpModel.getPatientFirstName() + AppConstants.STR_EMPTY_SPACE + receptionistFollowUpModel.getPatientLastName();
            String mDoctorName = receptionistFollowUpModel.getDoctorFirstName() + AppConstants.STR_EMPTY_SPACE + receptionistFollowUpModel.getDoctorLastName();
            String mCouncilorName = receptionistFollowUpModel.getCouncillorFirstName() + AppConstants.STR_EMPTY_SPACE + receptionistFollowUpModel.getCouncillorLastName();

            ((CustomViewHolder) holder).mTextName.setText(Common.isEmptyString(mActivity, mPatientName));
            ((CustomViewHolder) holder).mTextMobileNo.setText(Common.isEmptyString(mActivity, receptionistFollowUpModel.getPatientMobileNo()));
            ((CustomViewHolder) holder).mTextFollowUpDate.setText(Common.isEmptyString(mActivity, receptionistFollowUpModel.getFollowupDate()));
            ((CustomViewHolder) holder).mTextDoctorName.setText(Common.isEmptyString(mActivity, mDoctorName));
            ((CustomViewHolder) holder).mTextCouncillorName.setText(Common.isEmptyString(mActivity, mCouncilorName));
            ((CustomViewHolder) holder).mTextDescription.setText(Common.isEmptyString(mActivity, receptionistFollowUpModel.getNotes()));

            // ToDo: Appointment Click Listener
            ((CustomViewHolder) holder).mLinearAppointment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    openPopup(receptionistFollowUpModel);
                    callToAppointmentFragment(receptionistFollowUpModel);
                }
            });

            // ToDo: Reschedule Click Listener
            ((CustomViewHolder) holder).mLinearReschedule.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openRescheduleDialog(receptionistFollowUpModel);
                }
            });

            int pos = holder.getAdapterPosition();
            if (pos != selectedItem) {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                ((CustomViewHolder) holder).mExpandableLayout.collapse();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
            } else {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                ((CustomViewHolder) holder).mExpandableLayout.expand();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
            }

            // ToDo: Expandable Arrow Click Listener
            ((CustomViewHolder) holder).bind();
            ((CustomViewHolder) holder).mLinearExpandableArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                    ((CustomViewHolder) holder).mExpandableLayout.collapse();
                    ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));

                    Common.insertLog("Position:::> " + position);
                    Common.insertLog("selectedItem:::> " + selectedItem);

                    int pos = holder.getAdapterPosition();
                    if (pos == selectedItem) {
                        selectedItem = unselectedItem;
                    } else {
                        ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                        ((CustomViewHolder) holder).mExpandableLayout.expand();
                        ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        selectedItem = pos;
                    }
                    notifyDataSetChanged();
                }
            });
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    /**
     * Open dialog for follow up reschedule
     */
    public void openRescheduleDialog(ReceptionistFollowUpModel receptionistFollowUpModel) {
        try {
            final Dialog dialog = new Dialog(mActivity);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.custom_dialog_appointment_reschedule);

            // Edit Texts
            EditText mEditDate = dialog.findViewById(R.id.edit_dialog_appointment_reschedule_date);
            EditText mEditTime = dialog.findViewById(R.id.edit_dialog_appointment_reschedule_time);

            // Button
            Button mButtonOk = dialog.findViewById(R.id.button_dialog_appointment_reschedule_ok);

            // Set current date
            mEditDate.setText(Common.setCurrentDate(mActivity));

            // ToDo: Edit Text Date Click Listener
            mEditDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Common.openDatePicker(mActivity, mEditDate);
                }
            });

            // ToDo: Edit Text Date Click Listener
            mEditTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //GenerateTimeSlot.openTimeSlotDialog(mActivity, mEditTime);
                }
            });

            // ToDo: Button Ok Click Listener
            mButtonOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    doRescheduleFollowUp(dialog, mEditDate, mEditTime, receptionistFollowUpModel);
                }
            });

            dialog.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method checks the validation first then call the API
     */
    private void doRescheduleFollowUp(Dialog dialog, EditText mEditDate, EditText mEditTime, ReceptionistFollowUpModel receptionistFollowUpModel) {
        KeyboardUtility.HideKeyboard(mActivity, mEditDate);
        if (checkValidation(mEditTime)) {
            openConfirmationDialog(dialog, mEditDate.getText().toString(), mEditTime.getText().toString(), receptionistFollowUpModel);
        }
    }

    /**
     * This method should check the validation and return the value accordingly to that
     *
     * @return - returns true if all validations are correct otherwise return false and shows error
     */
    private boolean checkValidation(EditText mEditTime) {
        boolean status = true;

        if (mEditTime.getText().toString().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.val_select_time));
            status = false;
        }
        return status;
    }

    /**
     * Open confirmation popup for follow up reschedule
     */
    private void openConfirmationDialog(Dialog dialog, String mDate, String mTime, ReceptionistFollowUpModel receptionistFollowUpModel) {
        try {
            dialog.dismiss();
            AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
            builder.setMessage(R.string.dialog_reschedule_appointment_message)
                    .setPositiveButton(R.string.action_ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            callToRescheduleFollowUpAPI(mDate, mTime, receptionistFollowUpModel);
                        }
                    })
                    .setNegativeButton(R.string.action_cancel, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // User cancelled the dialog
                            dialog.dismiss();
                        }
                    });
            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the reschedule API for Follow Up
     *
     * @param mDate - Date
     * @param mTime - Time
     */
    private void callToRescheduleFollowUpAPI(String mDate, String mTime, ReceptionistFollowUpModel receptionistFollowUpModel) {
        try {
            String mConvertedDate = Common.convertDateToServer(mActivity, mDate);
            String mConvertedTime = Common.convertTime(mActivity, mTime);
            String mDateTime = mConvertedDate + AppConstants.STR_EMPTY_SPACE +mConvertedTime;

            String hospital_database=GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            RequestBody requestBody = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setFollowUpRescheduleJson(mDateTime, mUserId, receptionistFollowUpModel.getPatientID(),
                            receptionistFollowUpModel.getFollowupID(),hospital_database));

            Call<RescheduleModel> call = RetrofitClient.createService(ApiInterface.class).setFollowUpReschedule(requestBody);
            call.enqueue(new Callback<RescheduleModel>() {
                @Override
                public void onResponse(@NonNull Call<RescheduleModel> call, @NonNull Response<RescheduleModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mJson = (new Gson().toJson(response.body()));
                        JSONObject jsonObject = new JSONObject(mJson);
                        String mMessage = jsonObject.getString(WebFields.MESSAGE);
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() && response.body().getError() == 200) {
                            Common.setCustomToast(mActivity, mMessage);
                            sClickListener.onItemClickListener();
                        } else {
                            Common.setCustomToast(mActivity, response.body().getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<RescheduleModel> call, @NonNull Throwable t) {
                    Common.insertLog(t.getMessage());
                    //Common.setCustomToast(mActivity, response.body().getMessage());
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Open popup for Appointment
     *
     * @param receptionistFollowUpModel - Receptionist Follow Up Model
     */
    private void openPopup(final ReceptionistFollowUpModel receptionistFollowUpModel) {
        try {
            new MaterialDialog.Builder(mActivity)
                    .title(R.string.app_name)
                    .items(R.array.appointment_type)
                    .canceledOnTouchOutside(false)
                    .positiveText(R.string.action_ok)
                    .positiveColor(mActivity.getResources().getColor(R.color.app_main_theme))
                    .negativeText(R.string.action_cancel)
                    .negativeColor(mActivity.getResources().getColor(R.color.colorControlNormal))
                    .itemsCallbackSingleChoice(0, new MaterialDialog.ListCallbackSingleChoice() {
                        @Override
                        public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                            if (which == 0) {
                                callToAppointmentFragment(receptionistFollowUpModel);
                            } else {
                                callToSearchExistingAppointmentFragment();
                            }
                            return true;
                        }
                    })
                    .callback(new MaterialDialog.ButtonCallback() {
                        @Override
                        public void onPositive(MaterialDialog dialog) {
                            dialog.dismiss();
                        }

                        public void onNegative(MaterialDialog dialog) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Search Receptionist Existing Appointment Fragment
     */
    private void callToSearchExistingAppointmentFragment() {
        try {
            ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                    ().getString(R.string.header_search));
            Fragment fragment = new SearchReceptionistExistingAppointmentFragment();
            FragmentManager fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_search_receptionist_existing_appointment))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_search_receptionist_existing_appointment))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Receptionist Add New Appointment Fragment
     *
     * @param receptionistFollowUpModel - Receptionist Follow Up Model
     */
    private void callToAppointmentFragment(ReceptionistFollowUpModel receptionistFollowUpModel) {
        try {
            Bundle args = new Bundle();
            args.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, receptionistFollowUpModel.getPatientFirstName());
            args.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, receptionistFollowUpModel.getPatientLastName());
            args.putString(AppConstants.BUNDLE_PATIENT_MOBILE, receptionistFollowUpModel.getPatientMobileNo());
            args.putString(AppConstants.BUNDLE_PATIENT_ID, receptionistFollowUpModel.getPatientID());

            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            }
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            }
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            }
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_appointment));
            }

            Fragment fragment = new AddAppointmentFragment();
            fragment.setArguments(args);
            FragmentManager fragmentManager = null;

            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            }
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            }
            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_add_receptionist_appointment))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_receptionist_appointment))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mArrReceptionistFollowUpModel == null ? 0 : mArrReceptionistFollowUpModel.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrReceptionistFollowUpModel.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder implements ExpandableLayout.OnExpansionUpdateListener {
        LinearLayout mLinearAppointment, mLinearReschedule;
        ImageView mImageUserProfilePic, mImageStatus, mImageExpandableArrow;
        LinearLayout mLinearExpandableArrow;
        ExpandableLayout mExpandableLayout;
        TextView mTextName, mTextMobileNo, mTextFollowUpDate, mTextDoctorName, mTextCouncillorName, mTextDescription;
        RelativeLayout mRelativePicture;

        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Relative Layouts
            this.mRelativePicture = itemView.findViewById(R.id.relative_row_receptionist_follow_up_pic);

            // Linear Layouts
            this.mLinearAppointment = itemView.findViewById(R.id.linear_row_receptionist_follow_up_appointment);
            this.mLinearReschedule = itemView.findViewById(R.id.linear_row_receptionist_follow_up_reschedule);

            // Image Views
            this.mImageUserProfilePic = itemView.findViewById(R.id.image_row_receptionist_follow_up_pic);
            this.mImageStatus = itemView.findViewById(R.id.image_row_receptionist_appointment_status);
            this.mImageExpandableArrow = itemView.findViewById(R.id.image_row_receptionist_follow_up_expandable_view);

            // Linear Layouts
            this.mLinearExpandableArrow = itemView.findViewById(R.id.linear_row_receptionist_follow_up_expandable_view);

            // Expandable Layout
            this.mExpandableLayout = itemView.findViewById(R.id.expandable_layout_row_receptionist_follow_up);

            // Text Views
            this.mTextName = itemView.findViewById(R.id.text_row_receptionist_follow_up_name);
            this.mTextMobileNo = itemView.findViewById(R.id.text_row_receptionist_follow_up_mobile_no);
            this.mTextFollowUpDate = itemView.findViewById(R.id.text_row_receptionist_follow_up_date);
            this.mTextDoctorName = itemView.findViewById(R.id.text_row_receptionist_follow_up_doctor_name);
            this.mTextCouncillorName = itemView.findViewById(R.id.text_row_receptionist_follow_up_councillor_name);
            this.mTextDescription = itemView.findViewById(R.id.text_row_receptionist_follow_up_description);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Common.insertLog("ExpandableLayout State : " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                mRecyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }

        /**
         * Binds the expandable view
         */
        public void bind() {
            int position = getAdapterPosition();
            boolean isSelected = position == selectedItem;

            mLinearExpandableArrow.setSelected(isSelected);
            mExpandableLayout.setExpanded(isSelected, false);
        }
    }
}
