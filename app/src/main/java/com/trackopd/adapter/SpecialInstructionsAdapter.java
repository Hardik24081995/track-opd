package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.model.PreliminaryExaminationDetailsModel;
import com.trackopd.model.SpecialInstructionsModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import java.util.ArrayList;

public class SpecialInstructionsAdapter extends RecyclerView.Adapter {

    private Activity mActivity;
    private ArrayList<SpecialInstructionsModel> mArrSpecialInstructions;
    private ArrayList<SpecialInstructionsModel> mArrSelectedSpecialInstructions;
    private ArrayList<String> mSpecialInstruction = new ArrayList<>();
    private String mUserType;
    private static SingleClickListener sClickListener;

    public SpecialInstructionsAdapter(Activity mActivity, String mUserType, ArrayList<SpecialInstructionsModel> mArrSpecialInstructions) {
        this.mActivity = mActivity;
        this.mUserType = mUserType;
        this.mArrSpecialInstructions = mArrSpecialInstructions;
        mArrSelectedSpecialInstructions = new ArrayList<>();
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
    }

    /**
     * Create an interface for the recycler view click events
     */
    public void setOnItemClickListener(SingleClickListener clickListener) {
        sClickListener = clickListener;
    }

    public interface SingleClickListener {
        void onSpecialInstructionsClickListener(String mSpecialInstructions);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                .row_add_preliminary_examination_anatomical_location_item, parent, false);
        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
       final SpecialInstructionsModel specialInstructionsModel = mArrSpecialInstructions.get(position);

        ((CustomViewHolder) holder).mCheckBox.setTag(position);

        ((CustomViewHolder) holder).mTextTitle.setSelected(true);
        ((CustomViewHolder) holder).mTextTitle.setText(Common.isEmptyString(mActivity, specialInstructionsModel.getSpecialInstruction()));

        ((CustomViewHolder) holder).mCheckBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                if (cb.isChecked()) {
                    mArrSelectedSpecialInstructions.clear();
                    mArrSelectedSpecialInstructions.add(specialInstructionsModel);
                    for (int j = 0; j < mArrSelectedSpecialInstructions.size(); j++) {
                        String mmSpecialInstructionsName = mArrSelectedSpecialInstructions.get(j).getSpecialInstruction();
                        mSpecialInstruction.add(mmSpecialInstructionsName);
                    }
                } else {
                    mSpecialInstruction.remove(specialInstructionsModel.getSpecialInstruction());
                    mArrSelectedSpecialInstructions.remove(specialInstructionsModel);
                }
                String mFinalSpecialInstructions = mSpecialInstruction.toString().replaceAll("\\[", "").replaceAll("\\]", "").replace(", ", ",");
                sClickListener.onSpecialInstructionsClickListener(mFinalSpecialInstructions);
                Common.insertLog("Sel Special Instructions:::> " + mFinalSpecialInstructions);
            }
        });

        //this old code
        /*if(mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist)) || mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))) {

        } else {
            ((CustomViewHolder) holder).mCheckBox.setClickable(false);
        }*/
    }


    @Override
    public int getItemCount() {
        return mArrSpecialInstructions.size();
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder {

        CheckBox mCheckBox;
        TextView mTextTitle;

        public CustomViewHolder(final View itemView) {
            super(itemView);
            this.setIsRecyclable(false);

            // Check Box
            this.mCheckBox = itemView.findViewById(R.id.checkbox_row_add_preliminary_examination_anatomical_location);

            // Text Views
            this.mTextTitle = itemView.findViewById(R.id.text_row_add_preliminary_examination_anatomical_location);
        }
    }
}
