package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.ReceptionistPaymentModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import java.util.ArrayList;

public class ReceptionistPatientDetailPaymentAdapter extends RecyclerView.Adapter {
    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private Activity mActivity;
    private ArrayList<ReceptionistPaymentModel> mArrReceptionistPaymentModel;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;


    public ReceptionistPatientDetailPaymentAdapter(Activity mActivity,
                                                   RecyclerView mRecyclerView, ArrayList<ReceptionistPaymentModel>
                                                           mArrReceptionistPayment, boolean b) {
        this.mActivity = mActivity;
        this.mRecyclerView = mRecyclerView;
        this.mArrReceptionistPaymentModel = mArrReceptionistPayment;
        LayoutInflater mLayoutInflater = LayoutInflater.from(mActivity);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;

        setOnScrollListener();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_receptionist_patient_detail_payment_item, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof CustomViewHolder) {
            final ReceptionistPaymentModel receptionistPatientDetailPaymentModel = mArrReceptionistPaymentModel.get(position);

            //((CustomViewHolder) holder).mTextDate.setText(mActivity.getResources().getString(R.string.text_date) + Common.convertDate(mActivity, Common.isEmptyString(mActivity, receptionistPatientDetailPaymentModel.getAppointmentDate())));

            String payementDate = receptionistPatientDetailPaymentModel.getPaymentDate();

            String convertPaymentDate = Common.convertDateUsingDateFormat(mActivity, payementDate,
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_kk_mm_ss_hyphen),
                    mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy));

            String converted_appointment_date = "";

            if (!receptionistPatientDetailPaymentModel.getAppointmentDate()
                    .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {

                String appointmentDate = receptionistPatientDetailPaymentModel.getAppointmentDate();
                converted_appointment_date = Common.convertDateUsingDateFormat(mActivity, appointmentDate,
                        mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_kk_mm_ss_hyphen),
                        mActivity.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss));

            }

            ((CustomViewHolder) holder).mImagePrint.setOnClickListener(v ->

                    getPaymentPrint(receptionistPatientDetailPaymentModel.getAppointmentID()));

            ((CustomViewHolder) holder).mTextPaymentDate.setText(Common.isEmptyString(mActivity, convertPaymentDate));

            ((CustomViewHolder) holder).mTextPaymentReceipt.setText(Common.isEmptyString(mActivity, receptionistPatientDetailPaymentModel.getOrderNo()));

            ((CustomViewHolder) holder).mTextAppointmentDate.setText(Common.isEmptyString(mActivity, converted_appointment_date));
            ((CustomViewHolder) holder).mTextPaymentAmount.setText(mActivity.getResources().getString(R.string.text_amount) + Common.isEmptyString(mActivity, receptionistPatientDetailPaymentModel.getAmount()));
            ((CustomViewHolder) holder).mTextPaymentType.setText(Common.isEmptyString(mActivity, receptionistPatientDetailPaymentModel.getCheckUpType()));


        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    private void callAlert(String appointmentID) {
        AlertDialog.Builder builder = new AlertDialog.Builder(
                mActivity);
        builder.setTitle("Language Options");
        builder.setMessage("Choose your desired language");
        builder.setNegativeButton("Hindi",
                (dialog, which) -> getPaymentPrint(appointmentID));
        builder.setPositiveButton("English",
                (dialog, which) -> getPaymentPrint(appointmentID));
        builder.show();
    }

    protected void getPaymentPrint(String AppointmentID) {

    }

    protected void onSelectedItem(Activity mActivity, ReceptionistPaymentModel data) {

    }

    @Override
    public int getItemCount() {
        return mArrReceptionistPaymentModel == null ? 0 : mArrReceptionistPaymentModel.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrReceptionistPaymentModel.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView mTextPaymentDate, mTextPaymentReceipt, mTextAppointmentDate,
                mTextPaymentAmount, mTextPaymentType;
        ImageView mImagePrint;

        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Text Views
            this.mTextPaymentDate = itemView.findViewById(R.id.text_row_patient_detail_payment_date);
            this.mTextPaymentReceipt = itemView.findViewById(R.id.text_row_patient_detail_payment_receip);
            this.mTextAppointmentDate = itemView.findViewById(R.id.text_row_patient_detail_appointment_date);
            this.mTextPaymentAmount = itemView.findViewById(R.id.text_row_patient_detail_payment_amount);
            this.mTextPaymentType = itemView.findViewById(R.id.text_row_patient_detail_payment_type);
            this.mImagePrint = itemView.findViewById(R.id.image_print_payment);

        }
    }
}
