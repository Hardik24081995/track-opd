package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.trackopd.R;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.fragments.AddPaymentFragment;
import com.trackopd.fragments.AddPaymentStepOneFragment;
import com.trackopd.model.ReceptionistPatientModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.WebFields;

import java.util.ArrayList;

public class AddPaymentPatientSearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Activity mActivity;
    private ArrayList<ReceptionistPatientModel> mArrReceptionistPatientModel;
    private onSelectedList mSelectListener;

    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private static final int unselectedItem = -1;
    private int selectedItem = unselectedItem;

    public AddPaymentPatientSearchAdapter(Activity mActivity, RecyclerView mRecyclerView, String mIsFromSearch, ArrayList<ReceptionistPatientModel> mArrReceptionistPatients, onSelectedList mSelectListener) {
        this.mActivity = mActivity;
        this.mRecyclerView = mRecyclerView;
        this.mArrReceptionistPatientModel = mArrReceptionistPatients;
        LayoutInflater mLayoutInflater = LayoutInflater.from(mActivity);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        this.mSelectListener = mSelectListener;

        setOnScrollListener();
    }


    public interface onSelectedList {
        void onSelected(ReceptionistPatientModel receptionistPatientsModel);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_receptionist_add_payment_patient_list, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof CustomViewHolder) {
            final ReceptionistPatientModel receptionistPatientsModel = mArrReceptionistPatientModel.get(position);


            // Set Profile Pic
            if (receptionistPatientsModel.getProfileImage()!=null &&
                    !receptionistPatientsModel.getProfileImage().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
                //Set Thumbnail
                String thumbmail_url= WebFields.API_BASE_URL+WebFields.IMAGE_THUMBNAIL_URL+receptionistPatientsModel.getProfileImage();
                Glide.with(mActivity)
                        .load(thumbmail_url)
                        .apply(new RequestOptions().error(R.drawable.ic_documents_img).placeholder(R.drawable.ic_documents_img))
                        .into(((CustomViewHolder) holder).mImageUserProfilePic);
            }else {
                ((CustomViewHolder) holder).mImageUserProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, receptionistPatientsModel.getFirstName(), receptionistPatientsModel.getLastName()));

            }

            
            
            String mPatientName = receptionistPatientsModel.getFirstName() + AppConstants.STR_EMPTY_SPACE + receptionistPatientsModel.getLastName();
            ((CustomViewHolder) holder).mTextName.setText(Common.isEmptyString(mActivity, mPatientName));
            ((CustomViewHolder) holder).mTextMobileNo.setText(Common.isEmptyString(mActivity, receptionistPatientsModel.getMobileNo()));
            ((CustomViewHolder) holder).mTextPatientCode.setText(Common.isEmptyString(mActivity, receptionistPatientsModel.getPatientCode()));
            ((CustomViewHolder) holder).mTextLastVisitedDate.setText(mActivity.getResources().getString(R.string.text_visited_on) + Common.isEmptyString(mActivity, receptionistPatientsModel.getLastVisitedDate()));

            int pos = holder.getAdapterPosition();
            if (AddPaymentFragment.receptionistPatientModel != null) {
                if (AddPaymentFragment.receptionistPatientModel.getPatientCode().equals(receptionistPatientsModel.getPatientCode())) {
                    selectedItem = pos;
                }
            }

            if (pos != selectedItem) {
                ((CustomViewHolder) holder).mImageUserProfileSelectedPic.setVisibility(View.GONE);
            } else {
                ((CustomViewHolder) holder).mImageUserProfileSelectedPic.setVisibility(View.VISIBLE);
            }

            // ToDo: Full View Click Listener
            ((CustomViewHolder) holder).mLinearMainView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = holder.getAdapterPosition();
                    if (pos == selectedItem) {
                        selectedItem = unselectedItem;
                        ReceptionistPatientModel receptionistPatientModel = null;
                        mSelectListener.onSelected(receptionistPatientModel);

                    } else {
                        ((CustomViewHolder) holder).mImageUserProfileSelectedPic.setVisibility(View.VISIBLE);
                        selectedItem = pos;
                        mSelectListener.onSelected(receptionistPatientsModel);
                    }
                    notifyDataSetChanged();
                }
            });

            ((CustomViewHolder) holder).mLinearActionBottom.setVisibility(View.GONE);
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return mArrReceptionistPatientModel == null ? 0 : mArrReceptionistPatientModel.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrReceptionistPatientModel.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    /**
     * Class  RecyclerView in Custom View Holder declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder {

        ImageView mImageUserProfilePic, mImageUserProfileSelectedPic;
        LinearLayout mLinearMainView, mLinearAppointment, mLinearFollowUp, mLinearActionBottom;
        TextView mTextName, mTextMobileNo, mTextPatientCode, mTextLastVisitedDate, mTextAppointment, mTextFollowUp;
        View mViewAction;

        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Image Views
            this.mImageUserProfilePic = itemView.findViewById(R.id.image_row_receptionist_add_payment_patient_pic);
            this.mImageUserProfileSelectedPic = itemView.findViewById(R.id.image_row_receptionist_add_payment_patient_selected_pic);

            // Linear Layouts
            this.mLinearMainView = itemView.findViewById(R.id.linear_row_receptionist_add_payment_patient_main_view);
            this.mLinearAppointment = itemView.findViewById(R.id.linear_row_receptionist_add_payment_patient_appointment);
            this.mLinearFollowUp = itemView.findViewById(R.id.linear_row_receptionist_add_payment_patient_follow_up);
            this.mLinearActionBottom = itemView.findViewById(R.id.linear_row_receptionist_add_payment_patient_bottom_actions);

            // Text Views
            this.mTextName = itemView.findViewById(R.id.text_row_receptionist_add_payment_patient_name);
            this.mTextMobileNo = itemView.findViewById(R.id.text_row_receptionist_add_payment_patient_mobile_no);
            this.mTextPatientCode = itemView.findViewById(R.id.text_row_receptionist_add_payment_patient_code);
            this.mTextLastVisitedDate = itemView.findViewById(R.id.text_row_receptionist_add_payment_patient_last_visited_date);
            this.mTextAppointment = itemView.findViewById(R.id.text_row_receptionist_add_payment_patient_appointment);
            this.mTextFollowUp = itemView.findViewById(R.id.text_row_receptionist_add_payment_patient_follow_up);

            // Views
            this.mViewAction = itemView.findViewById(R.id.view_row_receptionist_add_payment_patient_bottom_actions);
        }
    }
}
