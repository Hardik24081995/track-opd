package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.fragments.AddNewPreliminaryExaminationFragment;
import com.trackopd.model.CheckInListModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.utils.SessionManager;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public class SurgeryListAdapter extends RecyclerView.Adapter {
    private static final int unselectedItem = -1;
    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private Activity mActivity;
    //private ArrayList<SurgeryListModel> mArrSurgery;
    private ArrayList<CheckInListModel.Data> mArrSurgery;
    private AppointmentReasonsAdapter mAdapterAppointmentReasons;
    private int selectedItem = unselectedItem;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private String mUserType, mUserId;


    public SurgeryListAdapter(Activity mActivity, RecyclerView mRecyclerView, ArrayList<CheckInListModel.Data> mArrCheckIn) {
        this.mActivity = mActivity;
        this.mRecyclerView = mRecyclerView;
        mUserId = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_ID);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

        this.mArrSurgery = mArrCheckIn;

        setOnScrollListener();
    }


    public void selectedItem() {
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_surgery_item, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (holder instanceof CustomViewHolder) {

            final CheckInListModel.Data surgeryListModel = mArrSurgery.get(position);

            if (surgeryListModel.getCheckUpType().equals("Surgery")) {

                // Set Profile Pic
                if (surgeryListModel.getProfileImage() != null &&
                        !surgeryListModel.getProfileImage().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                    //Set Thumbnail
                    String thumbmail_url = WebFields.API_BASE_URL + WebFields.IMAGE_THUMBNAIL_URL + surgeryListModel.getProfileImage();
                    Glide.with(mActivity)
                            .load(thumbmail_url)
                            .apply(new RequestOptions().error(R.drawable.ic_documents_img).placeholder(R.drawable.ic_documents_img))
                            .into(((CustomViewHolder) holder).mImageProfile);
                } else {
                    ((CustomViewHolder) holder).mImageProfile.setImageDrawable(Common.setLabeledImageView(mActivity,
                            surgeryListModel.getPatientFirstName(),
                            surgeryListModel.getPatientLastName()));
                }
                ((CustomViewHolder) holder).mTextName.setText(surgeryListModel.getPatientFirstName() +
                        AppConstants.STR_EMPTY_SPACE + surgeryListModel.getPatientLastName());

                ((CustomViewHolder) holder).mTextMobileNo.setText(surgeryListModel.getPatientMobileNo());
                ((CustomViewHolder) holder).mTextPatientCode.setText(surgeryListModel.getPatientCode());
                ((CustomViewHolder) holder).mTextAppointmentNo.setText(surgeryListModel.getTicketNumber());



                ((CustomViewHolder) holder).mTextSurgeryDateTime.setText(surgeryListModel.getCheckINDate());

                ((CustomViewHolder) holder).mTextSurgeryType.setText(surgeryListModel.getDoctorFirstName() +
                        AppConstants.STR_EMPTY_STRING + surgeryListModel.getDoctorLastName());

                ((CustomViewHolder) holder).mLinearItem.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SessionManager manager = new SessionManager(mActivity);
                        manager.setPreferences("CloseFiles", surgeryListModel.getCloseFile());
                        callToSurgeryDetailScreen(surgeryListModel);
                    }
                });
            }

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    private void callToSurgeryDetailScreen(CheckInListModel.Data surgeryListModel) {
        try {

            Bundle mBundle = new Bundle();
            mBundle.putString(AppConstants.BUNDLE_PATIENT_FIRST_NAME, surgeryListModel.getPatientFirstName());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_LAST_NAME, surgeryListModel.getPatientLastName());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_MOBILE, surgeryListModel.getPatientMobileNo());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_PROFILE, surgeryListModel.getProfileImage());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_ID, surgeryListModel.getPatientID());
            mBundle.putInt(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO, Integer.parseInt(surgeryListModel.getAppointmentID()));
            mBundle.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_DOCTOR, surgeryListModel.getDoctorID());
            mBundle.putString(AppConstants.BUNDLE_PATIENT_TREATMENT_DATE, surgeryListModel.getCheckINDate());
            mBundle.putString(AppConstants.BUNDLE_PRELIMINARY_EXAMINATION_ID, "0");
            mBundle.putString(AppConstants.BUNDLE_ADD_PATIENT_PHOTO,surgeryListModel.getProfileImage());
            mBundle.putString(AppConstants.BUNDLE_CHECK_TYPE, mActivity.getResources().getString(R.string.nav_menu_surgery));

            Fragment fragment = new AddNewPreliminaryExaminationFragment();
            fragment.setArguments(mBundle);

            FragmentManager fragmentManager = null;

            if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_patient))) {
                ((PatientHomeActivity) mActivity).setAppHeader(mActivity.getResources
                        ().getString(R.string.nav_menu_surgery));
                fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().replace(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_receptionist_patient_detail))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_receptionist_patient_detail))
                    .commit();
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }


    @Override
    public int getItemCount() {
        return mArrSurgery == null ? 0 : mArrSurgery.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrSurgery.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder implements ExpandableLayout.OnExpansionUpdateListener {

        private TextView mTextName, mTextMobileNo, mTextPatientCode, mTextAppointmentNo, mTextSurgeryDateTime, mTextSurgeryType;
        private ImageView mImageProfile;
        private RelativeLayout mRelativeProfile;
        private LinearLayout mLinearItem;

        public CustomViewHolder(@NonNull View itemView) {
            super(itemView);
            this.mTextName = itemView.findViewById(R.id.text_row_surgery_name);
            this.mTextMobileNo = itemView.findViewById(R.id.text_row_surgery_mobile_no);
            this.mTextPatientCode = itemView.findViewById(R.id.text_row_surgery_code);
            this.mTextAppointmentNo = itemView.findViewById(R.id.text_row_surgery_appointment_no);
            this.mTextSurgeryDateTime = itemView.findViewById(R.id.text_row_surgery_date_time);
            this.mTextSurgeryType = itemView.findViewById(R.id.text_row_surgery_type);

            // Image View
            this.mImageProfile = itemView.findViewById(R.id.image_row_surgery_pic);
            // Relative
            this.mRelativeProfile = itemView.findViewById(R.id.relative_row_surgery_pic);
            //Linear Layout
            this.mLinearItem = itemView.findViewById(R.id.linear_row_surgery_item);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {

        }
    }
}
