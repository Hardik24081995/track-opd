package com.trackopd.adapter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.FullImageActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.fragments.AddDocumentFragment;
import com.trackopd.fragments.ReceptionistPatientDetailDocumentFragment;
import com.trackopd.model.ReceptionistPatientDetailDocumentModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.WebFields;

import java.util.ArrayList;

public class ReceptionistPatientDetailDocumentAdapter extends RecyclerView.Adapter {
    private Activity mActivity;
    private ArrayList<ReceptionistPatientDetailDocumentModel> mArrDetailDocument;
    private LayoutInflater mLayoutInflater;
    public static ReceptionistPatientDetailDocumentModel patientDetailDocumentModel;
    private RecyclerView mRecyclerView;
    private boolean isLoading;
    private int lastVisibleItem, totalItemCount;
    private OnLoadMoreListener mOnLoadMoreListener;
    private int visibleThreshold = 2;
    private String mUserType;

    /**
     * Adapter contains the data to be displayed
     */
    public ReceptionistPatientDetailDocumentAdapter(Activity mActivity, RecyclerView mRecyclerView, ArrayList<ReceptionistPatientDetailDocumentModel> mArrReceptionistPatientDocument) {
        this.mActivity = mActivity;
        this.mArrDetailDocument = mArrReceptionistPatientDocument;
        mLayoutInflater = LayoutInflater.from(mActivity);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        this.mRecyclerView = mRecyclerView;
        mUserType= GetJsonData.getLoginData(mActivity,WebFields.LOGIN.RESPONSE_USER_TYPE);
        setOnScrollListener();

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.row_receptionist_patient_detail_document_item, parent,
                false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, final int position) {
        final ReceptionistPatientDetailDocumentModel receptionistPatientDetailDocumentModel = mArrDetailDocument.get(position);

        ((CustomViewHolder) holder).mRelativeMainView.setTag(position);

        ((CustomViewHolder) holder).mTextDocumentName.setText(Common.isEmptyString(mActivity, receptionistPatientDetailDocumentModel.getReport()));

        if (!receptionistPatientDetailDocumentModel.getReport().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)){
            String document_url= WebFields.API_BASE_URL+WebFields.DOC_URL+receptionistPatientDetailDocumentModel.getReport();

            Glide.with(mActivity)
                    .load(document_url)
                    .apply(new RequestOptions().error(R.drawable.ic_documents_img).placeholder(R.drawable.ic_documents_img))
                    .into(((CustomViewHolder) holder).mImageDocument);
        }


        if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_doctor))
                || mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist)))
        {
            ((CustomViewHolder) holder).mImageDocumentEdit.setVisibility(View.VISIBLE);
        }else {
            ((CustomViewHolder) holder).mImageDocumentEdit.setVisibility(View.GONE);
        }



        // ToDo: Full View Click Listener
        ((CustomViewHolder) holder).mRelativeMainView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, FullImageActivity.class);
                patientDetailDocumentModel = receptionistPatientDetailDocumentModel;
                mActivity.startActivity(intent);
            }
        });

        ((CustomViewHolder) holder).mImageDocumentEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 CallToEditDocumentUpload(receptionistPatientDetailDocumentModel);
            }
        });

    }

    /**
     *  Call To Edit Document Upload Document Call
     *
     * @param receptionistPatientDetailDocumentModel
     */
    private void CallToEditDocumentUpload(ReceptionistPatientDetailDocumentModel receptionistPatientDetailDocumentModel) {
       try{
           Bundle args = new Bundle();
           args.putString(AppConstants.BUNDLE_PIC_NAME,receptionistPatientDetailDocumentModel.getReport());
           args.putString(AppConstants.BUNDLE_DOCUMENT_ID,receptionistPatientDetailDocumentModel.getMyMedicalReportID());
           args.putString(AppConstants.BUNDLE_PATIENT_APPOINTMENT_NO,receptionistPatientDetailDocumentModel.getAppointmentID());

           if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
               ((ReceptionistHomeActivity) mActivity).setAppHeader(mActivity.getResources
                       ().getString(R.string.nav_menu_document));
           } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
               ((OptometristHomeActivity) mActivity).setAppHeader(mActivity.getResources
                       ().getString(R.string.nav_menu_document));
           } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
               ((DoctorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                       ().getString(R.string.nav_menu_document));
           } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
               ((CouncillorHomeActivity) mActivity).setAppHeader(mActivity.getResources
                       ().getString(R.string.nav_menu_document));
           }

           Fragment fragment = new AddDocumentFragment();
           fragment.setArguments(args);

           FragmentManager fragmentManager = null;
           if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_receptionist))) {
               fragmentManager =
                       ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
           } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_optometrist))) {
               fragmentManager =
                       ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
           } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
               fragmentManager =
                       ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
           } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_councillor))) {
               fragmentManager =
                       ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
           }

           fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                   fragment, mActivity.getResources()
                           .getString(R.string.tag_receptionist_patient_detail_document))
                   .addToBackStack(mActivity.getResources()
                           .getString(R.string.back_stack_receptionist_patient_detail_document))
                   .commit();
       }catch (Exception e){
           e.printStackTrace();
       }

    }

    @Override
    public int getItemCount() {
        return mArrDetailDocument.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder {
        RelativeLayout mRelativeMainView;
        ImageView mImageDocument,mImageDocumentEdit;
        TextView mTextDocumentName;

        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Relative Layouts
            this.mRelativeMainView = itemView.findViewById(R.id.relative_row_receptionist_patient_detail_document);

            // Image Views
            this.mImageDocument = itemView.findViewById(R.id.image_row_receptionist_patient_detail_document);
            this.mImageDocumentEdit=itemView.findViewById(R.id.image_row_receptionist_patient_detail_document_edit);
            // Text Views
            this.mTextDocumentName = itemView.findViewById(R.id.text_row_receptionist_patient_detail_document_name);
        }
    }
}