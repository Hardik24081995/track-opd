package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.PatientAppointmentModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;


public class PatientAppointmentAdapter extends RecyclerView.Adapter {
    private Activity mActivity;
    private ArrayList<PatientAppointmentModel> mArrPatientAppointmentModel;

    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;
    private static final int unselectedItem = -1;
    private int selectedItem = unselectedItem;

    public PatientAppointmentAdapter(Activity mActivity, RecyclerView mRecyclerView, ArrayList<PatientAppointmentModel> mArrPatientAppointmentModel) {
        this.mActivity = mActivity;
        this.mRecyclerView = mRecyclerView;
        this.mArrPatientAppointmentModel = mArrPatientAppointmentModel;
        LayoutInflater mLayoutInflater = LayoutInflater.from(mActivity);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;

        setOnScrollListener();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_patient_appointment_item, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (holder instanceof CustomViewHolder) {
            final PatientAppointmentModel patientAppointmentModel = mArrPatientAppointmentModel.get(position);

            ((CustomViewHolder) holder).mRelativeImageBorder.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.user_image_border));

            ((CustomViewHolder) holder).mImageUserProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, patientAppointmentModel.getPatientFirstName(),
                    patientAppointmentModel.getPatientLastName()));

            if (!patientAppointmentModel.getAppointmentStatus().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                ((CustomViewHolder) holder).mImageStatus.setVisibility(View.VISIBLE);
                if (patientAppointmentModel.getAppointmentStatus().equalsIgnoreCase(mActivity.getResources().getString(R.string.status_accept))) {
                    ((CustomViewHolder) holder).mImageStatus.setImageResource(R.drawable.ic_dot_green_icon);
                } else if (patientAppointmentModel.getAppointmentStatus().equalsIgnoreCase(mActivity.getResources().getString(R.string.status_reject))) {
                    ((CustomViewHolder) holder).mImageStatus.setImageResource(R.drawable.ic_dot_red_icon);
                } else if (patientAppointmentModel.getAppointmentStatus().equalsIgnoreCase(mActivity.getResources().getString(R.string.status_pending))) {
                    ((CustomViewHolder) holder).mImageStatus.setImageResource(R.drawable.ic_dot_orange_icon);
                } else if (patientAppointmentModel.getAppointmentStatus().equalsIgnoreCase(mActivity.getResources().getString(R.string.status_completed))) {
                    ((CustomViewHolder) holder).mImageStatus.setImageResource(R.drawable.ic_dot_blue_icon);
                }
            } else {
                ((CustomViewHolder) holder).mImageStatus.setVisibility(View.GONE);
            }

            String mPatientName = patientAppointmentModel.getPatientFirstName() + AppConstants.STR_EMPTY_SPACE + patientAppointmentModel.getPatientLastName();
            String mDoctorName = patientAppointmentModel.getDoctorFirstName() + AppConstants.STR_EMPTY_SPACE + patientAppointmentModel.getDoctorLastName();
            ((CustomViewHolder) holder).mTextName.setText(Common.isEmptyString(mActivity, mPatientName));
            ((CustomViewHolder) holder).mTextMobileNo.setText(Common.isEmptyString(mActivity, patientAppointmentModel.getPatientMobileNo()));
            ((CustomViewHolder) holder).mTextAppointmentNo.setText(Common.isEmptyString(mActivity, patientAppointmentModel.getTicketNumber()));
            ((CustomViewHolder) holder).mTextAppointmentDate.setText(Common.isEmptyString(mActivity, patientAppointmentModel.getAppointmentDate()));
            ((CustomViewHolder) holder).mTextDoctorName.setText(Common.isEmptyString(mActivity, mDoctorName));
            ((CustomViewHolder) holder).mTextBookingType.setText(Common.isEmptyString(mActivity, patientAppointmentModel.getAppointmentType()));
            ((CustomViewHolder) holder).mTextPaymentType.setText(Common.isEmptyString(mActivity, patientAppointmentModel.getPaymentStatus()));

            int pos = holder.getAdapterPosition();
            if (pos != selectedItem) {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                ((CustomViewHolder) holder).mExpandableLayout.collapse();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
            } else {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                ((CustomViewHolder) holder).mExpandableLayout.expand();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
            }

            // ToDo: Expandable Arrow Click Listener
            ((CustomViewHolder) holder).bind();
            ((CustomViewHolder) holder).mLinearExpandableArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                    ((CustomViewHolder) holder).mExpandableLayout.collapse();
                    ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));

                    Common.insertLog("Position:::> " + position);
                    Common.insertLog("selectedItem:::> " + selectedItem);

                    int pos = holder.getAdapterPosition();
                    if (pos == selectedItem) {
                        selectedItem = unselectedItem;
                    } else {
                        ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                        ((CustomViewHolder) holder).mExpandableLayout.expand();
                        ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        selectedItem = pos;
                    }
                    notifyDataSetChanged();
                }
            });
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return mArrPatientAppointmentModel == null ? 0 : mArrPatientAppointmentModel.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrPatientAppointmentModel.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder implements ExpandableLayout.OnExpansionUpdateListener {
        RelativeLayout mRelativeImageBorder;
        ImageView mImageUserProfilePic, mImageStatus, mImageExpandableArrow;
        LinearLayout mLinearExpandableArrow;
        ExpandableLayout mExpandableLayout;
        TextView mTextName, mTextMobileNo, mTextAppointmentNo, mTextAppointmentDate, mTextDoctorName,
                mTextBookingType, mTextPaymentType, mTextPaymentAmount;

        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Relative Layouts
            this.mRelativeImageBorder = itemView.findViewById(R.id.relative_row_patient_appointment_pic);

            // Image Views
            this.mImageUserProfilePic = itemView.findViewById(R.id.image_row_patient_appointment_pic);
            this.mImageStatus = itemView.findViewById(R.id.image_row_patient_appointment_status);
            this.mImageExpandableArrow = itemView.findViewById(R.id.image_row_patient_appointment_expandable_view);

            // Linear Layouts
            this.mLinearExpandableArrow = itemView.findViewById(R.id.linear_row_patient_appointment_expandable_view);

            // Expandable Layout
            this.mExpandableLayout = itemView.findViewById(R.id.expandable_layout_row_patient_appointment);

            // Text Views
            this.mTextName = itemView.findViewById(R.id.text_row_patient_appointment_name);
            this.mTextMobileNo = itemView.findViewById(R.id.text_row_patient_appointment_mobile_no);
            this.mTextAppointmentNo = itemView.findViewById(R.id.text_row_patient_appointment_number);
            this.mTextAppointmentDate = itemView.findViewById(R.id.text_row_patient_appointment_date);
            this.mTextDoctorName = itemView.findViewById(R.id.text_row_patient_appointment_doctor_name);
            this.mTextBookingType = itemView.findViewById(R.id.text_row_patient_appointment_booking_type);
            this.mTextPaymentType = itemView.findViewById(R.id.text_row_patient_appointment_payment_type);
            this.mTextPaymentAmount = itemView.findViewById(R.id.text_row_patient_appointment_payment_amount);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Common.insertLog("ExpandableLayout State : " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                mRecyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }

        /**
         * Binds the expandable view
         */
        public void bind() {
            int position = getAdapterPosition();
            boolean isSelected = position == selectedItem;

            mLinearExpandableArrow.setSelected(isSelected);
            mExpandableLayout.setExpanded(isSelected, false);
        }
    }
}
