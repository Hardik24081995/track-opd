package com.trackopd.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.model.ReceptionistPaymentModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import java.util.ArrayList;

public class MenuPaymentAdapter extends RecyclerView.Adapter<MenuPaymentAdapter.ViewHolder> {

    Context mContext;
    ArrayList<ReceptionistPaymentModel> mArrayList;

    public MenuPaymentAdapter(Context mContext, ArrayList<ReceptionistPaymentModel> mArrayList) {
        this.mContext = mContext;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public MenuPaymentAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View mView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_payment_listmenu, parent , false);

        return new MenuPaymentAdapter.ViewHolder(mView);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuPaymentAdapter.ViewHolder viewHolder, int i) {

        final ReceptionistPaymentModel receptionistPatientDetailPaymentModel = mArrayList.get(i);

        String payementDate = receptionistPatientDetailPaymentModel.getPaymentDate();

        String convertPaymentDate = Common.convertDateUsingDateFormat(mContext, payementDate,
                mContext.getResources().getString(R.string.date_format_dd_mm_yyyy_kk_mm_ss_hyphen),
                mContext.getResources().getString(R.string.date_format_dd_mm_yyyy));

        String converted_appointment_date = "";

        if (!receptionistPatientDetailPaymentModel.getAppointmentDate()
                .equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {

            String appointmentDate = receptionistPatientDetailPaymentModel.getAppointmentDate();
            converted_appointment_date = Common.convertDateUsingDateFormat(mContext, appointmentDate,
                    mContext.getResources().getString(R.string.date_format_dd_mm_yyyy_kk_mm_ss_hyphen),
                    mContext.getResources().getString(R.string.date_format_dd_mm_yyyy_hh_mm_ss));

        }

        viewHolder.mPatientName.setText(Common.isEmptyString(mContext, receptionistPatientDetailPaymentModel.getPatientFirstName() + " " + receptionistPatientDetailPaymentModel.getPatientLastName()));
        viewHolder.mTextPaymentDate.setText(Common.isEmptyString(mContext, convertPaymentDate));

        viewHolder.mTextPaymentReceipt.setText(Common.isEmptyString(mContext, receptionistPatientDetailPaymentModel.getOrderNo()));

        viewHolder.mTextAppointmentDate.setText(Common.isEmptyString(mContext, converted_appointment_date));
        viewHolder.mTextPaymentAmount.setText(mContext.getResources().getString(R.string.text_amount) + Common.isEmptyString(mContext, receptionistPatientDetailPaymentModel.getAmount()));
        viewHolder.mTextPaymentType.setText(Common.isEmptyString(mContext, receptionistPatientDetailPaymentModel.getCheckUpType()));

        viewHolder.mImagePrint.setOnClickListener(v -> {
            getPaymentPrint(receptionistPatientDetailPaymentModel.getAppointmentID());
        });

    }
    protected void getPaymentPrint(String AppointmentID) {

    }
    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView mTextPaymentDate, mTextPaymentReceipt, mTextAppointmentDate,
                mTextPaymentAmount, mTextPaymentType, mPatientName;

        ImageView mImagePrint;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            // Text Views
            this.mTextPaymentDate = itemView.findViewById(R.id.text_row_patient_detail_payment_date);
            this.mTextPaymentReceipt = itemView.findViewById(R.id.text_row_patient_detail_payment_receip);
            this.mTextAppointmentDate = itemView.findViewById(R.id.text_row_patient_detail_appointment_date);
            this.mTextPaymentAmount = itemView.findViewById(R.id.text_row_patient_detail_payment_amount);
            this.mTextPaymentType = itemView.findViewById(R.id.text_row_patient_detail_payment_type);
            this.mPatientName = itemView.findViewById(R.id.text_patient_name);

            this.mImagePrint = itemView.findViewById(R.id.image_print_payment);

        }
    }
}
