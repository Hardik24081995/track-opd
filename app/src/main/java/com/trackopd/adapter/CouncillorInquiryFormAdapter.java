package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.model.CouncillorInquiryFormsModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public class CouncillorInquiryFormAdapter extends RecyclerView.Adapter {

    private Activity mActivity;
    private ArrayList<CouncillorInquiryFormsModel> mArrCouncillorInquiryFormsModel;
    private static final int unselectedItem = -1;
    private int selectedItem = unselectedItem;

    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;

    public CouncillorInquiryFormAdapter(Activity mActivity, RecyclerView mRecyclerView, ArrayList<CouncillorInquiryFormsModel> mArrCouncillorPackage) {
        this.mActivity = mActivity;
        this.mRecyclerView = mRecyclerView;
        this.mArrCouncillorInquiryFormsModel = mArrCouncillorPackage;
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;

        setOnScrollListener();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_councillor_inquiry_forms_item, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (holder instanceof CustomViewHolder) {
            final CouncillorInquiryFormsModel councillorInquiryFormsModel = mArrCouncillorInquiryFormsModel.get(position);

            ((CustomViewHolder) holder).mLinearExpandableArrow.setTag(position);

            ((CustomViewHolder) holder).mRelativeImageBorder.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.user_image_border));

            ((CustomViewHolder) holder).mImageUserProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, councillorInquiryFormsModel.getPatientFirstName(),
                    councillorInquiryFormsModel.getPatientLastName()));

            String mPatientName = councillorInquiryFormsModel.getPatientFirstName() + AppConstants.STR_EMPTY_SPACE + councillorInquiryFormsModel.getPatientLastName();
            String mCouncillorName = councillorInquiryFormsModel.getCouncillorFirstName() + AppConstants.STR_EMPTY_SPACE + councillorInquiryFormsModel.getCouncillorLastName();
            ((CustomViewHolder) holder).mTextPatientName.setText(Common.isEmptyString(mActivity, mPatientName));
            ((CustomViewHolder) holder).mTextPatientMobile.setText(Common.isEmptyString(mActivity, councillorInquiryFormsModel.getPatientMobileNo()));
            ((CustomViewHolder) holder).mTextDate.setText(Common.isEmptyString(mActivity, councillorInquiryFormsModel.getCouncillorDate()));
            ((CustomViewHolder) holder).mTextCouncillorName.setText(Common.isEmptyString(mActivity, mCouncillorName));
            ((CustomViewHolder) holder).mTextCouncillorFor.setText(Common.isEmptyString(mActivity, councillorInquiryFormsModel.getCouncillorFor()));
            ((CustomViewHolder) holder).mTextRecommendedBy.setText(Common.isEmptyString(mActivity, councillorInquiryFormsModel.getRecommandBy()));
            ((CustomViewHolder) holder).mTextRemarks.setText(Common.isEmptyString(mActivity, councillorInquiryFormsModel.getRemarks()));

            int pos = holder.getAdapterPosition();
            if (pos != selectedItem) {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                ((CustomViewHolder) holder).mExpandableLayout.collapse();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
            } else {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                ((CustomViewHolder) holder).mExpandableLayout.expand();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
            }

            // ToDo: Expandable Arrow Click Listener
            ((CustomViewHolder) holder).bind();
            ((CustomViewHolder) holder).mLinearExpandableArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                    ((CustomViewHolder) holder).mExpandableLayout.collapse();
                    ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));

                    Common.insertLog("Position:::> " + position);
                    Common.insertLog("selectedItem:::> " + selectedItem);

                    int pos = holder.getAdapterPosition();
                    if (pos == selectedItem) {
                        selectedItem = unselectedItem;
                    } else {
                        ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                        ((CustomViewHolder) holder).mExpandableLayout.expand();
                        ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        selectedItem = pos;
                    }
                    notifyDataSetChanged();
                }
            });
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return mArrCouncillorInquiryFormsModel == null ? 0 : mArrCouncillorInquiryFormsModel.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrCouncillorInquiryFormsModel.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder implements ExpandableLayout.OnExpansionUpdateListener {
        RelativeLayout mRelativeImageBorder;
        ImageView mImageUserProfilePic, mImageExpandableArrow;
        LinearLayout mLinearExpandableArrow;
        ExpandableLayout mExpandableLayout;
        TextView mTextPatientName, mTextPatientMobile, mTextDate, mTextCouncillorName, mTextCouncillorFor, mTextRecommendedBy, mTextRemarks;

        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Relative Layouts
            this.mRelativeImageBorder = itemView.findViewById(R.id.relative_row_councillor_inquiry_form_patient_pic);

            // Image Views
            this.mImageUserProfilePic = itemView.findViewById(R.id.image_row_councillor_inquiry_form_patient_pic);
            this.mImageExpandableArrow = itemView.findViewById(R.id.image_row_councillor_inquiry_form_expandable_view);

            // Linear Layouts
            this.mLinearExpandableArrow = itemView.findViewById(R.id.linear_row_councillor_inquiry_form_expandable_view);

            // Expandable Layout
            this.mExpandableLayout = itemView.findViewById(R.id.expandable_layout_row_councillor_inquiry_form);

            // Text Views
            mTextPatientName = itemView.findViewById(R.id.text_row_councillor_inquiry_form_patient_name);
            mTextPatientMobile = itemView.findViewById(R.id.text_row_councillor_inquiry_form_patient_mobile_no);
            mTextDate = itemView.findViewById(R.id.text_row_councillor_inquiry_form_councillor_date);
            mTextCouncillorName = itemView.findViewById(R.id.text_row_councillor_inquiry_form_councillor_name);
            mTextCouncillorFor = itemView.findViewById(R.id.text_row_councillor_inquiry_form_councillor_for);
            mTextRecommendedBy = itemView.findViewById(R.id.text_row_councillor_inquiry_form_recommended_by);
            mTextRemarks = itemView.findViewById(R.id.text_row_councillor_inquiry_form_remarks);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Common.insertLog("ExpandableLayout State : " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                mRecyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }

        /**
         * Binds the expandable view
         */
        public void bind() {
            int position = getAdapterPosition();
            boolean isSelected = position == selectedItem;

            mLinearExpandableArrow.setSelected(isSelected);
            mExpandableLayout.setExpanded(isSelected, false);
        }
    }
}
