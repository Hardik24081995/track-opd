package com.trackopd.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.trackopd.R;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.fragments.CheckUpSurgeryFragment;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.HistoryPreliminaryExaminationModel.Data;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.util.ArrayList;

public class HistorySurgeryAdapter extends RecyclerView.Adapter {

    private static final int unselectedItem = -1;
    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    ArrayList<Data> mArrPreliminaryExamination;
    onHistoryDiagnosisListener onEditClickListener;
    private Activity mActivity;
    private int selectedItem = unselectedItem;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;


    public HistorySurgeryAdapter(Activity mActivity, RecyclerView recyclerView, ArrayList<Data> mArrHistory, CheckUpSurgeryFragment checkUpSurgeryFragment) {
        this.mActivity = mActivity;
        this.mRecyclerView = recyclerView;
        this.mArrPreliminaryExamination = mArrHistory;
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
        onEditClickListener = checkUpSurgeryFragment;
    }

    public HistorySurgeryAdapter(Activity mActivity, RecyclerView recyclerView, ArrayList<Data> mArrHistory) {
        this.mActivity = mActivity;
        this.mRecyclerView = recyclerView;
        this.mArrPreliminaryExamination = mArrHistory;
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_history_preliminary_examination_surgery_item, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") int position) {

        if (holder instanceof CustomViewHolder) {

            Data data = mArrPreliminaryExamination.get(position);

            ((CustomViewHolder) holder).mTextDate.setText(data.getTreatmentDate());

            if (data.getSurgery().size() > 0) {
                for (int a = 0; a < data.getSurgery().size(); a++) {

                    Common.insertLog("Investigation..." + data.getInvestigation());

                    HistoryPreliminaryExaminationModel.Surgery surgery = data.getSurgery().get(a);

                    ((CustomViewHolder) holder).mTextCompany.setText(Common.isEmptyString(mActivity, surgery.getCompany()));
                    ((CustomViewHolder) holder).mTextTPA.setText(Common.isEmptyString(mActivity, surgery.getTPA()));
                    ((CustomViewHolder) holder).mTextPolicyNo.setText(Common.isEmptyString(mActivity, surgery.getPolicyNo()));
                    ((CustomViewHolder) holder).mTextExpireOn.setText(Common.isEmptyString(mActivity, surgery.getExpiryDate()));
                    ((CustomViewHolder) holder).mTextSettlementReceived.setText(
                            Common.isEmptyString(mActivity, surgery.getSettlementReceived()));
                    ((CustomViewHolder) holder).mTextSettlementAmount.setText(Common.isEmptyString(mActivity, surgery.getSettlementAmount()));

                    ((CustomViewHolder) holder).mTextPulse.setText(Common.isEmptyString(mActivity, surgery.getPulse()));
                    //((CustomViewHolder) holder).mTextBP.setText(Common.isEmptyString(mActivity,surgery.getBP()));

                    ((CustomViewHolder) holder).mTextSPO2.setText(Common.isEmptyString(mActivity, surgery.getSPO2()));
                    ((CustomViewHolder) holder).mTextRBS.setText(Common.isEmptyString(mActivity, surgery.getRBS()));
                    ((CustomViewHolder) holder).mTextAnaesthesiyaEye.setText(Common.isEmptyString(mActivity, surgery.getEye()));
                    ((CustomViewHolder) holder).mTextAnaesthesiya.setText(Common.isEmptyString(mActivity, surgery.getAnaesthesiaType()));
                    ((CustomViewHolder) holder).mTextAnaesthesiyaMedicine.setText(Common.isEmptyString(mActivity, surgery.getAnaesthesiaMedicine()));
                    ((CustomViewHolder) holder).mTextAnaesthesiyaMedicine.setText(
                            Common.isEmptyString(mActivity, surgery.getAnaesthesiaMedicine()));


                    String convertStartDate = Common.isEmptyString(mActivity,
                            surgery.getSurgeryDate());

                    String convertStartTime = Common.isEmptyString(mActivity,
                            surgery.getStartTime());

                    String EndDate = Common.isEmptyString(mActivity, surgery.getEndDate());
                    String EndTime = Common.isEmptyString(mActivity, surgery.getEndTime());


                    ((CustomViewHolder) holder).mTextStartTime.setText(convertStartDate + AppConstants.STR_EMPTY_SPACE + convertStartTime);
                    ((CustomViewHolder) holder).mTextEndTime.setText(EndDate + AppConstants.STR_EMPTY_SPACE + EndTime);
                    ((CustomViewHolder) holder).mTextSurgery.setText(
                            Common.isEmptyString(mActivity, surgery.getSurgeryName()));
                    ((CustomViewHolder) holder).mTextSurgeryEye.setText(
                            Common.isEmptyString(mActivity, surgery.getSurgeryEye()));
                    ((CustomViewHolder) holder).mTextACD.setText(Common.isEmptyString(mActivity, surgery.getACD()));
                    ((CustomViewHolder) holder).mTextAL.setText(Common.isEmptyString(mActivity, surgery.getAL()));
                    ((CustomViewHolder) holder).mTextIncisionSize.setText(Common.isEmptyString(mActivity, surgery.getIncisionSize()));
                    ((CustomViewHolder) holder).mTextIncisionType.setText(
                            Common.isEmptyString(mActivity, surgery.getIncisionType()));
                    ((CustomViewHolder) holder).mTextViscoelastics.setText(
                            Common.isEmptyString(mActivity, surgery.getViscoelastics()));
                    ((CustomViewHolder) holder).mTextNoteRemarks.setText(Common.isEmptyString(mActivity, surgery.getRemarks()));

                    ((CustomViewHolder) holder).mTextImplantBrand.setText(Common.isEmptyString(mActivity, surgery.getImplantBrand()));
                    ((CustomViewHolder) holder).mTextImplantName.setText(Common.isEmptyString(mActivity, surgery.getImplantName()));
                    ((CustomViewHolder) holder).mTextAcostant.setText(
                            Common.isEmptyString(mActivity, surgery.getAConstant()));
                    ((CustomViewHolder) holder).mTextImplantPower.setText(
                            Common.isEmptyString(mActivity, surgery.getImplantPower()));
                    // ((CustomViewHolder) holder).mTextImplant.setText(surgery.get);
                    ((CustomViewHolder) holder).mTextPlacement.setText(
                            Common.isEmptyString(mActivity, surgery.getImplantPlacement()));
                    ((CustomViewHolder) holder).mTextExpireDate.setText(
                            Common.isEmptyString(mActivity, surgery.getImplantExpiry()));
                    ((CustomViewHolder) holder).mTextSLNo.setText(
                            Common.isEmptyString(mActivity, surgery.getImplantSlNo()));
                    ((CustomViewHolder) holder).mTextImplantNote.setText(
                            Common.isEmptyString(mActivity, surgery.getNotes()));


                    ((CustomViewHolder) holder).mTextSurgry2.setText(
                            Common.isEmptyString(mActivity, surgery.getSurgeryType2()));
                    ((CustomViewHolder) holder).mTextBPSystolic.setText(
                            Common.isEmptyString(mActivity, surgery.getSystolicBP()));
                    ((CustomViewHolder) holder).mTextDiastolic.setText(
                            Common.isEmptyString(mActivity, surgery.getDiastolicBP()));

                }
            }

            ((CustomViewHolder) holder).mImageMediclaim.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((CustomViewHolder) holder).mExpandLayoutMediclaim.isExpanded()) {
                        ((CustomViewHolder) holder).mImageMediclaim.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
                        ((CustomViewHolder) holder).mExpandLayoutMediclaim.collapse();

                    } else {
                        ((CustomViewHolder) holder).mImageMediclaim.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        ((CustomViewHolder) holder).mExpandLayoutMediclaim.expand();
                    }
                }
            });

            ((CustomViewHolder) holder).mImageAnaesthesia.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((CustomViewHolder) holder).mExpandLayoutAnaesthesia.isExpanded()) {
                        ((CustomViewHolder) holder).mImageAnaesthesia.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
                        ((CustomViewHolder) holder).mExpandLayoutAnaesthesia.collapse();
                    } else {
                        ((CustomViewHolder) holder).mImageAnaesthesia.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        ((CustomViewHolder) holder).mExpandLayoutAnaesthesia.expand();
                    }
                }
            });

            ((CustomViewHolder) holder).mImageSurgeryNote.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((CustomViewHolder) holder).mExpandLayoutSurgeryNote.isExpanded()) {
                        ((CustomViewHolder) holder).mImageSurgeryNote.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
                        ((CustomViewHolder) holder).mExpandLayoutSurgeryNote.collapse();
                    } else {
                        ((CustomViewHolder) holder).mImageSurgeryNote.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        ((CustomViewHolder) holder).mExpandLayoutSurgeryNote.expand();
                    }
                }
            });

            ((CustomViewHolder) holder).mImageImplant.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (((CustomViewHolder) holder).mExpandLayoutImplant.isExpanded()) {
                        ((CustomViewHolder) holder).mImageImplant.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
                        ((CustomViewHolder) holder).mExpandLayoutImplant.collapse();
                    } else {
                        ((CustomViewHolder) holder).mImageImplant.setImageDrawable(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        ((CustomViewHolder) holder).mExpandLayoutImplant.expand();
                    }
                }
            });


            ((CustomViewHolder) holder).mImageEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onSelectedItem(mActivity, data);

                    if (onEditClickListener != null) {
                        onEditClickListener.onSelectHistorySurgery(mActivity, data);
                    }

                }
            });

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }


    protected void onSelectedItem(Activity activity, Data data) {
    }


    @Override
    public int getItemCount() {
        return mArrPreliminaryExamination == null ? 0 : mArrPreliminaryExamination.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrPreliminaryExamination.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public interface onHistoryDiagnosisListener {
        void onSelectHistorySurgery(Activity mActivity, Data checkInData);
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder implements ExpandableLayout.OnExpansionUpdateListener {

        ImageView mImageEdit, mImageDate, mImageMediclaim, mImageAnaesthesia, mImageImplant, mImageSurgeryNote;
        TextView mTextDate;
        ExpandableLayout mExpandLayoutMediclaim, mExpandLayoutAnaesthesia, mExpandLayoutSurgeryNote, mExpandLayoutImplant;

        TextView mTextCompany, mTextTPA, mTextPolicyNo, mTextExpireOn, mTextSettlementReceived, mTextSettlementAmount,
                mTextPulse, mTextBP, mTextSPO2, mTextRBS, mTextAnaesthesiyaEye, mTextAnaesthesiya, mTextAnaesthesiyaMedicine,
                mTextImplantBrand, mTextImplantName, mTextAcostant, mTextImplantPower, mTextImplant, mTextPlacement, mTextExpireDate, mTextSLNo, mTextImplantNote,
                mTextStartTime, mTextEndTime, mTextSurgery, mTextSurgeryEye, mTextACD, mTextAL, mTextIncisionSize, mTextIncisionType,
                mTextViscoelastics, mTextNoteRemarks, mTextBPSystolic, mTextDiastolic, mTextSurgry2;

        public CustomViewHolder(final View mView) {
            super(mView);
            //Image View
            this.mImageDate = mView.findViewById(R.id.image_row_history_preliminary_examination_surgery_date);
            this.mImageEdit = mView.findViewById(R.id.image_row_history_preliminary_examination_surgery_edit);
            this.mImageMediclaim = mView.findViewById(R.id.image_row_history_preliminary_examination_surgery_mediclaim);
            this.mImageAnaesthesia = mView.findViewById(R.id.image_row_history_preliminary_examination_surgery_anaesthesia);
            this.mImageSurgeryNote = mView.findViewById(R.id.image_row_history_preliminary_examination_surgery_note);
            this.mImageImplant = mView.findViewById(R.id.image_row_history_preliminary_examination_surgery_implant);

            // Text View
            this.mTextDate = mView.findViewById(R.id.text_row_history_preliminary_examination_surgery_date);

            this.mTextCompany = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_company);
            this.mTextTPA = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_tpa);
            this.mTextPolicyNo = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_policy_no);
            this.mTextExpireOn = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_expire_on);
            this.mTextSettlementReceived = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_settlement);
            this.mTextSettlementAmount = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_settlement_amount);

            this.mTextPulse = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_anaesthesia_pulse);
            this.mTextBP = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_anaesthesia_bp);
            this.mTextBPSystolic = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_anaesthesia_systolic);
            this.mTextDiastolic = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_anaesthesia_diastolic);

            this.mTextSPO2 = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery__anaesthesia_spo_two);
            this.mTextRBS = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_anaesthesia_rbs);
            this.mTextAnaesthesiyaEye = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_anaesthesia_eye);
            this.mTextAnaesthesiya = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_anaesthesia);
            this.mTextAnaesthesiyaMedicine = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_anaesthesia_medicine);


            this.mTextStartTime = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_startTime);
            this.mTextEndTime = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_note_end_time);
            this.mTextSurgery = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_note_surgery);
            this.mTextSurgry2 = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_note_surgery2);
            this.mTextSurgeryEye = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_note_eye);
            this.mTextACD = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_note_acd);
            this.mTextAL = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_note_al);
            this.mTextIncisionSize = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_note_incision_size);
            this.mTextIncisionType = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_note_incision_type);
            this.mTextViscoelastics = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_note_viscoelastics);
            this.mTextNoteRemarks = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_note_remarks);

            this.mTextImplantBrand = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_brand_name);
            this.mTextImplantName = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_implant_name);
            this.mTextAcostant = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_implant_a_constant);
            this.mTextImplantPower = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_implant_power);
            this.mTextImplant = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_implant);
            this.mTextPlacement = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_placement);
            this.mTextExpireDate = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_implant_expire_date);
            this.mTextSLNo = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_implant_sl_no);
            this.mTextImplantNote = mView.findViewById(R.id.text_view_row_history_preliminary_examination_surgery_implant_note);

            //Set Expandable Layout
            this.mExpandLayoutMediclaim = mView.findViewById
                    (R.id.expandable_layout_row_history_preliminary_examination_surgery_medicliam);
            this.mExpandLayoutAnaesthesia = mView.findViewById
                    (R.id.expandable_layout_row_history_preliminary_examination_surgery_anaesthesia);

            this.mExpandLayoutSurgeryNote = mView.findViewById
                    (R.id.expndable_row_history_preliminary_complains_surgery_note);

            this.mExpandLayoutImplant = mView.findViewById
                    (R.id.expandable_layout_row_history_preliminary_examination_surgery_implant);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Common.insertLog("ExpandableLayout State : " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                mRecyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }

        /**
         * Binds the expandable view
         */
        public void bind() {
            int position = getAdapterPosition();
            boolean isSelected = position == selectedItem;
        }
    }

}
