package com.trackopd.adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.trackopd.CouncillorHomeActivity;
import com.trackopd.DoctorHomeActivity;
import com.trackopd.OptometristHomeActivity;
import com.trackopd.PatientHomeActivity;
import com.trackopd.R;
import com.trackopd.ReceptionistHomeActivity;
import com.trackopd.dao.OnLoadMoreListener;
import com.trackopd.fragments.AddNewPreliminaryExaminationFragment;
import com.trackopd.fragments.PatientPrescriptionDetailsFragment;
import com.trackopd.model.HistoryPreliminaryExaminationModel;
import com.trackopd.model.PatientPrescriptionModel;
import com.trackopd.model.PrintReceiptModel;
import com.trackopd.utils.AppConstants;
import com.trackopd.utils.Common;
import com.trackopd.webServices.APICommonMethods;
import com.trackopd.webServices.ApiInterface;
import com.trackopd.webServices.GetJsonData;
import com.trackopd.webServices.RetrofitClient;
import com.trackopd.webServices.WebFields;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.io.File;
import java.util.ArrayList;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientPrescriptionAdapter extends RecyclerView.Adapter {

    private Activity mActivity;
    private ArrayList<HistoryPreliminaryExaminationModel.Data> mArrPatientPrescriptionModel;
    private static final int unselectedItem = -1;
    private int selectedItem = unselectedItem;

    // Load More Listener Variables
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private RecyclerView mRecyclerView;
    private OnLoadMoreListener mOnLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;

    /**
     * Adapter contains the data to be displayed
     */
    public PatientPrescriptionAdapter(Activity mActivity, RecyclerView mRecyclerView, ArrayList<HistoryPreliminaryExaminationModel.Data> mArrPatientPrescriptionModel) {
        this.mActivity = mActivity;
        this.mRecyclerView = mRecyclerView;
        this.mArrPatientPrescriptionModel = mArrPatientPrescriptionModel;
        LayoutInflater mLayoutInflater = LayoutInflater.from(mActivity);
        AppConstants.IS_MATERIAL_DIALOG_EXIST = false;

        setOnScrollListener();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .row_patient_prescription_item, parent, false);
            return new CustomViewHolder(itemView);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout
                    .loading_more_data_footer, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        if (holder instanceof CustomViewHolder) {
            final HistoryPreliminaryExaminationModel.Data patientPrescriptionModel = mArrPatientPrescriptionModel.get(position);

            ((CustomViewHolder) holder).mImageDownloadPDF.setTag(position);
            ((CustomViewHolder) holder).mImagePrescriptionDetails.setTag(position);

            ((CustomViewHolder) holder).mRelativeImageBorder.setBackgroundDrawable(mActivity.getResources().getDrawable(R.drawable.user_image_border));

            ((CustomViewHolder) holder).mImageUserProfilePic.setImageDrawable(Common.setLabeledImageView(mActivity, patientPrescriptionModel.getPatientFirstName(),
                    patientPrescriptionModel.getPatientLastName()));

            String mPatientName = Common.isEmptyString(mActivity, patientPrescriptionModel.getPatientFirstName()) + AppConstants.STR_EMPTY_SPACE + Common.isEmptyString(mActivity, patientPrescriptionModel.getPatientLastName());
            ((CustomViewHolder) holder).mTextName.setText(mPatientName);
            ((CustomViewHolder) holder).mTextMobileNo.setText(Common.isEmptyString(mActivity, patientPrescriptionModel.getPatientMobileNo()));
            ((CustomViewHolder) holder).mTextAppointmentNo.setText(Common.isEmptyString(mActivity, patientPrescriptionModel.getTicketNumber()));
            ((CustomViewHolder) holder).mTextAppointmentDate.setText(Common.isEmptyString(mActivity, patientPrescriptionModel.getAppointmentDate()));
            ((CustomViewHolder) holder).mTextNotes.setText(Common.isEmptyString(mActivity, patientPrescriptionModel.getAppointmentDate()));

            int pos = holder.getAdapterPosition();
            if (pos != selectedItem) {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                ((CustomViewHolder) holder).mExpandableLayout.collapse();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));
            } else {
                ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                ((CustomViewHolder) holder).mExpandableLayout.expand();
                ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
            }

            if (patientPrescriptionModel.getMedication().size() == 0) {
                ((CustomViewHolder) holder).mImageDownloadPDF.setVisibility(View.GONE);
                ((CustomViewHolder) holder).mImagePrescriptionDetails.setVisibility(View.GONE);
            } else {
                ((CustomViewHolder) holder).mImageDownloadPDF.setVisibility(View.VISIBLE);
                ((CustomViewHolder) holder).mImagePrescriptionDetails.setVisibility(View.GONE);
            }


            if (((CustomViewHolder) holder).mLinearContainer.getChildCount() > 0) {
                ((CustomViewHolder) holder).mLinearContainer.removeAllViews();
            }

            AddPrescriptionData(((CustomViewHolder) holder).mLinearContainer, patientPrescriptionModel.getMedication());

            // ToDo: Image PDF Download Click Listener
            ((CustomViewHolder) holder).mImageDownloadPDF.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   /* if(!patientPrescriptionModel.getDocument().equalsIgnoreCase(AppConstants.STR_EMPTY_STRING)) {
                        downloadDocument(patientPrescriptionModel);
                    } else {
                        Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.no_prescription_found));
//                        ((CustomViewHolder) holder).mImageDownloadPDF.setVisibility(View.GONE);
                    }*/
                    callToPrintFinalVision(patientPrescriptionModel.getAppointmentID());
                }
            });

            // ToDo: Image Prescription Detail Click Listener
            /*((CustomViewHolder) holder).mImagePrescriptionDetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    callToPatientPrescriptionDetailsFragment(patientPrescriptionModel);
                }
            });*/

            // ToDo: Expandable Arrow Click Listener
            ((CustomViewHolder) holder).bind();
            ((CustomViewHolder) holder).mLinearExpandableArrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(false);
                    ((CustomViewHolder) holder).mExpandableLayout.collapse();
                    ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_down_arrow));

                    Common.insertLog("Position:::> " + position);
                    Common.insertLog("selectedItem:::> " + selectedItem);

                    int pos = holder.getAdapterPosition();
                    if (pos == selectedItem) {
                        selectedItem = unselectedItem;
                    } else {
                        ((CustomViewHolder) holder).mLinearExpandableArrow.setSelected(true);
                        ((CustomViewHolder) holder).mExpandableLayout.expand();
                        ((CustomViewHolder) holder).mImageExpandableArrow.setBackground(mActivity.getResources().getDrawable(R.drawable.ic_up_arrow));
                        selectedItem = pos;
                    }
                    notifyDataSetChanged();
                }
            });
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    /**
     * This method should check the permissions first and then able to download the document
     *
     * @param patientPrescriptionModel - Patient Prescription Model
     */
    public void downloadDocument(PatientPrescriptionModel patientPrescriptionModel) {
        try {
            if (checkPermission()) {
                File mDownloadedFile = Common.checkIsFileDownloaded(mActivity, mActivity.getResources().getString(R.string.text_medication), patientPrescriptionModel.getDocument());
                if (!(mDownloadedFile.exists())) {
                    callToDownloadPrescriptionsAPI(patientPrescriptionModel);
                } else {
                    Common.openDocument(mActivity, mDownloadedFile, patientPrescriptionModel.getDocument());
                }
            } else {
                requestPermission();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should check the custom permissions
     */
    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(mActivity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * This method should request the custom permissions
     */
    private void requestPermission() {
        try {
            int PERMISSION_REQUEST_CODE = 1;
            ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method should call the prescription API to download prescriptions
     */
    private void callToDownloadPrescriptionsAPI(PatientPrescriptionModel patientPrescriptionModel) {
        try {
            String mUrl = WebFields.API_BASE_URL + WebFields.PDF_URL + patientPrescriptionModel.getDocument();

            Call<ResponseBody> call = RetrofitClient.createService(ApiInterface.class).downloadFile(mUrl);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    try {
                        if (response.isSuccessful()) {
                            boolean isDownloaded = Common.saveFileToStorage(mActivity, mActivity.getResources().getString(R.string.text_medication), response.body(), patientPrescriptionModel.getDocument());

                            // ToDo: Checks if document is downloaded or not, if downloaded then it will open the document
                            File mDownloadedFile = Common.checkIsFileDownloaded(mActivity, mActivity.getResources().getString(R.string.text_medication), patientPrescriptionModel.getDocument());
                            Common.openDocument(mActivity, mDownloadedFile, patientPrescriptionModel.getDocument());
                            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.val_prescription_downloaded_successfully));
                            Common.insertLog("Prescription downloaded::::> " + isDownloaded);

                            // ToDo: Bind the notification
                            /*File mFile = new File(Environment.getExternalStorageDirectory() + "/"
                                    + Common.getFileSdcardPath(mActivity, mActivity.getResources().getString(R.string.text_medication)));
                            if (!mFile.exists()) {
                                mFile.mkdirs();
                            }

                            File file = new File(mFile, patientPrescriptionModel.getDocument());
                            String mFilePath = file.toString();
                            Common.bindNotification(mActivity, mFilePath, file, patientPrescriptionModel.getDocument());*/
                        } else {
                            Common.setCustomToast(mActivity, mActivity.getResources().getString(R.string.error_server));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets up the Patient Prescription Details Fragment
     *
     * @param patientPrescriptionModel - Patient Prescription Model
     */
    private void callToPatientPrescriptionDetailsFragment(final PatientPrescriptionModel patientPrescriptionModel) {
        try {
            String mPatientName = patientPrescriptionModel.getPatientFirstName() + AppConstants.STR_EMPTY_SPACE + patientPrescriptionModel.getPatientLastName();
            String mUserType = GetJsonData.getLoginData(mActivity, WebFields.LOGIN.RESPONSE_USER_TYPE);

            Fragment fragment = new PatientPrescriptionDetailsFragment();
            Bundle args = new Bundle();
            args.putSerializable(AppConstants.BUNDLE_PATIENT_ARRAY, patientPrescriptionModel);
            fragment.setArguments(args);
            FragmentManager fragmentManager = null;

            if (mUserType.equals(mActivity.getResources().getString(R.string.user_type_receptionist))) {
                ((ReceptionistHomeActivity) mActivity).setAppHeader(mPatientName);
                fragmentManager = ((ReceptionistHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getResources().getString(R.string.user_type_optometrist))) {
                ((OptometristHomeActivity) mActivity).setAppHeader(mPatientName);
                fragmentManager = ((OptometristHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_patient))) {
                ((PatientHomeActivity) mActivity).setAppHeader(mPatientName);
                fragmentManager = ((PatientHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((DoctorHomeActivity) mActivity).setAppHeader(mPatientName);
                fragmentManager = ((DoctorHomeActivity) mActivity).getSupportFragmentManager();
            } else if (mUserType.equalsIgnoreCase(mActivity.getString(R.string.user_type_doctor))) {
                ((CouncillorHomeActivity) mActivity).setAppHeader(mPatientName);
                fragmentManager = ((CouncillorHomeActivity) mActivity).getSupportFragmentManager();
            }

            fragmentManager.beginTransaction().add(R.id.fragment_content_frame,
                    fragment, mActivity.getResources()
                            .getString(R.string.tag_prescription_details))
                    .addToBackStack(mActivity.getResources()
                            .getString(R.string.back_stack_prescription_details))
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mArrPatientPrescriptionModel == null ? 0 : mArrPatientPrescriptionModel.size();
    }

    @Override
    public int getItemViewType(int position) {
        return mArrPatientPrescriptionModel.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    /**
     * Id declarations for loading progressbar
     */
    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    /**
     * Load more listener for loading more data
     *
     * @param mOnLoadMoreListener - On Load More Listener
     */
    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    /**
     * This method is used to stop receiving the more data
     */
    public void setLoaded() {
        isLoading = false;
    }

    /**
     * Set on scroll listener
     */
    private void setOnScrollListener() {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) mRecyclerView
                .getLayoutManager();
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (mOnLoadMoreListener != null) {
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    /**
     * Id declarations
     */
    public class CustomViewHolder extends RecyclerView.ViewHolder implements ExpandableLayout.OnExpansionUpdateListener {
        RelativeLayout mRelativeImageBorder;
        LinearLayout mLinearExpandableArrow, mLinearContainer;
        ExpandableLayout mExpandableLayout;
        ImageView mImageUserProfilePic, mImageExpandableArrow, mImageDownloadPDF, mImagePrescriptionDetails;
        TextView mTextName, mTextMobileNo, mTextAppointmentNo, mTextAppointmentDate, mTextNotes;
        ProgressBar mProgressBar;

        public CustomViewHolder(final View itemView) {
            super(itemView);

            // Relative Layouts
            this.mRelativeImageBorder = itemView.findViewById(R.id.relative_row_patient_prescription_pic);

            // Image Views
            this.mImageUserProfilePic = itemView.findViewById(R.id.image_row_patient_prescription_pic);
            this.mImageExpandableArrow = itemView.findViewById(R.id.image_row_patient_prescription_expandable_view);
            this.mImageDownloadPDF = itemView.findViewById(R.id.image_row_patient_prescription_pdf);
            this.mImagePrescriptionDetails = itemView.findViewById(R.id.image_row_patient_prescription_details);

            // Linear Layouts
            this.mLinearExpandableArrow = itemView.findViewById(R.id.linear_row_patient_prescription_expandable_view);
            this.mLinearContainer = itemView.findViewById(R.id.linear_patient_prescription_item_description);

            // Expandable Layout
            this.mExpandableLayout = itemView.findViewById(R.id.expandable_layout_row_receptionist_appointment);

            // Text Views
            this.mTextName = itemView.findViewById(R.id.text_row_patient_prescription_doctor_name);
            this.mTextMobileNo = itemView.findViewById(R.id.text_row_patient_prescription_doctor_mobile_no);
            this.mTextAppointmentNo = itemView.findViewById(R.id.text_row_patient_prescription_appointment_number);
            this.mTextAppointmentDate = itemView.findViewById(R.id.text_row_patient_prescription_appointment_date);
            this.mTextNotes = itemView.findViewById(R.id.text_row_patient_prescription_notes);

            // Progress Bar
            this.mProgressBar = itemView.findViewById(R.id.progress_row_patient_prescription);
        }

        @Override
        public void onExpansionUpdate(float expansionFraction, int state) {
            Common.insertLog("ExpandableLayout State : " + state);
            if (state == ExpandableLayout.State.EXPANDING) {
                mRecyclerView.smoothScrollToPosition(getAdapterPosition());
            }
        }

        /**
         * Binds the expandable view
         */
        public void bind() {
            int position = getAdapterPosition();
            boolean isSelected = position == selectedItem;

            mLinearExpandableArrow.setSelected(isSelected);
            mExpandableLayout.setExpanded(isSelected, false);
        }
    }

    /**
     * @param appointmentID - Appointment ID
     */
    private void callToPrintFinalVision(String appointmentID) {
        try {
            String hospital_database = GetJsonData.getHospitalData(mActivity, AppConstants.KEY_HOSPITAL_DATABASE_NAME);

            boolean mHistoryAndComplaints = false, mVisionUCVA = false, mVisionBCVAUndilated = false, mVisionFinal = false,
                    mVisionBCVADilated = false, mPrimaryExamination = false, mDiagnosis = false, mInvestigationSuggested = false,
                    mTreatmentSuggested = false, mCounselingDetails = false, mSurgeryDetails = false, mPrescription = true,
                    mPayment = false;

            RequestBody body = RequestBody.create(APICommonMethods.getMediaType(),
                    APICommonMethods.setPrintReceiptJson(AddNewPreliminaryExaminationFragment.PreliminaryExminationId,
                            String.valueOf(appointmentID), mHistoryAndComplaints, mVisionUCVA, mVisionBCVAUndilated,
                            mVisionFinal, mVisionBCVADilated, mPrimaryExamination, mDiagnosis, mInvestigationSuggested,
                            mTreatmentSuggested, mCounselingDetails, mSurgeryDetails, mPrescription,"",false, mPayment, hospital_database));

            Call<PrintReceiptModel> call = RetrofitClient.createService(ApiInterface.class).PrintReceipt(body);
            call.enqueue(new Callback<PrintReceiptModel>() {
                @Override
                public void onResponse(@NonNull Call<PrintReceiptModel> call, @NonNull Response<PrintReceiptModel> response) {

                    Common.insertLog("Response::::> " + new Gson().toJson(response.body()));
                    try {
                        String mMessage = response.body().getMessage();
                        Common.insertLog("mMessage:::> " + mMessage);

                        if (response.isSuccessful() &&
                                response.body().getError() == AppConstants.API_SUCCESS_ERROR) {
                            if (response.body() != null) {
                                String url = response.body().getData();
                                callToReceiptPrint(url);
                            }
                        } else {
                            Common.setCustomToast(mActivity, mMessage);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<PrintReceiptModel> call, @NonNull Throwable t) {
                    Common.insertLog("Failure:::> " + t.getMessage());
                }
            });
        } catch (Exception e) {
            Common.insertLog(e.getMessage());
        }
    }

    /**
     * Call To Receipt Print
     *
     * @param url - String Url Print
     */
    private void callToReceiptPrint(String url) {
        /*Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(browserIntent);*/

        //   String pdfOpen = AppConstants.PDF_OPEN + url;
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mActivity.startActivity(browserIntent);
    }

    private void AddPrescriptionData(LinearLayout mLinearContainer, ArrayList<HistoryPreliminaryExaminationModel.Medication> mArrMedications) {

        //set Medication Data
        for (int a = 0; a < mArrMedications.size(); a++) {
            LayoutInflater layoutInflater = (LayoutInflater) mActivity.getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view_medication = layoutInflater.inflate(R.layout.row_prescription_medication_item, null, false);

            TextView mTextMedication = view_medication.findViewById(R.id.text_row_prescription_medication_item_medication);
            TextView mTextStatDate = view_medication.findViewById(R.id.text_row_prescription_medication_item_start_date);
            TextView mTextEndDate = view_medication.findViewById(R.id.text_row_prescription_medication_item_end_date);
            TextView mTextTime = view_medication.findViewById(R.id.text_row_prescription_medication_item_time);

            mTextMedication.setText(mArrMedications.get(a).getMedicationName());
            mTextStatDate.setText(mArrMedications.get(a).getStartDate());
            mTextEndDate.setText(mArrMedications.get(a).getEndDate());

            //String days=mArrMedications.get(a).

            String strTime = "";
            if (!mArrMedications.get(a).getTotalTimes().equalsIgnoreCase("")) {
                int medication = Integer.parseInt(mArrMedications.get(a).getTotalTimes());

                for (int m = 0; m < medication; m++) {

                    if (strTime.equalsIgnoreCase("")) {
                        strTime = "1";
                    } else {
                        strTime = strTime + "-1";
                    }
                }
            }

            mTextTime.setText(Common.isEmptyString(mActivity, strTime));

            mLinearContainer.addView(view_medication);
        }
    }
}